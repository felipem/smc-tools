﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace MOPLA_Launcher.Entities
{
    class CaseLauncher
    {
        # region Properties

        private volatile int _tempCode;
        private readonly MProgressBar _mProgressBar;
        private readonly string _moplaCaseFolder;

        private readonly MoplaLPaths _moplaLPaths;

        private readonly MChecker _mChecker;
        private readonly ControlSwitcher _controlSwitcher;

        private TaskFactory _factory;
        private TaskScheduler _context;
        private readonly CancellationToken _tokenNeverCancel;

        # endregion

        # region Constructor

        public CaseLauncher(MoplaLPaths moplaLPaths, string moplaCaseFolder, ControlSwitcher controlSwitcher, MProgressBar mProgressBar)
        {
            _moplaCaseFolder = moplaCaseFolder;
            _moplaLPaths = moplaLPaths;
            _controlSwitcher = controlSwitcher;
            _mChecker = new MChecker(_moplaCaseFolder);
            _mProgressBar = mProgressBar;

            CancellationTokenSource tokenSource = new CancellationTokenSource();
            _tokenNeverCancel = tokenSource.Token;
        }

        # endregion

        public void CreateFactory(int maxDegreeOfParallelism)
        {
            // Create Task factory with a maximum degree of parallelism
            LimitedConcurrencyLevelTaskScheduler lcts = new LimitedConcurrencyLevelTaskScheduler(maxDegreeOfParallelism);
            _factory = new TaskFactory(lcts);
            _context = TaskScheduler.FromCurrentSynchronizationContext();
        }

        public void StartExecution(List<MCase> lstCasesToExecute, List<MMesh> lstMeshes, CancellationTokenSource tokenSource)
        {
            // Get token from external cancelation token sources
            CancellationToken token = tokenSource.Token;

            // Create a list of tasks to store each case task
            List<Task> caseTasksList = new List<Task>();

            // Reset temporal code int
            _tempCode = 1;

            // Initialize progress bar 
            _mProgressBar.ResetParameters();
            _mProgressBar.SetNumCases(lstCasesToExecute.Count);

            // Start one Task for each case to launch
            foreach (MCase mCase in lstCasesToExecute)
            {
                Task mainTask = _factory.StartNew(() =>
                {
                    // Do not start new cases if user asks for cancelation
                    if (!token.IsCancellationRequested)
                    {
                        // Fill progress bar (half case)
                        Task aux = Task.Factory.StartNew(() => _mProgressBar.AddHalfTask(), _tokenNeverCancel, TaskCreationOptions.None, _context); aux.Wait();

                        // Parent Mesh in Case
                        MMesh mMesh = lstMeshes.Find(mesh => mesh.MeshCode.Equals(mCase.CaseParameters.MeshCode));

                        // Initialize Case Log
                        CaseLog cLog = new CaseLog(_moplaCaseFolder, mCase.CaseParameters.Code);

                        // Start case processing
                        StartCaseProcessing(mCase, token);

                        #region Establece en la aplicatión el "." como separador decimal (solo valido para Windows)
                        CultureInfo cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
                        Thread.CurrentThread.CurrentCulture = cultureInfo;
                        #endregion

                        // Create in0.dat file for SpecWin
                        CreateIn0(mCase, mMesh, token, cLog);

                        // Create in.dat file
                        CreateInOlucaSp(mCase, mMesh, token, cLog);

                        // Execute OLUCA model
                        bool olucaSpSuccess = ExecuteOlucaSp(mCase, mMesh, token, cLog);

                        // Solve Case Child Meshes
                        foreach (string childMeshCode in mCase.CaseParameters.ListChildrenMeshCode)
                        {
                            if (olucaSpSuccess)
                            {
                                // CutterSp
                                olucaSpSuccess = DoCutterSp(mCase, mMesh, token, cLog);
                                if (!olucaSpSuccess)
                                {
                                    break;
                                }

                                // Change to next child mesh
                                mMesh = lstMeshes.Find(mesh => mesh.MeshCode.Equals(childMeshCode));

                                // Create in.dat file
                                CreateInOlucaSp(mCase, mMesh, token, cLog);

                                // Execute Oluca for child mesh
                                olucaSpSuccess = ExecuteOlucaSp(mCase, mMesh, token, cLog);
                            }
                        }

                        // Check case cancellation 
                        if (token.IsCancellationRequested)
                        {
                            // Change Case Status to Cancelled
                            SetStatusCancelled(mCase, cLog);
                        }

                        else if (olucaSpSuccess)
                        {
                            // Execute Copla
                            if (mCase.CoplaParameters.ExecuteCopla)
                            {
                                // Create CoplaSP input file
                                CreateCoplaDatFile(mCase, mMesh, token, cLog);

                                // Execute CoplaSP model
                                ExecuteCoplaSp(mCase, mMesh, token, cLog);
                            }

                            // Change Case Status to Completed
                            SetStatusComplete(mCase, cLog);
                        }

                        else // OlucaSP execution error
                        {
                            // Get OlucaSP error
                            enumCaseExecInfo olucaSpExecutionInfo = _mChecker.GetOlucaSPError(cLog.GetLogFilePath());

                            // Change Case Status to OlucaSp error
                            SetStatusOlucaError(mCase, mMesh, token, olucaSpExecutionInfo);
                        }

                        // Fill progress bar (half case)
                        aux = Task.Factory.StartNew(() => _mProgressBar.AddHalfTask(), _tokenNeverCancel, TaskCreationOptions.None, _context); aux.Wait();
                    }
                });

                caseTasksList.Add(mainTask);
            }

            // Array with all the Tasks created for each simulation case
            Task[] caseTasksArray = caseTasksList.ToArray();

            // Next Task will be started upon the completion of Mopla Cases Execution
            _factory.ContinueWhenAll(caseTasksArray, completedTasks =>
            {
                // Enable / Disable neccessary buttons
                Task aux = Task.Factory.StartNew(ResetMfButtons, _tokenNeverCancel, TaskCreationOptions.None, _context); aux.Wait();

                if (token.IsCancellationRequested)
                {
                    // Fill progress bar
                    aux = Task.Factory.StartNew(() => _mProgressBar.AllTasksFinished(), _tokenNeverCancel, TaskCreationOptions.None, _context); aux.Wait();
                }
            });
        }


        private void StartCaseProcessing(MCase mCase, CancellationToken token)
        {
            // Return if task is cancelled
            if (token.IsCancellationRequested)
            {
                return;
            }

            // Display on GUI
            Task childTask = Task.Factory.StartNew(() => ChangeCaseStatus(mCase, enumCaseStatus.Processing), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);
            childTask = Task.Factory.StartNew(() => changeLVIColor(mCase.CaseLvItem, Color.Black), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);
        }

        private void CreateIn0(MCase mCase, MMesh mMesh, CancellationToken token, CaseLog cLog)
        {
            // Return if task is cancelled
            if (token.IsCancellationRequested)
            {
                return;
            }

            string in0FileName = mCase.CaseParameters.MasterCode + "IN0.DAT";
            string pathToIn0File = Path.Combine(_moplaCaseFolder, "SP", in0FileName);

            // Create and write in0.dat file
            StreamWriter fileW = new StreamWriter(pathToIn0File);
            fileW.WriteLine("{0}  {1}  {2}", 0, 5, 10); // First =0 to create [CODE]_in.dat. 
            fileW.WriteLine("{0} {1}", mMesh.XDivision, mMesh.YDivision);
            fileW.WriteLine("{0} {1} {2} {3}", 1, (int)mCase.CaseSpectrum.Linearity, 0, Convert.ToInt32(mCase.CaseSpectrum.ContoursOpen));
            fileW.WriteLine("{0:0.000} {1:0.000} {2:0.000}", mMesh.XSpacing, mMesh.YSpacing, mCase.CaseParameters.Dt);
            fileW.WriteLine(" {0} {1}", 0, mMesh.SubdivisionChildren * mCase.CaseParameters.SubdivisionYLast);
            fileW.WriteLine("{0} {1} {2}", Convert.ToInt32(mCase.CaseSpectrum.BottomDissipationModel.isTurbulent), Convert.ToInt32(mCase.CaseSpectrum.BottomDissipationModel.isPorous), Convert.ToInt32(mCase.CaseSpectrum.BottomDissipationModel.isLaminar));
            fileW.WriteLine("{0}", (int)mCase.CaseSpectrum.BreakingDissipationModel);
            fileW.WriteLine(" {0}   {1}", Convert.ToInt32(mMesh.meshHasParent) + 1, Convert.ToInt32(mMesh.meshHasChildren) + 1);
            fileW.WriteLine("{0:0.000}", mCase.CaseSpectrum.Tide);
            fileW.WriteLine("{0}", 0);
            fileW.WriteLine("{0} {1}", 1, 1);
            fileW.WriteLine("{0}", 0);
            fileW.WriteLine(" {0} {1} {2} {3}", 1, mMesh.XDivisionLastChildren, 1, mMesh.YDivisionLastChildren);
            fileW.WriteLine(" {0} {1}", 0, Convert.ToInt32(mCase.CaseSpectrum.SpecUnits == enumSpecUnits.CGS));
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.TmaSpecDepth);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.TmaSpecPeakFrequency);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.TmaSpecMaxFrequency);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.TmaSpecHs);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.TmaSpecGamma);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.TmaSpecComponentsNum);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.MeanDirection);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.DirSpecDispersion);
            fileW.WriteLine(" {0}", mCase.CaseSpectrum.DirComponentsNum);
            fileW.WriteLine(" {0} {1}", 0, 0);
            fileW.WriteLine("{0}", 0);
            fileW.Close();

            // Display on GUI
            Task childTask = Task.Factory.StartNew(() => ChangeCaseExecInfo(mCase, mMesh, enumCaseExecInfo.In0Created), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);

            // Write Log
            cLog.FileCreated(in0FileName);
        }

        private void CreateInOlucaSp(MCase mCase, MMesh mMesh, CancellationToken token, CaseLog cLog)
        {
            if (token.IsCancellationRequested)
            {
                return;
            }

            String spFolderPath = Path.Combine(_moplaCaseFolder, "SP");
            string inFileName = mMesh.MeshCode + mCase.CaseParameters.Code + "_IN.DAT";
            string inFilePath = Path.Combine(spFolderPath, inFileName);

            if (mMesh.meshHasParent == false) // Parent mesh
            {
                // Get temporal code from volatil int
                int tempCode = _tempCode;
                _tempCode += 1;

                // Rename IN0.DAT file to 4 characters TEMPORAL CODE
                string in0FileName = mCase.CaseParameters.MasterCode + "IN0.DAT";
                string tempin0FileName = string.Format("{0:0000}", tempCode) + "IN0.DAT";
                string tempinFileName = string.Format("{0:0000}", tempCode) + "in.dat";

                string tempin0FilePath = Path.Combine(spFolderPath, tempin0FileName);
                string tempinFilePath = Path.Combine(spFolderPath, tempinFileName);
                string in0FilePath = Path.Combine(spFolderPath, in0FileName);


                if (File.Exists(tempin0FilePath))
                {
                    File.Delete(tempin0FilePath);
                }
                File.Copy(in0FilePath, tempin0FilePath);

                // Execute SpecWin
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WorkingDirectory = spFolderPath;
                //startInfo.Arguments = String.Format("/C {0} {1}", _moplaLPaths.GetSpecWinExecPath(), tempin0FileName);
                startInfo.Arguments = tempin0FileName;
                //startInfo.FileName = "CMD.exe";
                startInfo.FileName = _moplaLPaths.GetSpecWinExecPath();
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                Process exeProcess = Process.Start(startInfo);
                exeProcess.WaitForExit();

                // Return file to its original name
                File.Delete(Path.Combine(spFolderPath, tempin0FileName));

                if (File.Exists(inFilePath))
                {
                    File.Delete(inFilePath);
                }
                File.Move(tempinFilePath, inFilePath);

                // Change inFile first line 
                string[] lines = File.ReadAllLines(inFilePath);
                using (StreamWriter writer = new StreamWriter(inFilePath))
                {
                    for (int currentLine = 1; currentLine <= lines.Length; ++currentLine)
                    {
                        if (currentLine == 1)
                        {
                            writer.WriteLine("    {0}    {1}   {2}", 1, 4, 10);
                        }
                        else
                        {
                            writer.WriteLine(lines[currentLine - 1]);
                        }
                    }
                }

                // Read direction and period components from in.dat file
                GetCompDataFromMasterInFile(mCase);
            }

            else // Children mesh
            {
                // Write to _in.dat file:
                StreamWriter fileInW = new StreamWriter(inFilePath);

                // Header
                fileInW.WriteLine("    {0}    {1}   {2}", 1, 4, 10);
                fileInW.WriteLine("  {0}  {1}", mMesh.XDivision, mMesh.YDivision);
                fileInW.WriteLine("    {0}    {1}    {2}    {3}", 1, (int)mCase.CaseSpectrum.Linearity, 0, Convert.ToInt32(mCase.CaseSpectrum.ContoursOpen));
                fileInW.WriteLine("    {0:0.000}    {1:0.000}     {2:0.000}", mMesh.XSpacing, mMesh.YSpacing, mCase.CaseParameters.Dt);
                fileInW.WriteLine("    {0}    {1}", 0, mMesh.SubdivisionChildren * mCase.CaseParameters.SubdivisionYLast);
                fileInW.WriteLine("    {0}    {1}    {2}", Convert.ToInt32(mCase.CaseSpectrum.BottomDissipationModel.isTurbulent), Convert.ToInt32(mCase.CaseSpectrum.BottomDissipationModel.isPorous), Convert.ToInt32(mCase.CaseSpectrum.BottomDissipationModel.isLaminar));
                fileInW.WriteLine("    {0}", (int)mCase.CaseSpectrum.BreakingDissipationModel);
                fileInW.WriteLine(" {0}   {1}", Convert.ToInt32(mMesh.meshHasParent) + 1, Convert.ToInt32(mMesh.meshHasChildren) + 1);
                fileInW.WriteLine("{0} {1} {2}", 1, mCase.CaseSpectrum.TmaSpecComponentsNum, mCase.CaseSpectrum.DirComponentsNum);

                // Body
                fileInW.WriteLine("{0:0.000000} {1:0.000}", mCase.CaseSpectrum.PeriodComps[0], mCase.CaseSpectrum.Tide);
                for (int i = 1; i < mCase.CaseSpectrum.PeriodComps.Count(); i++)
                {
                    fileInW.WriteLine("{0:0.000000}", mCase.CaseSpectrum.PeriodComps[i]);
                }

                // Footer
                foreach (string line in mCase.CaseParameters.LstStrMasterInDatFooter)
                {
                    fileInW.WriteLine(line);
                }

                fileInW.Close();
            }

            // Write Log
            cLog.FileCreated(inFileName);
        }

        private void CreateCoplaDatFile(MCase mCase, MMesh mMesh, CancellationToken token, CaseLog cLog)
        {
            if (token.IsCancellationRequested)
            {
                return;
            }
            String spFolderPath = Path.Combine(_moplaCaseFolder, "SP");
            string datFileName = mMesh.MeshCode + mCase.CaseParameters.Code + "DAT";

            //COPLA DAT FILE
            string datFilePath = Path.Combine(spFolderPath, datFileName);

            if (File.Exists(datFilePath))
            {
                File.Delete(datFilePath);
            }

            StreamWriter fileDatW = new StreamWriter(datFilePath);

            fileDatW.WriteLine("* Fichero de control de COPLA");
            fileDatW.WriteLine("* Malla: {0}", mMesh.MeshCode);
            fileDatW.WriteLine("* COPLA: {0}", mCase.CaseParameters.Code);
            fileDatW.WriteLine("F (2F10.3,3I5)");
            fileDatW.WriteLine("     {0:0.000}     {1:0.000}    1   {2}    1", mCase.CoplaParameters.Interval, mCase.CoplaParameters.Chezy, mCase.CoplaParameters.Iterations);
            fileDatW.WriteLine("F (2F10.3,I5)");
            fileDatW.WriteLine("    {0:0.000}     0.000    3", mCase.CoplaParameters.Eddy);
            fileDatW.WriteLine("F (3I5)");
            fileDatW.WriteLine("    1    0    0");
            fileDatW.WriteLine("F (I5)");
            fileDatW.WriteLine("    0");
            fileDatW.WriteLine("F (1F10.3)");
            fileDatW.WriteLine("     {0:0.000}", mCase.CaseSpectrum.Tide);
            fileDatW.Close();

            // Write Log
            cLog.FileCreated(datFileName);
        }


        private void GetCompDataFromMasterInFile(MCase mCase)
        {
            // Initialize variables
            mCase.CaseSpectrum.PeriodComps = new double[mCase.CaseSpectrum.TmaSpecComponentsNum];
            mCase.CaseSpectrum.DirComps = new double[mCase.CaseSpectrum.DirComponentsNum];
            mCase.CaseSpectrum.IntensityComp = new double();
            mCase.CaseParameters.LstStrMasterInDatFooter = new List<string>();

            try
            {
                string line;

                string pathToMasterInFile = Path.Combine(_moplaCaseFolder, "SP", mCase.CaseParameters.MasterCode + "_IN.DAT");
                StreamReader fileMInR = new StreamReader(pathToMasterInFile);

                // Skip header
                for (int i = 0; i < 9; i++)
                {
                    fileMInR.ReadLine();
                }
                // Read necessary data from father mesh _in.dat file
                for (int i = 0; i < mCase.CaseSpectrum.PeriodComps.Count(); i++)
                {
                    line = fileMInR.ReadLine();
                    mCase.CaseSpectrum.PeriodComps[i] = Convert.ToDouble(line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)[0]);

                    // Skip line with number of Periods
                    fileMInR.ReadLine();

                    // Get directions and intensity at first iteration
                    if (i == 0)
                    {
                        for (int j = 0; j < mCase.CaseSpectrum.DirComps.Count(); j++)
                        {
                            line = fileMInR.ReadLine();
                            mCase.CaseSpectrum.DirComps[j] = Convert.ToDouble(line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)[1]);

                            if (j == 0)
                            {
                                mCase.CaseSpectrum.IntensityComp = Convert.ToDouble(line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)[0]);
                            }
                        }
                    }
                    else
                    {
                        // Skip lines
                        for (int j = 0; j < mCase.CaseSpectrum.DirComps.Count(); j++)
                        {
                            fileMInR.ReadLine();
                        }
                    }
                }
                // Get last parameters in mastercodeIN.dat
                while ((line = fileMInR.ReadLine()) != null)
                {
                    mCase.CaseParameters.LstStrMasterInDatFooter.Add(line);
                }
                fileMInR.Close();
            }
            catch { }
        }

        private bool ExecuteOlucaSp(MCase mCase, MMesh mMesh, CancellationToken token, CaseLog cLog)
        {
            if (token.IsCancellationRequested)
            {
                return false;
            }

            // Display on GUI
            Task childTask = Task.Factory.StartNew(() => ChangeCaseExecInfo(mCase, mMesh, enumCaseExecInfo.OlucaInProcess), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);

            // Execute Oluca
            string spFolderPath = Path.Combine(_moplaCaseFolder, "SP");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = spFolderPath;
            //startInfo.Arguments = String.Format("/C {0} {1}", _moplaLPaths.GetOlucaExecPath(), mMesh.MeshCode + mCase.CaseParameters.Code);
            startInfo.Arguments =  mMesh.MeshCode + mCase.CaseParameters.Code;
            //startInfo.FileName = "CMD.exe";
            startInfo.FileName = _moplaLPaths.GetOlucaExecPath();

            // Redirect process output:
            startInfo.RedirectStandardOutput = true;
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;

            // Clog: Start OlucaSP notification
            cLog.StartOlucaSp(mMesh.MeshCode);

            Process exeProcess = Process.Start(startInfo);

            // Check if process has not started
            if (exeProcess.StandardOutput.EndOfStream)
            {
                cLog.OlucaSpStartFailed();
                return false;
            }

            // Get process output
            List<string> lstOutputLines = new List<string>();
            bool startedCalc = false;

            while (!exeProcess.StandardOutput.EndOfStream)
            {
                string line = exeProcess.StandardOutput.ReadLine();
                
                if (string.IsNullOrEmpty(line)) continue;
                if (!line[0].Equals('+')) continue;

                startedCalc = true;
                cLog.AddLine(line);
                lstOutputLines.Add(line);
            }
            cLog.AddLine("");
            exeProcess.WaitForExit();

            // If there are no lines starting with '+' -> OlucaSP start failed
            if (!startedCalc)
            {
                cLog.OlucaSpStartFailed();
                return false;
            }

            // Search for int numbers at last line
            string lastLine = lstOutputLines.Last();
            Regex regex = new Regex(@"\d+");
            var listMatch = (from Match match in regex.Matches(lastLine) select match.Value).ToList();

            if (listMatch.Count == 2)
            {
                if (listMatch[0].Equals(listMatch[1]) & lastLine[0].Equals('+')) // OlucaSP completion
                {
                    return true;
                }

                // Write error in log and return
                cLog.OlucaSpFailed();
                return false;
            }
            return false;
        }

        private bool DoCutterSp(MCase mCase, MMesh mMesh, CancellationToken token, CaseLog cLog)
        {
            if (token.IsCancellationRequested)
            {
                return false;
            }

            // TODO: REPASAR ESTE METODO, SE PUEDE HACER MUCHO MEJOR Y MAS SIMPLE

            string winFileName = mMesh.ChildrenMeshCode + mCase.CaseParameters.Code + "WIN.DAT";
            string pathToWouFile = Path.Combine(_moplaCaseFolder, "SP", mMesh.MeshCode + mCase.CaseParameters.Code + "wou.dat");
            string pathToWinFile = Path.Combine(_moplaCaseFolder, "SP", winFileName);

            StreamReader fileR = new StreamReader(pathToWouFile);
            StreamWriter fileW = new StreamWriter(pathToWinFile);
            string line;

            int nSubDivY = mMesh.SubdivisionChildren * mCase.CaseParameters.SubdivisionYLast;

            int IN = mMesh.ChildrenNodeInit * nSubDivY;
            int FN = mMesh.ChildrenNodeEnd * nSubDivY;
            int N = (mMesh.YDivision - 1) * nSubDivY + 1;
            int NCOMP = mCase.CaseSpectrum.DirComponentsNum * mCase.CaseSpectrum.TmaSpecComponentsNum;


            List<String> lstStrAux;

            // Cut amplitude components
            for (int i = 0; i < NCOMP; i++)
            {
                // Discard first nodes
                lstStrAux = new List<string>();
                while (lstStrAux.Count < N)
                {
                    line = fileR.ReadLine();
                    lstStrAux.AddRange(line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList());
                }

                // Write nodes interval
                for (int j = IN; j <= FN; j += 2)
                {
                    if (j + 1 < lstStrAux.Count)
                    {
                        fileW.WriteLine("{0} {1}", lstStrAux[j], lstStrAux[j + 1]);
                    }
                    else
                    {
                        fileW.WriteLine("{0}", lstStrAux[j]);
                    }
                }
            }

            // Cut directional components
            for (int i = 0; i < NCOMP; i++)
            {
                // Discard first nodes
                lstStrAux = new List<string>();
                while (lstStrAux.Count < N)
                {
                    line = fileR.ReadLine();
                    if (line != null)
                    {
                        lstStrAux.AddRange(line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList());
                    }
                    else
                    {
                        // Write error in log and return
                        cLog.OlucaSpFailed();
                        return false;
                    }
                }

                // Write nodes interval
                for (int j = IN; j <= FN; j += 4)
                {
                    if (j + 3 < lstStrAux.Count)
                    {
                        fileW.WriteLine("{0} {1} {2} {3}", lstStrAux[j], lstStrAux[j + 1], lstStrAux[j + 2], lstStrAux[j + 3]);
                    }
                    else
                    {
                        string auxStrLastLine = string.Empty;
                        int numLeft = FN - j;

                        for (int k = j; k <= j + numLeft; k++)
                        {
                            auxStrLastLine += lstStrAux[k] + " ";
                        }
                        fileW.WriteLine(auxStrLastLine);
                    }
                }
            }

            fileW.Close();
            fileR.Close();

            // Write Log
            cLog.FileCreated(winFileName);

            return true;
        }

        private void ExecuteCoplaSp(MCase mCase, MMesh mMesh, CancellationToken token, CaseLog cLog)
        {
            if (token.IsCancellationRequested)
            {
                return;
            }

            // Display on GUI
            Task childTask = Task.Factory.StartNew(() => ChangeCaseExecInfo(mCase, mMesh, enumCaseExecInfo.CoplaInProcess), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);

            // Execute Copla
            string spFolderPath = Path.Combine(_moplaCaseFolder, "SP");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = spFolderPath;
            //startInfo.Arguments = String.Format("/C {0} {1}",  _moplaLPaths.GetCoplaExecPath(), mMesh.MeshCode + mCase.CaseParameters.Code);
            startInfo.Arguments = mMesh.MeshCode + mCase.CaseParameters.Code;
            //startInfo.FileName = "CMD.exe";
            startInfo.FileName =  _moplaLPaths.GetCoplaExecPath();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            Process exeProcess = Process.Start(startInfo);
            exeProcess.WaitForExit();

            // Write error in log and return
            cLog.CoplaSpSucess();
        }


        private void SetStatusOlucaError(MCase mCase, MMesh mMesh, CancellationToken token, enumCaseExecInfo olucaSpExecutionInfo)
        {
            // Display on GUI
            Task childTask = Task.Factory.StartNew(() => ChangeCaseStatus(mCase, enumCaseStatus.Error), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);
            childTask = Task.Factory.StartNew(() => changeLVIColor(mCase.CaseLvItem, Color.Red), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);

            childTask = Task.Factory.StartNew(() => ChangeCaseExecInfo(mCase, mMesh, olucaSpExecutionInfo), token, TaskCreationOptions.None, _context); waitIfNotCancelled(childTask, token);
        }

        private void SetStatusComplete(MCase mCase, CaseLog cLog)
        {
            // Independent tokenSource

            _factory.StartNew(() =>
            {
                // Change Case Status to Cancelled
                mCase.CaseParameters.CaseStatus = enumCaseStatus.Completed;
                mCase.CaseParameters.CaseExecInfo = enumCaseExecInfo.SimulationFinished;

                mCase.CaseLvItem.SubItems[1].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseStatus);
                mCase.CaseLvItem.SubItems[2].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseExecInfo);
                changeLVIColor(mCase.CaseLvItem, Color.DarkBlue);

                // Write Log
                cLog.SetCaseComplete();

            }, _tokenNeverCancel, TaskCreationOptions.None, _context);
        }

        private void SetStatusCancelled(MCase mCase, CaseLog cLog)
        {
            _factory.StartNew(() =>
            {
                // Change Case Status to Cancelled
                mCase.CaseParameters.CaseStatus = enumCaseStatus.Cancelled;
                mCase.CaseParameters.CaseExecInfo = enumCaseExecInfo.NoInfo;

                mCase.CaseLvItem.SubItems[1].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseStatus);
                mCase.CaseLvItem.SubItems[2].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseExecInfo);
                changeLVIColor(mCase.CaseLvItem, Color.Red);

                // Write Log
                cLog.SetCaseCancelled();

            }, _tokenNeverCancel, TaskCreationOptions.None, _context);
        }

        private void waitIfNotCancelled(Task childTask, CancellationToken token)
        {
            if (!token.IsCancellationRequested)
            {
                childTask.Wait();
            }
        }


        private void ChangeCaseStatus(MCase mCase, enumCaseStatus enumCaseStatus)
        {
            mCase.CaseParameters.CaseStatus = enumCaseStatus;
            mCase.CaseLvItem.SubItems[1].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseStatus);
        }

        private void ChangeCaseExecInfo(MCase mCase, MMesh mMesh, enumCaseExecInfo enumCaseExecInfo)
        {
            mCase.CaseParameters.CaseExecInfo = enumCaseExecInfo;
            mCase.CaseLvItem.SubItems[2].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseExecInfo);

            if (enumCaseExecInfo == enumCaseExecInfo.OlucaInProcess)
            {
                mCase.CaseLvItem.SubItems[2].Text = string.Format("{0} Solving mesh: {1}", GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseExecInfo), mMesh.MeshCode);
            }
        }

        private void changeLVIColor(ListViewItem listViewItem, Color color)
        {
            foreach (ListViewItem.ListViewSubItem subItem in listViewItem.SubItems)
            {
                subItem.ForeColor = color;
            }
        }

        private void ResetMfButtons()
        {
            _controlSwitcher.LeaveExecutionMode();
        }
    }


    # region Auxiliar Classes

    public class LimitedConcurrencyLevelTaskScheduler : TaskScheduler
    {
        /// <summary>Whether the current thread is processing work items.</summary>
        [ThreadStatic]
        private static bool _currentThreadIsProcessingItems;
        /// <summary>The list of tasks to be executed.</summary> 
        private readonly LinkedList<Task> _tasks = new LinkedList<Task>(); // protected by lock(_tasks) 
        /// <summary>The maximum concurrency level allowed by this scheduler.</summary> 
        private readonly int _maxDegreeOfParallelism;
        /// <summary>Whether the scheduler is currently processing work items.</summary> 
        private int _delegatesQueuedOrRunning = 0; // protected by lock(_tasks) 

        /// <summary> 
        /// Initializes an instance of the LimitedConcurrencyLevelTaskScheduler class with the 
        /// specified degree of parallelism. 
        /// </summary> 
        /// <param name="maxDegreeOfParallelism">The maximum degree of parallelism provided by this scheduler.</param>
        public LimitedConcurrencyLevelTaskScheduler(int maxDegreeOfParallelism)
        {
            if (maxDegreeOfParallelism < 1) throw new ArgumentOutOfRangeException("maxDegreeOfParallelism");
            _maxDegreeOfParallelism = maxDegreeOfParallelism;
        }

        /// <summary>Queues a task to the scheduler.</summary> 
        /// <param name="task">The task to be queued.</param>
        protected sealed override void QueueTask(Task task)
        {
            // Add the task to the list of tasks to be processed.  If there aren't enough 
            // delegates currently queued or running to process tasks, schedule another. 
            lock (_tasks)
            {
                _tasks.AddLast(task);
                if (_delegatesQueuedOrRunning < _maxDegreeOfParallelism)
                {
                    ++_delegatesQueuedOrRunning;
                    NotifyThreadPoolOfPendingWork();
                }
            }
        }

        /// <summary> 
        /// Informs the ThreadPool that there's work to be executed for this scheduler. 
        /// </summary> 
        private void NotifyThreadPoolOfPendingWork()
        {
            ThreadPool.UnsafeQueueUserWorkItem(_ =>
            {
                // Note that the current thread is now processing work items. 
                // This is necessary to enable inlining of tasks into this thread.
                _currentThreadIsProcessingItems = true;
                try
                {
                    // Process all available items in the queue. 
                    while (true)
                    {
                        Task item;
                        lock (_tasks)
                        {
                            // When there are no more items to be processed, 
                            // note that we're done processing, and get out. 
                            if (_tasks.Count == 0)
                            {
                                --_delegatesQueuedOrRunning;
                                break;
                            }

                            // Get the next item from the queue
                            item = _tasks.First.Value;
                            _tasks.RemoveFirst();
                        }

                        // Execute the task we pulled out of the queue 
                        TryExecuteTask(item);
                    }
                }
                // We're done processing items on the current thread 
                finally { _currentThreadIsProcessingItems = false; }
            }, null);
        }

        /// <summary>Attempts to execute the specified task on the current thread.</summary> 
        /// <param name="task">The task to be executed.</param>
        /// <param name="taskWasPreviouslyQueued"></param>
        /// <returns>Whether the task could be executed on the current thread.</returns> 
        protected sealed override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            // If this thread isn't already processing a task, we don't support inlining 
            if (!_currentThreadIsProcessingItems) return false;

            // If the task was previously queued, remove it from the queue 
            if (taskWasPreviouslyQueued) TryDequeue(task);

            // Try to run the task. 
            return TryExecuteTask(task);
        }

        /// <summary>Attempts to remove a previously scheduled task from the scheduler.</summary> 
        /// <param name="task">The task to be removed.</param>
        /// <returns>Whether the task could be found and removed.</returns> 
        protected sealed override bool TryDequeue(Task task)
        {
            lock (_tasks) return _tasks.Remove(task);
        }

        /// <summary>Gets the maximum concurrency level supported by this scheduler.</summary> 
        public sealed override int MaximumConcurrencyLevel { get { return _maxDegreeOfParallelism; } }

        /// <summary>Gets an enumerable of the tasks currently scheduled on this scheduler.</summary> 
        /// <returns>An enumerable of the tasks currently scheduled.</returns> 
        protected sealed override IEnumerable<Task> GetScheduledTasks()
        {
            bool lockTaken = false;
            try
            {
                Monitor.TryEnter(_tasks, ref lockTaken);
                if (lockTaken) return _tasks.ToArray();
                else throw new NotSupportedException();
            }
            finally
            {
                if (lockTaken) Monitor.Exit(_tasks);
            }
        }
    }
    
    # endregion
}

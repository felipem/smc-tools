﻿using System;
using System.IO;

namespace MOPLA_Launcher.Entities
{
    public class CaseLog
    {
        private readonly string _moplaCaseFolder;
        private readonly string _caseCode;
        private string _caseLogFilePath;
        private StreamWriter _fileLogW;

        public CaseLog(string moplaCaseFolder, String caseCode)
        {
            _moplaCaseFolder = moplaCaseFolder;
            _caseCode = caseCode;

            InitializeLog();
        }

        public void InitializeLog()
        {
            string logFolderPath = Path.Combine(_moplaCaseFolder, "SP", "CaseLogs");
            if (!Directory.Exists(logFolderPath))
            {
                Directory.CreateDirectory(logFolderPath);
            }

            _caseLogFilePath = Path.Combine(logFolderPath, _caseCode + ".log");

            if (File.Exists(_caseLogFilePath))
            {
                File.Delete(_caseLogFilePath);
            }

            _fileLogW = new StreamWriter(_caseLogFilePath, true);

            // Write Log Header
            _fileLogW.WriteLine("--------------------------------------------------------------");
            _fileLogW.WriteLine("MOPLA CASE SIMULATION LOG FILE{0}", Environment.NewLine);
            _fileLogW.WriteLine("Case name: {0}{1}", _caseCode, Environment.NewLine);
            _fileLogW.WriteLine("Date of creation: {0}", DateTime.Now);
            _fileLogW.WriteLine("--------------------------------------------------------------{0}", Environment.NewLine);
            _fileLogW.Close();
        }

        public string GetLogFilePath()
        {
            return _caseLogFilePath;
        }

        public void FileCreated(string fileName)
        {
            File.AppendAllText(_caseLogFilePath, string.Format("[ {0} ]{1}", DateTime.Now, Environment.NewLine));
            File.AppendAllText(_caseLogFilePath, string.Format("Created file: {0}{1}{1}", fileName, Environment.NewLine));
        }

        public void StartOlucaSp(string meshCode)
        {
            File.AppendAllText(_caseLogFilePath, string.Format("[ {0} ]{1}", DateTime.Now, Environment.NewLine));
            File.AppendAllText(_caseLogFilePath, string.Format("Executing OLUCASP numerical model ...{0}", Environment.NewLine));
            File.AppendAllText(_caseLogFilePath, string.Format("Mesh: {0}{1}{1}", meshCode, Environment.NewLine));
        }

        public void OlucaSpStartFailed()
        {
            File.AppendAllText(_caseLogFilePath, string.Format("-- OLUCASP could not start. --{0}", Environment.NewLine));
        }

        public void OlucaSpFailed()
        {
            File.AppendAllText(_caseLogFilePath, string.Format("-- OLUCASP execution failed. --{0}", Environment.NewLine));
        }

        public void CoplaSpSucess()
        {
            File.AppendAllText(_caseLogFilePath, string.Format("[ {0} ]{1}", DateTime.Now, Environment.NewLine));
            File.AppendAllText(_caseLogFilePath, string.Format("COPLASP execution finished.{0}{0}", Environment.NewLine));
        }

        public void AddLine(string line)
        {
            File.AppendAllText(_caseLogFilePath, string.Format("{0}{1}",line,Environment.NewLine));
        }

        public void SetCaseComplete()
        {
            File.AppendAllText(_caseLogFilePath, string.Format("-- NUMERICAL SIMULATION FINISHED WITHOUT PROBLEMS. --"));
        }

        public void SetCaseCancelled()
        {
            File.AppendAllText(_caseLogFilePath, string.Format("-- NUMERICAL SIMULATION CANCELLED BY USER. --"));
        }
    }
}

﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace MOPLA_Launcher.Entities
{
    class ControlSwitcher
    {
        private readonly List<Control> _lstCtrlOffWhenExecuting;
        private readonly List<ToolStripItem> _lstTsmiOffWhenExecuting;
        private readonly List<Control> _lstCTrlOnWhenExecution;

        public bool IsExecuting = false;

        public ControlSwitcher(List<Control> lstCtrlOffWhenExecuting, List<ToolStripItem> lstTsmiOffWhenExecuting, List<Control> lstOnWhenExecution)
        {
            _lstCtrlOffWhenExecuting = lstCtrlOffWhenExecuting;
            _lstCTrlOnWhenExecution = lstOnWhenExecution;
            _lstTsmiOffWhenExecuting = lstTsmiOffWhenExecuting;
        }

        public void SetExecutionMode()
        {
            IsExecuting = true;

            foreach (Control control in _lstCtrlOffWhenExecuting)
            {
                control.Enabled = false;
            }
            foreach (ToolStripItem Tstrip in _lstTsmiOffWhenExecuting)
            {
                Tstrip.Enabled = false;
            }
            foreach (Control button in _lstCTrlOnWhenExecution)
            {
                button.Enabled = true;
            }
        }

        public void LeaveExecutionMode()
        {
            IsExecuting = false;

            foreach (Control control in _lstCtrlOffWhenExecuting)
            {
                control.Enabled = true;
            }
            foreach (ToolStripItem Tstrip in _lstTsmiOffWhenExecuting)
            {
                Tstrip.Enabled = true;
            }
            foreach (Control control in _lstCTrlOnWhenExecution)
            {
                control.Enabled = false;
            }
        }
    }
}

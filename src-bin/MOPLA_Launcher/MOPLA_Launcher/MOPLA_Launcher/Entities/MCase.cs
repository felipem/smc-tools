﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MOPLA_Launcher.Entities
{
    public class MCase
    {
        // Spectrum Data
        public MSpectrum CaseSpectrum = new MSpectrum();
        // Copla Data
        public MCoplaParameters CoplaParameters = new MCoplaParameters();
        // Case General Parameters
        public MCaseParameters CaseParameters = new MCaseParameters();

        // ListViewItem
        public ListViewItem CaseLvItem;

        // Case files
        private readonly string _encFilePath;
        private readonly string _in2FilePath;
        private readonly string _cop2FilePath;

        // Constructor
        public MCase(string encFile)
        {
            // Case Files
            _encFilePath = encFile;
            _in2FilePath = encFile.Replace("ENC.DAT", "IN2.DAT");

            // Predefined Parameters
            CaseParameters.Dt = 0.333;

            // Read Case Files
            ReadEncDat();
            ReadIn2Dat();

            // Copla File
            string coplaMeshCode = CaseParameters.NumMesh > 1 ? CaseParameters.ListChildrenMeshCode[CaseParameters.ListChildrenMeshCode.Count - 1] : CaseParameters.MeshCode;
            string cop2FileName = coplaMeshCode + CaseParameters.Code + "COP2.DAT";
            _cop2FilePath = Path.Combine(Path.GetDirectoryName(encFile), cop2FileName);
            if (File.Exists(_cop2FilePath))
            {
                ReadCop2Dat();
            }

            // Case ListView Item
            string[] row = { CaseParameters.Code, GetEnumDesc.GetEnumDescription(CaseParameters.CaseStatus), GetEnumDesc.GetEnumDescription(CaseParameters.CaseExecInfo) };
            CaseLvItem = new ListViewItem(row) { UseItemStyleForSubItems = false };
        }


        private void ReadEncDat()
        {
            // Read ...ENC.DAT file
            var encDic = DicFromEqualsFile(_encFilePath);

            CaseParameters.Code = encDic["Clave"];
            CaseParameters.Description = encDic["Desc"];
            CaseParameters.NumMesh = Convert.ToInt32(encDic["NumMallas"]);
            CaseParameters.SubdivisionYLast = Convert.ToInt32(encDic["SubdivisionesYUltima"]);
            CaseParameters.MasterCode = encDic["CasoMaestro"];
            CaseParameters.CoplaCode = encDic["ClaveCopla"];
            CaseParameters.Zoom = Convert.ToInt32(encDic["Zoom"]);
            CaseParameters.ZoomMinX = Convert.ToInt32(encDic["ZoomMinX"]);
            CaseParameters.ZoomMinY = Convert.ToInt32(encDic["ZoomMinY"]);
            CaseParameters.ZoomMaxX = Convert.ToInt32(encDic["ZoomMaxX"]);
            CaseParameters.ZoomMaxY = Convert.ToInt32(encDic["ZoomMaxY"]);
            CaseParameters.LastModifiedDate = encDic["FechaModificacion"];
            CaseParameters.PspecCalcEn = Convert.ToSingle(encDic["CalcularEn"]);
            CaseParameters.PspecType = Convert.ToInt32(encDic["Tipo"]);
            CaseParameters.FreeSurfCalcEn = Convert.ToSingle(encDic["CalcularEn2"]);
            CaseParameters.MeshCode = encDic["CasoMaestro"].Replace(CaseParameters.Code, string.Empty);

            // Read [MALLAS] entry from ENC.DAT file
            if (CaseParameters.NumMesh <= 1) return;
            for (int i = 1; i < CaseParameters.NumMesh; i++)
            {
                CaseParameters.ListChildrenMeshCode.Add(encDic["Malla" + i]);
            }
        }

        private void ReadIn2Dat()
        {
            // Read ...IN2.DAT file
            var inTwoDic = DicFromEqualsFile(_in2FilePath);

            CaseSpectrum.Desc = inTwoDic["Desc"];
            CaseSpectrum.CaseType = inTwoDic["TipoCaso"];
            CaseSpectrum.LastModifiedDate = inTwoDic["FechaModificacion"];
            CaseSpectrum.ContoursOpen = inTwoDic["Contornos"].Equals("Abiertos");

            CaseSpectrum.BottomDissipationModel = new BottomDissipationModel
            {
                isLaminar = inTwoDic["Amortiguacion"].Contains("Laminar"),
                isPorous = inTwoDic["Amortiguacion"].Contains("FondoPoroso"),
                isTurbulent = inTwoDic["Amortiguacion"].Contains("Turbulenta")
            };

            if (inTwoDic["Linealidad"].Equals("Lineal"))
            {
                CaseSpectrum.Linearity = enumModelLinearity.Linear;
            }
            else if (inTwoDic["Linealidad"].Equals("Compuesto"))
            {
                CaseSpectrum.Linearity = enumModelLinearity.Composed;
            }
            else if (inTwoDic["Linealidad"].Equals("Stokes"))
            {
                CaseSpectrum.Linearity = enumModelLinearity.Stokes;
            }

            CaseSpectrum.Period = Convert.ToSingle(inTwoDic["Periodo"]);
            CaseSpectrum.Tide = Convert.ToSingle(inTwoDic["Marea"]);
            CaseSpectrum.Amplitude = Convert.ToSingle(inTwoDic["Amplitud"]);
            CaseSpectrum.Direction = Convert.ToSingle(inTwoDic["Direccion"]);
            CaseSpectrum.MaxSlope = Convert.ToSingle(inTwoDic["PendienteMaxFond"]);
            CaseSpectrum.FilterType = inTwoDic["TipoFiltro"];
            CaseSpectrum.SpecPropagate = Convert.ToInt32(inTwoDic["Propagar"]);
            CaseSpectrum.SpecFrequency = Convert.ToInt32(inTwoDic["Frecuencia"]);
            CaseSpectrum.SpecDirection = Convert.ToInt32(inTwoDic["Direccion2"]);

            if (inTwoDic["TipoEspectro"].Equals("TMA"))
            {
                CaseSpectrum.SpecType = enumSpecTypes.TMA;
            }
            else
            {
                CaseSpectrum.SpecType = enumSpecTypes.Readed;
            }

            if (inTwoDic["Unidades"].Equals("CGS"))
            {
                CaseSpectrum.SpecUnits = enumSpecUnits.CGS;
            }
            else if (inTwoDic["Unidades"].Equals("MKS"))
            {
                CaseSpectrum.SpecUnits = enumSpecUnits.MKS;
            }

            if (inTwoDic["ModeloDisipacion"].Equals("Thornton"))
            {
                CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Thornton;
            }
            else if (inTwoDic["ModeloDisipacion"].Equals("Battjes"))
            {
                CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Battjes;
            }
            else if (inTwoDic["ModeloDisipacion"].Equals("Winyu"))
            {
                CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Winyu;
            }

            CaseSpectrum.MeanDirection = Convert.ToSingle(inTwoDic["DireccionMedia"]);
            CaseSpectrum.DirSpecDispersion = Convert.ToSingle(inTwoDic["DispersionEspectroDireccional"]);
            CaseSpectrum.DirComponentsNum = Convert.ToInt32(inTwoDic["NumComponentesDireccionales"]);
            CaseSpectrum.TmaSpecDepth = Convert.ToSingle(inTwoDic["Profundidad"]);
            CaseSpectrum.TmaSpecPeakFrequency = Convert.ToSingle(inTwoDic["FrecuenciaPico"]);
            CaseSpectrum.TmaSpecMaxFrequency = Convert.ToSingle(inTwoDic["FrecuenciaMaxima"]);
            CaseSpectrum.TmaSpecHs = Convert.ToSingle(inTwoDic["Hs"]);
            CaseSpectrum.TmaSpecGamma = Convert.ToSingle(inTwoDic["Gama"]);
            CaseSpectrum.TmaSpecComponentsNum = Convert.ToInt32(inTwoDic["NumComponentes"]);
        }

        private void ReadCop2Dat()
        {
            // Read ...COP2.DAT file
            var cop2Dic = DicFromEqualsFile(_cop2FilePath);

            CoplaParameters.Code = cop2Dic["Clave"];
            CoplaParameters.Desc = cop2Dic["Descripcion"];
            CoplaParameters.Interval = Convert.ToSingle(cop2Dic["Intervalo"]);
            CoplaParameters.FileWritings = cop2Dic["EscriturasFichero"];
            CoplaParameters.Iterations = Convert.ToInt32(cop2Dic["Iteraciones"]);
            CoplaParameters.WriteBeginning = cop2Dic["ComienzoEscritura"];
            CoplaParameters.LastModifiedDate = cop2Dic["FechaModificacion"];
            CoplaParameters.Chezy = Convert.ToSingle(cop2Dic["Chezy"]);
            CoplaParameters.Eddy = Convert.ToSingle(cop2Dic["Eddy"]);
            CoplaParameters.Coriolis = cop2Dic["Coriolis"];
            CoplaParameters.NumNonLinearTerms = cop2Dic["NumTerminosNoLineales"];
            CoplaParameters.NonLinearity = cop2Dic["NoLinealidad"];
            CoplaParameters.Flooding = cop2Dic["Inundacion"];
            CoplaParameters.BoundaryFriction = cop2Dic["FriccionContorno"];
            CoplaParameters.TotalTime = Convert.ToInt32(cop2Dic["TiempoTotal"]);
            CoplaParameters.Tide = Convert.ToSingle(cop2Dic["Marea"]);

            CoplaParameters.ExecuteCopla = true;
        }


        public void SaveCase()
        {
            // Write ...IN2.DAT file
            WriteIn2Dat();

            // Write ...COP2.DAT file
            if (CoplaParameters.ExecuteCopla)
            {
                WriteCop2Dat();
            }

            //TODO: AL GUARDAR UN CASO HAY QUE REINICIAR SU LISTVIEW ITEM A PENDIENTE Y CARGARSE LOS FICHEROS RESULTADO
        }

        private void WriteIn2Dat()
        {
            // Aux. string
            string auxStrContornos = (CaseSpectrum.ContoursOpen) ? "Abiertos" : "Cerrados";

            string auxStrAmortiguacion = "";
            if (CaseSpectrum.BottomDissipationModel.isTurbulent)
            {
                auxStrAmortiguacion += "Turbulenta ";
            }
            if (CaseSpectrum.BottomDissipationModel.isPorous)
            {
                auxStrAmortiguacion += "FondoPoroso ";
            }
            if (CaseSpectrum.BottomDissipationModel.isLaminar)
            {
                auxStrAmortiguacion += "Laminar ";
            }
            auxStrAmortiguacion += " ";

            string auxStrLinealidad = "";
            switch (CaseSpectrum.Linearity)
            {
                case enumModelLinearity.Linear:
                    auxStrLinealidad = "Lineal";
                    break;
                case enumModelLinearity.Composed:
                    auxStrLinealidad = "Compuesto";
                    break;
                case enumModelLinearity.Stokes:
                    auxStrLinealidad = "Stokes";
                    break;
            }

            string auxStrTipoEspectro = "";
            switch (CaseSpectrum.SpecType)
            {
                case enumSpecTypes.TMA:
                    auxStrTipoEspectro = "TMA";
                    break;
                case enumSpecTypes.Readed:
                    //TODO: Texto desconocido si es lectura de fichero
                    break;
            }

            string auxStrUnidades = "";
            switch (CaseSpectrum.SpecUnits)
            {
                case enumSpecUnits.CGS:
                    auxStrUnidades = "CGS";
                    break;
                case enumSpecUnits.MKS:
                    auxStrUnidades = "MKS";
                    break;
            }

            string auxStrModeloDisipacion = "";
            switch (CaseSpectrum.BreakingDissipationModel)
            {
                case enumBreakingDissipationModel.Thornton:
                    auxStrModeloDisipacion = "Thornton";
                    break;
                case enumBreakingDissipationModel.Battjes:
                    auxStrModeloDisipacion = "Battjes";
                    break;
                case enumBreakingDissipationModel.Winyu:
                    auxStrModeloDisipacion = "Winyu";
                    break;
            }

            CaseSpectrum.LastModifiedDate = DateTime.Now.ToString();

            // File writing
            StreamWriter in2FileW = new StreamWriter(_in2FilePath);

            in2FileW.WriteLine("[GENERAL]");
            in2FileW.WriteLine("Desc={0}", CaseSpectrum.Desc);
            in2FileW.WriteLine("TipoCaso={0}", CaseSpectrum.CaseType);
            in2FileW.WriteLine("FechaModificacion={0}", CaseSpectrum.LastModifiedDate);

            in2FileW.WriteLine("[PARAMETROS]");
            in2FileW.WriteLine("Contornos={0}", auxStrContornos);
            in2FileW.WriteLine("Amortiguacion={0}", auxStrAmortiguacion);
            in2FileW.WriteLine("Linealidad={0}", auxStrLinealidad);
            in2FileW.WriteLine("Periodo={0}", CaseSpectrum.Period);
            in2FileW.WriteLine("Marea={0}", CaseSpectrum.Tide);
            in2FileW.WriteLine("Amplitud={0}", CaseSpectrum.Amplitude);
            in2FileW.WriteLine("Direccion={0}", CaseSpectrum.Direction);
            in2FileW.WriteLine("PendienteMaxFond={0}", CaseSpectrum.MaxSlope);
            in2FileW.WriteLine("TipoFiltro={0}", CaseSpectrum.FilterType);

            in2FileW.WriteLine("[COMPONENTES_ESPECTRO]");
            in2FileW.WriteLine("Propagar={0}", CaseSpectrum.SpecPropagate);
            in2FileW.WriteLine("Frecuencia={0}", CaseSpectrum.SpecFrequency);
            in2FileW.WriteLine("Direccion={0}", CaseSpectrum.SpecDirection);

            in2FileW.WriteLine("[ESPECTRO]");
            in2FileW.WriteLine("TipoEspectro={0}", auxStrTipoEspectro);
            in2FileW.WriteLine("Unidades={0}", auxStrUnidades);
            in2FileW.WriteLine("ModeloDisipacion={0}", auxStrModeloDisipacion);
            in2FileW.WriteLine("DireccionMedia={0}", CaseSpectrum.MeanDirection);
            in2FileW.WriteLine("DispersionEspectroDireccional={0}", CaseSpectrum.DirSpecDispersion);
            in2FileW.WriteLine("NumComponentesDireccionales={0}", CaseSpectrum.DirComponentsNum);

            in2FileW.WriteLine("[ESPECTRO_TMA]");
            in2FileW.WriteLine("Profundidad={0}", CaseSpectrum.TmaSpecDepth);
            in2FileW.WriteLine("FrecuenciaPico={0}", CaseSpectrum.TmaSpecPeakFrequency);
            in2FileW.WriteLine("FrecuenciaMaxima={0}", CaseSpectrum.TmaSpecMaxFrequency);
            in2FileW.WriteLine("Hs={0}", CaseSpectrum.TmaSpecHs);
            in2FileW.WriteLine("Gama={0}", CaseSpectrum.TmaSpecGamma);
            in2FileW.WriteLine("NumComponentes={0}", CaseSpectrum.TmaSpecComponentsNum);

            in2FileW.Close();
        }

        private void WriteCop2Dat()
        {
            CoplaParameters.LastModifiedDate = DateTime.Now.ToString();

            // File writing
            StreamWriter cop2FileW = new StreamWriter(_cop2FilePath);

            cop2FileW.WriteLine("[GENERAL]");
            cop2FileW.WriteLine("Clave={0}", CoplaParameters.Code);
            cop2FileW.WriteLine("Descripcion={0}", CoplaParameters.Desc);
            cop2FileW.WriteLine("Intervalo={0}", CoplaParameters.Interval);
            cop2FileW.WriteLine("EscriturasFichero={0}", CoplaParameters.FileWritings);
            cop2FileW.WriteLine("Iteraciones={0}", CoplaParameters.Iterations);
            cop2FileW.WriteLine("ComienzoEscritura={0}", CoplaParameters.WriteBeginning);
            cop2FileW.WriteLine("FechaModificacion={0}", CoplaParameters.LastModifiedDate);

            cop2FileW.WriteLine("[PARAMETROS]");
            cop2FileW.WriteLine("Chezy={0}", CoplaParameters.Chezy);
            cop2FileW.WriteLine("Eddy={0}", CoplaParameters.Eddy);
            cop2FileW.WriteLine("Coriolis={0}", CoplaParameters.Coriolis);

            cop2FileW.WriteLine("NumTerminosNoLineales={0}", CoplaParameters.NumNonLinearTerms);
            cop2FileW.WriteLine("NoLinealidad={0}", CoplaParameters.NonLinearity);
            cop2FileW.WriteLine("Inundacion={0}", CoplaParameters.Flooding);
            cop2FileW.WriteLine("FriccionContorno={0}", CoplaParameters.BoundaryFriction);
            cop2FileW.WriteLine("TiempoTotal={0}", CoplaParameters.TotalTime);
            cop2FileW.WriteLine("Marea={0}", CoplaParameters.Tide);

            cop2FileW.Close();
        }


        public void ResetCaseLVIState()
        {
            CaseParameters.CaseStatus = enumCaseStatus.Pending;
            CaseParameters.CaseExecInfo = enumCaseExecInfo.NoInfo;

            CaseLvItem.SubItems[1].Text = GetEnumDesc.GetEnumDescription(CaseParameters.CaseStatus);
            CaseLvItem.SubItems[2].Text = GetEnumDesc.GetEnumDescription(CaseParameters.CaseExecInfo);

            foreach (ListViewItem.ListViewSubItem subItem in CaseLvItem.SubItems)
            {
                subItem.ForeColor = Color.Black;
            }
        }

        public bool IsNotExecuted()
        {
            return CaseLvItem.SubItems[1].Text != GetEnumDesc.GetEnumDescription(enumCaseStatus.Completed);
        }

        private Dictionary<String, String> DicFromEqualsFile(string equalsFilePath)
        {
            StreamReader sr = File.OpenText(equalsFilePath);

            var auxDict = new Dictionary<string, string>();
            string s = "";
            int auxCont = 2;
            while ((s = sr.ReadLine()) != null)
            {
                if (s[0] != '[')
                {
                    string[] wordValue = s.Split('=');

                    if (!auxDict.ContainsKey(wordValue[0]))
                    {
                        auxDict.Add(wordValue[0], wordValue[1]);
                    }
                    else
                    {
                        auxDict.Add(wordValue[0] + auxCont.ToString(), wordValue[1]);
                        auxCont += 1;
                    }
                }
            }
            sr.Close();
            return auxDict;
        }
    }
}

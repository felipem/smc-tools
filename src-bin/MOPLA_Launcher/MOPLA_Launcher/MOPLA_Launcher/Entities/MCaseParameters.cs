﻿using System.Collections.Generic;

namespace MOPLA_Launcher.Entities
{
    public class MCaseParameters
    {
        // General Properties
        public string Code { get; set; }
        public string Description { get; set; }
        public int SubdivisionYLast { get; set; }
        public string MasterCode { get; set; }
        public string CoplaCode { get; set; }
        public int Zoom { get; set; }
        public int ZoomMinX { get; set; }
        public int ZoomMinY { get; set; }
        public int ZoomMaxX { get; set; }
        public int ZoomMaxY { get; set; }
        public string LastModifiedDate { get; set; }
        public double Dt { get; set; }

        // Punctual Spectrum
        public float PspecCalcEn { get; set; }
        public int PspecType { get; set; }

        // Free Surface
        public float FreeSurfCalcEn { get; set; }

        // Case Files Paths
        public string PathIn0 { get; set; }

        // Mesh Parameters
        public int NumMesh { get; set; }
        public string MeshCode;
        public List<string> ListChildrenMeshCode = new List<string>();

        // Case Execution Status and Info
        public enumCaseStatus CaseStatus = enumCaseStatus.Pending;
        public enumCaseExecInfo CaseExecInfo = enumCaseExecInfo.NoInfo;

        // Case MasterInDatFile footer
        public List<string> LstStrMasterInDatFooter { get; set; }
    }
}

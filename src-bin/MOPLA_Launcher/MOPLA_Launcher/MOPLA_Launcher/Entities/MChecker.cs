﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MOPLA_Launcher.Entities
{
    class MChecker
    {
        private readonly string _moplaCaseFolder;

        public MChecker(string moplaCaseFolder)
        {
            _moplaCaseFolder = moplaCaseFolder;
        }


        public bool CheckMeshFilesExists(string meshCode)
        {
            // Mesh files to check
            string ref2FilePath = Path.Combine(_moplaCaseFolder, meshCode + "REF2.DAT");
            string ref3FilePath = Path.Combine(_moplaCaseFolder, meshCode + "REF3.DAT");
            string batInpFilePath = Path.Combine(_moplaCaseFolder, "SP", meshCode + "_Bathymetry_Inp.grd");

            // Check mesh files existance
            if (!File.Exists(ref2FilePath) | !File.Exists(ref3FilePath) | !File.Exists(batInpFilePath))
            {
                return false;
            }

            return true;
        }

        public bool CheckCaseFilesExists(string caseCode)
        {
            // Case files to check
            string encFilePath = Path.Combine(_moplaCaseFolder, caseCode + "ENC.DAT");
            string in2FilePath = Path.Combine(_moplaCaseFolder, caseCode + "IN2.DAT");

            // Check case files existance
            if (!File.Exists(encFilePath) | !File.Exists(in2FilePath))
            {
                return false;
            }

            return true;
        }

        public enumCaseStatus checkMoplaCaseIsSolved(string caseLogPath)
        {
            //System.Windows.Forms.MessageBox.Show(caseLogPath);
            //TODO: PLANTEARSE EN VEZ DE TRUE OR FALSE, RETURN EL ESTADO DEL CASO LEYENDO LA ULTIMA LINEA DEL LOG
            // Check that LogFile exists
            if (!File.Exists(caseLogPath))
            {
                return enumCaseStatus.Pending;
            }

            // Get last line from LogFile
            //var lines = File.ReadLines(caseLogPath).Where(line => line != "");
            StreamReader sr = new StreamReader(caseLogPath);
            
            // Check that LogFile is not empty
            try
            {
                string[] lines=sr.ReadToEnd().Split(new string[]{System.Environment.NewLine},System.StringSplitOptions.RemoveEmptyEntries);
                sr.Close();
                if (!lines.Any())
                {
                    return enumCaseStatus.Error;
                }
                string lastLine = lines.Last();
                // Check last line info
                if (lastLine.Equals("-- NUMERICAL SIMULATION FINISHED WITHOUT PROBLEMS. --"))
                {
                    return enumCaseStatus.Completed;
                }
                else if (lastLine.Equals("-- NUMERICAL SIMULATION CANCELLED BY USER. --"))
                {
                    return enumCaseStatus.Cancelled;
                }
                else
                {
                    return enumCaseStatus.Error;
                }
            }
            catch (System.Exception ex)
            {
                sr.Close();
                System.Windows.Forms.MessageBox.Show(ex.Message);
                File.Delete(caseLogPath);
                return enumCaseStatus.Error;
            }
            
        }


        public enumCaseExecInfo GetOlucaSPError(string caseLogPath)
        {
            const string olucaNotInit = "-- OLUCASP could not start. --";
            const string olucaFailed = "-- OLUCASP execution failed. --";

            // Check that LogFile exists
            if (!File.Exists(caseLogPath))
            {
                return enumCaseExecInfo.NoInfo;
            }

            // Get last line from LogFile
            var lines = File.ReadLines(caseLogPath).Where(line => line != "");
            
            // Check that LogFile is not empty
            if (!lines.Any())
            {
                return enumCaseExecInfo.NoInfo;
            }

            string lastLine = lines.Last();

            // Search for int numbers
            Regex regex = new Regex(@"\d+");
            var listMatch = (from Match match in regex.Matches(lastLine) select match.Value).ToList();

            // Check last line info
            if (lastLine.Equals(olucaNotInit)) // OlucaSP did not start
            {
                return enumCaseExecInfo.ErrorOlucaNotInit;
            }
            if (lastLine.Equals(olucaFailed)) // OlucaSP did not start
            {
                return enumCaseExecInfo.ErrorOlucaFail;
            }

            if (listMatch.Count == 2)
            {
                if (!listMatch[0].Equals(listMatch[1])) // OlucaSP not complete
                {
                    return enumCaseExecInfo.ErrorOlucaFail;
                }
            }

            // If status can't be found
            return enumCaseExecInfo.NoInfo;
        }
    }
}

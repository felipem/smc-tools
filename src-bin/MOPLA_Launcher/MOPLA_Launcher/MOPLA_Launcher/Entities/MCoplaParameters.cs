﻿
namespace MOPLA_Launcher.Entities
{
    public class MCoplaParameters
    {
        // Copla execution 
        public bool ExecuteCopla = false;

        // General
        public string Code { get; set; }
        public string Desc { get; set; }
        public float Interval { get; set; }
        public string FileWritings { get; set; }
        public int Iterations { get; set; }
        public string WriteBeginning { get; set; }
        public string LastModifiedDate { get; set; }

        // Parameters
        public float Chezy { get; set; }
        public float Eddy { get; set; }
        public string Coriolis { get; set; }
        public string NumNonLinearTerms { get; set; }
        public string NonLinearity { get; set; }
        public string Flooding { get; set; }
        public string BoundaryFriction { get; set; }
        public float TotalTime { get; set; }
        public float Tide { get; set; } // Duplicated Parameter: same value as MSpectrum.Tide
    }
}

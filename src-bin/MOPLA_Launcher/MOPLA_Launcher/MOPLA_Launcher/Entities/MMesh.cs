﻿using System;
using System.IO;
using System.Linq;

namespace MOPLA_Launcher.Entities
{
    public class MMesh
    {
        // Properties 
        public string LastModifiedDate;
        public string MeshCode;
        public string MeshDescription;
        public double XOrigin;
        public double YOrigin;
        public float Angle;
        public float XDimension;
        public float YDimension;
        public int XDivision;
        public int YDivision;
        public float XSpacing;
        public float YSpacing;


        public float UnknowPar1;
        public int UnknowPar2;
        public int UnknowPar3;

        // Parent Mesh info
        public bool meshHasParent = false;
        public string ParentMeshCode;

        // Children Mesh info
        public bool meshHasChildren = false;
        public int ChildrenNodeInit;
        public int ChildrenNodeEnd;
        public string ChildrenMeshCode;
        public int SubdivisionChildren = 1;
        
        // Last Children Mesh info
        public int XDivisionLastChildren;
        public int YDivisionLastChildren;

        // Constructor
        public MMesh(FileInfo ref2FileI)
        {
            var pathRef2 = ref2FileI.FullName;
            var pathRef3 = pathRef2.Replace("REF2.DAT", "REF3.DAT");

            readRef2File(pathRef2);
            readRef3File(pathRef3);
        }

        private void readRef2File(string pathRef2)
        {
            StreamReader sr = File.OpenText(pathRef2);

            MeshCode = sr.ReadLine();
            MeshDescription = sr.ReadLine();

            var auxStr = sr.ReadLine().Split(' ');
            auxStr = auxStr.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            XOrigin = Convert.ToDouble(auxStr[0]);
            YOrigin = Convert.ToDouble(auxStr[1]);

            Angle = Convert.ToSingle(sr.ReadLine());

            auxStr = sr.ReadLine().Split(' ');
            auxStr = auxStr.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            XDimension = Convert.ToSingle(auxStr[0]);
            YDimension = Convert.ToSingle(auxStr[1]);

            auxStr = sr.ReadLine().Split(' ');
            XDivision = Convert.ToInt32(auxStr[0]);
            YDivision = Convert.ToInt32(auxStr[1]);

            auxStr = sr.ReadLine().Split(' ');
            auxStr = auxStr.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            XSpacing = Convert.ToSingle(auxStr[0]);
            YSpacing = Convert.ToSingle(auxStr[1]);
            UnknowPar1 = Convert.ToSingle(auxStr[2]);

            UnknowPar2 = Convert.ToInt32(sr.ReadLine());
            UnknowPar3 = Convert.ToInt32(sr.ReadLine());

            // Parent mesh info line
            auxStr = sr.ReadLine().Split(new string[] { }, StringSplitOptions.RemoveEmptyEntries);
            int auxInt = Convert.ToInt32(auxStr[0]);
            if (auxInt == 1)
            {
                meshHasParent = true;
                ParentMeshCode = auxStr[1];
            }

            // Children mesh info line
            var auxLastLine = sr.ReadLine();
            if (auxLastLine != null)
            {
                auxStr = auxLastLine.Split(new string[] { }, StringSplitOptions.RemoveEmptyEntries);
                ChildrenNodeInit = Convert.ToInt32(auxStr[0]);
                ChildrenNodeEnd = Convert.ToInt32(auxStr[1]);
                ChildrenMeshCode = auxStr[2];
                meshHasChildren = true;
            }
            else
            {
                XDivisionLastChildren = XDivision;
                YDivisionLastChildren = YDivision;
            }
        }

        private void readRef3File(string pathRef3)
        {
            StreamReader sr = File.OpenText(pathRef3);
            sr.ReadLine();
            sr.ReadLine();
            LastModifiedDate = sr.ReadLine().Split('=')[1];
        }
    }
}

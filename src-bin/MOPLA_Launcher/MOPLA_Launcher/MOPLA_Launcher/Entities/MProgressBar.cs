﻿using System;
using System.Windows.Forms;
using System.ComponentModel;

namespace MOPLA_Launcher.Entities
{
    public class MProgressBar
    {
        private ToolStripProgressBar _progressBar;
        public BackgroundWorker backgroundWorker;

        private static int _nMaxTask;
        private static volatile int _nCompletedHalfTask;

        public MProgressBar(ToolStripProgressBar progressBar)
        {
            _progressBar = progressBar;

            backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _progressBar.Value = e.ProgressPercentage;
        }

        public void ResetParameters()
        {
            _nCompletedHalfTask = 0;
        }

        public void SetNumCases(int nMaxTask)
        {
            _nMaxTask = nMaxTask;
        }

        public void AddHalfTask()
        {
            _nCompletedHalfTask += 1;
            backgroundWorker.ReportProgress(Convert.ToInt32((_nCompletedHalfTask * 0.5 / _nMaxTask * 1.0) * 100));
        }

        public void AllTasksFinished()
        {
            backgroundWorker.ReportProgress(100);
        }

        public void EmptyProgressBar()
        {
            backgroundWorker.ReportProgress(0);
        }
    }
}

﻿
namespace MOPLA_Launcher.Entities
{
    public class MSpectrum
    {
        // General
        public string Desc { get; set; }
        public string CaseType { get; set; }
        public string LastModifiedDate { get; set; }

        // Parameters
        public bool ContoursOpen { get; set; }
        public BottomDissipationModel BottomDissipationModel { get; set; }
        public enumModelLinearity Linearity { get; set; }
        public float Period { get; set; }
        public float Tide { get; set; }
        public float Amplitude { get; set; }
        public float Direction { get; set; }
        public float MaxSlope { get; set; }
        public string FilterType { get; set; }

        // Spectrum Components
        public int SpecPropagate { get; set; }
        public int SpecFrequency { get; set; }
        public int SpecDirection { get; set; }

        // Spectrum
        public enumSpecTypes SpecType { get; set; }
        public enumSpecUnits SpecUnits { get; set; }
        public enumBreakingDissipationModel BreakingDissipationModel { get; set; }
        public float MeanDirection { get; set; }
        public float DirSpecDispersion { get; set; }
        public int DirComponentsNum { get; set; }

        // TMA Spectrum
        public float TmaSpecDepth { get; set; }
        public float TmaSpecPeakFrequency { get; set; }
        public float TmaSpecMaxFrequency { get; set; }
        public float TmaSpecHs { get; set; }
        public float TmaSpecGamma { get; set; }
        public int TmaSpecComponentsNum { get; set; }

        // Components
        public double[] PeriodComps { get; set; }
        public double[] DirComps { get; set; }
        public double IntensityComp { get; set; }

    }

    public class BottomDissipationModel
    {
        public bool isPorous = false;
        public bool isTurbulent = false;
        public bool isLaminar = false;
    }
}

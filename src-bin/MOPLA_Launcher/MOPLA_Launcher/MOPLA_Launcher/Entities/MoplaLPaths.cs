﻿using System.IO;

namespace MOPLA_Launcher.Entities
{
    class MoplaLPaths
    {
        private readonly string _moplaLauncherFolder;
        private readonly string _coplaExecPath;
        private readonly string _olucaExecPath;
        private readonly string _specwinExecPath;


        public MoplaLPaths(string moplaLauncherFolder)
        {
            _moplaLauncherFolder = moplaLauncherFolder;

            // ML Numerical model paths
            _coplaExecPath = Path.Combine(_moplaLauncherFolder, "NumModExec", "coplasp3_1000_grd.exe");
            _olucaExecPath = Path.Combine(_moplaLauncherFolder, "NumModExec", "olucasp_1000_grd.exe");
            _specwinExecPath = Path.Combine(_moplaLauncherFolder, "NumModExec", "specwin2_0.exe");
        }

        public string GetCoplaExecPath()
        {
            return _coplaExecPath;
        }
        public string GetOlucaExecPath()
        {
            return _olucaExecPath;
        }
        public string GetSpecWinExecPath()
        {
            return _specwinExecPath;
        }
    }
}

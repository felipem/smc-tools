﻿using System;
using System.IO;

namespace MOPLA_Launcher.Entities
{
    class MoplaLog
    {
        private readonly string _moplaCaseFolder;
        private string _mainLogFilePath;
        private StreamWriter _fileLogW;

        public MoplaLog(string moplaCaseFolder)
        {
            _moplaCaseFolder = moplaCaseFolder;

            InitializeLog();
        }

        public void InitializeLog()
        {
            _mainLogFilePath = Path.Combine(_moplaCaseFolder, "_MoplaLauncher.log");

            if (File.Exists(_mainLogFilePath))
            {
                File.Delete(_mainLogFilePath);
            }

            _fileLogW = new StreamWriter(_mainLogFilePath, true);

            // Write Header
            _fileLogW.WriteLine("--------------------------------------------------------------");
            _fileLogW.WriteLine("MOPLA LAUNCHER LOG FILE{0}", Environment.NewLine);
            _fileLogW.WriteLine("Folder: {0}{1}", _moplaCaseFolder, Environment.NewLine);
            _fileLogW.WriteLine("Date of creation: {0}", DateTime.Now);
            _fileLogW.WriteLine("--------------------------------------------------------------");
            _fileLogW.Close();
        }

        public void AddMesh(string meshCode)
        {
            File.AppendAllText(_mainLogFilePath, string.Format(" {0}\t Mesh correctly loaded.{1}", meshCode, Environment.NewLine));
        }

        public void AddMeshLoadingError(string meshCode)
        {
            File.AppendAllText(_mainLogFilePath, string.Format(" {0}\t Error: Unable to read mesh files.{1}", meshCode, Environment.NewLine));
        }

        public void AddMeshMissingFilesError(string meshCode)
        {
            File.AppendAllText(_mainLogFilePath, string.Format(" {0}\t Error: Mesh files missing.{1}", meshCode, Environment.NewLine));
        }

        public void StartLoadingMeshes()
        {
            File.AppendAllText(_mainLogFilePath, string.Format("{0}{0}LOADING MOPLA MESHES.{0}{0}", Environment.NewLine));
        }

        public void NoMeshesOnFolder()
        {
            File.AppendAllText(_mainLogFilePath, string.Format("{0}{0}NO MOPLA MESHES WERE FOUND.{0}{0}", Environment.NewLine));
        }

        public void StartLoadingCases()
        {
            File.AppendAllText(_mainLogFilePath, string.Format("{0}{0}LOADING MOPLA CASES.{0}{0}", Environment.NewLine));
        }

        public void NoCasesOnFolder()
        {
            File.AppendAllText(_mainLogFilePath, string.Format("{0}{0}NO MOPLA CASES WERE FOUND.{0}{0}", Environment.NewLine));
        }

        public void AddCase(string caseCode)
        {
            File.AppendAllText(_mainLogFilePath, string.Format(" {0}\t Case correctly loaded.{1}", caseCode, Environment.NewLine));
        }

        public void AddCaseMissingMeshError(string caseCode)
        {
            File.AppendAllText(_mainLogFilePath, string.Format(" {0}\t Error: Some case meshes are missing.{1}", caseCode, Environment.NewLine));
        }

        public void AddCaseMissingFilesError(string caseCode)
        {
            File.AppendAllText(_mainLogFilePath, string.Format(" {0}\t Error: Case files missing.{1}", caseCode, Environment.NewLine));
        }

        public void AddCaseLoadingError(string caseCode)
        {
            File.AppendAllText(_mainLogFilePath, string.Format(" {0}\t Error: Unable to read case files.{1}", caseCode, Environment.NewLine));
        }
    }
}

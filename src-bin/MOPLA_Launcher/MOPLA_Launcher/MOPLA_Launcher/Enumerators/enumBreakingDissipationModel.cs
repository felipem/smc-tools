namespace MOPLA_Launcher.Entities
{
    public enum enumBreakingDissipationModel 
    {
        Thornton = 0,
        Battjes = 1,
        Winyu = 2,
    }
}
using System.ComponentModel;

namespace MOPLA_Launcher.Entities
{
    public enum enumCaseExecInfo
    {
        [Description("")]
        NoInfo,

        [Description("in0.dat file created.")]
        In0Created,

        [Description("Mopla case execution finished.")]
        SimulationFinished,

        [Description("Executing OlucaSp model...")]
        OlucaInProcess,

        [Description("OlucaSP not initialized.")]
        ErrorOlucaNotInit,

        [Description("OlucaSp model failed.")]
        ErrorOlucaFail,

        [Description("Executing CoplaSp model...")]
        CoplaInProcess,
    }
}
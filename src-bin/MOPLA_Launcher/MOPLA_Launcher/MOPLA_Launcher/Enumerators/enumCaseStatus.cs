namespace MOPLA_Launcher.Entities
{
    public enum enumCaseStatus
    {
        Pending,
        Processing,
        Completed,
        Error,
        Cancelled,
    }
}
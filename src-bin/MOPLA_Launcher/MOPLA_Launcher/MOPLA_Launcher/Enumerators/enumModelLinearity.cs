namespace MOPLA_Launcher.Entities
{
    public enum enumModelLinearity
    {
        Linear = 0,
        Composed = 1,
        Stokes = 2,
    }
}
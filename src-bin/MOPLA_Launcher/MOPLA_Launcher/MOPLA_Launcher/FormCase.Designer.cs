﻿namespace MOPLA_Launcher
{
    partial class FormCase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tBCaseCode = new System.Windows.Forms.TextBox();
            this.tBCaseDesc = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tBMeshYcol = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tBMeshXrows = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tBMeshAngle = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tBMeshYdiv = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tBMeshXdiv = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tBMeshYorig = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tBMeshXorig = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tBMeshCode = new System.Windows.Forms.TextBox();
            this.tBMeshDesc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.tBTMASpecNcom = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tBTMASpecGamma = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tBTMASpecMaxFreq = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tBTMASpecPeakFreq = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tBTMASpecHs = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tBTMASpecDepth = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tBMeanDir = new System.Windows.Forms.TextBox();
            this.tBNCom = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tBDisPar = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.rBModelStokes = new System.Windows.Forms.RadioButton();
            this.rBModelComposed = new System.Windows.Forms.RadioButton();
            this.rBModelLinear = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tBMeshYdim = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tBMeshXdim = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.rBSpectrumUnitsCGS = new System.Windows.Forms.RadioButton();
            this.rBSpectrumUnitsMKS = new System.Windows.Forms.RadioButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.rBSpectrumTypeREAD = new System.Windows.Forms.RadioButton();
            this.rBSpectrumTypeTMA = new System.Windows.Forms.RadioButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tBTidalRange = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.tBYSubDiv = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.cBopenLatBound = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cBbotDisLaminar = new System.Windows.Forms.CheckBox();
            this.cBbotDisPorous = new System.Windows.Forms.CheckBox();
            this.cBbotDisTurbulent = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rBbreakDisWinyu = new System.Windows.Forms.RadioButton();
            this.rBbreakDisBattjes = new System.Windows.Forms.RadioButton();
            this.rBbreakDisThornton = new System.Windows.Forms.RadioButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gBCopPhysPar = new System.Windows.Forms.GroupBox();
            this.btnCopEddyHelp = new System.Windows.Forms.Button();
            this.tBCopEddy = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnCopChezyHelp = new System.Windows.Forms.Button();
            this.tBCopChezy = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.gBCopTimePar = new System.Windows.Forms.GroupBox();
            this.btnCopTimeIntervHelp = new System.Windows.Forms.Button();
            this.tBCopTotalTime = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tBCopNumIter = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tBCopTimeInterval = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.cBCoplaCurrents = new System.Windows.Forms.CheckBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveCase = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.gBCopPhysPar.SuspendLayout();
            this.gBCopTimePar.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(131, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description";
            // 
            // tBCaseCode
            // 
            this.tBCaseCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBCaseCode.Location = new System.Drawing.Point(9, 32);
            this.tBCaseCode.Name = "tBCaseCode";
            this.tBCaseCode.ReadOnly = true;
            this.tBCaseCode.Size = new System.Drawing.Size(100, 20);
            this.tBCaseCode.TabIndex = 2;
            // 
            // tBCaseDesc
            // 
            this.tBCaseDesc.Location = new System.Drawing.Point(134, 32);
            this.tBCaseDesc.Name = "tBCaseDesc";
            this.tBCaseDesc.ReadOnly = true;
            this.tBCaseDesc.Size = new System.Drawing.Size(320, 20);
            this.tBCaseDesc.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tBCaseCode);
            this.groupBox1.Controls.Add(this.tBCaseDesc);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(460, 66);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Parameters";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tBMeshYcol);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.tBMeshXrows);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(229, 149);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(217, 91);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Spacing";
            // 
            // tBMeshYcol
            // 
            this.tBMeshYcol.Location = new System.Drawing.Point(107, 55);
            this.tBMeshYcol.Name = "tBMeshYcol";
            this.tBMeshYcol.ReadOnly = true;
            this.tBMeshYcol.Size = new System.Drawing.Size(90, 20);
            this.tBMeshYcol.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Y columns";
            // 
            // tBMeshXrows
            // 
            this.tBMeshXrows.Location = new System.Drawing.Point(107, 29);
            this.tBMeshXrows.Name = "tBMeshXrows";
            this.tBMeshXrows.ReadOnly = true;
            this.tBMeshXrows.Size = new System.Drawing.Size(90, 20);
            this.tBMeshXrows.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "X rows";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tBMeshAngle);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Location = new System.Drawing.Point(6, 246);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(217, 91);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Course";
            // 
            // tBMeshAngle
            // 
            this.tBMeshAngle.Location = new System.Drawing.Point(106, 39);
            this.tBMeshAngle.Name = "tBMeshAngle";
            this.tBMeshAngle.ReadOnly = true;
            this.tBMeshAngle.Size = new System.Drawing.Size(90, 20);
            this.tBMeshAngle.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(68, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Angle";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tBMeshYdiv);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.tBMeshXdiv);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Location = new System.Drawing.Point(6, 149);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(217, 91);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Divisions";
            // 
            // tBMeshYdiv
            // 
            this.tBMeshYdiv.Location = new System.Drawing.Point(107, 55);
            this.tBMeshYdiv.Name = "tBMeshYdiv";
            this.tBMeshYdiv.ReadOnly = true;
            this.tBMeshYdiv.Size = new System.Drawing.Size(90, 20);
            this.tBMeshYdiv.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(28, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Nº Y columns";
            // 
            // tBMeshXdiv
            // 
            this.tBMeshXdiv.Location = new System.Drawing.Point(107, 29);
            this.tBMeshXdiv.Name = "tBMeshXdiv";
            this.tBMeshXdiv.ReadOnly = true;
            this.tBMeshXdiv.Size = new System.Drawing.Size(90, 20);
            this.tBMeshXdiv.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Nº X rows";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tBMeshYorig);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.tBMeshXorig);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(6, 52);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(217, 91);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Origin";
            // 
            // tBMeshYorig
            // 
            this.tBMeshYorig.Location = new System.Drawing.Point(106, 55);
            this.tBMeshYorig.Name = "tBMeshYorig";
            this.tBMeshYorig.ReadOnly = true;
            this.tBMeshYorig.Size = new System.Drawing.Size(90, 20);
            this.tBMeshYorig.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Y";
            // 
            // tBMeshXorig
            // 
            this.tBMeshXorig.Location = new System.Drawing.Point(106, 29);
            this.tBMeshXorig.Name = "tBMeshXorig";
            this.tBMeshXorig.ReadOnly = true;
            this.tBMeshXorig.Size = new System.Drawing.Size(90, 20);
            this.tBMeshXorig.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "X";
            // 
            // tBMeshCode
            // 
            this.tBMeshCode.Location = new System.Drawing.Point(5, 22);
            this.tBMeshCode.Name = "tBMeshCode";
            this.tBMeshCode.ReadOnly = true;
            this.tBMeshCode.Size = new System.Drawing.Size(100, 20);
            this.tBMeshCode.TabIndex = 2;
            // 
            // tBMeshDesc
            // 
            this.tBMeshDesc.Location = new System.Drawing.Point(130, 22);
            this.tBMeshDesc.Name = "tBMeshDesc";
            this.tBMeshDesc.ReadOnly = true;
            this.tBMeshDesc.Size = new System.Drawing.Size(320, 20);
            this.tBMeshDesc.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Code";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Description";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.tBTMASpecNcom);
            this.groupBox13.Controls.Add(this.label17);
            this.groupBox13.Controls.Add(this.tBTMASpecGamma);
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.tBTMASpecMaxFreq);
            this.groupBox13.Controls.Add(this.label15);
            this.groupBox13.Controls.Add(this.tBTMASpecPeakFreq);
            this.groupBox13.Controls.Add(this.label13);
            this.groupBox13.Controls.Add(this.tBTMASpecHs);
            this.groupBox13.Controls.Add(this.label21);
            this.groupBox13.Controls.Add(this.tBTMASpecDepth);
            this.groupBox13.Controls.Add(this.label22);
            this.groupBox13.Location = new System.Drawing.Point(6, 111);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(440, 117);
            this.groupBox13.TabIndex = 4;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "TMA Spectrum Parameters";
            // 
            // tBTMASpecNcom
            // 
            this.tBTMASpecNcom.Location = new System.Drawing.Point(346, 78);
            this.tBTMASpecNcom.Name = "tBTMASpecNcom";
            this.tBTMASpecNcom.ReadOnly = true;
            this.tBTMASpecNcom.Size = new System.Drawing.Size(88, 20);
            this.tBTMASpecNcom.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(229, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "N. Comp.";
            // 
            // tBTMASpecGamma
            // 
            this.tBTMASpecGamma.Location = new System.Drawing.Point(346, 52);
            this.tBTMASpecGamma.Name = "tBTMASpecGamma";
            this.tBTMASpecGamma.Size = new System.Drawing.Size(88, 20);
            this.tBTMASpecGamma.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(229, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "Gamma";
            // 
            // tBTMASpecMaxFreq
            // 
            this.tBTMASpecMaxFreq.Location = new System.Drawing.Point(346, 26);
            this.tBTMASpecMaxFreq.Name = "tBTMASpecMaxFreq";
            this.tBTMASpecMaxFreq.ReadOnly = true;
            this.tBTMASpecMaxFreq.Size = new System.Drawing.Size(88, 20);
            this.tBTMASpecMaxFreq.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(229, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Max Freq. (Hz)";
            // 
            // tBTMASpecPeakFreq
            // 
            this.tBTMASpecPeakFreq.Location = new System.Drawing.Point(123, 78);
            this.tBTMASpecPeakFreq.Name = "tBTMASpecPeakFreq";
            this.tBTMASpecPeakFreq.Size = new System.Drawing.Size(88, 20);
            this.tBTMASpecPeakFreq.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 81);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Peak Freq. (Hz)";
            // 
            // tBTMASpecHs
            // 
            this.tBTMASpecHs.Location = new System.Drawing.Point(123, 52);
            this.tBTMASpecHs.Name = "tBTMASpecHs";
            this.tBTMASpecHs.Size = new System.Drawing.Size(88, 20);
            this.tBTMASpecHs.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 55);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(37, 13);
            this.label21.TabIndex = 5;
            this.label21.Text = "Hs (m)";
            // 
            // tBTMASpecDepth
            // 
            this.tBTMASpecDepth.Location = new System.Drawing.Point(123, 26);
            this.tBTMASpecDepth.Name = "tBTMASpecDepth";
            this.tBTMASpecDepth.ReadOnly = true;
            this.tBTMASpecDepth.Size = new System.Drawing.Size(88, 20);
            this.tBTMASpecDepth.TabIndex = 4;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 29);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Depth (m)";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.tBMeanDir);
            this.groupBox9.Controls.Add(this.tBNCom);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.tBDisPar);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Location = new System.Drawing.Point(6, 231);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(440, 103);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Directional Spectrum";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Mean Direction (º)";
            // 
            // tBMeanDir
            // 
            this.tBMeanDir.Location = new System.Drawing.Point(123, 19);
            this.tBMeanDir.Name = "tBMeanDir";
            this.tBMeanDir.ReadOnly = true;
            this.tBMeanDir.Size = new System.Drawing.Size(88, 20);
            this.tBMeanDir.TabIndex = 8;
            // 
            // tBNCom
            // 
            this.tBNCom.Location = new System.Drawing.Point(123, 71);
            this.tBNCom.Name = "tBNCom";
            this.tBNCom.ReadOnly = true;
            this.tBNCom.Size = new System.Drawing.Size(88, 20);
            this.tBNCom.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "N. Comp.";
            // 
            // tBDisPar
            // 
            this.tBDisPar.Location = new System.Drawing.Point(123, 45);
            this.tBDisPar.Name = "tBDisPar";
            this.tBDisPar.Size = new System.Drawing.Size(88, 20);
            this.tBDisPar.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 48);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "Dispersion Par. (º)";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.rBModelStokes);
            this.groupBox11.Controls.Add(this.rBModelComposed);
            this.groupBox11.Controls.Add(this.rBModelLinear);
            this.groupBox11.Location = new System.Drawing.Point(6, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(440, 50);
            this.groupBox11.TabIndex = 6;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Type";
            // 
            // rBModelStokes
            // 
            this.rBModelStokes.AutoSize = true;
            this.rBModelStokes.Location = new System.Drawing.Point(337, 19);
            this.rBModelStokes.Name = "rBModelStokes";
            this.rBModelStokes.Size = new System.Drawing.Size(58, 17);
            this.rBModelStokes.TabIndex = 2;
            this.rBModelStokes.Text = "Stokes";
            this.rBModelStokes.UseVisualStyleBackColor = true;
            // 
            // rBModelComposed
            // 
            this.rBModelComposed.AutoSize = true;
            this.rBModelComposed.Location = new System.Drawing.Point(191, 19);
            this.rBModelComposed.Name = "rBModelComposed";
            this.rBModelComposed.Size = new System.Drawing.Size(75, 17);
            this.rBModelComposed.TabIndex = 1;
            this.rBModelComposed.Text = "Composed";
            this.rBModelComposed.UseVisualStyleBackColor = true;
            // 
            // rBModelLinear
            // 
            this.rBModelLinear.AutoSize = true;
            this.rBModelLinear.Location = new System.Drawing.Point(45, 19);
            this.rBModelLinear.Name = "rBModelLinear";
            this.rBModelLinear.Size = new System.Drawing.Size(54, 17);
            this.rBModelLinear.TabIndex = 0;
            this.rBModelLinear.Text = "Linear";
            this.rBModelLinear.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 84);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(460, 369);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.tBMeshDesc);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.tBMeshCode);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(452, 343);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Mesh";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tBMeshYdim);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.tBMeshXdim);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(229, 52);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(217, 91);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dimensions";
            // 
            // tBMeshYdim
            // 
            this.tBMeshYdim.Location = new System.Drawing.Point(106, 55);
            this.tBMeshYdim.Name = "tBMeshYdim";
            this.tBMeshYdim.ReadOnly = true;
            this.tBMeshYdim.Size = new System.Drawing.Size(90, 20);
            this.tBMeshYdim.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(68, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Y";
            // 
            // tBMeshXdim
            // 
            this.tBMeshXdim.Location = new System.Drawing.Point(106, 29);
            this.tBMeshXdim.Name = "tBMeshXdim";
            this.tBMeshXdim.ReadOnly = true;
            this.tBMeshXdim.Size = new System.Drawing.Size(90, 20);
            this.tBMeshXdim.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(68, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "X";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox16);
            this.tabPage2.Controls.Add(this.groupBox15);
            this.tabPage2.Controls.Add(this.groupBox13);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(452, 343);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Spectrum";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.rBSpectrumUnitsCGS);
            this.groupBox16.Controls.Add(this.rBSpectrumUnitsMKS);
            this.groupBox16.Location = new System.Drawing.Point(6, 55);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(440, 50);
            this.groupBox16.TabIndex = 8;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Units";
            // 
            // rBSpectrumUnitsCGS
            // 
            this.rBSpectrumUnitsCGS.AutoCheck = false;
            this.rBSpectrumUnitsCGS.AutoSize = true;
            this.rBSpectrumUnitsCGS.Location = new System.Drawing.Point(265, 19);
            this.rBSpectrumUnitsCGS.Name = "rBSpectrumUnitsCGS";
            this.rBSpectrumUnitsCGS.Size = new System.Drawing.Size(93, 17);
            this.rBSpectrumUnitsCGS.TabIndex = 1;
            this.rBSpectrumUnitsCGS.TabStop = true;
            this.rBSpectrumUnitsCGS.Text = "C.G.S. (cm²  s)";
            this.rBSpectrumUnitsCGS.UseVisualStyleBackColor = true;
            // 
            // rBSpectrumUnitsMKS
            // 
            this.rBSpectrumUnitsMKS.AutoCheck = false;
            this.rBSpectrumUnitsMKS.AutoSize = true;
            this.rBSpectrumUnitsMKS.Location = new System.Drawing.Point(45, 19);
            this.rBSpectrumUnitsMKS.Name = "rBSpectrumUnitsMKS";
            this.rBSpectrumUnitsMKS.Size = new System.Drawing.Size(88, 17);
            this.rBSpectrumUnitsMKS.TabIndex = 0;
            this.rBSpectrumUnitsMKS.TabStop = true;
            this.rBSpectrumUnitsMKS.Text = "M.K.S. (m²  s)";
            this.rBSpectrumUnitsMKS.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.rBSpectrumTypeREAD);
            this.groupBox15.Controls.Add(this.rBSpectrumTypeTMA);
            this.groupBox15.Location = new System.Drawing.Point(6, 6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(440, 50);
            this.groupBox15.TabIndex = 7;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Type";
            // 
            // rBSpectrumTypeREAD
            // 
            this.rBSpectrumTypeREAD.AutoCheck = false;
            this.rBSpectrumTypeREAD.AutoSize = true;
            this.rBSpectrumTypeREAD.Location = new System.Drawing.Point(265, 19);
            this.rBSpectrumTypeREAD.Name = "rBSpectrumTypeREAD";
            this.rBSpectrumTypeREAD.Size = new System.Drawing.Size(108, 17);
            this.rBSpectrumTypeREAD.TabIndex = 1;
            this.rBSpectrumTypeREAD.TabStop = true;
            this.rBSpectrumTypeREAD.Text = "Spectrum reading";
            this.rBSpectrumTypeREAD.UseVisualStyleBackColor = true;
            // 
            // rBSpectrumTypeTMA
            // 
            this.rBSpectrumTypeTMA.AutoCheck = false;
            this.rBSpectrumTypeTMA.AutoSize = true;
            this.rBSpectrumTypeTMA.Location = new System.Drawing.Point(45, 19);
            this.rBSpectrumTypeTMA.Name = "rBSpectrumTypeTMA";
            this.rBSpectrumTypeTMA.Size = new System.Drawing.Size(96, 17);
            this.rBSpectrumTypeTMA.TabIndex = 0;
            this.rBSpectrumTypeTMA.TabStop = true;
            this.rBSpectrumTypeTMA.Text = "TMA Spectrum";
            this.rBSpectrumTypeTMA.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox14);
            this.tabPage3.Controls.Add(this.groupBox12);
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(452, 343);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Model";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tBTidalRange);
            this.groupBox14.Controls.Add(this.label25);
            this.groupBox14.Location = new System.Drawing.Point(6, 55);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(440, 50);
            this.groupBox14.TabIndex = 11;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Tidal Range";
            // 
            // tBTidalRange
            // 
            this.tBTidalRange.Location = new System.Drawing.Point(208, 19);
            this.tBTidalRange.Name = "tBTidalRange";
            this.tBTidalRange.ReadOnly = true;
            this.tBTidalRange.Size = new System.Drawing.Size(100, 20);
            this.tBTidalRange.TabIndex = 6;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(20, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(82, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "Tidal Range (m)";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.tBYSubDiv);
            this.groupBox12.Controls.Add(this.label23);
            this.groupBox12.Location = new System.Drawing.Point(6, 287);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(440, 50);
            this.groupBox12.TabIndex = 10;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Y Subdivision (simple or detail mesh)";
            // 
            // tBYSubDiv
            // 
            this.tBYSubDiv.Location = new System.Drawing.Point(208, 19);
            this.tBYSubDiv.Name = "tBYSubDiv";
            this.tBYSubDiv.ReadOnly = true;
            this.tBYSubDiv.Size = new System.Drawing.Size(100, 20);
            this.tBYSubDiv.TabIndex = 6;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(20, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(133, 13);
            this.label23.TabIndex = 5;
            this.label23.Text = "Y subdivisions at last mesh";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.cBopenLatBound);
            this.groupBox10.Location = new System.Drawing.Point(6, 231);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(440, 50);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Boundaries";
            // 
            // cBopenLatBound
            // 
            this.cBopenLatBound.AutoSize = true;
            this.cBopenLatBound.Location = new System.Drawing.Point(23, 19);
            this.cBopenLatBound.Name = "cBopenLatBound";
            this.cBopenLatBound.Size = new System.Drawing.Size(143, 17);
            this.cBopenLatBound.TabIndex = 11;
            this.cBopenLatBound.Text = "Open Lateral Boundaries";
            this.cBopenLatBound.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cBbotDisLaminar);
            this.groupBox8.Controls.Add(this.cBbotDisPorous);
            this.groupBox8.Controls.Add(this.cBbotDisTurbulent);
            this.groupBox8.Location = new System.Drawing.Point(229, 111);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(217, 114);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Bottom Dissipation";
            // 
            // cBbotDisLaminar
            // 
            this.cBbotDisLaminar.AutoSize = true;
            this.cBbotDisLaminar.Location = new System.Drawing.Point(6, 91);
            this.cBbotDisLaminar.Name = "cBbotDisLaminar";
            this.cBbotDisLaminar.Size = new System.Drawing.Size(140, 17);
            this.cBbotDisLaminar.TabIndex = 12;
            this.cBbotDisLaminar.Text = "Laminar Boundary Layer";
            this.cBbotDisLaminar.UseVisualStyleBackColor = true;
            // 
            // cBbotDisPorous
            // 
            this.cBbotDisPorous.AutoSize = true;
            this.cBbotDisPorous.Location = new System.Drawing.Point(6, 19);
            this.cBbotDisPorous.Name = "cBbotDisPorous";
            this.cBbotDisPorous.Size = new System.Drawing.Size(95, 17);
            this.cBbotDisPorous.TabIndex = 10;
            this.cBbotDisPorous.Text = "Porous Bottom";
            this.cBbotDisPorous.UseVisualStyleBackColor = true;
            // 
            // cBbotDisTurbulent
            // 
            this.cBbotDisTurbulent.AutoSize = true;
            this.cBbotDisTurbulent.Location = new System.Drawing.Point(6, 55);
            this.cBbotDisTurbulent.Name = "cBbotDisTurbulent";
            this.cBbotDisTurbulent.Size = new System.Drawing.Size(148, 17);
            this.cBbotDisTurbulent.TabIndex = 11;
            this.cBbotDisTurbulent.Text = "Turbulent Boundary Layer";
            this.cBbotDisTurbulent.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rBbreakDisWinyu);
            this.groupBox2.Controls.Add(this.rBbreakDisBattjes);
            this.groupBox2.Controls.Add(this.rBbreakDisThornton);
            this.groupBox2.Location = new System.Drawing.Point(6, 111);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(217, 114);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Breaking Dissipation";
            // 
            // rBbreakDisWinyu
            // 
            this.rBbreakDisWinyu.AutoSize = true;
            this.rBbreakDisWinyu.Location = new System.Drawing.Point(6, 91);
            this.rBbreakDisWinyu.Name = "rBbreakDisWinyu";
            this.rBbreakDisWinyu.Size = new System.Drawing.Size(117, 17);
            this.rBbreakDisWinyu.TabIndex = 2;
            this.rBbreakDisWinyu.Text = "Winyu and Tomoya";
            this.rBbreakDisWinyu.UseVisualStyleBackColor = true;
            // 
            // rBbreakDisBattjes
            // 
            this.rBbreakDisBattjes.AutoSize = true;
            this.rBbreakDisBattjes.Location = new System.Drawing.Point(6, 55);
            this.rBbreakDisBattjes.Name = "rBbreakDisBattjes";
            this.rBbreakDisBattjes.Size = new System.Drawing.Size(120, 17);
            this.rBbreakDisBattjes.TabIndex = 1;
            this.rBbreakDisBattjes.Text = "Battjes and Janssen";
            this.rBbreakDisBattjes.UseVisualStyleBackColor = true;
            // 
            // rBbreakDisThornton
            // 
            this.rBbreakDisThornton.AutoSize = true;
            this.rBbreakDisThornton.Location = new System.Drawing.Point(6, 19);
            this.rBbreakDisThornton.Name = "rBbreakDisThornton";
            this.rBbreakDisThornton.Size = new System.Drawing.Size(117, 17);
            this.rBbreakDisThornton.TabIndex = 0;
            this.rBbreakDisThornton.Text = "Thornton and Guza";
            this.rBbreakDisThornton.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gBCopPhysPar);
            this.tabPage4.Controls.Add(this.gBCopTimePar);
            this.tabPage4.Controls.Add(this.cBCoplaCurrents);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(452, 343);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Copla";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gBCopPhysPar
            // 
            this.gBCopPhysPar.Controls.Add(this.btnCopEddyHelp);
            this.gBCopPhysPar.Controls.Add(this.tBCopEddy);
            this.gBCopPhysPar.Controls.Add(this.label26);
            this.gBCopPhysPar.Controls.Add(this.btnCopChezyHelp);
            this.gBCopPhysPar.Controls.Add(this.tBCopChezy);
            this.gBCopPhysPar.Controls.Add(this.label30);
            this.gBCopPhysPar.Location = new System.Drawing.Point(6, 201);
            this.gBCopPhysPar.Name = "gBCopPhysPar";
            this.gBCopPhysPar.Size = new System.Drawing.Size(440, 130);
            this.gBCopPhysPar.TabIndex = 13;
            this.gBCopPhysPar.TabStop = false;
            this.gBCopPhysPar.Text = "Physical Parameters";
            // 
            // btnCopEddyHelp
            // 
            this.btnCopEddyHelp.Location = new System.Drawing.Point(257, 74);
            this.btnCopEddyHelp.Name = "btnCopEddyHelp";
            this.btnCopEddyHelp.Size = new System.Drawing.Size(75, 20);
            this.btnCopEddyHelp.TabIndex = 22;
            this.btnCopEddyHelp.Text = "Help...";
            this.btnCopEddyHelp.UseVisualStyleBackColor = true;
            this.btnCopEddyHelp.Click += new System.EventHandler(this.BtnCopEddyHelpClick);
            // 
            // tBCopEddy
            // 
            this.tBCopEddy.Location = new System.Drawing.Point(148, 74);
            this.tBCopEddy.Name = "tBCopEddy";
            this.tBCopEddy.Size = new System.Drawing.Size(88, 20);
            this.tBCopEddy.TabIndex = 21;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(105, 13);
            this.label26.TabIndex = 20;
            this.label26.Text = "Eddy Viscosity (m²/s)";
            // 
            // btnCopChezyHelp
            // 
            this.btnCopChezyHelp.Enabled = false;
            this.btnCopChezyHelp.Location = new System.Drawing.Point(257, 40);
            this.btnCopChezyHelp.Name = "btnCopChezyHelp";
            this.btnCopChezyHelp.Size = new System.Drawing.Size(75, 20);
            this.btnCopChezyHelp.TabIndex = 19;
            this.btnCopChezyHelp.Text = "Help...";
            this.btnCopChezyHelp.UseVisualStyleBackColor = true;
            this.btnCopChezyHelp.Click += new System.EventHandler(this.BtnCopChezyHelpClick);
            // 
            // tBCopChezy
            // 
            this.tBCopChezy.Location = new System.Drawing.Point(148, 40);
            this.tBCopChezy.Name = "tBCopChezy";
            this.tBCopChezy.Size = new System.Drawing.Size(88, 20);
            this.tBCopChezy.TabIndex = 12;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 43);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(115, 13);
            this.label30.TabIndex = 11;
            this.label30.Text = "Nikuradse Roughness ";
            // 
            // gBCopTimePar
            // 
            this.gBCopTimePar.Controls.Add(this.btnCopTimeIntervHelp);
            this.gBCopTimePar.Controls.Add(this.tBCopTotalTime);
            this.gBCopTimePar.Controls.Add(this.label24);
            this.gBCopTimePar.Controls.Add(this.tBCopNumIter);
            this.gBCopTimePar.Controls.Add(this.label28);
            this.gBCopTimePar.Controls.Add(this.tBCopTimeInterval);
            this.gBCopTimePar.Controls.Add(this.label27);
            this.gBCopTimePar.Location = new System.Drawing.Point(6, 55);
            this.gBCopTimePar.Name = "gBCopTimePar";
            this.gBCopTimePar.Size = new System.Drawing.Size(440, 130);
            this.gBCopTimePar.TabIndex = 12;
            this.gBCopTimePar.TabStop = false;
            this.gBCopTimePar.Text = "Time Parameters";
            // 
            // btnCopTimeIntervHelp
            // 
            this.btnCopTimeIntervHelp.Enabled = false;
            this.btnCopTimeIntervHelp.Location = new System.Drawing.Point(257, 40);
            this.btnCopTimeIntervHelp.Name = "btnCopTimeIntervHelp";
            this.btnCopTimeIntervHelp.Size = new System.Drawing.Size(75, 20);
            this.btnCopTimeIntervHelp.TabIndex = 19;
            this.btnCopTimeIntervHelp.Text = "Help...";
            this.btnCopTimeIntervHelp.UseVisualStyleBackColor = true;
            this.btnCopTimeIntervHelp.Click += new System.EventHandler(this.BtnCopTimeIntervHelpClick);
            // 
            // tBCopTotalTime
            // 
            this.tBCopTotalTime.Location = new System.Drawing.Point(346, 74);
            this.tBCopTotalTime.Name = "tBCopTotalTime";
            this.tBCopTotalTime.ReadOnly = true;
            this.tBCopTotalTime.Size = new System.Drawing.Size(88, 20);
            this.tBCopTotalTime.TabIndex = 18;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(254, 77);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(71, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "Total Time (s)";
            // 
            // tBCopNumIter
            // 
            this.tBCopNumIter.Location = new System.Drawing.Point(148, 74);
            this.tBCopNumIter.Name = "tBCopNumIter";
            this.tBCopNumIter.Size = new System.Drawing.Size(88, 20);
            this.tBCopNumIter.TabIndex = 16;
            this.tBCopNumIter.Leave += new System.EventHandler(this.tBCopNumIter_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 77);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(80, 13);
            this.label28.TabIndex = 15;
            this.label28.Text = "Nº of Iterations ";
            // 
            // tBCopTimeInterval
            // 
            this.tBCopTimeInterval.Location = new System.Drawing.Point(148, 40);
            this.tBCopTimeInterval.Name = "tBCopTimeInterval";
            this.tBCopTimeInterval.Size = new System.Drawing.Size(88, 20);
            this.tBCopTimeInterval.TabIndex = 12;
            this.tBCopTimeInterval.Leave += new System.EventHandler(this.tBCopTimeInterval_Leave);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 43);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 13);
            this.label27.TabIndex = 11;
            this.label27.Text = "Interval (s)";
            // 
            // cBCoplaCurrents
            // 
            this.cBCoplaCurrents.AutoCheck = false;
            this.cBCoplaCurrents.AutoSize = true;
            this.cBCoplaCurrents.Location = new System.Drawing.Point(15, 19);
            this.cBCoplaCurrents.Name = "cBCoplaCurrents";
            this.cBCoplaCurrents.Size = new System.Drawing.Size(65, 17);
            this.cBCoplaCurrents.TabIndex = 11;
            this.cBCoplaCurrents.Text = "Currents";
            this.cBCoplaCurrents.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Image = global::MOPLA_Launcher.Properties.Resources.refresh16;
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.Location = new System.Drawing.Point(266, 455);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(100, 24);
            this.btnReset.TabIndex = 13;
            this.btnReset.Text = "Reload";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.BtnResetClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Image = global::MOPLA_Launcher.Properties.Resources.delete16;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(372, 455);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 24);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // btnSaveCase
            // 
            this.btnSaveCase.Image = global::MOPLA_Launcher.Properties.Resources.check16;
            this.btnSaveCase.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveCase.Location = new System.Drawing.Point(160, 455);
            this.btnSaveCase.Name = "btnSaveCase";
            this.btnSaveCase.Size = new System.Drawing.Size(100, 24);
            this.btnSaveCase.TabIndex = 11;
            this.btnSaveCase.Text = "Save";
            this.btnSaveCase.UseVisualStyleBackColor = true;
            this.btnSaveCase.Click += new System.EventHandler(this.BtnSaveCaseClick);
            // 
            // FormCase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 482);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSaveCase);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimumSize = new System.Drawing.Size(500, 520);
            this.Name = "FormCase";
            this.ShowInTaskbar = false;
            this.Text = "Case Data";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.gBCopPhysPar.ResumeLayout(false);
            this.gBCopPhysPar.PerformLayout();
            this.gBCopTimePar.ResumeLayout(false);
            this.gBCopTimePar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tBCaseCode;
        private System.Windows.Forms.TextBox tBCaseDesc;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tBMeshCode;
        private System.Windows.Forms.TextBox tBMeshDesc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tBMeshAngle;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tBMeshYdiv;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tBMeshXdiv;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tBMeshYorig;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tBMeshXorig;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tBMeshYcol;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tBMeshXrows;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox tBTMASpecHs;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tBTMASpecDepth;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tBTMASpecNcom;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tBTMASpecGamma;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tBTMASpecMaxFreq;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tBTMASpecPeakFreq;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tBMeanDir;
        private System.Windows.Forms.TextBox tBNCom;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tBDisPar;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton rBModelStokes;
        private System.Windows.Forms.RadioButton rBModelComposed;
        private System.Windows.Forms.RadioButton rBModelLinear;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox cBbotDisLaminar;
        private System.Windows.Forms.CheckBox cBbotDisTurbulent;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox cBbotDisPorous;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rBbreakDisWinyu;
        private System.Windows.Forms.RadioButton rBbreakDisBattjes;
        private System.Windows.Forms.RadioButton rBbreakDisThornton;
        private System.Windows.Forms.CheckBox cBopenLatBound;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox tBTidalRange;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tBYSubDiv;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.RadioButton rBSpectrumUnitsCGS;
        private System.Windows.Forms.RadioButton rBSpectrumUnitsMKS;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.RadioButton rBSpectrumTypeREAD;
        private System.Windows.Forms.RadioButton rBSpectrumTypeTMA;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tBMeshYdim;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tBMeshXdim;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.CheckBox cBCoplaCurrents;
        private System.Windows.Forms.GroupBox gBCopTimePar;
        private System.Windows.Forms.TextBox tBCopTotalTime;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tBCopNumIter;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tBCopTimeInterval;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox gBCopPhysPar;
        private System.Windows.Forms.Button btnCopEddyHelp;
        private System.Windows.Forms.TextBox tBCopEddy;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnCopChezyHelp;
        private System.Windows.Forms.TextBox tBCopChezy;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnCopTimeIntervHelp;
        private System.Windows.Forms.Button btnSaveCase;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnReset;
    }
}
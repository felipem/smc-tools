﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using MOPLA_Launcher.Entities;

namespace MOPLA_Launcher
{
    public partial class FormCase : Form
    {
        private readonly MCase _mCase;
        private readonly MMesh _mMesh;
        private readonly MMesh _mGrandChildrenMesh;

        public FormCase(MCase Case, MMesh mesh, MMesh grandChildrenMesh)
        {
            InitializeComponent();

            _mCase = Case;
            _mMesh = mesh;
            _mGrandChildrenMesh = grandChildrenMesh;
            LoadCaseParameters();
        }

        private void LoadCaseParameters()
        {
            tBCaseCode.GotFocus += delegate { tBCaseCode.Select(0, 0); };

            // Load Case General parameters info
            tBCaseCode.Text = _mCase.CaseParameters.Code;
            tBCaseDesc.Text = _mCase.CaseParameters.Description;

            // Load mesh parameters

            tBMeshCode.Text = _mMesh.MeshCode;
            tBMeshDesc.Text = _mMesh.MeshDescription;
            tBMeshXorig.Text = _mMesh.XOrigin.ToString(CultureInfo.InvariantCulture);
            tBMeshYorig.Text = _mMesh.YOrigin.ToString(CultureInfo.InvariantCulture);
            tBMeshXdim.Text = _mMesh.XDimension.ToString(CultureInfo.InvariantCulture);
            tBMeshYdim.Text = _mMesh.YDimension.ToString(CultureInfo.InvariantCulture);
            tBMeshXdiv.Text = _mMesh.XDivision.ToString(CultureInfo.InvariantCulture);
            tBMeshYdiv.Text = _mMesh.YDivision.ToString(CultureInfo.InvariantCulture);
            tBMeshXrows.Text = _mMesh.XSpacing.ToString(CultureInfo.InvariantCulture);
            tBMeshYcol.Text = _mMesh.YSpacing.ToString(CultureInfo.InvariantCulture);
            tBMeshAngle.Text = _mMesh.Angle.ToString(CultureInfo.InvariantCulture);

            // Load Spectrum parameters
            if (_mCase.CaseSpectrum.SpecType == enumSpecTypes.TMA)
            {
                rBSpectrumTypeTMA.Checked = true;
            }
            else if (_mCase.CaseSpectrum.SpecType == enumSpecTypes.Readed)
            {
                rBSpectrumTypeREAD.Checked = true;
            }

            if (_mCase.CaseSpectrum.SpecUnits == enumSpecUnits.CGS)
            {
                rBSpectrumUnitsCGS.Checked = true;
            }
            else if (_mCase.CaseSpectrum.SpecUnits == enumSpecUnits.MKS)
            {
                rBSpectrumUnitsMKS.Checked = true;
            }

            tBTMASpecDepth.Text = _mCase.CaseSpectrum.TmaSpecDepth.ToString(CultureInfo.InvariantCulture);
            tBTMASpecHs.Text = _mCase.CaseSpectrum.TmaSpecHs.ToString(CultureInfo.InvariantCulture);
            tBTMASpecMaxFreq.Text = _mCase.CaseSpectrum.TmaSpecMaxFrequency.ToString(CultureInfo.InvariantCulture);
            tBTMASpecPeakFreq.Text = _mCase.CaseSpectrum.TmaSpecPeakFrequency.ToString(CultureInfo.InvariantCulture);
            tBTMASpecGamma.Text = _mCase.CaseSpectrum.TmaSpecGamma.ToString(CultureInfo.InvariantCulture);
            tBTMASpecNcom.Text = _mCase.CaseSpectrum.TmaSpecComponentsNum.ToString(CultureInfo.InvariantCulture);

            tBMeanDir.Text = _mCase.CaseSpectrum.MeanDirection.ToString(CultureInfo.InvariantCulture);
            tBNCom.Text = _mCase.CaseSpectrum.DirComponentsNum.ToString(CultureInfo.InvariantCulture);
            tBDisPar.Text = _mCase.CaseSpectrum.DirSpecDispersion.ToString(CultureInfo.InvariantCulture);

            // Load Model parameters
            if (_mCase.CaseSpectrum.Linearity == enumModelLinearity.Linear)
            {
                rBModelLinear.Checked = true;
            }
            else if (_mCase.CaseSpectrum.Linearity == enumModelLinearity.Composed)
            {
                rBModelComposed.Checked = true;
            }
            else if (_mCase.CaseSpectrum.Linearity == enumModelLinearity.Stokes)
            {
                rBModelStokes.Checked = true;
            }

            rBbreakDisThornton.Checked = (_mCase.CaseSpectrum.BreakingDissipationModel == enumBreakingDissipationModel.Thornton);
            rBbreakDisBattjes.Checked = (_mCase.CaseSpectrum.BreakingDissipationModel == enumBreakingDissipationModel.Battjes);
            rBbreakDisWinyu.Checked = (_mCase.CaseSpectrum.BreakingDissipationModel == enumBreakingDissipationModel.Winyu);

            cBbotDisLaminar.Checked = _mCase.CaseSpectrum.BottomDissipationModel.isLaminar;
            cBbotDisPorous.Checked = _mCase.CaseSpectrum.BottomDissipationModel.isPorous;
            cBbotDisTurbulent.Checked = _mCase.CaseSpectrum.BottomDissipationModel.isTurbulent;

            tBTidalRange.Text = _mCase.CaseSpectrum.Tide.ToString(CultureInfo.InvariantCulture);
            tBYSubDiv.Text = _mCase.CaseParameters.SubdivisionYLast.ToString(CultureInfo.InvariantCulture);

            cBopenLatBound.Checked = _mCase.CaseSpectrum.ContoursOpen;

            // Load Copla parameters
            cBCoplaCurrents.Checked = _mCase.CoplaParameters.ExecuteCopla;
            gBCopPhysPar.Enabled = _mCase.CoplaParameters.ExecuteCopla;
            gBCopTimePar.Enabled = _mCase.CoplaParameters.ExecuteCopla;

            tBCopTimeInterval.Text = _mCase.CoplaParameters.Interval.ToString(CultureInfo.InvariantCulture);
            tBCopNumIter.Text = _mCase.CoplaParameters.Iterations.ToString(CultureInfo.InvariantCulture);
            tBCopTotalTime.Text = _mCase.CoplaParameters.TotalTime.ToString(CultureInfo.InvariantCulture);

            tBCopChezy.Text = _mCase.CoplaParameters.Chezy.ToString(CultureInfo.InvariantCulture);
            tBCopEddy.Text = _mCase.CoplaParameters.Eddy.ToString(CultureInfo.InvariantCulture);
        }

        private void SaveCaseParameters()
        {
            // CASESPECTRUM PARAMETERS
            _mCase.CaseSpectrum.TmaSpecHs = Convert.ToSingle(tBTMASpecHs.Text);
            _mCase.CaseSpectrum.TmaSpecPeakFrequency = Convert.ToSingle(tBTMASpecPeakFreq.Text);
            _mCase.CaseSpectrum.TmaSpecGamma = Convert.ToSingle(tBTMASpecGamma.Text);
            _mCase.CaseSpectrum.DirSpecDispersion = Convert.ToSingle(tBDisPar.Text);

            // Model Linearity
            if (rBModelLinear.Checked)
            {
                _mCase.CaseSpectrum.Linearity = enumModelLinearity.Linear;
            }
            else if (rBModelComposed.Checked)
            {
                _mCase.CaseSpectrum.Linearity = enumModelLinearity.Composed;
            }
            else if (rBModelStokes.Checked)
            {
                _mCase.CaseSpectrum.Linearity = enumModelLinearity.Stokes;
            }

            // Bottom Dissipation
            _mCase.CaseSpectrum.BottomDissipationModel.isLaminar = cBbotDisLaminar.Checked;
            _mCase.CaseSpectrum.BottomDissipationModel.isPorous = cBbotDisPorous.Checked;
            _mCase.CaseSpectrum.BottomDissipationModel.isTurbulent = cBbotDisTurbulent.Checked;

            // Breaking Dissipation
            if (rBbreakDisThornton.Checked)
            {
                _mCase.CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Thornton;
            }
            else if (rBbreakDisBattjes.Checked)
            {
                _mCase.CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Battjes;
            }
            else if (rBbreakDisWinyu.Checked)
            {
                _mCase.CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Winyu;
            }

            // Open Boundaries
            _mCase.CaseSpectrum.ContoursOpen = cBopenLatBound.Checked;

            // COPLA PARAMETERS
            if (_mCase.CoplaParameters.ExecuteCopla)
            {
                _mCase.CoplaParameters.Iterations = Convert.ToInt32(tBCopNumIter.Text);
                _mCase.CoplaParameters.Interval = Convert.ToSingle(tBCopTimeInterval.Text);
                _mCase.CoplaParameters.TotalTime = Convert.ToSingle(tBCopTotalTime.Text);

                _mCase.CoplaParameters.Chezy = Convert.ToSingle(tBCopChezy.Text);
                _mCase.CoplaParameters.Eddy = Convert.ToSingle(tBCopEddy.Text);
            }
        }

        private bool CorrrectUserInput()
        {
            CleanTextBoxColors();

            bool boolRet = true;
            float auxFloat;
            int auxInt;

            if (!Single.TryParse(tBTMASpecHs.Text, out auxFloat))
            {
                tBTMASpecHs.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!Single.TryParse(tBTMASpecGamma.Text, out auxFloat))
            {
                tBTMASpecGamma.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!Single.TryParse(tBTMASpecPeakFreq.Text, out auxFloat))
            {
                tBTMASpecPeakFreq.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!Single.TryParse(tBDisPar.Text, out auxFloat))
            {
                tBDisPar.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!Single.TryParse(tBCopTimeInterval.Text, out auxFloat))
            {
                tBCopTimeInterval.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!int.TryParse(tBCopNumIter.Text, out auxInt))
            {
                tBCopNumIter.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!Single.TryParse(tBCopChezy.Text, out auxFloat))
            {
                tBCopChezy.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!Single.TryParse(tBCopEddy.Text, out auxFloat))
            {
                tBCopEddy.BackColor = Color.Yellow;
                boolRet = false;
            }

            return boolRet;
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnSaveCaseClick(object sender, EventArgs e)
        {
            // Change MoplaCase allowed case parameters
            if (CorrrectUserInput())
            {
                // Save parameters in MCase object
                SaveCaseParameters();

                // Save case to enc, in2, cop2 files.
                _mCase.SaveCase();

                // Close form
                Close();
            }
        }

        private void BtnResetClick(object sender, EventArgs e)
        {
            CleanTextBoxColors();
            LoadCaseParameters();
        }

        private void CleanTextBoxColors()
        {
            tBTMASpecHs.BackColor = DefaultBackColor;
            tBTMASpecGamma.BackColor = DefaultBackColor;
            tBTMASpecPeakFreq.BackColor = DefaultBackColor;
            tBDisPar.BackColor = DefaultBackColor;
            tBCopTimeInterval.BackColor = DefaultBackColor;
            tBCopNumIter.BackColor = DefaultBackColor;
            tBCopChezy.BackColor = DefaultBackColor;
            tBCopEddy.BackColor = DefaultBackColor;
        }

        private void BtnCopTimeIntervHelpClick(object sender, EventArgs e)
        {
            double incTime = Math.Min(_mGrandChildrenMesh.XSpacing, _mGrandChildrenMesh.YSpacing) / Math.Sqrt(9.81);
            //TODO: ECUACION INCOMPLETA, PARAMETRO ¿h? ???
        }

        private void BtnCopEddyHelpClick(object sender, EventArgs e)
        {
            tBCopEddy.Text = Math.Floor(Math.Min(_mGrandChildrenMesh.XSpacing / 2, _mGrandChildrenMesh.YSpacing / 2)).ToString(CultureInfo.InvariantCulture);
        }

        private void BtnCopChezyHelpClick(object sender, EventArgs e)
        {
            //TODO: ECUACION CHEZZY
        }

        private void tBCopTimeInterval_Leave(object sender, EventArgs e)
        {
            if (CorrrectUserInput())
            {
                float newTotalTime =  Convert.ToSingle(tBCopTimeInterval.Text)* Convert.ToSingle(tBCopNumIter.Text);

                tBCopTotalTime.Text = newTotalTime.ToString(CultureInfo.InvariantCulture);
            }
        }

        private void tBCopNumIter_Leave(object sender, EventArgs e)
        {
            if (CorrrectUserInput())
            {
                float newTotalTime = Convert.ToSingle(tBCopTimeInterval.Text) * Convert.ToSingle(tBCopNumIter.Text);

                tBCopTotalTime.Text = newTotalTime.ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}

﻿namespace MOPLA_Launcher
{
    partial class FormMain
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.listBoxCases = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.TSMIFile = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMenuopenMOPLAFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMenuLoadCalcScript = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMIParEdition = new System.Windows.Forms.ToolStripMenuItem();
            this.listViewCalcQue = new System.Windows.Forms.ListView();
            this.colCODE = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSTATUS = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colExecInfo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusBarText = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.numUDMaxPar = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tTipCleanFolder = new System.Windows.Forms.ToolTip(this.components);
            this.btnCleanFolder = new System.Windows.Forms.Button();
            this.tTipAddSelCases = new System.Windows.Forms.ToolTip(this.components);
            this.btnAddSelCaseToExecList = new System.Windows.Forms.Button();
            this.tTipAddAllCases = new System.Windows.Forms.ToolTip(this.components);
            this.btnAddAllCasesToExecList = new System.Windows.Forms.Button();
            this.tTipRemoveSelCases = new System.Windows.Forms.ToolTip(this.components);
            this.btnRmvSelCase = new System.Windows.Forms.Button();
            this.tTipRemoveAllCases = new System.Windows.Forms.ToolTip(this.components);
            this.btnClearExecList = new System.Windows.Forms.Button();
            this.btnStopExec = new System.Windows.Forms.Button();
            this.btnStartExec = new System.Windows.Forms.Button();
            this.btnCaseParams = new System.Windows.Forms.Button();
            this.tSMIglobalParEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMICaseParameters = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDMaxPar)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxCases
            // 
            this.listBoxCases.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxCases.FormattingEnabled = true;
            this.listBoxCases.IntegralHeight = false;
            this.listBoxCases.Location = new System.Drawing.Point(12, 52);
            this.listBoxCases.Name = "listBoxCases";
            this.listBoxCases.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxCases.Size = new System.Drawing.Size(120, 456);
            this.listBoxCases.TabIndex = 0;
            this.listBoxCases.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBoxCases_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Case List";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMIFile,
            this.TSMIParEdition});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // TSMIFile
            // 
            this.TSMIFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMenuopenMOPLAFolder,
            this.tSMenuLoadCalcScript,
            this.toolStripSeparator1,
            this.quitToolStripMenuItem});
            this.TSMIFile.Name = "TSMIFile";
            this.TSMIFile.Size = new System.Drawing.Size(37, 20);
            this.TSMIFile.Text = "File";
            // 
            // TSMenuopenMOPLAFolder
            // 
            this.TSMenuopenMOPLAFolder.Name = "TSMenuopenMOPLAFolder";
            this.TSMenuopenMOPLAFolder.Size = new System.Drawing.Size(193, 22);
            this.TSMenuopenMOPLAFolder.Text = "Open MOPLA folder";
            this.TSMenuopenMOPLAFolder.Click += new System.EventHandler(this.OpenMoplaFolderToolStripMenuItemClick);
            // 
            // tSMenuLoadCalcScript
            // 
            this.tSMenuLoadCalcScript.Name = "tSMenuLoadCalcScript";
            this.tSMenuLoadCalcScript.Size = new System.Drawing.Size(193, 22);
            this.tSMenuLoadCalcScript.Text = "Load calculation script";
            this.tSMenuLoadCalcScript.Click += new System.EventHandler(this.TsMenuLoadCalcScriptClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(190, 6);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.QuitToolStripMenuItemClick);
            // 
            // TSMIParEdition
            // 
            this.TSMIParEdition.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSMICaseParameters,
            this.tSMIglobalParEdit});
            this.TSMIParEdition.Name = "TSMIParEdition";
            this.TSMIParEdition.Size = new System.Drawing.Size(118, 20);
            this.TSMIParEdition.Text = "Parameters Edition";
            // 
            // listViewCalcQue
            // 
            this.listViewCalcQue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewCalcQue.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCODE,
            this.colSTATUS,
            this.colExecInfo});
            this.listViewCalcQue.FullRowSelect = true;
            this.listViewCalcQue.GridLines = true;
            this.listViewCalcQue.Location = new System.Drawing.Point(184, 52);
            this.listViewCalcQue.Name = "listViewCalcQue";
            this.listViewCalcQue.Size = new System.Drawing.Size(542, 456);
            this.listViewCalcQue.TabIndex = 3;
            this.listViewCalcQue.UseCompatibleStateImageBehavior = false;
            this.listViewCalcQue.View = System.Windows.Forms.View.Details;
            this.listViewCalcQue.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.listViewCalcQue_ColumnWidthChanging);
            this.listViewCalcQue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listViewCalcQue_KeyDown);
            this.listViewCalcQue.Resize += new System.EventHandler(this.listViewCalcQue_Resize);
            // 
            // colCODE
            // 
            this.colCODE.Text = "CODE";
            this.colCODE.Width = 120;
            // 
            // colSTATUS
            // 
            this.colSTATUS.Text = "STATUS";
            this.colSTATUS.Width = 120;
            // 
            // colExecInfo
            // 
            this.colExecInfo.Text = "Information";
            this.colExecInfo.Width = 227;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Calculation Queue";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarText,
            this.toolStripStatusLabel,
            this.toolStripProgressBar});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 13;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusBarText
            // 
            this.statusBarText.Name = "statusBarText";
            this.statusBarText.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgressBar.Margin = new System.Windows.Forms.Padding(1, 3, 44, 3);
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(131, 16);
            // 
            // numUDMaxPar
            // 
            this.numUDMaxPar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numUDMaxPar.Location = new System.Drawing.Point(683, 514);
            this.numUDMaxPar.Name = "numUDMaxPar";
            this.numUDMaxPar.Size = new System.Drawing.Size(43, 20);
            this.numUDMaxPar.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(592, 516);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Number of cores";
            // 
            // btnCleanFolder
            // 
            this.btnCleanFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCleanFolder.Image = global::MOPLA_Launcher.Properties.Resources.garbage16;
            this.btnCleanFolder.Location = new System.Drawing.Point(732, 398);
            this.btnCleanFolder.Name = "btnCleanFolder";
            this.btnCleanFolder.Size = new System.Drawing.Size(40, 23);
            this.btnCleanFolder.TabIndex = 17;
            this.tTipCleanFolder.SetToolTip(this.btnCleanFolder, "Clean SP directory");
            this.btnCleanFolder.UseVisualStyleBackColor = true;
            this.btnCleanFolder.Click += new System.EventHandler(this.btnCleanFolder_Click);
            // 
            // btnAddSelCaseToExecList
            // 
            this.btnAddSelCaseToExecList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddSelCaseToExecList.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSelCaseToExecList.Image")));
            this.btnAddSelCaseToExecList.Location = new System.Drawing.Point(138, 456);
            this.btnAddSelCaseToExecList.Name = "btnAddSelCaseToExecList";
            this.btnAddSelCaseToExecList.Size = new System.Drawing.Size(40, 23);
            this.btnAddSelCaseToExecList.TabIndex = 5;
            this.tTipAddSelCases.SetToolTip(this.btnAddSelCaseToExecList, "Add selected cases to queue");
            this.btnAddSelCaseToExecList.UseVisualStyleBackColor = true;
            this.btnAddSelCaseToExecList.Click += new System.EventHandler(this.btnAddSelCaseToExecList_Click);
            // 
            // btnAddAllCasesToExecList
            // 
            this.btnAddAllCasesToExecList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddAllCasesToExecList.Image = global::MOPLA_Launcher.Properties.Resources.navigate_right216;
            this.btnAddAllCasesToExecList.Location = new System.Drawing.Point(138, 485);
            this.btnAddAllCasesToExecList.Name = "btnAddAllCasesToExecList";
            this.btnAddAllCasesToExecList.Size = new System.Drawing.Size(40, 23);
            this.btnAddAllCasesToExecList.TabIndex = 6;
            this.tTipAddAllCases.SetToolTip(this.btnAddAllCasesToExecList, "Add all cases to queue");
            this.btnAddAllCasesToExecList.UseVisualStyleBackColor = true;
            this.btnAddAllCasesToExecList.Click += new System.EventHandler(this.btnAddAllCasesToExecList_Click);
            // 
            // btnRmvSelCase
            // 
            this.btnRmvSelCase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRmvSelCase.Image = global::MOPLA_Launcher.Properties.Resources.row_delete16;
            this.btnRmvSelCase.Location = new System.Drawing.Point(732, 456);
            this.btnRmvSelCase.Name = "btnRmvSelCase";
            this.btnRmvSelCase.Size = new System.Drawing.Size(40, 23);
            this.btnRmvSelCase.TabIndex = 7;
            this.tTipRemoveSelCases.SetToolTip(this.btnRmvSelCase, "Remove selected cases from queue");
            this.btnRmvSelCase.UseVisualStyleBackColor = true;
            this.btnRmvSelCase.Click += new System.EventHandler(this.btnRmvSelCase_Click);
            // 
            // btnClearExecList
            // 
            this.btnClearExecList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearExecList.Image = global::MOPLA_Launcher.Properties.Resources.table_delete16;
            this.btnClearExecList.Location = new System.Drawing.Point(732, 485);
            this.btnClearExecList.Name = "btnClearExecList";
            this.btnClearExecList.Size = new System.Drawing.Size(40, 23);
            this.btnClearExecList.TabIndex = 8;
            this.tTipRemoveAllCases.SetToolTip(this.btnClearExecList, "Remove all cases from queue");
            this.btnClearExecList.UseVisualStyleBackColor = true;
            this.btnClearExecList.Click += new System.EventHandler(this.btnClearExecList_Click);
            // 
            // btnStopExec
            // 
            this.btnStopExec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStopExec.Enabled = false;
            this.btnStopExec.Image = global::MOPLA_Launcher.Properties.Resources.gears_stop16;
            this.btnStopExec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStopExec.Location = new System.Drawing.Point(310, 514);
            this.btnStopExec.Name = "btnStopExec";
            this.btnStopExec.Size = new System.Drawing.Size(120, 23);
            this.btnStopExec.TabIndex = 12;
            this.btnStopExec.Text = "Stop";
            this.btnStopExec.UseVisualStyleBackColor = true;
            this.btnStopExec.Click += new System.EventHandler(this.btnStopExec_Click);
            // 
            // btnStartExec
            // 
            this.btnStartExec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStartExec.Image = global::MOPLA_Launcher.Properties.Resources.gears_run16;
            this.btnStartExec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStartExec.Location = new System.Drawing.Point(184, 514);
            this.btnStartExec.Name = "btnStartExec";
            this.btnStartExec.Size = new System.Drawing.Size(120, 23);
            this.btnStartExec.TabIndex = 10;
            this.btnStartExec.Text = "Start";
            this.btnStartExec.UseVisualStyleBackColor = true;
            this.btnStartExec.Click += new System.EventHandler(this.btnStartExec_Click);
            // 
            // btnCaseParams
            // 
            this.btnCaseParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCaseParams.Image = global::MOPLA_Launcher.Properties.Resources.note_view16;
            this.btnCaseParams.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCaseParams.Location = new System.Drawing.Point(12, 514);
            this.btnCaseParams.Name = "btnCaseParams";
            this.btnCaseParams.Size = new System.Drawing.Size(120, 23);
            this.btnCaseParams.TabIndex = 9;
            this.btnCaseParams.Text = "Case Parameters";
            this.btnCaseParams.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCaseParams.UseVisualStyleBackColor = true;
            this.btnCaseParams.Click += new System.EventHandler(this.btnCaseParams_Click);
            // 
            // tSMIglobalParEdit
            // 
            this.tSMIglobalParEdit.Name = "tSMIglobalParEdit";
            this.tSMIglobalParEdit.Size = new System.Drawing.Size(161, 22);
            this.tSMIglobalParEdit.Text = "Global Par. Edit";
            this.tSMIglobalParEdit.Click += new System.EventHandler(this.ToolStripMenuItem1Click);
            // 
            // tSMICaseParameters
            // 
            this.tSMICaseParameters.Name = "tSMICaseParameters";
            this.tSMICaseParameters.Size = new System.Drawing.Size(161, 22);
            this.tSMICaseParameters.Text = "Case Parameters";
            this.tSMICaseParameters.Click += new System.EventHandler(this.SmiCaseParametersClick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnCleanFolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numUDMaxPar);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnStopExec);
            this.Controls.Add(this.btnStartExec);
            this.Controls.Add(this.btnCaseParams);
            this.Controls.Add(this.btnClearExecList);
            this.Controls.Add(this.btnRmvSelCase);
            this.Controls.Add(this.btnAddAllCasesToExecList);
            this.Controls.Add(this.btnAddSelCaseToExecList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listViewCalcQue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxCases);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormMain";
            this.Text = "MOPLA Case Launcher";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDMaxPar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxCases;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TSMIFile;
        private System.Windows.Forms.ToolStripMenuItem TSMenuopenMOPLAFolder;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ListView listViewCalcQue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddSelCaseToExecList;
        private System.Windows.Forms.Button btnAddAllCasesToExecList;
        private System.Windows.Forms.Button btnClearExecList;
        private System.Windows.Forms.Button btnRmvSelCase;
        private System.Windows.Forms.Button btnCaseParams;
        private System.Windows.Forms.Button btnStartExec;
        private System.Windows.Forms.Button btnStopExec;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusBarText;
        private System.Windows.Forms.ColumnHeader colCODE;
        private System.Windows.Forms.ColumnHeader colSTATUS;
        private System.Windows.Forms.NumericUpDown numUDMaxPar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCleanFolder;
        private System.Windows.Forms.ColumnHeader colExecInfo;
        private System.Windows.Forms.ToolTip tTipCleanFolder;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolTip tTipAddSelCases;
        private System.Windows.Forms.ToolTip tTipAddAllCases;
        private System.Windows.Forms.ToolTip tTipRemoveSelCases;
        private System.Windows.Forms.ToolTip tTipRemoveAllCases;
        private System.Windows.Forms.ToolStripMenuItem tSMenuLoadCalcScript;
        private System.Windows.Forms.ToolStripMenuItem TSMIParEdition;
        private System.Windows.Forms.ToolStripMenuItem tSMIglobalParEdit;
        private System.Windows.Forms.ToolStripMenuItem tSMICaseParameters;
    }
}


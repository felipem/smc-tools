﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using MOPLA_Launcher.Entities;

namespace MOPLA_Launcher
{
    public partial class FormMain : Form
    {
        # region Properties

        private string _defaultInitialMoplaFolder = @"C:\TRABAJO\SMC_Tareas\sampleSMC3_2\Alternativa 1\Mopla\";  //TODO: RUTA POR DEFECTO PARA FACILITAR DEBUG
        private string _moplaCaseFolder;
        private readonly MoplaLPaths _moplaLPaths;
        private readonly List<MCase> _lstCases = new List<MCase>();
        private readonly List<MMesh> _lstMeshes = new List<MMesh>();

        private CaseLauncher _caseLauncher;
        private readonly ControlSwitcher _controlSwitcher;
        private MChecker _mChecker;
        private MoplaLog _moplaLog;
        public MProgressBar MProgressBar;

        private CancellationTokenSource _tokenSource;

        # endregion

        # region Constructor

        public FormMain()
        {
            InitializeComponent();

            #region Establece en la aplicatión el "." como separador decimal (solo valido para Windows)
            CultureInfo cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            #endregion

            // Number of parallel process
            int nCores = Environment.ProcessorCount;
            numUDMaxPar.Minimum = 1;
            numUDMaxPar.Maximum = nCores;
            numUDMaxPar.Value = nCores;

            // Initialize Control Switcher (enables/disables controls)
            List<Control> lstCtrlOffWhenExecuting = new List<Control>() { btnStartExec, btnAddSelCaseToExecList, btnAddAllCasesToExecList, btnRmvSelCase, btnClearExecList, btnCleanFolder, numUDMaxPar, btnCaseParams };
            List<ToolStripItem> lstTsmiOffWhenExecuting = new List<ToolStripItem>() { TSMIFile, TSMIParEdition };
            List<Control> lstCtrlOnWhenExecuting = new List<Control>() { btnStopExec };
            _controlSwitcher = new ControlSwitcher(lstCtrlOffWhenExecuting, lstTsmiOffWhenExecuting, lstCtrlOnWhenExecuting);

            // Get app execution folder
            _moplaLPaths = new MoplaLPaths(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));

            // Initialize toolStripProgressBar
            MProgressBar = new MProgressBar(toolStripProgressBar);

            // Load CalculationScript in arguments
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                if (File.Exists(args[1]))
                {
                    InitializeWithCalcScript(args[1]);
                }
            }
        }

        # endregion

        # region Events

        private void OpenMoplaFolderToolStripMenuItemClick(object sender, EventArgs e)
        {
            // Select MOPLA folder and load MOPLA Cases to GUI
            var fbd = new FolderBrowserDialog { SelectedPath = _defaultInitialMoplaFolder };
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                // Initialize the launcher
                InitializeMoplaLauncher(fbd.SelectedPath);
            }
        }

        private void TsMenuLoadCalcScriptClick(object sender, EventArgs e)
        {
            // User select a calculation script
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text files (*.dat)|*.dat|All files (*.*)|*.*";
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                // Initialize the launcher 
                InitializeWithCalcScript(ofd.FileName);
            }
        }

        private void QuitToolStripMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ToolStripMenuItem1Click(object sender, EventArgs e)
        {
            if (_lstCases.Count > 0)
            {
                FormMassParEdit formMassParEdit = new FormMassParEdit(_lstCases);
                formMassParEdit.ShowDialog();
            }
        }

        private void SmiCaseParametersClick(object sender, EventArgs e)
        {
            int lbIndex = listBoxCases.SelectedIndex;
            if (lbIndex != -1)
            {
                MCase CaseToLoad = _lstCases[lbIndex];
                MMesh MeshToLoad = _lstMeshes.Find(mesh => mesh.MeshCode.Equals(CaseToLoad.CaseParameters.MeshCode));

                // Get copla grandChildren mesh
                MMesh GrandChildrenMesh = CaseToLoad.CaseParameters.ListChildrenMeshCode.Count > 0 ? _lstMeshes.Find(mesh => mesh.MeshCode.Equals(CaseToLoad.CaseParameters.ListChildrenMeshCode[CaseToLoad.CaseParameters.ListChildrenMeshCode.Count - 1])) : MeshToLoad;

                FormCase formCase = new FormCase(CaseToLoad, MeshToLoad, GrandChildrenMesh);
                formCase.ShowDialog();
            }
        }


        private void btnCaseParams_Click(object sender, EventArgs e)
        {
            int lbIndex = listBoxCases.SelectedIndex;
            if (lbIndex != -1)
            {
                MCase CaseToLoad = _lstCases[lbIndex];
                MMesh MeshToLoad = _lstMeshes.Find(mesh => mesh.MeshCode.Equals(CaseToLoad.CaseParameters.MeshCode));

                // Get copla grandChildren mesh
                MMesh GrandChildrenMesh = CaseToLoad.CaseParameters.ListChildrenMeshCode.Count > 0 ? _lstMeshes.Find(mesh => mesh.MeshCode.Equals(CaseToLoad.CaseParameters.ListChildrenMeshCode[CaseToLoad.CaseParameters.ListChildrenMeshCode.Count - 1])) : MeshToLoad;

                FormCase formCase = new FormCase(CaseToLoad, MeshToLoad, GrandChildrenMesh);
                formCase.ShowDialog();
            }
        }

        private void btnAddSelCaseToExecList_Click(object sender, EventArgs e)
        {
            AddSelLBCasesToQueue();
        }

        private void btnAddAllCasesToExecList_Click(object sender, EventArgs e)
        {
            listViewCalcQue.Items.Clear();
            foreach (MCase Case in _lstCases)
            {
                listViewCalcQue.Items.Add(Case.CaseLvItem);
            }
        }

        private void btnRmvSelCase_Click(object sender, EventArgs e)
        {
            // Empty progress bar 
            MProgressBar.EmptyProgressBar();

            DelCasesFromLVQueue();
        }

        private void btnClearExecList_Click(object sender, EventArgs e)
        {
            // Empty progress bar 
            MProgressBar.EmptyProgressBar();

            listViewCalcQue.Items.Clear();
        }

        private void btnStartExec_Click(object sender, EventArgs e)
        {
            if (listViewCalcQue.Items.Count <= 0) return;

            // Get cases to execute
            List<MCase> lstCasesToExecute = listViewCalcQue.Items.Cast<ListViewItem>().Select(caseQueueItem => _lstCases.Find(algo => algo.CaseParameters.Code.Equals(caseQueueItem.Text))).Where(CaseToLaunch => CaseToLaunch.IsNotExecuted()).ToList();
            if (lstCasesToExecute.Count == 0) return;

            // Empty progress bar 
            MProgressBar.EmptyProgressBar();

            // Get degree of parallelism, create cancellation token, and start execution
            int maxDegreeOfParallelism = (int)numUDMaxPar.Value;

            // Create a new cancellation token source for this simulation
            _tokenSource = new CancellationTokenSource();

            // Set TaskFactory and start cases execution
            _caseLauncher.CreateFactory(maxDegreeOfParallelism);
            _caseLauncher.StartExecution(lstCasesToExecute, _lstMeshes, _tokenSource);

            // Disable / enable neccessary buttons
            _controlSwitcher.SetExecutionMode();
        }

        private void btnStopExec_Click(object sender, EventArgs e)
        {
            // Send cancelation token
            _tokenSource.Cancel();

            btnStopExec.Enabled = false;

            // kill olucasp process
            foreach (Process myProc in Process.GetProcesses())
            {
                if (myProc.ProcessName.Contains("olucasp"))
                {
                    myProc.Kill();
                }
            }
        }

        private void btnCleanFolder_Click(object sender, EventArgs e)
        {
            // Empty progress bar 
            MProgressBar.EmptyProgressBar();

            CultureInfo ci = new CultureInfo("en-US");
            if (_moplaCaseFolder != null)
            {
                // Delete output files
                string exeFolderPath = Path.Combine(_moplaCaseFolder, "SP");
                string logFolderPath = Path.Combine(_moplaCaseFolder, "SP", "CaseLogs");
                if (listViewCalcQue.Items.Count <= 0) return;
                // Get cases to execute
                List<MCase> lstCasesToExecute = listViewCalcQue.Items.Cast<ListViewItem>().Select(caseQueueItem => _lstCases.Find(algo => algo.CaseParameters.Code.Equals(caseQueueItem.Text))).ToList();
                foreach (MCase mCase in lstCasesToExecute)                                   
                {                    
                    var files = Directory.GetFiles(exeFolderPath, "*"+mCase.CaseParameters.Code+"*.*").ToList();
                    files.AddRange(Directory.GetFiles(logFolderPath, mCase.CaseParameters.Code + "*.log"));
                    
                    foreach (var file in files)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }
                    //if (Directory.Exists(logFolderPath))
                    //{
                    //    try
                    //    {
                    //        Directory.Delete(logFolderPath, true);
                    //    }
                    //    catch { }
                    //}
                }
                
                //List<MCase> lstCasesToExecute = listViewCalcQue.Items.Cast<ListViewItem>().Select(caseQueueItem => _lstCases.Find(algo => algo.CaseParameters.Code.Equals(caseQueueItem.Text))).Where(CaseToLaunch => CaseToLaunch.IsNotExecuted()).ToList();
                //if (lstCasesToExecute.Count == 0) return;

                //var files = Directory.GetFiles(exeFolderPath).Where(name => !name.EndsWith("Bathymetry_Inp.GRD", true, ci));
                
                

                // Delete cases log folder
                

                // Reset all MCases ListViewItems to original State
                foreach (MCase Mcase in _lstCases)
                {
                    Mcase.ResetCaseLVIState();
                }
            }
        }


        private void listViewCalcQue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete && !_controlSwitcher.IsExecuting)
            {
                // Empty progress bar 
                MProgressBar.EmptyProgressBar();

                DelCasesFromLVQueue();
            }
        }

        private void listBoxCases_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && !_controlSwitcher.IsExecuting)
            {
                AddSelLBCasesToQueue();
            }
        }


        private void listViewCalcQue_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = listViewCalcQue.Columns[e.ColumnIndex].Width;
        }

        private void listViewCalcQue_Resize(object sender, EventArgs e)
        {
            SizeLastColumn(listViewCalcQue);
        }


        private void FormMain_Load(object sender, EventArgs e)
        {
            SizeLastColumn(listViewCalcQue);
        }

        # endregion

        # region Methods

        private void InitializeMoplaLauncher(string folderPath)
        {
            // Mopla case folder
            _defaultInitialMoplaFolder = folderPath;
            _moplaCaseFolder = folderPath;
            toolStripStatusLabel.Text = folderPath;

            // Initialize Mopla Checker and Log
            _mChecker = new MChecker(_moplaCaseFolder);
            _moplaLog = new MoplaLog(_moplaCaseFolder);

            // Check folder files and load cases and meshes info to memory.
            LoadMoplaMeshes();
            LoadMoplaCases();
            GetCompletedMoplaCases();

            // Initialize CaseLauncher
            _caseLauncher = new CaseLauncher(_moplaLPaths, _moplaCaseFolder, _controlSwitcher, MProgressBar);

            // Populate Cases listbox
            PopulateLbCases();

            // Empty progress bar 
            MProgressBar.EmptyProgressBar();
        }

        private void InitializeWithCalcScript(string calcScripPath)
        {
            // Normally initialize the launcher
            InitializeMoplaLauncher(Path.GetDirectoryName(calcScripPath));
            // Read calculation script and add cases to queue
            AddCalcScriptCasesToQueue(calcScripPath);
        }

        private void LoadMoplaMeshes()
        {
            // Find files inside Mopla folder ended with "REF2.DAT"
            var files = Directory.GetFiles(_moplaCaseFolder).Where(name => name.EndsWith("REF2.DAT"));

            // Clear meshes list
            _lstMeshes.Clear();

            // return if there are not Mopla meshes to load
            if (!files.Any())
            {
                _moplaLog.NoMeshesOnFolder();
                return;
            }

            // Start reading mesh files
            _moplaLog.StartLoadingMeshes();

            foreach (string file in files)
            {
                string MeshCode = Path.GetFileName(file).Replace("REF2.DAT", string.Empty);

                if (_mChecker.CheckMeshFilesExists(MeshCode))
                {
                    try // To avoid exceptions caused by files with wrong content
                    {
                        MMesh meshToAdd = new MMesh(new FileInfo(file));
                        _lstMeshes.Add(meshToAdd);
                        _moplaLog.AddMesh(MeshCode);
                    }
                    catch
                    {
                        _moplaLog.AddMeshLoadingError(MeshCode);
                    }
                }
                else
                {
                    _moplaLog.AddMeshMissingFilesError(MeshCode);
                }
            }

            // Obtain necessary children mesh data for each mesh
            foreach (MMesh mMesh in _lstMeshes)
            {
                if (mMesh.meshHasChildren)
                {
                    // Calculate parent meshes subdivisions with children meshes
                    MMesh childrenMesh = _lstMeshes.Find(mesh => mesh.MeshCode.Equals(mMesh.ChildrenMeshCode));
                    if (childrenMesh != null)
                    {
                        mMesh.SubdivisionChildren = childrenMesh.YDivision / (mMesh.ChildrenNodeEnd - mMesh.ChildrenNodeInit);
                    }

                    // Get last children X and Y divisions
                    MMesh iterMesh = mMesh;
                    while (iterMesh.meshHasChildren)
                    {
                        MMesh tempMesh = _lstMeshes.Find(mesh => mesh.MeshCode.Equals(iterMesh.ChildrenMeshCode));
                        if (tempMesh != null)
                        {
                            iterMesh = tempMesh;
                        }
                        else
                        {
                            break;
                        }
                    }
                    mMesh.XDivisionLastChildren = iterMesh.XDivisionLastChildren;
                    mMesh.YDivisionLastChildren = iterMesh.YDivisionLastChildren;
                }
            }
        }

        private void LoadMoplaCases()
        {
            // Find files inside Mopla folder ended with "REF2.DAT"
            var files = Directory.GetFiles(_moplaCaseFolder).Where(name => name.EndsWith("ENC.DAT"));

            // Clear cases list
            _lstCases.Clear();

            // return if there are not Mopla meshes to load
            if (!files.Any())
            {
                _moplaLog.NoCasesOnFolder();
                return;
            }

            // Start reading case files
            _moplaLog.StartLoadingCases();

            List<string> listMeshesCodes = _lstMeshes.Select(mMesh => mMesh.MeshCode).ToList();
            List<string> lstCaseMeshesCode = new List<string>();

            // Create cases
            foreach (string encFile in files)
            {
                string caseMasterCode = Path.GetFileName(encFile).Replace("ENC.DAT", string.Empty);

                if (_mChecker.CheckCaseFilesExists(caseMasterCode))
                {
                    try  // To avoid exceptions caused by files with wrong content
                    {
                        // Create case from enc and in2 files
                        var caseToAdd = new MCase(encFile);

                        // Check case meshes are in the meshes list
                        lstCaseMeshesCode.Clear();
                        lstCaseMeshesCode.Add(caseToAdd.CaseParameters.MeshCode);
                        lstCaseMeshesCode.AddRange(caseToAdd.CaseParameters.ListChildrenMeshCode);

                        if (lstCaseMeshesCode.All(listMeshesCodes.Contains))
                        {
                            _lstCases.Add(caseToAdd);
                            _moplaLog.AddCase(caseToAdd.CaseParameters.Code);
                        }
                        else
                        {
                            _moplaLog.AddCaseMissingMeshError(caseToAdd.CaseParameters.Code);
                        }
                    }
                    catch
                    {
                        _moplaLog.AddCaseLoadingError(caseMasterCode);
                    }
                }
                else
                {
                    _moplaLog.AddCaseMissingFilesError(caseMasterCode);
                }
            }
        }

        private void GetCompletedMoplaCases()
        {
            // Check cases status with MOPLA/SP/ .log files
            foreach (MCase mCase in _lstCases)
            {
                string caseLogPath = Path.Combine(_moplaCaseFolder, "SP", "CaseLogs", mCase.CaseParameters.Code + ".log");

                enumCaseStatus caseStatus = _mChecker.checkMoplaCaseIsSolved(caseLogPath);

                switch (caseStatus)
                {
                    case enumCaseStatus.Completed:
                        mCase.CaseParameters.CaseStatus = enumCaseStatus.Completed;
                        mCase.CaseParameters.CaseExecInfo = enumCaseExecInfo.SimulationFinished;
                        foreach (ListViewItem.ListViewSubItem subItem in mCase.CaseLvItem.SubItems)                        
                            subItem.ForeColor = Color.DarkBlue;                        
                        break;
                    case enumCaseStatus.Cancelled:
                        mCase.CaseParameters.CaseStatus = enumCaseStatus.Cancelled;
                        mCase.CaseParameters.CaseExecInfo = enumCaseExecInfo.NoInfo;
                        foreach (ListViewItem.ListViewSubItem subItem in mCase.CaseLvItem.SubItems)
                            subItem.ForeColor = Color.Red;                        
                        break;
                    case enumCaseStatus.Error:
                        mCase.CaseParameters.CaseStatus = enumCaseStatus.Error;
                        mCase.CaseParameters.CaseExecInfo = enumCaseExecInfo.ErrorOlucaFail;
                        foreach (ListViewItem.ListViewSubItem subItem in mCase.CaseLvItem.SubItems)
                            subItem.ForeColor = Color.Red;                        
                        break;                        
                }

                //if (caseIsSolved==enumCaseStatus.Completed)
                //{
                //    // Change Case Status to Completed
                //    mCase.CaseParameters.CaseStatus = enumCaseStatus.Completed;
                //    mCase.CaseParameters.CaseExecInfo = enumCaseExecInfo.SimulationFinished;

                //    foreach (ListViewItem.ListViewSubItem subItem in mCase.CaseLvItem.SubItems)
                //    {
                //        subItem.ForeColor = Color.DarkBlue;
                //    }
                //}

                mCase.CaseLvItem.SubItems[1].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseStatus);
                mCase.CaseLvItem.SubItems[2].Text = GetEnumDesc.GetEnumDescription(mCase.CaseParameters.CaseExecInfo);
            }
        }

        private void PopulateLbCases()
        {
            // Clear GUI controls
            listViewCalcQue.Items.Clear();
            listBoxCases.DataSource = null;

            // Populate ListBox
            var auxStrList = new List<string>();
            foreach (var MCase in _lstCases)
            {
                auxStrList.Add(MCase.CaseParameters.Code);
            }
            listBoxCases.DataSource = auxStrList;
        }


        private void AddCalcScriptCasesToQueue(string calcScripPath)
        {
            var lines = File.ReadLines(calcScripPath);
            foreach (string line in lines)
            {
                string[] caseRecord = line.Split('"');

                if (caseRecord.Count() == 3)
                {
                    string caseCode = caseRecord[1];
                    MCase caseToAdd = _lstCases.Find(mcase => mcase.CaseParameters.Code == caseCode);
                    if (caseToAdd != null)
                    {
                        if (listViewCalcQue.FindItemWithText(caseToAdd.CaseParameters.Code) == null)
                        {
                            listViewCalcQue.Items.Add(caseToAdd.CaseLvItem);
                        }
                    }
                }
            }
        }

        private void AddSelLBCasesToQueue()
        {
            ListBox.SelectedObjectCollection lbSelectedItems = listBoxCases.SelectedItems;
            foreach (var lbSelectedItem in lbSelectedItems)
            {
                MCase caseToAdd = _lstCases.Find(mcase => mcase.CaseParameters.Code == (string)lbSelectedItem);

                if (listViewCalcQue.FindItemWithText(caseToAdd.CaseParameters.Code) == null)
                {
                    listViewCalcQue.Items.Add(caseToAdd.CaseLvItem);
                }
            }
        }

        private void DelCasesFromLVQueue()
        {
            foreach (ListViewItem selectedItem in listViewCalcQue.SelectedItems)
            {
                listViewCalcQue.Items.Remove(selectedItem);
            }
        }


        private static void SizeLastColumn(ListView lv)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
            lv.Columns[lv.Columns.Count - 1].Width -= 1;
        }

        #endregion

    }
}
﻿namespace MOPLA_Launcher
{
    partial class FormMassParEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.cBopenLatBound = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cBbotDisLaminar = new System.Windows.Forms.CheckBox();
            this.cBbotDisPorous = new System.Windows.Forms.CheckBox();
            this.cBbotDisTurbulent = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rBbreakDisWinyu = new System.Windows.Forms.RadioButton();
            this.rBbreakDisBattjes = new System.Windows.Forms.RadioButton();
            this.rBbreakDisThornton = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.rBModelStokes = new System.Windows.Forms.RadioButton();
            this.rBModelComposed = new System.Windows.Forms.RadioButton();
            this.rBModelLinear = new System.Windows.Forms.RadioButton();
            this.gBModel = new System.Windows.Forms.GroupBox();
            this.gBSpectrum = new System.Windows.Forms.GroupBox();
            this.tBTMASpecGamma = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tBDisPar = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.gbCopla = new System.Windows.Forms.GroupBox();
            this.tBCopNumIter = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.listViewEditCases = new System.Windows.Forms.ListView();
            this.colCODE = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSTATUS = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveCase = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rBacUserSel = new System.Windows.Forms.RadioButton();
            this.rBacUncCases = new System.Windows.Forms.RadioButton();
            this.rBacAllCases = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cBChangesModel = new System.Windows.Forms.CheckBox();
            this.cBchangesSpectrum = new System.Windows.Forms.CheckBox();
            this.cBChangesCopla = new System.Windows.Forms.CheckBox();
            this.groupBox10.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.gBModel.SuspendLayout();
            this.gBSpectrum.SuspendLayout();
            this.gbCopla.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.cBopenLatBound);
            this.groupBox10.Location = new System.Drawing.Point(6, 195);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(440, 50);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Boundaries";
            // 
            // cBopenLatBound
            // 
            this.cBopenLatBound.AutoSize = true;
            this.cBopenLatBound.Location = new System.Drawing.Point(23, 19);
            this.cBopenLatBound.Name = "cBopenLatBound";
            this.cBopenLatBound.Size = new System.Drawing.Size(143, 17);
            this.cBopenLatBound.TabIndex = 11;
            this.cBopenLatBound.Text = "Open Lateral Boundaries";
            this.cBopenLatBound.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cBbotDisLaminar);
            this.groupBox8.Controls.Add(this.cBbotDisPorous);
            this.groupBox8.Controls.Add(this.cBbotDisTurbulent);
            this.groupBox8.Location = new System.Drawing.Point(229, 75);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(217, 114);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Bottom Dissipation";
            // 
            // cBbotDisLaminar
            // 
            this.cBbotDisLaminar.AutoSize = true;
            this.cBbotDisLaminar.Location = new System.Drawing.Point(6, 91);
            this.cBbotDisLaminar.Name = "cBbotDisLaminar";
            this.cBbotDisLaminar.Size = new System.Drawing.Size(140, 17);
            this.cBbotDisLaminar.TabIndex = 12;
            this.cBbotDisLaminar.Text = "Laminar Boundary Layer";
            this.cBbotDisLaminar.UseVisualStyleBackColor = true;
            // 
            // cBbotDisPorous
            // 
            this.cBbotDisPorous.AutoSize = true;
            this.cBbotDisPorous.Location = new System.Drawing.Point(6, 19);
            this.cBbotDisPorous.Name = "cBbotDisPorous";
            this.cBbotDisPorous.Size = new System.Drawing.Size(95, 17);
            this.cBbotDisPorous.TabIndex = 10;
            this.cBbotDisPorous.Text = "Porous Bottom";
            this.cBbotDisPorous.UseVisualStyleBackColor = true;
            // 
            // cBbotDisTurbulent
            // 
            this.cBbotDisTurbulent.AutoSize = true;
            this.cBbotDisTurbulent.Location = new System.Drawing.Point(6, 55);
            this.cBbotDisTurbulent.Name = "cBbotDisTurbulent";
            this.cBbotDisTurbulent.Size = new System.Drawing.Size(148, 17);
            this.cBbotDisTurbulent.TabIndex = 11;
            this.cBbotDisTurbulent.Text = "Turbulent Boundary Layer";
            this.cBbotDisTurbulent.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rBbreakDisWinyu);
            this.groupBox2.Controls.Add(this.rBbreakDisBattjes);
            this.groupBox2.Controls.Add(this.rBbreakDisThornton);
            this.groupBox2.Location = new System.Drawing.Point(6, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(217, 114);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Breaking Dissipation";
            // 
            // rBbreakDisWinyu
            // 
            this.rBbreakDisWinyu.AutoSize = true;
            this.rBbreakDisWinyu.Location = new System.Drawing.Point(6, 91);
            this.rBbreakDisWinyu.Name = "rBbreakDisWinyu";
            this.rBbreakDisWinyu.Size = new System.Drawing.Size(117, 17);
            this.rBbreakDisWinyu.TabIndex = 2;
            this.rBbreakDisWinyu.Text = "Winyu and Tomoya";
            this.rBbreakDisWinyu.UseVisualStyleBackColor = true;
            // 
            // rBbreakDisBattjes
            // 
            this.rBbreakDisBattjes.AutoSize = true;
            this.rBbreakDisBattjes.Location = new System.Drawing.Point(6, 55);
            this.rBbreakDisBattjes.Name = "rBbreakDisBattjes";
            this.rBbreakDisBattjes.Size = new System.Drawing.Size(120, 17);
            this.rBbreakDisBattjes.TabIndex = 1;
            this.rBbreakDisBattjes.Text = "Battjes and Janssen";
            this.rBbreakDisBattjes.UseVisualStyleBackColor = true;
            // 
            // rBbreakDisThornton
            // 
            this.rBbreakDisThornton.AutoSize = true;
            this.rBbreakDisThornton.Location = new System.Drawing.Point(6, 19);
            this.rBbreakDisThornton.Name = "rBbreakDisThornton";
            this.rBbreakDisThornton.Size = new System.Drawing.Size(117, 17);
            this.rBbreakDisThornton.TabIndex = 0;
            this.rBbreakDisThornton.Text = "Thornton and Guza";
            this.rBbreakDisThornton.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.rBModelStokes);
            this.groupBox11.Controls.Add(this.rBModelComposed);
            this.groupBox11.Controls.Add(this.rBModelLinear);
            this.groupBox11.Location = new System.Drawing.Point(6, 19);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(440, 50);
            this.groupBox11.TabIndex = 10;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Type";
            // 
            // rBModelStokes
            // 
            this.rBModelStokes.AutoSize = true;
            this.rBModelStokes.Location = new System.Drawing.Point(337, 19);
            this.rBModelStokes.Name = "rBModelStokes";
            this.rBModelStokes.Size = new System.Drawing.Size(58, 17);
            this.rBModelStokes.TabIndex = 2;
            this.rBModelStokes.Text = "Stokes";
            this.rBModelStokes.UseVisualStyleBackColor = true;
            // 
            // rBModelComposed
            // 
            this.rBModelComposed.AutoSize = true;
            this.rBModelComposed.Location = new System.Drawing.Point(191, 19);
            this.rBModelComposed.Name = "rBModelComposed";
            this.rBModelComposed.Size = new System.Drawing.Size(75, 17);
            this.rBModelComposed.TabIndex = 1;
            this.rBModelComposed.Text = "Composed";
            this.rBModelComposed.UseVisualStyleBackColor = true;
            // 
            // rBModelLinear
            // 
            this.rBModelLinear.AutoSize = true;
            this.rBModelLinear.Location = new System.Drawing.Point(45, 19);
            this.rBModelLinear.Name = "rBModelLinear";
            this.rBModelLinear.Size = new System.Drawing.Size(54, 17);
            this.rBModelLinear.TabIndex = 0;
            this.rBModelLinear.Text = "Linear";
            this.rBModelLinear.UseVisualStyleBackColor = true;
            // 
            // gBModel
            // 
            this.gBModel.Controls.Add(this.groupBox11);
            this.gBModel.Controls.Add(this.groupBox10);
            this.gBModel.Controls.Add(this.groupBox2);
            this.gBModel.Controls.Add(this.groupBox8);
            this.gBModel.Enabled = false;
            this.gBModel.Location = new System.Drawing.Point(312, 98);
            this.gBModel.Name = "gBModel";
            this.gBModel.Size = new System.Drawing.Size(460, 256);
            this.gBModel.TabIndex = 14;
            this.gBModel.TabStop = false;
            this.gBModel.Text = "Model";
            // 
            // gBSpectrum
            // 
            this.gBSpectrum.Controls.Add(this.tBTMASpecGamma);
            this.gBSpectrum.Controls.Add(this.label16);
            this.gBSpectrum.Controls.Add(this.tBDisPar);
            this.gBSpectrum.Controls.Add(this.label20);
            this.gBSpectrum.Enabled = false;
            this.gBSpectrum.Location = new System.Drawing.Point(312, 12);
            this.gBSpectrum.Name = "gBSpectrum";
            this.gBSpectrum.Size = new System.Drawing.Size(225, 80);
            this.gBSpectrum.TabIndex = 15;
            this.gBSpectrum.TabStop = false;
            this.gBSpectrum.Text = "Spectrum";
            // 
            // tBTMASpecGamma
            // 
            this.tBTMASpecGamma.Location = new System.Drawing.Point(131, 28);
            this.tBTMASpecGamma.Name = "tBTMASpecGamma";
            this.tBTMASpecGamma.Size = new System.Drawing.Size(88, 20);
            this.tBTMASpecGamma.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Gamma";
            // 
            // tBDisPar
            // 
            this.tBDisPar.Location = new System.Drawing.Point(131, 54);
            this.tBDisPar.Name = "tBDisPar";
            this.tBDisPar.Size = new System.Drawing.Size(88, 20);
            this.tBDisPar.TabIndex = 14;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "Dispersion Par. (º)";
            // 
            // gbCopla
            // 
            this.gbCopla.Controls.Add(this.tBCopNumIter);
            this.gbCopla.Controls.Add(this.label27);
            this.gbCopla.Enabled = false;
            this.gbCopla.Location = new System.Drawing.Point(547, 12);
            this.gbCopla.Name = "gbCopla";
            this.gbCopla.Size = new System.Drawing.Size(225, 80);
            this.gbCopla.TabIndex = 16;
            this.gbCopla.TabStop = false;
            this.gbCopla.Text = "Copla";
            // 
            // tBCopNumIter
            // 
            this.tBCopNumIter.Location = new System.Drawing.Point(131, 28);
            this.tBCopNumIter.Name = "tBCopNumIter";
            this.tBCopNumIter.Size = new System.Drawing.Size(88, 20);
            this.tBCopNumIter.TabIndex = 14;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 31);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 13);
            this.label27.TabIndex = 13;
            this.label27.Text = "Nº of Iterations";
            // 
            // listViewEditCases
            // 
            this.listViewEditCases.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewEditCases.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCODE,
            this.colSTATUS});
            this.listViewEditCases.FullRowSelect = true;
            this.listViewEditCases.GridLines = true;
            this.listViewEditCases.HideSelection = false;
            this.listViewEditCases.Location = new System.Drawing.Point(12, 12);
            this.listViewEditCases.Name = "listViewEditCases";
            this.listViewEditCases.Size = new System.Drawing.Size(247, 464);
            this.listViewEditCases.TabIndex = 17;
            this.listViewEditCases.UseCompatibleStateImageBehavior = false;
            this.listViewEditCases.View = System.Windows.Forms.View.Details;
            this.listViewEditCases.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.listViewEditCases_ColumnWidthChanging);
            this.listViewEditCases.Click += new System.EventHandler(this.listViewEditCases_Click);
            // 
            // colCODE
            // 
            this.colCODE.Text = "CODE";
            this.colCODE.Width = 120;
            // 
            // colSTATUS
            // 
            this.colSTATUS.Text = "STATUS";
            this.colSTATUS.Width = 120;
            // 
            // btnCancel
            // 
            this.btnCancel.Image = global::MOPLA_Launcher.Properties.Resources.delete16;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(672, 452);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 24);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveCase
            // 
            this.btnSaveCase.Image = global::MOPLA_Launcher.Properties.Resources.check16;
            this.btnSaveCase.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveCase.Location = new System.Drawing.Point(566, 452);
            this.btnSaveCase.Name = "btnSaveCase";
            this.btnSaveCase.Size = new System.Drawing.Size(100, 24);
            this.btnSaveCase.TabIndex = 18;
            this.btnSaveCase.Text = "Save";
            this.btnSaveCase.UseVisualStyleBackColor = true;
            this.btnSaveCase.Click += new System.EventHandler(this.btnSaveCase_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rBacUserSel);
            this.groupBox5.Controls.Add(this.rBacUncCases);
            this.groupBox5.Controls.Add(this.rBacAllCases);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(547, 360);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(225, 86);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Apply Changes";
            // 
            // rBacUserSel
            // 
            this.rBacUserSel.AutoSize = true;
            this.rBacUserSel.Checked = true;
            this.rBacUserSel.Location = new System.Drawing.Point(12, 63);
            this.rBacUserSel.Name = "rBacUserSel";
            this.rBacUserSel.Size = new System.Drawing.Size(108, 17);
            this.rBacUserSel.TabIndex = 4;
            this.rBacUserSel.TabStop = true;
            this.rBacUserSel.Text = "User Selection";
            this.rBacUserSel.UseVisualStyleBackColor = true;
            this.rBacUserSel.Click += new System.EventHandler(this.rBacUserSel_Click);
            // 
            // rBacUncCases
            // 
            this.rBacUncCases.AutoSize = true;
            this.rBacUncCases.Location = new System.Drawing.Point(12, 19);
            this.rBacUncCases.Name = "rBacUncCases";
            this.rBacUncCases.Size = new System.Drawing.Size(137, 17);
            this.rBacUncCases.TabIndex = 3;
            this.rBacUncCases.Text = "Uncompleted Cases";
            this.rBacUncCases.UseVisualStyleBackColor = true;
            this.rBacUncCases.Click += new System.EventHandler(this.rBacUncCases_Click);
            // 
            // rBacAllCases
            // 
            this.rBacAllCases.AutoSize = true;
            this.rBacAllCases.Location = new System.Drawing.Point(12, 41);
            this.rBacAllCases.Name = "rBacAllCases";
            this.rBacAllCases.Size = new System.Drawing.Size(77, 17);
            this.rBacAllCases.TabIndex = 2;
            this.rBacAllCases.Text = "All Cases";
            this.rBacAllCases.UseVisualStyleBackColor = true;
            this.rBacAllCases.Click += new System.EventHandler(this.rBacAllCases_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cBChangesModel);
            this.groupBox6.Controls.Add(this.cBchangesSpectrum);
            this.groupBox6.Controls.Add(this.cBChangesCopla);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(312, 360);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(225, 86);
            this.groupBox6.TabIndex = 21;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Save Options";
            // 
            // cBChangesModel
            // 
            this.cBChangesModel.AutoSize = true;
            this.cBChangesModel.Location = new System.Drawing.Point(12, 65);
            this.cBChangesModel.Name = "cBChangesModel";
            this.cBChangesModel.Size = new System.Drawing.Size(60, 17);
            this.cBChangesModel.TabIndex = 15;
            this.cBChangesModel.Text = "Model";
            this.cBChangesModel.UseVisualStyleBackColor = true;
            this.cBChangesModel.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cBchangesSpectrum
            // 
            this.cBchangesSpectrum.AutoSize = true;
            this.cBchangesSpectrum.Location = new System.Drawing.Point(12, 19);
            this.cBchangesSpectrum.Name = "cBchangesSpectrum";
            this.cBchangesSpectrum.Size = new System.Drawing.Size(79, 17);
            this.cBchangesSpectrum.TabIndex = 13;
            this.cBchangesSpectrum.Text = "Spectrum";
            this.cBchangesSpectrum.UseVisualStyleBackColor = true;
            this.cBchangesSpectrum.CheckedChanged += new System.EventHandler(this.cBchangesSpectrum_CheckedChanged);
            // 
            // cBChangesCopla
            // 
            this.cBChangesCopla.AutoSize = true;
            this.cBChangesCopla.Location = new System.Drawing.Point(12, 42);
            this.cBChangesCopla.Name = "cBChangesCopla";
            this.cBChangesCopla.Size = new System.Drawing.Size(58, 17);
            this.cBChangesCopla.TabIndex = 14;
            this.cBChangesCopla.Text = "Copla";
            this.cBChangesCopla.UseVisualStyleBackColor = true;
            this.cBChangesCopla.CheckedChanged += new System.EventHandler(this.cBChangesCopla_CheckedChanged);
            // 
            // FormMassParEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 482);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSaveCase);
            this.Controls.Add(this.listViewEditCases);
            this.Controls.Add(this.gbCopla);
            this.Controls.Add(this.gBSpectrum);
            this.Controls.Add(this.gBModel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormMassParEdit";
            this.ShowInTaskbar = false;
            this.Text = "Global Parameters Edition";
            this.Load += new System.EventHandler(this.FormMassParEdit_Load);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.gBModel.ResumeLayout(false);
            this.gBSpectrum.ResumeLayout(false);
            this.gBSpectrum.PerformLayout();
            this.gbCopla.ResumeLayout(false);
            this.gbCopla.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox cBopenLatBound;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox cBbotDisLaminar;
        private System.Windows.Forms.CheckBox cBbotDisPorous;
        private System.Windows.Forms.CheckBox cBbotDisTurbulent;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rBbreakDisWinyu;
        private System.Windows.Forms.RadioButton rBbreakDisBattjes;
        private System.Windows.Forms.RadioButton rBbreakDisThornton;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton rBModelStokes;
        private System.Windows.Forms.RadioButton rBModelComposed;
        private System.Windows.Forms.RadioButton rBModelLinear;
        private System.Windows.Forms.GroupBox gBModel;
        private System.Windows.Forms.GroupBox gBSpectrum;
        private System.Windows.Forms.TextBox tBTMASpecGamma;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tBDisPar;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox gbCopla;
        private System.Windows.Forms.TextBox tBCopNumIter;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListView listViewEditCases;
        private System.Windows.Forms.ColumnHeader colCODE;
        private System.Windows.Forms.ColumnHeader colSTATUS;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveCase;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rBacUncCases;
        private System.Windows.Forms.RadioButton rBacAllCases;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox cBChangesModel;
        private System.Windows.Forms.CheckBox cBchangesSpectrum;
        private System.Windows.Forms.CheckBox cBChangesCopla;
        private System.Windows.Forms.RadioButton rBacUserSel;
    }
}
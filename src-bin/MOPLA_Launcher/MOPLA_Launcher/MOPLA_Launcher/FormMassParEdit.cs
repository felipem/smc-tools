﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MOPLA_Launcher.Entities;

namespace MOPLA_Launcher
{
    public partial class FormMassParEdit : Form
    {
        private readonly List<MCase> _lstCases;

        public FormMassParEdit(List<MCase> lstCases)
        {
            InitializeComponent();

            // Load Cases to listview
            _lstCases = lstCases;
            foreach (MCase Case in _lstCases)
            {
                listViewEditCases.Items.Add(Case.CaseLvItem.Clone() as ListViewItem);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            gBModel.Enabled = cBChangesModel.Checked;
            if (!cBChangesModel.Checked)
            {
                rBModelComposed.Checked = false;
                rBModelLinear.Checked = false;
                rBModelStokes.Checked = false;

                rBbreakDisBattjes.Checked = false;
                rBbreakDisThornton.Checked = false;
                rBbreakDisWinyu.Checked = false;

                cBbotDisLaminar.Checked = false;
                cBbotDisPorous.Checked = false;
                cBbotDisTurbulent.Checked = false;

                cBopenLatBound.Checked = false;
            }
        }

        private void cBchangesSpectrum_CheckedChanged(object sender, EventArgs e)
        {
            gBSpectrum.Enabled = cBchangesSpectrum.Checked;
            if (!cBchangesSpectrum.Checked)
            {
                tBTMASpecGamma.BackColor = DefaultBackColor;
                tBDisPar.BackColor = DefaultBackColor;

                tBTMASpecGamma.Text = String.Empty;
                tBDisPar.Text = String.Empty;
            }
        }

        private void cBChangesCopla_CheckedChanged(object sender, EventArgs e)
        {
            gbCopla.Enabled = cBChangesCopla.Checked;
            if (!cBChangesCopla.Checked)
            {
                tBCopNumIter.BackColor = DefaultBackColor;
                tBCopNumIter.Text = String.Empty;
            }
        }

        private static void SizeLastColumn(ListView lv)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
            lv.Columns[lv.Columns.Count - 1].Width -= 1;
        }

        private void listViewEditCases_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = listViewEditCases.Columns[e.ColumnIndex].Width;
        }

        private void FormMassParEdit_Load(object sender, EventArgs e)
        {
            SizeLastColumn(listViewEditCases);
        }

        private void rBacUncCases_Click(object sender, EventArgs e)
        {
            listViewEditCases.Focus();
            foreach (ListViewItem lVitem in listViewEditCases.Items)
            {
                lVitem.Selected = lVitem.SubItems[1].Text != GetEnumDesc.GetEnumDescription(enumCaseStatus.Completed);
            }
        }

        private void rBacAllCases_Click(object sender, EventArgs e)
        {
            listViewEditCases.Focus();
            foreach (ListViewItem lVitem in listViewEditCases.Items)
            {
                lVitem.Selected = true;
            }
        }

        private void rBacUserSel_Click(object sender, EventArgs e)
        {
            listViewEditCases.Focus();
        }

        private void listViewEditCases_Click(object sender, EventArgs e)
        {
            rBacUserSel.Checked = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSaveCase_Click(object sender, EventArgs e)
        {
            if (!cBchangesSpectrum.Checked && !cBChangesCopla.Checked && !cBChangesModel.Checked)
            {
                return;
            }

            if (CorrrectUserInput())
            {
                List<MCase> lstCasesToEdit = listViewEditCases.SelectedItems.Cast<ListViewItem>().Select(caseQueueItem => _lstCases.Find(algo => algo.CaseParameters.Code.Equals(caseQueueItem.Text))).ToList();

                foreach (MCase mCase in lstCasesToEdit)
                {
                    //TODO: MIRAR UNO A UNO, CUANDO NO HAY Q GUARDARLO SI ESTAN VACIOS (USUARIO PASA)
                    // CASESPECTRUM PARAMETERS
                    if (!tBTMASpecGamma.Text.Equals(string.Empty))
                    {
                        mCase.CaseSpectrum.TmaSpecGamma = Convert.ToSingle(tBTMASpecGamma.Text);
                    }
                    if (!tBDisPar.Text.Equals(string.Empty))
                    {
                        mCase.CaseSpectrum.DirSpecDispersion = Convert.ToSingle(tBDisPar.Text);
                    }

                    // Model Linearity
                    if (rBModelLinear.Checked)
                    {
                        mCase.CaseSpectrum.Linearity = enumModelLinearity.Linear;
                    }
                    else if (rBModelComposed.Checked)
                    {
                        mCase.CaseSpectrum.Linearity = enumModelLinearity.Composed;
                    }
                    else if (rBModelStokes.Checked)
                    {
                        mCase.CaseSpectrum.Linearity = enumModelLinearity.Stokes;
                    }

                    // Bottom Dissipation
                    mCase.CaseSpectrum.BottomDissipationModel.isLaminar = cBbotDisLaminar.Checked;
                    mCase.CaseSpectrum.BottomDissipationModel.isPorous = cBbotDisPorous.Checked;
                    mCase.CaseSpectrum.BottomDissipationModel.isTurbulent = cBbotDisTurbulent.Checked;

                    // Breaking Dissipation
                    if (rBbreakDisThornton.Checked)
                    {
                        mCase.CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Thornton;
                    }
                    else if (rBbreakDisBattjes.Checked)
                    {
                        mCase.CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Battjes;
                    }
                    else if (rBbreakDisWinyu.Checked)
                    {
                        mCase.CaseSpectrum.BreakingDissipationModel = enumBreakingDissipationModel.Winyu;
                    }

                    // Open Boundaries
                    mCase.CaseSpectrum.ContoursOpen = cBopenLatBound.Checked;

                    // COPLA PARAMETERS
                    if (mCase.CoplaParameters.ExecuteCopla && !tBCopNumIter.Text.Equals(string.Empty))
                    {
                        mCase.CoplaParameters.Iterations = Convert.ToInt32(tBCopNumIter.Text);
                        mCase.CoplaParameters.TotalTime = mCase.CoplaParameters.Interval * mCase.CoplaParameters.Iterations;
                    }

                    // Save case to enc, in2, cop2 files.
                    mCase.SaveCase();
                }

                // Close form
                Close();
            }
        }

        private bool CorrrectUserInput()
        {
            CleanTextBoxColors();

            bool boolRet = true;
            float auxFloat;
            int auxInt;

            if (!Single.TryParse(tBTMASpecGamma.Text, out auxFloat) && !tBTMASpecGamma.Text.Equals(String.Empty))
            {
                tBTMASpecGamma.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!Single.TryParse(tBDisPar.Text, out auxFloat) && !tBDisPar.Text.Equals(String.Empty))
            {
                tBDisPar.BackColor = Color.Yellow;
                boolRet = false;
            }

            if (!int.TryParse(tBCopNumIter.Text, out auxInt) && !tBCopNumIter.Text.Equals(String.Empty))
            {
                tBCopNumIter.BackColor = Color.Yellow;
                boolRet = false;
            }

            return boolRet;
        }

        private void CleanTextBoxColors()
        {
            tBTMASpecGamma.BackColor = DefaultBackColor;
            tBDisPar.BackColor = DefaultBackColor;
            tBCopNumIter.BackColor = DefaultBackColor;
        }
    }
}

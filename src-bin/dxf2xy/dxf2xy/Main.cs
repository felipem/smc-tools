using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using netDxf;
using System.Globalization;
using System.Threading;



namespace dxf2xy
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string input=args[0];			
			string output=args[1];			
			
			//Establece en la aplicatión el "." como separador decimal
            CultureInfo cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = cultureInfo;
			
			
			netDxf.DxfDocument dxfDoc = new netDxf.DxfDocument();
            dxfDoc.Load(input);
			StreamWriter sw=new StreamWriter(output);
			int coastId=1;
			foreach (netDxf.Entities.IPolyline polyline in dxfDoc.Polylines)
            {
                
                if (polyline.GetType() == typeof(netDxf.Entities.Polyline))
                {
                    foreach (netDxf.Entities.PolylineVertex vertex in ((netDxf.Entities.Polyline)polyline).Vertexes)
                    {                                              
						sw.WriteLine(vertex.Location.X.ToString()+" "+
							vertex.Location.Y+" " + coastId.ToString());						
                    }
                }
                else
                {
                    foreach (netDxf.Entities.Polyline3dVertex vertex in ((netDxf.Entities.Polyline3d)polyline).Vertexes)
                    {
                        sw.WriteLine(vertex.Location.X.ToString()+" "+
							vertex.Location.Y+" " + coastId.ToString());												
                    }
                }
                coastId++;
            }
			sw.Close();
			
		}
	}
}

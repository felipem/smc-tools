#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstdlib> 
#include <math.h> 
#include <stdlib.h>




bool ArrayContainsValue(int* Array,int length,int value)
{
	for (int i=0;i<length;i++)
	{
		if (Array[i]==value)
		{
			return true;
		}
	}
	return false;
}

extern "C" __declspec(dllexport) void  testMatlab(double* a,int n,int* out)
{
	double rt=0;
	for (int i=0;i<n;i++)
	{
		rt=rt+a[i];
	}
	out[0]=(int)rt;
	out[1]=(int)5;
	//return rt;
}

extern "C" __declspec(dllexport) void  GetSignificantIndexesByMaxDiss(int nCases,double* standardData,int rows,int columns,int primerValor,int* selectedIndex)
{	  
	        //int* selectedIndex=new int[nCases];
			selectedIndex[0]=primerValor;
	        double distance = 0;
            double tempDistance = 0;
            int tempIndex = 0;
            double* lastDistance = new double[rows];
			
			

            //////////////////Calula la distancia al primer elemento
            for (int i = 0; i < rows; i++)
            {
                if (i != primerValor)
                {
                    tempDistance = 0;
                    for (int j = 0; j < columns; j++)
                    {
						tempDistance += pow(standardData[i*columns+j] - standardData[primerValor*columns+ j],2);

                    }                    
                    lastDistance[i] = tempDistance;
                    if (tempDistance > distance)
                    {
                        distance = tempDistance;
                        tempIndex = i;
                    }
                }
            }			
            ////////////////////////////////////////////////////////

			selectedIndex[1]=tempIndex;
			

			for (int n=2;n<nCases;n++)
			{
				distance = 0;
                for (int i = 0; i < rows; i++)
                {
                    if (!ArrayContainsValue(selectedIndex,n,i))
                    {
                        tempDistance = 0;
                        for (int j = 0; j < columns; j++)
                        {
                            tempDistance += pow(standardData[i*columns+ j] - standardData[columns*selectedIndex[n-1]+j], 2);
                        }
						tempDistance = std::min(tempDistance, lastDistance[i]);
                        lastDistance[i] = tempDistance;
                        if (tempDistance > distance)
                        {
                            distance = tempDistance;
                            tempIndex = i;
                        }
                    }
                }
               selectedIndex[n]=tempIndex;
			}
			delete lastDistance;

			//return selectedIndex;
	  
}
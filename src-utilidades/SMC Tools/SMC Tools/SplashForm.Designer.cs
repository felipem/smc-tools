﻿namespace SMC_Tools
{
    partial class SplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashForm));
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.toolTipInit = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipUpdate = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipImportDB = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.bQuit = new System.Windows.Forms.Button();
            this.bImportDB = new System.Windows.Forms.Button();
            this.bUpdate = new System.Windows.Forms.Button();
            this.bInit = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lStatus = new System.Windows.Forms.Label();
            this.pbSplash = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSplash)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.bQuit);
            this.panel1.Controls.Add(this.bImportDB);
            this.panel1.Controls.Add(this.bUpdate);
            this.panel1.Controls.Add(this.bInit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 400);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(602, 51);
            this.panel1.TabIndex = 4;
            // 
            // bQuit
            // 
            this.bQuit.Location = new System.Drawing.Point(180, 9);
            this.bQuit.Name = "bQuit";
            this.bQuit.Size = new System.Drawing.Size(75, 33);
            this.bQuit.TabIndex = 8;
            this.bQuit.Text = "Salir";
            this.bQuit.UseVisualStyleBackColor = true;
            this.bQuit.Click += new System.EventHandler(this.bQuit_Click);
            // 
            // bImportDB
            // 
            this.bImportDB.Image = global::SMC_Tools.Properties.Resources.data_up;
            this.bImportDB.Location = new System.Drawing.Point(514, 9);
            this.bImportDB.Name = "bImportDB";
            this.bImportDB.Size = new System.Drawing.Size(75, 33);
            this.bImportDB.TabIndex = 7;
            this.bImportDB.UseVisualStyleBackColor = true;
            this.bImportDB.Click += new System.EventHandler(this.bImportDB_Click);
            // 
            // bUpdate
            // 
            this.bUpdate.Image = global::SMC_Tools.Properties.Resources.element_up;
            this.bUpdate.Location = new System.Drawing.Point(347, 9);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(75, 33);
            this.bUpdate.TabIndex = 6;
            this.bUpdate.UseVisualStyleBackColor = true;
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // bInit
            // 
            this.bInit.Location = new System.Drawing.Point(13, 9);
            this.bInit.Name = "bInit";
            this.bInit.Size = new System.Drawing.Size(75, 33);
            this.bInit.TabIndex = 5;
            this.bInit.Text = "Iniciar";
            this.bInit.UseVisualStyleBackColor = true;
            this.bInit.Click += new System.EventHandler(this.bInit_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(14, 358);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(576, 23);
            this.progressBar.TabIndex = 5;
            this.progressBar.Visible = false;
            // 
            // lStatus
            // 
            this.lStatus.AutoSize = true;
            this.lStatus.BackColor = System.Drawing.Color.Transparent;
            this.lStatus.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lStatus.Location = new System.Drawing.Point(142, 300);
            this.lStatus.Name = "lStatus";
            this.lStatus.Size = new System.Drawing.Size(114, 41);
            this.lStatus.TabIndex = 6;
            this.lStatus.Text = "label1";
            this.lStatus.Visible = false;
            // 
            // pbSplash
            // 
            this.pbSplash.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSplash.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbSplash.Location = new System.Drawing.Point(0, 0);
            this.pbSplash.Name = "pbSplash";
            this.pbSplash.Size = new System.Drawing.Size(602, 400);
            this.pbSplash.TabIndex = 3;
            this.pbSplash.TabStop = false;
            // 
            // SplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(602, 451);
            this.ControlBox = false;
            this.Controls.Add(this.lStatus);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pbSplash);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMC Tools";
            this.Load += new System.EventHandler(this.SplashForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbSplash)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolTip toolTipInit;
        private System.Windows.Forms.ToolTip toolTipUpdate;
        private System.Windows.Forms.ToolTip toolTipImportDB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bQuit;
        private System.Windows.Forms.Button bImportDB;
        private System.Windows.Forms.Button bUpdate;
        private System.Windows.Forms.Button bInit;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lStatus;
        private System.Windows.Forms.PictureBox pbSplash;
    }
}


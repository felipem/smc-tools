﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;


namespace SMC_Tools
{
    public partial class SplashForm : Form
    {
        string key = "";
        string keyFile = "";

        Dictionary<string, string> captions=new Dictionary<string,string>();
        Dictionary<string, string> config = new Dictionary<string, string>();

        public SplashForm()
        {
            InitializeComponent();
            
        }

        private void SplashForm_Load(object sender, EventArgs e)
        {
            
            this.loadConfig();
            this.loadCaptions();

            this.toolTipInit.SetToolTip(this.bInit,this.captions["ttInit"]);
            this.toolTipInit.SetToolTip(this.bUpdate, this.captions["ttUpdate"]);
            this.toolTipInit.SetToolTip(this.bImportDB, this.captions["ttImportDB"]);
            this.toolTipInit.SetToolTip(this.bQuit, this.captions["ttQuit"]);
            this.bInit.Text = this.captions["bInit"];
            this.bQuit.Text = this.captions["bQuit"];
            this.pbSplash.Image = Image.FromFile(Path.Combine(Application.StartupPath, "icon", this.config["region"], "splash.png"));
            this.pbSplash.SizeMode = PictureBoxSizeMode.StretchImage;
            

            if (!this.checkDataBase())
            {
                MessageBox.Show(this.captions["wCheckDBText"], this.captions["wCheckDBTitle"],
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.bInit.Enabled = false;
                this.bUpdate.Enabled = false;
            }
            
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (File.Exists(this.keyFile))
            {
                File.Delete(this.keyFile);
                this.Close();
            }
        }

        private void bInit_Click(object sender, EventArgs e)
        {
            this.bInit.Enabled = false;
            this.bQuit.Enabled = false;
            this.bImportDB.Enabled = false;
            this.bUpdate.Enabled = false;

            this.lStatus.Text = this.captions["lStatusLoading"];
            this.lStatus.Visible = true;
            this.Update();

            this.timer.Enabled = true;
            this.key = DateTime.Now.ToString("yyyyMMhhmmss");
            this.keyFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "smcTools", key + ".key");

            Process process = new Process();
            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            string smcPath = Application.StartupPath;
            //string smcPath = @"Z:\SMCTools";
            string smcToolsExe = Path.Combine(smcPath, "smcTools.exe");
            processStartInfo.FileName = smcToolsExe;

            processStartInfo.Arguments = this.key;
            processStartInfo.CreateNoWindow = true;
            process.StartInfo = processStartInfo;
            processStartInfo.WorkingDirectory = smcPath;
            try
            {
                process.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this.captions["wInitError"], "error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void loadCaptions()
        {
            string captionFile = Path.Combine(Application.StartupPath,
                "texts",this.config["language"],"splash.txt");

            if (File.Exists(captionFile))
            {
                StreamReader sr=new StreamReader(captionFile);                
                string line=sr.ReadLine();
                while (!String.IsNullOrEmpty(line))
                {
                    string[] caption=line.Split(new char[]{'='},StringSplitOptions.RemoveEmptyEntries);
                    this.captions.Add(caption[0], caption[1]);
                    line=sr.ReadLine();
                }
                sr.Close();
            }

        }

        private void loadConfig()
        {
            string configFile = Path.Combine(Application.StartupPath,
              "smcTools.cfg");

            if (File.Exists(configFile))
            {
                StreamReader sr = new StreamReader(configFile);
                string line = sr.ReadLine();
                while (!String.IsNullOrEmpty(line))
                {
                    string[] cfg = line.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    string value="";
                    if (cfg.Length > 1)
                        value = cfg[1];
                    
                    this.config.Add(cfg[0], value);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
        }

        private void bQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bImportDB_Click(object sender, EventArgs e)
        {
            this.disableButtons();

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "smcTools.db";
            ofd.Title = this.captions["wImportDBTitle"];
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                List<FileInfo> files = this.getDataFiles(Path.GetDirectoryName(ofd.FileName));
                if (files.Count > 1)
                {
                    if (MessageBox.Show(this.captions["wImportDBContinue"], "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        string error=this.importDataBase(files);
                        if (!String.IsNullOrEmpty(error))
                        {
                            MessageBox.Show(error,"error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                        }
                    }
                }
            }

            this.enableButtons();
        }

        private List<FileInfo> getDataFiles(string folder)
        {
            List<FileInfo> files = new List<FileInfo>();
            string[] dataDirectories=Directory.GetDirectories(folder);
            foreach (string directory in dataDirectories)
            {
                string[] dirFiles = Directory.GetFiles(directory);
                foreach (string file in dirFiles)
                {
                    files.Add(new FileInfo(file));
                }
                
            }
            files.Add(new FileInfo(Path.Combine(folder,"smcTools.db")));


            return files;
        }

        private string importDataBase(List<FileInfo> files)
        {
            string error="";
            this.progressBar.Visible = true;
            double totalSize = files.Select(f => f.Length).Sum();
            double currentSize = 0;
            string destFolder=Path.Combine(Application.StartupPath,"data",this.config["region"]);
            

            this.lStatus.Text = this.captions["lStatusImport"];
            this.lStatus.Visible = true;
            this.Update();
                
            foreach (FileInfo file in files)
            {
                string srcFile = file.FullName;
                string folder=destFolder;
                if (file.Name != "smcTools.db")
                {
                    //continue;
                    folder = Path.Combine(destFolder, Path.GetFileName(Path.GetDirectoryName(srcFile)));
                }

              

                string dstFile = Path.Combine(folder, file.Name);
                try
                {
                    if (!Directory.Exists(destFolder))
                        Directory.CreateDirectory(destFolder);

                    if (!Directory.Exists(folder))
                        Directory.CreateDirectory(folder);
                    //MessageBox.Show(srcFile + "\n" + dstFile);
                    File.Copy(srcFile, dstFile, true);
                }
                catch (Exception ex)
                {
                    if (typeof(System.UnauthorizedAccessException) == ex.GetType())
                    {
                        error = this.captions["wAdministratorRequired"];
                    }
                    else
                    {
                        error = ex.Message;
                    }
                    
                    this.progressBar.Visible = false;
                    this.lStatus.Visible = false;
                    return error;
                    
                }

                currentSize += file.Length;
                this.progressBar.Value = (int)(currentSize * 100 / totalSize);
                this.progressBar.Update();
                this.Update();
            }
            this.progressBar.Visible = false;
            this.lStatus.Visible = false;
            return error;
        }

        private void bUpdate_Click(object sender, EventArgs e)
        {
            this.disableButtons();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Zip Files (*.zip) | *.zip";
            ofd.Title = this.captions["wUpdateTitle"];
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string[] contents = this.checkUpdate(ofd.FileName);
                if (contents==null)
                    MessageBox.Show(this.captions["wUpdateNoFound"],
                        this.captions["wUpdateNoFoundTitle"],
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (MessageBox.Show(
                       this.captions["wUpdateContinue"]+" (" +contents[0]+")",
                       this.captions["wUpdateFound"],
                       MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        string error=this.update(ofd.FileName, contents);
                        if (String.IsNullOrEmpty(error))
                        {
                            MessageBox.Show(this.captions["wUpdateSuccess"],"", 
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show(this.captions["wUpdateError"] + Environment.NewLine + error,
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }
            }

            this.enableButtons();
        }

        private string[] checkUpdate(string updateFile)
        {
            ZipFile zip = new ICSharpCode.SharpZipLib.Zip.ZipFile(updateFile);
            ZipEntry zipEntry= zip.GetEntry("smcUpdate.txt");
            if (zipEntry == null)
            {
                zip.Close();
                return null;
            }
            else
            {
                StreamReader sr = new StreamReader(zip.GetInputStream(zipEntry));
                var ret = sr.ReadToEnd().Split(new String[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                sr.Close();
                zip.Close();
                return ret;
            }
        }

        private string update(string updateFile,string[] updates)
        {
            ZipFile zip = new ICSharpCode.SharpZipLib.Zip.ZipFile(updateFile);
            string outFolder=Path.GetDirectoryName(Application.StartupPath);
            this.progressBar.Visible = true;
            string error = "";
            this.lStatus.Text = this.captions["lStatusUpdate"];
            this.lStatus.Visible = true;
            this.Update();
            try
            {
                for (int i = 1; i < updates.Length; i++)
                {
                    ZipEntry zipEntry = zip.GetEntry(updates[i]);
                    Stream zipStream = zip.GetInputStream(zipEntry);
                    byte[] buffer = new byte[4096];
                    String fullZipToPath = Path.Combine(outFolder, zipEntry.Name);
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                    this.progressBar.Value = (i * 100 /updates.Length);
                }
            }
            catch (Exception ex)
            {
                if (typeof(System.UnauthorizedAccessException) == ex.GetType())
                {
                    error = this.captions["wAdministratorRequired"];
                }
                else
                {
                    error = ex.Message;
                }
                    
            }
            finally
            {
                this.progressBar.Visible = false;
                this.lStatus.Visible = false;
                if (zip != null)
                {
                    zip.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zip.Close(); // Ensure we release resources
                }
                
            }

            return error;
        }

        private void disableButtons()
        {
            this.bInit.Enabled = false;
            this.bQuit.Enabled = false;
            this.bImportDB.Enabled = false;
            this.bUpdate.Enabled = false;
        }

        private void enableButtons()
        {
            this.bInit.Enabled = true;
            this.bQuit.Enabled = true;
            this.bImportDB.Enabled = true;
            this.bUpdate.Enabled = true;
        }

        private bool checkDataBase()
        {
            string dbFile = Path.Combine(Application.StartupPath, "data",this.config["region"], "smcTools.db");
            return File.Exists(dbFile);
            
        }
    }
}

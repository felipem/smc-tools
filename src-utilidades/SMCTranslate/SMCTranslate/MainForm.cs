﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace SMCTranslate
{
    public partial class MainForm : Form
    {
        private DataSet data=new DataSet();
        private DataTable guiTable=new DataTable("GUI");
        private DataTable msgTable = new DataTable("Mensajes");
        private string folder = "";
        private List<string> languages=new List<string>();

        public MainForm()
        {
            InitializeComponent();
        }

        public enum Context
        {
            GUI,
            Mensajes
        }

        public class FileInfo
        {
            public string[] File
            {
                get
                {
                    if (this._context==Context.GUI)
                    
                        return new string[]
                                   {
                                       Path.Combine(this._folder, "Translation.xml"),
                                       Path.Combine(this._folder, "MOPLA_Translation.xml")
                                   };
                    else
                        return new string[]
                                   {
                                       Path.Combine(this._folder, "Strings.xml"),
                                       Path.Combine(this._folder, "Mopla_Strings.xml")
                                   };
                }

            }

            public string[] Root
            {
                get { return new string[] {"IHBox","IHBox_Mopla"}; }
            }
            public bool _HasChildren
            {
                get
                {
                    if (this._context == Context.GUI)
                        return true;
                    else
                        return
                            false;
                }
            }
            
            private Context _context;
            private string _folder;

            public FileInfo(Context context,string folder)
            {
                this._context = context;
                this._folder = folder;
            }
        }
        
        private void LoadDataGui()
        {
            var fileInfo = new FileInfo(Context.GUI, this.folder);
            var table = this.guiTable;
            table.Clear();
            table.Rows.Clear();
            table.Columns.Clear();
            

            var doc = new XmlDocument();
            var rowInit = 0;
            for (int i = 0; i < fileInfo.File.Length; i++)
            {
                doc.Load(fileInfo.File[i]);
                var root = doc.GetElementsByTagName(fileInfo.Root[i])[0];
                int index = 0;
                if (i == 0)
                    this.languages=new List<string>();

                foreach (XmlNode language in root.ChildNodes)
                {
                    var languageId = language.Name;
                    if (index == 0)
                    {
                        if (i == 0)
                            table.Columns.Add("ID");
                        foreach (XmlNode form in language.ChildNodes)
                        {
                            var id = form.Name;
                            foreach (XmlNode str in form.ChildNodes)
                            {
                                table.Rows.Add(new string[] {id + ";" + str.Name + ";" + i.ToString()});
                            }
                        }
                    }
                    if (i == 0)
                    {
                        rowInit = table.Rows.Count;
                        this.languages.Add(languageId);
                        table.Columns.Add(languageId);
                        table.Columns.Add(languageId + "_hint");
                    }
                    index += 2;

                }

                index = 1;
                foreach (XmlNode language in root.ChildNodes)
                {
                    var languageId = language.Name;
                    int row = 0;
                    if (i > 0) row += rowInit;
                    foreach (XmlNode form in language.ChildNodes)
                    {
                        foreach (XmlNode str in form.ChildNodes)
                        {
                            var caption = str.Attributes["Caption"].Value;
                            var hint = str.Attributes["Hint"].Value;
                            table.Rows[row][index] = caption;
                            table.Rows[row][index+1] = hint;
                            row += 1;
                        }
                    }
                    
                    index += 2;
                }
            }
        }

        private void LoadData()
        {
            var fileInfo = new FileInfo(Context.Mensajes, this.folder);
            var table = this.msgTable;
            table.Clear();
            table.Rows.Clear();
            table.Columns.Clear();

            var doc = new XmlDocument();
            var rowInit = 0;
            for (int i = 0; i < fileInfo.File.Length; i++)
            {
                doc.Load(fileInfo.File[i]);
                var root = doc.GetElementsByTagName(fileInfo.Root[i])[0];
                int index = 0;

                //this.languages = new List<string>();
                foreach (XmlNode language in root.ChildNodes)
                {
                    var languageId = language.Name;
                    if (index == 0)
                    {
                        if (i == 0)
                            table.Columns.Add("ID");

                        foreach (XmlNode str in language.ChildNodes)
                        {
                            var id = str.Name+";"+i.ToString();
                            table.Rows.Add(new string[] {id});
                        }
                    }

                    if (i == 0)
                    {
                        rowInit = table.Rows.Count;
                        table.Columns.Add(languageId);
                    }
                    index += 1;
                    
                }

                index = 1;
                foreach (XmlNode language in root.ChildNodes)
                {
                    var languageId = language.Name;
                    int row = 0;
                    if (i > 0) row += rowInit;
                    foreach (XmlNode str in language.ChildNodes)
                    {
                        var value = str.Attributes["Str"].Value;
                        table.Rows[row][index] = value;
                        row += 1;
                        //dictionary.Add(id,value);
                    }
                    index += 1;
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

            this.data.Tables.Add(this.guiTable);
            this.data.Tables.Add(this.msgTable);
            this.dgvMain.DataSource = this.data;
            this.timer.Enabled = true;
            this.tscText.SelectedIndex = 0;
            this.tscTranslateContext.SelectedIndex = 0;
            this.tscText.SelectedIndexChanged+=new EventHandler(tscText_SelectedIndexChanged);
            this.tscTranslateContext.SelectedIndexChanged += new EventHandler(tscTranslateContext_SelectedIndexChanged);
            
        }

        private void tsbOpen_Click(object sender, EventArgs e)
        {
            if (this.data.HasChanges())
            {
                if (MessageBox.Show("Hay cambios pendientes, desea grabarlos?","Cambios pendientes",
                    MessageBoxButtons.OKCancel,MessageBoxIcon.Question)==System.Windows.Forms.DialogResult.OK)
                {
                    this.SaveChangesGui();
                    this.SaveChangesMsg();
                }
            }
            FolderBrowserDialog fbd=new FolderBrowserDialog();
            fbd.Description = "Selecciona la carpeta donde están los ficheros de traduccion";
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.folder = fbd.SelectedPath;
                //this.LoadData( Context.GUI);
                //this.LoadData( Context.Mensajes);
                this.LoadDataGui();
                this.LoadData();
                this.data.AcceptChanges();
                this.tscTranslateContext.Enabled = true;
                this.dgvMain.DataMember = this.tscTranslateContext.SelectedItem.ToString();
                this.tscText.Enabled = true;
                this.cbBase.Enabled = true;
                this.cblanguage.Enabled = true;
                this.cbBase.Items.Clear();
                this.cbBase.Items.AddRange(this.languages.ToArray());
                this.cbBase.SelectedIndex = 0;
                this.tsbAddlanguage.Enabled = true;
                this.SetVisibleColumns();

            }
        }

        private void SaveChangesGui()
        {
            var fileInfo = new FileInfo(Context.GUI, this.folder);
            var table = this.guiTable;
            for (int i = 0; i < fileInfo.File.Length; i++)
            {
                var file = fileInfo.File[i];
                var root = fileInfo.Root[i];
                var sw = new StreamWriter(file);
                sw.WriteLine("<" + root + ">");

                var col = 1;
                foreach (var lng in this.languages)
                {
                    sw.WriteLine("\t<" + lng + ">");
                    var form = "";
                    var formClosed = false;
                    
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        var id = table.Rows[j][0].ToString().Split(new char[] { ';' });
                        if (Convert.ToInt32(id[2]) == i)
                        {
                            if (form != id[0])
                            {
                                if (j > 0 && form!="") sw.WriteLine("\t\t</" + form + ">");
                                form = id[0];
                                sw.WriteLine("\t\t<" + form + ">");
                                
                            }

                            var caption = table.Rows[j][col].ToString();
                            caption = caption.Replace("&", "&amp;");
                            caption = caption.Replace("\"", "&quot;");
                            caption = caption.Replace("<", "&lt;");
                            caption = caption.Replace(">", "&gt;");
                            var hint = table.Rows[j][col + 1].ToString();
                            hint = hint.Replace("&", "&amp;");
                            hint = hint.Replace("\"", "&quot;");
                            hint = hint.Replace("<", "&lt;");
                            hint = hint.Replace(">", "&gt;");
                            sw.WriteLine("\t\t\t<" + id[1] + " Caption=\"" + caption + "\" Hint=\"" + hint + "\"" + "/>");
                        }
                        else if (!formClosed && form!="")
                        {
                            //sw.WriteLine("\t\t</" + form + ">");
                            //formClosed = true;
                        }
                    }
                    sw.WriteLine("\t\t</" + form + ">");
                    sw.WriteLine("\t</" + lng + ">");
                    col += 2;
                }
                sw.WriteLine("</" + root + ">");
                sw.Close();
            }
        }

        private void SaveChangesMsg()
        {
            var fileInfo = new FileInfo(Context.Mensajes, this.folder);
            var table = this.msgTable;
            for (int i=0;i<fileInfo.File.Length;i++)
            {
                var file = fileInfo.File[i];
                var root = fileInfo.Root[i];
                var sw = new StreamWriter(file);
                sw.WriteLine("<" + root + ">");
                
                var col = 1;
                foreach (var lng in this.languages)
                {
                    sw.WriteLine("\t<" + lng + ">");
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        var id = table.Rows[j][0].ToString().Split(new char[] {';'});
                        if (Convert.ToInt32(id[1]) == i)
                        {
                            var s = table.Rows[j][col].ToString();
                            s = s.Replace("&", "&amp;");
                            s=s.Replace("\"", "&quot;");
                            s = s.Replace("<", "&lt;");
                            s = s.Replace(">", "&gt;");
                            sw.WriteLine("\t\t<" + id[0] + " Str=\"" + s + "\" Date=\"" + "\"" + "/>");
                        }
                    }
                    sw.WriteLine("\t</" + lng + ">");
                    col += 1;
                }
                sw.WriteLine("</" + root + ">");
                sw.Close();
            }
        }
        
        private void tscTranslateContext_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dgvMain.DataMember = this.tscTranslateContext.SelectedItem.ToString();
            this.SetVisibleColumns();
            var context = (Context) Enum.Parse(typeof (Context), this.tscTranslateContext.SelectedItem.ToString());
            if (context == Context.GUI)
                this.tscText.Enabled = true;
            else
                this.tscText.Enabled = false;
        }

        private void SetVisibleColumns()
        {
            var lenguajeBaseId = this.cbBase.SelectedItem.ToString();
            var lenguajeId = "";
            if (this.cblanguage.SelectedItem != null)
                lenguajeId = this.cblanguage.SelectedItem.ToString();

            var text = this.tscText.SelectedItem.ToString();
            if (text == "Hint")
            {
                lenguajeBaseId += "_hint";
                lenguajeId += "_hint";
            }
            

            foreach (DataGridViewColumn c in this.dgvMain.Columns)
            {
                if (c.HeaderText == lenguajeBaseId)
                {
                    c.Visible = true;
                    c.DisplayIndex = 0;
                    c.ReadOnly = true;
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;
                    foreach (DataGridViewRow row in this.dgvMain.Rows)
                    {
                        if (string.IsNullOrEmpty(row.Cells[c.Index].Value.ToString()))
                            row.Visible = false;
                        else
                        {
                            row.Visible = true;
                        }
                    }
                }
                else if (this.cblanguage.SelectedItem!=null)
                {
                    if (c.HeaderText == lenguajeId)
                    {
                        c.Visible = true;
                        c.DisplayIndex = 1;
                        c.ReadOnly = false;
                        c.SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                    else
                    {
                        c.Visible = false;
                    }
                }
                else
                {
                    c.Visible = false;
                }


            }
            
            //this.dgvMain.AutoResizeColumns();
        }

        private void cblanguage_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.SetVisibleColumns();

        }

        private void cbBase_SelectedIndexChanged(object sender, EventArgs e)
        {

            var lngStrings = this.languages.ToArray();
            var lngStringsList = lngStrings.ToList();
            lngStringsList.Remove(this.cbBase.SelectedItem.ToString());
            this.cblanguage.Items.Clear();
            if (lngStringsList.Count > 0)
            {

                this.cblanguage.Items.AddRange(lngStringsList.ToArray());
                this.cblanguage.SelectedIndex = 0;
            }
            else
            {
                this.SetVisibleColumns();
            }


        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (this.data.HasChanges())
                this.tsbSave.Enabled = true;
            else
            {
                this.tsbSave.Enabled = false;
            }

            if (this.cblanguage.SelectedItem != null)
                this.tsbRemove.Enabled = true;
            else
                this.tsbRemove.Enabled = false;
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            this.SaveChangesGui();
            this.SaveChangesMsg();
            this.data.AcceptChanges();
        }

        private void tscText_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetVisibleColumns();
        }

        private void tsbAddlanguage_Click(object sender, EventArgs e)
        {
            var form = new NewLanguageForm(this.languages);
            if (form.ShowDialog()==DialogResult.OK)
            {
                this.guiTable.Columns.Add(form.Language);
                this.guiTable.Columns.Add(form.Language+"_hint");
                this.msgTable.Columns.Add(form.Language);
                this.languages.Add(form.Language);
                this.cblanguage.Items.Add(form.Language);
                this.cbBase.Items.Add(form.Language);

                var lngBase = this.cbBase.SelectedItem.ToString();
                if (form.Clone)
                {
                    foreach (DataRow dr in this.guiTable.Rows)
                    {
                        dr[form.Language] = dr[lngBase];
                        dr[form.Language + "_hint"] = dr[lngBase + "_hint"];
                    }
                    foreach (DataRow dr in this.msgTable.Rows)
                        dr[form.Language] = dr[lngBase];
                    
                }

                MessageBox.Show("Lenaguaje añadido con exito!!", "Info", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                this.SetVisibleColumns();
            }
        }

        private void tsbRemove_Click(object sender, EventArgs e)
        {
            var language = this.cblanguage.SelectedItem.ToString();
            if (MessageBox.Show("¿Eliminar "+language+" ?","Eliminar",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==
                System.Windows.Forms.DialogResult.Yes)
            {
                this.msgTable.Columns.Remove(language);
                this.guiTable.Columns.Remove(language);
                this.guiTable.Columns.Remove(language+"_hint");
                this.cblanguage.Items.Remove(language);
                this.languages.Remove(language);
                //this.SetVisibleColumns();
            }
        }
    }
}

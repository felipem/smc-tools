﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SMCTranslate
{
    public partial class NewLanguageForm : Form
    {
        private List<string> languages=new List<string>();
        public string Language = "";
        public bool Clone
        {
            get { return this.cbClone.Checked;}
        }
        public NewLanguageForm(List<string> languages)
        {
            InitializeComponent();
            this.languages = languages;
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            this.Language = this.tbLanguage.Text;

            if (!this.languages.Exists(ExistLanguage) && !String.IsNullOrWhiteSpace(this.Language))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();

            }
            else
            {
                MessageBox.Show("El lenguaje ya existe en la colección", "Lenguaje existente", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }


        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private  bool ExistLanguage(string s)
        {
            if (s.ToUpper() == this.Language.ToUpper())

                return true;
            else
                return false;
            
            
        }

        


        
    }
}

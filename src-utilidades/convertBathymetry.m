%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% convert Bathymetry
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datFolder='/home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/Bat_smc-esp';
datFiles=dir(fullfile(datFolder,'*.xyz'));
outFolder=fullfile('data','bathymetry');
dbFile=fullfile('data','smcTools.db');
if ~exist(outFolder,'dir')
    mkdir(outFolder);
end
batRegion=[];
for i=1:length(datFiles)
    datFile=fullfile(datFolder,datFiles(i).name);
    xyz=load(datFile);
    region=datFiles(i).name;
    region=region(1:end-4);
    outFile=fullfile(outFolder,datFiles(i).name);
    batRegion(i).nombre=region;
    batRegion(i).lat=[min(xyz(:,2)),max(xyz(:,2))];
    batRegion(i).lon=[min(xyz(:,1)),max(xyz(:,1))];    
    save(outFile,'xyz','-mat');
end
db=[];
db.batRegion=batRegion;
save(dbFile,'db','-mat');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% linea de costa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
shpFile='/home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/SpCoastline/SpCoastlineWGS84.shp';
S = shaperead(shpFile);

xyRegion=[];
for i=1:length(batRegion)
    xyRegion(i).x=[];
    xyRegion(i).y=[];
    xyRegion(i).index=[];
end

for i=1:length(S)
    X=S(i).X;
    Y=S(i).Y;
    for j=1:length(batRegion)
       index=X>=batRegion(j).lon(1) & X<=batRegion(j).lon(2) & Y>=batRegion(j).lat(1) & Y<=batRegion(j).lat(2); 
       if sum(index)>0
           xyRegion(j).x=cat(1,xyRegion(j).x,X(index)');
           xyRegion(j).y=cat(1,xyRegion(j).y,Y(index)');
           if isempty(xyRegion(j).index)
               cIndex=1;
           else
               cIndex=xyRegion(j).index(end)+1;
           end
           cIndex=ones(sum(index),1)*cIndex;
           xyRegion(j).index=cat(1,xyRegion(j).index,cIndex);
       end
      
    end    
end

for i=1:length(xyRegion)
   xy=[xyRegion(i).x,xyRegion(i).y,xyRegion(i).index];
   outFile=fullfile(outFolder,[batRegion(i).nombre,'.xy']);
   save(outFile,'xy','-mat');   
end

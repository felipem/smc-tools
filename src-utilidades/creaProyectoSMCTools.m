%%%%%% SCRIPT PARA LA CREACIÓN DE UN PROYECTO DE SMC-TOOLS, PARTIR DE DATOS
%%%%%% NO ORIGINALES. ESTO ES A PARTIR DE DATOS DE IH-DATA, CREA LA BASE DE
%%%%%% DATOS DE SMC-TOOLS Y A PARTIR DE UN PROYECTO SMC3 CREA EL FICHERO
%%%%%% SMCT 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Creo el cache con los datos DOW
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('cache')

%%%Creo los datos binarios en la carpeta Cache
folder='';
files=dir(fullfile(folder,'DOW*.mat'));
lat=[];
lon=[];
z=[];
for i=1:length(files)    
    load(fullfile(folder,files(i).name));        
    outFile=fullfile('cache',['wd_',num2str(data.lat),'_',num2str(data.lon),'.dow']);
    tide=got.getTemporalSerie(data.lat,data.lon,data.time(1),data.time(end));
    tide=interp1(tide.time,tide.tide,data.time);
    lat(i)=data.lat;
    lon(i)=data.lon;
    if isempty(data.bat)
        data.bat=0;
    end
    z(i)=data.bat;
    
    fid=fopen(outFile,'w');
    
    fwrite(fid,int32(length(data.hs)),'int32');
    fwrite(fid,double(data.time(1)),'double');
    fwrite(fid,double(data.time(end)),'double');    
    
    fwrite(fid,data.hs,'double');
    fwrite(fid,data.wave_dir,'double');
    fwrite(fid,data.tp,'double');
    fwrite(fid,data.wave_spectral_angular_spread,'double');
    fwrite(fid,tide,'double');
    
    fclose(fid);
    
    if i==1
        %%%%%% UTM
        z1=utmzone(lat(1),lon(1));
        [ellipsoid,~] = utmgeoid(z1);
        utmstruct = defaultm('utm'); 
        utmstruct.zone = z1; 
        utmstruct.geoid = ellipsoid; 
        utmstruct = defaultm(utmstruct);
        %%%%%%%%%%%
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Creo el proyecto SMCT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
projectFolder='';%SMC_3_Mogan_base';
[xDow,yDow]=mfwdtran(utmstruct,lat,lon);
nodosDow.x=xDow;
nodosDow.y=yDow;
nodosDow.lat=lat;
nodosDow.lon=lon;
nodosDow.z=z;
smcToolsPrj.nodosDow=nodosDow;

file=fullfile(projectFolder,'Proyecto.smct');
save(file,'smcToolsPrj');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Creo la base de datos de niveles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear db
mkdir('data');
mkdir(fullfile('data','gos'));
mkdir(fullfile('data','got'));

files=dir(fullfile(folder,'GOT*.mat'));
lat=[];
lon=[];
names={};


for i=1:length(files)    
    load(fullfile(folder,files(i).name));    
    id=['node_',num2str(data.lat,'%5f'),'_',num2str(data.lon,'%5f')];
    outFile=fullfile('data','got',id);
    fid=fopen(outFile,'w');
    fwrite(fid,int32(length(data.tide)),'int32');
    fwrite(fid,double(data.time(1)),'double');
    fwrite(fid,double(data.time(end)),'double');    
    fwrite(fid,single(data.tide),'single');
    fclose(fid);   
    names{i}=id;
    lat(i)=data.lat;
    lon(i)=data.lon;
end
db.gotNodes.files=names;
db.gotNodes.lat=lat;
db.gotNodes.lon=lon;

files=dir(fullfile(folder,'GOS*.mat'));
lat=[];
lon=[];
names={};

for i=1:length(files)    
    load(fullfile(folder,files(i).name));    
    id=['node_',num2str(data.lat_zeta,'%5f'),'_',num2str(data.lon_zeta,'%5f')];
    outFile=fullfile('data','gos',id);
    fid=fopen(outFile,'w');
    fwrite(fid,int32(length(data.zeta)),'int32');
    fwrite(fid,double(data.time(1)),'double');
    fwrite(fid,double(data.time(end)),'double');    
    fwrite(fid,single(data.zeta),'single');
    fclose(fid);   
    names{i}=id;
    lat(i)=data.lat_zeta;
    lon(i)=data.lon_zeta;
end
db.gosNodes.files=names;
db.gosNodes.lat=lat;
db.gosNodes.lon=lon;

dbFile=fullfile('data','smcTools.db');
save(dbFile,'db');








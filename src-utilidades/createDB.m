%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%&
%%% Script para inicializar la base de datos
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%&

db.shp='land_Brazil.shp';

mallas=dir(fullfile('data','dow','M*_b.nc'));
for i=1:length(mallas)
    malla=mallas(i).name;
    malla=malla(1:end-5);
    [lat,lon,~]=data.getDOWNodes(malla);    
    db.mallas(i).nombre=malla;
    db.mallas(i).lat=[min(lat),max(lat)];
    db.mallas(i).lon=[min(lon),max(lon)];
end



db.gosNodes.lat=[];
db.gosNodes.lon=[];
db.gosNodes.files={};


xmlFile='/windows/Proyectos/SMC/SMC Brasil/DataBase/data/Baco.xml';
xDoc = xmlread(xmlFile);
cartas=xDoc.getElementsByTagName('cartaNautica');
for i=0:cartas.getLength-1    
    db.cartas(i+1).nombre=char(cartas.item(i).getAttribute('cartaId'));
    db.cartas(i+1).lat(1)=str2double(char(cartas.item(i).getAttribute('oriLatitud')));
    db.cartas(i+1).lat(2)=str2double(char(cartas.item(i).getAttribute('endLatitud')));
    db.cartas(i+1).lon(1)=str2double(char(cartas.item(i).getAttribute('oriLongitud')));
    db.cartas(i+1).lon(2)=str2double(char(cartas.item(i).getAttribute('endLongitud')));
end

% db.cartas(1).nombre='100';
% db.cartas(1).lat=[1.99988598908202,4.83355666004525];
% db.cartas(1).lon=[-51.9167676370076,-49.5835250863461];

db.batRegion(1).nombre='norte';
db.batRegion(1).lat=[-3.0248,5.0141];
db.batRegion(1).lon=[-51.9154,-39.9661];
db.batRegion(2).nombre='centro';
db.batRegion(2).lat=[ -19.0204, -1.2775];
db.batRegion(2).lon=[ -39.9665,-32.3412];
db.batRegion(3).nombre='sur';
db.batRegion(3).lat=[-35.6708,-18.9536];
db.batRegion(3).lon=[-54.9966,-26.2090];

db.gotNodes.lat=[];
db.gotNodes.lon=[];
db.gotNodes.files={};

gotFiles=dir(fullfile('data','got','node*.*'));
for i=1:length(gotFiles)
    node=gotFiles(i).name;
    nodeInfo=regexp(node,'_','split');
    db.gotNodes.lat(i)=str2double(nodeInfo{2});
    db.gotNodes.lon(i)=str2double(nodeInfo{3});
    db.gotNodes.files{i}=node;
end

save('smcTools.db','db');





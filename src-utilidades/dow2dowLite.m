clear all

folder='/home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/dow/mediterraneo';
files=dir(fullfile(folder,'*.nc'));
xyzFile='/home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/dow/lista_final_smcE.dat';
xyz=load(xyzFile);
plot(xyz(:,1),xyz(:,2),'ok')
hold on

mallasOk=[];
for i=1:length(files)
    disp(i)
    ncFile=fullfile(folder,files(i).name);
    ncId=netcdf.open(ncFile,'NC_NOWRITE');
    xOri=netcdf.getAtt(ncId,-1,'origin_longitude');
    yOri=netcdf.getAtt(ncId,-1,'origin_latitude');
    angle=netcdf.getAtt(ncId,-1,'angle');
    x=netcdf.getVar(ncId,netcdf.inqVarID(ncId,'x'));
    y=netcdf.getVar(ncId,netcdf.inqVarID(ncId,'y'));
    nx=length(x);ny=length(y);
    dx=x(2)-x(1);dy=y(2)-y(1);
    lx=x(end);ly=y(end);    
    xv=[0,0,lx,lx];
    yv=[0,ly,ly,0];
    XV=xOri+xv*cosd(angle)-yv*sind(angle);
    YV=yOri+xv*sind(angle)+yv*cosd(angle);
    
    mallasOk=cat(1,mallasOk,i);
    
%     [X,Y]=meshgrid(x,y);
%     x=reshape(X,nx*ny,1)+xOri;
%     y=reshape(Y,nx*ny,1)+yOri;
    index=inpolygon(xyz(:,1),xyz(:,2),XV,YV);
    
    if sum(index)>0
        x=xyz(index,1);
        y=xyz(index,2);        
        %supongo que es 200?
        if size(xyz,2)<3
            z=ones(length(x),1)*150;
        else
            z=xyz(index,3);
        end

        fill(XV,YV,'r','FaceColor','none','EdgeColor','r')
        name=files(i).name;    
        text(xOri,yOri,name(end-6:end-3));




        for j=1:length(files)
            if i~=j && ismember(j,mallasOk)
                
                
                ncFileSol=fullfile(folder,files(j).name);
                ncSolId=netcdf.open(ncFileSol,'NC_NOWRITE');
                xOriSol=netcdf.getAtt(ncSolId,-1,'origin_longitude');
                yOriSol=netcdf.getAtt(ncSolId,-1,'origin_latitude');
                angleSol=netcdf.getAtt(ncSolId,-1,'angle');
                xSol=netcdf.getVar(ncSolId,netcdf.inqVarID(ncSolId,'x'));
                ySol=netcdf.getVar(ncSolId,netcdf.inqVarID(ncSolId,'y'));            
                lxSol=xSol(end);lySol=ySol(end);                
                netcdf.close(ncSolId);

                xvSol=[0,0,lxSol,lxSol];
                yvSol=[0,lySol,lySol,0];
                XVSol=xOriSol+xvSol*cosd(angleSol)-yvSol*sind(angleSol);
                YVSol=yOriSol+xvSol*sind(angleSol)+yvSol*cosd(angleSol);

                index=inpolygon(x,y,XVSol,YVSol);

                if sum(index)>0
                    %fill([xOriSol,xOriSol,xOriSol+lxSol,xOriSol+lxSol],[yOriSol,yOriSol+lySol,yOriSol+lySol,yOriSol],'r')
                    %disp(j)
%                     xCom=x(index);yCom=y(index);zCom=z(index); 
% 
%                     xRel1=(xCom-xOri)*cosd(angle)+(yCom-yOri)*sind(angle);
%                     yRel1=-(xCom-xOri)*sind(angle)+(yCom-yOri)*cosd(angle);                
% 
%                     xRel2=(xCom-xOriSol)*cosd(angleSol)+(yCom-yOriSol)*sind(angleSol);
%                     yRel2=-(xCom-xOriSol)*sind(angleSol)+(yCom-yOriSol)*cosd(angleSol);                
% 
%                     dist1=min(abs([xRel1,yRel1,xRel1-lx,yRel1-ly]),[],2);
%                     dist2=min(abs([xRel2,yRel2,xRel2-lySol,yRel2-lySol]),[],2);
% 
%                     %dist2=min(abs([xCom-xOriSol,yCom-yOriSol,xCom-lxSol-xOriSol,yCom-lySol-yOriSol]),[],2);
%                     indexCom=dist1<dist2;
% 
%                     x=[x(~index);xCom(~indexCom)];
%                     y=[y(~index);yCom(~indexCom)];
%                     z=[z(~index);zCom(~indexCom)];
                      x=x(~index);
                      y=y(~index);
                      z=z(~index);

                end
            end
        end    
       
       plot(x,y,'.r')
       axis equal


       %Cargo los datos del fichero NETCDF    
       timeId=netcdf.inqVarID(ncId,'time');
       time=netcdf.getVar(ncId,timeId);

       hsId=netcdf.inqVarID(ncId,'hs');   
       hs=netcdf.getVar(ncId,hsId);
       dirId=netcdf.inqVarID(ncId,'wave_dir');   
       dir=netcdf.getVar(ncId,dirId);
       tpId=netcdf.inqVarID(ncId,'tp');
       tp=netcdf.getVar(ncId,tpId);   
       netcdf.close(ncId);   

       xRel=(x-xOri)*cosd(angle)+(y-yOri)*sind(angle);
       yRel=-(x-xOri)*sind(angle)+(y-yOri)*cosd(angle);  
       indexX=round(xRel/dx)+1;   
       indexX(indexX==0)=1;
       indexY=round(yRel/dy)+1;   
       indexY(indexY==0)=1;
       index=(indexX-1)*ny+indexY;

       hsLite=hs(:,index);
       tpLite=tp(:,index);
       dirLite=dir(:,index);

       dataInd=hsLite(1,:)>-990;
       x(~dataInd)=[];
       y(~dataInd)=[];
       z(~dataInd)=[];

       hsLite(:,~dataInd)=[];   
       tpLite(:,~dataInd)=[];   
       dirLite(:,~dataInd)=[];  

       %Creo el nuevo fichero NetCDF
       ncFileOut=fullfile(folder,'lite',[files(i).name]);
       ncIdOut=netcdf.create(ncFileOut,'CLOBBER');
       timeDimId=netcdf.defDim(ncIdOut,'time',size(hsLite,1));   
       nodeDimId=netcdf.defDim(ncIdOut,'node',size(hsLite,2));

       timeId=netcdf.defVar(ncIdOut,'time','int',timeDimId);
       netcdf.putAtt(ncIdOut,timeId,'units','hours since 1900-01-01 00:00:0.0');
       netcdf.putAtt(ncIdOut,timeId,'description','classified dates');

       nodeId=netcdf.defVar(ncIdOut,'node','int',nodeDimId);
       netcdf.putAtt(ncIdOut,nodeId,'units','index of node');
       netcdf.putAtt(ncIdOut,nodeId,'description','classified dates');

       latId=netcdf.defVar(ncIdOut,'latitude','double',nodeDimId);
       netcdf.putAtt(ncIdOut,latId,'units','euclidean degrees from mesh bottom-left corner');

       lonId=netcdf.defVar(ncIdOut,'longitude','double',nodeDimId);
       netcdf.putAtt(ncIdOut,lonId,'units','euclidean degrees from mesh bottom-left corner');

       zId=netcdf.defVar(ncIdOut,'bathymetry','float',nodeDimId);
       netcdf.putAtt(ncIdOut,zId,'units','m');

       hsId=netcdf.defVar(ncIdOut,'hs','float',[timeDimId,nodeDimId]);
       netcdf.putAtt(ncIdOut,hsId,'units','m');

       dirId=netcdf.defVar(ncIdOut,'wave_dir','float',[timeDimId,nodeDimId]);
       netcdf.putAtt(ncIdOut,dirId,'units','degree from North');

       tpId=netcdf.defVar(ncIdOut,'tp','float',[timeDimId,nodeDimId]);
       netcdf.putAtt(ncIdOut,tpId,'units','s');


       %putData
       netcdf.endDef(ncIdOut);
       netcdf.putVar(ncIdOut,timeId,time);
       netcdf.putVar(ncIdOut,nodeId,1:size(hsLite,2));
       netcdf.putVar(ncIdOut,latId,y);
       netcdf.putVar(ncIdOut,lonId,x);
       netcdf.putVar(ncIdOut,zId,z);
       netcdf.putVar(ncIdOut,hsId,hsLite);
       netcdf.putVar(ncIdOut,tpId,tpLite);
       netcdf.putVar(ncIdOut,dirId,dirLite);
       netcdf.close(ncIdOut); 
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Concatenacion de submallas en mallas (usando NCO)
%Paso de fix a record
%ls CS01?.nc | xargs -n1  -I ARG  -t  ncks --mk_rec_dmn node ARG tmp/ARG 
%Concateno
%ncrcat tmp/CS01?.nc tmp/CS01.nc
%Paso de record a fix
%ls CS0?.nc | xargs -n1  -I ARG  -t  ncks --fix_rec_dmn  ARG tmp/ARG  | more





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Aztualizo el smcTools.db
folder='/home/nabil/Proyectos/SMC/SMC Tools/trunk/src/data/es/dow';
dowMeshes=dir(fullfile(folder,'*.nc'));
load(fullfile('data','es','smcTools.db'),'-mat');
for i=1:length(dowMeshes)
    mesh=dowMeshes(i).name;   
    mesh=mesh(1:end-3);    
    [lat,lon,~]=data.getDOWNodes(mesh);    
    malla.nombre=mesh;
    malla.lat=[min(lat),max(lat)];
    malla.lon=[min(lon),max(lon)];
    db.mallas=cat(1,db.mallas,malla);
end
save(fullfile('data','es','smcTools.db'),'db','-mat');


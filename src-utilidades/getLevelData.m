%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Get GOS/GOT Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

coordsFile='//home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/nodosGos2.txt';
nodos=load(coordsFile);
outGosFolder=fullfile('data','gos');
outGotFolder=fullfile('data','got');
if ~exist(outGosFolder,'dir')
    mkdir(outGosFolder);
    mkdir(outGotFolder);
end


for i=1:length(nodos)
    lon=nodos(i,1);
    lat=nodos(i,2);
    dataGos=gos.getTemporalSerie(lat,lon,'SW_NCEP','EuropaSur');
    dataGot=got.getTemporalSerie(lat,lon,dataGos.time(1),dataGos.time(end));
    
    id=['node_',num2str(dataGos.lat_zeta,'%5f'),'_',num2str(dataGos.lon_zeta,'%5f'),];    
    outFile=fullfile(outGosFolder,id);
    fid=fopen(outFile,'w');
    fwrite(fid,int32(length(dataGos.zeta)),'int32');
    fwrite(fid,double(dataGos.time(1)),'double');
    fwrite(fid,double(dataGos.time(end)),'double');    
    fwrite(fid,single(dataGos.zeta),'single');
    fclose(fid);   
    
    id=['node_',num2str(dataGot.lat,'%5f'),'_',num2str(dataGot.lon,'%5f'),];    
    outFile=fullfile(outGotFolder,id);
    fid=fopen(outFile,'w');
    fwrite(fid,int32(length(dataGot.tide)),'int32');
    fwrite(fid,double(dataGot.time(1)),'double');
    fwrite(fid,double(dataGot.time(end)),'double');    
    fwrite(fid,single(dataGot.tide),'single');
    fclose(fid);   
    i
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Aztualizo el smcTools.db
load(fullfile('data','smcTools.db'),'-mat');

gotFiles=dir(fullfile('data','got','*'));

latGot=[];
lonGot=[];
filesGot={};
for i=3:length(gotFiles)
    nodeInfo=regexp(gotFiles(i).name,'_','split');
    latGot=cat(1,latGot,str2double(nodeInfo{2}));
    lonGot=cat(1,lonGot,str2double(nodeInfo{3}));
    filesGot=cat(1,filesGot,gotFiles(i).name);
end
db.gotNodes.lat=latGot;
db.gotNodes.lon=lonGot;
db.gotNodes.files=filesGot;

gosFiles=dir(fullfile('data','gos','*'));
latGos=[];
lonGos=[];
filesGos={};
for i=3:length(gosFiles)
    nodeInfo=regexp(gosFiles(i).name,'_','split');
    latGos=cat(1,latGos,str2double(nodeInfo{2}));
    lonGos=cat(1,lonGos,str2double(nodeInfo{3}));
    filesGos=cat(1,filesGos,gosFiles(i).name);
end
db.gosNodes.lat=latGos;
db.gosNodes.lon=lonGos;
db.gosNodes.files=filesGos;

save(fullfile('data','smcTools.db'),'db','-mat');





    
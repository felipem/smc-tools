%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Script para añadir en la bbdd de SMC Tools
%%% las cartas naúticas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bacoDir='/home/nabil/Proyectos/SMC/SMC_ESPANA/Instalaciones/interna/SIGMA/Data/Baco';
iniFile=fullfile(bacoDir,'Cartas.ini');
cartas=[];


fid=fopen(iniFile,'r');
line=fgetl(fid);
while ischar(line)
    
    if ~isempty(strfind(line,'['))
        c.nombre=line(2:end-1);
        limits=textscan(fid,'%s %f',4,'Delimiter','=');    
        limits=limits{2};        
        c.lat=[limits(2),limits(1)];
        c.lon=[limits(4),limits(3)];
        cartas=cat(1,cartas,c);        
    end
    line=fgetl(fid);
end

fclose(fid);

load(fullfile('data','smcTools.db'),'-mat');
db.cartas=cartas;
save(fullfile('data','smcTools.db'),'db','-mat');





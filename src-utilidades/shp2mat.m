%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%convertidor de SHP file a MAT

%shpFile=fullfile('data','shp','land_SudAmerica.shp');
%shpFile='/home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/ShpGis/noSpainRegional.shp';
%shpFile='/home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/ShpGis/spainDetail.shp';
shpFile='/home/nabil/Proyectos/SMC/SMC Tools/misc/Datos España/ShpGis/spainRegional.shp';
S = shaperead(shpFile);

maps=[];
for i=1:length(S)
    X=S(i).X;
    Y=S(i).Y;
    index=find(isnan(X)==1);
    m.x=X(1:index(1)-1);
    m.y=Y(1:index(1)-1);
    maps=cat(1,maps,m);
    for j=2:length(index)
        m.x=X(index(j-1)+1:index(j)-1);
        m.y=Y(index(j-1)+1:index(j)-1);
        maps=cat(1,maps,m);
    end
end
outFolder=fullfile('data','shp');
if ~exist(outFolder,'dir')
    mkdir(outFolder);
end
shp.maps=maps;
%mapFile=fullfile(outFolder,'regionalMapNoCountry.mat');
%mapFile=fullfile(outFolder,'detailMap.mat');
mapFile=fullfile(outFolder,'regionalMap.mat');
save(mapFile,'shp');

tic;
for i=1:length(maps)
    fill(maps(i).x,maps(i).y,'y');
    hold on
end
axis equal
toc






classdef AmevaForm<handle
    properties (Hidden)        
       
        
    end
    
    properties (Dependent)
        initDate;
        endDate;
    end
    
    properties (Access=private)
        enabled=true;
        data; %container.Map
        panel;
        %infoPanel;
        tools=containers.Map;   
        toolbar;
        toolsCount=0;        
        initDate_;
        endDate_;
        rootNode;
        tree;
        nodes=containers.Map;
        propertyGrid;
        info=[];
        txt;
        cmVar;
    end   

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Constructor/Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods 
        function self=AmevaForm(data,panel,toolbar,tree,rootNode,dataInfo)
            try
            if isvalid(self.nodes)
            if ~isempty(self.nodes)
                delete(self.nodes);                
            end
            end
            self.nodes=containers.Map;
            
            
            %%% Cargo el idioma
            self.txt=util.loadLanguage;            
            %%%%%%%%%%%%%%%%%%%
            
            
            
            if isvalid(self.tools)
            if ~isempty(self.tools)
                delete(self.tools);                
            end
            end
            self.tools=containers.Map;
            
            self.data=data;
            self.panel=panel;            
            self.toolbar=toolbar;
            self.rootNode=rootNode;
            self.tree=tree;
            %self.infoPanel=infoPanel;
            self.info=dataInfo;
            
            %%% Pongo los nombres de las variables como en la traducción
            vars = keys(self.data); 
            for i=1:length(vars)
                lngKey=['exVar',vars{i}];
                if isKey(self.txt.ameva,lngKey)
                    var=self.data(vars{i});
                    var.name=self.txt.ameva(lngKey);
                end
                
            end
            
            %%%%%%%%%%%%%%%%%%%
            
             
            self.initToolBar();
            self.initExplorer();
            %self.initInfoPanel();
            self.initContextMenu();
            %%% Init dates           
            var=data(vars{1});
            self.initDate=datenum(var.initDate,'dd-mm-yyyy');
            self.endDate=datenum(var.endDate,'dd-mm-yyyy');
            
            catch ex
                delete(self);
                error(ex.message);
            end
        end 
        
        function delete(self)                        
            
            self.tree.NodeSelectedCallback=[];
            %Elimino los hijos por si hay algo
            util.cleanNode(self.rootNode,self.tree);
            delete(self.nodes);
            
            delete(self.propertyGrid);
            util.resetPanel(self.panel);
          %  util.resetPanel(self.infoPanel);    
            jToolbar = get(get(self.toolbar,'JavaContainer'),'ComponentPeer');
            totalTools=jToolbar.getComponentCount();
            for i=totalTools-1:-1:totalTools-self.toolsCount;
                 jToolbar.remove(i);
            end
            delete(self.tools);
            jToolbar(1).repaint;
            jToolbar(1).revalidate; 
            delete(self.txt);
        end
      
        function initToolBar(self)
            isClon=false;
            if isempty(get(self.toolbar,'Children'))
                isClon=true;
            end
            self.toolsCount=0;
            self.toolsCount=self.toolsCount+core.navigateToolbar(self.toolbar,self.panel);
            
            if ~isClon
                self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar);
                ptDesacoplar=uipushtool(self.toolbar,'ClickedCallback',@self.clonarVentana,'TooltipString',self.txt.ameva('tbCloneWindow'));            
                util.setControlIcon(ptDesacoplar,'window_add.gif');    
                self.toolsCount=self.toolsCount+1; 
            end
         end
        
        function initExplorer(self)
            ddVarStrings = keys(self.data); 
             for i=1:length(ddVarStrings)
                graphs=self.getVarGraphs(ddVarStrings{i});
                graphs_keys=keys(graphs);
                graphs_values=values(graphs);
                varName=self.data(ddVarStrings{i}).name;
                varNode=uitreenode('v0',['fld_',ddVarStrings{i}],varName,[],false);
                util.setNodeIcon(varNode,'folder_closed.png');
                self.nodes(ddVarStrings{i})=varNode;
                self.rootNode.add(varNode);
                for j=1:length(graphs_keys)
                    value=regexp(graphs_values{j},'\','split');
                    folderKey=['fld_',ddVarStrings{i},'_',value{1}];                    
                    if ~isKey(self.nodes,folderKey) %Miros si existe la carpeta                        
                        folderNode=uitreenode('v0',folderKey,value{1},[],false);
                        util.setNodeIcon(folderNode,'folder_closed.png');
                        self.nodes(folderKey)=folderNode;                        
                        varNode.add(folderNode);
                    else
                        folderNode=self.nodes(folderKey);
                    end
                    graphKey=['graph_',ddVarStrings{i},'_',graphs_keys{j}];
                    graphNode=uitreenode('v0',graphKey,value{2},[],true);
                    util.setNodeIcon(graphNode,value{3});
                    self.nodes(graphKey)=graphNode;                        
                    folderNode.add(graphNode);                    
                end
            end
            self.tree.reloadNode(self.rootNode);            
            self.tree.NodeSelectedCallback=@self.nodeSelected;
           
        end
         
        function initContextMenu(self)
            self.cmVar.jmenu=javax.swing.JPopupMenu;            
            self.cmVar.properties=javax.swing.JMenuItem(self.txt.ameva('wVarProperties'));                        
            set(self.cmVar.properties,'ActionPerformedCallback',@self.varProperties);                    
            self.cmVar.jmenu.add(self.cmVar.properties);
        end
    end
    %%% End Constructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %%% Get/Set
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%
   methods    
        function set.initDate(self,value)
            try
                self.initDate_=datenum(value,'dd-mm-yyyy');
                vars=keys(self.data);
                for i=1:length(vars)
                    var=self.data(vars{i});
                    var.initDate=value;
                end
            catch                
            end
            
        end
        
        function value=get.initDate(self)
            value=datestr(self.initDate_,'dd-mm-yyyy');
        end
        
        function set.endDate(self,value)
            try
                self.endDate_=datenum(value,'dd-mm-yyyy');                
                vars=keys(self.data);
                for i=1:length(vars)
                    var=self.data(vars{i});
                    var.endDate=value;
                end
            catch                
            end
            
        end
        
        function value=get.endDate(self)
            value=datestr(self.endDate_,'dd-mm-yyyy');
        end
    end
   %%%End  Get/Set
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
   methods
        function mousePressedCallback(self,jtree,eventData)
            if ~self.enabled,return;end
            clickX = eventData.getX;
             clickY = eventData.getY;
             treePath = jtree.getPathForLocation(clickX, clickY);
              if ~isempty(treePath)
                 % check if the checkbox was clicked
                 node = treePath.getLastPathComponent;
                 jtree.setSelectionPath(treePath);
                 if eventData.isMetaDown
                     [~,~,nodeId]=util.isCheckNode(node);
                     
                     if strfind(nodeId,'fld')
                        nodoInfo=regexp(nodeId,'_','split');   
                        varId=nodoInfo{2};   
                        if length(nodoInfo)>2,return,end;
                        if ~isKey(self.nodes,varId);                                                    
                              return;
                        end
                        self.cmVar.jmenu.show(jtree, clickX, clickY);
                     end                       
                 end
              end
        end
        
        function info=getInfo(self)
            info=self.info;
        end
   end
    methods (Access=private)
            
        function plotGraph(self,varId,graphId)
            self.blockForm;
            try
            if strcmp(graphId,'rose') || strcmp(graphId,'table')
                eval(['ameva.Graphs.',graphId,'(self.panel,self.data(varId),self.data,self.txt);']);
            elseif strfind(graphId,'pdf2D')
                var2Id=graphId(7:end);
                eval(['ameva.Graphs.','pdf2D','(self.panel,self.data(varId),self.data(var2Id),self.txt);']);
            else
                eval(['ameva.Graphs.',graphId,'(self.panel,self.data(varId),self.txt);']);
            end
            catch ex
                msgbox(ex.message,'error','error');
            end
            self.blockForm;
        end
        
        function varGraphs=getVarGraphs(self,varId)
              var=self.data(varId);
              %TODO : Crear de forma dinámica los contextos y los iconos
              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
              varGraphs=containers.Map;
              varGraphs('temporalSerie')=[self.txt.ameva('exDescriptive'),'\',self.txt.ameva('exTemporalSerie'),'\serie.gif'];
              varGraphs('histogram')=[self.txt.ameva('exDescriptive'),'\',self.txt.ameva('exHistogram'),'\bar_chart.gif'];
              varGraphs('distFunction')=[self.txt.ameva('exDescriptive'),'\CDF','\cdf.gif'];
              varGraphs('distributionNormal')=[self.txt.ameva('exMedium'),'\Normal','\bar_chart.gif'];
              if min(var.getData)>=0
                  varGraphs('distributionLogNormal')=[self.txt.ameva('exMedium'),'\LogNormal','\bar_chart.gif'];
                  varGraphs('distributionWeibullMin')=[self.txt.ameva('exMedium'),'\Weibull Min','\bar_chart.gif'];

              end
              varGraphs('distributionGumbelMax')=[self.txt.ameva('exMedium'),'\Gumbel Max','\bar_chart.gif'];
              varGraphs('GEV')=[self.txt.ameva('exExtreme'),'\GEV','\cdf.gif'];
              if var.isDir
                  varGraphs('rose')=[self.txt.ameva('exDescriptive'),'\',self.txt.ameva('exRose'),'\pie_chart.gif'];
                  varGraphs('table')=[self.txt.ameva('exDescriptive'),'\',self.txt.ameva('exTable'),'\report.gif'];
              end
              
              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
              %%%%% PDF 3D
              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
              varsId=keys(self.data);
              for i=1:length(varsId)
                  if ~strcmp(varsId{i},varId)
                      varGraphs(['pdf2D-',varsId{i}])=[self.txt.ameva('exDescriptive'),'\',['pdf2D-',self.txt.ameva(['exVar',varsId{i}])],'\color_swatch.gif'];
                  end
              end
        end       
         
        function nodeSelected(self,tree,value)              
            node=get(value,'CurrentNode');                        
            nodeId=get(node,'Value');
            if ~isKey(self.nodes,nodeId);

                return;
            end           
            nodoInfo=regexp(nodeId,'_','split');
            if strcmp(nodoInfo{1},'fld'),return,end;
            varId=nodoInfo{1};            
            if length(nodoInfo)>1
                varId=nodoInfo{2}; 
                graphId=nodoInfo{3};
            self.plotGraph(varId,graphId);
            end
            

        end 
        
        function varProperties(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);
            var=self.data(nodeId(5:end));
            prompt = {self.txt.ameva('wVarPropertiesName'),...
                self.txt.ameva('wVarPropertiesUnits'),...
                [self.txt.ameva('wVarPropertiesDateIni'),' ','(dd-mm-yyyy)'],...
                [self.txt.ameva('wVarPropertiesDateEnd'),' ','(dd-mm-yyyy)'],...
                [self.txt.ameva('wVarPropertiesPrcIni'),' ','(%)'],...
                [self.txt.ameva('wVarPropertiesPrcEnd'),' ','(%)']};
            
            dlg_title = self.txt.ameva('wVarProperties');
            num_lines = 1;
            def = {var.name,var.units,var.initDate,var.endDate,num2str(var.percInit),num2str(var.percEnd)};
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            if  ~isempty(answer)                
                
                var.name=answer{1};
                var.units=answer{2};                
                var.initDate=answer{3};
                var.endDate=answer{4}; 
                var.percInit=str2double(answer{5}); 
                var.percEnd=str2double(answer{6});                 
                
                watchon;
                pause(0.5);
                util.resetPanel(self.panel);
                watchoff;
            end           
        end
        
        function clonarVentana(self,varargin)
            scrPos=get(0,'ScreenSize');
            width=800;
            height=600;
             f = figure( 'MenuBar', 'none', 'Name', ['Ameva ',get(self.rootNode,'Name')],...
                 'NumberTitle', 'off', 'Toolbar', 'none',...
                 'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@closeForm);
             tb=uitoolbar(f);
             panelM=uipanel(f,'Position',[0.25 0 0.75 1]);
             root=uitreenode('v0',get(self.rootNode,'Value'),get(self.rootNode,'Name'),[],false);    
             util.setNodeIcon(root,'dow.gif');
             pause(0.1);
             mtree = uitree('v0', 'Root', root);
             set(mtree,'Units','normalized')
             set(mtree,'Position',[0 0 0.25 1])
             util.initTree(mtree); 
            form=ameva.AmevaForm(self.data,panelM,tb,mtree,root,self.info);             
            function closeForm(varargin)
                delete(form);
                delete(f);
            end
        end
        
        function blockForm(self)
            f=get(self.panel,'Parent');
            if f==findobj('-regexp','Tag','smcToolsMainForm_*')
                core.blockMainForm;
                return;
            end
          self.enabled=~self.enabled;
          strEnable='on';
          if ~self.enabled,strEnable='off';end;
          disableForm(f);
          
          if self.enabled
            watchoff;
          else
            watchon;
            pause(0.1);
          end
          
           function disableForm(form)        
            if isprop(form,'Enable')
                set(form,'Enable',strEnable)
            end
            children=get(form,'Children');
            for i=1:length(children)
                disableForm(children(i));
            end        
        end
        end
    end
    
    
    
end
classdef AmevaVar <handle    
    properties       
        name;
        units;        
        dir='';
        percInit=10;
        percEnd=99.5;
    end
    
    properties (Access=private)
        initDate_;
        endDate_;       
        histX;
        histN;
        time;
        data;
    end
    
    properties (Dependent)
        initDate;
        endDate;
        isDir;
    end
    
    methods
        function self=AmevaVar(name,units,data,time,dir)
            self.data=data;
            self.name=name;
            self.units=units;
            self.time=time;
            self.initDate_=min(time);
            self.endDate_=max(time);
            if isreal(data)
                self.calculateHist();
            end
            if exist('dir','var')
                self.dir=dir;
            end
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
    %%%%%%% Get / Set
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
    methods 
        function set.initDate(self,value)
            try
                self.initDate_=datenum(value,'dd-mm-yyyy');
                self.calculateHist(self);
            catch                
            end
            
        end
        
        function value=get.initDate(self)
            value=datestr(self.initDate_,'dd-mm-yyyy');
        end
        
        function set.endDate(self,value)
            try
                self.endDate_=datenum([value,'-23-00'],'dd-mm-yyyy-hh-MM');
                self.calculateHist(self);
            catch                
            end
            
        end
        
        function value=get.endDate(self)
            value=datestr(self.endDate_,'dd-mm-yyyy');
        end
        
        function set.percInit(self,value)
            if value<0
                self.percInit=0;
            elseif value<self.percEnd
                self.percInit=value;
            end
        end
        
        function value=get.percInit(self)
            value=self.percInit;
        end
        
        function value=get.percEnd(self)
            value=self.percEnd;
        end
        
        function set.percEnd(self,value)
            if value>=100
                self.percEnd=100;
            elseif value>self.percInit
                self.percEnd=value;
            end
        end
        
        function isDir=get.isDir(self)
            isDir=~isempty(self.dir);
        end
        
        
        
        
    end
    %%%%%%% End Get / Set
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    methods
        
        function calculateHist(self)   
            data_=self.getData;            
            IncY=((max(data_)-min(data_))/50);
            V=floor(min(data_)):IncY:max(data_);
            %V=min(data_):IncY:max(data_);            
            [self.histN,self.histX]=hist(data_,V+(IncY/2));
        end
        
        function data=getData(self)
            index= self.time>=self.initDate_ & self.time<=self.endDate_;
            data=self.data(index);
        end
        
        function data=getIntervalData(self,iDate,eDate)
            index= self.time>=iDate & self.time<=eDate;
            data=self.data(index);
        end
        
        function [data,time]=getRMData(self)
            data_=self.getData();
            sortData=sort(data_);
            indexMin=floor(length(sortData)*self.percInit/100);
            if indexMin<1,indexMin=1;end
            indexMax=ceil(length(sortData)*self.percEnd/100);
            if indexMax>length(data_),indexMax=length(data_);end
            
            minValue=sortData(indexMin);
            maxValue=sortData(indexMax);
            index=data_>=minValue & data_<=maxValue;
            data=data_(index);
            time=self.time(index);
        end
        
        function data=getTime(self)
            index= self.time>=self.initDate_ & self.time<=self.endDate_;
            data=self.time(index);
        end
        
        
        function [data,time,dateMax]=getMaxYearData(self)
            data_=self.getData;
            time_=self.getTime;
            
            year=datevec(time_);
            year=year(:,1);
            data=[];
            time=[];
            dateMax=[];
            for y=year(1):year(end)
                index=year==y;
                [maxData,maxInd]=max(data_(index));
                maxTime=time_(index);
                maxTime=maxTime(maxInd);
                dateMax=cat(1,dateMax,maxTime);
                time=cat(1,time,y);
                data=cat(1,data,maxData);
            end
        end
        
        function [data,time]=getMeanYearData(self)
            data_=self.getData;
            time_=self.getTime;
            
            year=datevec(time_);
            year=year(:,1);                        
            time=[];
            data=[];
            
            if isreal(data_)
                
                for y=year(1):year(end)
                    index=year==y;
                    meanData=mean(data_(index));
                    meanTime=datenum(y,1,1);                
                    %time=cat(1,time,meanTime);
                    time=cat(1,time,y);
                    data=cat(1,data,meanData);
                end
            else  %Es una variable direccional
                x_=real(data_);
                y_=imag(data_);                
                for y=year(1):year(end)
                    index=year==y;
                    meanX=sum(x_(index));
                    meanY=sum(y_(index));
                    meanData=atan2(meanY,meanX)*180/pi;
                    if meanData<0,meanData=meanData+360;end
                    %meanTime=datenum(y,1,1);                
                    %time=cat(1,time,meanTime);
                    time=cat(1,time,y);
                    data=cat(1,data,meanData);
                end
            end
        end
        
        function [data,time]=getP995YearData(self)
            data_=self.getData;
            time_=self.getTime;
            
            year=datevec(time_);
            year=year(:,1);
            data=[];
            time=[];
            for y=year(1):year(end)
                index=year==y;
                maxData=prctile(data_(index),99.5);                
                meanTime=datenum(y,1,1);                
                %time=cat(1,time,meanTime);
                time=cat(1,time,y);
                data=cat(1,data,maxData);
            end
        end
        
        function [data,time]=getMaxMonthData(self)
            data_=self.getData;
            time_=self.getTime;            
            date=datevec(time_);
            year=date(:,1);            
            data=[];
            time=[];
            for y=year(1):year(end)
                index=year==y;
                yearData=data_(index);
                yearTime=time_(index);
                month=date(index,2);
                for m=1:12
                    index=month==m;
                    [maxData,maxInd]=max(yearData(index));
                    maxTime=yearTime(index);
                    maxTime=maxTime(maxInd);
                    time=cat(1,time,maxTime);
                    data=cat(1,data,maxData);
                end
            end
        end
        
        function [data,time]=getMeanMonthData(self)
            data_=self.getData;
            time_=self.getTime;            
            date=datevec(time_);
            year=date(:,1);            
            data=[];
            time=[];
            for y=year(1):year(end)
                index=year==y;
                yearData=data_(index);                
                month=date(index,2);
                for m=1:12
                    index=month==m;
                    meanData=mean(yearData(index));
                    meanTime=year(y,m,1);                    
                    time=cat(1,time,meanTime);
                    data=cat(1,data,meanData);
                end
            end
        end
        
        function histX=getHistX(self)
            histX=self.histX;
        end
        
        function histN=getHistN(self)
            histN=self.histN;
        end
        
        function [sectors,sectorsIndex]=getDirSectors(self,nDir)
            sectors=linspace(0,360*(1-1/nDir),nDir);
            sectorsIndex=logical(zeros(length(self.data),nDir));
            for i=1:nDir
                low=sectors(i)-180/nDir;
                up=sectors(i)+180/nDir;
                if low<0,low=low+360;end
                if low>up
                    sectorsIndex(:,i)=self.data>low | self.data<=up;
                else                
                    sectorsIndex(:,i)=self.data>low & self.data<=up;
                end
            end
        end
        
        function data=getSubsetData(self,index)
             data=self.data(index);
        end
        
        function setData(self,time,data)
            self.data=data;
            self.time=time;
        end
        
    end
    
end
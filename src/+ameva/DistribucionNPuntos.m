function [X,P]=DistribucionNPuntos(x,NPuntos);

[N,X1]=hist(x,NPuntos);%histograma
Area=sum((X1(2)-X1(1))*N);
n=N./Area;%Area debe ser igual a 1
P1=cumsum((X1(2)-X1(1))*n);%Probabilidad acumulada

%Quito el ultimo punto, que acumula el 100% de la probabilidad
X(1:NPuntos-1)=X1(1:NPuntos-1);
P(1:NPuntos-1)=P1(1:NPuntos-1);
%figure(1)
%plot(X,n,X,P,'o');
%xlabel('Hs')
%ylabel('Prob')
%close(1)
classdef Graphs      
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%% Estadistica Descriptica
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static=true)
        
        function temporalSerie(panel,amevaVar,txt)  
            time=amevaVar.getTime();
            y=amevaVar.getData();
            
            util.resetPanel(panel);
            ax=axes('Parent',panel);
            
            plot(ax,datenum(time),y,'k','linewidth',1.5);
            ylabel(ax,[amevaVar.name,' (',amevaVar.units,')']);
            datetick(ax,'x','yyyy')            
            title(ax,[txt.ameva('grSerieTitle'),' ',amevaVar.name]); 
        end
        
        function histogram(panel,amevaVar,txt)  
           
            util.resetPanel(panel);
            ax=axes('Parent',panel);
            
            N=amevaVar.getHistN();
            X=amevaVar.getHistX();            
            bar(ax,X,100*(N./sum(N)),1.0,'b');
            xlabel(ax,[amevaVar.name,' (',amevaVar.units,')']);
            ylabel(ax,txt.ameva('grHistYlabel'));
            Titulo=[txt.ameva('grHistTitle'),' ',amevaVar.name];
            title(ax,Titulo); 
        end
        
        function distFunction(panel,amevaVar,txt)                  
            
            util.resetPanel(panel);
            ax=axes('Parent',panel);
            
            N=amevaVar.getHistN();
            X=amevaVar.getHistX(); 
            y=amevaVar.getData();
            
            Area=sum((X(2)-X(1))*N);
            n=N./Area;%Area debe ser igual a 1
            P=cumsum((X(2)-X(1))*n);%Probabilidad acumulada

            Y25=ameva.prctile(y,25);    
            Y50=ameva.prctile(y,50);
            Y75=ameva.prctile(y,75);

            hold(ax,'on');
            plot(ax,[Y25 Y25 min(y)],[0 0.25 0.25],'--k','linewidth',0.75);
            plot(ax,[Y50 Y50 min(y)],[0 0.5 0.5],'--k','linewidth',1);
            plot(ax,[Y75 Y75 min(y)],[0 0.75 0.75],'--k','linewidth',1.25);

            plot(ax,X,P,'b','linewidth',1.5);
            hold(ax,'off');

            text25=[txt.ameva('grDistQ'),' ',amevaVar.name,' 25% = ',num2str(Y25,4)];
            text50=[txt.ameva('grDistQ'),' ',amevaVar.name,' 50% = ',num2str(Y50,4)];
            text75=[txt.ameva('grDistQ'),' ',amevaVar.name,' 75% = ',num2str(Y75,4)];
            legend(ax,text25,text50,text75,4);

            axis(ax,[min(y) max(y) 0 1]);
            xlabel(ax,[amevaVar.name,' (',amevaVar.units,')']);
            ylabel(ax,txt.ameva('grDistYlabel'))
            Titulo=[txt.ameva('grDistTitle'),' ',amevaVar.name];
            title(ax,Titulo);             
        end
        
        function rose(panel,amevaVar,data,txt)
            
            y=amevaVar.getData();
            if isempty(amevaVar.dir),return,end
           util.resetPanel(panel);
            varDir=data(amevaVar.dir);            
            varDir.initDate=amevaVar.initDate;
            varDir.endDate=amevaVar.endDate;
            
            yDir=varDir.getData();
            
            yDir(yDir>=360-11.25)=360-yDir(yDir>=360-11.25);

            % Pinto la rosa para distintas magnitudes
            Y25=ameva.prctile(y,25);
            dum25= y<=Y25;
            Ydir25=yDir(dum25);

            Y50=ameva.prctile(y,50);
            dum50= y<=Y50;
            Ydir50=yDir(dum50);

            Y75=prctile(y,75);
            dum75=y<=Y75;
            Ydir75=yDir(dum75);
            Y100=max(y);

            colorin=[0 0 0.4; 0 0 0.55; 0 0 0.7;0 0.5 0.98];
            ax=axes('Parent',panel,'Position',[0 0.1 0.8 0.8]);


            [~,fmax] =ameva.groseq(ax,yDir,16,1,0,colorin(1,:),length(yDir));
            hold(ax,'on');
            ameva.groseq(ax, Ydir75,16,1,0,colorin(2,:),length(yDir),fmax);
            ameva.groseq(ax,Ydir50,16,1,0,colorin(3,:),length(yDir),fmax);
            ameva.groseq(ax,Ydir25,16,1,0,colorin(4,:),length(yDir),fmax);

            hbar=axes('Parent',panel,'Position',[.85 .1 .03 .8]); %genero un eje para hacer la barra
            colormap(hbar,flipud(colorin));%le digo que colores debe de llevar
            caxis(hbar,[0 4]) %limito los ejes
            hc=colorbar(hbar); % dibujo la barra en los ejes correspondientes
            set(hc,'ytick',[2:1:4],'yticklabel',{'25% ';'50% ';'75% '}) % pongo ticks y leyendas

            xlabel(hbar,[txt.ameva('grRoseLegend'),' ',amevaVar.name])% leyenda en el eje x
            hold(ax,'off');
        end
        
        function table(panel,amevaVar,data,txt)
            y=amevaVar.getData();
            if isempty(amevaVar.dir),return,end
            util.resetPanel(panel);
            varDir=data(amevaVar.dir);            
            varDir.initDate=amevaVar.initDate;
            varDir.endDate=amevaVar.endDate;
            yDir=varDir.getData();
            titulo=amevaVar.name;
            n=length(yDir);

            p12=(1-12/8760)*100;
            for j=1:16,
                izq=(j-1.5)*22.5;
                der=(j-0.5)*22.5;
                dum=( yDir>=izq)&( yDir<der);
                if izq<0
                    izq=izq+360;             
                    dum=dum | ( yDir>=izq);
                end
                H=y(dum);
                if isempty(H) 
                    prob(j)=0;
                    H50(j)=0;
                    H90(j)=0;
                    H99(j)=0;
                    H12(j)=0;
                else
                    prob(j)=length(H)/n;
                    H50(j)=ameva.prctile(H,50);
                    H90(j)=ameva.prctile(H,90);
                    H99(j)=ameva.prctile(H,99);
                    H12(j)=ameva.prctile(H,p12);
                end
            end

            dires=['N  ';'NNE';'NE ';'ENE';'E  ';'ESE';'SE ';'SSE';'S  ';'SSW';'SW ';'WSW';'W  ';'WNW';'NW ';'NNW'];
            ax=axes('Parent',panel,'Position',[0 0.1 0.8 0.8]);
            text(0.05,0.90,txt.ameva('grTableTitle'),'fontsize',9,'Color',[1 0 0],'Parent',ax); 
            text(0.05,0.80,strcat(txt.ameva('grTableVariable'),' ',titulo),'fontsize',9,'Parent',ax); 

            %direcciones
            text(0.05,0.70,txt.ameva('grTableDirLabel'),'fontsize',8,'Parent',ax);
            posi=0.7;
            for i=1:length(dires)
                posi=posi-0.1;
                text(0.1,posi,dires(i,:),'Parent',ax); 
            end

            %probabildad direcciones
            text(0.3,0.70,txt.ameva('grTableDirProbLabel'),'fontsize',8,'Parent',ax);
            posi=0.7;
            for i=1:length(prob)
                posi=posi-0.1;
                text(0.3,posi,num2str(sprintf('%1.4f',prob(i))),'Parent',ax); 
            end

            % X_50%
            text(0.55,0.70,strcat(titulo,'_{50%}'),'fontsize',8,'Parent',ax);
            posi=0.7;
            for i=1:length(prob)
                posi=posi-0.1;
                text(0.55,posi,num2str(sprintf('%1.4f',H50(i))),'Parent',ax); 
            end

            % X_90%
            text(0.75,0.70,strcat(titulo,'_{90%}'),'fontsize',8,'Parent',ax);
            posi=0.7;
            for i=1:length(prob)
                posi=posi-0.1;
                text(0.75,posi,num2str(sprintf('%1.4f',H90(i))),'Parent',ax); 
            end

            % X_99%
            text(0.95,0.70,strcat(titulo,'_{99%}'),'fontsize',8,'Parent',ax);
            posi=0.7;
            for i=1:length(prob)
                posi=posi-0.1;
                text(0.95,posi,num2str(sprintf('%1.4f',H99(i))),'Parent',ax); 
            end


            % X_s12%
            text(1.15,0.70,strcat(titulo,'_{12}'),'fontsize',8,'Parent',ax);
            posi=0.7;
            for i=1:length(prob)
                posi=posi-0.1;
                text(1.15,posi,num2str(sprintf('%1.4f',H12(i))),'Parent',ax); 
            end

            axis(ax,[0 1.2 -1 1])
            axis(ax,'off');
        end
        
        function pdf2D(panel,amevaVar1,amevaVar2,txt)
             watchon;
             
             util.resetPanel(panel);
             ax=axes('Parent',panel);
             
             x=amevaVar1.getData();
             time=amevaVar1.getTime();
             y=amevaVar2.getIntervalData(time(1),time(end));
             ndx=round(3/2+log(length(x))/log(2))*2;
             
             [data,c]=hist3([x,y],[ndx,ndx]);
             data=data/sum(sum(data));
             [X,Y]=meshgrid(c{1},c{2});
             colormap(ax,'default');
             pcolor(ax,X,Y,data');              
             %contourf(ax,X,Y,data');              
             shading(ax,'flat');
             cb=colorbar('peer',ax);
             
             %set(cb,'ytick','auto');
             xlabel(ax,[amevaVar1.name,' (',amevaVar1.units,') ']);
             ylabel(ax,[amevaVar2.name,' (',amevaVar2.units,') ']);
             title(ax,['PDF ',amevaVar1.name,'-',amevaVar2.name]); 
             watchoff;             
        end
          
    end
    %%%% End Estad�stica Descriptica
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%% Regimen Medio
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static=true)
        
        function distributionNormal(panel,amevaVar,txt)
            
            Y=amevaVar.getData();
            titulo=[amevaVar.name,' (',amevaVar.units,')'];


            NPuntos=150;
            [YY,prob]=ameva.DistribucionNPuntos(Y,NPuntos);
            y=YY;

           
            dum=find(prob>=amevaVar.percInit/100 & prob<=amevaVar.percEnd/100);

            x=norminv(prob,0,1);

            % Ajuste de la recta:

            n=length(x(dum));

            xm=mean(x(dum));
            ym=mean(y(dum));
            Sxx=sum(x(dum).^2)-n*xm^2;  % Sxx es el sumatorio entre i=1 y n de (xi-xm)^2
            Sxy=sum(x(dum).*y(dum))-n*xm*ym;
            Syy=sum(y(dum).^2)-n*ym^2;

           

           

            % a0 y a1 son los estimadores de los parametros de la recta de ajuste de
            % minimos cuadrados
            % a1 es la pendiente y a0 es el valor de y para x=0
            a1=Sxy/Sxx;
            a0=ym-a1*xm;
              z=a1*x(dum)+a0;



            SSR=Sxy^2/Sxx;    % suma de cuadrados debidos a la regresion
            SSE=Syy-SSR;      % suma de cuadrados debidos a la desviacion de la linea
            SST=SSR+SSE;      % suma total
            R2=SSR/SST;       % coeficiente de multiple determinacion

            mu=a0;
            sigma=a1;



            P=[0.001 0.01 0.05 0.2 0.5 0.8 0.95 0.99 0.999 0.9999];
            P100=P*100;
            r=length(P); 

            %Con Dib=0 no dibuja los puntos sobre los que ajusta, con Dib=1 si

            util.resetPanel(panel);
            ax=axes('Parent',panel); 

            hold(ax,'on');

            plot(ax,x(dum),z,'-r',x,y,'.k','linewidth',2.5);


            % texto=strcat('Papel Normal en el intervalo:',num2str(100*P1ini,4),'%-',num2str(100*P1fin,4),'%');
            % title(texto)
            title(ax,txt.ameva('grPPNormalTitle'))

            xlabel(ax,txt.ameva('grPPXLabel'));
            set(ax, 'xtick', norminv(P))
            set(ax, 'xticklabel', P)
            ylabel(ax,titulo)

            text1=strcat('\mu=',num2str(mu,4),' \sigma=',num2str(sigma,4),' R^2=',num2str(R2,4));
            legend(ax,text1,4);

            hold(ax,'off');
            box(ax,'on');
            grid(ax,'on');

        end
        
        function distributionLogNormal(panel,amevaVar,txt)
             Y=amevaVar.getData();
            titulo=[amevaVar.name,' (',amevaVar.units,')'];
            
            NPuntos=150;
            [YY,prob]=ameva.DistribucionNPuntos(Y,NPuntos);
            y=log(YY);
            
            dum=find(prob>=amevaVar.percInit/100 & prob<=amevaVar.percEnd/100);
            x=norminv(prob,0,1);

            % Ajuste de la recta:

           n=length(x(dum));

            xm=mean(x(dum));
            ym=mean(y(dum));
            Sxx=sum(x(dum).^2)-n*xm^2;  % Sxx es el sumatorio entre i=1 y n de (xi-xm)^2
            Sxy=sum(x(dum).*y(dum))-n*xm*ym;
            Syy=sum(y(dum).^2)-n*ym^2;

            % a0 y a1 son los estimadores de los parametros de la recta de ajuste de
            % minimos cuadrados
            % a1 es la pendiente y a0 es el valor de y para x=0
            a1=Sxy/Sxx;
            a0=ym-a1*xm;
            z=a1*x(dum)+a0;

            SSR=Sxy^2/Sxx;    % suma de cuadrados debidos a la regresion
            SSE=Syy-SSR;      % suma de cuadrados debidos a la desviacion de la linea
            SST=SSR+SSE;      % suma total
            R2=SSR/SST;       % coeficiente de multiple determinacion

            mu=a0;
            sigma=a1;

            % mu1=exp(mu+(sigma^2)/2);
            % sigma1=(exp(2*mu)*(exp(2*sigma^2)-exp(sigma^2)))^0.5;


            P=[0.001 0.01 0.05 0.2 0.5 0.8 0.95 0.99 0.999 0.9999];
            P100=P*100;
            r=length(P); 
            util.resetPanel(panel);
            ax=axes('Parent',panel); 

            hold(ax,'on');
            plot(ax,x(dum),z,'-r',x,y,'.k','linewidth',2.5);

            % texto=strcat('Papel Lognormal en el intervalo:',num2str(100*Pini,4),'%-',num2str(100*Pfin,4),'%');
            % title(texto)
            title(ax,txt.ameva('grPPLogNormalTitle'));

            xlabel(ax,txt.ameva('grPPXLabel'));
            set(ax, 'xtick', norminv(P))
            set(ax, 'xticklabel', P)

            ylabel(ax,titulo)
            yes=[floor(10*min(Y))/10:ceil(1*(max(Y)-min(Y)))/10:max(Y)+(1*(max(Y)-min(Y)))/10];
            DUM=find(yes~=0);
            set(ax, 'ytick',log(yes(DUM)'))
            set(ax, 'yticklabel', yes(DUM))


            text1=strcat('\mu*=',num2str(mu,4),' \sigma*=',num2str(sigma,4),' R^2=',num2str(R2,4));
            legend(ax,text1,4);
            grid(ax,'on');
            hold(ax,'off');
            box(ax,'on');

        end
        
        function distributionGumbelMax(panel,amevaVar,txt)
            Y=amevaVar.getData();
            titulo=[amevaVar.name,' (',amevaVar.units,')'];
            
            NPuntos=150;
            [y,prob]=ameva.DistribucionNPuntos(Y,NPuntos);
            
             dum=find(prob>=amevaVar.percInit/100 & prob<=amevaVar.percEnd/100);
            n=length(dum);

            x=-log(log(1./prob));
            
            % Ajuste de la recta:

            xm=mean(x(dum));
            ym=mean(y(dum));
            Sxx=sum(x(dum).^2)-n*xm^2;  % Sxx es el sumatorio entre i=1 y n de (xi-xm)^2
            Sxy=sum(x(dum).*y(dum))-n*xm*ym;
            Syy=sum(y(dum).^2)-n*ym^2;
            % a0 y a1 son los estimadores de los parametros de la recta de ajuste de
            % minimos cuadrados
            % a1 es la pendiente y a0 es el valor de y para x=0
            a1=Sxy/Sxx;
            a0=ym-a1*xm;
            z=a1*x(dum)+a0;

            SSR=Sxy^2/Sxx;    % suma de cuadrados debidos a la regresion
            SSE=Syy-SSR;      % suma de cuadrados debidos a la desviacion de la linea
            SST=SSR+SSE;      % suma total
            R2=SSR/SST;       % coeficiente de multiple determinacion

            lambda=a0;
            delta=a1;

            P=[0.05 0.2 0.5 0.8 0.9 0.98 0.995 0.999 0.9999];
            P100=P*100;
            r=length(P); 

            util.resetPanel(panel);
            ax=axes('Parent',panel); 

            hold(ax,'on');
              plot(ax,x(dum),z,'-r',x,y,'.k','linewidth',2.5);


            %     Titulo=strcat('Papel Gumbel para Maximos en el intervalo:',num2str(100*Pini,4),'%-',num2str(100*Pfin,4),'%');
            %     title(Titulo)
           title(ax,txt.ameva('grPPGumbelMaxTitle'));

            xlabel(ax,txt.ameva('grPPXLabel'));
            set(ax, 'xtick', -log(log(1./P)))
            set(ax, 'xticklabel', P)

            ylabel(ax,titulo)

            grid(ax,'on');

            Text=strcat('\lambda=',num2str(lambda,4),' \delta=',num2str(delta,4),' R^2=',num2str(R2,4));
            legend(ax,Text,4);

            hold(ax,'off');
            box(ax,'on');
        end
        
        function distributionWeibullMin(panel,amevaVar,txt)
            Y=amevaVar.getData();
            titulo=[amevaVar.name,' (',amevaVar.units,')'];

            NPuntos=150;
            [YY,prob]=ameva.DistribucionNPuntos(Y,NPuntos);
           dum=find(prob>=amevaVar.percInit/100 & prob<=amevaVar.percEnd/100);
            n=length(dum);
            x=-log(-log(1-prob));
           

            Min=min(Y);
            Max=max(Y);
            M=(Min+Max)/2;

            R=0;
            for lambda=Min-M:0.01:Min-0.01

                y=-log(YY-lambda);

                xm=mean(x(dum));
                ym=mean(y(dum));
                Sxx=sum(x(dum).^2)-n*xm^2;  % Sxx es el sumatorio entre i=1 y n de (xi-xm)^2
                Sxy=sum(x(dum).*y(dum))-n*xm*ym;
                Syy=sum(y(dum).^2)-n*ym^2;


                % a0 y a1 son los estimadores de los parametros de la recta de ajuste de
                % minimos cuadrados
                % a1 es la pendiente y a0 es el valor de y para x=0
                a1=Sxy/Sxx;
                a0=ym-a1*xm;

                SSR=Sxy^2/Sxx;    % suma de cuadrados debidos a la regresion
                SSE=Syy-SSR;      % suma de cuadrados debidos a la desviacion de la linea
                SST=SSR+SSE;      % suma total
                R2=SSR/SST;       % coeficiente de multiple determinacion

                delta=exp(-a0);
                beta=1/a1;

                if R2>R
                    R=R2;
                    Lambda=lambda;
                    Delta=delta;
                    Beta=beta;
                end

            end
            

            P=[0.1 0.5 0.8 0.9 0.99 0.999 0.9999];P=flipud(P')';
            % P=[0.9999 0.999 0.99 0.9 0.8 0.5 0.1];
            P100=P*100;
            r=length(P); 

            yLambda=-log(YY-Lambda);
            z=x(dum).*(1/Beta)-log(Delta);
            
            util.resetPanel(panel);
            ax=axes('Parent',panel); 

            hold(ax,'on');

               plot(ax,x(dum),z,'-r',x,yLambda,'.k','linewidth',2.5);

            %     Titulo=strcat('Papel Weibull para Minimos en el intervalo:',num2str(100*Pini,4),'%-',num2str(100*Pfin,4),'%');
            %     title(Titulo)
            title(ax,txt.ameva('grPPWeibullMinTitle'));

            xlabel(ax,txt.ameva('grPPXLabel'));
            set(ax, 'xtick', (-log(-log(1-P))))
            set(ax, 'xticklabel', (P))
            set(ax, 'xdir', 'reverse')


            %ylabel('-log(Hs-\lambda)')
            ylabel(ax,titulo);
            yes=[floor(10*min(Y))/10:ceil(1*(max(Y)-min(Y)))/10:max(Y)+(1*(max(Y)-min(Y)))/10];yes=flipud(yes')';
            DUM=find(yes-Lambda>0);
            set(ax, 'ytick', (-log(yes(DUM)-Lambda)))
            set(ax, 'yticklabel', (yes(DUM)))
            set(ax, 'ydir','reverse')


            Text=strcat('\lambda=',num2str(Lambda,4),' \delta=',num2str(Delta,4),' \beta=',num2str(Beta,4),' R^2=',num2str(R2,4));
            legend(ax,Text,4);
            grid(ax,'on');
             hold(ax,'off');
            box(ax,'on');
        end        
        
    end
    %%%% end Regimen Medio
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%% Regimen Extremo
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static=true)
        
        function GEV(panel,amevaVar,txt)
            [Ymax,timeMax]=amevaVar.getMaxYearData;
            titulo=[amevaVar.name,' (',amevaVar.units,')'];
            tipdis='GEV';
             % Ajuste a la GEV 
            [fstorage,bestsolution,bestf] =ameva.sce(Ymax,tipdis);

            psifinal=bestsolution(1);
            xifinal=bestsolution(2);
            mufinal=bestsolution(3);

            n=length(bestsolution);
            MIO=zeros(n,n);
    
            for i=1:n  % columnas
                for j=1:n  % filas
                    MIO(i,j) = ameva.derivadasegunda(bestsolution,i,j,Ymax);
                end
            end
            J=inv(MIO);
    

            Yord=sort(Ymax);

            ndatos=length(Ymax);
            for i=1:ndatos
            Prob(i)=i/(ndatos+1);
            end

            RRR=-1./log(Prob) ;

            PeriodoRetorno=[0.5 1 2 3 4 5 6 7 8 9 10 20 30 40 50 60 70 80 90 100 120 140 160 180 200];
            for i=1:length(PeriodoRetorno)

            if length(tipdis)<10
            Yn(i)=mufinal- (psifinal./xifinal.* ( 1-(-log(    (exp(-1/PeriodoRetorno(i)))     )).^(-xifinal) ) );

            else
            Yn(i)=mufinal- psifinal.*log(-log(exp(-1/PeriodoRetorno(i))) ) ;
            end

            end
            %-------------------------------------------------------------------------------------

            % OBTENCION DEL INTERVALO DE CONFIANZA PARA UN CUANTIL:
            % Periodo de Retorno, a�os:
            n=length(bestsolution);

            for k=1:length(PeriodoRetorno)

            P(k)=exp(-1./PeriodoRetorno(k));

            ic=zeros(1,1);
            if length(tipdis)<10 % Funcion GEV

            for i=1:n
                for j=1:n
                    Zp1=ameva.derivadaprimera(i,bestsolution,P(k));
                    Zp2=ameva.derivadaprimera(j,bestsolution,P(k));
                    ic=ic+(Zp1*Zp2*J(i,j));
                end
            end
            ic=ic.^0.5;
            icsup95(k)=Yn(k)+1.96*ic;
            icinf95(k)=Yn(k)-1.96*ic;

            else  % funcion GUMBEL

            for i=1:nn
                for j=1:nn
                    Zp1=ameva.derivadaprimera(i,pargumbel,P(k));
                    Zp2=ameva.derivadaprimera(j,pargumbel,P(k));
                    ic=ic+(Zp1*Zp2*J(i,j));
                end
            end
            ic=ic.^0.5;
            icsup95(k)=Yn(k)+1.96*ic;
            icinf95(k)=Yn(k)-1.96*ic;

            end

            end

            % -------------------------------------------------------------------------
            % Relacion R-Variable:

            util.resetPanel(panel);
            ax=axes('Parent',panel); 
            hold(ax,'on');


            ejex=[2 5 10 25 50 100 200];
            hh=plot(ax,log(PeriodoRetorno),Yn,'-b','LineWidth',1.5);
            plot(ax,log(RRR),Yord,'.k')
            plot(ax,log(PeriodoRetorno),icsup95,'-.k');
            plot(ax,log(PeriodoRetorno),icinf95,'-.k');

            axis(ax,[log(RRR(1)) log(250) 0 max(Yn)+2])
            set(ax,'XTick',log(ejex));
            set(ax,'XTickLabel',{ejex})
            %title('Return level plot');
            %set(gca,'Fontsize',7.5)
            

            xlabel(ax,txt.ameva('grGevXlabel'));
            ylabel(ax,titulo,'Fontsize',10)
            grid(ax,'on');
            title(ax,txt.ameva('grGevTitle'));
            text1=strcat('\mu = ',num2str(sprintf('%1.3f',mufinal)),' \xi = ',num2str(sprintf('%1.3f',xifinal)),' \psi = ',num2str(sprintf('%1.3f',psifinal)));
            legend(ax,text1,4)
        end
        
    end
    %%%% end Regimen Extremo
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end


function [Q,Dq] = QuantilePOT (F,u,lambda0,sigma0,gamma0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function Quantile calculates the quantile q associated with a given
%   parameterization
%
%   Input:
%       F -> Probability
%       beta0 -> Optimal constant parameter related to location
%       alpha0 -> Optimal constant parameter related to scale
%       gamma0 -> Optimal constant parameter related to shape
%       
%   Output:
%
%       Q -> Quantile related to the given probability
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Check the number of input arguments
    if nargin<5,
        gamma0 = [];
    end
    if isempty(gamma0),
        neps0 = 0;
    else
        neps0 = 1;
    end
    %   Evaluate the location parameter at each time t as a function
    %   of the actual values of the parameters given by p
    lambt = lambda0*ones(size(F));
    %   Evaluate the scale parameter at each time t as a function
    %   of the actual values of the parameters given by p
    sigt = exp(sigma0)*ones(size(F));
    %   Evaluate the shape parameter at each time t as a function
    %   of the actual values of the parameters given by p
    if neps0
        epst = gamma0*ones(size(F));
    else
        epst = zeros(size(F));
    end

    %   The values whose shape parameter is almost cero corresponds to
    %   the GUMBEL distribution, locate their positions if they exist
    posG = find(abs(epst)<=1e-8);
    %   The remaining values correspond to WEIBULL or FRECHET
    pos  = find(abs(epst)>1e-8);
    %   The corresponding GUMBEl values are set to 1 to avoid 
    %   numerical problems, note that for those cases the GUMBEL
    %   expressions are used
    epst(posG)=1;

    
    %   Evaluate the quantile, not
    %   that the general and Gumbel expresions are used
    Q = zeros(size(F));
    Q(pos) = u - (1-(-log(F(pos))./lambt(pos)).^(-epst(pos))).*sigt(pos)./epst(pos);

    Q(posG) = u - sigt(posG).*log(-log(F(posG))./lambt(posG));
    
    if nargout==2,
        Dq = zeros(2+neps0,length(F));
        Dqlambt(pos) = sigt(pos)./lambt(pos).*(-log(F(pos))./lambt(pos)).^(-epst(pos));
        Dqsigt(pos) = -(1-(-log(F(pos))./lambt(pos)).^(-epst(pos)))./epst(pos);
        Dqepst(pos) = sigt(pos).*(1-(-log(F(pos))./lambt(pos)).^(-epst(pos)).*(1+epst(pos).*log(-log(F(pos))./lambt(pos))))./(epst(pos).*epst(pos));

        %   Gumbel derivatives
        Dqlambt(posG) = sigt(posG)./lambt(posG);
        Dqsigt(posG) = -log(-log(F(posG))./lambt(posG));
        Dqepst(posG) =zeros(size(lambt(posG)));
        
        Dq = sparse(zeros(2+neps0,length(F)));
        %   Jacobian elements related to the location parameters beta0
        %   and beta
        Dq(1,:) = Dqlambt;
        %   Jacobian elements related to the scale parameters alpha0
        %   and alpha
    	Dq(2,:) = Dqsigt.*(sigt');
        %   Jacobian elements related to the shape parameters gamma0
        %   and gamma
        if neps0 == 1,
            Dq(2+neps0,:) = Dqepst;
        end  
    end

end




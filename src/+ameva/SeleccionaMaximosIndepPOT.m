function [ymax,fecha,duracion,datemax,xmax,ini_fin]=SeleccionaMaximosIndepPOT(Serie,u,indtemp)
% =========================================================================
%Programa para sacar los maximos relativos de una serie temporal de oleaje
%sobre un umbral dado
%
% Datos de partida: 
%   Serie  -----  variable de estudio con fechas y mediciones
%                 [datenum yyyy mm dd hh Hs]
%   u ----------  umbral de corte determinado
%   indtemp ----  independencia entre temporales (dias)

% Datos de salida: 
%   fecha ------  [datenum yyyy mm dd hh]
%   ymax -------  xi - umbral
%   duracion ---  tiempo de duracion de cada temporal
%   xmax -------  encuetro los maximos anuales xmax
%   datemax ----  encuetro los maximos anuales datemax
%   ini_fin ----- [ini fin] inicio y fin de cada temporal
% =========================================================================

%maximos yi:
Serie(:,6)=Serie(:,6)-u;

% Quito los valores q estan por debajo del umbral:
nt=length(Serie(:,6));
dum=find(Serie(:,6)<=0);
Serie(dum,6)=NaN;

%--------------------------------------------------------------------------
% Deteccion cdo no hay valores extremales:
indiNaN=isnan(Serie(:,6));
%OJO OJO se pierda la referenci de los indice
%OJO OJO como solo interesa los valores se queda con ellos OJO OJO auiq

indicesyi=find(indiNaN(:)==0); % indices donde si hay valores extremales
nyi=length(indicesyi);
clear indiNaN

ini_fin=[];  %creo vector para inicios y finales de eventos extremales
indice=ones(nt,1)*NaN;  %creo vector para indices de eventos extremales
duracion=ones(nt,1)*NaN; %creo vector para duraciones de eventos extremales
ymax=ones(nt,1)*NaN;    %creo vector para los valores maximos de cada temporal
%--------------------------------------------------------------------------
if nyi>2, %OJO lo he cambiado OC el if
    j=1;    %contador duracion
    k=1;    %contador indice
    m=1;    %contador de inicios
    for i=2:nyi %escaneo todos los datos extremales
        % OJO OJO que pasa si los tres datos son continuos OJO no pasan por
        % aqui OJO OJO
        if indicesyi(i)-indicesyi(i-1)==1 %si el dato es continuo
            j=j+1;
        else    % si termina el temporal:
            iniciotemporal=indicesyi((i-1)-(j-1));
            fintemporal=indicesyi(i-1);
            maximo=max(Serie(iniciotemporal:fintemporal,6));
            posi=find(Serie(iniciotemporal:fintemporal,6)==maximo);
            posimax=iniciotemporal+(posi(1)-1); % si los ymax son = cojo el primero
            inicio=Serie(iniciotemporal,1);
            final=Serie(fintemporal,1);
            ini_fin(m,:)=([inicio,final]);
            m=m+1;
            indice(posimax)=k;
            k=k+1;
            duracion(posimax)=j;
            j=1;
            ymax(posimax)=maximo;
        end
    end
    clear posi posimax maximo iniciotemporal fintemporal inicio final
    clear j k m nyi nt indicesyi
elseif nyi==1, %OJO lo he cambiado OC
    indice(indicesyi)=0;
    duracion(indicesyi)=1;
    ymax(indicesyi)=Serie(indicesyi,6);
elseif nyi==2, %OJO lo he cambiado OC
    maximo=max(Serie(indicesyi,6));
    posi=find(Serie(indicesyi,6)==maximo);
    indice(indicesyi(posi))=0;
    duracion(indicesyi(posi))=1;
    ymax(indicesyi(posi))=Serie(indicesyi(posi),6);
end

%--------------------------------------------------------------------------
%Selecciono donde hay datos:
matriz01=isnan(indice);
indicesbuenos=find(matriz01==0);

fecha=Serie(indicesbuenos,[1 2 3 4 5]);
ymax=ymax(indicesbuenos);
duracion=duracion(indicesbuenos); 
Indice=indicesbuenos;

clear matriz01 indicesbuenos;
%--------------------------------------------------------------------------
% Eliminacion de mini-picos:
ndato0=length(Indice);
ndato=10000;
while ndato0>1 && ndato~=ndato0 %OJO OJO
   ndato0=length(ymax);
    for i=2:ndato0
        if ndato>i & (fecha(i,1)-fecha(i-1,1)<=indtemp)  % duracion entre tormentas menor a indtemp
            
            ymenor=min(ymax(i-1),ymax(i));
            posi=find(ymax((i-1):i)==ymenor); 
            posimin=(i-1)+(posi(1)-1);  % si los ymax son = cojo uno
            
            fecha(posimin,:)=NaN;
            ymax(posimin)=NaN;
            duracion(posimin)=NaN; 
            ini_fin(posimin,:)=NaN;
            Indice(posimin)=NaN;
            
            %Selecciono donde hay datos:
            matriz01=isnan(ymax);
            indicesbuenos=find(matriz01==0);
            
            fecha=fecha(indicesbuenos,[1 2 3 4 5]);
            ymax=ymax(indicesbuenos);
            duracion=duracion(indicesbuenos); 
            ini_fin=ini_fin(indicesbuenos,:);
            Indice=Indice(indicesbuenos);
        end
        ndato=length(ymax);
    end
end

ymax=ymax+u;%maximos POT pueden haber varios en un anos

%--------------------------------------------------------------------------
% encuetro los maximos anuales xmax datemax uno por cada ano
xmax=[];datemax=[];
if ~isempty(ymax)
    n1=ceil(fecha(1,2));
    ny=ceil(fecha(end,2))-floor(fecha(1,2))+1;
    k=1;
    for i=1:ny
        aux=find(fecha(:,2)==n1+i-1);
        if ~isempty(aux)
            [maxi,posi]=max(ymax(aux));
            xmax(k)=maxi;
            datemax(k)=fecha(aux(posi),1);
            k=k+1;
        end
    end
    xmax=xmax(:);datemax=datemax(:);
end

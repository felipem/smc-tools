function [x_min,x_max] = bounds
% This scriopt reads from an external file the lower and upper bound for
% every parameter. Two columns: x_min x_max
%
% The variable 'file' must contain the name of the file

% file='limites.txt';
% aux=load(file);
aux=[-1000 2000;-1 1;0.001 2000];

x_min=aux(1:end,1)';     
x_max=aux(1:end,2)';
function [mu,sigma]=calculeTrendValues(param,serie,significative,year)

   mu=significative(1)*param.mu0+significative(2)*param.mu1*year;
   sigma=significative(3)*param.sigma0+significative(4)*param.sigma1*year;
        
    if isreal(serie)
        if ~significative(1) && ~significative(2)
            mu=mean(serie);
        end
        if ~significative(4) && ~significative(3)
            sigma=std(serie);
        end
        
    else %es direccional
        if ~significative(1) && ~significative(2)
            mu=atan2(sum(imag(serie)),sum(real(serie)))*180/pi;
            if mu<0,mu=mu+360;end
        end        
        
         if ~significative(3) && ~significative(4)
%             sigma=atan2(std(imag(serie)),std(real(serie)))*180/pi;
%             if sigma<0,sigma=sigma+360;end
              sigma=param.sigma0;
         end
        
    end
    
     
    
    
end
function g = derivadaprimera(k,estimadores,P)

% n = numero de parametros
n=length(estimadores);
h=0.0001;

% g(1=psi  ,  2=xi ,  3=mu)
% derivada1 = a-b/c  = (Zp(estrucaos)-Zp)/delta


% restando:
b=ameva.gx(estimadores,P);


% 1er termino:
estimadores(k)=estimadores(k)+h;
a=ameva.gx(estimadores,P);

g=(a-b)/h;




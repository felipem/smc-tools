function x = derivadasegunda(bestsolution,i,j,Ymax)

% n = numero de parametros
n=length(bestsolution);

h=0.0001;
alfai=zeros(1,n);

alfai(i)=1; alfai(j)=alfai(j)+1;

% sumando 1:
x=ameva.flogMLE(bestsolution,Ymax);

estrucao=[];
for k=1:n
    estrucao(k)=bestsolution(k)+alfai(k).*h;
end

% sumando 2:
x=x+ameva.flogMLE(estrucao,Ymax);

restando=0;
for k=1:n
    estrucao2=bestsolution;
    estrucao2(k)=estrucao2(k)+h;
    
    restando=restando-alfai(k).*ameva.flogMLE(estrucao2,Ymax);
    
    clear estrucao2;
end

x=x+restando;
x=-x./(h.^2);

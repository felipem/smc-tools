function l = flogMLE(estimadores,Y_vector);
global tipdis

n=length(Y_vector);

l=0;
% -------------------------------------------------------------------------
if length(tipdis)>10
    for i=1:n
        l=l+( -log(estimadores(1))...
            - ( (Y_vector(i)-estimadores(2))/estimadores(1) )...
            - exp(-((Y_vector(i)-estimadores(2))/estimadores(1)))...
            );
    end
    
else
    
    for i=1:n
        if (1+estimadores(2).*(Y_vector-estimadores(3))./estimadores(1))>0
            l=l+( -log(estimadores(1))-(1+1/estimadores(2)).*log(1+estimadores(2).*(Y_vector(i)-estimadores(3))./estimadores(1)) - ((1+estimadores(2).*(Y_vector(i)-estimadores(3))./estimadores(1)).^(-1/estimadores(2))));
        else
            l=-10000000000000;
        end
        
    end
    
end

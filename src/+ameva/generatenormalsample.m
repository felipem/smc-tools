function  [z] = generatenormalsample(z0,sigma,x_min,x_max);

n=length(z0);
for k=1:n
    z(k)=z0(k)+sigma(k).*(x_max(k)-x_min(k)).*normrnd(0,1,1,1); 
    while (z(k)<x_min(k)|z(k)>x_max(k))
        z(k)=z0(k)+sigma(k).*(x_max(k)-x_min(k)).*normrnd(0,1,1,1); 
    end
end
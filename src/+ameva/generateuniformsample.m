function [x] = generateuniformsample (x_min,x_max)

n=length(x_min);

x(1:n)=x_min+(x_max-x_min).*rand(1,n);




function [trend,param,residuo,outliers]=getTrend(time,data,k,alpha,conf)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   k es el numero de modelo
%     [mu_lineal mu_cuadra sig_lineal sig_cuadra]
%     [0 0 0 0] -> k=0 -> fmu = p1; fsigma = p4
%     [0 0 0 1] -> k=1 -> fmu = p1; fsigma = p4+p6*z^2
%     [0 0 1 0] -> k=2 -> fmu = p1; fsigma = p4+p5*z
%     [0 0 1 1] -> k=3 -> fmu = p1; fsigma = p4+p5*z+p6*z^2
%     [0 1 0 0] -> k=4 -> fmu = p1+p3*z^2; fsigma = p4
%     [0 1 0 1] -> k=5 -> fmu = p1+p3*z^2; fsigma = p4+p6*z^2
%     [0 1 1 0] -> k=6 -> fmu = p1+p3*z^2; fsigma = p4+p5*z
%     [0 1 1 1] -> k=7 -> fmu = p1+p3*z^2; fsigma = p4+p5*z+p6*z^2
%     [1 0 0 0] -> k=8 -> fmu = p1+p2*z; fsigma = p4
%     [1 0 0 1] -> k=9 -> fmu = p1+p2*z; fsigma = p4+p6*z^2
%     [1 0 1 0] -> k=10 -> fmu = p1+p2*z; fsigma = p4+p5*z
%     [1 0 1 1] -> k=11 -> fmu = p1+p2*z; fsigma = p4+p5*z+p6*z^2
%     [1 1 0 0] -> k=12 -> fmu = p1+p2*z+p3*z^2; fsigma = p4
%     [1 1 0 1] -> k=13 -> fmu = p1+p2*z+p3*z^2; fsigma = p4+p6*z^2
%     [1 1 1 0] -> k=14 -> fmu = p1+p2*z+p3*z^2; fsigma = p4+p5*z
%     [1 1 1 1] -> k=15 -> fmu = p1+p2*z+p3*z^2; fsigma = p4+p5*z+p6*z^2

%   alpha=Intervalo de confianza
%   conf=Umbral de los outliers


%   Dimensions
    [n m] = size(time);
    if min(n,m)>1,
        error('Data time must be a row or column vector')
    elseif n==1,
        %   Transform data to a column vector
        time = time';
        n = m;
        m = 1;
    end
    [n1 m1] = size(data);
    if min(n1,m1)>1,
        error('Data data must be a row or column vector')
    elseif n1==1,
        %   Transform data to a column vector
        data = data';
        n1 = m1;
        m1 = 1;
    end
    if n1~=n,
       error('Data time and data must be consistent') 
    end
    
    if nargin<3, k = 10; end
    if isempty(k), k = 10; end
    if nargin<4, alpha = 0.95; end
    if isempty(alpha), alpha = 0.95; end

%   Una vez seleccionado el mejor modelo por AKAIKE lo ajusto de nuevo
%   Defino el modelo binario
mo = dec2bin(k,4);
model = zeros(4,1);
for i = 1:4,
    model(i) = str2num(mo(i));
end
%   Valores iniciales de las variables
if ~model(1) && ~model(2),
%       Model: fmu = p1+p2*time+p3*time^2
%              fsigma = p4+p5*time+p6*time^2 
    beta = mean(data);
    stde = std(data);
    X = [ones(n,1)];
elseif model(1) && ~model(2),
%   Initial estimates for the parameters
    X = [ones(n,1) time];
    %   Solucion de las ecuaciones normales
    L = chol(X'*X,'lower'); 
    Xy = X'*data;
    %   Parameter estimates with the linear model
    beta = L'\(L\Xy);
    %   Residual standard deviation
    stde = std(data-beta(1)-beta(2)*time);
elseif ~model(1) && model(2)
    %   Initial estimates for the parameters
    X = [ones(n,1) time.^2];
    %   Solucion de las ecuaciones normales
    L = chol(X'*X,'lower'); 
    Xy = X'*data;
    %   Parameter estimates with the linear model
    beta = L'\(L\Xy);
    beta = [beta(1);0;beta(2)];
    %   Residual standard deviation
    stde = std(data-beta(1)-beta(2)*time.^2);
else
    %   Initial estimates for the parameters
    X = [ones(n,1) time time.^2];
    %   Solucion de las ecuaciones normales
    L = chol(X'*X,'lower'); 
    Xy = X'*data;
    %   Parameter estimates with the linear model
    beta = L'\(L\Xy);
    %   Residual standard deviation
    stde = std(data-beta(1)-beta(2)*time-beta(3)*time.^2);
end   
%   Initial values
pini = zeros(2+sum(model),1);
pini(1) = beta(1);
pos = 2;
if model(1),
    pini(pos) = beta(2);  %param(2)
    pos = pos+1;
end
if model(2),
    pini(pos) = beta(3);  %param(3)
    pos = pos+1;
end
pini(pos)=stde;

%   Parameter estimation using the maximum likelihood method
[param,fval,exitflag,output,lambda,grad,hessian,residuo]=  OptiParamHessian (time,data,model,pini,'sinconstraints');
fsigma = param(pos)*ones(size(time));
pos = pos + 1;
if model(3),
    fsigma = fsigma + param(pos)*time;
    pos = pos + 1;
end
if model(4),
    fsigma = fsigma + param(pos)*time.^2;
    pos = pos + 1;
end
if any(fsigma<0)
    [param,fval,exitflag,output,lambda,grad,hessian,residuo]=  OptiParamHessian (time,data,model,pini,'conconstraints');
end

%   LU decomposition of the Information matrix
[LX,UX,PX] = lu(hessian);
%   Inverse
invI0 = (UX)\(LX\(PX*eye(length(param))));
%    Standard deviation
stdpara = sqrt(diag(invI0));
%   Bounds
upper = param+tinv(1-(1-alpha)/2,length(time)-length(param)-1)*stdpara;
lower = param-tinv(1-(1-alpha)/2,length(time)-length(param)-1)*stdpara;

%   Calculo del pvalue (NO PONER EL param-VALUE)
pvalue = zeros(size(param));
% % % for i=1:length(param),
% % %     if param(i)>0,
% % %         pvalue(i) = tcdf((0-param(i))./stdpara(i),length(time)-length(param)-1);
% % %     else
% % %         pvalue(i) = 1-tcdf((0-param(i))./stdpara(i),length(time)-length(param)-1);
% % %     end
% % % end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Fin del ajuste
%   Second order derivatives with respecto to data and parameters
%       Model: fmu = p1+p2*time+p3*time^2
%              fsigma = p4+p5*time+p6*time^2
%   Expected values
fmu = param(1)*ones(size(time)); 
pos = 2;
if model(1),
    fmu = fmu + param(pos)*time;
    pos = pos + 1;
end
if model(2),
    fmu = fmu + param(pos)*time.^2;
    pos = pos + 1;
end
fsigma = param(pos)*ones(size(time));
pos = pos + 1;
if model(3),
    fsigma = fsigma + param(pos)*time;
    pos = pos + 1;
end
if model(4),
    fsigma = fsigma + param(pos)*time.^2;
    pos = pos + 1;
end
%   Intervalos de confianza de los valores medios y de las predicciones
aux = ones(size(time));
if model(1),
    aux = [aux time];
end
if model(2),
    aux = [aux time.^2];
end
if sum(model(1:2))>0,
    stdmean = sqrt(sum(((aux*invI0(1:1+sum(model(1:2)),1:1+sum(model(1:2)))).*aux)')');
else
    stdmean = sqrt(((aux*invI0(1:1+sum(model(1:2)),1:1+sum(model(1:2)))).*aux));
end
uppermean = fmu+tinv(1-(1-alpha)/2,length(time)-length(param)-1)*stdmean;
lowermean = fmu-tinv(1-(1-alpha)/2,length(time)-length(param)-1)*stdmean;
stdpredict = sqrt(fsigma.^2+stdmean.^2);
upperpredict = fmu+tinv(1-(1-alpha)/2,length(time)-length(param)-1)*stdpredict;
lowerpredict = fmu-tinv(1-(1-alpha)/2,length(time)-length(param)-1)*stdpredict;

if nargin<5,
    conf = [];
end

trend.mu=fmu;
trend.upper=upperpredict;
trend.lower=lowerpredict;
trend.upperParam=upper;
trend.lowerParam=lower;

if ~isempty(conf),
    %   Derivatives   
    Df1 = ones(size(time));
    Df2 = time;
    Df3 = time.^2;
    Ds1 = ones(n,1);
    Ds2 = time;
    Ds3 = time.^2;

    if ~isempty(find(fsigma<0)),
        error('La varianza de cada dato ha de ser positiva')
    end

    %   Second Order Cross Derivatives
    D2etay(:,1)=(Df1./(fsigma.^2));
    pos = 2;
    if model(1),
        D2etay(:,pos)=(Df2./(fsigma.^2));
        pos = pos + 1;
    end
    if model(2),
        D2etay(:,pos)=(Df3./(fsigma.^2));
        pos = pos + 1;
    end
    D2etay(:,pos)=2*(Ds1.*residuo./(fsigma.^3));
    pos = pos + 1;
    if model(3),
        D2etay(:,pos)=2*(Ds2.*residuo./(fsigma.^3));
        pos = pos + 1;
    end    
    if model(4),
        D2etay(:,pos)=2*(Ds3.*residuo./(fsigma.^3));
    end

    % % % Dfx = param(2)*fmu./time; 
    % % % Dsx = param(4)*param(5)*time.^(param(5)-1);
    % % % D2fx1 = Dfx/param(1);
    % % % D2fx2 = fmu.*(1+param(2)*log(time/z0))./time;
    % % % D2sx3 = zeros(n,1);
    % % % D2sx4 = param(5)*time.^(param(5)-1);
    % % % D2sx5 = param(4)*time.^(param(5)-1).*(1+param(5)*log(time));
    % % % D2etax(:,1)=-(Df1.*Dfx./(fsigma.^2))-2*(Df1.*Dsx.*residuo./(fsigma.^3))+(D2fx1.*residuo./(fsigma.^2));
    % % % D2etax(:,2)=-(Df2.*Dfx./(fsigma.^2))-2*(Df2.*Dsx.*residuo./(fsigma.^3))+(D2fx2.*residuo./(fsigma.^2));
    % % % D2etax(:,3)=(Ds1.*Dsx./(fsigma.^2))-(D2sx3./(fsigma))-2*(Ds1.*Dfx.*residuo./(fsigma.^3))-3*(Ds1.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx3.*residuo.^2./(fsigma.^3));
    % % % D2etax(:,4)=(Ds2.*Dsx./(fsigma.^2))-(D2sx4./(fsigma))-2*(Ds2.*Dfx.*residuo./(fsigma.^3))-3*(Ds2.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx4.*residuo.^2./(fsigma.^3));
    % % % D2etax(:,5)=(Ds3.*Dsx./(fsigma.^2))-(D2sx5./(fsigma))-2*(Ds3.*Dfx.*residuo./(fsigma.^3))-3*(Ds3.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx5.*residuo.^2./(fsigma.^3));

    %   Derivatives of eta (optimal parameters) with respect to data
    DetaY = (invI0*D2etay')';
    %   Derivatives of eta (optimal parameters) with respect to time
    % % % DetaX = (invI0*D2etax')';
    if ~model(1) && ~model(2),
    % % %     DmuY=sum([Df1]'.*DetaY(:,1)')';
        DmuY=0;
    elseif model(1) && ~model(2),
        DmuY=sum([Df1 Df2]'.*DetaY(:,1:2)')';
    elseif ~model(1) && model(2),
        DmuY=sum([Df1 Df3]'.*DetaY(:,1:2)')';
    else
        DmuY=sum([Df1 Df2 Df3]'.*DetaY(:,1:3)')';
    end
    % % % DsigmaY = sum([Ds1 Ds2 Ds3]'.*DetaY(:,3:5)')';
    % % % DmuX=sum([Df1 Df2]'.*DetaX(:,1:2)')';
    % % % DsigmaX = sum([Ds1 Ds2 Ds3]'.*DetaX(:,3:5)')';

    % % % Derivatives = [DmuY DsigmaY DmuX DsigmaX];

    %   Diagonal of the sensitivity matrix
    S=ones(length(time),1)-DmuY;
    %   All sensitivity matrix
    % % % Saux = eye(length(time))-[Df1 Df2]*DetaY(:,1:2)';

    Omega=(S).^2.*(fsigma).^2;

    rN=residuo./sqrt(Omega);

    % rNadj = mle(rN, 'dist','tlocationscale');
    % rNStudent = (rN-rNadj(1))/rNadj(2);
    % id = find(abs(rNStudent)>tinv(1-(1-alpha)/2,rNadj(3)));

    % rNadj = mle(rN, 'dist','tlocationscale');
    % rNStudent = (rN-rNadj(1))/rNadj(2);
    % id = find(abs(rNStudent)>tinv(1-1/(length(time)+1),rNadj(3)));


    for i=1:length(conf),
        id{i} = find(abs(rN)>norminv(1-(1-conf(i))/2,0,1));
    end
else
        id = {};
        rN = [];
        Omega = [];
end
outliers=id;

end

function [param,f,exitflag,output,lambda,grad,hessian,residuo] =  OptiParamHessian (time,data,bin,pini,type,lb,ub)

%   Initial bounds and possible initial values for the constant parameters
if ~isempty(pini),
    param = pini;
else
    param = zeros(2+sum(bin),1);
end

if isempty(type)
    type = 'sinconstraints';
end

if nargin<6 || isempty(lb),
    lb = -Inf*ones(length(param),1);
end
if nargin<7 || isempty(lb),
    ub = Inf*ones(length(param),1);
end

% options = optimset('GradObj','off','Hessian','off','TolFun',1e-12,'MaxFunEvals',1000,'MaxIter',1000);
% [paaa,faaa,exitflagaaa,outputaaa,lambdaaaa,gradaaa,hessianaaa] = fmincon(@(param) loglikelihood (param,time,data,ID),param,[],[],[],[],lb,up,[],options);

%   Set the options for the optimization routine, note that Gradients are
%   provided but not Hessian
%   Call the optimization routine
%   Note that it is a minimization problem, instead a maximization problem,
%   for this reason the log likelihood function sign is changed
% % % options = optimset('GradObj','off','Hessian','off','TolFun',1e-12,'MaxFunEvals',1000,'MaxIter',1000);
% % % [param,f,exitflag,output,lambda,grad,hessian] = fmincon(@(param) loglikelihood (param,time,data),param,[],[],[],[],lb,up,[],options);
options = optimset('GradObj','on','Hessian','on','TolFun',1e-8,'TolCon',1e-8,'TolX',1e-8,'MaxFunEvals',1000000,'MaxIter',10000);
aux = -[zeros(length(time),1+sum(bin(1:2)))  ones(size(time))];
if bin(3),
    aux = [aux -time];
end
if bin(4),
    aux = [aux -time.^2];
end

if strcmp(type,'sinconstraints')
    if bin(2),
    
    end
    if bin(4),
    
    end
    [param,f,exitflag,output,lambda,grad,hessian] = fmincon(@(param) loglikelihood (param,time,data,bin),param,[],[],[],[],lb,ub,[],options);
elseif strcmp(type,'conconstraints')
    [param,f,exitflag,output,lambda,grad,hessian] = fmincon(@(param) loglikelihood (param,time,data,bin),param,aux,zeros(size(time)),[],[],lb,ub,[],options);
else
    error('El tipo de modelo ha de ser "sinconstraints" o "conconstraints"')
end
[f grad hessian residuo] = loglikelihood (param,time,data,bin);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Loglikelihood function definition of the nonlinear regression with 
%   variance being a function of time (HETEROCEDASTIC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [f Jx Hxx residuo] = loglikelihood (param,time,data,bin)
        %   bin is a binary vector which establishes the kind of model
        %   This function calculates the loglikelihood function with the
        %   sign changed for the values of the paremeters given by param
        n = max(size(data));
        %   It Calculates the gradients of each parameter param
        fmu = param(1)*ones(size(time)); 
        pos = 2;
        if bin(1),
            fmu = fmu + param(pos)*time;
            pos = pos + 1;
        end
        if bin(2),
            fmu = fmu + param(pos)*time.^2;
            pos = pos + 1;
        end
        fsigma = param(pos)*ones(size(time));
        pos = pos + 1;
        if bin(3),
            fsigma = fsigma + param(pos)*time;
            pos = pos + 1;
        end
        if bin(4),
            fsigma = fsigma + param(pos)*time.^2;
            pos = pos + 1;
        end
    
        %   Evaluate the loglikelihood function 
        f =  -0.5*sum(log(2*pi*fsigma.^2))-0.5*sum(((data-fmu)./fsigma).^2);
        %   Convert a maximization problem into a minimization problem
        f=-f;
        
        %   Jacobian of the log-likelihood function
        if nargout >1,
            Jx = zeros(2+sum(bin),1);
            
%       Model: fmu = p1+p2*time+p3*time^2
%              fsigma = p4+p5*time+p6*time^2
            Df1 = ones(size(time));
            Df2 = time;
            Df3 = time.^2;
            Ds1 = ones(n,1);
            Ds2 = time;
            Ds3 = time.^2;
  
            Jx(1)=sum(Df1.*(data-fmu)./fsigma.^2);  %param(1)
            pos = 2;
            if bin(1),
                Jx(pos)=sum(Df2.*(data-fmu)./fsigma.^2);  %param(2)
                pos = pos+1;
            end
            if bin(2),
                Jx(pos)=sum(Df3.*(data-fmu)./fsigma.^2);  %param(3)
                pos = pos+1;
            end
            Jx(pos)=-sum(Ds1./fsigma)+sum(Ds1.*(data-fmu).^2./fsigma.^3);  %param(4)
            pos = pos+1;
            if bin(3),
                Jx(pos)=-sum(Ds2./fsigma)+sum(Ds2.*(data-fmu).^2./fsigma.^3);  %param(5)
                pos = pos+1;
            end
            if bin(4),
                Jx(pos)=-sum(Ds3./fsigma)+sum(Ds3.*(data-fmu).^2./fsigma.^3);  %param(6)
            end
            Jx=-Jx;
        end
        
        %   Hessian of the log-likelihood function
        if nargout >2,
            Hxx = zeros(6);           
%       Model: fmu = p1+p2*time+p3*time^2
%              fsigma = p4+p5*time+p6*time^2
%   Para este modelo todas las derivadas son nulas
            D2f11 = zeros(n,1);
            D2f22 = zeros(n,1);
            D2f33 = zeros(n,1);
            D2f12 = zeros(n,1);
            D2f13 = zeros(n,1);
            D2f23 = zeros(n,1);
            D2s11 = zeros(n,1);
            D2s22 = zeros(n,1);
            D2s33 = zeros(n,1);
            D2s12 = zeros(n,1);
            D2s13 = zeros(n,1);
            D2s23 = zeros(n,1);
                
            Hxx(1,1) = sum(1./fsigma.^2.*((data-fmu).* D2f11 - Df1.^2));
            Hxx(2,2) = sum(1./fsigma.^2.*((data-fmu).* D2f22 - Df2.^2)); 
            Hxx(3,3) = sum(1./fsigma.^2.*((data-fmu).* D2f33 - Df3.^2));
            Hxx(1,2) = sum(1./fsigma.^2.*((data-fmu).* D2f12 - Df1.*Df2)); 
            Hxx(1,3) = sum(1./fsigma.^2.*((data-fmu).* D2f13 - Df1.*Df3)); 
            Hxx(2,3) = sum(1./fsigma.^2.*((data-fmu).* D2f23 - Df2.*Df3)); 
            Hxx(4,4) = -sum(1./fsigma.^2.*(fsigma.* D2s11 - Ds1.^2))+sum((data-fmu).^2./fsigma.^3.*(D2s11 - 3./fsigma.* Ds1.^2));
            Hxx(5,5) = -sum(1./fsigma.^2.*(fsigma.* D2s22 - Ds2.^2))+sum((data-fmu).^2./fsigma.^3.*(D2s22 - 3./fsigma.* Ds2.^2));
            Hxx(6,6) = -sum(1./fsigma.^2.*(fsigma.* D2s33 - Ds3.^2))+sum((data-fmu).^2./fsigma.^3.*(D2s33 - 3./fsigma.* Ds3.^2));
            Hxx(4,5) = -sum(1./fsigma.^2.*(fsigma.* D2s12 - Ds1.*Ds2))+sum((data-fmu).^2./fsigma.^3.*(D2s12 - 3./fsigma.* Ds1.*Ds2));
            Hxx(4,6) = -sum(1./fsigma.^2.*(fsigma.* D2s13 - Ds1.*Ds3))+sum((data-fmu).^2./fsigma.^3.*(D2s13 - 3./fsigma.* Ds1.*Ds3));
            Hxx(5,6) = -sum(1./fsigma.^2.*(fsigma.* D2s23 - Ds2.*Ds3))+sum((data-fmu).^2./fsigma.^3.*(D2s23 - 3./fsigma.* Ds2.*Ds3));
            Hxx(1,4) = -2*sum((data-fmu)./fsigma.^3.*Df1.*Ds1);
            Hxx(1,5) = -2*sum((data-fmu)./fsigma.^3.*Df1.*Ds2);
            Hxx(1,6) = -2*sum((data-fmu)./fsigma.^3.*Df1.*Ds3);
            Hxx(2,4) = -2*sum((data-fmu)./fsigma.^3.*Df2.*Ds1);
            Hxx(2,5) = -2*sum((data-fmu)./fsigma.^3.*Df2.*Ds2);
            Hxx(2,6) = -2*sum((data-fmu)./fsigma.^3.*Df2.*Ds3);
            Hxx(3,4) = -2*sum((data-fmu)./fsigma.^3.*Df3.*Ds1);
            Hxx(3,5) = -2*sum((data-fmu)./fsigma.^3.*Df3.*Ds2);
            Hxx(3,6) = -2*sum((data-fmu)./fsigma.^3.*Df3.*Ds3);
            Hxx=-(Hxx+triu(Hxx,1)');
            auxHxx = zeros(2+sum(bin));
            pos = [1 bin(1) bin(2) 1 bin(3) bin(4)];
            pos1 = 1;
            for ii = 1:6,
                if pos(ii),
                    pos2 = 1;
                    for jj = 1:6,
                        if pos(jj),
                            auxHxx(pos1,pos2) = Hxx(ii,jj);
                            pos2 = pos2 + 1;
                        end


                    end
                    pos1 = pos1 + 1;
                end
            end
            Hxx = auxHxx;
            
        end
        
        if nargout>3,
            residuo = (data-fmu);
        end

    end

end
function [param,x,xFit,yFit]=gevRM(y)
%%%% Ajuste Gev para Regimen medio

         p=gevfit(y);
         param.mu=p(3);  %mu
         param.psi=p(2); %sigma
         param.xi=p(1);  %k
         
         [prob,xHist]=hist(y,500);
         prob=cumsum(prob);
         prob=prob/prob(end);
         probY=interp1(xHist,prob,y);
         x=-log(log(1./probY));          
         
         xFit=linspace(min(x),max(x),100);
         probFit=1./(exp(exp(-xFit)));
         yFit=gevinv(probFit,param.xi,param.psi,param.mu);            
end
function [ff,fmax] = groseq(ax,az, nb, typ, fscale,col,total,fmax);
% ff = groseq2(az, nb, typ, fscale,levels)
% This is a replacement of MATLAB's rose
% for those in the geophysical sciences.
% It draws a rose diagram of the azimuths (in degrees) 
% in the vector az, after classifying them into
% nb classes. typ is set to 1 (the default value)
% for 360 degree data, or 2 for 180 degree data.
% set fscale = 0 for arithmetic, 1 for square root
% scale for frequency.
% This version uses a modified verson of centaxes
% (centax2) to plot a central scale: to turn this
% off, comment out the call to centax2.  The
% scale shown is the frequency, or the square root
% of the frequency, depending on fscale.
% Written by Gerry Middleton, December 1996
%
%Modified by Omar Quetzalcoatl Gtz.
%                                           june, 2006
%   to draw the histogram in bars, get the values of the bars, etc...
%   Aditional inputs:
%               fscale: limits for axis. controlling this value is possible
%               to make diferent roses in the same axes.  fscale is an
%               output too.
%
%    Last modification on october 11, 2006 by Omar Quetzalcoatl Gutierrez
%  line 30 and others, hist -->histc to take in account the limits of bins, and not
%  the mean of the limits.
%           total:  number of elements or data
%
%   use:
%           to make only one rose
%           [ff,fmax] = groseq2(az, nb, typ, fscale,col,total,fmax);
%       with: 
%           az: directions, form -11.25 to 348.75 degrees (to simplify the definition of the North)
%           nb: number of bins. 16 to have a rose with the main directions
%           typ: set 1 to have a 0:360 degrees rose
%                   set 2 to have a 0:180 degrees rose
%           fscale: scale to use in the rose, 0 for arithmetic
%                                                               1 for square root
%           col: color of the bars, could be an rgb value or a typic color like in the plot function, i.e. 'r'
%           total: length of the data, to get percentages.
%           fmax: max limit of the axes rose.
%
%                                               Quetzal.

if typ == 2    % double, then divide by 2 if necessary
  k = find(az > 179.99);
  az(k) = az(k) - 180;    % corrected az
  az2 = [az;(az+180)];    % extended az
end
x = [-11.25:360/nb:360-11.25];
x2 = [180/nb:360/nb:(360 - 180/nb)];  % class limits
if typ == 1        
  [f,xk] = histc(az,x);   % draw histogram
  f=f';
%   stairs(x, [f 0]);
else
  xx = [0:360/nb:180];    % change x scale
  x3 = [180/nb:360/nb:180-180/nb];   % frequencies for histogram
  [f,x3] = histc(az,x3);   % draw histogram
%   stairs(xx, [f 0]);
  [f, x2] = histc(az2, x2); % frequencies for rose 
end
f=round(f./total*10000)./100;
ftot = sum(f);    % find total frequency
if typ == 2
  ftot = ftot/2;  % modify if 180 data
end
if nargin<8,
    fmax = ceil(max(f));    % set up axes for histogram
    re=rem(fmax,5);
    fmax=fmax-re+5;
end
if typ == 1
  axis(ax,[0 360 0 (fmax+1)]);
else
  axis(ax,[0 180 0 (fmax+1)]);
end
set(ax, 'XTick',x);
% fh = figure;          % begin new figure for rose
ff = f;
if fscale == 1   % scale frequency by maximum
   f = sqrt(f);
   fmax = max(f);
   f0 = fmax/1000; %10
else
   f0 = fmax/1000; %10
end
xt = [-fmax:fmax/4:fmax];
yt = xt;
% plot axes
p = plot(ax,[-fmax,-f0],[0,0],'r', [f0 fmax], [0, 0],'r',...
  [0,0],[-fmax,-f0],'r', [0 0], [f0,fmax], 'r');
axis(ax,[-fmax fmax -fmax fmax]);
ameva.centax2(ax);
axis(ax,'square');
axis(ax,'off');
hold(ax,'on')
% blank out central square, and draw inner circle
% h = fill([-f0 -f0 f0 f0],[-f0 f0 f0 -f0],[0.8 0.8 0.8],...
%    'EdgeColor',[0.8 0.8 0.8]);
[rs,rc]=ameva.arcq(f0,0,360);
fill(rs,rc,'k','Parent',ax);
% text(-0.6*f0, 0, num2str(ftot));  % print total frequency

rs10 = f0*sin(pi*x(1:nb)/180);   % draw radial lines
rc10 = f0*cos(pi*x(1:nb)/180);
rs1 = f(1:nb) .* sin(pi*x(1:nb)/180);
rc1 = f(1:nb) .* cos(pi*x(1:nb)/180);
rs20 = f0*sin(pi*x(2:nb+1)/180);
rc20 = f0*cos(pi*x(2:nb+1)/180);
rs2 = f(1:nb) .* sin(pi*x(2:nb+1)/180);
rc2 = f(1:nb) .* cos(pi*x(2:nb+1)/180);
RS=[]; RC=[];
for i = 1:nb     % draw arcs
  [rs,rc]=ameva.arcq(f(i), x(i), x(i+1));
  rct=[rs10(i),rs1(i),rs,rs2(i),rs20(i)];
  rst=[rc10(i),rc1(i),rc,rc2(i),rc20(i)];
  RS=[RS ;rct];
  RC=[RC ;rst];
end
hf=fill(RS',RC',col,'Parent',ax); 
set(hf,'facealpha',1,'edgecolor','k','linewidth',1)

k = find(f > 0);
% plot([rs10(k); rs1(k)],[rc10(k); rc1(k)],'y','linewidth',2);
% plot([rs20(k); rs2(k)],[rc20(k); rc2(k)],'y','linewidth',2);

for hj=[2,5,10:5:(fmax-5)],
    [rc,rs]=ameva.arc2(hj, 0, 360);
    plot(ax,rs, rc,'k--')
    hold(ax,'on');
%     text(.5,hj,[num2str(hj),'%'],'fontsize',12,'fontweight','bold');
%     text(-1.5,-hj,[num2str(hj),'%'],'fontsize',12,'fontweight','bold');
     text(hj,.5,[num2str(hj),'%'],'fontsize',12,'fontweight','bold','Parent',ax);
    text(-hj,-1.5,[num2str(hj),'%'],'fontsize',12,'fontweight','bold','Parent',ax);
end

[rc,rs]=ameva.arc2(fmax, 0, 360);
plot(ax,rs, rc,'-k','LineWidth',1.5)





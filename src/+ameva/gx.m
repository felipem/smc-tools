function x = gx(estimadores,P)
global tipdis

if length(tipdis)<10
    x=estimadores(3)- (estimadores(1)./estimadores(2).* ( 1-(-log(P)).^-estimadores(2) ) );
else
    x=estimadores(2)-  estimadores(1).*log(  -log(P)  ) ;
end


function [year,Hs12]=hs12(time,Hs)
year=[];
Hs12=[];

tyear=datevec(time);
tyear=tyear(:,1);

if isempty(tyear)
    year=[];
    Hs12=NaN;
    return;
end

t1=tyear(1);


while 1
    
    I=tyear==t1;
    ht=sort(Hs(I));
    if length(ht)>11
        Hs12=cat(1,Hs12,mean(ht(end-11:end)));
    end       
    year=cat(1,year,t1);    
    t1=t1+1;
    if t1>max(tyear)
        break
    end
end





end


 function amevaData=loadAmevaData(dataFile)
        load(dataFile);
        amevaData=containers.Map;
        hs=ameva.AmevaVar('hs','m',gowData.hs,gowData.time);amevaData('hs')=hs;
        tp=ameva.AmevaVar('tp','s',gowData.tm,gowData.time);amevaData('tp')=tp;
        dir=ameva.AmevaVar('dir','º N',gowData.dir,gowData.time);amevaData('dir')=dir;
        hs.dir='dir';
        tp.dir='dir';
end
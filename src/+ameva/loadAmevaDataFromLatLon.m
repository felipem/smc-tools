function amevaData=loadAmevaDataFromLatLon(db,lat,lon,n)
        
        [outData,~]=data.getDOWTemporalSerie(db,lat,lon,n);
        pause(0.3);

        amevaData=containers.Map;
        hs=ameva.AmevaVar('hs',' m',outData.hs,outData.time);amevaData('hs')=hs;
        tp=ameva.AmevaVar('tp',' s',outData.tp,outData.time);amevaData('tp')=tp;
        wave_dir=ameva.AmevaVar('waveDir','� N',outData.dir,outData.time);amevaData('waveDir')=wave_dir;
        tide=ameva.AmevaVar('tide','m',outData.tide,outData.time);amevaData('tide')=tide;
        hs.dir='waveDir';
        tp.dir='waveDir';
        
        
       %Añado las mareas y el nivel
       [tideTime,tide]=data.getGOTTemporalSerie(db,lat,lon);
       tide=interp1(tideTime,tide,amevaData('hs').getTime);
       [time,eta]=data.getGOSTemporalSerie(db,lat,lon);            
       eta=interp1(time,eta,amevaData('hs').getTime);  
       etaVar=ameva.AmevaVar('surge','m',eta,amevaData('hs').getTime);amevaData('surge')=etaVar;            
       tideVar=ameva.AmevaVar('tide','m',tide,amevaData('hs').getTime);amevaData('tide')=tideVar;  

end


function amevaData = loadAmevaDataFromMopla(mopla )
        amevaData=containers.Map;
        hs=ameva.AmevaVar('hs',' m',mopla.hs,mopla.time);amevaData('hs')=hs;
        
        tp=ameva.AmevaVar('tp',' s',mopla.tp,mopla.time);amevaData('tp')=tp;
        wave_dir=ameva.AmevaVar('waveDir','� N',mopla.dir,mopla.time);amevaData('waveDir')=wave_dir;
        %tide=ameva.AmevaVar('tide','m',outData.tide,outData.time);amevaData('tide')=tide;
        hs.dir='waveDir';
        tp.dir='waveDir';
        
        %A�ado la marea astronomica y metereologica
        [~,eta_,tide_] = data.getLevelTemporalSerie(util.loadDb(),mopla.lat,mopla.lon,mopla.time);
        tide=ameva.AmevaVar('tide','m ',tide_,mopla.time);amevaData('tide')=tide;
        eta=ameva.AmevaVar('surge','m ',eta_,mopla.time);amevaData('surge')=eta;
    
end


function amevaData = loadAmevaDataFromPerfil(moplaFolder,mopla,perfilId)
     %moplaFolder=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId); 
     tteFile=fullfile(moplaFolder,[perfilId,'_tte.mat']);
     
     pTTE=load(tteFile,'-mat','hs_b','h_b','l_b','dir_b','dir_b_north','l_b','uAvg');
     if isfield(pTTE,'uAvg')
        uAvg=pTTE.uAvg;
    else
        uAvg=[];
    end
     perfil=mopla.perfiles(perfilId);
     %perfilTTE=perfilTTE.perfilTTE;     
     
     %%%%Pongo la direcci� de 0 a 360 grados
     dir_b_north=pTTE.dir_b_north;
     dir_b_north(dir_b_north<0)=dir_b_north(dir_b_north<0)+360;
     
     amevaData=containers.Map;
     hs=ameva.AmevaVar('hs.b',' m',pTTE.hs_b,mopla.time);amevaData('hs.b')=hs;
     wave_dir=ameva.AmevaVar('dir.b','� N',dir_b_north,mopla.time);amevaData('dir.b')=wave_dir;
     
     
     q_=post.longshoreTransport(pTTE.hs_b,pTTE.h_b,pTTE.dir_b,mopla.tp,pTTE.l_b,uAvg,perfil,mopla.transportModel,mopla.kModel);
     q=ameva.AmevaVar('Q','m3/s ',q_,mopla.time);amevaData('Q')=q;
     
    [level_,eta_,tide_] = data.getLevelTemporalSerie(util.loadDb(),mopla.lat,mopla.lon,mopla.time);
    tide=ameva.AmevaVar('tide','m ',tide_,mopla.time);amevaData('tide')=tide;
    eta=ameva.AmevaVar('surge','m ',eta_,mopla.time);amevaData('surge')=eta;
    level=ameva.AmevaVar('level','m ',level_,mopla.time);amevaData('level')=level;
    
    slope=perfil.slope;
    %str2double(answer{1});                 
    %reference=str2double(answer{2}); 
    reference=0;
    if slope>=0.1
       runUp=1.98*0.47*sqrt(pTTE.hs_b.*(1.56*mopla.tp.^2))*slope;
    else
       runUp=1.98*0.04*sqrt(pTTE.hs_b.*(1.56*mopla.tp.^2));
    end% 
    ci=reference+level_+runUp;                
    ciVar=ameva.AmevaVar('ci','m',ci,mopla.time);amevaData('ci')=ciVar; 
    runUpVar=ameva.AmevaVar('runUp','m',runUp,mopla.time);amevaData('runUp')=runUpVar; 
end

function amevaData = loadAmevaDataFromPoi(mopla,poiId,project,alternativa)
    outData = data.getPOITemporalSerie(mopla,poiId,project,alternativa);  
    amevaData=[];
    if isempty(outData)
        return;
    end
    amevaData=containers.Map;
    hs=ameva.AmevaVar('hs',' m',outData.hs,outData.time);amevaData('hs')=hs;
    wave_dir=ameva.AmevaVar('waveDir','� N',outData.dir,outData.time);amevaData('waveDir')=wave_dir;
    hs.dir='waveDir';
end


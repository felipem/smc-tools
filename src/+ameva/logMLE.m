function l = logMLE(Y_vector);
global psi xi mu;

n=length(Y_vector);

l=0;
% -------------------------------------------------------------------------

for i=1:n
    if (1+xi.*(Y_vector-mu)./psi)>0
        l=l+( -log(psi)-(1+1/xi).*log(1+xi.*(Y_vector(i)-mu)./psi) - ((1+xi.*(Y_vector(i)-mu)./psi).^(-1/xi)));
    else
        l=-10000000000000;
    end
end

l=-l; %Cambio a minimizacion para SCE

function casos=maxDiss(hs,tp,dir,n)
  
 arch=computer('arch');
 if isempty(strfind(arch,'win'))
     casos=ceil(rand(n,1)*length(hs));
	 return;
 end
 
%  if ~ispc
%      casos=ceil(rand(n,1)*length(hs));
% 	 return;
%  end
%     
	
	data=[hs,tp,dir];
    %Toma el maximo de altura de ola como seed    
    [~,seedIndex]=max(hs);
	isDir=[false,false,true];
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Standarization
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   if max(max(data))>1  
      tmpData=zeros(size(data,2)+sum(isDir),size(data,1));
      cc=1;
      for i=1:length(isDir)
           if isDir(i)
               x=data(:,i);
               tmpData(cc,:)=((sind(x)-mean(sind(x)))/std(sind(x)))';
               tmpData(cc+1,:)=((cosd(x)-mean(cosd(x)))/std(cosd(x)))';
%                tmpData(:,cc)=sind(x);
%                tmpData(:,cc+1)=cosd(x);
               cc=cc+2;
           else
               x=data(:,i);
               tmpData(cc,:)=((x-mean(x))/std(x))';
               cc=cc+1;
           end
      end
      data=tmpData;
   end
    %%%%% End Standarization
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
    siPtr=libpointer('int32Ptr',zeros(n,1));
    loadlibrary(fullfile('bin','MaxDiss'),@ameva.maxDissDll);
    calllib('MaxDiss','GetSignificantIndexesByMaxDiss',n,data,size(data,2)...
    ,size(data,1),seedIndex-1,siPtr);
    casos=get(siPtr,'Value')+1;      
    unloadlibrary('MaxDiss');

%     plot(data(:,1),data(:,2),'.')
%     hold on
%     plot(data(casos,1),data(casos,2),'.r')
%     hold off
    
end
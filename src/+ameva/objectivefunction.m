% This function defines the cost or objective function
% x is the parameter vector
function f =objectivefunction(x,Y_vector)

global psi xi mu;

psi=x(1);
xi=x(2);
mu=x(3);

f = ameva.logMLE(Y_vector);

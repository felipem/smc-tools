function [quanGEVX,Ts,Tapprox,Xor,fitParameters,stdDqX,stdloX,stdupX,quanX10]=potfit(xumb,umbral,ny,xmax)
%ATENCION unificar con pot-herramienta. OJO OJO usado por rmall_fig
%xmax  maximos anuales
%Peak over threshold selected data
% aux=25;ny=60;k=0;
% xumb=gprnd(-0.1512,0.6863,umbral,ny*aux,1);
% for i=1:ny, xmax(i)=max(xumb(k+1:k+aux)); k=k+aux; end


alfa = 0.05;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Ajuste extremal reanalisis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
paramEsts=mle((xumb - umbral), 'dist','generalized pareto', 'alpha',alfa);%gpfit(xumb - umbral);
[lambda0,sigma0,gamma0,~,~,hessianPOT,~] = ...
    ameva.OptiParamHessianPOT(xumb,umbral,ny,[length(xumb)/ny; log(paramEsts(2));paramEsts(1)]);



%   Best parameter vector and confidence value
popt = [lambda0; sigma0; gamma0];

Ts = [2:1:9 10:10:90 100:100:500];
%   Datos anuales OC OJO
Proxorny = (((1:ny))/(ny+1))';
Proxorxm = (((1:length(xmax)))/(length(xmax)+1))';
%
if length(Proxorxm)<length(Proxorny)
    Xor = sort(xmax);
    Proxor=Proxorny(end-length(Proxorxm)+1:end);
elseif length(Proxorxm)==length(Proxorny);
    Xor = sort(xmax);
    Proxor=Proxorny;
else
    error('length error of vector xmax and year number');
end
Tapprox = 1./(1-Proxor);
id = find(Tapprox>=Ts(1));
Tapprox=Tapprox(id);
Xor=Xor(id);
%   Aggregate quantiles for different return periods
nts = length(Ts);
quanGEVX = zeros(nts,1);
stdDqX = zeros(nts,1);
stdupX = zeros(nts,1);
stdloX = zeros(nts,1);
invI0X = inv(hessianPOT);
for j = 1:nts, %            Annual
    [quanX,DqX] = ameva.QuantilePOT (1-1/Ts(j),umbral,lambda0,sigma0,gamma0);
    if Ts(j)==10, quanX10=quanX; end;
    stdDqX(j) = sqrt(sum((DqX'*invI0X).*DqX',2));
    quanGEVX(j)=quanX;
    %           Intervalos de confianza del cuantil anual
    stdupX(j) = quanX+stdDqX(j)*norminv(1-alfa/2,0,1);
    stdloX(j) = quanX-stdDqX(j)*norminv(1-alfa/2,0,1);
end
quanGEVX=quanGEVX(:);

%si solo salen 3 el utimo son los parametros optimos
popt(2)=exp(popt(2));%paso a lineal el sigma del parametro optomo
if nargout==3,
    Tapprox=popt;
elseif nargout==4,
    Tapprox=popt;
    Xor=stdDqX;
end


fitParameters.u=umbral;

fitParameters.lambda=popt(1);
fitParameters.sigma=popt(2);
fitParameters.xi=popt(3);

%parametros GEV
%mu=u-(sigma/xi)*(1-1/(lambda.^(-xi)));    
fitParameters.mu=umbral-(popt(2)/popt(3))*(1-1/(popt(1)^-popt(3)));
%psi=sigma/(lambda.^(-xi));
fitParameters.psi=popt(2)/(popt(1)^-popt(3));




end
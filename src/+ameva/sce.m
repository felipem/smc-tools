function [fstorage,bestsolution,bestf] =sce(Y_vector,tipdis)
%=====================================================
% The Shuffled Complex Evolution Method, SCE-UA v2.1
%=====================================================
%
% Method developed and coded in fortran by Q. Duan (April, 1992)
% at the Department of Hydrology & Water Resources of the
% University of Arizona 
%
% References:
%  Duan, Q., Sorooshian, S., Gupta, V. (1992) "Effective and efficient
%   global optimization for conceptual rainfall-runoff models". Water
%   Resources Research, 28(4), 1015-1031.
%  Duan, Q., Sorooshian, S., Gupta, V. (1994) "Optimal use of the SCE-UA 
%   global optimization method for calibrating watershed models".
%   Journal of Hydrology, 158, 265-284.
% 
% Recoded in MATLAB by Fernando J. Mendez (September, 2003) at Cornell
% University
% - Notation of variables follows Duan et al. (1992) paper
% 
% Comments: September 12, 2003
%   - Check convergence is implemented considering 
%        * maximum number of trials
%        * Absotule error of objective function
%   - "No improvement in KSTOP shuffling loops" is NOT implemented yet
%
% e-mail: mendezf@unican.es
%
% See the structure of the functions and scripts in sce-matlab.pdf
% 
% Definition of variables

nitemax=1500; %Maximum number of iterations
numberofloops=1; % Number of loops repeating the whole process



% Load the lower and upper bounds
if length(tipdis)<10
    [x_min,x_max] = ameva.bounds ;% Feasible space Omega in R^n
else
    [x_min,x_max] = ameva.bounds2;
end


% 0. Initiate the variables of the method

n=length(x_min);    % Dimension of the problem

p=2;    % Initial number of complexes
pmin=p; % Minimum number of complexes (recommended in Duan et al, 1994)
m=2*n+1;    % Number of points in each complex (recommended in Duan et al, 1994)
s=m*p;  % Sample size
beta=2*n+1; % Number of evolution steps allowed for each complex before complex shuffling (Duan et al, 1994)
q=n+1;      % Number of points in each sub-complex (Duan et al, 1994)
alfa=1;     % Internal parameter of CCE (Duan et al, 1994)



fopt=-100; % for the minimum value of the objective function (lower)

fstorage=ones(nitemax,numberofloops)*1000000; % initialize the array with the values of the objective function for every trial 


for iloop=1:numberofloops % loop repeating the run



% 1. Initiate the process

errmin=.01; % porcentage error
%kstop=5; % number of shuffling loops in which the criterion value must change by 'pecnto' before optimization is terminated
%pcento=.005; % porcentage by which the criterion value must change in 'kstop' shuffling loops (in o/1) 



ite=1;
%criter=1000.*ones(20,1);

% 2. Generation of a sample
for i=1:s
 x(i,1:n)=ameva.generateuniformsample(x_min,x_max); % sample s points x_1,...,x_s in the feasible space Omega
                                              % using a uniform sampling distribution
                                                
 f(i)=ameva.objectivefunction(x(i,:),Y_vector); % compute the function value f_i at each point x_i
 ite=ite+1;  % Calculate the number of iterations
 fstorage(ite,iloop)=min([f(i); fstorage(ite-1,iloop)]);

end

% 3. Rank the points: sort the s points in order of increasing function value
% and store them in an array D={x_i,f_i} where i=1 represent the point with
% the smallest function value
[faux index]=sort(f);
D=[x(index,1:n) faux'];

err=10000;
nloop=0;
%timeou=Inf;

while (err>=errmin)&(ite<=nitemax) % 7. Check convergence

    nloop=nloop+1;  % number of loops - shufflings

% 4. Partition D into p complexes A(1),...A(p) each containing m points;
clear A;
for ip=1:p
    for im=1:m
        A(ip).D(im,1:n+1)=D(ip+p*(im-1),1:n+1);
    end
end

% 5. Evolve each complex A(k) according to the competitive complex
% evolution CCE algorithm
for ip=1:p
    complexA=A(ip).D;
    for i=1:beta
        L=ameva.simulatrapezoidal(m,q); % Select q points from complex according to the trapezoidal distr. 
                                  % Them the q points define a sub-complex
                                  % Storage the positions in L
                                  % m is the number of points of each complex
        B=complexA(L,1:n+1);      % B is the sub-complex
        
        [baux index]=sort(B(:,n+1));
        B=[B(index,1:n) baux];    % Now B is the sub-complex sorted
        
        L=sort(L);                % Now L is sorted        
        
        for j=1:alfa
            g=mean(B(1:(end-1),1:n));  % Calculate the centroid of q-1 best points
            uq=B(end,1:n);             % uq is the worst point in B
            fq=B(end,n+1);             % fq is the function value in the worst point in B
            r=2*g-uq;                  % r is the reflection step
            iflag=0;                   % flag to check if r is in the feasible space (0 = yes, 1 = no)
            
             
            for k=1:n                  % check if the point 'r' is in the feasible space
                    if(r(k)<x_min(k)|r(k)>x_max(k))
                        iflag=iflag+1;
                    end
            end
            
            
            if iflag==0
                fr=ameva.objectivefunction(r,Y_vector);
                ite=ite+1;  % Calculate the number of iterations
                fstorage(ite,iloop)=min([fr ; fstorage(ite-1,iloop)]);
            else
                
                % Generate a point z in Omega according to a Normal distribution with best point of the sub-complex                                                                    % mal
                % as mean and standard deviation of the population as std                                                                     
                             
                for in=1:n
                    sigma(in)=std(D(1:s,in),1)./(x_max(in)-x_min(in));
                end
                
                
                z=ameva.generatenormalsample(complexA(1,1:n),sigma,x_min,x_max);
                
   
                r=z;
                fr=ameva.objectivefunction(r,Y_vector);
                ite=ite+1;  % Calculate the number of iterations
                fstorage(ite,iloop)=min([fr ; fstorage(ite-1,iloop)]);
            end
            if (fr<fq)
                uq=r;
                fq=fr;
            else
                c=(g+uq)/2;             % c is the contraction step
                fc=ameva.objectivefunction(c,Y_vector);
                ite=ite+1;  % Calculate the number of iterations
                fstorage(ite,iloop)=min([fc ; fstorage(ite-1,iloop)]);

                if(fc<fq)
                    uq=c;
                    fq=fc;
                else
                    
                    
                    % Generate a point z in Omega according to a Normal distribution with best point of the sub-complex                                                                    % mal
                    % as mean and standard deviation of the population as std                                                                     
                             
                    for in=1:n
                        sigma(in)=std(D(1:s,in),1)./(x_max(in)-x_min(in));
                    end
                    
                    
                    z=ameva.generatenormalsample(complexA(1,1:n),sigma,x_min,x_max);
                    

                    fz=ameva.objectivefunction(z,Y_vector);
                    ite=ite+1;  % Calculate the number of iterations
                    fstorage(ite,iloop)=min([fz ; fstorage(ite-1,iloop)]);

                    uq=z;
                    fq=fz;
                end
            end  
             
            B(end,1:n)=uq;
            B(end,n+1)=fq;
         
        end  % loop for alfa
        
        complexA(L,:)=B;  % Replace B into A according to L
        
        [faux index]=sort(complexA(:,n+1)); % Sort A in order of increasing function value
        complexA=[complexA(index,1:n) faux];
        
       
    end % loop for beta
    
    
    A(ip).D=complexA;
    
end  % loop for each complex A(k)


 
  
    

% 6. Shuffle the complexes replacing A(i) into D and sorting D in order of
% increasing function value

x2=[];
for ip=1:p
    x2=[x2; A(ip).D(:,:)];
end
[faux index]=sort(x2(:,n+1));
D2=[x2(index,1:n) faux];

%err=abs(fopt-D2(1,n+1))/abs(fopt)*100; % Relative error in %
err=abs(fopt-D2(1,n+1)); % Absolute error in %


%criter(20)=D2(1,n+1);
%if(nloop>=(kstop+1))
%    denomi=abs(criter(20-kstop)+criter(20))/2;
%    timeou=abs(criter(20-kstop)-criter(20))/denomi;
%end
%for l=1:19
%    criter(l)=criter(l+1);
%end


D=D2;

if(p>pmin) % Reduction in the number of complexes (Duan et al., 1994)
    p=p-1;
    s=p*m;
end


end % close loop about err>=errmin 


end % close loop of 'iloop'

bestsolution=D(1,1:n);
bestf=D(1,n+1);

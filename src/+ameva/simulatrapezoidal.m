function lista = simulatrapezoidal(m,q)
% Function to simulate q points in a trapezoidal distribution of m elements
% without replacement


lista=zeros(1,q);
lista(1)=ameva.trapezoidal(m);
for i=2:q
    lista(i)=ameva.trapezoidal(m);
    while isempty(find(lista(1:i-1)==lista(i)))==0
        lista(i)=ameva.trapezoidal(m);
    end
end

function inv = trapezoidal (m)
% This function calculates the random value associated to a discrete
% trapezoidal distribution
% m is the dimension of the complex
im=1:m;
prob=2*(m+1-im)/(m*(m+1)); 
F=cumsum(prob);
xr=rand(1,1);
if(xr<=F(1))
    iposi=1;
end
for i=2:m
    if(xr>F(i-1)&xr<=F(i))
        iposi=i;
    end
end
inv=iposi;


        
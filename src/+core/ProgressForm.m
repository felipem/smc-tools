classdef ProgressForm<handle
    
    properties
        form;
        mainBar;
        secondBar;
        width=500;
        height=300;
        currentBar;
        axMain;
        axSecond;
        bCancel;
        
    end
    
    methods
        function self=ProgressForm(name,cancelFcn)
            
            scrPos=get(0,'ScreenSize');
            self.form=figure('Position',[(scrPos(3)-self.width)/2,(scrPos(4)-self.height)/2,self.width,self.height],...
                'CloseRequestFcn',cancelFcn, 'MenuBar', 'none','NumberTitle', 'off', 'Toolbar', 'none','WindowStyle','modal');
            set(self.form,'resize','off');
            self.setFigureTitle(name);
            self.bCancel=uicontrol(self.form,'Style','pushbutton','String', 'Cancel','Position',[30 10 60 20],'Callback',cancelFcn);    
            
            panel=uipanel(self.form,'units','pixel','position',[0 40 self.width self.height-40]);
            self.axSecond=axes('Parent',panel,'units','pixel','Position',[20,66,self.width-40,30]);
            self.axMain=axes('Parent',panel,'units','pixel','Position',[20,160,self.width-40,30]);
            
            
            polyX=[0 0 0 0];
            polyY=[0 1 1 0];
            %color=[0.4 0.55 1];
            color=[1 0 0];
            self.mainBar=fill(polyX,polyY,color,'Parent',self.axMain);
            self.secondBar=fill(polyX,polyY,color,'Parent',self.axSecond);
            self.currentBar=self.mainBar;
            set(self.axMain,'XTick',[],'YTick',[],'XLim',[0 1],'YLim',[0 1]);
            set(self.axSecond,'XTick',[],'YTick',[],'XLim',[0 1],'YLim',[0 1]);
            
        end 
        
        function delete(self)
            delete(self.form);
        end
        
        function setProgress(self,value)
            set(self.currentBar,'XData',[0 0 value value]);
            drawnow
        end
        
        function setCurrentBar(self,bar)
            if strcmp(bar,'first')
                 self.currentBar=self.mainBar;
            else
                self.currentBar=self.secondBar;
            end                
        end
        
        function setMainTitle(self,string)
            title(self.axMain,string);
        end
        
        function setSecondTitle(self,string)
            title(self.axSecond,string);
        end
        
        function disableCancel(self)
            set(self.bCancel,'Enable','off');
            drawnow;
        end
        
        function setFigureTitle(self,string)
            set(self.form,'Name',string);
        end
        
        
       
    end
end
classdef Project<handle
    properties
        name;
        elements;
        alternativas;
        utmZone;
    end
    
    properties (Transient=true)
        path='';
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% Constructor/Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function self=Project(name,utmZone)
            self.elements=util.initMap(self.elements);
            self.alternativas=util.initMap(self.alternativas);
            self.name=name;
            self.utmZone=utmZone;
        end
        
        function delete(self)
            delete(self.elements);
            delete(self.alternativas);
        end
    end
    %%%%%%%% End Constructor/Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% Alternativa Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function  [altName]=createAlternativaFromElements(self,elIds)
            bool=false;
            altName='';
            while ~bool
                answer = inputdlg(' Nombre de la alternativa: ','New Alternativa',1,...
                {['Alternativa ',length(self.alternativas)] });
                if ~isempty(answer)                
                    altName=char(answer);
                    if ~isKey(self.alternativas,altName)                    
                        alt=mdt.Alternativa(altName,self);
                        for i=length(elIds)
                            alt.atachElement(elIds{i});
                        end
                        self.alternativas(altName)=alt;                        
                        return;
                    end
                else
                    return;
                end
            end
        end
        
        function [altName]=createAlternativaFromScatterData(self,sd)
              bool=false;
              altName='';
            %while ~bool
                answer = inputdlg(' Nombre de la alternativa: ','New Alternativa',1,...
                {['Alternativa ',length(self.alternativas)] });
                bool=true;
                if ~isempty(answer)                
                    altName=char(answer);
                    if ~isKey(self.alternativas,altName)                    
                        alt=mdt.Alternativa(altName,self);
                        alt.addElement(sd);
                        self.alternativas(altName)=alt;
                        bool=true;
                        %break;
                    end
                else
                    bool=true;
                    %break;
                end
            %end
       
        end
        
        function altName=createAlternativa(self)
              bool=false;
              altName='';
            while ~bool
                answer = inputdlg(' Nombre de la alternativa: ','New Alternativa',1,...
                {['Alternativa ',length(self.alternativas)] });
                if ~isempty(answer)                
                    altName=char(answer);
                    if ~isKey(self.alternativas,altName)                    
                        alt=mdt.Alternativa(altName,self);
                        self.alternativas(altName)=alt;                        
                        return;
                    end
                else
                    return;
                end
            end
       
        end
        
        function removeAlternativa(self,altName)
            if isKet(self.alternativas,altName)
                alternativa=self.alternativas(altName);
                delete(alternativa);
                remove(self.alternativa,altName);                
            end
        end
    end
    %%%%%%%% End MDT Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% Element Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function value=getElementsByType(self,type)
            value={};
            typeElements=keys(self.elements);
            for i=1:length(typeElements)
                elementId=typeElements{i};
                element=self.elements(elementId);
                if strcmp(element.getType,type)
                    value=cat(1,value,elementId);
                end
            end
         end
        
        function isFree=isFreeElement(self,elId)
            alters=values(self.alternativas);
            isFree=1;
            for i=1:length(alters)
                if ~isKey(alters{i}.project.elements,elId)                
                    isFree=0;
                    return;
                end
            end
        end
        
        function removeElement(self,elId)
            if isKey(self.elements,elId)
                element=self.elements(elId);
                delete(element);
                remove(self.elements,elId);                
            end
        end
    
    
    
        function names=getElementsName(self)
            names={};
            ids=keys(self.elements);
            for i=1:length(ids)
                element=self.elements(ids{i});
                names=cat(1,names,element.name);
            end
        end
        
        function id=getElementId(self,name)            
            id=[];
            ids=keys(self.elements);
            for i=1:length(ids)
                element=self.elements(ids{i});
                if strcmp(element.name,name)
                    id=ids{i};
                    break;
                end
            end
        end
    end
    %%%%%%%% Element Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% I/O Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
      function save(self)
          if ~isempty(self.path)
              if exists(self.path,'file')
                  save(self.path,'self');
                  return;
              end
          end
          self.saveAs();
      end
      
      function saveAs(self)
        [file,folder]=uiputfile('*.smc','Save Project');
        save(fullfile(folder,file),'self');
        self.path=fullfile(folder,file);
      end
      
    end
     
     methods (Static=true)
         function project=loadProject(fileName)
             load(fileName,'-mat');
             project=self;
         end
     end
     %%%%%%%% End I/O Methods
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    
end
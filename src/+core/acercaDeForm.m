function acercaDeForm(varargin)
    %%% Cargo el idioma
    txt=util.loadLanguage;
    cfg=util.loadConfig;
    %%%%%%%%%%%%%%%%%%%
    
    h=dialog('Visible','off','Name',txt.main('mHelpAbout'));
    %pos=get(h,'position');
    set(h,'position',[200,200,600,400]);
    
    ax=axes('Parent',h,'YTick',[],'XTick',[],'position',[0 0 1 1]);
    imshow(fullfile('icon',cfg('region'),'about_that.png'),'Parent',ax);
    set(h,'Visible','on');
    
end


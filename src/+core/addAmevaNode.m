function addAmevaNode(tree,lat,lon,type)
    %Añade un node a la carpeta principal de Ameva

    if ~exist('type','var'), type='DOW';end
    
    typeId='';
    
    if strcmp(type,'DOW')
        typeId='dow';        
    elseif strcmp(type,'GOS')   
        typeId='gos';
    elseif strcmp(type,'GOT')   
        typeId='got';
    end
        
    
    node=util.getNodeByKey(tree,['fld_ameva_',typeId]);
    amevaNode=util.getNodeByKey(tree,['ameva_root',typeId,'_',num2str(lat),'_',num2str(lon)]);
    if isempty(amevaNode)
        amevaNode=uitreenode('v0',['ameva_root',typeId,'_',num2str(lat),'_',num2str(lon)],[type,' [',num2str(lat),' , ',num2str(lon),']'],[],false);    
        util.setNodeIcon(amevaNode,[typeId,'.gif']);
        node.add(amevaNode);
        util.refreshTree(tree);
    end
end


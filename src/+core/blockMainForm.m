function blockMainForm

f=findobj('-regexp','Tag','smcToolsMainForm_*');
set(0,'currentfigure',f);

tag=get(f,'Tag');
if isempty(tag)
    return;
end
enable=~logical(str2double(tag(end)));
strEnable='on';
if ~enable,strEnable='off';end;

tag(end)=num2str(enable);
set(f,'Tag',tag);
disableForm(f);

if enable    
    watchoff(f);
    
else
    watchon;
    pause(0.1);
end
    
    function disableForm(form)        
        if isprop(form,'Enable')
            set(form,'Enable',strEnable)
        end
        children=get(form,'Children');
        for i=1:length(children)
            disableForm(children(i));
        end        
    end

end


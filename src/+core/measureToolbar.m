function tools =measureToolbar(toolBar,ax,coordSystem)
 tools=1;
 
 %Regla
 ptRuler=uitoggletool(toolBar,'OnCallback',{@measure,'ruler'},'OffCallback',{@close,'ruler'});         
 util.setControlIcon(ptRuler,'ruler.gif'); 
 units='(m)';
 
 
    function close(varargin)
         type=varargin(end);
         type=cell2mat(type);
         iObj=findobj('Tag',type);
         if ishandle(iObj),delete(iObj);end
         title(ax,'');
    end
 
    function measure(varargin)
         type=varargin(end);
         type=cell2mat(type);
         
         switch type
             case 'ruler'
                 util.disableOtherButtons(toolBar,ptRuler)
                 iObj=imline(ax);     
                 pos=iObj.getPosition;
                 if strcmp(coordSystem,'geo')
                     perim=6378.137*acos(cosd(pos(1,2))*cosd(pos(2,2))*cosd(pos(2,1)-pos(1,1))+sind(pos(1,2))*sind(pos(2,2)));
                     units='(km)';
                 else                     
                     perim=sqrt(sum((pos(2,:)-pos(1,:)).^2));
                 end
                 
                 angle=atan2(pos(2,2)-pos(1,2),pos(2,1)-pos(1,1))*180/pi;
                 title(ax,['L ',units,':',num2str(perim),'  Ang:',num2str(angle)]);
                 set(iObj,'Tag','ruler');
                 iObj.addNewPositionCallback(@(pos) changeVertex(pos));  
         end
         
         function changeVertex(pos)
             switch type
                 case 'ruler'
                if strcmp(coordSystem,'geo')
                     perim=6378.137*acos(cosd(pos(1,2))*cosd(pos(2,2))*cosd(pos(2,1)-pos(1,1))+sind(pos(1,2))*sind(pos(2,2)));
                 else
                     perim=sqrt(sum((pos(2,:)-pos(1,:)).^2));
                end
                angle=atan2(pos(2,2)-pos(1,2),pos(2,1)-pos(1,1))*180/pi;
                title(ax,['L ',units,':',num2str(perim),'  Ang:',num2str(angle)]);
             end
         end
    end

end


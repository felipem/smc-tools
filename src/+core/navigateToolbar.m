function nTools = navigateToolbar(toolBar,panel)
    
    %%% Cargo el idioma
     txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    nTools=4;
    %Zoom In
    ptZoomIn=uitoggletool(toolBar,'TooltipString',txt.main('tbZoom'),'OnCallback',@zoomOn,'OffCallback','zoom off');        
    util.setControlIcon(ptZoomIn,'zoom_in.gif');
    %Reset zoom
    ptZoomReset=uipushtool(toolBar,'TooltipString',txt.main('tbResetZoom'),'ClickedCallback',@zoomReset);
    util.setControlIcon(ptZoomReset,'reset_zoom.gif');
    %Pan
    ptPan=uitoggletool(toolBar,'TooltipString',txt.main('tbPan'),'OnCallback',@panOn,'OffCallback','pan off');           
    util.setControlIcon(ptPan,'pan.gif');
    %Snapshot
    ptSnapshot=uipushtool(toolBar,'TooltipString',txt.main('tbSnapshot'),'ClickedCallback',@snapshot);
    util.setControlIcon(ptSnapshot,'snapshot.gif');
    
    
    function zoomOn(varargin)
      
        util.disableOtherButtons(toolBar,ptZoomIn)
        zoom on;
    end

    function zoomReset(varargin)      
        util.disableOtherButtons(toolBar,ptZoomReset)
        zoom out;
    end
    function panOn(varargin)
         util.disableOtherButtons(toolBar,ptPan)
        pan on;
    end

    function snapshot(varargin)
         util.disableOtherButtons(toolBar,ptSnapshot)
         [filename, ext, user_canceled] = imputfile;
           if ~user_canceled 
               c=colormap(gca);
               f=figure('Visible','off');
               pos=get(panel,'Position');
               fig=get(panel,'Parent');
               set(panel,'Position',[0 0 1 1],'Parent',f);               
               colormap(c);
               format=getImgFormat(ext);
               print(f,['-d',format],'-r150',filename);
               set(panel,'Position',pos,'Parent',fig);               
               close(f);                               
           end
    end

    function format=getImgFormat(ext)
        switch ext
            case 'j2c'
                format='jpeg';
            case 'jp2'
                format='jpeg';
            case 'jpg'
                format='jpeg';
            case 'pbm'
                format='pbm';
            case 'ppm'
                format='ppm';
            case  'pgm'
                format='pgm';
            case  'png'              
                format='png';
            case  'ras'              
                format='tiff';
            case  'tif'              
                format='tiff';
        end
    end
end


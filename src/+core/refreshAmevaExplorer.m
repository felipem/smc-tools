function refreshAmevaExplorer(tree,project, currentForm)
    watchon;
    
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    cAlt='';
    cMopla='';
    cPoi='';        
    isAmevaForm=isa(currentForm,'ameva.AmevaForm');
    if isAmevaForm
        poiInfo=currentForm.getInfo();
        if ~isempty(poiInfo)
            if strcmp(poiInfo.type,'POI')
             cMopla=poiInfo.mopla;
             cAlt=poiInfo.alternativa;
             cPoi=poiInfo.nombre;             
            else 
                isAmevaForm=false;
            end
        else 
            isAmevaForm=false;
        end
    end
    
    %%%AMEVA    
    node=util.getNodeByKey(tree,'fld_ameva_project');    
   
    if isAmevaForm         
        util.cleanNode(node,tree);   
        cNode=util.getNodeByKey(tree,['moplaPoi_',cAlt,'_',cMopla,'_',cPoi]);
    end
    
    util.cleanNode(node,tree);    
    alternativas=smc.getMoplaPois(project);
    for i=1:length(alternativas) 
        alt=alternativas(i);
        altNode=uitreenode('v0',['fld_ameva_alt_',alt.nombre],alt.nombre,[],false);    
        util.setNodeIcon(altNode,'folder_closed.png');
        node.add(altNode);
        for j=1:length(alt.moplas)
            mopla=alt.moplas(j);
            moplaNode=uitreenode('v0',['moplaAmeva_',alt.nombre,'_',mopla.nombre],mopla.nombre,[],false);
            util.setNodeIcon(moplaNode,'ball_green.gif');
            altNode.add(moplaNode);  
            moplaSource=uitreenode('v0',['moplaSource_',alt.nombre,'_',mopla.nombre],txt.main('exAmevaSource'),[],false);
            util.setNodeIcon(moplaSource,'dow.gif');
            moplaNode.add(moplaSource);
            pois=keys(mopla.pois);
            for k=1:length(pois)                
                poi=pois{k};
                if ~strcmp(cPoi,poi) || ~strmp(cAlt,altNombre) || ~strmp(cMopla,moplaNombre)
                    poiNode=uitreenode('v0',['moplaPoi_',alt.nombre,'_',mopla.nombre,'_',poi],poi,[],false);
                    util.setNodeIcon(poiNode,'add_point.gif');
                    moplaNode.add(poiNode);                                                
                else
                    moplaNode.add(cNode);
                end
            end    
             
                
            transporteNode=uitreenode('v0',['transporteAmeva_',alt.nombre,'_',mopla.nombre],txt.main('exAmevaTransport'),[],false);              
            moplaNode.add(transporteNode);
            util.setNodeIcon(transporteNode,'transporte.gif');
            perf=keys(mopla.perfiles);
            moplaFolder=fullfile(project.folder,alt.nombre,'Mopla',mopla.nombre);
            
            for k=1:length(perf)
                perfilId=perf{k};
                if exist(fullfile(moplaFolder,[perfilId,'_tte.mat']),'file')
                    if ~strcmp(cPoi,perfilId) || ~strmp(cAlt,altNombre) || ~strmp(cMopla,moplaNombre)
                        tteNode=uitreenode('v0',['moplaPerfil_',alt.nombre,'_',mopla.nombre,'_',perfilId],perfilId,[],false);
                        util.setNodeIcon(tteNode,'open_polygon.gif');
                        transporteNode.add(tteNode);                                                
                    else
                        transporteNode.add(cNode);                                                
                    end
                end            
            end
        end
    end
    
    
    util.refreshTree(tree);
    watchoff;
    
end
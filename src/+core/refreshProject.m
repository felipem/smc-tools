function project=refreshProject(tree,folder)        
    watchon;
    
        %%% Cargo el idioma
        txt=util.loadLanguage;
        %%%%%%%%%%%%%%%%%%%
        project=smc.loadProject(folder);
        if isempty(project)
            watchoff
           return; 
        end
        
        node=util.getNodeByKey(tree,'fld_project');
        util.cleanNode(node,tree);        
        set(node,'Name',[txt.main('exProject'),' [',project.nombre,']']);
       
        for i=1:length(project.alternativas) 
            alt=project.alternativas{i};
            mallas=smc.loadMallas(project.folder,alt);
            altNode=uitreenode('v0',['fld_alt_',alt],alt,[],false);    
            util.setNodeIcon(altNode,'folder_closed.png');
            node.add(altNode);
            
            editAltNode=uitreenode('v0',['edit_alt_',alt],alt,[],false);            
            util.setNodeIcon(editAltNode,'Layers.png');
            altNode.add(editAltNode);
            
            moplaAltNode=uitreenode('v0',['mopla_alt_',alt],alt,[],false);            
            util.setNodeIcon(moplaAltNode,'smc.gif');
            altNode.add(moplaAltNode);            
            moplaFiles=dir(fullfile(project.folder,alt,'Mopla','*.smct'));            
            
            for j=1:length(moplaFiles)
                moplaId=moplaFiles(j).name;
                moplaId=moplaId(1:end-5);
                
                status=smc.getMoplaStatus(fullfile(project.folder,alt,'Mopla'),moplaId,mallas);                
                if isempty(status)
                    question=sprintf(txt.main('qMoplaErrorLoadLabel'),moplaId);
                    button = questdlg(question,txt.main('qMoplaErrorLoadTitle'),txt.main('qYes'),txt.main('qNo'),txt.main('qYes'));
                    if ~strcmp(button,txt.main('qYes'))
                        util.cleanNode(node,tree);
                         set(node,'Name',[txt.main('exProject'),' []']);
                        return;
                    else
                        poiDir=fullfile(project.folder,alt,'Mopla',moplaId);            
                        delete(fullfile(poiDir,'*.poi'));                        
                        delete(fullfile(poiDir,'*.poi_'));    
                        if exist(poiDir,'dir')
                            rmdir(poiDir);
                        end
                        moplaFile=fullfile(project.folder,alt,'Mopla',[moplaId,'.smct']);
                        delete(moplaFile);                        
                    end
                else
                casoMoplaNode=uitreenode('v0',['moplaView_',alt,'_',moplaId],[moplaId, '[',num2str(status.run),...
                   '/', num2str(status.all),']'],[],false);
                icon='ball_yellow.gif'; 
                if status.run>0, icon='ball_red.gif'; end
                if status.run>=status.all, icon='ball_green.gif'; end
                util.setNodeIcon(casoMoplaNode,icon);
                moplaAltNode.add(casoMoplaNode);
                end
            end           
        end
        
        %%%%% AMEVA
        node=util.getNodeByKey(tree,'fld_ameva_project');
        util.cleanNode(node,tree);
        set(node,'Name',[txt.main('exProject'),' [',project.nombre,']']);
        alternativas=smc.getMoplaPois(project);
        for i=1:length(alternativas) 
            alt=alternativas(i);
            altNode=uitreenode('v0',['fld_ameva_alt_',alt.nombre],alt.nombre,[],false);    
            util.setNodeIcon(altNode,'folder_closed.png');
            node.add(altNode);
            for j=1:length(alt.moplas)
                mopla=alt.moplas(j);
                moplaNode=uitreenode('v0',['moplaAmeva_',alt.nombre,'_',mopla.nombre],mopla.nombre,[],false);
                util.setNodeIcon(moplaNode,'ball_green.gif');                  
                altNode.add(moplaNode);                      
                moplaSource=uitreenode('v0',['moplaSource_',alt.nombre,'_',mopla.nombre],txt.main('exAmevaSource'),[],false);
                util.setNodeIcon(moplaSource,'dow.gif');
                moplaNode.add(moplaSource);
                pois=keys(mopla.pois);
                for k=1:length(pois)
                    poi=pois{k};
                    poiNode=uitreenode('v0',['moplaPoi_',alt.nombre,'_',mopla.nombre,'_',poi],poi,[],false);
                    util.setNodeIcon(poiNode,'add_point.gif');
                    moplaNode.add(poiNode);                                
                end
                transporteNode=uitreenode('v0',['transporteAmeva_',alt.nombre,'_',mopla.nombre],txt.main('exAmevaTransport'),[],false);            
                moplaNode.add(transporteNode);
                util.setNodeIcon(transporteNode,'transporte.gif');
                perf=keys(mopla.perfiles);
                moplaFolder=fullfile(project.folder,alt.nombre,'Mopla',mopla.nombre);
                for k=1:length(perf)
                    perfilId=perf{k};
                    if exist(fullfile(moplaFolder,[perfilId,'_tte.mat']),'file')                        
                        tteNode=uitreenode('v0',['moplaPerfil_',alt.nombre,'_',mopla.nombre,'_',perfilId],perfilId,[],false);
                        util.setNodeIcon(tteNode,'open_polygon.gif');
                        transporteNode.add(tteNode);                                                                        
                    end            
                end

            end
        end
         util.refreshTree(tree);  
         watchoff;
end
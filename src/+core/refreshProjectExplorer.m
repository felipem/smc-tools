function refreshProjectExplorer(tree,project, currentForm)
   
    %%% Cargo el idioma
     txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    watchon;
    cAlt='';
    cMopla='';    
    isAltForm=isa(currentForm,'smc.AlternativaForm');
    isPostForm=isa(currentForm,'post.PostprocesoForm');        
    if isPostForm
        cMopla=currentForm.mopla.moplaId;
        cAlt=currentForm.alternativa.nombre;
    end
    
    if isAltForm
        cAlt=currentForm.alternativa.nombre;
    end
        
    
    %%Postproceso
    for i=1:length(project.alternativas) 
        alt=project.alternativas{i};
        moplaFiles=dir(fullfile(project.folder,alt,'Mopla','*.smct'));
        moplaAltNode=util.getNodeByKey(tree,['mopla_alt_',alt]);
       
%         if isempty(cAlt)
%             util.cleanNode(moplaAltNode,tree);
%         end
        if strcmp(alt,cAlt)
            if isPostForm
                cNode=util.getNodeByKey(tree,['moplaView_',cAlt,'_',cMopla]);            
                util.cleanSiblingNodes(cNode);
            else
                util.cleanNode(moplaAltNode,tree);
            end
        else
                 util.cleanNode(moplaAltNode,tree);
        end
       
        
         for j=1:length(moplaFiles)
            moplaId=moplaFiles(j).name;
            moplaId=moplaId(1:end-5);
            if ~strcmp(moplaId,cMopla) || ~isPostForm || ~strcmp(alt,cAlt)
                %alternativa=smc.loadAlternativa(project.folder,alt);
                mallas=smc.loadMallas(project.folder,alt);
                status=smc.getMoplaStatus(fullfile(project.folder,alt,'Mopla'),moplaId,mallas);
                casoMoplaNode=uitreenode('v0',['moplaView_',alt,'_',moplaId],[moplaId, '[',num2str(status.run),...
                   '/', num2str(status.all),']'],[],false);
                icon='ball_yellow.gif'; 
                if status.run>0, icon='ball_red.gif'; end
                if status.run>=status.all                  
                    icon='ball_green.gif'; 
                    
                end
                
                util.setNodeIcon(casoMoplaNode,icon);
                moplaAltNode.add(casoMoplaNode);
            end
         end  
         tree.reloadNode(moplaAltNode); 
    end
    
     %%%ALTERNATIVA
    if isAltForm
        currentForm.refreshMoplaExplorer;
    end
    
    
    util.refreshTree(tree);
    watchoff;

end
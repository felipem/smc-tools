function [years,msl]=getAnnualMSL(db,lat,lon)

    %%%Load Data and Get Level=Astro + Surge
    [gotTime,gotLevel]=data.getGOTTemporalSerie(db,lat,lon);
    [gosTime,gosLevel]=data.getGOSTemporalSerie(db,lat,lon);

    index=ismember(gotTime,gosTime);
    time=gotTime(index);
    level=gotLevel(index);

    index=ismember(gosTime,time);
    time=gosTime(index);
    level=level+gosLevel(index);

    %%%Annual Mean Sea Level
    timeVec=datevec(time);

    years=[];
    msl=[];
    for y=timeVec(1,1):timeVec(end,1)
        index=timeVec(:,1)==y;
        years=cat(1,years,y);
        msl=cat(1,msl,mean(level(index,1)));
    end
    
    %Data load....
end
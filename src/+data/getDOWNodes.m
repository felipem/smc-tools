function [lat,lon,z]=getDOWNodes(mesh)
    cfg=util.loadConfig;
    ncFile=fullfile('data',cfg('region'),'dow',[mesh,'_b.nc']);
    if ~exist(ncFile,'file') %Solo habrá un nivel de marea
        ncFile=fullfile('data',cfg('region'),'dow',[mesh,'.nc']);
    end
    ncid=netcdf.open(ncFile,'NC_NOWRITE');
    latId=netcdf.inqVarID(ncid,'latitude');
    lonId=netcdf.inqVarID(ncid,'longitude');
    batId=netcdf.inqVarID(ncid,'bathymetry');
    lat=netcdf.getVar(ncid,latId);
    lon=netcdf.getVar(ncid,lonId);
    z=netcdf.getVar(ncid,batId);
    netcdf.close(ncid);
end
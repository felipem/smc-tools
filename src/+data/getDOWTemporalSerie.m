function  [outData,malla]=getDOWTemporalSerie(db,lat,lon,n)
   
    
    %%% Cargo el idioma
    txt=util.loadLanguage;
    cfg=util.loadConfig;
    %%%%%%%%%%%%%%%%%%%
    folders=util.getUserFolders;
    
   
    tic;
    index=-1;
    malla=[];
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Version sin Base de datos solo para usar Cache
    %%%%% Busca el nodo más cercano del cache, no de la 
    %%%%% base de datos
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     cacheFiles=dir(fullfile(folders.cache,'wd_*.dow'));
%     latCache=[];
%     lonCache=[];
%     for i=1:length(cacheFiles)        
%         latLon=regexp(cacheFiles(i).name(4:end-4),'_','split');
%         latCache=cat(1,latCache,str2double(latLon{1}));
%         lonCache=cat(1,lonCache,str2double(latLon{2}));        
%     end
%     if ~isempty(cacheFiles)
%         [~,idx]=min((latCache-lat).^2+(lonCache-lon).^2);    
%         cacheFile=fullfile(folders.cache,cacheFiles(idx).name);
%         outData=data.loadDOWCache(cacheFile);         
%         return;
%     else
%         msgbox('No encontrados ficheros de Cache');
%         outData=[];
%         return;
%     end
    %%%%% End version solo cache
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %Encontrar   
    iMalla=-1;
    dist=realmax;  
    for i=1:length(db.mallas)
        malla=db.mallas(i).nombre;
        [latNodes,lonNodes]=data.getDOWNodes(malla);
              
        if lon<=max(lonNodes) && lon>=min(lonNodes) && lat<=max(latNodes) && lat>=min(latNodes)
            [dist_,index_]=min(sqrt((latNodes-lat).^2+(lonNodes-lon).^2));            
            if dist_<dist
                dist=dist_;
                index=index_;
                iMalla=i;
            end
        end
    end
    
     %Sacar el dato
    if index<0
        %delete(h)
        return;
    else
        malla=db.mallas(iMalla).nombre;
        [latNodes,lonNodes]=data.getDOWNodes(malla);
    end
    
    %compruebo que no está en la cache
   
    file=fullfile(folders.cache,['wd_',num2str(latNodes(index)),'_',num2str(lonNodes(index)),'.dow']);
    %%%Cambiar si da problemas TUNEO Para España
    %file=fullfile(folders.cache,['wd_',num2str(lat),'_',num2str(lon),'.dow']);
    if exist(file,'file')
        outData=data.loadDOWCache(file); %load(file,'-mat');
        %outData=outData.outData;
        %delete(h)
        return;
    end
    
    %Cargo la marea astronomica
    [tideTime,tide]=data.getGOTTemporalSerie(db,lat,lon);
    
    if ~exist(fullfile('data',cfg('region'),'dow',[malla,'_b.nc']),'file') %Solo un nivel de marea
        outData=data.getDOWTemporalSerieNoTide(txt,cfg,malla,index,tide,tideTime);
    else %Con varias nivels de marea           
        
        h=waitbar(0,txt.main('pdLoadData'),'WindowStyle','Modal');

        %Abro el source
        ncid=netcdf.open(fullfile('data',cfg('region'),'dow',[malla(1:4),'_SourceData.nc']),'NC_NOWRITE');
        sdId=netcdf.inqVarID(ncid,'source_data');
        tId=netcdf.inqVarID(ncid,'time');
        sourceData=netcdf.getVar(ncid,sdId);
        time=netcdf.getVar(ncid,tId);
        classIndex=netcdf.getAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'class_index');
        netcdf.close(ncid);
        %%%%%%%%%%%%%%%

        %Con varias nivels de marea   
        ncid=netcdf.open(fullfile('data',cfg('region'),'dow',[malla,'_b.nc']),'NC_NOWRITE');
        hsId=netcdf.inqVarID(ncid,'hs');
        dirId=netcdf.inqVarID(ncid,'wave_dir');
        tId=netcdf.inqVarID(ncid,'time');
        timeMD=netcdf.getVar(ncid,tId);
        hs_b=double(squeeze(netcdf.getVar(ncid,hsId,[0,index-1],[length(timeMD),1])));
        hs_b(hs_b==-999)=NaN;
        dir_b=double(squeeze(netcdf.getVar(ncid,dirId,[0,index-1],[length(timeMD),1])));
        dir_b(dir_b==-999)=NaN;
        netcdf.close(ncid);

        ncid=netcdf.open(fullfile('data',cfg('region'),'dow',[malla,'_p.nc']),'NC_NOWRITE');
        hsId=netcdf.inqVarID(ncid,'hs');
        dirId=netcdf.inqVarID(ncid,'wave_dir');
        hs_p=double(squeeze(netcdf.getVar(ncid,hsId,[0,index-1],[length(timeMD),1])));
        hs_p(hs_p==-999)=NaN;
        dir_p=double(squeeze(netcdf.getVar(ncid,dirId,[0,index-1],[length(timeMD),1])));
        dir_p(dir_p==-999)=NaN;
        netcdf.close(ncid);

        ncid=netcdf.open(fullfile('data',cfg('region'),'dow',[malla,'_m.nc']),'NC_NOWRITE');
        hsId=netcdf.inqVarID(ncid,'hs');
        dirId=netcdf.inqVarID(ncid,'wave_dir');
        tmId=netcdf.inqVarID(ncid,'tp');
        w_sprId=netcdf.inqVarID(ncid,'wave_spectral_angular_spread');
        hs_m=double(squeeze(netcdf.getVar(ncid,hsId,[0,index-1],[length(timeMD),1])));
        hs_m(hs_m==-999)=NaN;
        dir_m=double(squeeze(netcdf.getVar(ncid,dirId,[0,index-1],[length(timeMD),1])));
        dir_m(dir_m==-999)=NaN;
        tm=double(squeeze(netcdf.getVar(ncid,tmId,[0,index-1],[length(timeMD),1])));
        tm(tm==-999)=NaN;
        w_spr=double(squeeze(netcdf.getVar(ncid,w_sprId,[0,index-1],[length(timeMD),1])));
        w_spr(w_spr==-999)=NaN;
        netcdf.close(ncid);

        %reconstruir

        %Normalize data


         if ~exist('n','var') || isempty(n)
             n=length(timeMD);
         end     

         classIndex=classIndex(1:n);

         deltaSourceData=(max(sourceData,[],1)-min(sourceData,[],1));
         minSourceData=min(sourceData,[],1);     
         for i=1:size(sourceData,2)         
             sourceData(:,i)=(sourceData(:,i)-minSourceData(:,i))/deltaSourceData(i);
         end
         centers=sourceData(classIndex',:);

        %   ndir=1;cdir=1;
        %   if isempty(strfind(vars{i},'dir'))
        %      ndir=0;cdir=0;
        %  end     

        waitbar(0.1);

         [~, hs_b] = dow.InterpolacionRBF_Parametros_old (1, 0, 0, centers, sourceData, hs_b(1:n));
         waitbar(0.2);
         [~, hs_m] = dow.InterpolacionRBF_Parametros_old (1, 0, 0, centers, sourceData, hs_m(1:n));
         waitbar(0.3);
         [~, hs_p] = dow.InterpolacionRBF_Parametros_old (1, 0, 0, centers, sourceData, hs_p(1:n));
         waitbar(0.4);

         [~, dir_b] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, dir_b(1:n));
         waitbar(0.5);
         [~, dir_m] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, dir_m(1:n));
         waitbar(0.6);
         [~, dir_p] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, dir_p(1:n));
         waitbar(0.7);


         [~, tm] = dow.InterpolacionRBF_Parametros_old (1, 0, 0, centers, sourceData, tm(1:n));
         waitbar(0.8);
         [~, w_spr] = dow.InterpolacionRBF_Parametros_old (1, 0, 0, centers, sourceData, w_spr(1:n));
         waitbar(0.9);

         time=double(time)/24+datenum(1900,1,1);
         %Cargar marea astronómica y saca los factores de ponderación

         indexIni=find(tideTime==time(1));
         indexEnd=find(tideTime==time(end));   

         tide=tide(indexIni:indexEnd);

         outData.tide=tide;     

         tide=tide-min(tide);
         tide=tide/max(tide);

         d_b=tide; 
         d_b(d_b>0.5)=0.5;
         p_b=(0.5-d_b)/0.5;

         d_m=abs(tide-0.5); 
         d_m(d_m>0.5)=0.5;
         p_m=(0.5-d_m)/0.5;

         d_p=abs(tide-1); 
         d_p(d_p>0.5)=0.5;
         p_p=(0.5-d_p)/0.5;


         %Interpolo
         hs=p_b.*hs_b+p_m.*hs_m+p_p.*hs_p;
         waitbar(0.95);

         cos_dir_b=cosd(dir_b);
         cos_dir_m=cosd(dir_m);
         cos_dir_p=cosd(dir_p);
         sin_dir_b=sind(dir_b);
         sin_dir_m=sind(dir_m);
         sin_dir_p=sind(dir_p);

         cos_dir=cos_dir_b.*p_b+cos_dir_m.*p_m+cos_dir_p.*p_p;
         sin_dir=sin_dir_b.*d_b+sin_dir_m.*d_m+sin_dir_p.*d_p;

         dir_=atan2(sin_dir,cos_dir)*180/pi;
         dir_(dir_<0)=dir_(dir_<0)+360;

         outData.hs=hs;
         outData.dir=dir_;
         outData.time=time;
         outData.tp=tm;
         outData.w_spr=w_spr;
         waitbar(1);
         delete(h);
    end
     
     %%%%%% Grabo la cache
     %save(file,'outData','-v7.3');
     data.saveDOWCache(file,outData);
     toc
end




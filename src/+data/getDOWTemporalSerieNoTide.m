function outData=getDOWTemporalSerieNoTide(txt,cfg,malla,nodeIndex,tide,tideTime)
   
    
    h=waitbar(0,txt.main('pdLoadData'),'WindowStyle','Modal');

    %Abro el source
    ncid=netcdf.open(fullfile('data',cfg('region'),'dow',[malla(1:4),'_SourceData.nc']),'NC_NOWRITE');
    sdId=netcdf.inqVarID(ncid,'source_data');
    tId=netcdf.inqVarID(ncid,'time');
    sourceData=netcdf.getVar(ncid,sdId);
    time=netcdf.getVar(ncid,tId);
    classIndex=netcdf.getAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'class_index');
    netcdf.close(ncid);
    %%%%%%%%%%%%%%%
       
    ncid=netcdf.open(fullfile('data',cfg('region'),'dow',[malla,'.nc']),'NC_NOWRITE');
    hsId=netcdf.inqVarID(ncid,'hs');
    dirId=netcdf.inqVarID(ncid,'wave_dir');
    tpId=netcdf.inqVarID(ncid,'tp');
    tId=netcdf.inqVarID(ncid,'time');
    timeMD=netcdf.getVar(ncid,tId);
    hs=double(squeeze(netcdf.getVar(ncid,hsId,[0,nodeIndex-1],[length(timeMD),1])));
    hs(hs==-999)=NaN;
    dir=double(squeeze(netcdf.getVar(ncid,dirId,[0,nodeIndex-1],[length(timeMD),1])));
    dir(dir==-999)=NaN;
    tp=double(squeeze(netcdf.getVar(ncid,tpId,[0,nodeIndex-1],[length(timeMD),1])));
    tp(tp==-999)=NaN;
    netcdf.close(ncid);

    time=double(time)/24+datenum(1900,1,1);
    %Marea astronómica 
    indexIni=find(tideTime==time(1));
    indexEnd=find(tideTime==time(end));        
    tide=tide(indexIni:indexEnd);
    outData.tide=tide;     
     
    %Normalize data     
    n=length(timeMD);
     
     
     classIndex=classIndex(1:n);

     deltaSourceData=(max(sourceData,[],1)-min(sourceData,[],1));
     minSourceData=min(sourceData,[],1);     
     for i=1:size(sourceData,2)         
         sourceData(:,i)=(sourceData(:,i)-minSourceData(:,i))/deltaSourceData(i);
     end
     centers=sourceData(classIndex',:);  

     waitbar(0.1);
    
    [~, hs] = dow.InterpolacionRBF_Parametros_old (1, 0, 0, centers, sourceData, hs(1:n));     
    waitbar(0.4);
    [~, dir] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, dir(1:n));     
    waitbar(0.7);
    [~, tp] = dow.InterpolacionRBF_Parametros_old (1, 0, 0, centers, sourceData, tp(1:n));
%      
%      

     time=double(time)/24+datenum(1900,1,1);
    
     
%      dir_=atan2(sin_dir,cos_dir)*180/pi;
%      dir_(dir_<0)=dir_(dir_<0)+360;
     
     outData.hs=hs;
     outData.dir=dir;
     outData.time=time;
     outData.tp=tp;
     waitbar(1);
     delete(h);
end
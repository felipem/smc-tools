function [latGos,lonGos] = getGOSPoint(db,lat,lon)
    dist=(db.gosNodes.lat-lat).^2+(db.gosNodes.lon-lon).^2;
    [~,index]=min(dist);
    latGos=db.gosNodes.lat(index);
    lonGos=db.gosNodes.lon(index);
end


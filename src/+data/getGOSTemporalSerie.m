function [time,eta]=getGOSTemporalSerie(db,lat,lon)
    cfg=util.loadConfig;
    dist=(db.gosNodes.lat-lat).^2+(db.gosNodes.lon-lon).^2;
    [~,index]=min(dist);
    file=fullfile('data',cfg('region'),'gos',db.gosNodes.files{index});
    fid=fopen(file,'r');
    n=fread(fid,1,'int32');
    dateIni=fread(fid,1,'double');
    dateEnd=fread(fid,1,'double');
    eta=double(fread(fid,n,'single'));
    fclose(fid);
    time=linspace(dateIni,dateEnd,n)';
end



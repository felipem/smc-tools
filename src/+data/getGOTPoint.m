function [latGot,lonGot] = getGOTPoint(db,lat,lon)
    dist=(db.gotNodes.lat-lat).^2+(db.gotNodes.lon-lon).^2;
    [~,index]=min(dist);
    latGot=db.gotNodes.lat(index);
    lonGot=db.gotNodes.lon(index);
end


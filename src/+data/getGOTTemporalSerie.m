function [time,tide]=getGOTTemporalSerie(db,lat,lon)
    cfg=util.loadConfig;
    dist=(db.gotNodes.lat-lat).^2+(db.gotNodes.lon-lon).^2;
    [~,index]=min(dist);
    file=fullfile('data',cfg('region'),'got',db.gotNodes.files{index});
    fid=fopen(file,'r');
    n=fread(fid,1,'int32');
    dateIni=fread(fid,1,'double');
    dateEnd=fread(fid,1,'double');
    tide=double(fread(fid,n,'single'));
    fclose(fid);
    time=linspace(dateIni,dateEnd,n)';
end
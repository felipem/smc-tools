function [level,eta,tide] = getLevelTemporalSerie(db,lat,lon,time)
       [tideTime,tide]=data.getGOTTemporalSerie(db,lat,lon);
       tide=interp1(tideTime,tide,time);
       [etaTime,eta]=data.getGOSTemporalSerie(db,lat,lon);            
       eta=interp1(etaTime,eta,time);  
       
       level=eta+tide;
       


end


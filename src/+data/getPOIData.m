function [liteData,error]=getPOIData(mopla,poiId,project,alternativa,progressForm)   
%getPOIData: Extrae el resultado de las prpagaciones de mopla en un punto,
%buscando el nodo de la malla m�s cercano
        error='';
        mallas=mopla.mallas(logical(mopla.propagate));
        poi=mopla.pois(poiId);
        folder=fullfile(project.folder,alternativa.nombre,'Mopla','SP');    
        c=1;
        hs=zeros(length(mallas),length(mopla.tide));
        dir=hs;
        %compruebo que no est� en la cache
        poiDir=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId);
        if ~exist(poiDir,'dir')
            mkdir(poiDir);
        end    
        file=fullfile(poiDir,[poiId,'.poi_']);
        
        if exist(file,'file')
            liteData=load(file,'-mat');
            liteData=liteData.liteData;            
            return;
        end
        
        nTotal=length(mopla.tide)*length(mallas);
        for i=1:length(mopla.tide)        
            for j=1:length(mallas)
                mallaId=mallas{j};            
                malla=alternativa.mallas(mallaId);
                casoId=['000',num2str(c)];
                casoId=casoId(end-3:end);
                grdFileHs=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_Height.GRD']);
                grdFileDir=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_Direction.GRD']);
                try
                    hs(j,i) = util.readGrdPoint(grdFileHs,malla,poi.x,poi.y);
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %%% Direcciones, la pongo DESDE su procedencia y 0� Norte
                    %%% 90� Este
                    %%%%%%%%%%%%%%%%%%%%%%%%
                    dir_=util.readGrdPoint(grdFileDir,malla,poi.x,poi.y);  %%La que lee del GRD                
                    dir_=-dir_+malla.angle; %%%Respecto al eje X_UTM (el signo del GRD est� mal).                
                    dir_=270-dir_;  %%%% Respecto al norte y en la direcci�n de la que viene el oleaje 
                    if dir_>360,dir_=dir_-360;end   %%% entre 0 y 360�
                    if dir_<0,dir_=dir_+360;end
                    %%% Fin direcciones
                    %%%%%%%%%%%%%%%%%%%%%%%%
                catch
                    liteData=[];
                    error=[mallaId,mopla.moplaId,casoId];                    
                    return;
                end
                dir(j,i) =dir_; 
                if exist('progressForm','var')
                    progressForm.setProgress(c/nTotal);
                end
                c=c+1;
            end
        end
        
        %Grabo el fichero
        liteData.hs=hs;
        liteData.dir=dir;
        save(file,'liteData','-v7.3');
end
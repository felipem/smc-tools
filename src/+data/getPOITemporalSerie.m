function outData = getPOITemporalSerie(mopla,poiId,project,alternativa)

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    
    
    casosMD=mopla.casosMD(logical(mopla.propagate));
    mallas=mopla.mallas(logical(mopla.propagate));
    
   
    
    poi=mopla.pois(poiId);
    folder=fullfile(project.folder,alternativa.nombre,'Mopla','SP');    
    c=1;
    hs=zeros(length(mallas),length(mopla.tide));
    dir=hs;
    
    
    %compruebo que no est� en la cache
    poiDir=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId);
    if ~exist(poiDir,'dir')
        mkdir(poiDir);
    end
    file=fullfile(poiDir,[poiId,'.poi']);
    liteFile=fullfile(poiDir,[poiId,'.poi_']);
    if exist(file,'file')
        outData=load(file,'-mat');
        outData=outData.outData;       
        return;
    end
    
     h=waitbar(0,txt.main('pdLoadData'),'WindowStyle','Modal');
    
    for i=1:length(mopla.tide)        
        for j=1:length(mallas)
            mallaId=mallas{j};            
            malla=alternativa.mallas(mallaId);
            casoId=['000',num2str(c)];
            casoId=casoId(end-3:end);
            grdFileHs=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_Height.GRD']);
            grdFileDir=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_Direction.GRD']);
            try
                hs(j,i) = util.readGrdPoint(grdFileHs,malla,poi.x,poi.y);
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%% Direcciones, la pongo DESDE su procedencia y 0� Norte
                %%% 90� Este
                %%%%%%%%%%%%%%%%%%%%%%%%
                dir_=util.readGrdPoint(grdFileDir,malla,poi.x,poi.y);  %%La que lee del GRD                
                dir_=-dir_+malla.angle; %%%Respecto al eje X_UTM (el signo del GRD est� mal).                
                dir_=270-dir_;  %%%% Respecto al norte y en la direcci�n de la que viene el oleaje 
                if dir_>360,dir_=dir_-360;end   %%% entre 0 y 360�
                if dir_<0,dir_=dir_+360;end
                %%% Fin direcciones
                %%%%%%%%%%%%%%%%%%%%%%%%
            catch
                outData=[];
                msgbox({txt.post('iOlucaReadErrorLabel'),[mallaId,mopla.moplaId,casoId]},txt.post('iOlucaReadErrorTitle'),'error');
                delete(h);
                return;
            end
            dir(j,i) =dir_; 
            c=c+1;
        end
    end
    
    
    %%%Creo el source
    %sourceData=[mopla.hs,cosd(mopla.dir),sind(mopla.dir),mopla.tp];
    sourceData=[mopla.hs,mopla.tp,cosd(mopla.dir),sind(mopla.dir)];
    classIndex=casosMD;
    
    
    %%%%Creo el fichero .poi_ para poder obtener los datos intermedios
    liteData.hs=hs;
    liteData.dir=dir;
    save(liteFile,'liteData','-v7.3');
    
    

%      deltaSourceData=(max(sourceData,[],1)-min(sourceData,[],1));
%      minSourceData=min(sourceData,[],1);     
%      for i=1:size(sourceData,2)         
%          sourceData(:,i)=(sourceData(:,i)-minSourceData(:,i))/deltaSourceData(i);
%      end
    
    sourceData(:,1)=(sourceData(:,1)-min(sourceData(:,1)))/(max(sourceData(:,1))-min(sourceData(:,1)));
    sourceData(:,2)=(sourceData(:,2)-min(sourceData(:,2)))/(max(sourceData(:,2))-min(sourceData(:,2)));

     centers=sourceData(classIndex,:);
     
     
     %%%Corrijo la parte de las funciones de transferencia
     hs_prop=mopla.hs(casosMD);
     tp_prop=mopla.tp(casosMD);
     dir_prop=mopla.dir(casosMD);
     index=hs_prop<mopla.hsMin;     
     for i=1:length(mopla.tide)
         alpha_hs=hs(:,i)./hs_prop;
         %alpha_cosdir=cosd(dir(:,i))./cosd(dir_prop);
         %alpha_sindir=sind(dir(:,i))./sind(dir_prop);
         
         hs(index,i)=hs_prop(index).*alpha_hs(index);
%          cos_dir=cosd(dir_prop(index)).*alpha_cosdir(index);
%          sin_dir=sind(dir_prop(index)).*alpha_sindir(index);
%          dir_tmp=atan2(sin_dir,cos_dir)*pi/180;
%          dir_tmp(dir_tmp<0)=360+dir_tmp(dir_tmp<0);
 %        dir(index,i)=dir_tmp;
     end
     
     waitbar(0.3);
     
     %%%Cargo la marea astronómica
     db=util.loadDb;
     [tideTime,tide]=data.getGOTTemporalSerie(db,mopla.lat,mopla.lon);
     tide=interp1(tideTime,tide,mopla.time);
     tide=tide-min(tide); %Esto no está muy bien..... ya que depende del periodo temporal.
     %index= tide.time==mopla.time;
     %tide=tide.tide(index);
     
     %Normalizo entre 0-1 y obtengo los pesos
%      tide=tide-min(tide);
%      tide=tide/max(tide);
%      tideN=mopla.tide;
%      tideN=tideN-min(tideN);
%      tideN=tideN/max(tideN);
%      
%      pesos=zeros(length(tide),length(tideN));     
%      for i=2:length(tideN)
%          deltaTide=tideN(i)-tideN(i-1);
%          delta2=tideN(i)-tide;
%          delta1=tideN(i-1)-tide;
%          index=(delta2>=0 & delta1<=0);
%          pesos(index,i-1)=1-delta1(index)./deltaTide;
%          pesos(index,i)=1-abs(delta2(index))./deltaTide;         
%      end
%      if length(tideN)==1
%          pesos=ones(length(tide),length(tideN)); 
%      end
     
     %%%Reconstruyo HS
     outData.hs=zeros(length(mopla.hs),1);
     outData.hs_all=zeros(length(mopla.hs),length(mopla.tide));
     
     for i=1:length(mopla.tide)
        [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, hs(:,i));
        tmp(tmp<0.001)=0.001;
        outData.hs_all(:,i)=tmp;  
        %outData.hs=outData.hs+tmp.*pesos(:,i);
     end
     outData.hs=post.interpolateTide(tide,mopla.tide,outData.hs_all);
     waitbar(0.6);
     
     %%%Reconstruyo Direcion
     
     outData.dir_all=zeros(length(mopla.hs),length(mopla.tide));
     for i=1:length(mopla.tide)
        [~,tmp] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, dir(:,i));
        outData.dir_all(:,i)=tmp;     
     end
     
     cos_dir=post.interpolateTide(tide,mopla.tide,cosd(outData.dir_all));
     sin_dir=post.interpolateTide(tide,mopla.tide,sind(outData.dir_all));
            
     
     waitbar(0.9);
     
     outData.dir=atan2(sin_dir,cos_dir)*180/pi;
     outData.dir(outData.dir<0)=outData.dir(outData.dir<0)+360;
     outData.tp=mopla.tp;
     outData.time=mopla.time;
     
     waitbar(1);
     delete(h);
     
     %%%%%% Grabo la cache
     save(file,'outData','-v7.3');
end


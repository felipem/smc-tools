function data=loadDOWCache(file)

    fid=fopen(file,'r');
    
    n=fread(fid,1,'int32');
    dateIni=fread(fid,1,'double');
    dateEnd=fread(fid,1,'double');
    data.time=linspace(dateIni,dateEnd,n)';
    data.hs=fread(fid,n,'double');   
    data.dir=fread(fid,n,'double');   
    data.tp=fread(fid,n,'double');   
    %data.w_spr=fread(fid,n,'double');   
    data.tide=fread(fid,n,'double');       
    
    fclose(fid);

end
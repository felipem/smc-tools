function saveDOWCache(file,data)
    fid=fopen(file,'w');
    fwrite(fid,int32(length(data.hs)),'int32');
    fwrite(fid,double(data.time(1)),'double');
    fwrite(fid,double(data.time(end)),'double');    
    
    fwrite(fid,data.hs,'double');
    fwrite(fid,data.dir,'double');
    fwrite(fid,data.tp,'double');
    %fwrite(fid,data.w_spr,'double');
    fwrite(fid,data.tide,'double');
    
    fclose(fid);
end
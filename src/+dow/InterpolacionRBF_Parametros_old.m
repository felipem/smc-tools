function [sigma_optimo, resultados] = InterpolacionRBF_Parametros_old (parametros, ndireccion, cdireccion, subset_n, PCs_n, Propagaciones)

%Barrido en sigma
sigmin=0.010;
sigmax=0.5;

%Calculo del sigma optimo para cada uno de los parametros
sigma_optimo=zeros(parametros+ndireccion,1);
resultados=zeros(length(PCs_n),parametros);
par=1;
for j=1:parametros
    
    if ismember(j,cdireccion) %variable direccion 
        
        %Componentes X e Y de la direccion 
        m=length(Propagaciones);
        DirX=zeros(m,1);
        DirY=zeros(m,1);
        temp=pi/2-Propagaciones(:,j)*pi/180;
        dd=find(temp<-pi);
        temp(dd)=2*pi+temp(dd);
        DirX=cos(temp);  %componentes angulares
        DirY=sin(temp);
        sigma_optimo(par) = dow.evaluarmin(subset_n', DirX', sigmin, sigmax);   %DirX
        sigma_optimo(par+1) = dow.evaluarmin(subset_n', DirY', sigmin, sigmax);  %DirY
        
        %Reconstruccion serie DirX
        coeff_DirX = dow.rbfcreate_modificado(subset_n',DirX','RBFFunction','gaussian','RBFConstant',sigma_optimo(par));
        DirXp = dow.rbfinterp_modificado(PCs_n',coeff_DirX);
        
        %Reconstruccion serie DirY
        coeff_DirY = dow.rbfcreate_modificado(subset_n',DirY','RBFFunction','gaussian','RBFConstant',sigma_optimo(par+1));
        DirYp = dow.rbfinterp_modificado(PCs_n',coeff_DirY);
        
        %Dir
        Dirp = atan2(DirYp,DirXp)*180/pi; %angulo seg�n el criterio de matlab
        Dirp = 90-Dirp;
        qq=find(Dirp<0);  Dirp(qq)=Dirp(qq)+360;
        resultados(:,j)=Dirp;
        
        par=par+2;
    else
            sigma_optimo(par) = dow.evaluarmin(subset_n', Propagaciones(:,j)', sigmin, sigmax);
            coeff = dow.rbfcreate_modificado(subset_n',Propagaciones(:,j)','RBFFunction','gaussian','RBFConstant',sigma_optimo(par));
            resultados(:,j) = dow.rbfinterp_modificado(PCs_n',coeff);
            par=par+1;
        
    end
end





function [dis] = distancia (M,D,tipo)  
    
   % [m,n]=size(M);
   % dx=zeros(m,n);
    
    switch tipo
        case 'euclidiana'
            Dist=(M-D).^2;
            [qerr bmu] = min(((M-D).^2)); % minimum distance(^2) and the BMU
            dis=Dist(bmu(1));
        case  'euclidiana2'
             noang=[1 2 4];
             dx(noang,:)=M(noang,:) - D(noang,:); 
             ang=[3 5];
             dx(ang,:)=min(abs(D(ang,:)-M(ang,:)),2*pi-abs(D(ang,:)-M(ang,:)))./pi;  
             Dist=sqrt(abs(sum(dx.^2,1)));
             Dst=sort(Dist);
             pos=find(Dst(2)==Dist);
             dis= Dist(pos); % minimum distance(^2) and the BMU    
                               
      otherwise error('Error');
          
  end    
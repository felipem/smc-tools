classdef GisForm<handle
    properties (Access=private)
        % oldInfoWidth;
         meshIndex=[];
         dowLayers=[];         
         gotLayers=[];
         gosLayers=[];
         cartasLayer=[];
         maxRegion=1;
         batys=[];
         imageLayers=[];
         cacheLayer=[];
         cmImage=[];
         cmImages=[];
         currentPoint=[];
         currentRegion=[];
         txt;
         cfg;
         folders;
    end
    properties  
        ax;
        %infoPanel;
        rootNode;
        panel;
        tree;
        db;
        toolbar;
        toolsCount;
    end
    
    events
        ChangeMenu;
    end
    
    methods
        function self=GisForm(db,panel,toolbar,tree,rootNode)
            try
            %%% Cargo el idioma
           self.txt=util.loadLanguage;
           self.cfg=util.loadConfig;
            %%%%%%%%%%%%%%%%%%%
            self.folders=util.getUserFolders;
            
            %%%%Esconder el panel lateral
%             set(infoPanel,'Visible','off');
%             pos=get(infoPanel,'Position');
%             self.oldInfoWidth=pos(3);
%             pos=get(panel,'Position');pos(3)=pos(3)+self.oldInfoWidth;
%             set(panel,'Position',pos);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            self.panel=panel;
            self.toolbar=toolbar;
            self.db=db;
            self.tree=tree;
            self.rootNode=rootNode;
            
            %%%%%InitMap
            self.ax=axes('Parent',self.panel,'Position',[0.05 0.05 0.9 0.9]);            
            hold(self.ax,'on');
            shp=load(fullfile('data',self.cfg('region'),'shp','regionalMapNoCountry.mat'));
            shp=shp.shp;
            for i=1:length(shp.maps)
                fill(shp.maps(i).x,shp.maps(i).y,str2num(self.cfg('regionColor')),'Parent',self.ax);
            end            
            shp=load(fullfile('data',self.cfg('region'),'shp','detailMap.mat'));
            shp=shp.shp;
            for i=1:length(shp.maps)
                fill(shp.maps(i).x,shp.maps(i).y,str2num(self.cfg('countryColor')),'Parent',self.ax);
            end
            hold(self.ax,'off');
            set(self.ax,'box','on');
            %axis(self.ax,[-90, -20, -60,20]);           
            axis(self.ax,'equal');

            %%%%%%%%%%%%%%%%%%%%%%%
            
            self.initExplorer();
            self.initLayers;
            self.initContextMenu();
            self.initToolBar();
            catch ex
                delete(self);
                error(ex.message);
            end
        end
        
        function delete(self)
%             pos=get(self.panel,'Position');pos(3)=pos(3)-self.oldInfoWidth;
%             set(self.panel,'Position',pos);
%             set(self.infoPanel,'Visible','on');
            
            %Elimino los hijos por si hay algo
            util.cleanNode(self.rootNode,self.tree)
    
            
            jToolbar = get(get(self.toolbar,'JavaContainer'),'ComponentPeer');
            totalTools=jToolbar.getComponentCount();
            for i=totalTools-1:-1:totalTools-self.toolsCount;
                 jToolbar.remove(i);
            end
            jToolbar(1).repaint;
            jToolbar(1).revalidate;
            
            util.resetPanel(self.panel);
            
            for i=1:length(self.cartasLayer)
                delete(self.cartasLayer(i));
            end
            
             for i=1:length(self.dowLayers)
                delete(self.dowLayers(i));
             end
             
             for i=1:length(self.gosLayers)
                delete(get(self.gosLayers(i),'UIContextMenu'));       
                delete(self.gosLayers(i));
             end
             
              for i=1:length(self.gotLayers)
                delete(get(self.gotLayers(i),'UIContextMenu'));       
                delete(self.gotLayers(i));
             end
             
             images=keys(self.imageLayers);
             for i=1:length(images)
                 delete(self.imageLayers(images{i}));
             end
             delete(self.imageLayers);
               
            self.dowLayers=[];
            self.cartasLayer=[];
            delete(self.cacheLayer);
            self.cacheLayer=[];
            if ishandle(self.currentPoint),delete(self.currentPoint);end;
            if ishandle(self.currentRegion),delete(self.currentRegion);end;
            delete(self.txt);
        end
        
        function initToolBar(self)
            self.toolsCount=0;
            self.toolsCount=self.toolsCount+core.navigateToolbar(self.toolbar,gca);
            self.toolsCount=self.toolsCount+core.measureToolbar(self.toolbar,gca,'geo');
            pause(0.2);
            self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar);
  
            ptSelectPoint=uipushtool(self.toolbar,'ClickedCallback',@self.selectPoint,'TooltipString',self.txt.gis('tbSelectPoint'));            
            util.setControlIcon( ptSelectPoint,'add_point.gif');    
            
            ptSelectRegion=uipushtool(self.toolbar,'ClickedCallback',@self.selectRegion,'TooltipString',self.txt.gis('tbSelectRegion'));            
            util.setControlIcon( ptSelectRegion,'selection.gif');  
            
            ptAddImage=uipushtool(self.toolbar,'ClickedCallback',@self.addImage,'TooltipString',self.txt.gis('tbAddImage'));            
            util.setControlIcon(ptAddImage,'google_earth.gif');  
            
            self.toolsCount=self.toolsCount+3;
        end
        
        function initExplorer(self)
            util.cleanNode(self.rootNode,self.tree);
            dowNode=uitreenode('v0','gis_dow',self.txt.gis('exDow'),[],false);
            util.setNodeIcon(dowNode,'dow.gif');
            gosNode=uitreenode('v0','chk_gos_0',self.txt.gis('exGos'),[],true);
            util.setNodeIcon(gosNode,'gos.gif');
            gotNode=uitreenode('v0','chk_got_0',self.txt.gis('exGot'),[],true);
            util.setNodeIcon(gotNode,'got.gif');
            cartasNode=uitreenode('v0','chk_cartas_0',self.txt.gis('exNauticalCharts'),[],true);
            util.setNodeIcon(cartasNode,'image.gif');
            cacheNode=uitreenode('v0','chk_cache_0',self.txt.gis('exCache'),[],true);
            util.setNodeIcon(cacheNode,'cache.gif');
            self.rootNode.add(cartasNode);
            self.rootNode.add(cacheNode);
            self.rootNode.add(gotNode);
            self.rootNode.add(gosNode);
            self.rootNode.add(dowNode);            
            for i=1:length(self.db.mallas)
                mesh=self.db.mallas(i).nombre;
                dowMeshNode=uitreenode('v0',['chk_gis_dow_',mesh,'_0'],mesh,[],true);
                util.setNodeIcon(dowMeshNode,'malla.gif');
                dowNode.add(dowMeshNode);                
            end
            imagesNode=uitreenode('v0','fld_gis_images',self.txt.gis('exImages'),[],false);
            util.setNodeIcon(imagesNode,'folder_closed.png');
            self.rootNode.add(imagesNode);
            imFiles=dir(fullfile(self.folders.cache,'*.img'));
            for i=1:length(imFiles)
                imFile=fullfile(self.folders.cache,imFiles(i).name);
                [~,imageId,~]=fileparts(imFile);
                imageNode=uitreenode('v0',['chk_gis_image_',imageId,'_1'],imageId,[],true);
                util.setNodeIcon(imageNode,'google_earth.gif');
                imagesNode.add(imageNode);
            end
            
            
            self.tree.reloadNode(self.rootNode);
            util.refreshTree(self.tree);
        end
        
        function initLayers(self)            
            hold(gca,'on');
            
            %%%%%Imagenes
            self.imageLayers=containers.Map;
            imFiles=dir(fullfile(self.folders.cache,'*.img'));
            for i=1:length(imFiles)
                imFile=fullfile(self.folders.cache,imFiles(i).name);
                [imageId,I,x,y]=self.loadImagen(imFile);   
                im=image(x,y,I); 
                self.imageLayers(imageId)=im;
            end
            
            %%%%%Cache
             files=dir(fullfile(self.folders.cache,'*.dow'));
             lat=zeros(length(files),1);
             lon=lat;
             for i=1:length(files)
                 nodo=files(i).name;
                 latLon=regexp(nodo(4:end-4),'_','split');
                 lon(i)=str2double(latLon{2});
                 lat(i)=str2double(latLon{1});                 
             end
             
             self.cacheLayer=plot(self.ax,lon,lat,'or','MarkerSize',12,'MarkerFaceColor',[255;190;50]/255,'Visible','off','LineWidth',2);

            
            %%%%%Mallas DOW            
            self.meshIndex=util.initMap(self.meshIndex);
            for i=1:length(self.db.mallas)
                mesh=self.db.mallas(i).nombre;
                [lat,lon,z]=data.getDOWNodes(mesh);                              
                p=plot(self.ax,lon,lat,'ko','Visible','off','MarkerFaceColor','k','MarkerSize',3);                                  
                self.meshIndex(mesh)=i;
                self.dowLayers(i)=p;                
                self.batys(i).z=z;
            end
            
            
            %%%%%Nodos Gos (pelotas azules)
            for i=1:length(self.db.gosNodes.lon)                
                lat=self.db.gosNodes.lat(i);
                lon=self.db.gosNodes.lon(i);
                self.gosLayers(i)=plot(lon,lat,'ob','MarkerSize',12,'MarkerFaceColor',[135;206;250]/255,'Visible','off','LineWidth',2);
                gosMenu=uicontextmenu;
                uimenu(gosMenu,'Label',['Lat: ',num2str(lat)]);
                uimenu(gosMenu,'Label',['Lon: ',num2str(lon)]);
                uimenu(gosMenu,'Label','Ameva...','Callback',{@self.addAmevaNode,lat,lon,'GOS'});
                set(self.gosLayers(i),'UIContextMenu',gosMenu);                
            end
            
            
            %%%%%Nodos Got (pelotas verdes)
            for i=1:length(self.db.gotNodes.lon)
                lat=self.db.gotNodes.lat(i);
                lon=self.db.gotNodes.lon(i);
                self.gotLayers(i)=plot(lon,lat,'o','Color',[47;79;47]/255,'MarkerSize',12,'MarkerFaceColor',[155;205;155]/255,'Visible','off','LineWidth',2);
                gotMenu=uicontextmenu;
                uimenu(gotMenu,'Label',['Lat: ',num2str(lat)]);
                uimenu(gotMenu,'Label',['Lon: ',num2str(lon)]);
                uimenu(gotMenu,'Label','Ameva...','Callback',{@self.addAmevaNode,lat,lon,'GOT'});
                set(self.gotLayers(i),'UIContextMenu',gotMenu);                
            end
            
            
            %%%%%Cartas Nauticas
             for i=1:length(self.db.cartas)
                carta=self.db.cartas(i);                
                f=fill(carta.lon([1 1 2 2]),carta.lat([1 2 2 1]),'r','EdgeColor','r','FaceColor','none','Visible','off');                               
                self.cartasLayer(i)=f;
             end  
             
                          hold(gca,'off');
        end
        
        function initContextMenu(self)
            self.cmImage.jmenu=javax.swing.JPopupMenu;
            
            self.cmImage.eliminar=javax.swing.JMenuItem(self.txt.gis('cmDeleteImage'));                        
            set(self.cmImage.eliminar,'ActionPerformedCallback',@self.eliminarImagen);                    
            self.cmImage.jmenu.add(self.cmImage.eliminar);
            
             self.cmImages.jmenu=javax.swing.JPopupMenu;
            
            self.cmImages.eliminar=javax.swing.JMenuItem(self.txt.gis('cmDeleteImageAll'));                        
            set(self.cmImages.eliminar,'ActionPerformedCallback',@self.eliminarTodasImagenes);                    
            self.cmImages.jmenu.add(self.cmImages.eliminar);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Toolbar
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods 
        function selectPoint(self,varargin) 
            core.blockMainForm;
       util.disableOtherButtons(self.toolbar,0)
            [x,y]=ginput(1);
            lat=[];
            lon=[];
            Z=0;
            mesh=[];
            index=-1;
            iMalla=-1;
            for i=1:length(self.dowLayers)
                p=self.dowLayers(i);
                xData=get(p,'XData');yData=get(p,'YData');
                xMin=min(xData);xMax=max(xData);
                yMin=min(yData);yMax=max(yData);
                dist=realmax;
                if xMin<=x && xMax>=x && yMin<=y && yMax>=y
                    [dist_,index_]=min(sqrt((xData-x).^2+(yData-y).^2));
                    if dist_<dist
                        dist=dist_;
                        index=index_;
                        iMalla=i;
                    end
                end                    
            end
            if index>0
                i=iMalla;
                p=self.dowLayers(i);
                xData=get(p,'XData');yData=get(p,'YData');
                lon=xData(index);
                lat=yData(index);
                mesh=self.db.mallas(i).nombre;
                %TODO obtener Z con una funci�n propia
                z=self.batys(i).z(index);
            end
            
            if isempty(mesh)
                core.blockMainForm;
                return;
            end
            
            if ishandle(self.currentPoint),delete(self.currentPoint);end;
            hold(gca,'on');
            point=plot(lon,lat,'or','MarkerSize',12,'MarkerFaceColor','r');
            hold(gca,'off');
            core.blockMainForm;
            
           option = gis.selectPointForm(lat,lon,z);
           switch option
               case 'cancel'
                   if ishandle(self.currentPoint),delete(self.currentPoint);end;
                 if ishandle(point),delete(point);end;                                  
               case 'ameva'
                  if ishandle(self.currentPoint),delete(self.currentPoint);end;
                 if ishandle(point),delete(point);end;    
                  core.addAmevaNode(self.tree,lat,lon,'DOW');
                  notify(self,'ChangeMenu');
               case 'move'
                 if ishandle(self.currentPoint),delete(self.currentPoint);end;
                 self.currentPoint=point;                 
                 self.selectPoint;    
           end
        end
        
        function selectRegion(self,varargin) 
              core.blockMainForm;
           try
              util.disableOtherButtons(self.toolbar,0)
             
            rect=getrect(self.ax);
            
            x=[rect(1),rect(1)+rect(3)];
           
            y=[rect(2),rect(2)+rect(4)];
           
            
            %%%Comprueba si hay m�s de una zona UTM
            utmZones=gis.getUtmZones(x(1),y(1),x(2),y(2));
            nUtmZones=size(utmZones,1);
            if nUtmZones>1
                [y,x] = gis.selectUtmZoneForm(utmZones,y,x);
                if isempty(y)
                    core.blockMainForm;
                    return;
                end
            end
            
           xRect=x([1 1 2 2]);
           yRect=y([1 2 2 1]);
            
            hold(self.ax,'on');
            if ishandle(self.currentRegion),delete(self.currentRegion),end;
            self.currentRegion=fill(xRect,yRect,'y','EdgeColor','k');
            hold(self.ax,'off');
           
            %%%%Carga la batimetría y las líneas de costa            
           
            bat=[];
            lineas=[];
            for i=1:length(self.db.batRegion) 
                region=self.db.batRegion(i);
                
                %batimetria
                xyz=load(fullfile('data',self.cfg('region'),'bathymetry',[region.nombre,'.xyz']),'-mat');
                xyz=xyz.xyz;
                index=xyz(:,1)>=xRect(1) & xyz(:,1)<=xRect(3) & xyz(:,2)>=yRect(1) & xyz(:,2)<=yRect(2);
                bat=cat(1,bat,xyz(index,:));
                
                %linea de costa
                xy=load(fullfile('data',self.cfg('region'),'bathymetry',[region.nombre,'.xy']),'-mat');
                xy=xy.xy;
                index=xy(:,1)>=xRect(1) & xy(:,1)<=xRect(3) & xy(:,2)>=yRect(1) & xy(:,2)<=yRect(2);
                xy=xy(index,:);
                lc=unique(xy(:,3));
                for l=1:length(lc)
                    indexL=xy(:,3)==lc(l);
                    line.x=xy(indexL,1);
                    line.y=xy(indexL,2);
                    lineas=cat(1,lineas,line);
                    %%%% Add coast to bat
                    %bat=cat(1,bat,[line.x,line.y,-6*ones(length(line.x),1)]);
                end                
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
           
           %%%% Grideo la batimetria
           x=linspace(min(bat(:,1)),max(bat(:,1)),50);
           y=linspace(min(bat(:,2)),max(bat(:,2)),50);
           [X,Y]=meshgrid(x,y);
           F = TriScatteredInterp(bat(:,1),bat(:,2),bat(:,3));
           Z=F(X,Y);         
           images=self.getImagesRegion(rect(1),rect(1)+rect(3),rect(2),rect(2)+rect(4));              
           catch ex
               msgbox(ex.message);               
               core.blockMainForm;
               return;
           end
           core.blockMainForm;
         [option,checkImages]= gis.selectRegionForm(X,Y,Z,lineas,images);
         switch option
             case 'redefine'
                  self.selectRegion();
             case 'cancel'
                      if ishandle(self.currentRegion),delete(self.currentRegion),end;
                       if ishandle(self.currentRegion),delete(self.currentRegion),end;
             case 'createSmc'  
                 core.blockMainForm;
                  if ishandle(self.currentRegion),delete(self.currentRegion),end;
                       if ishandle(self.currentRegion),delete(self.currentRegion),end;
                
                folder=uigetdir('',self.txt.gis('qSMCFolder'));
                name=inputdlg(self.txt.gis('qSMCName')); 
                pause(0.1);
                if isempty(folder) || isempty(name)
                    core.blockMainForm;
                    return;
                end
                dowNodes.lon=[];
                dowNodes.lat=[];
                dowNodes.z=[];
                 for j=1:length(self.dowLayers)
                    p=self.dowLayers(j);
                    xData=get(p,'XData');yData=get(p,'YData');
                    index=xData>=xRect(1) & xData<=xRect(3) & yData>=yRect(1) & yData<=yRect(2);                                
                    dowNodes.lon=cat(1,dowNodes.lon,xData(index)');
                    dowNodes.lat=cat(1,dowNodes.lat,yData(index)');
                    dowNodes.z=cat(1,dowNodes.z,self.batys(j).z(index));
                 end               
                 if checkImages
                    [created,err]=smc.createProyect(folder,name{1},bat,[],lineas,dowNodes,images);
                 else
                    [created,err]=smc.createProyect(folder,name{1},bat,[],lineas,dowNodes);
                 end
                 core.blockMainForm;
                 if created
                     msgbox(self.txt.gis('eSmcOk'));
                 else
                     msgbox(err,'error','error')
                 end
                 
         end
            
        end
        
        function addImage(self,varargin)
          util.disableOtherButtons(self.toolbar,0)
            
            rect=getrect(self.ax);
            lonIni=rect(1);
            latIni=rect(2);
            lonEnd=lonIni+rect(3);
            latEnd=latIni+rect(4);
            [I,x,y]=util.getGMImage(latIni,lonIni,latEnd,lonEnd,'satellite',[]);            
            if isempty(I) %%%Problemas con la conexion a internet
                return;
            end
            hold(self.ax,'on');
            im=image(x,y,I);    
            self.refreshLayers;
            hold(self.ax,'off');
            imagesNode=util.getNodeByKey(self.tree,'fld_gis_images');            
            imageId=['GM',datestr(clock,'yyyymmddhhMMss')];
            self.imageLayers(imageId)=im;
            imageNode=uitreenode('v0',['chk_gis_image_',imageId,'_1'],imageId,[],true);
            util.setNodeIcon(imageNode,'google_earth.gif');
            imagesNode.add(imageNode);
            self.tree.reloadNode(imagesNode);
            self.grabarImagen(imageId,I,x,y);
            %util.refreshTree(self.tree);
            
        end
    end
    %%%   End Toolbar
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Layers
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods 
        function showHideDow(self,mesh)
            meshInd=self.meshIndex(mesh);
            visible=get(self.dowLayers(meshInd),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            set(self.dowLayers(meshInd),'Visible',visible);
        end
        
        function showHideCartas(self)
            if isempty(self.cartasLayer),return;end                        
            visible=get(self.cartasLayer(1),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            for i=1:length(self.cartasLayer)
                set(self.cartasLayer,'Visible',visible);
            end
        end
        
        function showHideCache(self)
            if isempty(self.cacheLayer),return;end                        
            visible=get(self.cacheLayer,'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                            
            set(self.cacheLayer,'Visible',visible);
            
        end      
        
        function showHideGos(self)
            if isempty(self.gosLayers),return;end                        
            visible=get(self.gosLayers(1),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                            
            for i=1:length(self.gosLayers)
                set(self.gosLayers(i),'Visible',visible);
            end
         end      
                 
        function showHideGot(self)
             if isempty(self.gotLayers),return;end                        
            visible=get(self.gotLayers(1),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                            
            for i=1:length(self.gotLayers)
                set(self.gotLayers(i),'Visible',visible);
            end
            
        end      
        
        function showHideImage(self,imageId)
            visible=get(self.imageLayers(imageId),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            set(self.imageLayers(imageId),'Visible',visible);
        end
        
        function refreshLayers(self)
            hold(self.ax,'on');           
            
            for i=1:length(self.db.mallas)                                                  
                x=get(self.dowLayers(i),'XData');
                y=get(self.dowLayers(i),'YData');
                visible=get(self.dowLayers(i),'Visible');
                delete(self.dowLayers(i));
                self.dowLayers(i)=plot(self.ax,x,y,'k.','Visible',visible);                                
            end
            
            if ~isempty(self.gosLayers)
                visible=get(self.gosLayers(1),'Visible');
            end
            
            for i=1:length(self.gosLayers)
                x=get(self.gosLayers(i),'XData');
                y=get(self.gosLayers(i),'YData');                
                delete(get(self.gosLayers(i),'UIContextMenu'));                
                delete(self.gosLayers(i));                
                self.gosLayers(i)=plot(self.ax,x,y,'ob','MarkerSize',12,'MarkerFaceColor',[135;206;250]/255,'Visible',visible);  
                gosMenu=uicontextmenu;
                uimenu(gosMenu,'Label',['Lat: ',num2str(y)]);
                uimenu(gosMenu,'Label',['Lon: ',num2str(x)]);
                uimenu(gosMenu,'Label','Ameva...','Callback',{@self.addAmevaNode,y,x,'GOS'});
                set(self.gosLayers(i),'UIContextMenu',gosMenu);         
            end
             if ~isempty(self.gotLayers)
                visible=get(self.gotLayers(1),'Visible');
             end
            
             for i=1:length(self.gotLayers)
                x=get(self.gotLayers(i),'XData');
                y=get(self.gotLayers(i),'YData');                
                delete(get(self.gotLayers(i),'UIContextMenu'));                
                delete(self.gotLayers(i));                
                self.gotLayers(i)=plot(self.ax,x,y,'o','MarkerSize',12,'Color',[47;79;47]/255,'MarkerFaceColor',[155;205;155]/255,'Visible',visible);  
                gotMenu=uicontextmenu;
                uimenu(gotMenu,'Label',['Lat: ',num2str(y)]);
                uimenu(gotMenu,'Label',['Lon: ',num2str(x)]);
                uimenu(gotMenu,'Label','Ameva...','Callback',{@self.addAmevaNode,y,x,'GOT'});
                set(self.gotLayers(i),'UIContextMenu',gotMenu);         
             end
            
            
             for i=1:length(self.db.cartas)
                x=get(self.cartasLayer(i),'XData');
                y=get(self.cartasLayer(i),'YData');
                visible=get(self.cartasLayer(i),'Visible');
                delete(self.cartasLayer(i));
                self.cartasLayer(i)=fill(x,y,'r','EdgeColor','r','FaceColor','none','Visible',visible);                                                
             end
             
               x=get(self.cacheLayer,'XData');
                y=get(self.cacheLayer,'YData');
                visible=get(self.cacheLayer,'Visible');
                delete(self.cacheLayer);
                self.cacheLayer=plot(self.ax,x,y,'r.','Visible',visible);                                               
             
             hold(self.ax,'off');
        end
     end       
    %%%   End Layers
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Images
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     methods
        function eliminarImagen(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);
            node.removeFromParent;
            parent=util.getNodeByKey(self.tree,'fld_gis_images');
            self.tree.reloadNode(parent);
            imageId=nodeId(11:end);
            delete(self.imageLayers(imageId));
            self.imageLayers.remove(imageId);
            file=fullfile(self.folders.cache,[imageId,'.img']);
            delete(file);
        end

        function eliminarTodasImagenes(self,varargin)
            node=util.getNodeByKey(self.tree,'fld_gis_images');
            util.cleanNode(node,self.tree);            
            imFiles=dir(fullfile(self.folders.cache,'*.img'));
            for i=1:length(imFiles)
                imFile=fullfile(self.folders.cache,imFiles(i).name);
                [~,imageId,~]=fileparts(imFile); 
                delete(self.imageLayers(imageId));
                self.imageLayers.remove(imageId); 
                delete(imFile);
            end
        end
        
        function grabarImagen(self,imageId,I,x,y)
            
            file=fullfile(self.folders.cache,[imageId,'.img']);
            img.I=I;
            img.x=x;
            img.y=y;
            save(file,'img','-v7.3');
        end
        
        function [imageId,I,x,y]=loadImagen(self,file)
            img=load(file,'-mat');
            img=img.img;
            [~,imageId,~]=fileparts(file);
            I=img.I;
            x=img.x;
            y=img.y;
        end
        
        function images=getImagesRegion(self,lonIni,lonEnd,latIni,latEnd)
            imgs=keys(self.imageLayers);
            images=[];
            for i=1:length(imgs)
                img=self.imageLayers(imgs{i});
                xMin=min(get(img,'XData'));
                xMax=max(get(img,'XData'));
                yMin=min(get(img,'YData'));
                yMax=max(get(img,'YData'));
                
                if xMin>=lonIni && xMax<=lonEnd && yMin>=latIni && yMax<=latEnd
                    im=load(fullfile(self.folders.cache,[imgs{i},'.img']),'-mat');
                    im=im.img;
                    images=cat(1,images,im);
                end
            end
        end
     end
    %%%   End Images
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function mousePressedCallback(self,jtree,eventData)
             % Get the clicked node
             clickX = eventData.getX;
             clickY = eventData.getY;
             treePath = jtree.getPathForLocation(clickX, clickY);
              if ~isempty(treePath)
                 % check if the checkbox was clicked
                 node = treePath.getLastPathComponent;
                 if clickX <= (jtree.getPathBounds(treePath).x+16)         
                    [~,isCheckNode,~]=util.isCheckNode(node);                                   
                    if isCheckNode                        
                        [~,nodeId] = util.checkNode(self.tree,node);
                        if  ~isempty(strfind(nodeId,'gis_dow_'))
                            malla=nodeId(9:end);
                            self.showHideDow(malla)
                        elseif strcmp(nodeId,'cartas')
                             self.showHideCartas;
                        elseif strcmp(nodeId,'cache')
                             self.showHideCache;
                        elseif strcmp(nodeId,'gos')
                             self.showHideGos;
                         elseif strcmp(nodeId,'got')
                             self.showHideGot;
                        elseif ~isempty(strfind(nodeId,'gis_image_'))
                            imageId=nodeId(11:end);
                            self.showHideImage(imageId)
                        end
                    end
                 end
                 %%%%Select Node Method
                 %self.nodeSelected(node);
                 jtree.setSelectionPath(treePath);
                 if eventData.isMetaDown
                     [~,isCheckNode,nodeId]=util.isCheckNode(node);
                     if isCheckNode
                         if ~isempty(strfind(nodeId,'gis_image_'))
                             self.cmImage.jmenu.show(jtree, clickX, clickY);
                         end
                     elseif strcmp(nodeId,'fld_gis_images')
                         self.cmImages.jmenu.show(jtree, clickX, clickY);
                     end
                 end
                 %%%%ContextMenu
                 
              end
        end
        
        function addAmevaNode(self,varargin)
            lat=cell2mat(varargin(end-2));
            lon=cell2mat(varargin(end-1));
            type=cell2mat(varargin(end));            
            core.addAmevaNode(self.tree,lat,lon,type);
            notify(self,'ChangeMenu');
        end
    end
    %%% End  Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end


function utmZones=getUtmZones(lonIni,latIni,lonEnd,latEnd)
     husoIni = fix( ( lonIni / 6 ) + 31);
     husoEnd = fix( ( lonEnd / 6 ) + 31);
     husos=husoIni:1:husoEnd;
     latZoneIni=ceil((latIni+72+1E-10)/8);     
     latZoneEnd=ceil((latEnd+72+1E-10)/8);
     latZones=latZoneIni:1:latZoneEnd;
     latZones(latZones<0)=0;
     latZones(latZones>19)=19;
     [H,L]=meshgrid(husos,latZones+1);
     Husos=reshape(H,size(H,1)*size(H,2),1);
     LatZones=reshape(L,size(H,1)*size(H,2),1);     
     chrLatZones=('CDEFGHJKLMNPQRSTUVWX')';     
     utmZones=[num2str(Husos,'%.2d'),chrLatZones(LatZones)];     
end


function option = selectPointForm(lat,lon,z)

 option='cancel';   
 %%% Cargo el idioma
 txt=util.loadLanguage;
 %%%%%%%%%%%%%%%%%%%

  scrPos=get(0,'ScreenSize');
  width=400;
  height=225;
  logoPos=[236,28,128,128];
  %xLeft=10;    
  xLeftTab=40;
  yPos=130:-35:25;
 
h=dialog('Visible','off','Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]);
%hpos=get(h,'Position');hpos(3)=300;hpos(4)=220;
%set(h,'Position',hpos);
pos=get(h,'Position');

uicontrol(h,'Style','pushbutton','String', 'Ameva','units','pixel','Position',[30 10 60 20],'Callback',{@close,'ameva'});
uicontrol(h,'Style','pushbutton','String', txt.gis('wPointMove'),'units','pixel','Position',[120 10 60 20],'Callback',{@close,'move'});
uicontrol(h,'Style','pushbutton','String',txt.gis('wPointCancel'),'units','pixel','Position',[210 10 60 20],'Callback',{@close,'cancel'}); 

pMain=uipanel(h,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
pLogo=uipanel(pMain,'units','pixel','position',logoPos);  
ax=axes('Parent',pLogo,'YTick',[],'XTick',[],'position',[0,0,1,1]);
imshow(fullfile('icon','dow_large.png'),'Parent',ax);
axis(ax,'image');
    
uicontrol(pMain,'Style','text','String',['Lat (�): ',num2str(lat,'%.3f')],'Position',[xLeftTab yPos(1) 85 15]);            
uicontrol(pMain,'Style','text','String',['Lon (�): ',num2str(lon,'%.3f')],'Position',[xLeftTab yPos(2) 85 15]);            
uicontrol(pMain,'Style','text','String',['Z (m)  : ',num2str(z,'%.2f')],'Position',[xLeftTab yPos(3) 85 15]);            

set(h,'Visible','on');
uiwait(h);

    function close(varargin)
        option=cell2mat(varargin(end));
        delete(h);
        pause(0.3);
    end
end


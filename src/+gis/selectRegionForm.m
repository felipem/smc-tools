function [option,checkImages]= selectRegionForm(X,Y,Z,lineas,images)
    option='cancel';
    checkImages=false;
    %%% Cargo el idioma
   txt=util.loadLanguage;

  scrPos=get(0,'ScreenSize');
  width=600;
  height=600;
  
    f = figure( 'MenuBar', 'none', 'Name', txt.gis('wRegionTitle'), 'NumberTitle', 'off', 'Toolbar', 'none','Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]);
    pos=get(f,'Position');
    set(f,'ResizeFcn',@resize);
    color=get(f,'color');
    
    %panelAxes=uipanel(f,'Position',[0 0.14  1 0.86]);
    panelAxes=uipanel(f,'units','pixel','Position',[0 40 pos(3)  pos(4)-40]);
    %panelCheck=uipanel(f,'Position',[0 0.10 1 0.04]);
    %panelButton=uipanel(f,'Position',[0 0 1 0.1]);

    ax_=axes('Parent',panelAxes);         
    chkImages=uicontrol(f,'Style','checkbox','String',txt.gis('wRegionImages'),'Value',1,'Position',[300 10 200 20],'Callback',@checkImagenes,...
        'BackgroundColor',color);

    uicontrol(f,'Style','pushbutton','String', txt.gis('wRegionRedefine'),'Position',[30 10 60 20],'Callback',{@close,'redefine'});
    uicontrol(f,'Style','pushbutton','String', txt.gis('wRegionSMC'),'Position',[120 10 60 20],'Callback',{@close,'createSmc'});
    uicontrol(f,'Style','pushbutton','String', txt.gis('wRegionCancel'),'Position',[210 10 60 20],'Callback',{@close,'cancel'});            

    set(f,'WindowStyle','Modal')
   
    
    %%%%%%%Pinto
     %levels=util.getDemLevels(Z);
     %contourf(ax_,X,Y,-Z,40,'LineStyle','none');colorbar     
     %contourf(ax_,X,Y,-Z,levels,'LineStyle','none');colorbar              
     %pcolor(ax_,X,Y+0.003,-Z);shading interp;colorbar     %Añadiendo
     %un desfase
     pcolor(ax_,X,Y,-Z);shading interp;colorbar     
     util.demcmap(-Z)
     %scatter(ax_,bat(:,1),bat(:,2),7,bat(:,3),'filled');
     hold(ax_,'on');
     for i=1:length(lineas)
         plot(lineas(i).x,lineas(i).y,'-r','LineWidth',3,'Parent',ax_);
     end
     axis(ax_,'equal');


     %%%%%%%Pinto las imagenes
     imagesLayer=[];

     for i=1:length(images)
        imagesLayer=cat(1,imagesLayer,image(images(i).x,images(i).y,images(i).I,'Parent',ax_));
     end

     hold(ax_,'off');
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %reset(h);
      uiwait(f);

       function checkImagenes(varargin)
            isCheck=get(chkImages,'Value');
            visible='off';
            if isCheck
                visible='on';
            end
            for j=1:length(imagesLayer)
                set(imagesLayer(j),'Visible',visible);
            end
       end
        
    function close(varargin)
        option=varargin{end};
        checkImages=get(chkImages,'Value');            
        delete(f);
        pause(0.2);
    end
          
    function resize(varargin)
        pos=get(f,'Position');
    
    set(panelAxes,'Position',[0 40 pos(3)  pos(4)-40]);
    end

end


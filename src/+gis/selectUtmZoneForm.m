function [lat,lon] = selectUtmZoneForm(utmZones,latOri,lonOri)    
lat=[];
lon=[];
 txt=util.loadLanguage;
 scrPos=get(0,'ScreenSize');
 width=250;
 height=100;    
 
 f = figure( 'MenuBar', 'none', 'Name', txt.gis('wSelectUtmZone'), 'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@cancel,'WindowStyle','modal');
 set(f, 'resize', 'off');  
 
 uicontrol(f,'Style','pushbutton','String', txt.gis('wSelectUtmZoneOk'),'Position',[30 10 60 20],'Callback',@ok);    
 uicontrol(f,'Style','pushbutton','String', txt.gis('wSelectUtmZoneCancel'),'Position',[120 10 60 20],'Callback',@cancel);    
 
 ddUtm=uicontrol(f,'Style','popup','String',utmZones,'Position',[30 60 100 20]);
 
 uiwait(f);
    function ok(varargin)
        utmZone=utmZones(get(ddUtm,'Value'),:);
        huso=str2double(utmZone(1:2));
        lonIni=(huso-31)*6;
        lonEnd=(huso-30)*6;
        chrLatZones=('CDEFGHJKLMNPQRSTUVWX');
        latZone=find(chrLatZones==utmZone(3))-1;
        if latZone==0
            latIni=-90;
            latEnd=-72;
        elseif latZone==length(chrLatZones)-1
            latIni=72;
            latEnd=90;
        else
           latIni=(latZone-1)*8-72;
           latEnd=(latZone)*8-72;
        end
        
        lat=latOri;
        lon=lonOri;
        
        lat(lat<latIni)=latIni;
        lat(lat>=latEnd)=latEnd-1E-10;
        lon(lon<lonIni)=lonIni;
        lon(lon>=lonEnd)=lonEnd-1E-10;
        
        delete(f);
        pause(0.2);
    end
    function cancel(varargin)
        delete(f);
        pause(0.2);
    end

end


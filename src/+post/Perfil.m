classdef Perfil <handle
    
    
    properties
        posicion=[];
        nombre=[];
        d50;
        rho_s;
        roi;
        n;
        x;
        y;
        h;
        dx; 
        slope;
        defaultSlope;
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%   Constructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function self= Perfil(pos,mallas,roi,alternativa,perfiles,level)            
            %self.F=TriScatteredInterp(xyz(:,1),xyz(:,2),xyz(:,3));
            self.posicion=pos;
            self.roi=roi;
            %%%Ordeno punto 1) tierra y  2) mar
            z1=alternativa.F(pos(1,1),pos(1,2));
            z2=alternativa.F(pos(2,1),pos(2,2));
            if z1>z2 %Está al revés
                self.posicion(1,:)=pos(2,:);
                self.posicion(2,:)=pos(1,:);
            end
            
            %%%Discretizo (mallas es una diccionario)
            mallasStr=keys(mallas);
            dx_=9999;
            for i=1:length(mallasStr)
                malla=mallas(mallasStr{i});
                if malla.dx<dx_
                    dx_=malla.dx;
                end                
            end
            dx_=dx_/2;%1 Pongo metros como Jara %dx_/2; %%TODO con el ángulo
            self.dx=dx_;
            long=sqrt(sum((pos(2,:)-pos(1,:)).^2));
            nPerfil=round(long/self.dx);
            x_=linspace(self.posicion(1,1),self.posicion(2,1),nPerfil);
            y_=linspace(self.posicion(1,2),self.posicion(2,2),nPerfil);    
            in=inpolygon(x_,y_,roi(:,1),roi(:,2));
            self.x=x_(in);
            self.y=y_(in);            
            %%%%%%%%%%%%%%%%%%%%%%%%%%
            if isempty(self.x) %%No hay puntos dentro de las mallas
                return;
            end
            self.h=alternativa.F(self.x,self.y);
            
            %%%Calcula la pendiente
            maxLevel=max(level);
            minLevel=min(level);
            r=sqrt((self.x-self.x(1)).^2+(self.y-self.y(1)).^2);            
            riMax=util.getIntersection(r,-self.h,maxLevel);
            riMin=util.getIntersection(r,-self.h,minLevel);
            try
                tanB=(maxLevel-minLevel)/(riMin(end)-riMax(1));
            catch
                tanB=0;
            end
            
            %[nom,d50_,rho_s_,n_]=post.newProfileDlg(perfiles);
            self.defaultSlope=tanB;
            data=post.newProfileDlg(perfiles,tanB);            
            pause(0.3);
            if isempty(data)
                return;
            end
            self.nombre=data.name;
            self.d50=data.d50;
            self.rho_s=data.rho;
            self.n=data.n;
            self.slope=data.slope;
        end       
    end    
    %%%%% End Constructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%   Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function setPosicion(self,pos,alternativa)
            long=sqrt(sum((pos(2,:)-pos(1,:)).^2));
            nPerfil=round(long/self.dx);
            %%%Ordeno punto 1) tierra y  2) mar
            z1=alternativa.F(pos(1,1),pos(1,2));
            z2=alternativa.F(pos(2,1),pos(2,2));
            pos_=pos;
            if z1>z2 %Está al revés
                pos_(1,:)=pos(2,:);
                pos_(2,:)=pos(1,:);
            end
            pos=pos_;
            
            
            x_=linspace(pos(1,1),pos(2,1),nPerfil);
            y_=linspace(pos(1,2),pos(2,2),nPerfil);    
            in=inpolygon(x_,y_,self.roi(:,1),self.roi(:,2));
            
            if isempty(x_(in)) %%No hay puntos dentro de las mallas
                return;
            end            
            
            self.x=x_(in);
            self.y=y_(in);            
            self.posicion=pos;
            
            self.h=alternativa.F(self.x,self.y);            
        end
        
        function editProps(self)
            data.name=self.nombre;
            data.d50=self.d50;
            data.rho=self.rho_s;
            data.n=self.n;
            data.slope=self.slope;
            
            data=post.newProfileDlg(data,self.defaultSlope,data);            
            if ~isempty(data)
                self.d50=data.d50;
                self.rho_s=data.rho;
                self.n=data.n;
                self.slope=data.slope;
            end
        end 
        
        function length=getLength(self)
            length=sqrt((self.posicion(2,1)-self.posicion(1,1)).^2+(self.posicion(2,2)-self.posicion(1,2)).^2);
        end
        
        function [x_1,y_1,x_2,y_2]=getNormalVector(self,mod,relPosition)
            
            if ~exist('relPosition','var')
                relPosition=0.5;
            end
            alpha=self.getAngle;
            l=self.getLength*relPosition;
            x_1=self.posicion(1,1)+cosd(alpha)*l;
            y_1=self.posicion(1,2)+sind(alpha)*l;
            
            ang=atan2(self.posicion(2,2)-self.posicion(1,2),self.posicion(2,1)-self.posicion(1,1));
            ang=ang+sign(mod)*pi/2;
            x_2=x_1+cos(ang)*abs(mod);
            y_2=y_1+sin(ang)*abs(mod);
        end
        
        function [x_m,y_m]=getMiddlePoint(self)
            x_m=self.posicion(1,1)+(self.posicion(2,1)-self.posicion(1,1))/2;
            y_m=self.posicion(1,2)+(self.posicion(2,2)-self.posicion(1,2))/2;
        end
        
        
        function angle=getAngle(self)
            angle=atan2(self.posicion(2,2)-self.posicion(1,2),self.posicion(2,1)-self.posicion(1,1))*180/pi;
        end
        
    end    
    %%%%%  End Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end


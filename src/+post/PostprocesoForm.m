classdef PostprocesoForm <handle   
    
    properties         
        toolbar;
        alternativa;
        mopla;
        project;        
        ax;        
        rootNode;
        panel;
        tree;              
        toolsCount=0;
        currentPoi=[];        
        pointsPorMalla=[]
        mallasLayer=[];
        roiPoly;
        roiLayer=[];  %Region Of Interest
        poiLayers=[];
        perfilLayers=[];
        cmPerfil=[];
        cmTransporte=[];
        costaLayer=[];
        dowLayer=[];                
        cmPoi=[];  
        cmPois=[];
        roturaLayer=[]; %Diccionario
        transporteLayer=[];
        %transporteValues={};
        flujoEnergiaLayer=[];
        flujoEnergiaPoisLayer=[]; %Diccionario
        %flujoEnergiaValues={};        
        mapLegend=[];
        %transporteMapOpciones=[];
        cmMapaTransporte=[];
        maxElements=20; %Numero m�ximo de perfiles o Pois
        profileColor=[255,160,122]/255; %Ligth Salmon	
        femColor=[178,34,34]/255; %Firebrick
        transportColor=[184,134,11]/255; %Dark Goldenrod
        txt;
    end
    
    events
        ChangeMenu;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Constructor/Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
         function self=PostprocesoForm(panel,project,moplaId,altName,toolbar,tree,rootNode)   
            try
            %%% Cargo el idioma
            self.txt=util.loadLanguage;
            %%%%%%%%%%%%%%%%%%%
            
            self.panel=panel;
            self.toolbar=toolbar;
            self.tree=tree;
            self.rootNode=rootNode; 
            %self.infoPanel=infoPanel;
            self.project=project;
            
            self.alternativa=smc.loadAlternativa(project.folder,altName);
            self.mopla=smc.loadMopla(fullfile(project.folder,altName),moplaId);
            
            
            %%%&Convex hull de las mallas
            mallasTmp=containers.Map;
            mallasTmp=util.initMap(mallasTmp);
            mallas=[];
             for i=1:length(self.mopla.mallas)
                 if self.mopla.propagate(i)
                 if ~isKey(mallasTmp,self.mopla.mallas{i})
                     mallasTmp(self.mopla.mallas{i})='';
                     malla=self.alternativa.mallas(self.mopla.mallas{i});
                     mallas=cat(1,mallas,malla);
                 end
                 end
             end
            delete(mallasTmp);
            [self.roiPoly,ppm] = post.getConvexMallas(mallas);
            self.pointsPorMalla=ppm;   
            
            
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Init Axes
            self.ax=axes('Parent',self.panel,'Position',[0.05 0.05 0.9 0.9]);  
            xIni=min(self.roiPoly(:,1)); xEnd=max(self.roiPoly(:,1));
            yIni=min(self.roiPoly(:,2)); yEnd=max(self.roiPoly(:,2));             
             dx=xEnd-xIni;dy=yEnd-yIni;
             xIni=xIni-dx;xEnd=xEnd+dx;
             yIni=yIni-dy;yEnd=yEnd+dy;
             if xIni<min(self.alternativa.xyz(:,1)), xIni=min(self.alternativa.xyz(:,1));end
             if yIni<min(self.alternativa.xyz(:,2)), yIni=min(self.alternativa.xyz(:,2));end
             if xEnd>max(self.alternativa.xyz(:,1)), xEnd=max(self.alternativa.xyz(:,1));end
             if yEnd>max(self.alternativa.xyz(:,2)), yEnd=max(self.alternativa.xyz(:,2));end
            x=linspace(xIni,xEnd,100);
            y=linspace(yIni,yEnd,100);             
            [X,Y]=meshgrid(x,y);        
%             x_=self.alternativa.xyz(:,1);
%             y_=self.alternativa.xyz(:,2);
%             %z=self.alternativa.xyz(:,3);
%             x=linspace(min(x_),max(x_),100);
%             y=linspace(min(y_),max(y_),100);
%             [X,Y]=meshgrid(x,y);
            %F = TriScatteredInterp(x_,y_,z);
            Z=self.alternativa.F(X,Y);         
            %contourf(self.ax,X,Y,-Z,50,'LineStyle','none');colorbar                
            levels=util.getDemLevels(Z);
            contourf(self.ax,X,Y,-Z,-levels,'LineStyle','none');colorbar;
            util.demcmap(-Z)
            axis equal
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            
            %%%%%%%% Inicializo los l�mites para los mapas
%             dates=datevec(self.mopla.time);
%             self.transporteMapOpciones.ini=dates(1,1);
%             self.transporteMapOpciones.end=dates(end,1);
%             self.transporteMapOpciones.type='MediaAnualTotal';
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            if ~isfield(self.mopla,'probabilidad');
                self.mopla=smc.calculaProbabilidadMD(self.mopla);
                smc.saveMopla(fullfile(self.project.folder,self.alternativa.nombre),self.mopla,{'probabilidad'});
            end
            self.initLayers();
            self.initExplorer();
            self.initContexMenu();
            self.initToolBar();
            
            catch ex               
                delete(self);
                error(ex.message);
            end
            
         end
         
         function delete(self)
%             pos=get(self.panel,'Position');pos(3)=pos(3)-self.oldInfoWidth;
%             set(self.panel,'Position',pos);
%             set(self.infoPanel,'Visible','on');
            
            %Elimino los hijos por si hay algo
            util.cleanNode(self.rootNode,self.tree);
    
            
            jToolbar = get(get(self.toolbar,'JavaContainer'),'ComponentPeer');
            totalTools=jToolbar.getComponentCount();
            for i=totalTools-1:-1:totalTools-self.toolsCount;
                 jToolbar.remove(i);
            end
            jToolbar(1).repaint;
            jToolbar(1).revalidate;
            util.resetPanel(self.panel);
           
            for i=1:length(self.mallasLayer)
               delete(self.mallasLayer(i));
            end
             
            perfiles=keys(self.perfilLayers);
            for i=1:length(self.perfilLayers)                
               delete(self.perfilLayers(perfiles{i}));
               delete(perfil);
            end
            
            delete(self.mallasLayer);
            delete(self.roiLayer);
            delete(self.dowLayer);            
            delete(self.perfilLayers);
            self.borraMapaRotura();
            self.borraMapaTransporte();
            self.borraMapaFlujoEnergia();
            self.borraMapaFlujoEnergiaPois();
            delete(self.mapLegend);
            self.mapLegend=[];
            
            perfiles=keys(self.mopla.perfiles);
            for i=1:length(perfiles)
               delete(self.mopla.perfiles(perfiles{i}));
            end
            self.mopla.perfiles=[];
            delete(self.txt);
            
         end
         
         function initToolBar(self)
            self.toolsCount=0;
            %ptSaveAlt=uipushtool(self.toolbar,'ClickedCallback',@self.saveAlternativa);                 
            %util.setControlIcon(ptSaveAlt,'file_save.gif');
            %self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar);
            
            ptSaveMopla=uipushtool(self.toolbar,'ClickedCallback',@self.saveMopla,'TooltipString',self.txt.post('tbSave'));                 
            util.setControlIcon(ptSaveMopla,'file_save.gif');
            self.toolsCount=self.toolsCount+1;
%             ptConfigCases=uipushtool(self.toolbar,'ClickedCallback',@self.configCases,'TooltipString',self.txt.post('tbConfig'));                 
%             util.setControlIcon(ptConfigCases,'gear.gif');
%             self.toolsCount=self.toolsCount+1;
            self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar);
            
            self.toolsCount=self.toolsCount+core.navigateToolbar(self.toolbar,self.panel);
            self.toolsCount=self.toolsCount+core.measureToolbar(self.toolbar,gca,'utm');            
            self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar);            
            
            ptSelectPoint=uipushtool(self.toolbar,'ClickedCallback',@self.selectPoint,'TooltipString',self.txt.post('tbSelectPoint'));            
            util.setControlIcon( ptSelectPoint,'add_point.gif');    
            self.toolsCount=self.toolsCount+1;            
            
            ptFemPois=uipushtool(self.toolbar,'ClickedCallback',@self.calcularFemPois,'TooltipString',self.txt.post('tbFemPois'));            
            util.setControlIcon(ptFemPois,'energia.gif');
            self.toolsCount=self.toolsCount+1;
            
             ptLimpiarPois=uipushtool(self.toolbar,'ClickedCallback',@self.limpiarPois,'TooltipString',self.txt.post('tbCleanPois'));            
            util.setControlIcon(ptLimpiarPois,'clean_pois.gif');    
            self.toolsCount=self.toolsCount+1;
            
             self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar); 
            
            ptAddProfile=uipushtool(self.toolbar,'ClickedCallback',@self.createPerfil,'TooltipString',self.txt.post('tbProfile'));            
            util.setControlIcon( ptAddProfile,'open_polygon.gif');    
            self.toolsCount=self.toolsCount+1;
            
            ptRotura=uipushtool(self.toolbar,'ClickedCallback',@self.calcularRotura,'TooltipString',self.txt.post('tbBreaking'));            
            util.setControlIcon(ptRotura,'wave.gif');    
            self.toolsCount=self.toolsCount+1;
            
            ptTransporte=uipushtool(self.toolbar,'ClickedCallback',@self.calcularTransporte,'TooltipString',self.txt.post('tbTransport'));            
            util.setControlIcon( ptTransporte,'transporte.gif');    
            self.toolsCount=self.toolsCount+1;            
            
            ptLimpiar=uipushtool(self.toolbar,'ClickedCallback',@self.limpiarTransporte,'TooltipString',self.txt.post('tbClean'));            
            util.setControlIcon(ptLimpiar,'delete.gif');    
            self.toolsCount=self.toolsCount+1;
            
             self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar); 
            
             
            ptReports=uipushtool(self.toolbar,'ClickedCallback',@self.generateReports,'TooltipString',self.txt.post('tbReportGenerator'));            
            util.setControlIcon(ptReports,'report.gif');
            self.toolsCount=self.toolsCount+1;
            
            ptExport=uipushtool(self.toolbar,'ClickedCallback',@self.exportResults,'TooltipString',self.txt.post('tbExportResults'));            
            util.setControlIcon(ptExport,'export_results.gif');
            self.toolsCount=self.toolsCount+1;
            
%             ptShowResults=uipushtool(self.toolbar,'ClickedCallback',@self.showResults,'TooltipString',self.txt.post('tbShowResults'));                 
%             util.setControlIcon(ptShowResults,'layers.gif');
%             self.toolsCount=self.toolsCount+1;
            
        end
         
         function initLayers(self)
            hold(self.ax,'on');
            %costas=self.alternativa.costas; 
            xIni=min(self.roiPoly(:,1)); xEnd=max(self.roiPoly(:,1));
            yIni=min(self.roiPoly(:,2)); yEnd=max(self.roiPoly(:,2));             
             dx=xEnd-xIni; dy=yEnd-yIni;
             xIni=xIni-dx; xEnd=xEnd+dx;
             yIni=yIni-dy; yEnd=yEnd+dy;
            costas=util.costasInPoly(self.alternativa.costas,[xIni,xEnd,xEnd,xIni],[yIni,yEnd,yIni,yEnd]);
            for i=1:length(costas)
                self.costaLayer(i)=plot(self.ax,costas(i).x,costas(i).y,'-k','LineWidth',2);
            end
            
            mallas=self.mopla.mallas;
            for i=1:length(mallas)
                if self.mopla.propagate(i)
                    malla=self.alternativa.mallas(mallas{i});
                    xIni=malla.xIni;yIni=malla.yIni;                
                    x_=[0,0,malla.x,malla.x];y_=[0,malla.y,malla.y,0];                
                    x=xIni+cosd(malla.angle).*x_-sind(malla.angle).*y_;
                    y=yIni+sind(malla.angle).*x_+cosd(malla.angle).*y_;                
                    polX=x;
                    polY=y;
                    m=fill(polX,polY,'k','EdgeColor','k','LineWidth',2.5,'FaceColor','none','Visible','off','Parent',self.ax);
                    self.mallasLayer=cat(1,self.mallasLayer,m);
                end
            end
            
            self.perfilLayers=util.initMap(self.perfilLayers);             
            perfiles=keys(self.mopla.perfiles);
            for i=1:length(perfiles)
                perfil=self.mopla.perfiles(perfiles{i});                
                if ~isempty(perfil)
                    self.perfilLayers(perfiles{i})=plot(perfil.posicion(:,1),perfil.posicion(:,2),'Color',self.profileColor,'lineWidth',2,'Visible','off');                               
                end
            end

            pMenu=uicontextmenu;
            
            uimenu(pMenu,'Label',sprintf('Lat: %.2f � ; %.0f m',self.mopla.lat,self.mopla.y));
            uimenu(pMenu,'Label',sprintf('Lon: %.2f � ; %.0f m',self.mopla.lon,self.mopla.x));
            uimenu(pMenu,'Label',sprintf('z (m): %.2f',self.mopla.z));        
            self.dowLayer=plot(self.ax,self.mopla.x,self.mopla.y,'oy','MarkerFaceColor','y',...
                'MarkerSize',10,'Visible','off','UIContextMenu',pMenu);
            
                    
            %self.roiLayer=fill(poly(:,1),poly(:,2),'r','FaceAlpha',0.5,'Visible','off');            
            self.roiLayer=fill(self.roiPoly(:,1),self.roiPoly(:,2),'g','EdgeColor','g','FaceColor','none','LineWidth',3,'Visible','off');            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            self.poiLayers=util.initMap(self.poiLayers);
            pois=keys(self.mopla.pois);
            for i=1:length(pois)                                
                poi=self.mopla.pois(pois{i});
                pMenu=uicontextmenu;
                uimenu(pMenu,'Label',poi.nombre);                        
                uimenu(pMenu,'Label',sprintf('x (m): %.0f',poi.x));
                uimenu(pMenu,'Label',sprintf('y (m): %.0f',poi.y));
                uimenu(pMenu,'Label',sprintf('z (m): %.2f',poi.z));                        
                p=plot(self.ax,poi.x,poi.y,'or','MarkerFaceColor',[255;190;50]/255,'MarkerSize',10,...
                    'UIContextMenu',pMenu,'LineWidth',1.5,'Visible','off');           
                self.poiLayers(pois{i})=p;
            end
            
            self.flujoEnergiaPoisLayer=util.initMap(self.flujoEnergiaPoisLayer);
            
            %%%% Creo los mapas de rotura
            self.roturaLayer=util.initMap(self.roturaLayer);
            
            %%%% Creo los mapas transporte
            self.transporteLayer=util.initMap(self.transporteLayer);
            self.flujoEnergiaLayer=util.initMap(self.flujoEnergiaLayer);
            
            
%             if self.transporteCalculado
%                self.crearMapaTransporte;
%                self.crearMapaFlujoEnergia;
%                self.updateLegend;
%             end
%             
            hold(self.ax,'off');            
         end
         
         function initExplorer(self,varargin)
            util.cleanNode(self.rootNode,self.tree);             
            mallasNode=uitreenode('v0','chk_mallas_0',self.txt.post('exMeshes'),[],true);
            util.setNodeIcon(mallasNode,'malla.gif');
            self.rootNode.add(mallasNode);            
            
            costaNode=uitreenode('v0','chk_costa_1',self.txt.post('exCoast'),[],true);
            util.setNodeIcon(costaNode,'open_polygon.gif');
            self.rootNode.add(costaNode);

            oleajeNode=uitreenode('v0','chk_dow_0',self.txt.post('exSourceWave'),[],true);
            util.setNodeIcon(oleajeNode,'dow.gif');
            self.rootNode.add(oleajeNode);
            
            roiNode=uitreenode('v0','chk_roi_0','ROI',[],true);
            util.setNodeIcon(roiNode,'PolygonSetIcon.gif');
            self.rootNode.add(roiNode);            
            
            poisNode=uitreenode('v0','fld_poi','POI',[],false);
            util.setNodeIcon(poisNode,'folder_closed.png');
            self.rootNode.add(poisNode);
            pois=keys(self.mopla.pois);
            for i=1:length(pois)
                poi=pois{i};
                poiNode=uitreenode('v0',['chk_poi_',poi,'_0'],...
                poi,[],true);
                util.setNodeIcon(poiNode,'add_point.gif');                                
                poisNode.add(poiNode);
            end
            
            transporteNode=uitreenode('v0','fld_transporte',self.txt.post('exTransport'),[],false);
            util.setNodeIcon(transporteNode,'folder_closed.png');
            self.rootNode.add(transporteNode);
            self.refreshTransporteExplorer;
            
            mapasNode=uitreenode('v0','fld_mapas',self.txt.post('exMaps'),[],false);
            util.setNodeIcon(mapasNode,'folder_closed.png');
            self.rootNode.add(mapasNode);
            self.refreshMapaExplorer;
            
            
            self.tree.reloadNode(self.rootNode);
            util.refreshTree(self.tree);
            
         end     
         
         function initContexMenu(self)
             
             %POIS
             self.cmPois.jmenu=javax.swing.JPopupMenu;  
             
             self.cmPois.rebuild=javax.swing.JMenuItem(self.txt.post('cmPoisRebuild'));               
             set(self.cmPois.rebuild,'ActionPerformedCallback',@self.rebuildPois);  
             self.cmPois.jmenu.add(self.cmPois.rebuild); 
             
             self.cmPois.clean=javax.swing.JMenuItem(self.txt.post('tbCleanPois'));               
             set(self.cmPois.clean,'ActionPerformedCallback',@self.limpiarPois);  
             self.cmPois.jmenu.add(self.cmPois.clean); 
            
            
             
            %POI
            self.cmPoi.jmenu=javax.swing.JPopupMenu;  
            
            self.cmPoi.propiedades=javax.swing.JMenuItem(self.txt.post('cmProperties'));               
            set(self.cmPoi.propiedades,'ActionPerformedCallback',@self.poiProperties);  
            self.cmPoi.jmenu.add(self.cmPoi.propiedades);                        
            
            self.cmPoi.results=javax.swing.JMenuItem(self.txt.post('cmPoiResults'));               
            set(self.cmPoi.results,'ActionPerformedCallback',@self.poiResults);  
            self.cmPoi.jmenu.add(self.cmPoi.results);            
            
            self.cmPoi.jmenu.addSeparator;

            self.cmPoi.eliminar=javax.swing.JMenuItem(self.txt.post('cmDelete'));               
            set(self.cmPoi.eliminar,'ActionPerformedCallback',@self.poiDelete);        
            self.cmPoi.jmenu.add(self.cmPoi.eliminar);      
            
            
              %Perfiles
             self.cmPerfil.jmenu=javax.swing.JPopupMenu;             
             
             self.cmPerfil.visualizar=javax.swing.JMenuItem(self.txt.post('cmProfileView'));               
             set(self.cmPerfil.visualizar,'ActionPerformedCallback',@self.visualizarPerfil);        
             self.cmPerfil.jmenu.add(self.cmPerfil.visualizar);            
             
             self.cmPerfil.jmenu.addSeparator;
             
             self.cmPerfil.editar=javax.swing.JMenuItem(self.txt.post('cmProfileEdit'));               
             set(self.cmPerfil.editar,'ActionPerformedCallback',@self.editarPerfil);        
             self.cmPerfil.jmenu.add(self.cmPerfil.editar);                      
             
             self.cmPerfil.editarXY=javax.swing.JMenuItem(self.txt.post('cmProfileEditXY'));               
             set(self.cmPerfil.editarXY,'ActionPerformedCallback',@self.editarPerfilXY);        
             self.cmPerfil.jmenu.add(self.cmPerfil.editarXY);                      
             
             self.cmPerfil.propiedades=javax.swing.JMenuItem(self.txt.post('cmProperties'));               
             set(self.cmPerfil.propiedades,'ActionPerformedCallback',@self.propiedadesPerfil);        
             self.cmPerfil.jmenu.add(self.cmPerfil.propiedades);                      
             
             self.cmPerfil.eliminar=javax.swing.JMenuItem(self.txt.post('cmDelete'));               
             set(self.cmPerfil.eliminar,'ActionPerformedCallback',@self.eliminarPerfil);        
             self.cmPerfil.jmenu.add(self.cmPerfil.eliminar);    
              
             self.cmPerfil.jmenu.addSeparator;
             
             self.cmPerfil.evolucion=javax.swing.JMenuItem(self.txt.post('cmProfileEvolution'));
             self.cmPerfil.histograma=javax.swing.JMenuItem(self.txt.post('cmProfileHistogram'));
             set(self.cmPerfil.evolucion,'ActionPerformedCallback',@self.evolucionRotura);        
             set(self.cmPerfil.histograma,'ActionPerformedCallback',@self.histogramaRotura);        
             self.cmPerfil.jmenu.add(self.cmPerfil.evolucion);    
             self.cmPerfil.jmenu.add(self.cmPerfil.histograma);    
             set(self.cmPerfil.evolucion,'Enable','off');
             set(self.cmPerfil.histograma,'Enable','off');
             
             %Transporte
             self.cmTransporte.jmenu=javax.swing.JPopupMenu;             
             
             self.cmTransporte.calcular=javax.swing.JMenuItem(self.txt.post('tbBreaking'));               
             set(self.cmTransporte.calcular,'ActionPerformedCallback',@self.calcularRotura);        
             self.cmTransporte.jmenu.add(self.cmTransporte.calcular);                         
             
             self.cmTransporte.calcularTTE=javax.swing.JMenuItem(self.txt.post('tbTransport'));               
             set(self.cmTransporte.calcularTTE,'ActionPerformedCallback',@self.calcularTransporte);        
             self.cmTransporte.jmenu.add(self.cmTransporte.calcularTTE);      
             %set(self.cmTransporte.calcularTTE,'Enable','off');
             self.cmTransporte.jmenu.addSeparator;
             
              self.cmTransporte.rebuild=javax.swing.JMenuItem(self.txt.post('cmProfilesRebuild'));               
              set(self.cmTransporte.rebuild,'ActionPerformedCallback',@self.rebuildProfiles);        
              self.cmTransporte.jmenu.add(self.cmTransporte.rebuild);      
%              
              self.cmTransporte.jmenu.addSeparator;
             
             self.cmTransporte.limpiar=javax.swing.JMenuItem(self.txt.post('cmTransportClean'));               
             set(self.cmTransporte.limpiar,'ActionPerformedCallback',@self.limpiarTransporte);        
             self.cmTransporte.jmenu.add(self.cmTransporte.limpiar);    
             
             
             %%%%Mapas
%              self.cmMapaTransporte.jmenu=javax.swing.JPopupMenu;             
%              
%              self.cmMapaTransporte.propiedades=javax.swing.JMenuItem(self.txt.post('cmProperties'));               
%              set(self.cmMapaTransporte.propiedades,'ActionPerformedCallback',@self.propiedadesMapaTransporte);        
%              self.cmMapaTransporte.jmenu.add(self.cmMapaTransporte.propiedades); 
             

         end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% End Constructor/Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Toolbar Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function selectPoint(self,varargin)
            if length(self.mopla.pois)>=self.maxElements
                msgbox(sprintf(self.txt.post('iMaxElements'),num2str(self.maxElements)));
                return;
            end
             
             core.blockMainForm;
            %%%Me aseguro que se muestra la ROI
            roiNode=util.getNodeByKey(self.tree,'chk_roi_0');
            [isCheck,~,~]=util.isCheckNode(roiNode);
            if ~isCheck
                self.showHideRoi;
                util.checkNode(self.tree,roiNode);
            end
           %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
               [x,y]=ginput(1);                
               xyz=self.alternativa.xyz;
               
               F=TriScatteredInterp(xyz(:,1),xyz(:,2),xyz(:,3));
               z=F(x,y);
               core.blockMainForm;

               hold(self.ax,'on');
               if ishandle(self.currentPoi),delete(self.currentPoi);end;
               point=plot(self.ax,x,y,'ro','MarkerSize',12,'MarkerFaceColor','r');
               hold(self.ax,'off');
               
               [option,name] = post.poiForm(self.mopla.pois,x,y,z);
               
               switch option
                   case 'select'
                       poi.x=x;
                        poi.nombre=name;
                        poi.y=y;
                        poi.z=z;                    
                        self.mopla.pois(name)=poi;                    
                        
                        %%A�do a la capa
                        if ishandle(point),delete(point);end;    
                        hold(self.ax,'on');
                        pMenu=uicontextmenu;
                        uimenu(pMenu,'Label',poi.nombre);                        
                        uimenu(pMenu,'Label',sprintf('x (m): %.0f',poi.x));
                        uimenu(pMenu,'Label',sprintf('y (m): %.0f',poi.y));
                        uimenu(pMenu,'Label',sprintf('z (m): %.2f',poi.z));                        
                        p=plot(self.ax,x,y,'or','MarkerFaceColor',[255;190;50]/255,'MarkerSize',10,...
                            'UIContextMenu',pMenu,'LineWidth',1.5);            
                        self.poiLayers(name)=p;                        
                        hold(self.ax,'off');   
                        
                        %%A�ado al explorador
                        parent=util.getNodeByKey(self.tree,'fld_poi');
                        poiNode=uitreenode('v0',['chk_poi_',name,'_1'],...
                        name,[],true);
                        util.setNodeIcon(poiNode,'add_point.gif');                                
                        parent.add(poiNode);
                        self.tree.reloadNode(parent);                                             
                       
                   case 'move'
                       if ishandle(self.currentPoi),delete(self.currentPoi);end;
                        self.currentPoi=point;
                        self.selectPoint;   
                   case 'cancel'   
                        if ishandle(point),delete(point);end;                                  
                        if ishandle(self.currentPoi),delete(self.currentPoi);end;
               end
        end
        
        function saveMopla(self,varargin)
            core.blockMainForm;
            try
            %%%Limpio los puntos de cache que no sean POI
            %pois=keys(self.mopla.pois);
            poisFolder=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId);             
            poiFiles=dir(fullfile(poisFolder,'*.poi'));
            for i=1:length(poiFiles)
                poi=poiFiles(i).name;
                poi=poi(1:end-4);
                if ~isKey(self.mopla.pois,poi)
                    delete(fullfile(poisFolder,poiFiles(i).name));
                end
            end
            %%%%Creo la carpeta si no existe
            if ~exist(poisFolder,'dir')
                mkdir(poisFolder);
            end
            smc.saveMopla(fullfile(self.project.folder,self.alternativa.nombre),self.mopla,{'pois','perfiles','transportModel','kModel','transportPeriod'});
            core.refreshAmevaExplorer(self.tree,self.project,self);
            notify(self,'ChangeMenu');
            
            %%%%%Si estaba aplicando cambios en algún perfil los revierto
            perfiles=keys(self.mopla.perfiles);
            for i=1:length(perfiles)                
                perfilId=perfiles{i};                
                isEdit=strcmp(get(self.perfilLayers(perfilId),'Tag'),'imline');
                if isEdit
                    perfilLine=self.perfilLayers(perfilId);                
                    delete(perfilLine);
                    perfil=self.mopla.perfiles(perfilId);                
                    hold(self.ax,'on');
                    self.perfilLayers(perfilId)=plot(perfil.posicion(:,1),perfil.posicion(:,2),'Color',self.profileColor,'lineWidth',2);                               
                    hold(self.ax,'off');
                end
            end
            catch ex
                msgbox(ex.message,'error','error');
            end
            core.blockMainForm;
        end
        
        function createPerfil(self,varargin)  
            if length(self.mopla.pois)>=self.maxElements
                msgbox(sprintf(self.txt.post('iMaxElements'),num2str(self.maxElements)));
                return;
            end
            util.disableOtherButtons(self.toolbar,0)
            core.blockMainForm;
            try
            %%%Me aseguro que se muestra la ROI
            roiNode=util.getNodeByKey(self.tree,'chk_roi_0');
            [isCheck,~,~]=util.isCheckNode(roiNode);
            if ~isCheck
                self.showHideRoi;
                util.checkNode(self.tree,roiNode);
            end
            line=imline(self.ax); 
            if isempty(line)
                core.blockMainForm;
                return;
            end
            roi_x=get(self.roiLayer,'XData');
            roi_y=get(self.roiLayer,'YData');
            pos=line.getPosition;            
            level=data.getLevelTemporalSerie(util.loadDb(),self.mopla.lat,self.mopla.lon,self.mopla.time);            
           %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
            catch ex 
                msgbox(ex.message,'error','error');
            end
            core.blockMainForm;
            
            p=post.Perfil(pos,self.alternativa.mallas,[roi_x,roi_y],self.alternativa,self.mopla.perfiles,level);            
            delete(line);
            if isempty(p.nombre)
                return;
            end
             hold(self.ax,'on');
            self.perfilLayers(p.nombre)=plot(p.posicion(:,1),p.posicion(:,2),'Color',self.profileColor,'lineWidth',2);                                           
            hold(self.ax,'off');
            self.mopla.perfiles(p.nombre)=p;
            self.refreshTransporteExplorer;  
           
        end
        
        function calcularTransporte(self,varargin)
            [hasChanges,self.mopla]=post.transportForm(self.mopla,self.project,self.alternativa,self.transporteLayer,...
                self.flujoEnergiaLayer);
            pause(0.2);
            if hasChanges
                self.saveMopla();
            end            
            self.crearMapaFlujoEnergia(); 
            self.crearMapaTransporte(); 
            self.refreshMapaExplorer();
        end
        
        function calcularRotura(self,varargin)
            self.saveMopla; 
            util.disableOtherButtons(self.toolbar,0);            
            perfiles=post.breakingForm(self.mopla,self.project,self.alternativa);
            if ~isempty(perfiles)
                    self.borraMapaRotura();
                    self.crearMapaRotura(perfiles);
                    self.refreshMapaExplorer;
            end
        end        
        
        function calcularFemPois(self,varargin)            
            %[efAll,efDirAll,hasChanges]=post.poisFemForm(self.mopla,self.project,self.alternativa,true);
            hasChanges=post.poisFemForm(self.mopla,self.project,self.alternativa,true,self.flujoEnergiaPoisLayer);
            pause(0.2);
            if hasChanges
                self.saveMopla();
            end
            %self.flujoEnergiaPoisValues=[efAll,efDirAll];
            self.crearMapaFlujoEnergiaPois();    
            self.refreshMapaExplorer();
        end     
                
        function generateReports(self,varargin)
            f=findobj('Tag',['smcWinReport_',self.alternativa.nombre,self.mopla.moplaId]);
            if isempty(f)
                core.blockMainForm
                try
                report.reportForm(self.project,self.alternativa.nombre,self.mopla.moplaId)
                catch ex
                    msgbox(ex.message,'error','error');
                end
                core.blockMainForm
            else                
                figure(f);
            end
            
        end
        
        function configCases(self,varargin)
           msgbox('No implementado', '','warn');
        end
        
        function showResults(self,varargin)
            msgbox('No implementado', '','warn');
        end
        
        function exportResults(self, varargin)
            post.exportResultsForm(self.project,self.alternativa.nombre,self.mopla.moplaId);
        end

    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% End Toolbar Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods        
        function mousePressedCallback(self,jtree,eventData)           
             % Get the clicked node
             clickX = eventData.getX;
             clickY = eventData.getY;
             treePath = jtree.getPathForLocation(clickX, clickY);
              if ~isempty(treePath)
                 % check if the checkbox was clicked
                 node = treePath.getLastPathComponent;
                 if clickX <= (jtree.getPathBounds(treePath).x+16)         
                    [~,isCheckNode,~]=util.isCheckNode(node);                                   
                    if isCheckNode                        
                        [~,nodeId] = util.checkNode(self.tree,node);
                        if  ~isempty(strfind(nodeId,'mallas'))                            
                            self.showHideMallas()
                         elseif strcmp(nodeId,'dow')
                              self.showHideDow;
                         elseif strcmp(nodeId,'costa')
                              self.showHideCosta;                                                                                           
                        elseif strfind(nodeId,'perfil_')                            
                           perfil=nodeId(8:end);                            
                           self.showHidePerfil(perfil);                            
                        elseif strfind(nodeId,'roi')
                            self.showHideRoi();                                                
                        elseif strfind(nodeId,'poi')
                            poi=nodeId(5:end);
                            self.showHidePoi(poi);
                        elseif strfind(nodeId,'rotura')                            
                            self.showHideRotura();
                        elseif strfind(nodeId,'transporte')                            
                            self.showHideTransporte();
                        elseif strfind(nodeId,'flujoEnergiaPois')                            
                            self.showHideFlujoEnergiaPois();
                        elseif strfind(nodeId,'flujoEnergia_')                            
                            self.showHideFlujoEnergia();
                        end                                           
                    end
                 end
                 %%%%Select Node Method
                 %self.nodeSelected(node);
                 jtree.setSelectionPath(treePath);
                  if eventData.isMetaDown
                      [isCheck,isCheckNode,nodeId]=util.isCheckNode(node);
                        if isCheckNode                            
                            if strfind(nodeId,'poi_')
                                self.cmPoi.jmenu.show(jtree, clickX, clickY);
                            elseif strfind(nodeId,'perfil_')
                                perfil=nodeId(8:end);
                                
                                perfilPlot=self.perfilLayers(perfil);
                                isEdit=strcmp(get(perfilPlot,'Tag'),'imline');
                                set(self.cmPerfil.editar,'Text',self.txt.post('cmProfileEdit'));
                                if isEdit,set(self.cmPerfil.editar,'Text',self.txt.post('cmProfileApply'));end
                                
                                if self.roturaCalculada(perfil)
                                    %set(self.cmPerfil.editar,'Enabled','off');
                                    %set(self.cmPerfil.eliminar,'Enabled','off');
                                    set(self.cmPerfil.evolucion,'Enabled','on');
                                    set(self.cmPerfil.histograma,'Enabled','on');
                                else
                                        %set(self.cmPerfil.editar,'Enabled','on');
                                    if ~isCheck,set(self.cmPerfil.editar,'Enabled','off');end
                                    %set(self.cmPerfil.eliminar,'Enabled','on');
                                    set(self.cmPerfil.evolucion,'Enabled','off');
                                    set(self.cmPerfil.histograma,'Enabled','off');
                                end
                                
                                self.cmPerfil.jmenu.show(jtree, clickX, clickY);
%                             elseif strfind(nodeId,'transporte_');
%                                  self.cmMapaTransporte.jmenu.show(jtree, clickX, clickY);
                            end
                        else
                            if strcmp(nodeId,'fld_transporte')
                                self.cmTransporte.jmenu.show(jtree, clickX, clickY);
                            elseif strcmp(nodeId,'fld_poi')
                                self.cmPois.jmenu.show(jtree, clickX, clickY);
                            end
                        end

                  end
                 %%%%ContextMenu
              end
        end               
    end    
    %%%%% End Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% POIS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods 
       function poiProperties(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);            
            poi=self.mopla.pois(nodeId(5:end));
            
            hold(self.ax,'on');
            point=plot(poi.x,poi.y,'ro','MarkerSize',12,'MarkerFaceColor','r');
            hold(self.ax,'off');
            
            post.poiForm([],poi.x,poi.y,poi.z,poi.nombre);
           
            if ishandle(point),delete(point);end;                                                 
            delete(gcbf);
        end
        
       function poiDelete(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);
            poi=nodeId(5:end);
            poiDir=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId);
            poiFiles=dir(fullfile(poiDir,[poi,'.poi*']));
            if ~isempty(poiFiles)
                button=questdlg(self.txt.post('qResultsFoundTitle'),...
                    '',self.txt.post('qResultsFoundYes'),self.txt.post('qResultsFoundNo'),self.txt.post('qResultsFoundYes'));
                if ~strcmp(button,self.txt.post('qResultsFoundYes'))
                    return;
                else
                    delete(fullfile(poiDir,[poi,'.poi']));
                    delete(fullfile(poiDir,[poi,'.poi_']));       
                    if isKey(self.flujoEnergiaPoisLayer,poi)
                        efResult=self.flujoEnergiaPoisLayer{poi};
                        delete(efResult.plot);                        
                        remove(self.flujoEnergiaPoisLayer,poi);
                        if isemtpy(self.flujoEnergiaPoisLayer)
                           self.refreshMapaExplorer;
                        end
                    end
                    %if ~isempty(self.flujoEnergiaPoisLayer)
                    %    [efAll,efDirAll]=post.poisFemForm(self.mopla,self.project,self.alternativa,false);                        
                    %    self.flujoEnergiaPoisValues=[efAll,efDirAll];
                    %    self.crearMapaFlujoEnergiaPois();    
                    %    self.refreshMapaExplorer();
                    %end
                    %core.refreshAmevaExplorer(self.tree,self.project,self);
                end
            
            end
            node.removeFromParent;
            parent=util.getNodeByKey(self.tree,'fld_poi');
            self.tree.reloadNode(parent);            
            self.mopla.pois.remove(nodeId(5:end));
            delete(self.poiLayers(nodeId(5:end)));
            self.poiLayers.remove(nodeId(5:end));
            
        end
        
       function poiResults(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);            
            poiId=nodeId(5:end);
            core.blockMainForm             
            try
                smc.saveMopla(fullfile(self.project.folder,self.alternativa.nombre),self.mopla,{'pois'});
                core.refreshAmevaExplorer(self.tree,self.project,self);
                notify(self,'ChangeMenu');            
                [liteData,error]=data.getPOIData(self.mopla,poiId,self.project,self.alternativa);                 
            catch ex
                msgbox(ex.message,'error','error');
            end
            core.blockMainForm             
            if isempty(error)                
                post.poiResultsForm(self.mopla,poiId,liteData)
            else                
                msgbox({self.txt.post('iOlucaReadErrorLabel'),error},self.txt.post('iOlucaReadErrorTitle'),'error');
            end
            
       end
       
       function rebuildPois(self,varargin)
           pois=keys(self.mopla.pois);
           core.blockMainForm;    
           post.rebuildPois(self.mopla,pois,self.project,self.alternativa);
           core.blockMainForm;  
       end
       
       function limpiarPois(self,varargin)
            core.blockMainForm;    
            pois=keys(self.mopla.pois);
            moplaDir=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId); 
            try
            for i=1:length(pois)
                liteFile=fullfile(moplaDir,[pois{i},'.poi_']);
                if exist(liteFile,'file')
                    delete(liteFile)
                end
                dataFile=fullfile(moplaDir,[pois{i},'.poi']);
                if exist(dataFile,'file')
                    delete(dataFile)
                end                
            end
            self.borraMapaFlujoEnergiaPois();
            self.refreshMapaExplorer();
            catch ex
                msgbox(ex.message,'error','error');
            end
            core.blockMainForm;    
       end
        
    end
    %%%%% End POIS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Layers
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods 
         function showHideMallas(self) 
             if isempty(self.mallasLayer),return;end
             visible=get(self.mallasLayer(1),'Visible');
             if strcmp(visible,'on'),visible='off';
             else visible='on';end                
             for i=1:length(self.mallasLayer)
               set(self.mallasLayer,'Visible',visible);
             end            
        end
        
         function showHideDow(self)            
            visible=get(self.dowLayer,'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end
            set(self.dowLayer,'Visible',visible);
         end
        
         function showHideCosta(self)
             if isempty(self.costaLayer),return;end
              visible=get(self.costaLayer(1),'Visible');
              if strcmp(visible,'on'),visible='off';
              else visible='on';end                
              for i=1:length(self.costaLayer)
                set(self.costaLayer,'Visible',visible);
              end
         end                 
         
         function showHideRoi(self)
            visible=get(self.roiLayer,'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end
            set(self.roiLayer,'Visible',visible);
         end
         
         function showHidePoi(self,poiKey)
            visible=get(self.poiLayers(poiKey),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            set(self.poiLayers(poiKey),'Visible',visible);
         end   
         
         function showHidePerfil(self,perfil)
            visible=get(self.perfilLayers(perfil),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            set(self.perfilLayers(perfil),'Visible',visible);
         end
         
         function showHideRotura(self)
             perfiles=keys(self.roturaLayer);
             if ~isempty(perfiles)
                 roturaPerfilLayer=self.roturaLayer(perfiles{1});                 
                 visible=get(roturaPerfilLayer(1),'Visible');
                 if strcmp(visible,'on'),visible='off';
                else visible='on';end
                for i=1:length(perfiles)
                    roturaPerfilLayer=self.roturaLayer(perfiles{i});                 
                    for j=1:length(roturaPerfilLayer)
                        set(roturaPerfilLayer(j),'Visible',visible);
                    end
                end
             end
         end
         
         function showHideTransporte(self)
             if ~isempty(self.transporteLayer)
                 perfiles=keys(self.transporteLayer);
                 result=self.transporteLayer(perfiles{1});
                 visible=get(result.plot,'Visible');
                 if strcmp(visible,'on'),visible='off';
                else visible='on';end
                for i=1:length(perfiles)
                    result=self.transporteLayer(perfiles{i});                    
                    set(result.plot,'Visible',visible);                    
                end                
             end   
         end
         
         function showHideFlujoEnergia(self)
             if ~isempty(self.flujoEnergiaLayer)
                 perfiles=keys(self.flujoEnergiaLayer);
                 efResult=self.flujoEnergiaLayer(perfiles{1});
                 visible=get(efResult.plot,'Visible');
                 if strcmp(visible,'on'),visible='off';
                else visible='on';end
                for i=1:length(perfiles)
                    efResult=self.flujoEnergiaLayer(perfiles{i});                    
                    set(efResult.plot,'Visible',visible);                    
                end                
             end      
         end   
         
         function showHideFlujoEnergiaPois(self)             
             if ~isempty(self.flujoEnergiaPoisLayer)
                 pois=keys(self.flujoEnergiaPoisLayer);
                 efResult=self.flujoEnergiaPoisLayer(pois{1});
                 visible=get(efResult.plot(1),'Visible');
                 if strcmp(visible,'on'),visible='off';
                else visible='on';end
                for i=1:length(pois)
                    efResult=self.flujoEnergiaPoisLayer(pois{i});                    
                    set(efResult.plot,'Visible',visible);                    
                end                
             end             
         end                     
     end       
    %%%   End Layers
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%  Rotura
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
          function calculado=roturaCalculada(self,perfilId)
            calculado=false;
            roturaFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[perfilId,'_prf.mat']);
            if exist(roturaFile,'file')
                calculado=true;
            end    
          end
          
          function evolucionRotura(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);            
            perfilId=nodeId(8:end);
            f=findobj('Tag',['smcWinBreakingEvo_',perfilId]);
            if isempty(f)
                core.blockMainForm;
                try
                folder=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId);
                perfil=load(fullfile(folder,[perfilId,'_prf.mat']),'-mat');
                perfil=perfil.perfil;
                post.breakingEvolutionForm(self.mopla,perfil);
                catch ex
                    msgbox(ex.message,'Error','error');
                end
                core.blockMainForm;
                    
            else                
                figure(f);
            end
          end
          
          function histogramaRotura(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);            
            perfilId=nodeId(8:end);
            core.blockMainForm;
            try
            post.breakingHistogramForm(self.project,self.alternativa,self.mopla,perfilId)
            catch ex
                msgbox(ex.message,'error','error');
            end
            core.blockMainForm;    
          end
    end    
    %%%  End Rotura
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Transporte/Perfiles
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function refreshTransporteExplorer(self)
             transporteNode= util.getNodeByKey(self.tree,'fld_transporte');
             util.cleanNode(transporteNode,self.tree);
             perfiles=keys(self.mopla.perfiles);
             for i=1:length(perfiles)           
                 check='_1';
                 if ~isempty(self.perfilLayers)                     
                     if strcmp(get(self.perfilLayers(perfiles{i}),'Visible'),'off'), check='_0';end
                 end
                 perfilNode=uitreenode('v0',['chk_perfil_',perfiles{i},check],perfiles{i},[],true);
                 util.setNodeIcon(perfilNode,'open_polygon.gif');
                 transporteNode.add(perfilNode);
             end
             self.tree.reloadNode(transporteNode);
            util.refreshTree(self.tree);
        end
         
        function eliminarPerfil(self,varargin)                  
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);
            perfilId=nodeId(8:end);
            hasChanges=false;
            if self.transporteCalculadoPerfil(perfilId)
                res=questdlg(self.txt.post('qResultsFoundTitle'));    
                pause(0.3); %% En windows se bloquea
                if ~strcmp(res,'Yes')
                     return;
                end
                hasChanges=true;
            end
            node.removeFromParent;
            parent=util.getNodeByKey(self.tree,'fld_transporte');            
            self.tree.reloadNode(parent);
            delete(self.mopla.perfiles(nodeId(8:end)));            
            delete(self.perfilLayers(nodeId(8:end)));
            self.perfilLayers.remove(nodeId(8:end));
            self.mopla.perfiles.remove(nodeId(8:end)); 
            self.limpiarPerfil(perfilId);
            if hasChanges
                self.saveMopla();
            end
        end
        
        function editarPerfil(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);	
            perfilId=nodeId(8:end);            
            hasChanges=false;
            isEdit=strcmp(get(self.perfilLayers(perfilId),'Tag'),'imline');
            if ~isEdit
                if self.transporteCalculadoPerfil(perfilId)
                    res=questdlg(self.txt.post('qResultsFoundTitle'));    
                    pause(0.3); %% En windows se bloquea
                    if ~strcmp(res,'Yes')
                         return;
                    end
                    hasChanges=true;
                end                
                perfil=self.mopla.perfiles(perfilId);                
                delete(self.perfilLayers(perfilId));
                self.limpiarPerfil(perfilId); 
                self.perfilLayers(perfilId)=imline(self.ax,perfil.posicion);
                
            else
                perfilLine=self.perfilLayers(perfilId);
                position=perfilLine.getPosition;
                delete(perfilLine);
                perfil=self.mopla.perfiles(perfilId);
                perfil.setPosicion(position,self.alternativa);                
                hold(self.ax,'on');
                self.perfilLayers(perfilId)=plot(position(:,1),position(:,2),'Color',self.profileColor,'lineWidth',2);                               
                hold(self.ax,'off');
            end
            if hasChanges
                self.saveMopla();
            end
        end
        
        function editarPerfilXY(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [isCheck,~,nodeId]=util.isCheckNode(node);	
            perfilId=nodeId(8:end);        
            perfil=self.mopla.perfiles(perfilId);    
            perfilLine=self.perfilLayers(perfilId);
            set(perfilLine,'Color','y','LineWidth',4,'Visible','on');
            
            posicion=post.setProfileCoordsDlg(perfil);       
            diff=sqrt(sum(sum((perfil.posicion-posicion).^2)));
            hasChanges=false;
            visible='on';
            if ~isCheck,visible='off';end
            set(perfilLine,'Color',self.profileColor,'LineWidth',2,'Visible',visible);            
            
            if diff>1E-01
                if self.transporteCalculadoPerfil(perfilId)
                    res=questdlg(self.txt.post('qResultsFoundTitle'));    
                    pause(0.3); %% En windows se bloquea
                    if ~strcmp(res,'Yes')
                         return;
                    end
                    hasChanges=true;
                end                
                perfil.setPosicion(posicion,self.alternativa);                
                set(perfilLine,'XData',perfil.posicion(:,1),'YData',perfil.posicion(:,2));
                self.limpiarPerfil(perfilId);   
            end
            if hasChanges
                self.saveMopla();
            end
        end
        
        function propiedadesPerfil(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [isCheck,~,nodeId]=util.isCheckNode(node);	
            perfilId=nodeId(8:end);  
            perfil=self.mopla.perfiles(perfilId); 
            d50=perfil.d50;
            n=perfil.n;
            rho_s=perfil.rho_s;            
            
            perfilLine=self.perfilLayers(perfilId);
            set(perfilLine,'Color','y','LineWidth',4,'Visible','on');            
            perfil.editProps();
            visible='on';
            if ~isCheck,visible='off';end
            set(perfilLine,'Color',self.profileColor,'lineWidth',2,'Visible',visible);                                           
            
            hasChanges=d50~=perfil.d50 | rho_s~=perfil.rho_s |n~=perfil.n;
            %%Por si cambian las propiedades dinámicas
             if self.transporteCalculadoPerfil(perfilId) && hasChanges  && isKey(self.transporteLayer,perfilId)            
                 self.actualizarTransporte(perfilId);
                 self.crearMapaTransporte;                 
             end
            
        end
        
        function visualizarPerfil(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);	
            perfilId=nodeId(8:end);   
            post.profileForm(self.mopla,perfilId)
        end   
        
        function calculado=transporteCalculado(self)   
            calculado=false;
            perfiles=keys(self.mopla.perfiles);
            for i=1:length(perfiles)
                perfilId=perfiles{i};
                if self.transporteCalculadoPerfil(perfilId)
                    calculado=true;
                    break;
                end               
                
            end
           
        end
        
        function calculado=transporteCalculadoPerfil(self,perfilId)
            %calculado=isKey(self.transporteLayer,perfilId);
            moplaDir=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId);            
            tteFile=fullfile(moplaDir,[perfilId,'_tte.mat']);
            calculado=exist(tteFile,'file');
        end
        
        function limpiarTransporte(self,varargin)
            perfiles=keys(self.mopla.perfiles);
            for i=1:length(perfiles)                
                perfilId=perfiles{i};
                %%%% Si estaba en modo edicion lo revierto
                isEdit=strcmp(get(self.perfilLayers(perfilId),'Tag'),'imline');
                if isEdit
                    perfilLine=self.perfilLayers(perfilId);                
                    delete(perfilLine);
                    perfil=self.mopla.perfiles(perfilId);                
                    hold(self.ax,'on');
                    self.perfilLayers(perfilId)=plot(perfil.posicion(:,1),perfil.posicion(:,2),self.profileColor,'lineWidth',2);                               
                    hold(self.ax,'off');
                end
                
                %%%%Borro el fichero de resultados
                moplaFolder=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId);
                transFile=dir(fullfile(moplaFolder,[perfilId,'*.mat']));
                for j=1:length(transFile)
                    delete(fullfile(moplaFolder,transFile(j).name));
                end
                
            end            
            self.borraMapaRotura; 
            self.borraMapaTransporte;
            self.borraMapaFlujoEnergia;
            self.refreshMapaExplorer;
            core.refreshAmevaExplorer(self.tree,self.project,self);
        end
        
        function actualizarTransporte(self,perfilId)
            core.blockMainForm;
            try                
            moplaDir=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId);
            perfil=self.mopla.perfiles(perfilId);
            tteFile=fullfile(moplaDir,[perfilId,'_tte.mat']);
            if exist(tteFile,'file')
                pTTE=load(tteFile,'-mat','hs_b','h_b','l_b','dir_b');
                method=self.mopla.tranportModel;
                kMethod=self.mopla.kModel;
                q=post.longshoreTransport(pTTE.hs_b,pTTE.h_b,pTTE.dir_b,self.mopla.tp,...
                    pTTE.l_b,perfil,method,kMethod);        

                [index,n]=self.getTransporteTimeIndex;                
                q=sum(q(index))*3600/n;
                if isKey(self.transporteLayer,perfilId);
                    result=self.transporteLayer(perfilId);
                    if ~isempty(result.plot)
                        delete(get(result.plot,'UIContextmenu'));
                        delete(result.plot);
                    end
                end
                result.q=q;
                result.plot=[];
                self.transporteLayer(perfilId)=result;
            end
            catch ex
                msgbox(ex.message,'error','error');
            end
            core.blockMainForm;
        end
        
        function rebuildProfiles(self,varargin)
           perfiles=keys(self.mopla.perfiles);
           perfilesBrk={};
           for i=1:length(perfiles)
               if self.roturaCalculada(perfiles{i})
                 perfilesBrk=cat(1,perfilesBrk,perfiles{i});               
               end
           end           
           if ~isempty(perfilesBrk) 
               self.saveMopla;
                core.blockMainForm;  
                post.rebuildProfiles(self.mopla,perfilesBrk,self.project,self.alternativa);
                core.blockMainForm;  
           end           
        end
        
%         function resultadosTransporte(self,varargin)
%             data=[self.transporteValues,self.flujoEnergiaValues];
%             if isempty(data),return;end;
%             perfiles=keys(self.mopla.perfiles);
%             perfilesTabla={};
%             for i=1:length(perfiles)                
%                 perfilId=perfiles{i};
%                 if self.transporteCalculadoPerfil(perfilId)     
%                     perfilesTabla=cat(1,perfilesTabla,perfilId);
%                 end                
%             end
%             data=[perfilesTabla,data];
%             f=figure( 'MenuBar', 'none', 'Name', self.txt.post('wTransportResultTitle'), 'NumberTitle', 'off', 'Toolbar', 'none','Position',[200 200 400 600]);
%             table=uitable(f,'units','normalized','ColumnName',{'Id','Q [m^3/yr]',self.txt.post('wPoisFemColEf'),self.txt.post('wPoisFemColEfDir')});
%             set(table,'Data',data,'Position',[0,0,1,1]);
%             set(table,'ColumnWidth','auto');
%             set(f,'Visible','on');
%         end
        
        function limpiarPerfil(self,perfilId)            
             moplaFolder=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId);
             prfFile=fullfile(moplaFolder,[perfilId,'_prf.mat']);             
             tteFile=fullfile(moplaFolder,[perfilId,'_tte.mat']);
             if exist(prfFile,'file')
                 delete(prfFile);
             end
             if exist(tteFile,'file')
                 delete(tteFile);
             end
             if isKey(self.roturaLayer,perfilId)
                 roturaPerfil=self.roturaLayer(perfilId);
                 for i=1:length(roturaPerfil)
                     delete(roturaPerfil(i));
                 end
                 remove(self.roturaLayer,perfilId);
             end             
             if isKey(self.flujoEnergiaLayer,perfilId)
                 ef=self.roturaLayer(perfilId);
                 if ~isempty(ef.plot)
                     efMenu=get(ef.plot,'UIContextMenu');
                     delete(efMenu);
                    delete(ef.plot);
                 end
                 remove(self.flujoEnergiaLayer,perfilId);
             end       
             if isKey(self.flujoEnergiaLayer,perfilId)
                 q=self.roturaLayer(perfilId);
                 if ~isempty(q.plot)
                     qMenu=get(q.plot,'UIContextMenu');
                     delete(qMenu);
                    delete(q.plot);
                 end
                 remove(self.flujoEnergiaLayer,perfilId);
             end      
             self.refreshMapaExplorer;
        end
    end
    %%%  End Transporte
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Mapas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function crearMapaRotura(self,perfiles)
            %tic
            %h=waitbar(0,'Creando mapa de rotura');
            %x=[];y=[];hist_b=[];
            hold(self.ax,'on');             
             for i=1:length(perfiles)
                 [x_,y_,hist_b_]=post.histogramaRotura(self.mopla,perfiles(i));
                 idx=(hist_b_>0);                 
                 x=x_(idx);
                 y=y_(idx);
                 hist_b=hist_b_(idx);
                 roturaLayerPerfil=[];
                 for j=1:length(hist_b)                     
                     c=[1-hist_b(j),1-hist_b(j),1-hist_b(j)];
                     roturaLayerPerfil=cat(1,roturaLayerPerfil,plot(self.ax,x(j),y(j),'o','MarkerEdgeColor',c,'MarkerFaceColor',c,'Visible','off'));                     
                 end                 
                 if ~isempty(x)
                    self.roturaLayer(perfiles(i).id)=roturaLayerPerfil;
                 end
             end
             hold(self.ax,'off');
        end
        
        function crearMapaTransporte(self)
             core.blockMainForm;
            try                
            format='%.2f';           
            perfiles=keys(self.transporteLayer);
            
            l=self.calcularFemEscala(); 
            q=[];
            qPlus=[];
            qMinus=[];
            
            for i=1:length(perfiles)
                result=self.transporteLayer(perfiles{i});                
                q=cat(1,q,result.q);
                qPlus=cat(1,qPlus,result.qPlus);
                qMinus=cat(1,qMinus,result.qMinus);
                
            end
            
            hold(self.ax,'on');
            for i=1:length(perfiles)
                result=self.transporteLayer(perfiles{i});                
                if ~isempty(result.plot)
                    delete(get(result.plot,'UIContextMenu'));
                    delete(result.plot);                    
                end
                qMenu=uicontextmenu;
                p=self.mopla.perfiles(perfiles{i});
                uimenu(qMenu,'Label',['Id: ',perfiles{i}]);                
                uimenu(qMenu,'Label',['Q [m^3]: ',num2str(q(i),format)]);                
                uimenu(qMenu,'Label',['Q+ [m^3]: ',num2str(qPlus(i),format)]);                
                uimenu(qMenu,'Label',['Q- [m^3]: ',num2str(qMinus(i),format)]);                
                [x_1,y_1,x_2,y_2]=p.getNormalVector(-sign(q(i))*abs(l*q(i)/max(abs(q))));                
                a=util.arrow(x_1,y_1,x_2-x_1,y_2-y_1,self.ax,'UIContextMenu',qMenu);                
                set(a,'linewidth',2);
                set(a,'color',self.transportColor);                
                result.plot=a;
                self.transporteLayer(perfiles{i})=result;
            end
            hold(self.ax,'off');
            catch ex
                msgbox(ex.message,'Error','error');
            end
            core.blockMainForm;
            
        
        end
        
        function crearMapaFlujoEnergia(self) 
          core.blockMainForm;
            try                
            format='%.2f';           
            perfiles=keys(self.flujoEnergiaLayer);
            
            l=self.calcularFemEscala(); 
            ef=[];
            efDir=[];
            for i=1:length(perfiles)
                efResult=self.flujoEnergiaLayer(perfiles{i});                
                ef=cat(1,ef,efResult.ef);
                efDir=cat(1,efDir,efResult.efDir);
            end
            dirCartesiana=270-efDir;            
            
            hold(self.ax,'on');
            for i=1:length(perfiles)
                efResult=self.flujoEnergiaLayer(perfiles{i});                
                if ~isempty(efResult.plot)
                    delete(get(efResult.plot,'UIContextMenu'));
                    delete(efResult.plot);                    
                end
                efMenu=uicontextmenu;
                p=self.mopla.perfiles(perfiles{i});
                uimenu(efMenu,'Label',['Id: ',perfiles{i}]);                
                uimenu(efMenu,'Label',[self.txt.post('wPoisFemColEf'),': ',num2str(ef(i),format)]);
                uimenu(efMenu,'Label',[self.txt.post('wPoisFemColEfDir'),': ',num2str(efDir(i),format)]);   
                [x_m,y_m]=p.getMiddlePoint();
                [x,y,u,v]=util.getVectorVertex(x_m,y_m,l*ef(i)/max(ef),dirCartesiana(i),'origin');                
                a=util.arrow(x,y,u,v,self.ax,'UIContextMenu',efMenu);                
                set(a,'linewidth',2);
                set(a,'color',self.femColor);                
                efResult.plot=a;
                self.flujoEnergiaLayer(perfiles{i})=efResult;
            end
            hold(self.ax,'off');
            catch ex
                msgbox(ex.message,'Error','error');
            end
            core.blockMainForm;
    
        end
        
        function femLength=calcularFemEscala(self)
            femLength=400; %Longitud de la flecha grande (m)
            perfiles=keys(self.mopla.perfiles);          
            l=zeros(length(perfiles),1);
            for i=1:length(perfiles)   
               perfilId=perfiles{i};
               p=self.mopla.perfiles(perfilId);
               l(i)=p.getLength;                 
            end            
            l=max(l)/3;                  
            if l>0
                femLength=l;
            end
        end
        
        function crearMapaFlujoEnergiaPois(self)
            core.blockMainForm;
            try                
            format='%.2f';           
            pois=keys(self.flujoEnergiaPoisLayer);
            
            l=self.calcularFemEscala(); 
            ef=[];
            efDir=[];
            for i=1:length(pois)
                efResult=self.flujoEnergiaPoisLayer(pois{i});                
                ef=cat(1,ef,efResult.ef);
                efDir=cat(1,efDir,efResult.dir);
            end
            dirCartesiana=90-efDir+180;            
            
            hold(self.ax,'on');
            for i=1:length(pois)
                efResult=self.flujoEnergiaPoisLayer(pois{i});                
                if ~isempty(efResult.plot)
                    delete(get(efResult.plot(1),'UIContextMenu'));
                    delete(efResult.plot(1));                    
                end
                poiMenu=uicontextmenu;
                poi=self.mopla.pois(pois{i});
                uimenu(poiMenu,'Label',['Id: ',pois{i}]);
                uimenu(poiMenu,'Label',['x: ',num2str(poi.x,format)]);
                uimenu(poiMenu,'Label',['y: ',num2str(poi.y,format)]);
                uimenu(poiMenu,'Label',[self.txt.post('wPoisFemColEf'),': ',num2str(ef(i),format)]);
                uimenu(poiMenu,'Label',[self.txt.post('wPoisFemColEfDir'),': ',num2str(efDir(i),format)]);   
                
                [x,y,u,v]=util.getVectorVertex(poi.x,poi.y,l*ef(i)/max(ef),dirCartesiana(i),'center');                
                a=util.arrow(x,y,u,v,self.ax,'UIContextMenu',poiMenu);                
                set(a,'linewidth',2);
                set(a,'color',self.femColor);                
                efResult.plot=a;
                self.flujoEnergiaPoisLayer(pois{i})=efResult;
            end
            hold(self.ax,'off');
            catch ex
                msgbox(ex.message,'Error','error');
            end
            core.blockMainForm;
        end         
              
        function borraMapaRotura(self)
            if ~isempty(self.roturaLayer)
                perfiles=keys(self.roturaLayer);                
                for i=1:length(perfiles)
                    delete(self.roturaLayer(perfiles{i}));
                    remove(self.roturaLayer,perfiles{i});                    
                end
                
            end
        end
        
        function borraMapaTransporte(self)
            if ~isempty(self.transporteLayer)
               perfiles=keys(self.transporteLayer);
               for i=1:length(perfiles)
                   result=self.transporteLayer(perfiles{i});                   
                   qMenu=get(result.plot,'UIContextMenu');                   
                    delete(qMenu);
                    delete(result.plot);                       
                   
                   remove(self.transporteLayer,perfiles{i});
               end
           end    
        end
        
        function borraMapaFlujoEnergia(self)
           if ~isempty(self.flujoEnergiaLayer)
               perfiles=keys(self.flujoEnergiaLayer);
               for i=1:length(perfiles)
                   efResult=self.flujoEnergiaLayer(perfiles{i});                   
                   efMenu=get(efResult.plot,'UIContextMenu');                   
                    delete(efMenu);
                    delete(efResult.plot);                       
                   
                   remove(self.flujoEnergiaLayer,perfiles{i});
               end
           end            
        end       
        
        function borraMapaFlujoEnergiaPois(self)           
           if ~isempty(self.flujoEnergiaPoisLayer)
               pois=keys(self.flujoEnergiaPoisLayer);
               for i=1:length(pois)
                   efResult=self.flujoEnergiaPoisLayer(pois{i});
                   for j=1:length(efResult.plot)
                       poiMenu=get(efResult.plot(j),'UIContextMenu');                   
                       delete(poiMenu);
                       delete(efResult.plot(j));                       
                   end
                   remove(self.flujoEnergiaPoisLayer,pois{i});
               end
           end
        end
        
        function refreshMapaExplorer(self)
            %%%De momento solo la rotura
             mapasNode= util.getNodeByKey(self.tree,'fld_mapas');
             util.cleanNode(mapasNode,self.tree);
             check='_1';
             if ~isempty(self.roturaLayer) 
                 perfiles=keys(self.roturaLayer);
                 roturaPerfil=self.roturaLayer(perfiles{1});
                 if strcmp(get(roturaPerfil(1),'Visible'),'off'), check='_0';end
                 roturaNode=uitreenode('v0',['chk_rotura_',check],self.txt.post('exMapBreaking'),[],true);
                 util.setNodeIcon(roturaNode,'wave.gif');
                 mapasNode.add(roturaNode);                 
             end
             check='_1';
             if ~isempty(self.transporteLayer) 
                 perfiles=keys(self.transporteLayer);
                 result=self.transporteLayer(perfiles{1});
                  if strcmp(get(result.plot,'Visible'),'off'), check='_0';end
                  transporteNode=uitreenode('v0',['chk_transporte_',check],self.txt.post('exMapTransport'),[],true);
                  util.setNodeIcon(transporteNode,'transporte.gif');
                  mapasNode.add(transporteNode);                 
             end
             check='_1';
             if ~isempty(self.flujoEnergiaLayer) 
                 perfiles=keys(self.flujoEnergiaLayer);
                 efResult=self.flujoEnergiaLayer(perfiles{1});
                 if strcmp(get(efResult.plot,'Visible'),'off'), check='_0';end
                 flujoNode=uitreenode('v0',['chk_flujoEnergia_',check],self.txt.post('exMapEnergy'),[],true);
                 util.setNodeIcon(flujoNode,'energia.gif');
                 mapasNode.add(flujoNode);                 
             end             
             check='_1';
             if ~isempty(self.flujoEnergiaPoisLayer) 
                 pois=keys(self.flujoEnergiaPoisLayer);
                 efResult=self.flujoEnergiaPoisLayer(pois{1});
                 if strcmp(get(efResult.plot(1),'Visible'),'off'), check='_0';end
                 flujoPoisNode=uitreenode('v0',['chk_flujoEnergiaPois_',check],self.txt.post('exMapEnergyPois'),[],true);
                 util.setNodeIcon(flujoPoisNode,'energia.gif');
                 mapasNode.add(flujoPoisNode);                 
             end
             self.tree.reloadNode(mapasNode);
             util.refreshTree(self.tree);
        end
        
        function updateLegend(self)
%             legendStr={};
%             curves=[];
%             if ~isempty(self.transporteLayer)
%                  if strcmp(get(self.transporteLayer(1),'Visible'),'on');
%                      legendStr=self.transporteValues;
%                      curves=self.transporteLayer;
%                  end
%             end
%             
%             if ~isempty(self.flujoEnergiaLayer)
%                  if strcmp(get(self.flujoEnergiaLayer(1),'Visible'),'on');
%                      legendStr=cat(1,legendStr,self.flujoEnergiaValues);
%                      curves=cat(1,curves,self.flujoEnergiaLayer);
%                  end
%             end
%             delete(self.mapLegend);
%             self.mapLegend=[];
%             if ~isempty(curves)
%                 %self.mapLegend=legend(curves,legendStr);
%             end
        end
        
        function [index,n,units]=getTransporteTimeIndex(self)
            index=true(length(self.mopla.time),1);            
            dates=datevec(self.mopla.time);
            yearIni=dates(1,1);
            yearEnd=dates(end,1);
            n=yearEnd-yearIni+1;
            ini_=self.mopla.transportPeriod.ini;
            end_=self.mopla.transportPeriod.end;            
            units='m^3/year';
            if strcmp(self.mopla.transportPeriod,'MediaAnualTotal')
                
                
            elseif strcmp(self.mopla.transportPeriod,'MediaAnual')
                date=datevec(self.mopla.time);                
                index=date(:,1)==ini_;
                n=1;
                
            elseif strcmp(self.mopla.transportPeriod,'MediaEstacional')                               
                date=datevec(self.mopla.time);
                n_=end_-ini_+1;
                if ini_<=end_
                    index=date(:,2)>=ini_ & date(:,2)<=end_;                    
                else
                    n_=n_+12;
                    index=date(:,2)>=ini_ | date(:,2)<=end_;                    
                end
                units=['m^3/',num2str(n_),' months'];
                
            elseif strcmp(self.mopla.transportPeriod,'MediaMensual')
                date=datevec(self.mopla.time);                
                index=date(:,2)==ini_;
                units='m^3/month';
            end                
        end
        
%         function propiedadesMapaTransporte(self,varargin)
%             self.transporteMapOpciones=post.editMapPeriodForm(self.transporteMapOpciones,self.mopla.time);
%             self.crearMapaTransporte;
%             self.updateLegend();
%         end        
    end
    
    
end

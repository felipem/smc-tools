function [backward,backwardPdf,mu,sigma]=beachRotation(muDELTA_BETA,sigmaDELTA_BETA,l)
    backward=[];backwardPdf=[];mu=[];sigma=[];
    
    if l<=0,return;end
    
    npint = 7;
    
    parameters.mu = [muDELTA_BETA];
    parameters.varcov = [sigmaDELTA_BETA];
    
    
    %   Funcion para la estimacion de momentos
    moments = ClimChanSensitivity2(@(x) funcionGiro (x,l),@ (z,parameters) rosenblattBrunn (z,parameters),parameters,parameters.mu,1,npint,'display');
    
    means=moments(1);
    stddesviations=sqrt(moments(2));
    
     if ((means>=0) && (stddesviations>=0))        
        if ((means>0)||(stddesviations>0))
             x_ = linspace(means-3*stddesviations,means+3*stddesviations,1000);
            [pdf_, ~] = edgewothExpansion(x_,moments(1),sqrt(moments(2)),moments(3),moments(4));
            backward=x_;
            backwardPdf=pdf_;
            mu=means;
            sigma=stddesviations;
        end
     end
    
     
     
     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%         SUBFUNCIONES          %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1. ClimChanSensitivity2

     function [mom,S] = ClimChanSensitivity2(FunFcnIn,FunFcnRos,parameters,mu,M,N,dispres)
         
         if nargin <7,
             dispres = 'Not display';
             if nargin<6,
                 N = 2;
                 if nargin<5,
                     error('The minimum number parameters is 5: functional, Rosenblatt, parameters, means and dimension');
                 end
             end
         end
         
%          [FunFcn,errStruct] = fcnchk(FunFcnIn);
%          if ~isempty(errStruct)
%              error('MATLAB:InvalidFUN',errStruct.message)
%          end
%          [FunFcnRos,errStruct] = fcnchk(FunFcnRosIn);
%          if ~isempty(errStruct)
%              error('MATLAB:InvalidFUN',errStruct.message)
%          end
%          if ~isa(FunFcn,'function_handle') || ~isa(FunFcnRos,'function_handle')  % function handle
%              % % %     Ffcnstr = func2str(FunFcn);  % get the name passed in
%              % % %     Ftype = 'function_handle';
%              % % % else
%              error('Functional and Rosenblatt must be introduced as function_handles');
%          end
         
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %   Seleccion del numero de puntros de Gauss-Hermite, y calculo de pesos y
         %   localizaciones
         %   Numero de puntos
         [localizaciones,pesos] = Hermite (N);
         
         mom = ClimChanMoments(FunFcnIn,FunFcnRos,parameters,mu,pesos,localizaciones,M,N);
         
         parametersold = parameters;
         epsi = 10^(-5);
         if nargout == 2,
             %   Sensitivity analysis
             %   Create structure
             S = struct;
             names = fieldnames(parameters);
             nf = length(names);
             %   For all elements in parameters
             for i = 1:nf,
                 f = getfield(parameters,names{i});
                 [nf,nc] = size(f);
                 S = setfield(S,names{i},zeros(nf,nc,max(size(mom))));
                 for j = 1:nf,
                     for k = 1:nc,
                         parameters = parametersold;
                         aux = f(j,k)*(1+epsi);
                         parameters = setfield(parameters,names{i},{j,k},aux);
                         auxmomR = ClimChanMoments(FunFcnIn,FunFcnRos,parameters,mu,pesos,localizaciones,M,N);
                         aux = f(j,k)*(1-epsi);
                         parameters = setfield(parameters,names{i},{j,k},aux);
                         auxmomL = ClimChanMoments(FunFcnIn,FunFcnRos,parameters,mu,pesos,localizaciones,M,N);
                         if abs(f(j,k))>10^(-8),
                             deriv = (auxmomR-auxmomL)/(2*f(j,k)*epsi);
                         else
                             deriv = zeros(size(auxmomR));
                         end
                         S = setfield(S,names{i},{j,k,1:max(size(mom))},deriv);
                     end
                 end
             end
             
         end
         
         return
         
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %   Subfunctions
         
         function moments = ClimChanMoments(FunFcnIn,FunFcnRos,parameters,mu,pesos,localizaciones,M,N,dispres)
             
             dispres = 'No display';
             
             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
             %   M�todo de c�lculo de momentos por estimaci�n por puntos
             %   para las M variables aleatorias
             %   Numero de puntos, parametros para los valores de las ecuaicones (13)-(14)
             %   del art�culo de Zhao
             medias = zeros(M,1);
             varianzas = zeros(M,1);
             asimetrias = zeros(M,1);
             kurtosis = zeros(M,1);
             for l = 1:M,
                 if strcmpi(dispres,'display')
                     disp(['Variable: ' num2str(l)])
                 end
                 %   Para las tres localizaciones, la ultima corresponde al
                 %   valor central y solo se hace una vez
                 for r = 1:N,
                     if strcmpi(dispres,'display')
                         disp(['Localizacion: ' num2str(r)])
                         disp(['Peso ' num2str(pesos(r)) '; Local ' num2str(localizaciones(r))])
                     end
                     %   Calculo los terminos medios y desviaciones tipicas
                     %   x inicialmente se corresponde con el ZERO en el espacio normal
                     %   estandar multidimansional, usar Rosenblatt
                     z = zeros(M,1);
                     z(l)=localizaciones(r);
                     %   Calculo el punto en el espacio original
                     x = FunFcnRos (z,parameters);
                     auxmu = mu;
                     auxmu(l) = x(l);
                     %   Calculo la media
                     medias(l) = medias(l) + pesos(r)*FunFcnIn (auxmu);
                 end
                 for r = 1:N,
                     %   Calculo las varianzas, asimetrias y kurtosis
                     z = zeros(M,1);
                     z(l)=localizaciones(r);
                     x = FunFcnRos (z,parameters);
                     auxmu = mu;
                     auxmu(l) = x(l);
                     varianzas(l) = varianzas(l) + pesos(r)*(FunFcnIn (auxmu)-medias(l))^2;
                     asimetrias(l) = asimetrias(l) + pesos(r)*(FunFcnIn (auxmu)-medias(l))^3;
                     kurtosis(l) = kurtosis(l) + pesos(r)*(FunFcnIn (auxmu)-medias(l))^4;
                 end
             end
             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
             %   Calculo los momentos del funcional con las ecuaciones
             %   (22),(23),(24),(25)
             %         media = sum(medias)-FunFcn(FunFcnRos (zeros(M,1),parameters))*(M-1);
             media = sum(medias)-FunFcnIn (mu)*(M-1);
             varianza = sum(varianzas);
             asimetria = sum(asimetrias)/varianza^(3/2);
             kurtosi = sum(kurtosis);
             for ii = 1:M-1,
                 for jj = ii+1:M,
                     kurtosi = kurtosi + 6*varianzas(ii)*varianzas(jj);
                 end
             end
             kurtosi = kurtosi/varianza^2;
             
             moments = [media; varianza; asimetria; kurtosi];
             
         end
         
         
         function [n,x,w] = gqzero(n)
             % ref: http://www.aims.ac.za/resources/courses/na/gauss_quad.pdf
             % n: order of the hermite polynomila
             % x : nodes
             % w : weights
             d = sqrt((1:n)/2);
             T = diag(d,1)+diag(d,-1);
             [V,D] = eig(T);
             w = V(1,:).^2;
             w = w*sqrt(pi);
             x = diag(D);
             [x,ind] = sort(x);
             w = w(ind);
             w=w';
         end
         
         function [u,p] = Hermite (n)
             
             [n,x,w] = gqzero(n-1);
             
             u=sqrt(2)*x;
             p=w/sqrt(pi);
         end
         
     end
 
 % 2. outlierfilterRestCurv
 
     function [id,p,model,AIKlist,fval,exitflag,output,lambda,grad,hessian,residuo,rN,fmu,fsigma,Omega,upper,lower,...
             uppermean,lowermean,upperpredict,lowerpredict] = outlierfilterRestrCurv (z,u,alpha,conf,model)
         
         
         %
         %   Funtion outlierfilterRNoCte automatically detects the presence of
         %   outliers for a confidence level alpha and considering different
         %   regression and residual variance models identified by ID. The methods
         %   frist estimates the optimal parameters of the regression model
         %   maximizing the log-likelihood function, then calculates the normalized
         %   residuals and finally locates data which can be considered as outlier.
         %
         %   INPUT:
         %
         %   u-> Response variable (variable of interest)
         %   z-> Predictor variable (time)
         %   alpha-> Confidence level
         %       Model: fmu = p1+p2*z+p3*z^2
         %              fsigma = p4+p5*z+p6*z^2
         %
         %   OUTPUT:
         %
         %   p-> Optimal estimates of the regression parameters
         %
         
         
         %   Dimensions
         [n m] = size(z);
         if min(n,m)>1,
             error('Data z must be a row or column vector')
         elseif n==1,
             %   Transform data to a column vector
             z = z';
             n = m;
             m = 1;
         end
         [n1 m1] = size(u);
         if min(n1,m1)>1,
             error('Data u must be a row or column vector')
         elseif n1==1,
             %   Transform data to a column vector
             u = u';
             n1 = m1;
             m1 = 1;
         end
         if n1~=n,
             error('Data z and u must be consistent')
         end
         
         %   Procedimiento de c�lculo de los todos los modelos tomando el m�s
         %   completo de forma que todos los par�metros sean estad�sticamente
         %   significativos
         AIK = inf;
         optmod = 0;
         AIKlist = [];
         % % % for k = 15:-1:0,
         if nargin<5 || isempty(model),
             model = [0 8 12 14 2 3 11 10 15];
         else
             if isempty(model),
                 error('Que modelo quieres ajustar?')
             end
         end
         %
         for k = model,
             %   Defino el modelo binario
             mo = dec2bin(k,4);
             model = zeros(4,1);
             for i = 1:4,
                 model(i) = str2num(mo(i));
             end
             %   Valores iniciales de las variables
             if ~model(1) && ~model(2),
                 %       Model: fmu = p1+p2*z+p3*z^2
                 %              fsigma = p4+p5*z+p6*z^2
                 beta = mean(u);
                 stde = std(u);
                 X = [ones(n,1)];
             elseif model(1) && ~model(2),
                 %   Initial estimates for the parameters
                 X = [ones(n,1) z];
                 %   Solucion de las ecuaciones normales
                 L = chol(X'*X,'lower');
                 Xy = X'*u;
                 %   Parameter estimates with the linear model
                 beta = L'\(L\Xy);
                 %   Residual standard deviation
                 stde = std(u-beta(1)-beta(2)*z);
             elseif ~model(1) && model(2)
                 %   Initial estimates for the parameters
                 X = [ones(n,1) z.^2];
                 %   Solucion de las ecuaciones normales
                 L = chol(X'*X,'lower');
                 Xy = X'*u;
                 %   Parameter estimates with the linear model
                 beta = L'\(L\Xy);
                 beta = [beta(1);0;beta(2)];
                 %   Residual standard deviation
                 stde = std(u-beta(1)-beta(2)*z.^2);
             else
                 %   Initial estimates for the parameters
                 X = [ones(n,1) z z.^2];
                 %   Solucion de las ecuaciones normales
                 L = chol(X'*X,'lower');
                 Xy = X'*u;
                 %   Parameter estimates with the linear model
                 beta = L'\(L\Xy);
                 %   Residual standard deviation
                 stde = std(u-beta(1)-beta(2)*z-beta(3)*z.^2);
             end
             %   Initial values
             pini = zeros(2+sum(model),1);
             pini(1) = beta(1);
             pos = 2;
             if model(1),
                 pini(pos) = beta(2);  %p(2)
                 pos = pos+1;
             end
             if model(2),
                 pini(pos) = beta(3);  %p(3)
                 pos = pos+1;
             end
             pini(pos)=stde;
             % % %     if model(3),
             % % %         pini(pos+1) = 0.01;
             % % %     end
             
             %   Parameter estimation using the maximum likelihood method
             lb = -Inf*ones(2+sum(model),1);
             ub = Inf*ones(2+sum(model),1);
             if k==3 || k==11,
                 if auxsignstd>0,
                     lb(2+sum(model(1:2))+1:end) = 0;
                 else
                     ub(2+sum(model(1:2))+1:end) = 0;
                 end
             end
             if k==12 || k==14,
                 if auxsignmu>0,
                     lb(2:3) = 0;
                 else
                     ub(2:3) = 0;
                 end
             end
             if k==15,
                 if auxsignmu>0,
                     lb(2:3) = 0;
                 else
                     ub(2:3) = 0;
                 end
                 if auxsignstd>0
                     lb(2+sum(model(1:2))+1:end) = 0;
                 else
                     ub(2+sum(model(1:2))+1:end) = 0;
                 end
             end
             [p,fval,exitflag,output,lambda,grad,hessian,residuo]=  OptiParamHessian (z,u,model,pini,'sinconstraints',lb,ub);
             fsigma = p(pos)*ones(size(z));
             pos = pos + 1;
             if model(3),
                 fsigma = fsigma + p(pos)*z;
                 pos = pos + 1;
             end
             if model(4),
                 fsigma = fsigma + p(pos)*z.^2;
                 pos = pos + 1;
             end
             if any(fsigma<0)
                 [p,fval,exitflag,output,lambda,grad,hessian,residuo]=  OptiParamHessian (z,u,model,pini,'conconstraints',lb,ub);
             end
             %   Si el modelo es 0010 almaceno el signo de la tendencia lineal
             if k==2,
                 auxsignstd = p(3);
             end
             if k==8,
                 auxsignmu = p(2);
             end
             if k==10,
                 auxsignmu = p(2);
                 auxsignstd = p(4);
             end
             %   LU decomposition of the Information matrix
             [LX,UX,PX] = lu(hessian);
             %   Inverse
             invI0 = (UX)\(LX\(PX*eye(length(p))));
             %    Standard deviation
             stdpara = sqrt(diag(invI0));
             % % %     stdaux = sqrt(diag(stde^2*inv(X'*X)));
             aux = 2*fval+2*(2+sum(model));
             AIKlist = [AIKlist; aux];
             if ~any(imag(stdpara)),
                 
                 if aux<AIK,
                     AIK = aux;
                     optmod = k;
                     lbopt = lb;
                     ubopt = ub;
                 end
                 % % %         %   Bounds
                 % % %         upper = p+tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdpara;
                 % % %         lower = p-tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdpara;
                 % % %         if sum(sign(lower).*sign(upper))==length(p)
                 % % %             %   Hemos alcanzado el modelo mas completo en el que todos los
                 % % %             %   parametros son estadisticamente significtivos
                 % % %             break
                 % % %         end
             end
         end
         %   Una vez seleccionado el mejor modelo por AKAIKE lo ajusto de nuevo
         %   Defino el modelo binario
         k = optmod;
         mo = dec2bin(k,4);
         model = zeros(4,1);
         for i = 1:4,
             model(i) = str2num(mo(i));
         end
         %   Valores iniciales de las variables
         if ~model(1) && ~model(2),
             %       Model: fmu = p1+p2*z+p3*z^2
             %              fsigma = p4+p5*z+p6*z^2
             beta = mean(u);
             stde = std(u);
             X = [ones(n,1)];
         elseif model(1) && ~model(2),
             %   Initial estimates for the parameters
             X = [ones(n,1) z];
             %   Solucion de las ecuaciones normales
             L = chol(X'*X,'lower');
             Xy = X'*u;
             %   Parameter estimates with the linear model
             beta = L'\(L\Xy);
             %   Residual standard deviation
             stde = std(u-beta(1)-beta(2)*z);
         elseif ~model(1) && model(2)
             %   Initial estimates for the parameters
             X = [ones(n,1) z.^2];
             %   Solucion de las ecuaciones normales
             L = chol(X'*X,'lower');
             Xy = X'*u;
             %   Parameter estimates with the linear model
             beta = L'\(L\Xy);
             beta = [beta(1);0;beta(2)];
             %   Residual standard deviation
             stde = std(u-beta(1)-beta(2)*z.^2);
         else
             %   Initial estimates for the parameters
             X = [ones(n,1) z z.^2];
             %   Solucion de las ecuaciones normales
             L = chol(X'*X,'lower');
             Xy = X'*u;
             %   Parameter estimates with the linear model
             beta = L'\(L\Xy);
             %   Residual standard deviation
             stde = std(u-beta(1)-beta(2)*z-beta(3)*z.^2);
         end
         %   Initial values
         pini = zeros(2+sum(model),1);
         pini(1) = beta(1);
         pos = 2;
         if model(1),
             pini(pos) = beta(2);  %p(2)
             pos = pos+1;
         end
         if model(2),
             pini(pos) = beta(3);  %p(3)
             pos = pos+1;
         end
         pini(pos)=stde;
         
         %   Parameter estimation using the maximum likelihood method
         [p,fval,exitflag,output,lambda,grad,hessian,residuo]=  OptiParamHessian (z,u,model,pini,'sinconstraints',lbopt,ubopt);
         fsigma = p(pos)*ones(size(z));
         pos = pos + 1;
         if model(3),
             fsigma = fsigma + p(pos)*z;
             pos = pos + 1;
         end
         if model(4),
             fsigma = fsigma + p(pos)*z.^2;
             pos = pos + 1;
         end
         if any(fsigma<0)
             [p,fval,exitflag,output,lambda,grad,hessian,residuo]=  OptiParamHessian (z,u,model,pini,'conconstraints',lbopt,ubopt);
         end
         
         %   LU decomposition of the Information matrix
         [LX,UX,PX] = lu(hessian);
         %   Inverse
         invI0 = (UX)\(LX\(PX*eye(length(p))));
         %    Standard deviation
         stdpara = sqrt(diag(invI0));
         %   Bounds
         upper = p+tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdpara;
         lower = p-tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdpara;
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %   Fin del ajuste
         %   Second order derivatives with respecto to data and parameters
         %       Model: fmu = p1+p2*z+p3*z^2
         %              fsigma = p4+p5*z+p6*z^2
         %   Expected values
         fmu = p(1)*ones(size(z));
         pos = 2;
         if model(1),
             fmu = fmu + p(pos)*z;
             pos = pos + 1;
         end
         if model(2),
             fmu = fmu + p(pos)*z.^2;
             pos = pos + 1;
         end
         fsigma = p(pos)*ones(size(z));
         pos = pos + 1;
         if model(3),
             fsigma = fsigma + p(pos)*z;
             pos = pos + 1;
         end
         if model(4),
             fsigma = fsigma + p(pos)*z.^2;
             pos = pos + 1;
         end
         %   Intervalos de confianza de los valores medios y de las predicciones
         aux = ones(size(z));
         if model(1),
             aux = [aux z];
         end
         if model(2),
             aux = [aux z.^2];
         end
         if sum(model(1:2))>0,
             stdmean = sqrt(sum(((aux*invI0(1:1+sum(model(1:2)),1:1+sum(model(1:2)))).*aux)')');
         else
             stdmean = sqrt(((aux*invI0(1:1+sum(model(1:2)),1:1+sum(model(1:2)))).*aux));
         end
         uppermean = fmu+tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdmean;
         lowermean = fmu-tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdmean;
         stdpredict = sqrt(fsigma.^2+stdmean.^2);
         upperpredict = fmu+tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdpredict;
         lowerpredict = fmu-tinv(1-(1-alpha)/2,length(z)-length(p)-1)*stdpredict;
         
         %   Derivatives
         Df1 = ones(size(z));
         Df2 = z;
         Df3 = z.^2;
         Ds1 = ones(n,1);
         Ds2 = z;
         Ds3 = z.^2;
         
         if ~isempty(find(fsigma<0)),
             error('La varianza de cada dato ha de ser positiva')
         end
         
         %   Second Order Cross Derivatives
         D2etay(:,1)=(Df1./(fsigma.^2));
         pos = 2;
         if model(1),
             D2etay(:,pos)=(Df2./(fsigma.^2));
             pos = pos + 1;
         end
         if model(2),
             D2etay(:,pos)=(Df3./(fsigma.^2));
             pos = pos + 1;
         end
         D2etay(:,pos)=2*(Ds1.*residuo./(fsigma.^3));
         pos = pos + 1;
         if model(3),
             D2etay(:,pos)=2*(Ds2.*residuo./(fsigma.^3));
             pos = pos + 1;
         end
         if model(4),
             D2etay(:,pos)=2*(Ds3.*residuo./(fsigma.^3));
         end
         
         % % % Dfx = p(2)*fmu./z;
         % % % Dsx = p(4)*p(5)*z.^(p(5)-1);
         % % % D2fx1 = Dfx/p(1);
         % % % D2fx2 = fmu.*(1+p(2)*log(z/z0))./z;
         % % % D2sx3 = zeros(n,1);
         % % % D2sx4 = p(5)*z.^(p(5)-1);
         % % % D2sx5 = p(4)*z.^(p(5)-1).*(1+p(5)*log(z));
         % % % D2etax(:,1)=-(Df1.*Dfx./(fsigma.^2))-2*(Df1.*Dsx.*residuo./(fsigma.^3))+(D2fx1.*residuo./(fsigma.^2));
         % % % D2etax(:,2)=-(Df2.*Dfx./(fsigma.^2))-2*(Df2.*Dsx.*residuo./(fsigma.^3))+(D2fx2.*residuo./(fsigma.^2));
         % % % D2etax(:,3)=(Ds1.*Dsx./(fsigma.^2))-(D2sx3./(fsigma))-2*(Ds1.*Dfx.*residuo./(fsigma.^3))-3*(Ds1.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx3.*residuo.^2./(fsigma.^3));
         % % % D2etax(:,4)=(Ds2.*Dsx./(fsigma.^2))-(D2sx4./(fsigma))-2*(Ds2.*Dfx.*residuo./(fsigma.^3))-3*(Ds2.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx4.*residuo.^2./(fsigma.^3));
         % % % D2etax(:,5)=(Ds3.*Dsx./(fsigma.^2))-(D2sx5./(fsigma))-2*(Ds3.*Dfx.*residuo./(fsigma.^3))-3*(Ds3.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx5.*residuo.^2./(fsigma.^3));
         
         %   Derivatives of eta (optimal parameters) with respect to u
         DetaY = (invI0*D2etay')';
         %   Derivatives of eta (optimal parameters) with respect to z
         % % % DetaX = (invI0*D2etax')';
         if ~model(1) && ~model(2),
             % % %     DmuY=sum([Df1]'.*DetaY(:,1)')';
             DmuY=0;
         elseif model(1) && ~model(2),
             DmuY=sum([Df1 Df2]'.*DetaY(:,1:2)')';
         elseif ~model(1) && model(2),
             DmuY=sum([Df1 Df3]'.*DetaY(:,1:2)')';
         else
             DmuY=sum([Df1 Df2 Df3]'.*DetaY(:,1:3)')';
         end
         % % % DsigmaY = sum([Ds1 Ds2 Ds3]'.*DetaY(:,3:5)')';
         % % % DmuX=sum([Df1 Df2]'.*DetaX(:,1:2)')';
         % % % DsigmaX = sum([Ds1 Ds2 Ds3]'.*DetaX(:,3:5)')';
         
         % % % Derivatives = [DmuY DsigmaY DmuX DsigmaX];
         
         %   Diagonal of the sensitivity matrix
         S=ones(length(z),1)-DmuY;
         %   All sensitivity matrix
         % % % Saux = eye(length(z))-[Df1 Df2]*DetaY(:,1:2)';
         
         Omega=(S).^2.*(fsigma).^2;
         
         rN=residuo./sqrt(Omega);
         
         % rNadj = mle(rN, 'dist','tlocationscale');
         % rNStudent = (rN-rNadj(1))/rNadj(2);
         % id = find(abs(rNStudent)>tinv(1-(1-alpha)/2,rNadj(3)));
         
         % rNadj = mle(rN, 'dist','tlocationscale');
         % rNStudent = (rN-rNadj(1))/rNadj(2);
         % id = find(abs(rNStudent)>tinv(1-1/(length(z)+1),rNadj(3)));
         
         
         for i=1:length(conf),
             id{i} = find(abs(rN)>norminv(1-(1-conf(i))/2,0,1));
         end
         
     end
 
 % 3. heterocedasticmodel
 
     function [fmu fsigma stdmean stdpredict boundsmean boundspredict] = heterocedasticmodel(z,p,model,hessian,alpha)
         
         if nargin<5,
             alpha = 0.9;
         end
         
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %       Model: fmu = p1+p2*z+p3*z^2
         %              fsigma = p4+p5*z+p6*z^2
         %   Expected values
         fmu = p(1)*ones(size(z));
         pos = 2;
         if model(1),
             fmu = fmu + p(pos)*z;
             pos = pos + 1;
         end
         if model(2),
             fmu = fmu + p(pos)*z.^2;
             pos = pos + 1;
         end
         
         if nargout>1,
             fsigma = p(pos)*ones(size(z));
             pos = pos + 1;
             if model(3),
                 fsigma = fsigma + p(pos)*z;
                 pos = pos + 1;
             end
             if model(4),
                 fsigma = fsigma + p(pos)*z.^2;
                 pos = pos + 1;
             end
         end
         if nargout >2,
             %   LU decomposition of the Information matrix
             [LX,UX,PX] = lu(hessian);
             %   Inverse
             invI0 = (UX)\(LX\(PX*eye(length(p))));
             %    Standard deviation
             stdpara = sqrt(diag(invI0));
             %   Bounds
             upper = p+norminv(1-(1-alpha)/2)*stdpara;
             lower = p-norminv(1-(1-alpha)/2)*stdpara;
             %   Intervalos de confianza de los valores medios y de las predicciones
             aux = ones(size(z));
             if model(1),
                 aux = [aux z];
             end
             if model(2),
                 aux = [aux z.^2];
             end
             if sum(model(1:2))>0,
                 stdmean = sqrt(sum(((aux*invI0(1:1+sum(model(1:2)),1:1+sum(model(1:2)))).*aux)')');
             else
                 stdmean = sqrt(((aux*invI0(1:1+sum(model(1:2)),1:1+sum(model(1:2)))).*aux));
             end
             boundsmean = fmu-norminv(1-(1-alpha)/2)*stdmean;
             boundsmean = [boundsmean fmu+norminv(1-(1-alpha)/2)*stdmean];
             stdpredict = sqrt(fsigma.^2+stdmean.^2);
             boundspredict = fmu-norminv(1-(1-alpha)/2)*stdpredict;
             boundspredict = [boundspredict fmu+norminv(1-(1-alpha)/2)*stdpredict];
         end
     end
 
 % 4. edgewothExpansion
 
     function [pdf, cdf] = edgewothExpansion(x,mu,sigma,skewness,kurtosis)
         
         zu = (x-mu)/sigma;
         
         H2 = zu.^2-1;
         H3 = zu.^3-3*zu;
         H4 = zu.^4-6*zu.^2+3;
         H5 = zu.^5-10*zu.^3+15*zu;
         H6 = zu.^6-15*zu.^4+45*zu.^2-15;
         
         % dH2 = 2.*zu;
         % dH3 = 3*zu.^2-3;
         % dH5 = 5*zu.^4-30*zu.^2+15;
         
         auxpdf = normpdf(zu,0,1);
         
         % pdof0 = auxpdf.*(1+skewness*(zu.*H2-dH2)/6+(kurtosis-3)*(zu.*H3-dH3)/24+...
         %     skewness^2*(zu.*H5-dH5)/72);
         
         pdf = auxpdf.*(1+skewness*H3/6+(kurtosis-3)*H4/24+...
             skewness^2*H6/72)/sigma;
         
         if nargout >1,
             cdf = normcdf(zu,0,1)-auxpdf.*(skewness*H2/6+(kurtosis-3)*H3/24+...
                 skewness^2*H5/72);
         end
         
     end
 
 % 5. rosenblattBrunn
 
     function x = rosenblattBrunn (z,parameters)
         
         %   Calculo Cholesky para independizar las variables
         mu = parameters.mu;
         varcov = parameters.varcov;
         
         x = mu+z.*varcov;
     end
 % 6. optiparaHessian
 
     function [p,f,exitflag,output,lambda,grad,hessian,residuo] =  OptiParamHessian (z,u,bin,pini,type,lb,ub)
         
         %   Initial bounds and possible initial values for the constant parameters
         if ~isempty(pini),
             p = pini;
         else
             p = zeros(2+sum(bin),1);
         end
         
         if isempty(type)
             type = 'sinconstraints';
         end
         
         if nargin<6 || isempty(lb),
             lb = -Inf*ones(length(p),1);
         end
         if nargin<7 || isempty(lb),
             ub = Inf*ones(length(p),1);
         end
         
         % options = optimset('GradObj','off','Hessian','off','TolFun',1e-12,'MaxFunEvals',1000,'MaxIter',1000);
         % [paaa,faaa,exitflagaaa,outputaaa,lambdaaaa,gradaaa,hessianaaa] = fmincon(@(p) loglikelihood (p,z,u,ID),p,[],[],[],[],lb,up,[],options);
         
         %   Set the options for the optimization routine, note that Gradients are
         %   provided but not Hessian
         %   Call the optimization routine
         %   Note that it is a minimization problem, instead a maximization problem,
         %   for this reason the log likelihood function sign is changed
         % % % options = optimset('GradObj','off','Hessian','off','TolFun',1e-12,'MaxFunEvals',1000,'MaxIter',1000);
         % % % [p,f,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,z,u),p,[],[],[],[],lb,up,[],options);
         options = optimset('GradObj','on','Hessian','on','TolFun',1e-8,'TolCon',1e-8,'TolX',1e-8,'MaxFunEvals',1000000,'MaxIter',10000);
         aux = -[zeros(length(z),1+sum(bin(1:2)))  ones(size(z))];
         if bin(3),
             aux = [aux -z];
         end
         if bin(4),
             aux = [aux -z.^2];
         end
         
         if strcmp(type,'sinconstraints')
             if bin(2),
                 
             end
             if bin(4),
                 
             end
             [p,f,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,z,u,bin),p,[],[],[],[],lb,ub,[],options);
         elseif strcmp(type,'conconstraints')
             [p,f,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,z,u,bin),p,aux,zeros(size(z)),[],[],lb,ub,[],options);
         else
             error('El tipo de modelo ha de ser "sinconstraints" o "conconstraints"')
         end
         [f grad hessian residuo] = loglikelihood (p,z,u,bin);
         
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %   Loglikelihood function definition of the nonlinear regression with
         %   variance being a function of z (HETEROCEDASTIC)
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         
         function [f Jx Hxx residuo] = loglikelihood (p,z,u,bin)
             %   bin is a binary vector which establishes the kind of model
             %   This function calculates the loglikelihood function with the
             %   sign changed for the values of the paremeters given by p
             n = max(size(u));
             %   It Calculates the gradients of each parameter p
             fmu = p(1)*ones(size(z));
             pos = 2;
             if bin(1),
                 fmu = fmu + p(pos)*z;
                 pos = pos + 1;
             end
             if bin(2),
                 fmu = fmu + p(pos)*z.^2;
                 pos = pos + 1;
             end
             fsigma = p(pos)*ones(size(z));
             pos = pos + 1;
             if bin(3),
                 fsigma = fsigma + p(pos)*z;
                 pos = pos + 1;
             end
             if bin(4),
                 fsigma = fsigma + p(pos)*z.^2;
                 pos = pos + 1;
             end
             
             %   Evaluate the loglikelihood function
             f =  -0.5*sum(log(2*pi*fsigma.^2))-0.5*sum(((u-fmu)./fsigma).^2);
             %   Convert a maximization problem into a minimization problem
             f=-f;
             
             %   Jacobian of the log-likelihood function
             if nargout >1,
                 Jx = zeros(2+sum(bin),1);
                 
                 %       Model: fmu = p1+p2*z+p3*z^2
                 %              fsigma = p4+p5*z+p6*z^2
                 Df1 = ones(size(z));
                 Df2 = z;
                 Df3 = z.^2;
                 Ds1 = ones(n,1);
                 Ds2 = z;
                 Ds3 = z.^2;
                 
                 Jx(1)=sum(Df1.*(u-fmu)./fsigma.^2);  %p(1)
                 pos = 2;
                 if bin(1),
                     Jx(pos)=sum(Df2.*(u-fmu)./fsigma.^2);  %p(2)
                     pos = pos+1;
                 end
                 if bin(2),
                     Jx(pos)=sum(Df3.*(u-fmu)./fsigma.^2);  %p(3)
                     pos = pos+1;
                 end
                 Jx(pos)=-sum(Ds1./fsigma)+sum(Ds1.*(u-fmu).^2./fsigma.^3);  %p(4)
                 pos = pos+1;
                 if bin(3),
                     Jx(pos)=-sum(Ds2./fsigma)+sum(Ds2.*(u-fmu).^2./fsigma.^3);  %p(5)
                     pos = pos+1;
                 end
                 if bin(4),
                     Jx(pos)=-sum(Ds3./fsigma)+sum(Ds3.*(u-fmu).^2./fsigma.^3);  %p(6)
                 end
                 Jx=-Jx;
             end
             
             %   Hessian of the log-likelihood function
             if nargout >2,
                 Hxx = zeros(6);
                 %       Model: fmu = p1+p2*z+p3*z^2
                 %              fsigma = p4+p5*z+p6*z^2
                 %   Para este modelo todas las derivadas son nulas
                 D2f11 = zeros(n,1);
                 D2f22 = zeros(n,1);
                 D2f33 = zeros(n,1);
                 D2f12 = zeros(n,1);
                 D2f13 = zeros(n,1);
                 D2f23 = zeros(n,1);
                 D2s11 = zeros(n,1);
                 D2s22 = zeros(n,1);
                 D2s33 = zeros(n,1);
                 D2s12 = zeros(n,1);
                 D2s13 = zeros(n,1);
                 D2s23 = zeros(n,1);
                 
                 Hxx(1,1) = sum(1./fsigma.^2.*((u-fmu).* D2f11 - Df1.^2));
                 Hxx(2,2) = sum(1./fsigma.^2.*((u-fmu).* D2f22 - Df2.^2));
                 Hxx(3,3) = sum(1./fsigma.^2.*((u-fmu).* D2f33 - Df3.^2));
                 Hxx(1,2) = sum(1./fsigma.^2.*((u-fmu).* D2f12 - Df1.*Df2));
                 Hxx(1,3) = sum(1./fsigma.^2.*((u-fmu).* D2f13 - Df1.*Df3));
                 Hxx(2,3) = sum(1./fsigma.^2.*((u-fmu).* D2f23 - Df2.*Df3));
                 Hxx(4,4) = -sum(1./fsigma.^2.*(fsigma.* D2s11 - Ds1.^2))+sum((u-fmu).^2./fsigma.^3.*(D2s11 - 3./fsigma.* Ds1.^2));
                 Hxx(5,5) = -sum(1./fsigma.^2.*(fsigma.* D2s22 - Ds2.^2))+sum((u-fmu).^2./fsigma.^3.*(D2s22 - 3./fsigma.* Ds2.^2));
                 Hxx(6,6) = -sum(1./fsigma.^2.*(fsigma.* D2s33 - Ds3.^2))+sum((u-fmu).^2./fsigma.^3.*(D2s33 - 3./fsigma.* Ds3.^2));
                 Hxx(4,5) = -sum(1./fsigma.^2.*(fsigma.* D2s12 - Ds1.*Ds2))+sum((u-fmu).^2./fsigma.^3.*(D2s12 - 3./fsigma.* Ds1.*Ds2));
                 Hxx(4,6) = -sum(1./fsigma.^2.*(fsigma.* D2s13 - Ds1.*Ds3))+sum((u-fmu).^2./fsigma.^3.*(D2s13 - 3./fsigma.* Ds1.*Ds3));
                 Hxx(5,6) = -sum(1./fsigma.^2.*(fsigma.* D2s23 - Ds2.*Ds3))+sum((u-fmu).^2./fsigma.^3.*(D2s23 - 3./fsigma.* Ds2.*Ds3));
                 Hxx(1,4) = -2*sum((u-fmu)./fsigma.^3.*Df1.*Ds1);
                 Hxx(1,5) = -2*sum((u-fmu)./fsigma.^3.*Df1.*Ds2);
                 Hxx(1,6) = -2*sum((u-fmu)./fsigma.^3.*Df1.*Ds3);
                 Hxx(2,4) = -2*sum((u-fmu)./fsigma.^3.*Df2.*Ds1);
                 Hxx(2,5) = -2*sum((u-fmu)./fsigma.^3.*Df2.*Ds2);
                 Hxx(2,6) = -2*sum((u-fmu)./fsigma.^3.*Df2.*Ds3);
                 Hxx(3,4) = -2*sum((u-fmu)./fsigma.^3.*Df3.*Ds1);
                 Hxx(3,5) = -2*sum((u-fmu)./fsigma.^3.*Df3.*Ds2);
                 Hxx(3,6) = -2*sum((u-fmu)./fsigma.^3.*Df3.*Ds3);
                 Hxx=-(Hxx+triu(Hxx,1)');
                 auxHxx = zeros(2+sum(bin));
                 pos = [1 bin(1) bin(2) 1 bin(3) bin(4)];
                 pos1 = 1;
                 for ii = 1:6,
                     if pos(ii),
                         pos2 = 1;
                         for jj = 1:6,
                             if pos(jj),
                                 auxHxx(pos1,pos2) = Hxx(ii,jj);
                                 pos2 = pos2 + 1;
                             end
                             
                             
                         end
                         pos1 = pos1 + 1;
                     end
                 end
                 Hxx = auxHxx;
                 
             end
             
             if nargout>3,
                 residuo = (u-fmu);
             end
             
         end
     end
 
 % 7. loglikelihood
 
     function [f Jx Hxx residuo] = loglikelihood (p,z,u,bin)
         %   bin is a binary vector which establishes the kind of model
         %   This function calculates the loglikelihood function with the
         %   sign changed for the values of the paremeters given by p
         n = max(size(u));
         %   It Calculates the gradients of each parameter p
         fmu = p(1)*ones(size(z));
         pos = 2;
         if bin(1),
             fmu = fmu + p(pos)*z;
             pos = pos + 1;
         end
         if bin(2),
             fmu = fmu + p(pos)*z.^2;
             pos = pos + 1;
         end
         fsigma = p(pos)*ones(size(z));
         pos = pos + 1;
         if bin(3),
             fsigma = fsigma + p(pos)*z;
             pos = pos + 1;
         end
         if bin(4),
             fsigma = fsigma + p(pos)*z.^2;
             pos = pos + 1;
         end
         
         %   Evaluate the loglikelihood function
         f =  -0.5*sum(log(2*pi*fsigma.^2))-0.5*sum(((u-fmu)./fsigma).^2);
         %   Convert a maximization problem into a minimization problem
         f=-f;
         
         %   Jacobian of the log-likelihood function
         if nargout >1,
             Jx = zeros(2+sum(bin),1);
             
             %       Model: fmu = p1+p2*z+p3*z^2
             %              fsigma = p4+p5*z+p6*z^2
             Df1 = ones(size(z));
             Df2 = z;
             Df3 = z.^2;
             Ds1 = ones(n,1);
             Ds2 = z;
             Ds3 = z.^2;
             
             Jx(1)=sum(Df1.*(u-fmu)./fsigma.^2);  %p(1)
             pos = 2;
             if bin(1),
                 Jx(pos)=sum(Df2.*(u-fmu)./fsigma.^2);  %p(2)
                 pos = pos+1;
             end
             if bin(2),
                 Jx(pos)=sum(Df3.*(u-fmu)./fsigma.^2);  %p(3)
                 pos = pos+1;
             end
             Jx(pos)=-sum(Ds1./fsigma)+sum(Ds1.*(u-fmu).^2./fsigma.^3);  %p(4)
             pos = pos+1;
             if bin(3),
                 Jx(pos)=-sum(Ds2./fsigma)+sum(Ds2.*(u-fmu).^2./fsigma.^3);  %p(5)
                 pos = pos+1;
             end
             if bin(4),
                 Jx(pos)=-sum(Ds3./fsigma)+sum(Ds3.*(u-fmu).^2./fsigma.^3);  %p(6)
             end
             Jx=-Jx;
         end
         
         %   Hessian of the log-likelihood function
         if nargout >2,
             Hxx = zeros(6);
             %       Model: fmu = p1+p2*z+p3*z^2
             %              fsigma = p4+p5*z+p6*z^2
             %   Para este modelo todas las derivadas son nulas
             D2f11 = zeros(n,1);
             D2f22 = zeros(n,1);
             D2f33 = zeros(n,1);
             D2f12 = zeros(n,1);
             D2f13 = zeros(n,1);
             D2f23 = zeros(n,1);
             D2s11 = zeros(n,1);
             D2s22 = zeros(n,1);
             D2s33 = zeros(n,1);
             D2s12 = zeros(n,1);
             D2s13 = zeros(n,1);
             D2s23 = zeros(n,1);
             
             Hxx(1,1) = sum(1./fsigma.^2.*((u-fmu).* D2f11 - Df1.^2));
             Hxx(2,2) = sum(1./fsigma.^2.*((u-fmu).* D2f22 - Df2.^2));
             Hxx(3,3) = sum(1./fsigma.^2.*((u-fmu).* D2f33 - Df3.^2));
             Hxx(1,2) = sum(1./fsigma.^2.*((u-fmu).* D2f12 - Df1.*Df2));
             Hxx(1,3) = sum(1./fsigma.^2.*((u-fmu).* D2f13 - Df1.*Df3));
             Hxx(2,3) = sum(1./fsigma.^2.*((u-fmu).* D2f23 - Df2.*Df3));
             Hxx(4,4) = -sum(1./fsigma.^2.*(fsigma.* D2s11 - Ds1.^2))+sum((u-fmu).^2./fsigma.^3.*(D2s11 - 3./fsigma.* Ds1.^2));
             Hxx(5,5) = -sum(1./fsigma.^2.*(fsigma.* D2s22 - Ds2.^2))+sum((u-fmu).^2./fsigma.^3.*(D2s22 - 3./fsigma.* Ds2.^2));
             Hxx(6,6) = -sum(1./fsigma.^2.*(fsigma.* D2s33 - Ds3.^2))+sum((u-fmu).^2./fsigma.^3.*(D2s33 - 3./fsigma.* Ds3.^2));
             Hxx(4,5) = -sum(1./fsigma.^2.*(fsigma.* D2s12 - Ds1.*Ds2))+sum((u-fmu).^2./fsigma.^3.*(D2s12 - 3./fsigma.* Ds1.*Ds2));
             Hxx(4,6) = -sum(1./fsigma.^2.*(fsigma.* D2s13 - Ds1.*Ds3))+sum((u-fmu).^2./fsigma.^3.*(D2s13 - 3./fsigma.* Ds1.*Ds3));
             Hxx(5,6) = -sum(1./fsigma.^2.*(fsigma.* D2s23 - Ds2.*Ds3))+sum((u-fmu).^2./fsigma.^3.*(D2s23 - 3./fsigma.* Ds2.*Ds3));
             Hxx(1,4) = -2*sum((u-fmu)./fsigma.^3.*Df1.*Ds1);
             Hxx(1,5) = -2*sum((u-fmu)./fsigma.^3.*Df1.*Ds2);
             Hxx(1,6) = -2*sum((u-fmu)./fsigma.^3.*Df1.*Ds3);
             Hxx(2,4) = -2*sum((u-fmu)./fsigma.^3.*Df2.*Ds1);
             Hxx(2,5) = -2*sum((u-fmu)./fsigma.^3.*Df2.*Ds2);
             Hxx(2,6) = -2*sum((u-fmu)./fsigma.^3.*Df2.*Ds3);
             Hxx(3,4) = -2*sum((u-fmu)./fsigma.^3.*Df3.*Ds1);
             Hxx(3,5) = -2*sum((u-fmu)./fsigma.^3.*Df3.*Ds2);
             Hxx(3,6) = -2*sum((u-fmu)./fsigma.^3.*Df3.*Ds3);
             Hxx=-(Hxx+triu(Hxx,1)');
             auxHxx = zeros(2+sum(bin));
             pos = [1 bin(1) bin(2) 1 bin(3) bin(4)];
             pos1 = 1;
             for ii = 1:6,
                 if pos(ii),
                     pos2 = 1;
                     for jj = 1:6,
                         if pos(jj),
                             auxHxx(pos1,pos2) = Hxx(ii,jj);
                             pos2 = pos2 + 1;
                         end
                         
                         
                     end
                     pos1 = pos1 + 1;
                 end
             end
             Hxx = auxHxx;
             
         end
         
         if nargout>3,
             residuo = (u-fmu);
         end
         
     end
 
 % 8. funcionGIRO
 
     function y = funcionGiro (x,L)

         %   x(1)->Giro del flujo medio
        
         y=(L/2)*tand(x(1));

         
     end
 
 end
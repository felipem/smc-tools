function criterioRotura=breakingCriteriaForm(hasCurrents)
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    criterioRotura=[];
    f = dialog(  'Name', txt.post('wBreakingCriteriaTitle'), 'Position',[100 100 400 180],'Visible','off','CloseRequestFcn',@closeForm);
    %chPanel=uipanel('Parent',f,'units','pixels,'Position'
    uicontrol(f,'Style','pushbutton','String', txt.post('wMapPeriodAcept'),'Position',[200 10 80 30],'Callback',@ok);
    uicontrol(f,'Style','pushbutton','String', txt.post('wMapPeriodCancel'),'Position',[300 10 80 30],'Callback',@closeForm);

    bg=uibuttongroup('Parent',f,'units','pixel','Position',[0 50 400 130]);
    enabled='on';
    if ~hasCurrents,enabled='off';end
    rbVelMax=uicontrol(bg,'Style','radiobutton','Value',hasCurrents,'Enable',enabled,'String',txt.post('wBreakingCriteriaVelMax'),'Position',[10 80 200 30]);
    rbQb=uicontrol(bg,'Style','radiobutton','Value',~hasCurrents,'Enable','on','String',txt.post('wBreakingCriteriaQb'),'Position',[10 30 200 30]);
    %%Esta deshabilitado el criterio Qb
    
    
    set(f,'Visible','on');
    uiwait(f);
    
    function closeForm(varargin)        
        delete(gcbf);
    end

    function ok(varargin)
        if get(rbVelMax,'Value')            
            criterioRotura='velMax';          
        elseif get(rbQb,'Value')            
            criterioRotura='qb';
        end
        delete(gcbf);
    end 
end
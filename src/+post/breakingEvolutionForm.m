function breakingEvolutionForm(mopla,perfil)    

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    enabled=true;
    scrPos=get(0,'ScreenSize');
    width=800;
    height=700;
    
        f = figure( 'MenuBar', 'none', 'Name', [txt.post('wBreakingEvoTitle'),' ',perfil.id],...
         'NumberTitle', 'off', 'Toolbar', 'none','ResizeFcn',@resize,...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'tag',['smcWinBreakingEvo_',perfil.id]);
    %set(f, 'resize', 'off');
    color=get(f,'Color');
    
    
    pos=get(f,'Position');
    axPanel=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    
    
    tb=uitoolbar(f);
    core.navigateToolbar(tb,axPanel);
    
    uicontrol(f,'Style','pushbutton','String', txt.post('wProfileNewAcept'),'Position',[30 10 60 20],'Callback',@ok);    
    
     uicontrol(f,'Style','text','String',txt.post('wBreakingEvoTide'),...
        'Position',[120 6 60 20],'BackgroundColor',color);
    cMarea=uicontrol(f,'Style','popup','String',num2str(mopla.tide,'%.2f'),'Position',[180,10,60,20]...
        ,'BackgroundColor','white','Callback',@refreshGraph);
    
      uicontrol(f,'Style','text','String',txt.post('wBreakingEvoCase'),...
        'Position',[270 6 60 20],'BackgroundColor',color);
    cCaso=uicontrol(f,'Style','popup','String',1:size(perfil.hs,2),'Position',[330,10,60,20]...
        ,'BackgroundColor','white','Callback',@refreshGraph);
   
    x=sqrt((perfil.x-perfil.x(1)).^2+(perfil.y-perfil.y(1)).^2);
    %refreshGraph;    
    
     ax1=subplot(3,1,1,'Parent',axPanel);
     ax2=subplot(3,1,2,'Parent',axPanel);
     ax3=subplot(3,1,3,'Parent',axPanel);   
    
    
        h=perfil.h+mopla.tide(1); %Sumo la marea a la profundidad!!
        hs=squeeze(perfil.hs(1,1,:));
        dir_north=squeeze(perfil.dir_north(1,1,:));
        u=squeeze(perfil.uPerfil(1,1,:));    
        qb=squeeze(perfil.qb(1,1,:));    
        h_b=perfil.h_b(1,1);
        dir_b_north=perfil.dir_b_north(1,1);
        x_b=sqrt((perfil.x_b(1,1)-perfil.x(1)).^2 + (perfil.y_b(1,1)-perfil.y(1)).^2);
        hs_b=perfil.hs_b(1,1);    
        
        if ~isfield(perfil,'criterioRotura')
            perfil.criterioRotura='velMax';
        end
        
        
        if strcmp(perfil.criterioRotura,'velMax')
            brkVar=u*100;
            leg='Vel (cm/s)';
            brkValue=max(u)*100;
        else
            brkVar=qb*100;
            leg='Qb (%)';            
            brkValue=max(brkVar);
            if brkValue>=10 %%En el caso que se alcance el valor de un 10% de olas rotas
                brkValue=10;
            end
        end
        
        [ax1,plotHs,plotBrk]=plotyy(x,hs,x,brkVar,'Parent',ax1);
        set(plotHs,'LineWidth',1.5);
        set(plotBrk,'LineWidth',1.5);
        
        hold(ax1(1),'on');    
        hold(ax1(2),'on');
        plotBrkPoint1=plot(ax1(1),x_b,hs_b,'ok','MarkerFaceColor','k','MarkerSize',8);        
        plotBrkLine=plot(ax1(2),[x_b,x_b],[0,brkValue],'--k');
        legend(ax1(1),'Hs (m)', ['Hs_b (m) = ',num2str(hs_b)],leg);
        title(ax1(1),txt.post('wBreakingEvoPlot1Title'));
        ylabel(ax1(1),'H_s (m)');
        ylabel(ax1(2),leg);
        
        plotDir=plot(ax2,x,dir_north,'r','LineWidth',1.5);
        hold(ax2,'on');    
        plotBrkPoint2=plot(ax2,x_b,dir_b_north,'ok','MarkerFaceColor','k','MarkerSize',8);  
        legend(ax2,'\thetam (� N)', ['\thetam_b (� N) = ',num2str(dir_b_north)]);
        title(ax2,txt.post('wBreakingEvoPlot2Title'));
        
        ylabel(ax2,'\theta_m (� N)');

        
        plotH=plot(ax3,x,h,'Color',[0.4510,0.2627,0.2627],'LineWidth',1.5);
        hold(ax3,'on');    
        plotBrkPoint3=plot(ax3,x_b,h_b,'ok','MarkerFaceColor','k','MarkerSize',8);  
        legend(ax3,'h (m)', ['h_b (m) = ',num2str(h_b)]);
        title(ax3,txt.post('wBreakingEvoPlot3Title'));
        set(ax3,'YDir','Reverse');
        xlabel(ax3,'x (m)');
        ylabel(ax3,'h (m)');
    
    
    function ok(varargin)
        delete(f);
    end
    function refreshGraph(varargin)

        mareaIdx=get(cMarea,'Value');
        casoIdx=get(cCaso,'Value');
        h=perfil.h+mopla.tide(mareaIdx); %Sumo la marea a la profundidad!!
        hs=squeeze(perfil.hs(mareaIdx,casoIdx,:));
        dir_north=squeeze(perfil.dir_north(mareaIdx,casoIdx,:));
        u=squeeze(perfil.uPerfil(mareaIdx,casoIdx,:));    
        qb=squeeze(perfil.qb(mareaIdx,casoIdx,:));    
        h_b=perfil.h_b(mareaIdx,casoIdx);
        dir_b_north=perfil.dir_b_north(mareaIdx,casoIdx);
        x_b=sqrt((perfil.x_b(mareaIdx,casoIdx)-perfil.x(1)).^2 + (perfil.y_b(mareaIdx,casoIdx)-perfil.y(1)).^2);
        hs_b=perfil.hs_b(mareaIdx,casoIdx);    
        
        
        if strcmp(perfil.criterioRotura,'velMax')
            brkVar=u*100;
            brkValue=max(u)*100;
        else
            brkVar=qb*100;
            brkValue=max(brkVar);
            if brkValue>=10 %%En el caso que se alcance el valor de un 10% de olas rotas
                brkValue=10;
            end
        end

        set(plotHs,'YData',hs);
        set(plotBrk,'YData',brkVar);
        set(plotBrkPoint1,'XData',x_b,'YData',hs_b);
        set(plotBrkLine,'XData',[x_b,x_b],'YData',[0,brkValue]);

        set(plotDir,'YData',dir_north);
        set(plotBrkPoint2,'XData',x_b,'YData',dir_b_north);
        
        set(plotH,'YData',h);
        set(plotBrkPoint3,'XData',x_b,'YData',h_b);       
        
        %yLimMode(ax1(1),'auto');        
        %yTickMode(ax1(1),'auto');        
        set(ax1(1),'YLimMode','auto','YTickMode','auto');
        set(ax1(2),'YLimMode','auto','YTickMode','auto');        
        %ytick(ax1(1),'auto');                
        %ytick(ax1(2),'auto');        
        set(ax2,'YLimMode','auto','YTickMode','auto');
        set(ax3,'YLimMode','auto','YTickMode','auto');
        legend(ax1(1),'Hs (m)', ['Hs_b (m) = ',num2str(hs_b)],leg);
        legend(ax2,'\thetam (� N)', ['\thetam_b (� N) = ',num2str(dir_b_north)]);
        legend(ax3,'h (m)', ['h_b (m) = ',num2str(h_b)]);
        

    end 

  
    function resize(varargin)
         pos=get(f,'Position');
        set(axPanel,'position',[0 40 pos(3) pos(4)-40]);
    end
end


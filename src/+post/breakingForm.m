function perfiles=breakingForm(mopla,project,alternativa)
    core.blockMainForm;
    d=dialog('Visible','off','CloseRequestFcn',@quit);
    txt=util.loadLanguage;    
    
    bCalculate=uicontrol(d,'String',txt.post('tbBreaking'),'Style','pushbutton','Callback',@calcularRotura,...
        'Position',[10,5,80,20]);
    bQuit=uicontrol(d,'String',txt.post('wPoisFemQuit'),'Style','pushbutton','Callback',@quit,...
        'Enable','off','Position',[110,5,80,20]);
    
    pos=get(d,'Position');
    table=uitable(d,'Position',[0,35,pos(3),pos(4)-35],'ColumnName',{'Id',txt.post('wBreakingFormCol')});
    
    profilesToRebuild={};
    perfiles=[];
    profiles=keys(mopla.perfiles);
    moplaDir=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId);
             
    core.blockMainForm;
    setTable();
    
     if isempty(profilesToRebuild);
        set(bCalculate,'Enable','off');
     end    
     set(d,'Visible','on');     
     uiwait(d);
    
    function setTable()
        dataTbl={};
        profilesToRebuild={};
        perfiles=[];
        for i=1:length(profiles)
            file=fullfile(moplaDir,[profiles{i},'_prf.mat']);
            val='True';
            if ~exist(file,'file')                  
                profilesToRebuild=cat(1,profilesToRebuild,profiles{i});
                val=colText('False','red');
            else
                try
                    perfil=load(file,'-mat');
                    perfil=perfil.perfil;
                    perfiles=cat(1,perfil,perfiles,perfil);
                catch ex
                    msgbox({ex.message},'Error','error');   
                    return;                    
                end
            end
            dataTbl=cat(1,dataTbl,{profiles{i},val});
        end
        set(table,'Data',dataTbl);    
        set(table,'ColumnWidth','auto');
        if isempty(profilesToRebuild);
            set(bCalculate,'Enable','off');
        end  
        set(bQuit,'Enable','on');
    end

    function outHtml = colText(inText, inColor)
          % return a HTML string with colored font
        outHtml = ['<html><font color="', ...
        inColor, ...
        '">', ...
        inText, ...
        '</font></html>'];
    end

    function calcularRotura(varargin)      
        criterioRotura='velMax';
        post.calculaRotura(mopla,project,alternativa,criterioRotura,profilesToRebuild);
        setTable();
    end

    function quit(varargin)        
        delete(d);
        pause(0.15);
    end

end
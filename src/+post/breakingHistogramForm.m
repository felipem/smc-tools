function breakingHistogramForm(project,alternativa,mopla,perfilId)

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    scrPos=get(0,'ScreenSize');
    width=800;
    height=500;
    
    file=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId,[perfilId,'_prf.mat']);
    if ~exist(file,'file'),return;end
    perfil=load(file,'-mat');
    perfil=perfil.perfil;
    
    f = figure( 'MenuBar', 'none', 'Name', [txt.post('wBreakingHistogramTitle'),' [',perfil.id,']'],...
         'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]);
    set(f, 'resize', 'off');
    color=get(f,'Color');
    
    pos=get(f,'Position');
    pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    uicontrol(f,'Style','pushbutton','String', txt.post('wProfileNewAcept'),'Position',[30 10 60 20],'Callback',@ok);    
    
     uicontrol(f,'Style','text','String',txt.post('wPoiResultsTide'),...
        'Position',[120 6 60 20],'BackgroundColor',color);
    cMarea=uicontrol(f,'Style','popup','String',num2str(mopla.tide,'%.2f'),'Position',[180,10,60,20]...
        ,'BackgroundColor','white','Callback',@updateGraph);
    
    l=sqrt((perfil.x-perfil.x(1)).^2+(perfil.y-perfil.y(1)).^2);
    h=-perfil.h;
    
    [x,y,~,hist_b_tide]=post.histogramaRotura(mopla,perfil);
    lProb=sqrt((x-x(1)).^2+(y-y(1)).^2);
    
    polX=[0,l,l(end)];
    polY=[min(h),h,min(h)];
    polSeaX=[0,0,l(end),l(end)];
    polSeaY=[min(h),0,0,min(h)];
    
    axBeach=axes('Parent',pMain);
    sandyBrown=[244 164 96]/255;
    seaGreen=[32 178 170]/255;
    sea=fill(polSeaX,polSeaY,seaGreen,'Parent',axBeach,'EdgeColor','none');
    hold(axBeach,'on');
    beach=fill(polX,polY,sandyBrown,'Parent',axBeach,'EdgeColor','none');
    ylim=get(axBeach,'ylim');
    yLim=ylim(2);
    set(axBeach,'ylim',[min(h),yLim]);
    set(axBeach,'xlim',[0,l(end)]);
    hold(axBeach,'off');
    xlabel(axBeach, 'x (m)');
    ylabel(axBeach, 'h (m)');
    set(axBeach,'box','off');
    
    axProb=axes('Parent',pMain,'Position',get(axBeach,'Position'));
    
    probPlot=plot(axProb,lProb,hist_b_tide(1,:)*100,'--k','LineWidth',1.5);
    set(axProb,'Color','none');        
    set(axProb,'ylim',[0,max(hist_b_tide(1,:))*100]);
    set(axProb,'XTick',[],'YAxisLocation','right');
    ylabel(axProb,'Prob. %');
    set(axProb,'box','off');
    updateGraph;
    
    
    function ok(varargin)
        delete(f);
    end
    
    function updateGraph(varargin)
        value=get(cMarea,'Value');
        tide=mopla.tide(value);
        polY=[min(h)-tide,h-tide,min(h)-tide];
        set(beach,'YData',polY);
        polSeaY=[min(h)-tide,0,0,min(h)-tide];
        set(sea,'YData',polSeaY);
        set(axBeach,'ylim',[min(h)-tide,yLim-tide]);        
        set(probPlot,'YData',hist_b_tide(value,:)*100);
        set(axProb,'ylim',[0,max(hist_b_tide(value,:))*100]);
    end
end


function qb=calculaQb(malla,perfil,qbFile)
    %folder='/home/nabil/Proyectos/SMC/Prueba_tte_SMC3/Alternativa 2/Mopla/';

    %malla=smc.loadMalla('/home/nabil/Proyectos/SMC/Prueba_tte_SMC3/Alternativa 1/Mopla/D100000000REF2.DAT');

    %qbFile='/home/nabil/Proyectos/SMC/Prueba_tte_SMC3/Alternativa 1/Mopla/SP/D100000000mopla10001_qb.dat';
    %xFile='/home/nabil/Proyectos/SMC/Prueba_tte_SMC3/Alternativa 2/Mopla/SP/M4000000000000000002__x.dat';
    xFile=regexprep(qbFile,'_qb','__x');

    x=load(xFile);

    fid=fopen(qbFile,'r');
    text=fread(fid,inf,'*char');
    fclose(fid);

    data=regexp(text','#','split');

    num=textscan(data{1},'%f');
    qb=zeros(length(x),length(num{1}));
    qb(1,:)=num{1};

    for i=2:length(x)
        num=textscan(data{i},'%f');
        qb(i,:)=num{1};
    end
    qb=qb';
    y=linspace(0,malla.y,length(num{1}));
    [X_,Y_]=meshgrid(x,y);
    X=malla.xIni + X_*cosd(malla.angle)-Y_*sind(malla.angle);
    Y=malla.yIni + X_*sind(malla.angle)+Y_*cosd(malla.angle);
    x=reshape(X,size(qb,1)*size(qb,2),1);
    y=reshape(Y,size(qb,1)*size(qb,2),1);
    qb=reshape(qb,size(qb,1)*size(qb,2),1);
    
    d=2*max(malla.dx,malla.dy);
    
     xIni=min(perfil.x);xEnd=max(perfil.x);yIni=min(perfil.y);yEnd=max(perfil.y);
     xIni=xIni-d;
     xEnd=xEnd+d;
     yIni=yIni-d;
     yEnd=yEnd+d;

     in=inpolygon(x,y,[xIni,xIni,xEnd,xEnd],[yIni,yEnd,yEnd,yEnd]);
     x=x(in);
     y=y(in);
     qb=qb(in);
     
    
    
    F=TriScatteredInterp(x,y,qb);
    qb=F(perfil.x,perfil.y);
    
%     perfiles_qb=[];
%     for i=1:length(perfiles)
%         perfiles_qb(i).qb=F(perfiles(i).x,perfiles(i).y);
%     end
    

    %pcolor(X,Y,qb);colorbar;shading interp
end





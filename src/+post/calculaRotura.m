function perfiles = calculaRotura(mopla,project,alternativa,criterioRotura,perfilesId)    
    core.blockMainForm();

    %%%%%%%%%%%%%%%%%%%%%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    casosMD=mopla.casosMD(logical(mopla.propagate));
    mallas=mopla.mallas(logical(mopla.propagate));
    hs_prop=mopla.hs(casosMD);
    h=waitbar(0,txt.post('pdBreaking'),'WindowStyle','Modal');
    %set(h,'CloseRequestFcn',@close);
    folder=fullfile(project.folder,alternativa.nombre,'Mopla','SP');    
    if ~exist('perfilesId','var')
        perfilesId=keys(mopla.perfiles);
    end
    perfiles=[];
    for i=1:length(perfilesId)
        p=mopla.perfiles(perfilesId{i});
        perfil.id=perfilesId{i};        
        perfil.hs=zeros(length(mopla.tide),length(mallas),length(p.x));
        perfil.qb=zeros(length(mopla.tide),length(mallas),length(p.x));
        perfil.dir=perfil.hs;
        perfil.dir_north=perfil.hs;
        perfil.uPerfil=perfil.hs;        
        perfil.x=p.x;
        perfil.y=p.y;
        perfil.h=p.h;
        perfil.criterioRotura=criterioRotura;
        perfiles=cat(1,perfiles,perfil);       
    end
    c=1;        
    total=length(mopla.tide)*length(mallas);
    clear perfil    
    try
    for i=1:length(mopla.tide)        
        for j=1:length(mallas)
            mallaId=mallas{j};            
            malla=alternativa.mallas(mallaId);
            casoId=['000',num2str(c)];
            casoId=casoId(end-3:end);
            grdFileHs=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_Height.GRD']);
            grdFileDir=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_Direction.GRD']);
            grdFileUx=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_VelUx.GRD']);
            grdFileUy=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_VelUy.GRD']);
            try
                [hs,x,y]=util.readGrd(grdFileHs,malla);
                [dir,~,~]=util.readGrd(grdFileDir,malla);
                
                if strcmp(criterioRotura,'velMax')
                    [ux,~,~]=util.readGrd(grdFileUx,malla);
                    [uy,~,~]=util.readGrd(grdFileUy,malla);

                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %%% En las velocidades se a�aden dos ceros encima
                    %%% y unos a los lados
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    ux=cat(2,zeros(malla.ny-2,2),ux);
                    ux=cat(1,zeros(1,malla.nx),ux);
                    ux=cat(1,ux,zeros(1,malla.nx));                

                    uy=cat(2,zeros(malla.ny-2,2),uy);
                    uy=cat(1,zeros(1,malla.nx),uy);
                    uy=cat(1,uy,zeros(1,malla.nx));
                    %Paso a coordenadas batimetricas
                    uy=-uy;
                    uxRot=ux*cosd(malla.angle)-uy*sind(malla.angle);
                    uyRot=ux*sind(malla.angle)+uy*cosd(malla.angle);
                    ux=uxRot;uy=uyRot;
                    %%% Fin de tuneo en velocidades
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                else
                     qbFile= fullfile(folder,[mallaId,mopla.moplaId,casoId,'_qb.dat']);
                     [x_qb,y_qb,qb]=util.readQb(malla,qbFile);
                end
                
            catch ex
                perfiles=[];
                delete(h);
                core.blockMainForm();
                msgbox({txt.post('iBreakingError'),[mallaId,mopla.moplaId,casoId],ex.message},'Error','error');                
                return;
            end            
          
                
            %%%Corrijo el �ngulo respecto a la batimetria
            dir=-dir; %%Comprobar!!;            
            
            x=reshape(x,size(hs,1)*size(hs,2),1);
            y=reshape(y,size(hs,1)*size(hs,2),1);           
            hsData=reshape(hs,size(hs,1)*size(hs,2),1);
            dirData=reshape(dir,size(hs,1)*size(hs,2),1);                      
            
            if strcmp(criterioRotura,'velMax')
                uxData=reshape(ux,size(ux,1)*size(ux,2),1);
                uyData=reshape(uy,size(uy,1)*size(uy,2),1);                
            else %%%%Calculo por porcentaje olas rotas                
                [x_qb,y_qb,qb]=util.readQb(malla,qbFile);                
                x_qb=reshape(x_qb,size(qb,1)*size(qb,2),1);
                y_qb=reshape(y_qb,size(qb,1)*size(qb,2),1);
                qbData=reshape(qb,size(qb,1)*size(qb,2),1);
            end
            
            d=max(malla.dx,malla.dy)*2;
            for k=1:length(perfiles)                
                p=mopla.perfiles( perfiles(k).id);
                perfilAng=atan2(p.posicion(2,2)-p.posicion(1,2),p.posicion(2,1)-p.posicion(1,1));
                mallaPerfilAng=malla.angle*pi/180-perfilAng; %%Angulo de la malla respecto al perfil               
                px=(p.posicion(2,1)-p.posicion(1,1))/p.getLength();
                py=(p.posicion(2,2)-p.posicion(1,2))/p.getLength();
               
                 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                 %%% Calculo el punto de rotura                 
                 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                 if strcmp(criterioRotura,'velMax') %%Velocidad Máxima
                     ux=util.interpPerfil(x,y,uxData,perfiles(k),d);
                     uy=util.interpPerfil(x,y,uyData,perfiles(k),d);
                     
                    %ux=uxF(p.x,p.y);
                    %uy=uyF(p.x,p.y);
                    
                    %%% Velocidad perpendicular al perfil
                    %perfiles(k).uPerfil(i,j,:)=abs(ux*sin(mallaPerfilAng)+uy*cos(mallaPerfilAng));                                                                    
                    %%%%Velocidad absoulta EN el perfil
                    perfiles(k).uPerfil(i,j,:)=sqrt(ux.^2+uy.^2);
                    [maxVel,index]=max(perfiles(k).uPerfil(i,j,:));
                    if maxVel==0 || isnan(maxVel) %No hay rotura
                        %[~,index]=min(abs(perfiles(k).h)+mopla.tide(i)); %El punto es el que está mas cercano a la costa
                        %index=index(end);
                        index=find((perfiles(k).h+mopla.tide(i))>0);
                        index=index(1);
                    end
                    %Velocidad promediada desde el punto de rotura hasta la
                    %costa, util para calcular el transporte de Bayram
                    uPerPerfil=ux*py-uy*px; %Modulo de la velocidad perpendicular al perfil
                    waterIndex=find((p.h+mopla.tide(i))>=0);
                    waterIndex=waterIndex(1);
                    uBrk=uPerPerfil(waterIndex:index);
                    uBrk(isnan(uBrk))=0;
                    if isempty(uBrk),uBrk=0;end
                    perfiles(k).uAvg(i,j)=mean(uBrk);
                    
                     
                 else %% Porcentaje de olas rotas

                     qb=util.interpPerfil(x_qb,y_qb,qbData,perfiles(k),d);
                     perfiles(k).qb(i,j,:)=qb;
                     qb_umbral=0.1; %% En un futuro cambiar
                     positivos=find(qb>qb_umbral);
                     if isempty(positivos) %% Busco el punto con el máximo QB
                          [~,index]=max(qb);
                     else
                         saltos=find(diff(positivos)~=1);
                         if isempty(saltos)%por si solo hay una zona de rompientes
                             index=positivos(end);
                         else%si hay mas de una zona de rompientes se queda con la m�s cercana a la costa
                            index=positivos(saltos(1));
                         end
                     end
                 end
                 %%% End Calculo el punto de rotura                 
                 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                cosDir=util.interpPerfil(x,y,cosd(dirData),perfiles(k),d);
                sinDir=util.interpPerfil(x,y,sind(dirData),perfiles(k),d);
                dirX=cosDir*cos(mallaPerfilAng)-sinDir*sin(mallaPerfilAng);
                dirY=sinDir*cos(mallaPerfilAng)+cosDir*sin(mallaPerfilAng);
                
                perfiles(k).hs(i,j,:)=util.interpPerfil(x,y,hsData,perfiles(k),d);
                
                %%% Direccion respecto al perfil (ángulo de procedencia del
                %%% oleaje, en el sentido contrario a las agujas del reloj)
                dir=atan2(dirY,dirX)*180/pi+180;
                dir(dir>180)=dir(dir>180)-360;
                perfiles(k).dir(i,j,:)=dir;
                
                %%% Direccion respeto al norte
                dir_north=270-(atan2(sinDir,cosDir)*180/pi+malla.angle);
                dir_north(dir_north>360)=dir_north(dir_north>360)-360;
                dir_north(dir_north<0)=dir_north(dir_north<0)+360;
                perfiles(k).dir_north(i,j,:)=dir_north;                
                
                
                perfiles(k).x_b(i,j)=perfiles(k).x(index);
                perfiles(k).y_b(i,j)=perfiles(k).y(index);
                perfiles(k).dir_b(i,j)=perfiles(k).dir(i,j,index);
                perfiles(k).dir_b_north(i,j)=perfiles(k).dir_north(i,j,index);
                
                
                %%%%Distancia desde la costa hasta la rotura
                waterIndex=find((p.h+mopla.tide(i))>=0);
                waterIndex=waterIndex(1);
                x_l=p.x(waterIndex);y_l=p.y(waterIndex);               
                l_b=sqrt((perfiles(k).x_b(i,j)-x_l).^2+(perfiles(k).y_b(i,j)-y_l).^2);
                if l_b<0,l_b=0;end
                perfiles(k).l_b(i,j)=l_b;
                
                
                
                hs_b=perfiles(k).hs(i,j,index);
                if isnan(hs_b)
                    hs_b=0;
                end
                h_b=perfiles(k).h(index)+mopla.tide(i);  %Sumo la marea, a la profundidad;
                gamma=hs_b/h_b;
                %gammaFull=squeeze(perfiles(k).hs(i,j,:))./(perfiles(k).h'+mopla.tide(i));
                %%Corrijo
                if hs_prop(j)<mopla.hsMin;
                    perfiles(k).hs(i,j,:)=perfiles(k).hs(i,j,:)*hs_prop(j)/mopla.hsMin;
                    %perfiles(k).h=squeeze(perfiles(k).hs(i,j,:))./gammaFull-mopla.tide(i);
                    %perfiles(k).h=perfiles(k).h';
                    hs_b=hs_prop(j)*hs_b/mopla.hsMin;
                    h_b=hs_b/gamma;
                    if isnan(h_b),h_b=0;end
                end
                perfiles(k).hs_b(i,j)=hs_b;
                perfiles(k).h_b(i,j)=h_b;
            end
            
            waitbar(c/total);
            c=c+1;
        end
    end
    
   
   
    %%%%Grabo
    for k=1:length(perfiles)
        perfil=perfiles(k);
        file=fullfile(fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId,[perfil.id,'_prf.mat']));
        save(file,'perfil');
    end
    
    if exist('h','var')
        delete(h);
    end
    core.blockMainForm();
    
    catch ex
       if exist('h','var'),delete(h);end        
       core.blockMainForm();       
       msgbox({ex.message},'Error','error');                       
       return;
    end
    
    function close(varargin)
        
    end
        
    
end


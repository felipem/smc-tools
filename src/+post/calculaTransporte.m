function perfilesTTE= calculaTransporte(mopla,project,alternativa)    
    
    %Compruebo si se ha calculado la rotura
    moplaFolder=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId);       
    perfiles=dir(fullfile(moplaFolder,'*_prf.mat'));        
    %%%%%%%%    
    
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    
    %Alturas y profundidades m�nimas para la rotura
    h_b_min=0; 
    hs_b_min=1E-03;
    
    
    %%%Creo el source
    casosMD=mopla.casosMD(logical(mopla.propagate));
    sourceData=[mopla.hs,mopla.tp,cosd(mopla.dir),sind(mopla.dir)];    
    
    classIndex=casosMD;

    sourceData(:,1)=(sourceData(:,1)-min(sourceData(:,1)))/(max(sourceData(:,1))-min(sourceData(:,1)));
    sourceData(:,2)=(sourceData(:,2)-min(sourceData(:,2)))/(max(sourceData(:,2))-min(sourceData(:,2)));
    centers=sourceData(classIndex,:);    
     
    h=waitbar(0,txt.post('pdTransport'));%,'WindowStyle','Modal');
     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
     %%% Inicializar MAREA 
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
     %%%Cargo la marea astronómica
     db=util.loadDb;
     [tideTime,tide]=data.getGOTTemporalSerie(db,mopla.lat,mopla.lon);
     tide=interp1(tideTime,tide,mopla.time);
     tide=tide-min(tide); %Esto no está muy bien..... ya que depende del periodo temporal.    

    
     %%% Inicializar MAREA
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    waitbar(0.1);
     
    perfilesTTE=[];
    for i=1:length(perfiles)
        
        perfil=load(fullfile(moplaFolder,perfiles(i).name));
        perfil=perfil.perfil;
        
        perfilAng=atan2(perfil.y(end)-perfil.y(1),perfil.x(end)-perfil.x(1))*180/pi;
        if isKey(mopla.perfiles,perfil.id)
            p=mopla.perfiles(perfil.id);
            
            perfilTTE.hs_b=zeros(length(mopla.hs),1);
            perfilTTE.hs_b_all=zeros(length(mopla.hs),length(mopla.tide));
            
           
            %%%Reconstruyo HS  
            for j=1:length(mopla.tide)
                
                    %Compruebo casos sin rotura (h_b<=0) YA ESTA HECHO en
                    %calculaRotura.m
%                     h_b=squeeze(perfil.h_b(j,:));
%                     index=h_b<=0;
%                     perfil.h_b(j,index)=h_b_min;
%                     perfil.hs_b(j,index)=hs_b_min;
                    
                   [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, squeeze(perfil.hs_b(j,:))');                                        
                   tmp(tmp<hs_b_min)=hs_b_min;
                   perfilTTE.hs_b_all(:,j)=tmp;                   
                   %perfilTTE.hs_b=perfilTTE.hs_b+tmp.*pesos(:,j);
            end
            perfilTTE.hs_b=post.interpolateTide(tide,mopla.tide,perfilTTE.hs_b_all);
            perfilTTE.dir_b_all=zeros(length(mopla.hs),length(mopla.tide));
     
             waitbar((i-0.666)/length(perfiles));
             
            for j=1:length(mopla.tide)
                [~,tmp] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, squeeze(perfil.dir_b(j,:))');                
                perfilTTE.dir_b_all(:,j)=tmp;
            end
            cos_dir=post.interpolateTide(tide,mopla.tide,cosd(perfilTTE.dir_b_all));
            sin_dir=post.interpolateTide(tide,mopla.tide,sind(perfilTTE.dir_b_all));
            
            waitbar((i-0.333)/length(perfiles));
            
            perfilTTE.dir_b=atan2(sin_dir,cos_dir)*180/pi;
            
            perfilTTE.h_b=zeros(length(mopla.hs),1);
            perfilTTE.h_b_all=zeros(length(mopla.hs),length(mopla.tide));
            %%%Reconstruyo hb             
            for j=1:length(mopla.tide)
                
                [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, squeeze(perfil.h_b(j,:))');
                tmp(tmp<h_b_min)=h_b_min;
                perfilTTE.h_b_all(:,j)=tmp;
            end 
            perfilTTE.h_b=post.interpolateTide(tide,mopla.tide,perfilTTE.h_b_all);            
            
            waitbar((i)/length(perfiles));
            
            %%%% Angulo de rotura respecto al norte!!!
            dir_b_north=90-(perfilTTE.dir_b+perfilAng);
            dir_b_north(dir_b_north>360)=dir_b_north(dir_b_north>360)-360;
            dir_b_north(dir_b_north>360)=dir_b_north(dir_b_north>360)-360;
            perfilTTE.dir_b_north=dir_b_north;
            
            

            
            
            h_nivel=perfilTTE.h_b; %No hace falta sumar el nivel porque ya se lo metemos en la interpolación

            perfilTTE.time=mopla.time;
            
            
            K=1.4*exp(-2.5*p.d50);
            rho=1025;           
            g=9.81;
            hrms_b=perfilTTE.hs_b/sqrt(2);   
            %k=0.78; %TODO
            %k=perfilTTE.hs_b./h_nivel; %LARA Dice!! lo meto en la ecuación
            %princiapl
            
            %minK=min(k(k>0));
            %k(k<=0)=minK;
            %hs_b=perfilTTE.hs_b;
            %hs_b(hs_b<=0)=hs_b_min;
             h_nivel(h_nivel<0)=0;
            
            perfilTTE.q=K*(sqrt(g*h_nivel)./(16*sqrt(sqrt(2))*(-1+p.rho_s/rho)*(1-p.n))).*sind(2*perfilTTE.dir_b).*(hrms_b.^2);
            
            %%%% EF (Energy Flux)
            %tp=mopla.tp; 
            %hb=perfilTTE.hs_b/k;  %%%Pongo que la rotura es más o menos constante            
            %h_nivel(h_nivel<0)=0;
            cg=sqrt(g* h_nivel);         %%%Celeridad de grupo en shallow water
            ef=rho*g*cg.*(perfilTTE.hs_b).^2/8;  
            
            dir_x=perfilTTE.dir_b+perfilAng-180; %%Dirección repecto a X_UTM
            perfilTTE.ef_x=ef.*cosd(dir_x);
            perfilTTE.ef_y=ef.*sind(dir_x);
            
            file=fullfile(moplaFolder,[perfil.id,'_tte.mat']);
            save(file,'perfilTTE','-mat');
            perfilesTTE=cat(1,perfilesTTE,perfilTTE);
            
        end
    end
    waitbar(1);
    delete(h);
    
end
function hasCurrents=checkCurrents(mopla,project,alternativa)
      hasCurrents=true;      
      mallas=mopla.mallas(logical(mopla.propagate));
      
      folder=fullfile(project.folder,alternativa.nombre,'Mopla','SP');    
       c=1;  
       for i=1:length(mopla.tide)        
        for j=1:length(mallas)
            mallaId=mallas{j};                        
            casoId=['000',num2str(c)];
            casoId=casoId(end-3:end);
            grdFileUx=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_VelUx.GRD']);
            grdFileUy=fullfile(folder,[mallaId,mopla.moplaId,casoId,'_VelUy.GRD']);
            if ~exist(grdFileUx,'file') || ~exist(grdFileUy,'file')
                hasCurrents=false;
                return;
            end
            c=c+1;
        end
       end
end
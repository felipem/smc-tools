function mapPeriod=editMapPeriodForm(mapPeriod,times)

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
      scrPos=get(0,'ScreenSize');
    width=400;
    height=300;
    f = dialog('Name',txt.post('wMapPeriodTitle'), 'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'Visible','off','CloseRequestFcn',@closeForm);
    
    dates=datevec(times);
    years=dates(1,1):dates(end,1);    
    months=util.months;
    
    %chPanel=uipanel('Parent',f,'units','pixels,'Position'
    uicontrol(f,'Style','pushbutton','String', txt.post('wMapPeriodAcept'),'Position',[10,5,80,20],'Callback',@ok);
    uicontrol(f,'Style','pushbutton','String', txt.post('wMapPeriodCancel'),'Position',[110,5,80,20],'Callback',@closeForm);

    bg=uibuttongroup('Parent',f,'units','pixel','Position',[0 50 400 270]);

    rbMediaAnualTotal=uicontrol(bg,'Style','radiobutton','String',txt.post('wMapPeriodAll'),'Position',[10 200 200 30]);
    rbMediaAnual=uicontrol(bg,'Style','radiobutton','String',txt.post('wMapPeriodYear'),'Position',[10 150 200 30]);
    rbMediaEstacional=uicontrol(bg,'Style','radiobutton','String',txt.post('wMapPeriodSeason'),'Position',[10 100 200 30]);
    rbMediaMensual=uicontrol(bg,'Style','radiobutton','String',txt.post('wMapPeriodMonth'),'Position',[10 50 200 30]);

    cbMediaAnual=uicontrol(bg,'Style','popupmenu','String',years,'Position',[200 140 180 30]);
    cbMediaMensual=uicontrol(bg,'Style','popupmenu','String',months,'Position',[200 40 180 30]);
    cbMediaEstacionalIni=uicontrol(bg,'Style','popupmenu','String',months,'Position',[200 90 80 30]);
    cbMediaEstacionalEnd=uicontrol(bg,'Style','popupmenu','String',months,'Position',[300 90 80 30]);
    
    
    if strcmp(mapPeriod.type,'MediaAnualTotal')
        set(rbMediaAnualTotal,'Value',1)
    elseif strcmp(mapPeriod.type,'MediaAnual')
        set(rbMediaAnual,'Value',1);   
        value=find(years==mapPeriod.ini);
        set(cbMediaAnual,'Value',value);
    elseif strcmp(mapPeriod.type,'MediaMensual')
        set(rbMediaMensual,'Value',1)
        set(cbMediaMensual,'Value',mapPeriod.ini);
    elseif strcmp(mapPeriod.type,'MediaEstacional')
        set(cbMediaEstacionalIni,'Value',mapPeriod.ini);
        set(cbMediaEstacionalEnd,'Value',mapPeriod.end);
        set(rbMediaEstacional,'Value',1)
    end
    
    set(f,'Visible','on');
    uiwait(f);
    function closeForm(varargin)        
        delete(gcbf);
    end

    function ok(varargin)
        if get(rbMediaAnualTotal,'Value')            
            mapPeriod.type='MediaAnualTotal';
            mapPeriod.ini=dates(1,1);
            mapPeriod.end=dates(end,1);            
        elseif get(rbMediaAnual,'Value')            
            mapPeriod.type='MediaAnual';          
            mapPeriod.ini=years(get(cbMediaAnual,'Value'));
        elseif get(rbMediaMensual,'Value')            
            mapPeriod.type='MediaMensual';          
            mapPeriod.ini=get(cbMediaMensual,'Value');
        elseif get(rbMediaEstacional,'Value')            
            mapPeriod.type='MediaEstacional';          
            mapPeriod.ini=get(cbMediaEstacionalIni,'Value');
            mapPeriod.end=get(cbMediaEstacionalEnd,'Value');
        end
        delete(gcbf);
    end


end




function [d50,rho_s,n] = editProfileDlg(perfil)
    %%% Cargo el idioma
     txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    prompt = {txt.post('wProfileEditName'),'D50 (mm)','Rho_s (kg/m^3)','n'};
    dlg_title = txt.post('wProfileEditTitle');
    num_lines = 1;
    def = {perfil.nombre,num2str(perfil.d50),num2str(perfil.rho_s),num2str(perfil.n)};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    if isempty(answer)        
        d50=perfil.d50;
        rho_s=perfil.rho_s;
        n=perfil.n;        
        return;
    end
    
    d50=str2double(answer{2});
    rho_s=str2double(answer{3});
    n=str2double(answer{4});
end


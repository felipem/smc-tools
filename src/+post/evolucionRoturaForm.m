function evolucionRoturaForm(mopla,perfil)    

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    f=figure('Name', [txt.post('wBreakingEvoTitle'),' ',perfil.id], 'NumberTitle','off');   
    
    %axes('Parent',axPanel);
    axPanel=[];
    
    bPanel=uipanel(f,'Units','Normalized','Position',[0,0,1,0.1]);
    uicontrol(bPanel,'Style','pushbutton','String','Refrescar','Position',[10,5,70,30],'Callback',@refreshGraph);
    uicontrol(bPanel,'Style','text','String',txt.post('wBreakingEvoTide'),'Position',[110,0,40,25]);
    cMarea=uicontrol(bPanel,'Style','popup','String',1:size(perfil.hs,1),'Position',[160,0,40,30]);
    uicontrol(bPanel,'Style','text','String',txt.post('wBreakingEvoCase'),'Position',[250,0,40,25]);
    cCaso=uicontrol(bPanel,'Style','popup','String',1:size(perfil.hs,2),'Position',[300,0,40,30]);    
    
    x=sqrt((perfil.x-perfil.x(1)).^2+(perfil.y-perfil.y(1)).^2);
    refreshGraph;    
    
    function refreshGraph(varargin)
        if ~isempty(axPanel)
            util.resetPanel(axPanel);
        end
        
        axPanel=uipanel(f,'Units','Normalized','Position',[0,0.1,1,0.9]);
        ax1=subplot(3,1,1,'Parent',axPanel);
        ax2=subplot(3,1,2,'Parent',axPanel);
        ax3=subplot(3,1,3,'Parent',axPanel);                 
    
        mareaIdx=get(cMarea,'Value');
        casoIdx=get(cCaso,'Value');
        h=perfil.h+mopla.tide(mareaIdx); %Sumo la marea a la profundidad!!
        hs=squeeze(perfil.hs(mareaIdx,casoIdx,:));
        dir_north=squeeze(perfil.dir_north(mareaIdx,casoIdx,:));
        u=squeeze(perfil.uPerfil(mareaIdx,casoIdx,:));    
        qb=squeeze(perfil.qb(mareaIdx,casoIdx,:));    
        h_b=perfil.h_b(mareaIdx,casoIdx);
        dir_b_north=perfil.dir_b_north(mareaIdx,casoIdx);
        x_b=sqrt((perfil.x_b(mareaIdx,casoIdx)-perfil.x(1)).^2 + (perfil.y_b(mareaIdx,casoIdx)-perfil.y(1)).^2);
        hs_b=perfil.hs_b(mareaIdx,casoIdx);    
        
        if ~isfield(perfil,'criterioRotura')
            perfil.criterioRotura='velMax';
        end
        
        
        if strcmp(perfil.criterioRotura,'velMax')
            brkVar=u*100;
            leg='Vel (cm/s)';
            brkValue=max(u);
        else
            brkVar=qb*100;
            leg='Qb (%)';            
            brkValue=max(brkVar);
            if brkValue>=10 %%En el caso que se alcance el valor de un 10% de olas rotas
                brkValue=10;
            end
        end
        
        
        ax1=plotyy(x,hs,x,brkVar,'Parent',ax1);
        
        hold(ax1(1),'on');    
        hold(ax1(2),'on');
        plot(ax1(1),x_b,hs_b,'*k');        
        plot(ax1(2),[x_b,x_b],[0,brkValue],'k');
        legend(ax1(1),'Hs (m)', ['Hs_b (m) = ',num2str(hs_b)],leg);
        title(ax1(1),txt.post('wBreakingEvoPlot1Title'));

        
        plot(ax2,x,dir_north,'b');
        hold(ax2,'on');    
        plot(ax2,x_b,dir_b_north,'*k');
        legend(ax2,'Dir (º N)', ['Dir_b (º N) = ',num2str(dir_b_north)]);
        title(ax2,txt.post('wBreakingEvoPlot2Title'));

        
        plot(ax3,x,h,'r');
        hold(ax3,'on');    
        plot(ax3,x_b,h_b,'*k');
        legend(ax3,'h (m)', ['h_b (m) = ',num2str(h_b)]);
        title(ax3,txt.post('wBreakingEvoPlot3Title'));
        set(ax3,'YDir','Reverse');
    end        
end


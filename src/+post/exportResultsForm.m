function exportResultsForm(project,altName,moplaId)
%exportResultsForm:
% Ventana para exportar los resultados de un mopla en fichero ASCII, esto
% son el FEM en POIS y el transporte de sedimentos en perfiles

    altDir=fullfile(project.folder,altName);
    mopla=smc.loadMopla(altDir,moplaId,{'perfiles','pois','tp','time','moplaId','propagate',...
        'tide','transportModel','kModel','lat','lon'});
    profiles=getRebuildedProfiles;
    pois=getRebuildedPois;
    txt=util.loadLanguage;
    scrPos=get(0,'ScreenSize');
    
    nElements=max(length(pois),length(profiles));
    width=450;
    height=nElements*25+125;    
    xLeft=10;    
    xLeftTab=30;
    xLeftLeft=width/2+10;
    xLeftLeftTab=width/2+30;
    yPos=(nElements*25+50):-25:25;
      
    f = figure( 'MenuBar', 'none', 'Name', txt.post('wExportResultsTitle'), 'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@cancel,'WindowStyle','modal');
    set(f, 'resize', 'off');    
    pos=get(f,'Position');
    uicontrol(f,'Style','pushbutton','String', txt.post('wExportResultsOk'),'Position',[30 10 60 20],'Callback',@ok);    
    uicontrol(f,'Style','pushbutton','String', txt.post('wExportResultsCancel'),'Position',[120 10 60 20],'Callback',@cancel);    
    
    
    pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);
    
    uicontrol(pMain,'Style','text','String',txt.post('wExportResultsOutput'),'Position',[xLeft yPos(1) 90 15],'HorizontalAlignment','left');
    tbOutput=uicontrol(pMain,'Style','edit','String',fullfile(project.folder,altName,'Mopla',moplaId),...
        'Position',[xLeft + 100 yPos(1) 300 20],'Enable','off','HorizontalAlignment','left');
    bOutput=uicontrol(pMain,'Style','pushbutton','Position',[xLeft+410 yPos(1) 24 24],'Callback',@setFolder);
    util.setControlIcon(bOutput,'folder.gif');  
    
    %maxElements=20;
    %%%Pois
    uicontrol(pMain,'Style','text','String',txt.post('wExportResultsPois'),'Position',[xLeft yPos(2) width/2 15],'HorizontalAlignment','left','FontWeight','bold');
    poisCheck=nan(length(pois),1);
    for i=1:length(pois)
        poi=pois{i};
        poisCheck(i)=uicontrol(pMain,'Style','checkbox','String',poi,'Position',[xLeftTab yPos(i+2) 100 15]);
    end
    
    uicontrol(pMain,'Style','text','String',txt.post('wExportResultsProfiles'),'Position',[xLeftLeft yPos(2) width/2 15],'HorizontalAlignment','left','FontWeight','bold');
    profilesCheck=nan(length(profiles),1);
    for i=1:length(profiles)
        perfil=profiles{i};
        profilesCheck(i)=uicontrol(pMain,'Style','checkbox','String',perfil,'Position',[xLeftLeftTab yPos(i+2) 100 15]);
    end
    
    function cancel(varargin)
        delete(f);
        pause(0.2);
    end

    function ok(varargin)
        poisToExport={};
        profilesToExport={};
        for i=1:length(poisCheck)
            if get(poisCheck(i),'Value')
                poisToExport=cat(1,poisToExport,get(poisCheck(i),'String'));
            end
        end
        for i=1:length(profilesCheck)
            if get(profilesCheck(i),'Value')
                profilesToExport=cat(1,profilesToExport,get(profilesCheck(i),'String'));
            end
        end
        total=length(profilesToExport)+length(poisToExport);
        outFolder=get(tbOutput,'String');
        h=waitbar(0,txt.post('wExportResultWaitbar'));
        errorMsg={};
        for i=1:length(poisToExport)
            try
                post.savePoiResults(mopla,project,altName,poisToExport{i},outFolder);
            catch ex
                errorMsg=sprintf('%s\n%s',txt.post('wExportResultError'),poisToExport{i});
                break;
            end
            waitbar(i/total,h);
        end
        
        if isempty(errorMsg) %No hubo erro con los POIS
             for i=1:length(profilesToExport)
                try
                    post.saveProfileResults(mopla,project,altName,profilesToExport{i},outFolder);
                catch 
                    errorMsg=sprintf('%s\n%s',txt.post('wExportResultError'),profilesToExport{i});
                    break;
                end
                waitbar((i+length(poisToExport))/total,h);
            end
        end
        delete(h);
        if ~isempty(errorMsg)
            msgbox(errorMsg,'Error','Error');
        else        
            delete(f);
            pause(0.2);
        end
    end

    function setFolder(varargin)
        folder=get(tbOutput,'String');
        dir=uigetdir(folder,txt.post('wExportResultsOutDir'));
        if ~isempty(dir) && ischar(dir)
             set(tbOutput,'String',dir,'HorizontalAlignment','left');
        end
    end

   function profiles=getRebuildedProfiles()
         profiles={};
         profilesKeys=keys(mopla.perfiles);
         for i=1:length(profilesKeys)
             profileFile=fullfile(project.folder,altName,'Mopla',moplaId,[profilesKeys{i},'_tte.mat']);
             if exist(profileFile,'file')
                profiles=cat(1,profiles,profilesKeys{i});
             end
         end
   end

    function pois=getRebuildedPois()
         pois={};
         poisKeys=keys(mopla.pois);
         for i=1:length(poisKeys)                       
             poiFile=fullfile(project.folder,altName,'Mopla',moplaId,[poisKeys{i},'.poi']);
             if exist(poiFile,'file')
                pois=cat(1,pois,poisKeys{i});
              end
         end
    end                
end


function [ef_x,ef_y,ef] = fem(hs,h,tp,dir_x)
    g=9.81;
    rho=1025;
%    cg=sqrt(g* h);         %%%Celeridad de grupo en shallow water
     [k,~]=util.dispersion_hunt(h,tp);
     w=2*pi./tp;        
     kh=k.*h;
     cg=g*(tanh(kh)+kh./(cosh(kh).^2))./(2*w);
    ef=rho*g*cg.*(hs).^2/8; 
    ef(isnan(ef))=0;
%     perfilAng=atan2(perfil.y(end)-perfil.y(1),perfil.x(end)-perfil.x(1))*180/pi;
%     dir_x=dir_b+perfilAng-180; %%Direcci�n repecto a X_UTM
    ef_x=ef.*cosd(dir_x);
    ef_y=ef.*sind(dir_x);    
end


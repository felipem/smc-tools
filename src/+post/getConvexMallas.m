function [poly,pointsPorMalla] = getConvexMallas(mallas)
    points=[];
    pointsPorMalla=containers.Map;
    pointsPorMalla=util.initMap(pointsPorMalla);
    
    for i=1:length(mallas)        
        malla=mallas(i);
        nombre=malla.nombre;
        x1=linspace(0,malla.x,malla.nx);
        y1=linspace(0,malla.y,malla.ny);
        [x1,y1]=meshgrid(x1,y1);
        x1_=x1*cosd(malla.angle)-y1*sind(malla.angle);
        y1_=x1*sind(malla.angle)+y1*cosd(malla.angle);
        x1=x1_+malla.xIni;
        y1=y1_+malla.yIni;
        x1=reshape(x1,malla.nx*malla.ny,1);
        y1=reshape(y1,malla.nx*malla.ny,1);        
        
%         if ~isempty(malla.parent)
%             mallaP=mallasDic(malla.parent);
%             xp=linspace(0,mallaP.x,mallaP.nx);
%             yp=linspace(0,mallaP.y,mallaP.ny);
%             [xp,yp]=meshgrid(xp,yp);
%             xp_=xp*cosd(mallaP.angle)-yp*sind(mallaP.angle);
%             yp_=xp*sind(mallaP.angle)+yp*cosd(mallaP.angle);
%             xp=xp_+mallaP.xIni;
%             yp=yp_+mallaP.yIni;
%             xp=reshape(xp,mallaP.nx*mallaP.ny,1);
%             yp=reshape(yp,mallaP.nx*mallaP.ny,1);    
%             x1=cat(1,x1,xp);
%             y1=cat(1,y1,yp);
%          end
        
        
        
        for j=1:length(mallas)
            if j~=i
                malla=mallas(j);                  
                x2=[0,0,malla.x,malla.x];
                y2=[0,malla.y,malla.y,0];
                x2_=x2*cosd(malla.angle)-y2*sind(malla.angle);
                y2_=x2*sind(malla.angle)+y2*cosd(malla.angle);
                x2=x2_+malla.xIni;
                y2=y2_+malla.yIni;                
%                 if ~isempty(malla.parent)
%                     mallaP=mallasDic(malla.parent);
%                     xp=[0,0, mallaP.x, mallaP.x];
%                     yp=[0, mallaP.y, mallaP.y,0];
%                     xp_=xp*cosd( mallaP.angle)-yp*sind( mallaP.angle);
%                     yp_=xp*sind( mallaP.angle)+yp*cosd( mallaP.angle);
%                     xp=xp_+malla.xIni;
%                     yp=yp_+malla.yIni; 
%                     x2=cat(2,x2,xp);
%                     y2=cat(2,y2,yp);
%                 end
%                p=convhull(x2,y2);
                
                in=inpolygon(x1,y1,x2,y2);               
%                in=inpolygon(x1,y1,x2(p),y2(p));
                x1=x1(in);
                y1=y1(in);
            end
        end
       
        xy=[x1,y1];
        points=cat(1,points,xy);
        pointsPorMalla(nombre)=xy;
    end
    poly=convhull(xy(:,1),xy(:,2));
    poly=xy(poly,:);
end


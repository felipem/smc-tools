function tanB = getProfileSlope(mopla,level,perfilId)
    %Calcula la pendiente del perfil dada la marea
    maxLevel=max(level);
    minLevel=min(level);
    perfil=mopla.perfiles(perfilId);
    if maxLevel>max(-perfil.h),maxLevel=max(-perfil.h);end
    if minLevel<min(-perfil.h),minLevel=min(-perfil.h);end
    
    r=sqrt((perfil.x-perfil.x(1)).^2+(perfil.y-perfil.y(1)).^2);
    riMax=util.getIntersection(r,-perfil.h,maxLevel);
    riMin=util.getIntersection(r,-perfil.h,minLevel);
    
    try
        tanB=(maxLevel-minLevel)/(riMin(end)-riMax(1));
    catch
        tanB=0;
    end
    
    
    %Calcula la pendiente del perfil en la zona de rompientes
%     tanB=0;
%     prfFile=fullfile(project.folder,altName,'Mopla',mopla.moplaId,[perfilId,'_prf.mat']);
%     if ~exist(prfFile,'file'),return;end    
%     
%     perfil=load(prfFile,'-mat');
%     perfil=perfil.perfil;
%     
%     tanBTide=zeros(length(mopla.tide),1);
%     
%     for i=1:length(mopla.tide)
%         
%         [~, index1]=min(abs(perfil.x(1)-perfil.x_b(i,:))); %% punto más cercano a tierra
%         [~, index2]=max(abs(perfil.x(1)-perfil.x_b(i,:))); %% punto más lejano a tierra
%         
%         l=sqrt((perfil.x_b(i,index2)-perfil.x_b(i,index1)).^2+(perfil.y_b(i,index2)-perfil.y_b(i,index1)).^2); 
%         %%%Longitud de la zona de rompientes
%         dh=perfil.h(index2)-perfil.h(index1);
%         tanBTide(i)=dh/l; 
%     end
%     tanB=mean(tanBTide);
    
end


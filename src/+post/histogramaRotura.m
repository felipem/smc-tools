function [x,y,hist_b,hist_b_tide]=histogramaRotura(mopla,perfil)

    %%%Creo el histrograma numérico de las posiciones de rotura
    n=40;
    if length(perfil.x)<n
        n=length(perfil.x);
    end
    x=linspace(perfil.x(1),perfil.x(end),n);
    y=linspace(perfil.y(1),perfil.y(end),n);    
    h=sqrt((x-x(1)).^2+(y-y(1)).^2);
    dh=h(2)-h(1);    
    x=x-(x(2)-x(1))/2;
    y=y-(y(2)-y(1))/2;
    
    x_ini=perfil.x(1);
    y_ini=perfil.y(1);
    
    hist_b=zeros(n,1);    
    hist_b_tide=zeros(length(mopla.tide),n);    
    
    
    for i=1:size(perfil.x_b,1)
        for j=1:size(perfil.x_b,2)
            h_=sqrt((perfil.x_b(i,j)-x_ini).^2+(perfil.y_b(i,j)-y_ini).^2);
            idx=floor(h_/dh)+1;            
            hist_b(idx)=hist_b(idx)+mopla.probabilidad(j);   
            hist_b_tide(i,idx)=hist_b_tide(i,idx)+mopla.probabilidad(j); 
        end
    end
    
    hist_b=hist_b/sum(hist_b);
    
    for i=1:length(mopla.tide)
        hist_b_tide(i,:)=hist_b_tide(i,:)./sum(hist_b_tide(i,:));
    end
    
end


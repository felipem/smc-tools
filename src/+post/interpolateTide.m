function yi=interpolateTide(tide,tideLevels,y)
    if length(tideLevels)==1
        yi=y;
        return;        
    end
    
    yi=zeros(length(y),1);
    
    %Cota máxima
    index=tide>tideLevels(end);
    yi(index)=y(index,end);
    
    %Cota mínima
    index=tide<=tideLevels(1);
    yi(index)=y(index,1);
    
    for i=2:length(tideLevels)
        index=tide>tideLevels(i-1) & tide<=tideLevels(i);
        tideMin=tideLevels(i-1);
        tideMax=tideLevels(i);
        yi(index)=y(index,i-1)+(y(index,i)-y(index,i-1)).*(tide(index)-tideMin)/(tideMax-tideMin);        
    end
end
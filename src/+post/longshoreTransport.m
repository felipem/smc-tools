function q=longshoreTransport(hs_b,h_b,dir_b,tp,l_b,uAvg,perfil,method,kMethod)

rho=1025;           
g=9.81;

%x_b distancia desde la casta hasta el punto de rotura
L0=g*tp.^2/(2*pi);

h_bMin=1E-03;
h_b(h_b<h_bMin)=h_bMin;
l_bMin=1E-03;
l_b(l_b<l_bMin)=l_bMin;
m_b=h_b./l_b;
gamma_b=h_b./hs_b;
dir_b(dir_b>90 | dir_b<-90)=90;

switch method
    case 'Cerc'
        switch kMethod
            case 'Valle'
                k=1.4*exp(-2.5*perfil.d50);
            case 'Mil-Homens'               
                k=(2232.7.*(h_b./L0).^1.45 + 4.505).^-1;
        end
        hrms_b=hs_b/sqrt(2);        
        q=k.*(sqrt(g*h_b)./(16*sqrt(sqrt(2))*(-1+perfil.rho_s/rho)*(1-perfil.n))).*sind(2*dir_b).*(hrms_b.^2);
        %q=k./(16*sqrt(m_b)*(perfil.rho_s-rho)*(1-perfil.n))*rho*sqrt(g).*hs_b.^(5/2).*sin(2*dir_b); %Omar!
        
    case 'Kamphuis'
        a=2;
        b=1.5;
        c=0.75;
        d=-0.25;
        e=0.6;
        switch kMethod
            case 'Kamphuis'
                k=2.27;
            case 'Schoones'
                k=2.77;
            case 'Mil-Homens'
                k=17.5;
                a=2.75;
                b=0.89;
                c=0.86;
                d=-0.69;
                e=0.5;   
        end        
        q=sign(sind(dir_b)).*k.*hs_b.^a.*tp.^b.*m_b.^c.*perfil.d50.^d.*sind(2.*abs(dir_b)).^e/((perfil.rho_s-rho)*(1-perfil.n));
        %q=k.*hs_b(266).^a.*tp(266).^b.*m_b(266).^c.*perfil.d50.^d.*sind(2.*dir_b(266)).^e/((perfil.rho_s-rho)*(1-perfil.n))
        
    case 'Bayram'
        a=1-perfil.n;
        if perfil.d50<0.1 %mm
            ws=1.1e6*(perfil.d50*1E-03)^2;
        elseif perfil.d50>=0.1 %& D50<1, % mm
            ws=273*(perfil.d50*1E-03)^1.1;
        elseif perfil.d50>=1, %mm
            ws=4.36*(perfil.d50*1E-03)^0.5; 
        end
        
        switch kMethod
             case 'Bayram'
                k=(9+4.*hs_b./(ws.*tp))*1e-5;
            case 'Mil-Homens'
                k=(7.682e-5*(hs_b./L0).^1.283+1672.2).^-1;
        end
        %kw=util.dispersion_hunt(h_b,tp);
        %um=pi*Hs_b./(Tw.*sinh(kw.*h_b)); %Nabil
        %cf=1.39*(um.*tp/(2*pi*d50/12)).^-0.52; %Nabil
        %nikuradse=1; %m
        %cf=18*log(12*h_b/nikuradse); %drag coefficient, Chezy
        if isempty(uAvg)
        cf=0.005;
        %A=9/4*(ws^2/g)^(1/3);
        A=0.51*ws^0.44;
        Vm=5/32*pi*sqrt(g)./cf*A^(3/2).*sind(dir_b);        
        else 
            Vm=uAvg; %Hay que mirar las unidades
        end
        %cg=sqrt(g* h_b);         %%%Celeridad de grupo en shallow water
        %fb=rho*g*cg.*(hs_b).^2/8;
        fb=2^(5/4)*g^(3/2)*rho*hs_b.^(5/2).*cosd(dir_b)./(8*sqrt(gamma_b));        
        q=k./((perfil.rho_s-rho)*(1-a)*g*ws).*fb.*Vm;
end

end

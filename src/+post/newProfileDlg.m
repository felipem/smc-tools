function data=newProfileDlg(perfiles,defaultSlope,data)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Formulario para crear un perfil nuevo
    %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%% Cargo el idioma
    txt=util.loadLanguage;    
    %%%%%%%%%%%%%%%%%%%
    
    
    if ~exist('data','var')    
        %%%%Busca un n�mero libre de perfil
        for i=1:20        
            id=num2str(i,'0%d');
            id=id(end-1:end);
            if ~isKey(perfiles,['profile',id])
                break;
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%  Default Text Box data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        textBoxData.tbName=['profile',id];    
        textBoxData.tbD50='0.2';
        textBoxData.tbRho='2650';
        textBoxData.tbN='0.4';
        textBoxData.tbSlope=num2str(defaultSlope,'%.3f');
        %%%% End Defaul Text Box data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        strTitle=txt.post('wProfileNewTitle');
        newMode='on';
    else        
        textBoxData.tbName=data.name;    
        textBoxData.tbD50=num2str(data.d50);
        textBoxData.tbRho=num2str(data.rho);
        textBoxData.tbN=num2str(data.n);
        textBoxData.tbSlope=num2str(data.slope,'%.3f');
        data=[];
        
        strTitle=txt.post('wProfileEditTitle');
        newMode='off';
    end
    
    scrPos=get(0,'ScreenSize');
    width=400;
    height=225;
    logoPos=[236,28,128,128];
    xLeft=10;    
    xLeftTab=100;
    yPos=150:-25:50;
    
    
    f = figure( 'MenuBar', 'none', 'Name', strTitle, 'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@cancel);
    set(f, 'resize', 'off','WindowStyle','Modal');
    pos=get(f,'Position');
    uicontrol(f,'Style','pushbutton','String', txt.post('wProfileNewAcept'),'Position',[30 10 60 20],'Callback',@ok);    
    uicontrol(f,'Style','pushbutton','String',txt.post('wProfileNewCancel'), 'Position',[120 10 60 20],'Callback',@cancel);  
    
    pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    pLogo=uipanel(pMain,'units','pixel','position',logoPos);  
    ax=axes('Parent',pLogo,'YTick',[],'XTick',[],'position',[0,0,1,1]);
    imshow(fullfile('icon','profile_large.png'),'Parent',ax);
    axis(ax,'image');
     
    uicontrol(pMain,'Style','text','String',txt.post('wProfileNewName'),'Position',[xLeft yPos(1) 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbName,'Tag','tbName',...
        'Position',[xLeftTab yPos(1) 60 15],'BackgroundColor','w','Callback',@checkProfileName,...
        'Enable',newMode);
    lUsed=uicontrol(pMain,'Style','text','String','*','Position',[xLeftTab+65 yPos(1) 5 15],'Visible','off');
    
    uicontrol(pMain,'Style','text','String','D50 (mm)','Position',[xLeft yPos(2) 50 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbD50,'Tag','tbD50',...
        'Position',[xLeftTab yPos(2) 50 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,0.1,10});
    
    uicontrol(pMain,'Style','text','String','Rho_s (kg/m^3)','Position',[xLeft yPos(3) 80 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbRho,'Tag','tbRho',...
        'Position',[xLeftTab yPos(3) 50 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,1000,5000});
    
    uicontrol(pMain,'Style','text','String','n','Position',[xLeft yPos(4) 10 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbN,'Tag','tbN',...
        'Position',[xLeftTab yPos(4) 50 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,0,1});
    
    uicontrol(pMain,'Style','text','String',txt.post('wProfileNewSlope'),'Position',[xLeft yPos(5) 50 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbSlope,'Tag','tbSlope',...
        'Position',[xLeftTab yPos(5) 50 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,0,10});
    bSlope=uicontrol(pMain,'Style','pushbutton',...
        'tooltipstring',txt.post('wProfileNewSlopeHelp'),...
        'Position',[xLeftTab+60 yPos(5)-4 24 24],'Callback',@resetSlope);  
    util.setControlIcon(bSlope,'gear.gif');
    
   
    
    function ok(varargin)
        if ~strcmp(get(lUsed,'Visible'),'on')
            data.name=textBoxData.tbName;    
            data.d50=str2double(textBoxData.tbD50);
            data.rho=str2double(textBoxData.tbRho);
            data.n=str2double(textBoxData.tbN);
            data.slope=str2double(textBoxData.tbSlope);
            delete(f);
            pause(0.3);
        end        
    end

    function cancel(varargin)
        data=[];
        delete(f);
        pause(0.3);
    end
    
    function resetSlope(varargin)
        textBoxData.tbSlope=num2str(defaultSlope,'%.3f');
        tbControl=findall(pMain,'Tag','tbSlope');
        set(tbControl,'String',num2str(defaultSlope,'%.3f'));
    end

    function checkTextBox(tbControl,~,isInteger,minValue,maxValue)
        strValue=get(tbControl,'String');
        textBox=get(tbControl,'Tag');
        
        value=str2double(strValue);
        
        if ~isnan(value) 
            if isInteger,value=round(value);end
            if value<minValue,value=minValue;end
            if value>maxValue,value=maxValue;end
            value=num2str(value);
            textBoxData.(textBox)=value;
            
        else
            value=textBoxData.(textBox);            
        end
        tbControls=findall(pMain,'Tag',textBox);
        for j=1:length(tbControls)
            set(tbControls(j),'String',value);
        end
    end

    function checkProfileName(tbControl,varargin)
        strValue=get(tbControl,'String');
        index=strfind(strValue,'_');
        strValue(index)='-';
        if ~isKey(perfiles,strValue)
            textBoxData.tbName=strValue;
            set(tbControl,'String',strValue);
            set(lUsed,'Visible','off');
        else
            set(lUsed,'Visible','on');
        end
        
    end
    uiwait(f);

end


function phaseGraphForm(project)

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%%%
    
    h=waitbar(0,txt.post('wPhaseLoading'));
    alternativas=containers.Map;
    for i=1:length(project.alternativas)
        alternativa=project.alternativas{i};
        rdFolder=fullfile(project.folder,alternativa,'Mopla','RD');
        outFiles=dir(fullfile(rdFolder,'*out.dat'));
        if ~isempty(outFiles)
            casos={};
            for j=1:length(outFiles)                
                casos{j}=outFiles(i).name(1:end-7);                
                waitbar(i/length(project.alternativas)+j/(length(project.alternativas)*length(outFiles)));
            end
            altData.casos=casos;
            altData.alt=smc.loadAlternativa(project.folder,alternativa);
            alternativas(alternativa)=altData;            
        end        
        %waitbar(i/length(project.alternativas));
    end   
    
    if isempty(alternativas)
        delete(alternativas);  
        delete(h);
        return;
    end
    
    delete(h);    
    
    altNames=keys(alternativas);
    f=figure('Name', txt.post('wPhaseTitle'), 'NumberTitle','off');   
    axPanel=[];    
    bPanel=uipanel(f,'Units','Normalized','Position',[0,0,1,0.1]);
    %uicontrol(bPanel,'Style','pushbutton','String',txt.post('wPhaseDraw'),'Position',[10,5,70,30],'Callback',@refreshGraph);
    uicontrol(bPanel,'Style','text','String',txt.post('wPhaseAlt'),'Position',[10,0,80,25]);
    cAlternativa=uicontrol(bPanel,'Style','popup','String',altNames,'Position',[100,0,100,30],'Callback',@changeAlternativa);
    uicontrol(bPanel,'Style','text','String',txt.post('wPhaseCase'),'Position',[230,0,60,25]);
    cCaso=uicontrol(bPanel,'Style','popup','String',alternativas(altNames{1}).casos,'Position',[300,0,100,30],'Callback',@draw);   
    %cType=uicontrol(bPanel,'Style','popup','String',{'pcolor','countour'},'Position',[410,0,100,30],'Callback',@draw);   
    chkRef=uicontrol(bPanel,'Style','checkbox','Enable','on','Value',true,'String',txt.post('wPhaseRef'),'Position',[410 0 200 30],'Callback',@draw);            
    draw();
    
    function draw(varargin)
         if ~isempty(axPanel)
            util.resetPanel(axPanel);
         end        
        axPanel=uipanel(f,'Units','Normalized','Position',[0,0.1,1,0.9]);  
        ax=axes('Parent',axPanel);
        alt=get(cAlternativa,'Value');
        alt=altNames{alt};
        caso=get(cCaso,'Value');
        alternativa=alternativas(alt).alt;
        casoNames=alternativas(alt).casos;
        alternativa=alternativas(alt).alt;
        caso=casoNames{caso};
        title(ax,[alt,' - ',caso]);
        malla=caso(1:end/2);        
        
        malla=alternativa.mallas(malla);
        
        outFile=fullfile(project.folder,alt,'Mopla','RD',[caso,'out.dat']);
        [x,y,x_malla,y_malla,phase]=util.readOutFile(outFile,malla);
        c=load(fullfile('colormap','blue.mat'));
        c=c.c;
        
        %graphType=get(cType,'Value');
        graphType=1; 
        refBat=get(chkRef,'Value');
        
        
        if refBat
            %contourf(ax,x,y,phase,'LineStyle','none');
            
            if graphType==1
                %pcolor(ax,x,y,phase);shading interp;         
                contourf(ax,x,y,phase,50,'LineStyle','none');
                colorbar;
            else
                contour(ax,x,y,phase,[170,171],'Color',[0.5,0.5,0.5],'LineWidth',2);
                
            end
        else
             if graphType==1
                
                %pcolor(ax,x_malla,y_malla,phase);shading interp;     
                contourf(ax,x_malla,y_malla,phase,50,'LineStyle','none');
                colorbar;
            else
                contour(ax,x_malla,y_malla,phase,[170,171],'Color',[0.5,0.5,0.5],'LineWidth',2);
            end
         end
        axis(ax,'equal');
        %shading interp;
       
        colormap(ax,c);
        caxis([0 180]);
        hold(ax,'on');
        
        costas=alternativa.costas; 
        costas = util.costasInMalla(costas,malla);
        for k=1:length(costas)
            x=costas(k).x;
            y=costas(k).y;            
            if ~refBat
                [x,y]=util.xy2malla(x,y,malla);
            end
            
            plot(ax,x,y,'Color',[226/255,132/255,45/255],'LineWidth',2);
        end  
        
    end

    function changeAlternativa(varargin)
        alt=get(cAlternativa,'Value');
        set(cCaso,'String',alternativas(alt).casos);
        draw();
    end
    
    
end
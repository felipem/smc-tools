function [option,name] = poiForm(pois,x,y,z,name)

    option='cancel';   
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
   if ~exist('name','var') 
       %Busco un punto libre
        for i=1:20        
            id=num2str(i,'0%d');
            id=id(end-1:end);
            if ~isKey(pois,['poi',id])
                break;
            end
        end
        textBoxData.tbName=['poi',id];
        strTitle=txt.post('wPoiNewTitle');
        newMode='on';
   else
       textBoxData.tbName=name;
       strTitle=txt.post('wPoiPropTitle');
       newMode='off';
   end   
   textBoxData.tbX=num2str(x,'%.0f');
   textBoxData.tbY=num2str(y,'%.0f');
   textBoxData.tbZ=num2str(z,'%.2f');
   
   scrPos=get(0,'ScreenSize');
   width=400;
   height=225;
   logoPos=[236,28,128,128];
   xLeft=10;    
   xLeftTab=70;
   yPos=140:-25:40;
   
    f = figure( 'MenuBar', 'none', 'Name', strTitle, 'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@cancel,'WindowStyle','modal');
    set(f, 'resize', 'off');
    pos=get(f,'Position');
    uicontrol(f,'Style','pushbutton','String', txt.post('wPoiSelect'),'Position',[30 10 60 20],'Callback',@select);    
    uicontrol(f,'Style','pushbutton','String', txt.post('wPoiMove'),'Position',[120 10 60 20],...
        'Visible',newMode,'Callback',@move);    
    uicontrol(f,'Style','pushbutton','String',txt.post('wPoiCancel'), 'Position',[210 10 60 20],...
        'Visible',newMode,'Callback',@cancel);  
    
    pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    pLogo=uipanel(pMain,'units','pixel','position',logoPos);  
    ax=axes('Parent',pLogo,'YTick',[],'XTick',[],'position',[0,0,1,1]);
    imshow(fullfile('icon','poi_large.png'),'Parent',ax);
    axis(ax,'image');
    
    uicontrol(pMain,'Style','text','String',txt.post('wProfileNewName'),'Position',[xLeft yPos(1) 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbName,'Tag','tbName',...
        'Position',[xLeftTab yPos(1) 70 15],'BackgroundColor','w','Callback',@checkProfileName,...
        'Enable',newMode);
    lUsed=uicontrol(pMain,'Style','text','String','*','Position',[xLeftTab+65 yPos(1) 5 15],'Visible','off');
    
    uicontrol(pMain,'Style','text','String','x (m)','Position',[xLeft yPos(3) 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbX,'Tag','tbX',...
        'Position',[xLeftTab yPos(3) 70 15],'BackgroundColor','w','Enable','off');
    
    uicontrol(pMain,'Style','text','String','y (m)','Position',[xLeft yPos(4) 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbY,'Tag','tbY',...
        'Position',[xLeftTab yPos(4) 70 15],'BackgroundColor','w','Enable','off');
    
    uicontrol(pMain,'Style','text','String','z (m)','Position',[xLeft yPos(5) 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbZ,'Tag','tbZ',...
        'Position',[xLeftTab yPos(5) 70 15],'BackgroundColor','w','Enable','off');
    
    uiwait(f);
   
   
   function select(varargin)
        if ~strcmp(get(lUsed,'Visible'),'on')
            name=textBoxData.tbName;                
            option='select';
            delete(f);            
        end        
   end  

    function move(varargin)
        %if ~strcmp(get(lUsed,'Visible'),'on')
        %    name=textBoxData.tbName; 
            name=[];
            option='move';
            delete(f);          
            pause(0.3);
        %end        
    end  
    
  function cancel(varargin)
        option='cancel';
        name=[];
        close(gcbf);
        pause(0.3);
  end

  function checkProfileName(tbControl,varargin)
        strValue=get(tbControl,'String');
        index=strfind(strValue,'_');
        strValue(index)='-';
        if ~isKey(pois,strValue)
            textBoxData.tbName=strValue;
            set(tbControl,'String',strValue);
            set(lUsed,'Visible','off');
        else
            set(lUsed,'Visible','on');
        end
        
  end
  
end


function poiResultsForm(mopla,poiId,liteData)
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%    
    
    
    
    scrPos=get(0,'ScreenSize');
    width=900;
    height=600;   
    f=figure('Name', [txt.post('wPoiResultsTitle'),' ',poiId], 'NumberTitle','off','MenuBar', 'none',...
        'ResizeFcn',@resize,'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]);   
    color=get(f,'Color');
    
    uicontrol(f,'Style','pushbutton','String', txt.post('wPoiResultsOk'),'Position',[30 10 60 20],'Callback',@exit);    
    uicontrol(f,'Style','text','String',txt.post('wPoiResultsTide'),...
        'Position',[120 6 60 20],'BackgroundColor',color);
    cMarea=uicontrol(f,'Style','popup','String',num2str(mopla.tide,'%.2f'),'Position',[180,10,60,20]...
        ,'BackgroundColor','white','Callback',@updateTable);
    
    
    pos=get(f,'Position');
    panel=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);    
    
    ax1=axes('Parent',panel,'Position',[0.1 0.54 0.45 0.4]);
    ax2=axes('Parent',panel,'Position',[0.1 0.07 0.45 0.4]);
    hsPlot=plot(ax1,liteData.hs(:,1),'b','LineWidth',1.5);
    dirPlot=plot(ax2,liteData.dir(:,1),'r','LineWidth',1.5);
    ylabel(ax1,'H_s (m)');
    ylabel(ax2,'\theta_m (�N)');
    xlim(ax1,[1,length(liteData.hs)]);
    xlim(ax2,[1,length(liteData.hs)]);
    xlabel(ax1,txt.post('wPoiResultsCase'));
    xlabel(ax2,txt.post('wPoiResultsCase'));
    
    data={};
    cNames={txt.post('wPoiResultsMesh'),'Hs (m)','Dir (�N)'};
    mallasStr=mopla.mallas(logical(mopla.propagate));
    for i=1:length(mallasStr)
        data=cat(1,data,{mallasStr{i},num2str(liteData.hs(i,1),'%.2f'),num2str(liteData.dir(i,1),'%.2f')});
    end
    
     tbl=uitable(panel,'units','normalized','Position',[0.65 0 0.35 1],...
         'Data',data,'ColumnName',cNames,'columneditable',[false,false,false],...
         'ColumnWidth','auto');
     
    function resize(varargin)
        pos=get(f,'Position');
        set(panel,'position',[0 40 pos(3) pos(4)-40]);            
    end

    function exit(varargin)
        delete(f);
    end

    function updateTable(varargin)
        value=get(cMarea,'Value');
        hs=liteData.hs(:,value);
        dir=liteData.dir(:,value);
        data=get(tbl,'Data');
        for j=1:length(hs)
            data{j,2}=num2str(hs(j),'%.2f');
            data{j,3}=num2str(dir(j),'%.2f');
        end
        set(tbl,'Data',data);
        set(hsPlot,'YData',hs);
        set(dirPlot,'YData',dir);
    end
end


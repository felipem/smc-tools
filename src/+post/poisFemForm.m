%function [efAll,efDirAll,hasChanges]=poisFemForm(mopla,project,alternativa,showForm,flujoEnergiaPoisLayer)   
function hasChanges=poisFemForm(mopla,project,alternativa,showForm,flujoEnergiaPoisLayer)   

    core.blockMainForm;
    d=dialog('Visible','off');
    g=9.81;
    rho=1025;
    format='%.2f';  %Formato de visualizacion de numeros;
    hasChanges=false;
     
    txt=util.loadLanguage;
    
    
    bRebuild=uicontrol(d,'String',txt.post('wPoisFemRebuild'),'Style','pushbutton','Callback',@reconstruir,'Position',[10,5,80,20]);
    uicontrol(d,'String',txt.post('wPoisFemQuit'),'Style','pushbutton','Callback','delete(gcbf)','Position',[110,5,80,20]);
    
    pos=get(d,'Position');
    table=uitable(d,'Position',[0,35,pos(3),pos(4)-35],'ColumnName',{'Id','x (m)','y (m)','z (m)',txt.post('wPoisFemColEf'),txt.post('wPoisFemColEfDir')});

    
    %Añado las mareas y el nivel
    db=util.loadDb;

    [tideTime,tide]=data.getGOTTemporalSerie(db,mopla.lat,mopla.lon);
    tide=interp1(tideTime,tide,mopla.time);
    %[time,eta]=data.getGOSTemporalSerie(db,mopla.lat,mopla.lon);            
    %eta=interp1(time,eta,mopla.time);  
    nivel=tide;%+eta; ESTO EN EL FUTURO AHORA SOLO MAREA ASTRONÓMICA
    
    nivelMarea=mopla.tide(1); %%Media marea
    if length(mopla.tide)==2 %%%Bajamar + Pleamar
        nivelMarea=(mopla.tide(2)+mopla.tide(1))/2;
    elseif length(mopla.tide)==3
        nivelMarea=mopla.tide(2);
    end
    

    nivel=nivel+nivelMarea;
    
    %%%%Cargo los pois y calculo el FEM
    pois=keys(mopla.pois);
    poisToRebuild={};        
    efAll=zeros(length(pois),1);
    efDirAll=zeros(length(pois),1);
    dataTol={};
    core.blockMainForm; 
    calulaFemAll();   
    setTable();
    
    
     if isempty(poisToRebuild);
        set(bRebuild,'Enable','off');
     end    
    
     if showForm
        set(d,'Visible','on');
        uiwait(d);
     else
         delete(d);
     end
    
    
    function reconstruir(varargin)
       rebuild=post.rebuildPois(mopla,poisToRebuild,project,alternativa);
       if rebuild
            set(bRebuild,'Enable','off');
            calulaFemAll();   
            setTable();            
       end
       hasChanges=true;
       
    end
    
    function [ef,efDir]=calculaFem(poiData,h)
        h_nivel=h+nivel;
        h_nivel(h_nivel<0)=0;
        [k,~]=util.dispersion_hunt(h_nivel,mopla.tp);
        w=2*pi./mopla.tp;
        %cg=sqrt(g* h_nivel);         %%%Celeridad de grupo en shallow water
        kh=k.*h_nivel;
        cg=g*(tanh(kh)+kh./(cosh(kh).^2))./(2*w);
        ef=rho*g*cg.*(poiData.hs).^2/8;          
        n=round((mopla.time(end)-mopla.time(1)+1)*24);
        ef_x=sum(ef.*cosd(poiData.dir))/n;
        ef_y=sum(ef.*sind(poiData.dir))/n;
        ef=sqrt(ef_x^2+ef_y^2);
        efDir=atan2(ef_y,ef_x)*180/pi;
        efDir(efDir<0)=efDir(efDir<0)+360;         
    end

    function calulaFemAll()
        h=waitbar(0,txt.post('pdEnergyFlux'),'WindowStyle','modal');
        poisToRebuild={};
        for i=1:length(pois)       
           if isKey(flujoEnergiaPoisLayer,pois{i})
               efResult=flujoEnergiaPoisLayer(pois{i});
               efAll(i)=efResult.ef;
               efDirAll(i)=efResult.dir;
           else
               poi=mopla.pois(pois{i});
               poiFile=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId,[pois{i},'.poi']);
                if exist(poiFile,'file')
                    poiData=load(poiFile,'-mat');
                    poiData=poiData.outData;
                    [efAll(i),efDirAll(i)]=calculaFem(poiData,poi.z);            
                    efResult.ef=efAll(i);
                    efResult.dir=efDirAll(i);
                    efResult.plot=[];
                    flujoEnergiaPoisLayer(pois{i})=efResult;
                else
                    efAll(i)=NaN;
                    efDirAll(i)=NaN;
                    poisToRebuild=cat(1,poisToRebuild,pois{i});
                end
           end
            h=waitbar(i/length(pois));
        end        
        delete(h);
        pause(0.2);
    end    
    
    function setTable()
        dataTol={};
        for i=1:length(pois)
              poi=mopla.pois(pois{i});
              xStr=num2str(poi.x,format);
              yStr=num2str(poi.y,format);
              zStr=num2str(poi.z,format);
              
              if isnan(efAll(i))
                  efStr=colText('NaN','red');                  
                  efDirStr=colText('NaN','red');                  
              else
                  efStr=num2str(efAll(i),format);
                  efDirStr=num2str(efDirAll(i),format);
              end
           dataTol=cat(1,dataTol,{pois{i},xStr,yStr,zStr,efStr,efDirStr});   
        end
        set(table,'Data',dataTol);
        set(table,'ColumnWidth','auto');
    end    

    function outHtml = colText(inText, inColor)
        % return a HTML string with colored font
        outHtml = ['<html><font color="', ...
        inColor, ...
        '">', ...
        inText, ...
        '</font></html>'];
    end
    
end


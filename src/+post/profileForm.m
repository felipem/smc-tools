function profileForm(mopla,perfilId)
    

    perfil=mopla.perfiles(perfilId);
    
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    scrPos=get(0,'ScreenSize');
    width=800;
    height=500;
    
     f = figure( 'MenuBar', 'none', 'Name',perfilId,...
         'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]);
    set(f, 'resize', 'off');
    color=get(f,'Color');
    
    pos=get(f,'Position');
    pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    uicontrol(f,'Style','pushbutton','String', txt.post('wProfileNewAcept'),'Position',[30 10 60 20],'Callback',@ok);    
    
     uicontrol(f,'Style','text','String',txt.post('wPoiResultsTide'),...
        'Position',[120 6 60 20],'BackgroundColor',color);
    cMarea=uicontrol(f,'Style','popup','String',num2str(mopla.tide,'%.2f'),'Position',[180,10,60,20]...
        ,'BackgroundColor','white','Callback',@updateGraph);
    
     l=sqrt((perfil.x-perfil.x(1)).^2+(perfil.y-perfil.y(1)).^2);
    h=-perfil.h;
    
     polX=[0,l,l(end)];
    polY=[min(h),h,min(h)];
    polSeaX=[0,0,l(end),l(end)];
    polSeaY=[min(h),0,0,min(h)];
    
    axBeach=axes('Parent',pMain);
    sandyBrown=[244 164 96]/255;
    seaGreen=[32 178 170]/255;
    sea=fill(polSeaX,polSeaY,seaGreen,'Parent',axBeach,'EdgeColor','none');
    hold(axBeach,'on');
    beach=fill(polX,polY,sandyBrown,'Parent',axBeach,'EdgeColor','none');
    ylim=get(axBeach,'ylim');
    yLim=ylim(2);
    set(axBeach,'ylim',[min(h),yLim]);
    set(axBeach,'xlim',[0,l(end)]);
    hold(axBeach,'off');
    xlabel(axBeach, 'x (m)');
    ylabel(axBeach, 'h (m)');
    util.setgrid(axBeach,'xy','yx');
    
    updateGraph();
    
    function ok(varargin)
        delete(f);
    end
    
    function updateGraph(varargin)
        value=get(cMarea,'Value');
        tide=mopla.tide(value);
        polY=[min(h)-tide,h-tide,min(h)-tide];
        set(beach,'YData',polY);
        polSeaY=[min(h)-tide,0,0,min(h)-tide];
        set(sea,'YData',polSeaY);
        set(axBeach,'ylim',[min(h)-tide,yLim-tide]);          
        util.setgrid(axBeach,'xy','yx');
    end
end
function outData=rebuildPoi(mopla,poiFile,liteData,progressForm)
    if exist(poiFile,'file')
        outData=load(poiFile,'-mat');
        outData=outData.outData;
        return;
    end
    sourceData=[mopla.hs,mopla.tp,cosd(mopla.dir),sind(mopla.dir)];
    casosMD=mopla.casosMD(logical(mopla.propagate));
    classIndex=casosMD;
    
    sourceData(:,1)=(sourceData(:,1)-min(sourceData(:,1)))/(max(sourceData(:,1))-min(sourceData(:,1)));
    sourceData(:,2)=(sourceData(:,2)-min(sourceData(:,2)))/(max(sourceData(:,2))-min(sourceData(:,2)));
    centers=sourceData(classIndex,:);
     
    hs=liteData.hs;
    dir=liteData.dir;
     
     %%%Corrijo la parte de las funciones de transferencia
     hs_prop=mopla.hs(casosMD);     
     index=hs_prop<mopla.hsMin;     
     for i=1:length(mopla.tide)
         alpha_hs=hs(:,i)./hs_prop;
         hs(index,i)=hs_prop(index).*alpha_hs(index);
     end
    
     if exist('progressForm','var')
        progressForm.setProgress(0.1);
     end
     
     
     %%%Cargo la marea astronůmica
     db=util.loadDb();
     [tideTime,tide]=data.getGOTTemporalSerie(db,mopla.lat,mopla.lon);
     tide=interp1(tideTime,tide,mopla.time);
     tide=tide-min(tide); %Esto no esta muy bien..... ya que depende del periodo temporal. 
     
     %%%Reconstruyo Hs
     outData.hs=zeros(length(mopla.hs),1);
     outData.hs_all=zeros(length(mopla.hs),length(mopla.tide));
     
     for i=1:length(mopla.tide)
        [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, hs(:,i));
        tmp(tmp<0.001)=0.001;
        outData.hs_all(:,i)=tmp;         
        if exist('progressForm','var')
            progressForm.setProgress(0.1+i*0.4/length(mopla.tide));
        end
     end
     outData.hs=post.interpolateTide(tide,mopla.tide,outData.hs_all);
     
     
     
     %%%Reconstruyo Direcion     
     outData.dir_all=zeros(length(mopla.hs),length(mopla.tide));
     for i=1:length(mopla.tide)
        [~,tmp] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, dir(:,i));
        outData.dir_all(:,i)=tmp;     
        progressForm.setProgress(0.5+i*0.4/length(mopla.tide));
     end
     
     cos_dir=post.interpolateTide(tide,mopla.tide,cosd(outData.dir_all));
     sin_dir=post.interpolateTide(tide,mopla.tide,sind(outData.dir_all));
            
     
     
     
     outData.dir=atan2(sin_dir,cos_dir)*180/pi;
     outData.dir(outData.dir<0)=outData.dir(outData.dir<0)+360;
     outData.tp=mopla.tp;
     outData.time=mopla.time;
     
     %%%%%% Grabo la cache
     save(poiFile,'outData','-v7.3');
     
     progressForm.setProgress(1);
end

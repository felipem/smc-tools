function  rebuild=rebuildPois(mopla,pois,project,alternativa)

     %%% Cargo el idioma
     txt=util.loadLanguage;
     %%%%%%%%%%%%%%%%%%%
     
     rebuild=true;
     pf=core.ProgressForm(txt.post('wPoisRebuildTitle'),@cancel);
     pause(0.1);
     stop=false;
     err='';
     
     for i=1:length(pois)
         pf.setMainTitle(sprintf('%s %.0f/%.0f',pois{i},i,length(pois)));
         pf.setSecondTitle(txt.post('wPoisRebuildLoading'));
         pf.setCurrentBar('second');
         if stop,break;end
         [liteData,err]=data.getPOIData(mopla,pois{i},project,alternativa,pf);         
         if ~isempty(err)
             err={txt.post('iOlucaReadErrorLabel'),err};
         end
         if ~isempty(err) || stop;break;end
         pf.setCurrentBar('first');
         pf.setProgress((i-0.5)/length(pois));         
         pf.setCurrentBar('second');
         poiFile=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId,[pois{i},'.poi']);        
         pf.setSecondTitle(txt.post('wPoisRebuildRBF'));
         try
         post.rebuildPoi(mopla,poiFile,liteData,pf);
         catch ex
              err={txt.post('iRebuilErrorLabel'),pois{i},ex.message};
              break;
         end
         pf.setCurrentBar('first');
         pf.setProgress(i/length(pois));         
     end
     delete(pf);
     if ~isempty(err)          
         msgbox(err,txt.post('iOlucaReadErrorTitle'),'error');
         pause(0.05);
         rebuild=false;
     end
     
    function cancel(varargin)
       pf.disableCancel;
       stop=true;
    end
end


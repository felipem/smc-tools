function perfilTTE=rebuildProfile(mopla,tteFile,perfil,progressForm)
    if exist(tteFile,'file')
        perfilTTE=load(tteFile,'-mat');
        if isfield(perfilTTE,'perfilTTE') %formato antiguo
            perfilTTE=perfilTTE.perfilTTE;
            fields=fieldnames(perfilTTE);       
            save(tteFile,'-struct','perfilTTE',fields{:},'-mat');
        end
        return;
    end
    
    %Alturas y profundidades m�nimas para la rotura
    h_b_min=0; 
    hs_b_min=1E-03;
    l_b_min=0;
    
     %%%Creo el source
    casosMD=mopla.casosMD(logical(mopla.propagate));
    sourceData=[mopla.hs,mopla.tp,cosd(mopla.dir),sind(mopla.dir)];    
    
    classIndex=casosMD;

    sourceData(:,1)=(sourceData(:,1)-min(sourceData(:,1)))/(max(sourceData(:,1))-min(sourceData(:,1)));
    sourceData(:,2)=(sourceData(:,2)-min(sourceData(:,2)))/(max(sourceData(:,2))-min(sourceData(:,2)));
    centers=sourceData(classIndex,:);    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    %%% Inicializar MAREA 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    %%%Cargo la marea astron�mica     
    [tideTime,tide]=data.getGOTTemporalSerie(util.loadDb,mopla.lat,mopla.lon);
    tide=interp1(tideTime,tide,mopla.time);
    tide=tide-min(tide); %Esto no est� muy bien..... ya que depende del periodo temporal.    
    %%% Inicializar MAREA
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
     
     if exist('progressForm','var')
        progressForm.setProgress(0.05);
     end
     
        perfilAng=atan2(perfil.y(end)-perfil.y(1),perfil.x(end)-perfil.x(1))*180/pi;
        if isKey(mopla.perfiles,perfil.id)
            
            perfilTTE.hs_b_all=zeros(length(mopla.hs),length(mopla.tide));
            perfilTTE.dir_b_all=zeros(length(mopla.hs),length(mopla.tide));            
            perfilTTE.h_b_all=zeros(length(mopla.hs),length(mopla.tide));
            perfilTTE.l_b_all=zeros(length(mopla.hs),length(mopla.tide));
            perfilTTE.uAvg_all=zeros(length(mopla.hs),length(mopla.tide));
            
            %Hs
            for j=1:length(mopla.tide)
                 [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, squeeze(perfil.hs_b(j,:))');                                        
                   tmp(tmp<hs_b_min)=hs_b_min;
                   perfilTTE.hs_b_all(:,j)=tmp;     
                   if exist('progressForm','var')
                     progressForm.setProgress(0.1+j*0.16/length(mopla.tide));
                   end            
            end
            perfilTTE.hs_b=post.interpolateTide(tide,mopla.tide,perfilTTE.hs_b_all);
            
            %Dir
            for j=1:length(mopla.tide)
                [~,tmp] = dow.InterpolacionRBF_Parametros_old (1, 1, 1, centers, sourceData, squeeze(perfil.dir_b(j,:))');                
                perfilTTE.dir_b_all(:,j)=tmp;
                if exist('progressForm','var')
                   progressForm.setProgress(0.26+j*0.16/length(mopla.tide));
                end            
            end
            cos_dir=post.interpolateTide(tide,mopla.tide,cosd(perfilTTE.dir_b_all));
            sin_dir=post.interpolateTide(tide,mopla.tide,sind(perfilTTE.dir_b_all));
            
            perfilTTE.dir_b=atan2(sin_dir,cos_dir)*180/pi;
            
            %%%Reconstruyo hb             
            for j=1:length(mopla.tide)                
                [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, squeeze(perfil.h_b(j,:))');
                tmp(tmp<h_b_min)=h_b_min;
                perfilTTE.h_b_all(:,j)=tmp;
                if exist('progressForm','var')
                   progressForm.setProgress(0.42+j*0.16/length(mopla.tide));
                end            
            end 
            perfilTTE.h_b=post.interpolateTide(tide,mopla.tide,perfilTTE.h_b_all);  
            
            %%%Reconstruyo l_b     
            for j=1:length(mopla.tide)                
                [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, squeeze(perfil.l_b(j,:))');
                tmp(tmp<l_b_min)=l_b_min;
                perfilTTE.l_b_all(:,j)=tmp;
                if exist('progressForm','var')
                   progressForm.setProgress(0.58+j*0.16/length(mopla.tide));
                end            
            end 
            perfilTTE.l_b=post.interpolateTide(tide,mopla.tide,perfilTTE.l_b_all);  
            
            %%%Reconstruyo corrientes (uAvg)
            if isfield(perfil,'uAvg')
                for j=1:length(mopla.tide)                
                    [~,tmp] = dow.InterpolacionRBF_Parametros_old(1, 0, 0, centers, sourceData, squeeze(perfil.uAvg(j,:))');                    
                    perfilTTE.uAvg_all(:,j)=tmp;
                    if exist('progressForm','var')
                       progressForm.setProgress(0.74+j*0.16/length(mopla.tide));
                    end            
                end 
            perfilTTE.uAvg=post.interpolateTide(tide,mopla.tide,perfilTTE.uAvg_all);  
            end
               
            
            %Angulo respecto al norte
            dir_b_north=90-(perfilTTE.dir_b+perfilAng);
            dir_b_north(dir_b_north>360)=dir_b_north(dir_b_north>360)-360;
            dir_b_north(dir_b_north>360)=dir_b_north(dir_b_north>360)-360;
            perfilTTE.dir_b_north=dir_b_north;
            
            
            perfilTTE.time=mopla.time;
            fields=fieldnames(perfilTTE);       
            save(tteFile,'-struct','perfilTTE',fields{:},'-mat');
            if exist('progressForm','var')
               progressForm.setProgress(1);               
            end            
        end
end
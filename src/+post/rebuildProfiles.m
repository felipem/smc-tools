function rebuild= rebuildProfiles(mopla,perfiles,project,alternativa)
      %%% Cargo el idioma
     txt=util.loadLanguage;
     %%%%%%%%%%%%%%%%%%%
     
     rebuild=true;
     pf=core.ProgressForm(txt.post('wProfilesRebuildTitle'),@cancel);
     pause(0.1);
     stop=false;
     err='';
     for i=1:length(perfiles)         
         pf.setCurrentBar('first');
         pf.setProgress((i-1)/length(perfiles));         
         pf.setMainTitle(sprintf('%s %.0f/%.0f',perfiles{i},i,length(perfiles)));
         pf.setSecondTitle(txt.post('wPoisRebuildLoading'));
         pf.setCurrentBar('second');
         if stop,break;end
         prfFile=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId,[perfiles{i},'_prf.mat']);
         if exist(prfFile,'file')
             perfil=load(prfFile);
             perfil=perfil.perfil;             
         else
             err=sprintf(txt.post('wProfilesRebuildLoadError'),prfFile);
         end
         if ~isempty(err) || stop;break;end         
         
         
         
         pf.setCurrentBar('second');
         tteFile=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId,[perfiles{i},'_tte.mat']);        
         pf.setSecondTitle(txt.post('wPoisRebuildRBF'));
         try
         post.rebuildProfile(mopla,tteFile,perfil,pf);
         catch ex
              err={txt.post('iRebuilErrorLabel'),perfiles{i},ex.message};
              break;
         end
         pf.setCurrentBar('first');
         pf.setProgress(i/length(perfiles));         
     end
     delete(pf);
     if ~isempty(err)          
         msgbox(err,'error','error');
         pause(0.05);
         rebuild=false;
     end
     
     function cancel(varargin)
       pf.disableCancel;
       stop=true;
    end
end


function savePoiResults(mopla,project,altName,poiId,outFolder)
    
    txt=util.loadLanguage;
    %Load Data
    moplaFolder=fullfile(project.folder,altName,'Mopla',mopla.moplaId);
    poiFile=fullfile(moplaFolder,[poiId,'.poi']);
    poiData=load(poiFile,'-mat');
    poiData=poiData.outData;
    poi=mopla.pois(poiId);
    [~,~,ef] = post.fem(poiData.hs,poi.z,mopla.tp,poiData.dir);
    time=datevec(mopla.time);
    %Out Data
    outFile=fullfile(outFolder,[poiId,'.txt']);
    
    
    fid=fopen(outFile,'w');
    fprintf(fid,'%s %s\n','%',mopla.moplaId);
    fprintf(fid,'%s %s:\t %d\n','%',txt.post('wExportResultsPropagationsTitle'),sum(mopla.propagate));
    fprintf(fid,'%s %s:\t %s\n','%',txt.post('wExportResultsTideTitle'),num2str(mopla.tide','%.2f m, '));
    fprintf(fid,'%s %s:\t Lat(UTM)=%.0f m\t Lon(UTM)=%.0f m\n','%',txt.post('wExportResultsLocationTitle'),poi.y,poi.x);
    fprintf(fid,'%s %s:\t %.2f m\n','%',txt.post('wExportResultsDepthTitle'),poi.z);    
    fprintf(fid,'%s YYYY    MM    DD    HH    FE(J/m*s)    DirFE (�N)\n','%'); 
    fprintf(fid,'%6d%6d%6d%6d%12.2f%14.2f\n',[time(:,1:4)';ef';poiData.dir']);
    fclose(fid);
    
    
end


function saveProfileResults(mopla,project,altName,profileId,outFolder)
    
    txt=util.loadLanguage;
    %Load Data
    moplaFolder=fullfile(project.folder,altName,'Mopla',mopla.moplaId);
    tteData = ameva.loadAmevaDataFromPerfil(moplaFolder,mopla,profileId);
    tteFile=fullfile(moplaFolder,[profileId,'_tte.mat']);
    tteData_=load(tteFile,'struct','l_b','h_b');

    perfil=mopla.perfiles(profileId);    
    time=datevec(mopla.time);
    perAng=90-perfil.getAngle; %Respecto al norte
    if perAng<0,perAng=perAng+360;end    
    
    %Out Data
    outFile=fullfile(outFolder,[profileId,'.txt']);        
    fid=fopen(outFile,'w');
    fprintf(fid,'%s %s\n','%',mopla.moplaId);
    fprintf(fid,'%s %s:\t %d\n','%',txt.post('wExportResultsPropagationsTitle'),sum(mopla.propagate));
    fprintf(fid,'%s %s:\t %s\n','%',txt.post('wExportResultsTideTitle'),num2str(mopla.tide','%.2f m, '));
    fprintf(fid,'%s %s:\t X0(UTM)=%.0f m;\t Y0(UTM)=%.0f m;\talpha=%.2f (�N);\tL=%.2f m\n',...
        '%',txt.post('wExportResultsProfileTitle'),perfil.posicion(1,1),perfil.posicion(1,2),perAng,perfil.getLength);
    fprintf(fid,'%s %s:\t D50=%.2f mm;\trho_s=%.2f kg/m3;\tn=%.2f\n',...
        '%',txt.post('wExportResultsSedimentTitle'),perfil.d50,perfil.rho_s,perfil.n);    
    fprintf(fid,'%s YYYY    MM    DD    HH    DB(m)    HB(m)    CI(m)    Q(m3/h)    DirFE (�N)    RU(m)\n','%'); 
    fprintf(fid,'%6d%6d%6d%6d%9.2f%9.2f%9.2f%11.3f%14.2f%9.2f\n'...
        ,[time(:,1:4)';tteData_.l_b';tteData_.h_b';(tteData('ci').getData)';(tteData('Q').getData*3600)';(tteData('dir.b').getData)';(tteData('runUp').getData)']);
    fclose(fid);
    
end

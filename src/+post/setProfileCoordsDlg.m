
function posicion=setProfileCoordsDlg(perfil)

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    textBoxData.tbX1=num2str(perfil.posicion(1,1),'%.0f');
    textBoxData.tbY1=num2str(perfil.posicion(1,2),'%.0f');
    textBoxData.tbX2=num2str(perfil.posicion(2,1),'%.0f');
    textBoxData.tbY2=num2str(perfil.posicion(2,2),'%.0f');    
    
    scrPos=get(0,'ScreenSize');
    width=400;
    height=225;
    logoPos=[236,28,128,128];
    xLeft=10;    
    xLeftTab=100;
    yPos=150:-25:25;
    
     f = figure( 'MenuBar', 'none', 'Name', [txt.post('wProfileEditGeoTitle'),' [',perfil.nombre,']'],...
         'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@cancel);
    set(f, 'resize', 'off','WindowStyle','modal');
    pos=get(f,'Position');
    uicontrol(f,'Style','pushbutton','String', txt.post('wProfileNewAcept'),'Position',[30 10 60 20],'Callback',@ok);    
    uicontrol(f,'Style','pushbutton','String',txt.post('wProfileNewCancel'), 'Position',[120 10 60 20],'Callback',@cancel);  
    
    pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    pLogo=uipanel(pMain,'units','pixel','position',logoPos);  
    ax=axes('Parent',pLogo,'YTick',[],'XTick',[],'position',[0,0,1,1]);
    imshow(fullfile('icon','profile_large.png'),'Parent',ax);
    axis(ax,'image');
    
    uicontrol(pMain,'Style','text','String',['x_',txt.post('wProfileEditGeoLand'),' (m)'],'Position',[xLeft yPos(1) 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbX1,'Tag','tbX1',...
        'Position',[xLeftTab yPos(1) 80 15],'BackgroundColor','w',...
        'Callback', {@checkTextBox,false,perfil.posicion(1,1)-1000,perfil.posicion(1,1)+1000});
    
    uicontrol(pMain,'Style','text','String',['y_',txt.post('wProfileEditGeoLand'),' (m)'],'Position',[xLeft yPos(2) 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbY1,'Tag','tbY1',...
        'Position',[xLeftTab yPos(2) 80 15],'BackgroundColor','w',...
        'Callback', {@checkTextBox,false,perfil.posicion(1,2)-1000,perfil.posicion(1,2)+1000});
    
   uicontrol(pMain,'Style','text','String',['z_',txt.post('wProfileEditGeoLand'),' (m)'],'Position',[xLeft yPos(3) 40 15]);
   uicontrol(pMain,'Style','edit','String',num2str(perfil.h(1),'%.3f'),...
        'Position',[xLeftTab yPos(3) 80 15],'BackgroundColor','w',...
        'Enable','off');
    
    uicontrol(pMain,'Style','text','String',['x_',txt.post('wProfileEditGeoSea'),' (m)'],'Position',[xLeft yPos(4)-10 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbX2,'Tag','tbX2',...
        'Position',[xLeftTab yPos(4)-10 80 15],'BackgroundColor','w',...
        'Callback', {@checkTextBox,false,perfil.posicion(2,1)-1000,perfil.posicion(2,1)+1000});
    
    
    uicontrol(pMain,'Style','text','String',['y_',txt.post('wProfileEditGeoSea'),' (m)'],'Position',[xLeft yPos(5)-10 40 15]);
    uicontrol(pMain,'Style','edit','String',textBoxData.tbY2,'Tag','tbY2',...
        'Position',[xLeftTab yPos(5)-10 80 15],'BackgroundColor','w',...
        'Callback', {@checkTextBox,false,perfil.posicion(2,2)-1000,perfil.posicion(2,2)+1000});
   uicontrol(pMain,'Style','text','String',['z_',txt.post('wProfileEditGeoSea'),' (m)'],'Position',[xLeft yPos(6)-10 40 15]);
   uicontrol(pMain,'Style','edit','String',num2str(perfil.h(end),'%.3f'),...
        'Position',[xLeftTab yPos(6)-10 80 15],'BackgroundColor','w',...
        'Enable','off');
   
    
    posicion=perfil.posicion;
    uiwait(f);
 
    function ok(varargin)
        posicion=[str2double(textBoxData.tbX1),str2double(textBoxData.tbY1);
            str2double(textBoxData.tbX2),str2double(textBoxData.tbY2)];
        delete(f);   
        pause(0.3);
    end

    function cancel(varargin)         
        delete(f);  
        pause(0.3);
    end
      
    function checkTextBox(tbControl,~,isInteger,minValue,maxValue)
            strValue=get(tbControl,'String');
            textBox=get(tbControl,'Tag');

            value=str2double(strValue);

            if ~isnan(value) 
                if isInteger,value=round(value);end
                if value<minValue,value=minValue;end
                if value>maxValue,value=maxValue;end
                value=num2str(value);
                textBoxData.(textBox)=value;
            else
                value=textBoxData.(textBox);            
            end
            tbControls=findall(pMain,'Tag',textBox);
            for j=1:length(tbControls)
                set(tbControls(j),'String',value);
            end
    end

 
    
%     prompt = {['x_',txt.post('wProfileEditGeoLand')],['y_',txt.post('wProfileEditGeoLand')],...
%         ['x_',txt.post('wProfileEditGeoSea')],['y_',txt.post('wProfileEditGeoSea')]};
%     dlg_title = txt.post('wProfileEditGeoTitle');
%     num_lines = 1;
%     def = {num2str(perfil.posicion(1,1)),num2str(perfil.posicion(1,2)),num2str(perfil.posicion(2,1)),num2str(perfil.posicion(2,2))};
%     answer = inputdlg(prompt,dlg_title,num_lines,def);
%     if isempty(answer)        
%         return;
%     end
%     
%     if isEdit
%         pos=[str2double(answer{1}),str2double(answer{2});str2double(answer{3}),str2double(answer{4})];    
%         perfil.setPosicion(pos,alternativa);
%     end
end
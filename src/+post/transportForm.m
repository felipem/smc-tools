function [hasChanges,mopla] = transportForm(mopla,project,alternativa,transportLayer,flujoEnergiaLayer)


    dates=datevec(mopla.time);
    i_=dates(:,1)>1948; %quito el primero a�o porque empieza en Febrero....�?
    times=mopla.time(i_);
    dates=dates(i_,:);
    
    months=util.months;
    hasChanges=false;
    core.blockMainForm;
    try
    txt=util.loadLanguage;
    scrPos=get(0,'ScreenSize');
    width=800;
    height=700;
    
    d=dialog('Visible','off','Name',txt.post('wTransportTitle'),...
    'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]);  
    format='%.2f';  %Formato de visualizacion de numeros;
    hasChanges=false;
    color=get(d,'Color');
     pos=get(d,'Position');
    
    %%%Botones de arriba
    methods={'Cerc','Kamphuis','Bayram'};
    kM=kMethods(mopla.transportModel);
    
    uicontrol(d,'Style','text','String',txt.post('wTransportMethod'),...
        'Position',[10 pos(4)-30 60 20],'BackgroundColor',color);
     cMethod=uicontrol(d,'Style','popup','String',methods,'Position',[80,pos(4)-25,80,20]...
        ,'BackgroundColor','white','Callback',@refreshK);
    
    uicontrol(d,'Style','text','String','k: ',...
        'Position',[170 pos(4)-30 30 20],'BackgroundColor',color);
     cK=uicontrol(d,'Style','popup','String',kM,'Position',[200,pos(4)-25,80,20]...
        ,'BackgroundColor','white');
    
    uicontrol(d,'Style','text','String',txt.post('wTransportPeriod'),...
        'Position',[290 pos(4)-30 40 20],'BackgroundColor',color);
    
    strPeriod=txt.post('wTransportPeriodTotal');
     if strcmp(mopla.transportPeriod.type,'MediaAnual')                                
            strPeriod=num2str(mopla.transportPeriod.ini);
     elseif strcmp(mopla.transportPeriod.type,'MediaEstacional')                                               
            strPeriod=[months{mopla.transportPeriod.ini},'-',months{mopla.transportPeriod.end}];                
     elseif strcmp(mopla.transportPeriod.type,'MediaMensual')                
            strPeriod=months{mopla.transportPeriod.ini};                
     end                             
         
    
    lPeriod=uicontrol(d,'Style','text','String',strPeriod,...
        'Position',[330 pos(4)-30 70 20],'BackgroundColor',color,'FontWeight','bold');
    
    bPeriod=uicontrol(d,'Style','pushbutton',...
        'tooltipstring',txt.post('wTransportPeriodHelp'),...
        'Position',[410,pos(4)-30,24,24],'Callback',@definePeriod);  
    util.setControlIcon(bPeriod,'calendar.gif');
    
     bCalculate=uicontrol(d,'Style','pushbutton',...
        'tooltipstring',txt.post('wTransportCalculateHelp'),...
        'Position',[450,pos(4)-30,24,24],'Callback',@refreshTransporte);  
    
     util.setControlIcon(bCalculate,'calculator.gif');
    
    bRebuild=uicontrol(d,'String',txt.post('wTransportRebuild'),'Style','pushbutton','Callback',@reconstruir,'Position',[10,5,80,20]);
    uicontrol(d,'String',txt.post('wTransportQuit'),'Style','pushbutton','Callback',@quit,'Position',[110,5,80,20]);    
      
    table=uitable(d,'Position',[0,35,pos(3),pos(4)-70],'ColumnName',{'Id',txt.post('wPoisFemColEf'),txt.post('wPoisFemColEfDir'),'Q [m^3]','Q+ [m^3]','Q- [m^3]'});
    

    
    %%%Compruebo si hay perfiles con rotura calculada
    moplaDir=fullfile(project.folder,alternativa.nombre,'Mopla',mopla.moplaId);
    perfiles=keys(mopla.perfiles);
    
    perfilesBrk={};
    for i=1:length(perfiles)
        prfFile=fullfile(moplaDir,[perfiles{i},'_prf.mat']);
        if exist(prfFile,'file')
            perfilesBrk=cat(1,perfilesBrk,perfiles{i});
        end
    end
    
    
    if isempty(perfilesBrk)
         msgbox(txt.post('wTransportNoBreaking'),'error','warn');         
         pause(0.05);
         core.blockMainForm;
         quit();
    end
   
    methodIndex=find(strcmp(methods,mopla.transportModel));
    set(cMethod,'Value',methodIndex);
    
    
     profilesToRebuild={};
     efAll=zeros(length(perfilesBrk),1);
    efDirAll=zeros(length(perfilesBrk),1);
    qAll=zeros(length(perfilesBrk),1);
    qPlusAll=zeros(length(perfilesBrk),1);
    qMinusAll=zeros(length(perfilesBrk),1);
    dataTol={};
    period=mopla.transportPeriod;
    core.blockMainForm; 
    calculaTransporteAll();   
    setTable();
    
    if isempty(profilesToRebuild);
        set(bRebuild,'Enable','off');
    end    
     
      set(d,'Visible','on');
      uiwait(d);
    catch ex        
        msgbox(ex.message,'error','error');
    end
    
    function refreshTransporte(varargin)
        calculaTransporteAll(true);
        setTable();
        hasChanges=true;
    end

    function calculaTransporteAll(refresh)
        if ~exist('refresh','var')
            refresh=false;
        end
        mopla.transportPeriod=period;
        h=waitbar(0,txt.post('pdTransport'),'WindowStyle','modal');
       
        profilesToRebuild={};
        for i=1:length(perfilesBrk)       
           if isKey(transportLayer,perfilesBrk{i}) && ~refresh
              resultQ=transportLayer(perfilesBrk{i});
              resultEF=flujoEnergiaLayer(perfilesBrk{i});
               efAll(i)=resultEF.ef;
               efDirAll(i)=resultEF.efDir;
               qAll(i)=resultQ.q;
               qPlusAll(i)=resultQ.qPlus;
               qMinusAll(i)=resultQ.qMinus;
           else
               p=mopla.perfiles(perfilesBrk{i});
               tteFile=fullfile(moplaDir,[perfilesBrk{i},'_tte.mat']);
                if exist(tteFile,'file')
                    pTTE=load(tteFile,'-mat','hs_b','h_b','l_b','dir_b','uAvg');
                    if isfield(pTTE,'uAvg')
                        uAvg=pTTE.uAvg;
                    else
                        uAvg=[];
                    end
                    [qAll(i),qPlusAll(i),qMinusAll(i),efAll(i),efDirAll(i)]=calculaTransporte(p,pTTE.hs_b,pTTE.h_b,pTTE.l_b,pTTE.dir_b,uAvg);            
                    resultEF.plot=[];
                    resultQ.plot=[];
                    if isKey(transportLayer,perfilesBrk{i})
                        resultQ=transportLayer(perfilesBrk{i});
                        resultEF=flujoEnergiaLayer(perfilesBrk{i});
                    end
                    resultEF.ef=efAll(i);
                    resultEF.efDir=efDirAll(i);
                    resultQ.q=qAll(i);
                    resultQ.qPlus=qPlusAll(i);
                    resultQ.qMinus=qMinusAll(i);
                    
                    transportLayer(perfilesBrk{i})=resultQ;
                    flujoEnergiaLayer(perfilesBrk{i})=resultEF;
                else
                    efAll(i)=NaN;
                    efDirAll(i)=NaN;
                    qAll(i)=NaN;
                    qPlusAll(i)=NaN;
                    qMinusAll(i)=NaN;
                    profilesToRebuild=cat(1,profilesToRebuild,perfilesBrk{i});
                end
           end
            h=waitbar(i/length(perfilesBrk));
        end        
        delete(h);
        pause(0.2);
        
    end

    function [q,qPlus,qMinus,ef,efDir]=calculaTransporte(perfil,hs_b,h_b,l_b,dir_b,uAvg)
        method=methods{get(cMethod,'Value')};
        kMethods=kMethods(method);
        kMethod=kMethods{get(cK,'Value')}; 
        mopla.transportModel=method;
        mopla.kModel=kMethod;
        q=post.longshoreTransport(hs_b,h_b,dir_b,mopla.tp,l_b,uAvg,perfil,method,kMethod);        

        perfilAng=atan2(perfil.y(end)-perfil.y(1),perfil.x(end)-perfil.x(1))*180/pi;
        dir_x=dir_b+perfilAng-180; %%Direcci�n repecto a X_UTM
        [ef_x,ef_y] = post.fem(hs_b,h_b,mopla.tp,dir_x);
        
        [index,n,nHours]=getTransporteTimeIndex;        
        q=q(index);  
        qPlus=sum(q(q>=0))*3600/n;
        qMinus=sum(q(q<0))*3600/n;
        q=sum(q)*3600/n;
        
        efDir=270-atan2(sum(ef_y(index)),sum(ef_x(index)))*180/pi;
        if efDir<0,efDir=efDir+360;end
        if efDir>360,efDir=efDir-360;end
        ef_x=sum(ef_x(index))/nHours;
        ef_y=sum(ef_y(index))/nHours;
        ef=sqrt(ef_x^2+ef_y^2);
    end

    function setTable()
         dataTol={};
        for i=1:length(perfilesBrk)
              if isnan(efAll(i))
                  qStr=colText('NaN','red');  
                  qPlusStr=colText('NaN','red');  
                  qMinusStr=colText('NaN','red');  
                  efStr=colText('NaN','red');                  
                  efDirStr=colText('NaN','red');                  
              else
                  qStr=num2str(qAll(i),format);
                  qPlusStr=num2str(qPlusAll(i),format);
                  qMinusStr=num2str(qMinusAll(i),format);
                  efStr=num2str(efAll(i),format);
                  efDirStr=num2str(efDirAll(i),format);
              end
           dataTol=cat(1,dataTol,{perfilesBrk{i},efStr,efDirStr,qStr,qPlusStr,qMinusStr});
        end
        set(table,'Data',dataTol);
        set(table,'ColumnWidth','auto');
    end
    
    function reconstruir(varargin)
       rebuild=post.rebuildProfiles(mopla,profilesToRebuild,project,alternativa);
       if rebuild
            set(bRebuild,'Enable','off');
            calculaTransporteAll(true);   
            setTable();            
       end
       hasChanges=true;
    end

   function refreshK(varargin)
        method=methods{get(cMethod,'Value')};
        set(cK,'String',kMethods(method));
        set(cK,'Value',1);
    end

   function kM=kMethods(method)
        switch method
            case  'Cerc'
             kM={'Valle','Mil-Homens'};
            case 'Kamphuis'                
            kM={'Kamphuis','Schoones','Mil-Homens'};
            case 'Bayram'
            kM={'Bayram','Mil-Homens'};
        end
    end

   function quit(varargin)
        delete(d);
        pause(0.15);
    end

   function [index,n,nHours]=getTransporteTimeIndex()
            yearIni=dates(1,1);
            yearEnd=dates(end,1);
            
            dd=datevec(mopla.time);
            index=dd(:,1)>=yearIni;
            
            n=yearEnd-yearIni+1;
            ini_=mopla.transportPeriod.ini;
            end_=mopla.transportPeriod.end;            
            
            if strcmp(mopla.transportPeriod.type,'MediaAnualTotal')                
                nHours=n*365*24;
            elseif strcmp(mopla.transportPeriod.type,'MediaAnual')
                nHours=365*24;
                index=dates(:,1)==ini_;
                n=1;
                
            elseif strcmp(mopla.transportPeriod.type,'MediaEstacional')                               
                if ini_<=end_                    
                    index=dates(:,2)>=ini_ & dates(:,2)<=end_;                    
                    nHours=0;
                    for j=ini_:end_
                        nHours=nHours+eomday(dates(end,1),j);
                    end
                else                
                    index=dates(:,2)>=ini_ | dates(:,2)<=end_; 
                    for j=ini_:end_+12
                        nHours=nHours+eomday(dates(end,1),j);
                    end
                end
              
                
            elseif strcmp(mopla.transportPeriod.type,'MediaMensual')                
                index=dates(:,2)==ini_;
                nHours=eomday(dates(end,1),ini_);
                
            end                
   end
    
   function definePeriod(varargin)
         period=post.editMapPeriodForm(period,times);
         strPeriod=txt.post('wTransportPeriodTotal');
         if strcmp(period.type,'MediaAnual')                                
                strPeriod=num2str(period.ini);
         elseif strcmp(period.type,'MediaEstacional')                                               
                strPeriod=[months{period.ini},'-',months{period.end}];                
         elseif strcmp(period.type,'MediaMensual')                
                strPeriod=months{period.ini};                
         end                             
         set(lPeriod,'String',strPeriod);
   end

    function outHtml = colText(inText, inColor)
        % return a HTML string with colored font
        outHtml = ['<html><font color="', ...
        inColor, ...
        '">', ...
        inText, ...
        '</font></html>'];
    end
end


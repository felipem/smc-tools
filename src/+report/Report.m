classdef Report < handle
    
    properties
        % Fonts 
        fontName = 'Arial';
        fontSizeLarge = 13;
        fontSizeMedium = 8;
        fontSizeEquation = 7;
        fontSizeSmall = 6;
        fontSomText = 3.5;
        
        % Panels
        mainPanel = [];
        secondPanel = [];        
        mapAxes=[];
    end
    
    properties (Access=private)
        titleAnnotation = [];
        fig=[];
        
        % Size
        width=1;
        widthMain=0.75;
        widthSecond=0.25;
        widthInfoLabel=0.05;
        height=0.9;
        heightTop=0.063;
        heightSecond= 0.38;
        heightMap=0.24
        heightLogo=0.097;
        heightInfo=0.12/4;
        edgeWidth=0.5;
        
        % Language & Config
        txt;       
        cfg;
       
    end
    
    properties (Dependent=true)
        title;
        visible;
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Constructor / Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function self = Report(caseInfo, batData,title)
            % Constructor: stores label dictionary
            %self.txt.report = util.loadText('report');               
            %%% Cargo el idioma
            self.txt=util.loadLanguage;
            self.cfg=util.loadConfig;
            %%%%%%%%%%%%%%%%%%%
            
            % Create empty figure
            self.fig = figure('PaperUnits','normalized','Color','w', 'Visible', 'off','PaperOrientation','landscape','PaperPosition', [0 0 1 1],'units','centimeters','Position',[0 0 42 29.7],'Renderer','zbuffer');
            
            % Create title and information annotation
            self.writeInfo(caseInfo,title);
            
            % Create report and logo panels without edges
            self.mainPanel = self.createPanel(self.getElementPosition('main'), false);
            self.secondPanel = self.createPanel(self.getElementPosition('second'), false);

            self.putLogos()
            
            % Draw map            
            p = self.createPanel(self.getElementPosition('map'), false);            
            self.mapAxes=self.addMap([0 0 1 1],batData,p);
            set(self.mapAxes,'YTick',[],'XTick',[],'XColor','w','YColor','w','box','off');
            
            % Create edges grid 
            self.drawEdges();
        end
        
        function delete(self)
            try
                delete(self.fig);
            catch
            end
        end
        
        function writeInfo(self, caseInfo,title)
            % caseInfo: Matlab structure. Fields:
            % caseInfo.lat
            % caseInfo.lon
            % caseInfo.depth
            % caseInfo.yearIni
            % caseInfo.yearEnd
            % caseInfo.projectDes
            % caseInfo.alternativeName
            % caseInfo.title
            
            % Annotation array
            note=[];
            
            % Left Info. Annotations
            note(1) = annotation('textbox', self.getElementPosition('info_coords_label'), 'String', self.txt.report('lDowWave'),'LineWidth',self.edgeWidth);
            note(2) = annotation('textbox', self.getElementPosition('info_period_label'), 'String', self.txt.report('lPeriod'),'LineWidth',self.edgeWidth);
            note(3) = annotation('textbox', self.getElementPosition('info_project_label'), 'String', self.txt.report('lProject'),'LineWidth',self.edgeWidth);
            note(4) = annotation('textbox', self.getElementPosition('info_alternativa_label'), 'String', self.txt.report('lAlternativa'),'LineWidth',self.edgeWidth);
            
            % Right Info. Annotations
            note(5) = annotation('textbox', self.getElementPosition('info_coords'),'LineWidth',self.edgeWidth);
            set(note(5), 'String', sprintf([self.txt.report('lat'), ': %.1f �  ', self.txt.report('lon'), ': %.1f �  ', self.txt.report('depth'), ': %.1f m'], caseInfo.lat, caseInfo.lon, caseInfo.depth));
            
            note(6) = annotation('textbox', self.getElementPosition('info_period'),'LineWidth',self.edgeWidth);
            set(note(6), 'String', sprintf('%d-%d', caseInfo.yearIni, caseInfo.yearEnd));
            
            note(7) = annotation('textbox', self.getElementPosition('info_project'), 'String', caseInfo.projectDes,'LineWidth',self.edgeWidth);
            note(8) = annotation('textbox', self.getElementPosition('info_alternativa'), 'String', caseInfo.alternativeName,'LineWidth',self.edgeWidth);
            
            % Set Annotation properties
            for i = 1 : length(note);
                set(note(i), 'FontName', self.fontName, 'FontSize', self.fontSizeSmall, 'HorizontalAlignment', 'left','VerticalAlignment', 'middle', 'BackgroundColor', 'none','Interpreter','none');
            end
            
            % Create titlePanel
            self.titleAnnotation = annotation('textbox', self.getElementPosition('title'), 'String', title, 'EdgeColor', 'none');
            set(self.titleAnnotation, 'FontName', self.fontName, 'FontSize', self.fontSizeLarge, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle');
        end
    end    

        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Get / Set
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods        
        function set.visible(self,value)
            if value
                set(self.fig,'Visible','on');
            else
                set(self.fig,'Visible','off');
            end
        end
        
        function value=get.visible(self)
            val=get(self.fig,'Visible');
            if strcmp(val,'on')
                value=true;
            else
                value=false;
            end            
        end
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% General
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function printPng(self, file)
            print(self.fig, file, '-dpng', '-r300');
            %saveas(self.fig,file,'fig');
            disp('Printed.');
        end
        
        function printPdf(self, file)
            print(self.fig, file, '-dpdf');
            disp('Printed.');
        end
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Individual Graphs
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function addTimeSeries(self, relPosition, amevaVar, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax = axes('Parent', parent,'Position', relPosition);
            
            % Data plot
            time = amevaVar.getTime;
            data = amevaVar.getData;
            plot(ax, time, data, 'linewidth', 0.1);
            
            % XTicks each 10 years
            datetick(ax, 'x', 'yyyy');
            xTicks = get(ax, 'Xtick');
            xTicksLab = (get(ax, 'XtickLabel'));
            numxTicksLab = str2num(xTicksLab);
            tenMxTicksLab = numxTicksLab(rem(numxTicksLab,10)==0);
            tenMxTicks = xTicks(rem(numxTicksLab,10)==0);
            set(ax, 'Xtick', tenMxTicks);
            set(ax, 'XtickLabel', num2str(tenMxTicksLab));
            
            % Plot configuration
            xlim(ax, [time(1), time(end)]);
            grid(ax, 'on');
            xlabel(ax,self.txt.report('xYear'));
            ylabel(ax, [self.txt.report([amevaVar.name,'Short']) ' (' amevaVar.units ')' ]);
            title(ax, [self.txt.report('tSeries') ' ' self.txt.report(amevaVar.name)]);
            
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);
        end
        
        function addDirRose(self, relPosition, amevaVar,amevaVarDir, parent, seasonsStr)
          
            if ~exist('parent','var')
                parent = self.mainPanel;
            end   
            if ~exist('seasonsStr', 'var')
                seasonsStr = '';
            end
            ax = axes('Parent', parent,'Position', relPosition,'Visible','off');
            
            % Data plot
            directions = amevaVarDir.getData+180;
            data = amevaVar.getData;
            labLegend = [self.txt.report([amevaVar.name,'Short']) ' (' amevaVar.units ')' ];

            
            VH=linspace(floor(min(data)),ceil(max(data)),8);
%             deltaH = round((ceil(max(data)) - floor(min(data))) / 4);
%             VH = floor(min(data)) : deltaH/2.0 : ceil(max(data));
%               VH=linspace(floor(min(data)),ceil(max(data)),7);
%              VH=floor(min(data)) : dVal : ceil(max(data));
%             if deltaH == 0
%                 deltaH = round((ceil(max(data)) - floor(min(data))) / 2);
%                 VH = floor(min(data)) : deltaH/4.0 : ceil(max(data));
%                 if deltaH == 0
%                      deltaH = (max(data) - min(data)) / 8;
%                      VH = floor(min(data)) : deltaH : ceil(max(data));
%                 end
%             end   
            
            report.iha_wind_rose(directions,data,'ax',[ax 0.45 0.5 1.2],'percbg','none','n',16,'di',VH,'dtype','meteo','quad',1,'idioma','eng','lablegend',labLegend,'parent',ax);
            
            % Plot configuration
            set(get(ax,'title'),'string',[self.txt.report('tRose') ' ' self.txt.report([amevaVar.name,'Short']), ' ' seasonsStr]);%,'FontWeight','b');
            self.setFormat([ax,findall(ax,'type','text')']);
            
        end
        
        function addHist(self, relPosition, amevaVar, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax = axes('Parent', parent,'Position', relPosition);
            
            % Data plot
            N = amevaVar.getHistN();
            X = amevaVar.getHistX();
            bar(ax, X, 100 * (N./sum(N)), 1.0, 'b');
            
            % Plot configuration
            xlabel(ax, [self.txt.report([amevaVar.name,'Short']) ' (' amevaVar.units ')' ]);
            ylabel(ax, self.txt.report('labOccurFreq'));
            title(ax, [self.txt.report('tHist') ' ' self.txt.report(amevaVar.name)]);
            
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);
        end
        
        function [ax] = addPDF2D(self, relPosition, amevaVar1, amevaVar2, parent)
            util.freezeColors(self.mapAxes);
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax = axes('Parent', parent,'Position', relPosition);           
         
            % Data Plot
            x = amevaVar1.getData();
            time = amevaVar1.getTime();
            y = amevaVar2.getIntervalData(time(1),time(end));
            ndx = round(3/2+log(length(x))/log(2))*2;
            [data,c] = hist3([x,y],[ndx,ndx]);
            data = data/sum(sum(data));
            [X,Y] = meshgrid(c{1},c{2});
            colormap(ax,'jet');
            pcolor(ax,X,Y,data');  
            
            % Colorbar
            cb = colorbar('peer',ax);   
            set(get(cb,'Title'),'String','Prob.','fontsize',8); %%%% aqui puse un tama�o de texto! Quetzal
            
            % Plot configuration
            shading(ax,'flat');
            xlabel(ax, [self.txt.report([amevaVar1.name,'Short']),' (',amevaVar1.units,')']);
            ylabel(ax, [self.txt.report([amevaVar2.name,'Short']),' (',amevaVar2.units,')']);
            title(ax,[self.txt.report('tPDF'),' ', self.txt.report([amevaVar1.name,'Short']),...
                '-', self.txt.report([amevaVar2.name,'Short'])]);%, 'FontWeight','bold');
            
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),cb]);
        end
        
        function yPeak = addPDF2D_Percentile(self, relPosition, amevaVarHs, amevaVarTp, percentile, PDFaxes, colorPlot, fixedYlim, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax = axes('Parent', parent,'Position', relPosition);
            
            % Raw data
            hs = amevaVarHs.getData();
            time = amevaVarHs.getTime();
            tp = amevaVarTp.getIntervalData(time(1),time(end));
            
            % Hs Percentiles
            hsPerc = prctile(hs, percentile);
            
            hsRange = 0.05; % TODO: ESTE RANGO DEBE CUMPLIR CONDICIONES: SER PEQUE�O PERO DAR AL MENOS 30 DATOS. PROB EN PERCENTILES EXTREMOS
            auxPos = hs >= hsPerc - hsRange & hs <= hsPerc + hsRange;
                        
            tpHsPerc = tp(auxPos);
            [fPerc, xiPerc] = ksdensity(tpHsPerc);
            
            % PDF2D plot  
            hold(PDFaxes, 'on');
            xLimAx = get(PDFaxes, 'Xlim'); XSize = diff(xLimAx);
            plot(PDFaxes, [hsPerc, hsPerc],[min(tp), max(tp)], 'Color', colorPlot, 'lineWidth', 1.5);
            strPerc=['Hs_{',num2str(percentile),'%}'];
            if percentile==100*(1-12/(365*24)); %Entonces es Hs12
                strPerc='Hs_{12}';
            end
            t=text(hsPerc+hsRange, 0.95*max(tp),strPerc,'Color', colorPlot,'Parent',PDFaxes);
            hold(PDFaxes, 'off');            
            
            % ksdensity plot
            plot(ax, xiPerc, fPerc, 'linewidth', 1.5, 'Color', colorPlot);

            % Plot configuration
            xlim(ax, [min(tp), max(tp)]);
            grid(ax, 'on');
            ylabel(ax, 'f(x)');
            xlabel(ax, [self.txt.report([amevaVarTp.name,'Short']) '( ' self.txt.report([amevaVarTp.name,'Short'])  ' | ' strPerc ') (s)']);
            title(ax, [self.txt.report('tPDFperc') ' ' strPerc]);
            if exist('fixedYlim','var')
               ylim(ax, fixedYlim);
            end
            % return yLim
            %returnYlim = get(ax,'YLim');            
            [~,index]=max(fPerc);
           % yPeak=tp(index);
           yPeak=xiPerc(index); %%% aqui es tpHsPerc %%% Quetzal           
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),t],'FontSize',self.fontSizeSmall);
        end
        
        function param = addRmGevScalar(self, relPosition, amevaVar, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax = axes('Parent', parent,'Position', relPosition, 'FontSize', self.fontSizeSmall, 'FontName', self.fontName);
            
            % Data plot
            y = amevaVar.getData;
            [param,x,xFit,yFit] = ameva.gevRM(y);            
            plot(ax,xFit,yFit,'r','LineWidth',1.5);
            hold(ax,'on');
            plot(ax,x,y,'.k');
            
            % XTick configuration
            probFigure = [0.001 0.2 0.5 0.8 0.90 0.95 0.98 0.99 0.999];
            xFigure = -log(log(1./probFigure));           
            set(ax, 'XTick',xFigure,'XTickLabel',probFigure,'XGrid','on','YGrid','on');
            
            % Plot configuration
            xlim(ax, [min(xFigure) max(xFigure)]);
            title(ax, sprintf(self.txt.report('tRmGev')));
            ylabel(ax, [self.txt.report([amevaVar.name,'Short']),' (',amevaVar.units,') ']);
            xlabel(ax, [self.txt.report('xRmGev'),', F(',self.txt.report([amevaVar.name,'Short']),')']);
            l = legend(ax,['\mu= ',num2str(param.mu,'%.3f'),', \psi= ',num2str(param.psi,'%.3f'),', \xi= ',num2str(param.xi,'%.3f')],'Location','SouthEast');            
            
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
             
            % Fix legend
%             legChild = get(l,'Children');
%             delete(legChild(2));
%             set(legChild(3),'Position',[0.15 0.5 0])
            
           
        end
        
        function addClasifficationChart(self, relPosition, prob, amevaVar1, amevaVar2, amevaVarDir, subSetIndex, parent)
            util.freezeColors(self.mapAxes);
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            
            nTotal=length(prob);
            n=floor(sqrt(nTotal));
            m=ceil(nTotal/n);
            resto=n*m-nTotal;
            
            graphPosition=relPosition;
            graphPosition(3)=graphPosition(3)*2/3;
            cbWidth=0.05;
            
            %%%%Grafico de probabilidad
            cm=1-copper(15);
            cm=cm([1:9,12],:);
            prob=cat(1,prob,nan(resto,1));
            logProb=log(prob);
            probNorm=(logProb-min(logProb))/(max(logProb)-min(logProb));
            index=round(probNorm*9+1);
            index(isnan(index))=1;
            
            ax=self.drawSomPatch(m,n,1,cm(index,:),'none','',[],graphPosition,parent);
            cbPosition=[relPosition(1)+0.7*relPosition(3),relPosition(2)+0.06,cbWidth*relPosition(3),relPosition(4)-0.12];
            options.logScale=true;options.title='Prob (%)';
            self.drawColorBar(cbPosition,cm,[min(logProb),max(logProb)]+4.6052,options,parent);
            
            hold(ax,'on');
            %%%%Grafico de var1
            cm=repmat([1 1 1],14,1)-gray(14);
            cm=cm(1:11,:);
            var=amevaVar1.getSubsetData(subSetIndex);
            var=cat(1,var,zeros(resto,1));
            varNorm=(var-min(var))/(max(var)-min(var));
            index=round(varNorm*10+1);
            self.drawSomPatch(m,n,0.6,cm(index,:),'none','',ax);
            cbPosition=[relPosition(1)+0.7*relPosition(3)+4*cbWidth,relPosition(2)+0.06,cbWidth*relPosition(3),relPosition(4)-0.12];
            options.logScale=false;options.title=[self.txt.report([amevaVar1.name,'Short']) ' (' amevaVar1.units ')' ];
            self.drawColorBar(cbPosition,cm,[min(var),max(var)],options,parent);
            
            %%%%Grafico de var2
            cm=flipud(hot(256));
            var=amevaVar2.getSubsetData(subSetIndex);
            var=cat(1,var,zeros(resto,1));
            varNorm=(var-min(var))/(max(var)-min(var));
            index=round(varNorm*255+1);
            self.drawSomPatch(m,n,0.3,cm(index,:),'none','',ax);
            cbPosition=[relPosition(1)+0.7*relPosition(3)+2*cbWidth,relPosition(2)+0.06,cbWidth*relPosition(3),relPosition(4)-0.12];
            options.logScale=false;options.title=[self.txt.report([amevaVar2.name,'Short']) ' (' amevaVar2.units ')' ];
            self.drawColorBar(cbPosition,cm,[min(var),max(var)],options,parent);
            
            %%%%Grafico de Direccion
            dir=amevaVarDir.getSubsetData(subSetIndex);
            dir=cat(1,dir,nan(resto,1));
            self.drawSomVector(m,n,dir,ax);
            hold(ax,'off');
            
        end
        
        function addClasifficationScatterChart(self, relPosition, amevaVar1, amevaVar2, subSetIndex, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax=axes('Parent',parent,'Position',relPosition);
            plot(ax,amevaVar1.getData,amevaVar2.getData,'o','MarkerFaceColor',[0.7,0.7,0.7],'MarkerEdgeColor','none','MarkerSize',1);
            hold(ax,'on');
            plot(ax,amevaVar1.getSubsetData(subSetIndex),amevaVar2.getSubsetData(subSetIndex),'.r');
            hold(ax,'off');
            xlabel(ax,[self.txt.report([amevaVar1.name,'Short']) ' (' amevaVar1.units ')' ]);
            ylabel(ax,[self.txt.report([amevaVar2.name,'Short']) ' (' amevaVar2.units ')' ]);
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);
            
        end
        
        function [fitParameters,bandCoeff,threshold]=addPotChart(self,relPosition,amevaVar,parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax=axes('Parent',parent,'Position',relPosition);
            

            time=amevaVar.getTime;
            var=amevaVar.getData;
            
            timeVec=datevec(time);
            nYears=timeVec(end,1)-timeVec(1,1)+1;
            Serie=[time,timeVec(:,1:4),var];
            thresholdPerc=99.5;
            indtemp=3;
            threshold=prctile(var,thresholdPerc);

            [ymax,~,~,~,xmax]=ameva.SeleccionaMaximosIndepPOT(Serie,threshold,indtemp);
            [quanGEVX,Ts,Tapprox,Xor,fitParameters,stdDqX]=ameva.potfit(ymax,threshold,nYears,xmax);
            bandCoeff=polyfit(quanGEVX,stdDqX,3);
            
            p=semilogx(ax,Ts,quanGEVX,'-r','Linewidth',1.2);
            hold(ax,'on');
            bc=semilogx(ax,Ts,quanGEVX+polyval(bandCoeff,quanGEVX),'k--','Linewidth',1.1);
            semilogx(ax,Ts,quanGEVX-polyval(bandCoeff,quanGEVX),'k--','Linewidth',1.1);
            semilogx(ax,Tapprox,Xor,'.b');            
            hold(ax,'off');
            
            legend(ax,[p,bc],{['Fit (',amevaVar.name,') '],'95%'},'Location','SouthEast');
            title(ax, sprintf(self.txt.report('tPot')));
            xlabel(ax, sprintf(self.txt.report('xPot')));
            ylabel(ax, [amevaVar.name,' (',amevaVar.units,') ']);
            
            xticks=[1 2 5 10 20 50 100 250 500];
            grid(ax,'on');%grid(ax,'minor')
            hlim=get(ax,'ylim');
            %hlim=[0 hlim(2)];
            set(ax,'ylim',hlim,'xlim',[1 Ts(end)],'XTick',xticks,'XMinorTick','off','XMinorGrid','off');
            
           self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);
            
            
        end 
        
        function fitParameters=addGevChart(self,relPosition,amevaVar,parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax=axes('Parent',parent,'Position',relPosition);
            Ymax=amevaVar.getMaxYearData;            
            tipdis='GEV';
            % Ajuste a la GEV 
            [~,bestsolution,~] =ameva.sce(Ymax,tipdis);
            
            psifinal=bestsolution(1);
            xifinal=bestsolution(2);
            mufinal=bestsolution(3);

            n=length(bestsolution);
            MIO=zeros(n,n);
    
            for i=1:n  % columnas
                for j=1:n  % filas
                    MIO(i,j) = ameva.derivadasegunda(bestsolution,i,j,Ymax);
                end
            end
            
            J=inv(MIO);
            Yord=sort(Ymax);
            ndatos=length(Ymax);
            for i=1:ndatos
                Prob(i)=i/(ndatos+1);
            end
            RRR=-1./log(Prob) ;
            PeriodoRetorno=[0.5 1 2 3 4 5 6 7 8 9 10 20 30 40 50 60 70 80 90 100 120 140 160 180 200];
            for i=1:length(PeriodoRetorno)
                if length(tipdis)<10
                Yn(i)=mufinal- (psifinal./xifinal.* ( 1-(-log(    (exp(-1/PeriodoRetorno(i)))     )).^(-xifinal) ) );
                else
                Yn(i)=mufinal- psifinal.*log(-log(exp(-1/PeriodoRetorno(i))) ) ;
                end
            end
            
            % OBTENCION DEL INTERVALO DE CONFIANZA PARA UN CUANTIL:
            % Periodo de Retorno, a�os:
            n=length(bestsolution);

            for k=1:length(PeriodoRetorno)
                P(k)=exp(-1./PeriodoRetorno(k));
            ic=zeros(1,1);
            if length(tipdis)<10 % Funcion GEV

            for i=1:n
                for j=1:n
                    Zp1=ameva.derivadaprimera(i,bestsolution,P(k));
                    Zp2=ameva.derivadaprimera(j,bestsolution,P(k));
                    ic=ic+(Zp1*Zp2*J(i,j));
                end
            end
            ic=ic.^0.5;
            icsup95(k)=Yn(k)+1.96*ic;
            icinf95(k)=Yn(k)-1.96*ic;

            else  % funcion GUMBEL

            for i=1:nn
                for j=1:nn
                    Zp1=ameva.derivadaprimera(i,pargumbel,P(k));
                    Zp2=ameva.derivadaprimera(j,pargumbel,P(k));
                    ic=ic+(Zp1*Zp2*J(i,j));
                end
            end
            ic=ic.^0.5;
            icsup95(k)=Yn(k)+1.96*ic;
            icinf95(k)=Yn(k)-1.96*ic;

            end

            end
            
            
            
            ejex=[2 5 10 25 50 100 200];
            p=plot(ax,log(PeriodoRetorno),Yn,'-r','LineWidth',1.5);
            hold(ax,'on');
            plot(ax,log(RRR),Yord,'.b')
            bc=plot(ax,log(PeriodoRetorno),icsup95,'--k','LineWidth',1.2);
            plot(ax,log(PeriodoRetorno),icinf95,'--k','LineWidth',1.2);
            hold(ax,'off');

            %axis(ax,[log(RRR(1)) log(250) 0 max(Yn)+2])
            set(ax,'XTick',log(ejex));
            set(ax,'XTickLabel',{ejex})

            xlabel(ax,self.txt.report('xPot'));
            ylabel(ax, [self.txt.report([amevaVar.name,'Short']),' (',amevaVar.units,') '])
            grid(ax,'on');
            title(ax,sprintf(self.txt.report('tGev')));
            text1=strcat('\mu = ',num2str(sprintf('%1.3f',mufinal)),' \xi = ',num2str(sprintf('%1.3f',xifinal)),' \psi = ',num2str(sprintf('%1.3f',psifinal)));
            l=legend(ax,[p,bc],{text1,'95%'},'Location','SouthEast');
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
            
            fitParameters.mu=mufinal;
            fitParameters.xi=xifinal;
            fitParameters.psi=psifinal;
        end
        
        function [fitParameters,muIni,muFuture]=addExtremalTrend(self,relPosition,amevaVar,param,isSignificative,yearIni,yearFuture,parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax=axes('Parent',parent,'Position',relPosition);            

            time=amevaVar.getTime;
            var=amevaVar.getData;
            
            timeVec=datevec(time);            
            nYears=timeVec(end,1)-timeVec(1,1)+1;
            Serie=[time,timeVec(:,1:4),var];
            thresholdPerc=99.5;
            indtemp=3;
            threshold=prctile(var,thresholdPerc);

            [ymax,~,~,~,xmax]=ameva.SeleccionaMaximosIndepPOT(Serie,threshold,indtemp);
            [~,~,~,~,fitParameters,~]=ameva.potfit(ymax,threshold,nYears,xmax);
            
           
            
            %Meto la tendencia
            
            RT=[1 2 3 4 5 6 7 8 9 10 20 30 40 50 60 70 80 90 100 120 140 160 180 200 230 250 300 350 400 450 500];
            P=exp(-1./RT);            
            
            deltaMuIni=isSignificative(1)*param.mu1*(yearIni-timeVec(end,1));
            deltaMuFuture=isSignificative(1)*param.mu1*(yearFuture-timeVec(end,1));
            muIni=fitParameters.mu+deltaMuIni;            
            muFuture=fitParameters.mu+deltaMuFuture;
            
            yIni=muIni- (fitParameters.psi./fitParameters.xi.* (1-(-log(P)).^-fitParameters.xi));
            yFuture=muFuture-(fitParameters.psi./fitParameters.xi.* (1-(-log(P)).^-fitParameters.xi));            
            
            plot(ax,log(RT),yIni,'color','r','LineWidth',1.5);
            hold(ax,'on');
            plot(ax,log(RT),yFuture,'color','b','LineWidth',1.5);
            
            l=legend(num2str(yearIni),num2str(yearFuture));
            title(ax, self.txt.report('tExtremalTrend'));            
            axis(ax,[log(1) log(500) floor(min(min(yIni),min(yFuture)))-0.5 ceil(max(max(yIni),max(yFuture)))+0.5])% Modificado por Antonio
            ejex=[1 2 5 10 25 50 100 150 250 450];% Modificado por Antonio
            set(ax,'XTick',log(ejex));
            set(ax,'XTickLabel',{ejex})

            ylabel(ax, [self.txt.report([amevaVar.name,'Short']) ' (' amevaVar.units ')' ]);
            xlabel(ax, self.txt.report('xPot'));
            grid(ax,'on');
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
        end
        
        function fitParameters=addAnnualMaxChart(self,relPosition,amevaVar1,amevaVar2,parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax=axes('Parent',parent,'Position',relPosition);
            
            [var1Max,~,dateMax]=amevaVar1.getMaxYearData();
            index=ismember(amevaVar1.getTime,dateMax);
            var2Max=amevaVar2.getSubsetData(index);
            alfa=0.95;conf=0.99;
            
            [~,polycoef] = ameva.outlierfilterRNoCte (var1Max,var2Max,alfa,6,conf);
            fitParameters.a=polycoef(1);
            fitParameters.b=polycoef(2);
            fitParameters.aSigma=polycoef(3);
            fitParameters.bSigma=polycoef(4);
            
            x=linspace(0,max(var1Max)*3/2,100);
            xSigma=linspace(min(var1Max)/2,max(var1Max)*3/2,100);
            p=plot(ax,x,polycoef(1)*x.^polycoef(2),'r','Linewidth',1.5);
            hold(ax,'on');
            bc=plot(ax,xSigma,polycoef(1)*xSigma.^polycoef(2)+polycoef(3)*xSigma.^polycoef(4),'--k','Linewidth',1.2);
            plot(ax,xSigma,polycoef(1)*xSigma.^polycoef(2)-polycoef(3)*xSigma.^polycoef(4),'--k','Linewidth',1.2);
            plot(ax,var1Max,var2Max,'.b');
            hold(ax,'off');
            
            
            l=legend(ax,[p,bc],{'\mu_{MA}','\mu_{MA} \pm \sigma_{MA}'});
            set(l,'Location','SouthEast');
            title(ax, sprintf(self.txt.report('tAnnualMax')));
            xlabel(ax, [self.txt.report([amevaVar1.name,'Short']) ' (' amevaVar1.units ')' ]);
            ylabel(ax, [self.txt.report([amevaVar2.name,'Short']) ' (' amevaVar2.units ')' ]);                        
            
            grid(ax,'on');
            hlim=get(ax,'ylim');
            hlim=[0 hlim(2)];
            set(ax,'ylim',hlim,'XMinorTick','off','XMinorGrid','off');    
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);          
            
        end
        
        function ax=addMap(self,relPosition,batData,parent)
            
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax=axes('Parent',parent,'Position',relPosition);            
            
            levels=util.getDemLevels(batData.Z);
            contourf(ax,batData.X,batData.Y,-batData.Z,-levels,'LineStyle','none');
            util.demcmap(-batData.Z);
            hold(ax,'on');
            for i=1:length(batData.costas)
                plot(ax,batData.costas(i).x,batData.costas(i).y,'-k','LineWidth',1);
            end
            hold(ax,'off');
            xMin=min(min(batData.X));xMax=max(max(batData.X));
            yMin=min(min(batData.Y));yMax=max(max(batData.Y));
            axis(ax,'image');
            set(ax,'xlim',[xMin xMax],'ylim',[yMin,yMax]);           
            self.setFormat([ax,get(ax,'xlabel'),get(ax,'ylabel')]);
                        
        end
        
        function ax=addCoastMap(self,relPosition,batData,parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax=axes('Parent',parent,'Position',relPosition,'box','on');            
            hold(ax,'on');
            for i=1:length(batData.costas)
                plot(ax,batData.costas(i).x,batData.costas(i).y,'-','Color',[0.5 0.5 0.5]);
            end
            hold(ax,'off');
            xMin=min(min(batData.X));xMax=max(max(batData.X));
            yMin=min(min(batData.Y));yMax=max(max(batData.Y));
            axis(ax,'image');
            set(ax,'xlim',[xMin xMax],'ylim',[yMin,yMax]);           
            self.setFormat([ax,get(ax,'xlabel'),get(ax,'ylabel')]);
        end
        
        function addZoomBox(self,batDataZoom,color,varargin)            
            
            xMin=min(min(batDataZoom.X));xMax=max(max(batDataZoom.X));
            yMin=min(min(batDataZoom.Y));yMax=max(max(batDataZoom.Y));
            polX=[xMin,xMin,xMax,xMax];
            polY=[yMin,yMax,yMax,yMin];
            hold(self.mapAxes,'on');
            f=fill(polX,polY,'r','EdgeColor',color,'LineWidth',1.5,'FaceColor','none','Parent',self.mapAxes);
            for j=1:2:length(varargin)
                    set(f,varargin{j},varargin{j+1});
            end 
            hold(self.mapAxes,'off');
            
        end
        
        function [param,isNormal,isSignificative,significativa]=addHeterocedasticTrendPlot(self,relPosition,amevaVar,timeOptions,parent)
            if ~exist('timeOptions','var')
                timeOptions.scale='Year';
                timeOptions.type='Max';
            end
            if ~exist('parent','var')
                parent = self.mainPanel;
            end         
            
            [data,time]=eval(['amevaVar.get',timeOptions.type,timeOptions.scale,'Data()']);
%              time=datevec(time); %N
%              time=time(:,1);%N
            
            ax = axes('Parent', parent,'Position', relPosition);
            
            km=10;
            conf = 0.90;
            outlierconf = 0.95;
            
            [trend,param_,residuo,outliers]=ameva.getTrend(time,data,km,conf,outlierconf);
            significativa=(trend.upperParam.*trend.lowerParam)>0;
            isSignificative=[significativa(2),significativa(4)]; %Media y Desviaviación 
            
            %Análisis de significancia
            if significativa(2) && ~significativa(4) %mu lineal sigma cte
              [trend,param_,residuo,outliers]=ameva.getTrend(time,data,8,conf,outlierconf); 
              param_=[param_;0];
              if ~significativa(3)
                param_(3)=std(data);  
              end
            elseif ~significativa(2) && significativa(4) %mu cte sigma lineal
                [trend,param_,residuo,outliers]=ameva.getTrend(time,data,2,conf,outlierconf);
                param_=[param_(1);0;param_(2:3)];
                if ~significativa(1)
                    param_(1)=mean(data);  
                end
             elseif ~significativa(2) && ~significativa(4) %mu cte sigma cte
                [trend,param_,residuo,outliers]=ameva.getTrend(time,data,0,conf,outlierconf);
				param_=[param_(1);0;param_(2);0];
                if ~significativa(1)
                    param_(1)=mean(data);  
                end
                if ~significativa(3)
                    param_(3)=std(data);  
                end
            end
            
            [hr pvalue] = kstest((residuo-mean(residuo))/std(residuo),[],1-conf);
            isNormal=~logical(hr);            
            
            param.mu0=param_(1);
            param.mu1=param_(2);
            param.sigma0=param_(3);
            param.sigma1=param_(4);
            
            dataPlot=plot(ax,time,data,'.b','MarkerSize',10);
            hold(ax,'on');
            muPlot=plot(ax,time,trend.mu,'-k','Linewidth',1.5);
            bcPlot=plot(ax,time,trend.upper,'--','Linewidth',1,'Color',[30 144 255]/255);
            plot(ax,time,trend.lower,'--','Linewidth',1,'Color',[30 144 255]/255);
            outPlot=[];
            if ~isempty(outliers) && ~isempty(outliers{1})                
                    outPlot=plot(ax,time(outliers{1}),data(outliers{1}),'or');                
            end
            hold(ax,'off');
            
            % XTicks each 10 years            
%             timeVec=datevec(time);
%             year=timeVec(:,1)/5;
%             year=floor(year(1))*5:5:ceil(year(end))*5;
%             set(ax,'XTick',datenum([year',zeros(length(year),2)]));
%             %datetick(ax, 'x', 'yyyy');
%             set(ax,'XTickLabel',year);
            
            
            % Plot configuration
            xlim(ax, [time(1), time(end)]);            
            xlabel(ax,self.txt.report('xYear'));
            ylabel(ax, [self.txt.report([amevaVar.name,'Short']) ' (' amevaVar.units ')' ]);
            title(ax, sprintf(self.txt.report('tTrend'), self.txt.report(amevaVar.name)));
            l=legend(ax,[dataPlot,muPlot,bcPlot,outPlot],self.txt.report('data'),self.txt.report('mean')...
                ,sprintf(self.txt.report('confidenceInterval'),conf*100,'%'),'Outliers','Location','NorthEastOutside');
            
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
            
            
        end
        
        function addTrendPdf(self,relPosition,deltaMu,deltaSigma,name,units,parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end         
            ax = axes('Parent', parent,'Position', relPosition);
            
            xPdf=linspace(deltaMu-4.5*deltaSigma,deltaMu+4.5*deltaSigma,1000);
            yPdf = pdf('normal', xPdf, deltaMu, deltaSigma);
            plot(ax,xPdf,yPdf,'LineWidth',1.5);            
            grid(ax,'on');
            %title(ax,sprintf(self.txt.report('tTrendPdf'),nYears));
            %xlabel(ax, [self.txt.report([amevaVar.name,'Short']) ' (' amevaVar.units ')' ]);
            xlabel(ax,[name,' (',units,')']);
            ylabel(ax,self.txt.report('tPDF'));
            %l=legend(ax,self.txt.report('lFuturePdf'),[self.txt.report([amevaVar.name,'Short']),' ',self.txt.report('current')]);
            l=legend(ax,['N(\mu^F_{',name,'},(\sigma^F_{',name,'})^2)']);            
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
        end
        
        function addBackTrendPdf(self,relPosition,x,pdf,type,parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end         
            
            ax = axes('Parent', parent,'Position', relPosition);
            plot(ax,x,pdf,'LineWidth',1.5,'Color',[0,128/255,0]);            
            grid(ax,'on');
            xlabel(ax,self.txt.report('xBackward'));
            ylabel(ax,self.txt.report('tPDF'));
            title(ax,sprintf(self.txt.report(['tTrendPdf_',type])));
            self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);
            
        end
        
        function addTrendTestTable(self,relPosition,isNormal,isSignificative)
             if ~exist('parent','var')
                parent = self.mainPanel;
             end               
            header={' ',self.txt.report('yes'),self.txt.report('no')};
            rowNames={};
            if ~isempty(isSignificative), rowNames=cat(2,rowNames,self.txt.report('lSignificativeTrend'));end
            if ~isempty(isNormal), rowNames=cat(2,rowNames,self.txt.report('lNormalDistribution'));end            
            
            if isSignificative
                data={'x',''};
            else
                data={'','x'};
            end
            
            if ~isempty(isNormal)
                if isNormal
                    data=cat(1,data,{'x',''});
                else
                    data=cat(1,data,{'','x'});
                end
            end
            
            self.addTable(relPosition, data, header, rowNames, parent);
            
            
        end
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Methodological Graphs
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function addBin2DTable(self, relPosition, amevaVar1, amevaVar2)
            x = amevaVar1.getData();
            time = amevaVar1.getTime();
            y = amevaVar2.getIntervalData(time(1),time(end));
            
            % Edges (Options)
            auxRowInterv = [0.2, 0.25, 0.5, 1, 2];
            auxColInterv = [0.5, 1, 2, 3, 4];
            numRows = 8;
            numCols = 6;
            
            % Possible edges
            auxEdgRow = cell(length(auxRowInterv),1);
            dstRow = zeros(1,length(auxEdgRow));
            for auxIndex = 1: length(auxRowInterv)
                 auxEdgRow{auxIndex} =  floor(min(x)):auxRowInterv(auxIndex):ceil(max(x));
                 dstRow(auxIndex) = length(auxEdgRow{auxIndex});
            end
            
            auxEdgCol = cell(length(auxColInterv),1);
            dstCol = zeros(1,length(auxEdgCol));
            for auxIndex = 1: length(auxColInterv)
                 auxEdgCol{auxIndex} =  floor(min(y)):auxColInterv(auxIndex):ceil(max(y));
                 dstCol(auxIndex) = length(auxEdgCol{auxIndex});
            end
            
            % Optimal edges
            [dump optIndRow] = min(abs(dstRow - numRows));
            [dump optIndCol] = min(abs(dstCol - numCols));
             
            edgesRow =  auxEdgRow{optIndRow};
            edgesColumn = auxEdgCol{optIndCol};
            edges = {edgesRow, edgesColumn};
            
            % Table calculation
            [data,c] = hist3([x,y], 'Edges', edges);
            data = data(1:size(data,1)-1,1:size(data,2)-1);
            tableData = (data / sum(sum(data))) .* 100;
            tableData = tableData';
            
            header = cell(1, size(tableData,2)+1);
            header{1} = [self.txt.report([amevaVar2.name,'Short']),' (',amevaVar2.units,')',...
                ' \\ ', self.txt.report([amevaVar1.name,'Short']),' (',amevaVar1.units,')'];            
            for iHeader = 2 : length(header)
                header{iHeader} = [num2str(edgesRow(iHeader-1)) ' - ' num2str(edgesRow(iHeader))];
            end
            
            colName = cell(1, size(tableData,1));
            for iCol = 1 : length(colName)
                colName{iCol} = [num2str(edgesColumn(iCol)) ' - ' num2str(edgesColumn(iCol+1))];
            end
            
            %A�ado la fila de total
            tableData=cat(2,tableData,sum(tableData,2));
            tableData=cat(1,tableData,sum(tableData,1));
            tableData(end,end)=NaN;
            header=cat(2,header,'Total (%)');
            colName=cat(2,colName,'Total (%)');
            
            % Table plot
            self.addTable(relPosition, tableData, header, colName);
            
            
            
            
            
        end
        
        function addDirRoseSeas(self, amevaVar, amevaVarDir, rosesSize)
            % Roses position and size
            x1 = (0.5 - rosesSize)/2;
            x2 = 0.5 + (0.5 - rosesSize)/2;
            y1 = (0.5 - rosesSize)/2;
            y2 = 0.5 + (0.5 - rosesSize)/2;
            
            % Raw Data
            time = amevaVar.getTime;
            data = amevaVar.getData;
            dir = amevaVarDir.getData;
            
            timeVec=datevec(time);
            month=timeVec(:,2);
            
            % Season roses
            posRoses = {[x1, y2, rosesSize,rosesSize],[x2, y2, rosesSize,rosesSize],[x1, y1, rosesSize,rosesSize],[x2, y1, rosesSize,rosesSize]};
            seasonMonths = {[12,1,2],[3,4,5],[6,7,8],[9,10,11]};
            seasonMonthsName = {'(DEF)','(MAM)','(JJA)','(SON)'};
            for indexSeason = 1:4
                posSeason = ismember(month, seasonMonths{indexSeason});
                
                % Season Ameva Variables
                amevaDirS = ameva.AmevaVar('Dir','�',dir(posSeason),time(posSeason));
                amevaVarS = ameva.AmevaVar(amevaVar.name, amevaVar.units, data(posSeason), time(posSeason));
                
                % Add Rose
                self.addDirRose(posRoses{indexSeason}, amevaVarS, amevaDirS, self.mainPanel, seasonMonthsName{indexSeason});
            end
        end
        
        function addSOMChar(self, amevaVarHs, amevaVarTp, amevaVarDir, nSom)
            util.freezeColors(self.mapAxes);
            % SOM-Plot color and position parameters
            smallHexSize = 0.23;
            bigHexSize = 0.5;
            yDownPanels = 0.02;
            yMedPanels = 0.43;
            yHighPanels = 0.73;
            xP1 = 0; xP2 = 0.25; xP3 = 0.50; xP4 = 0.75;
            edgeColor = [0 0 0];
            darkTextColor = [0 0 0];
            whiteTextcolor = [192 192 192]/255;
            
            cbHeigh = 0.03;
            cbYDown = 0.3;
            yBigHexa = yHighPanels + smallHexSize - bigHexSize;
            
            % TimeArray
            time=amevaVarHs.getTime;
            %time=time(1:10500);
            TimeArray = datevec(time);
            
            % SOM calculation
            [final, bmus, prob100] = som.rep_solveSOM3D(amevaVarHs, amevaVarTp, amevaVarDir, nSom);
            seasons = [12,1,2; 3,4,5; 6,7,8; 9,10,11];
            probMonth = som.rep_probSeasons(TimeArray, bmus, nSom*nSom, seasons);
            
            % Fix Prob
            prob100(prob100<0.01)=0.01;
            probMonth(probMonth<0.01)=0.01;
            
            % SOM variables
            somHs = final(:,1);
            somTp = final(:,2);
            somDir = final(:,3);
            somProbLog = log(prob100);
            SomProbMonthLog = log(probMonth);
            
            minac = min(min(somProbLog),min(min(SomProbMonthLog)));
            maxac = max(max(somProbLog),max(max(SomProbMonthLog)));
            
            % SOM variables color data
            HsColorData = round(som.normaliza(somHs, [1 256]));
            TpColorData = round(som.normaliza(somTp, [1 64]));
            DirColorData = round(som.normaliza(somDir, [1 64]));
            
            YearColorData = round(som.normaliza2(somProbLog, maxac, minac, [1 65]));
            DEFColorData = round(som.normaliza2(SomProbMonthLog(:,1), maxac, minac, [1 65]));
            MAMColorData = round(som.normaliza2(SomProbMonthLog(:,2), maxac, minac, [1 65]));
            JJAColorData = round(som.normaliza2(SomProbMonthLog(:,3), maxac, minac, [1 65]));
            SONColorData = round(som.normaliza2(SomProbMonthLog(:,4), maxac, minac, [1 65]));
            
            % SOM variables colormap
            cc = 1-copper(65);
            c1 = cc(2:65,:);
            cm = 1 - copper(75);
            
            cHs = flipud(hot(320)); cHs = cHs(1 : 256, :);
            cTp=[]; cTp(:,1)=c1(:,1); cTp(:,2)=c1(:,3); cTp(:,3)=c1(:,2);
            cDir = hsv(64);
            cProb = cm(1:65,:);
            
            % Hexa color input
            colorHs = cHs(HsColorData, :);
            colorTp = cTp(TpColorData, :);
            colorDir = cDir(DirColorData, :);
            colorYearProb = cProb(YearColorData, :);
            colorDEFProb = cProb(DEFColorData, :);
            colorMAMProb = cProb(MAMColorData, :);
            colorJJAProb = cProb(JJAColorData, :);
            colorSONProb = cProb(SONColorData, :);
            
            % SOM (HS,TP,DIR)
            titleHs = [self.txt.report([amevaVarHs.name,'Short']) ' (' amevaVarHs.units ')' ];
            titleTp = [self.txt.report([amevaVarTp.name,'Short']) ' (' amevaVarTp.units ')' ];
            titleDir = [self.txt.report([amevaVarDir.name,'Short']) ' (' amevaVarDir.units ')' ];
            titleSOM = ['SOM_{',num2str(nSom),'x',num2str(nSom),'} (', self.txt.report([amevaVarHs.name,'Short']),...
                ', ', self.txt.report([amevaVarTp.name,'Short']) ', ',...
                self.txt.report([amevaVarDir.name,'Short']) ')'];
            
            % SOM (HS,TP,DIR)
            somAxes = self.drawSomPatch(nSom, nSom, 1, colorTp, edgeColor, titleSOM, [], [xP1, yBigHexa, bigHexSize, bigHexSize], self.mainPanel);
            self.drawSomPatch(nSom, nSom, 0.5, colorHs, 'none', [], somAxes, [xP1, yBigHexa, bigHexSize, bigHexSize], self.mainPanel); hold(somAxes,'on');
            self.drawSomVector(nSom, nSom, somDir, somAxes);
            
            % Hs
            [hsAxes, unitC] = self.drawSomPatch(nSom, nSom, 1, colorHs, edgeColor, titleHs, [], [xP1, yDownPanels, smallHexSize, smallHexSize], self.mainPanel);
            self.drawSomText(somHs, hsAxes, unitC, darkTextColor);
            self.setFormat(findall(hsAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(hsAxes,'title'),'FontSize',self.fontSizeSmall);
            
            % Tp
            tpAxes = self.drawSomPatch(nSom, nSom, 1, colorTp, edgeColor, titleTp, [], [xP2, yDownPanels, smallHexSize, smallHexSize], self.mainPanel);
            self.drawSomText(somTp, tpAxes, unitC, darkTextColor);
            self.setFormat(findall(tpAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(tpAxes,'title'),'FontSize',self.fontSizeSmall);
            
            % Dir
            dirAxes = self.drawSomPatch(nSom, nSom, 1, colorDir, edgeColor, titleDir, [], [xP3, yDownPanels, smallHexSize, smallHexSize], self.mainPanel);
            self.drawSomText(somDir, dirAxes, unitC, darkTextColor);
            self.setFormat(findall(dirAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(dirAxes,'title'),'FontSize',self.fontSizeSmall);
            
            % Year Prob
            yProbAxes = self.drawSomPatch(nSom, nSom, 1, colorYearProb, edgeColor, self.txt.report('tSomYFreq'), [], [xP4, yDownPanels, smallHexSize, smallHexSize], self.mainPanel);
            self.drawSomText(prob100, yProbAxes, unitC, whiteTextcolor);
            self.setFormat(findall(yProbAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(yProbAxes,'title'),'FontSize',self.fontSizeSmall);
            
            % Seasons Prob
            DEFProbAxes = self.drawSomPatch(nSom, nSom, 1, colorDEFProb, edgeColor, self.txt.report('tSomWinterFreq'), [], [xP3, yHighPanels, smallHexSize, smallHexSize], self.mainPanel);
            MAMProbAxes = self.drawSomPatch(nSom, nSom, 1, colorMAMProb, edgeColor, self.txt.report('tSomSpringFreq'), [], [xP4, yHighPanels, smallHexSize, smallHexSize], self.mainPanel);
            JJAProbAxes = self.drawSomPatch(nSom, nSom, 1, colorJJAProb, edgeColor, self.txt.report('tSomSummerFreq'), [], [xP3, yMedPanels, smallHexSize, smallHexSize], self.mainPanel);
            SONProbAxes = self.drawSomPatch(nSom, nSom, 1, colorSONProb, edgeColor, self.txt.report('tSomAutumnFreq'), [], [xP4, yMedPanels, smallHexSize, smallHexSize], self.mainPanel);
            
            self.drawSomText(probMonth(:,1), DEFProbAxes, unitC, whiteTextcolor);
            self.drawSomText(probMonth(:,2), MAMProbAxes, unitC, whiteTextcolor);
            self.drawSomText(probMonth(:,3), JJAProbAxes, unitC, whiteTextcolor);
            self.drawSomText(probMonth(:,4), SONProbAxes, unitC, whiteTextcolor);
            
            self.setFormat(findall(DEFProbAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(DEFProbAxes,'title'),'FontSize',self.fontSizeSmall);
            self.setFormat(findall(MAMProbAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(MAMProbAxes,'title'),'FontSize',self.fontSizeSmall);
            self.setFormat(findall(JJAProbAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(JJAProbAxes,'title'),'FontSize',self.fontSizeSmall);
            self.setFormat(findall(SONProbAxes,'type','text'),'FontSize',self.fontSomText);
            self.setFormat(get(SONProbAxes,'title'),'FontSize',self.fontSizeSmall);
            
            % Colorbars
            %util.freezeColors(self.mapAxes);
            
            % Tp cb
            cbTpPos = [0.02, cbYDown, bigHexSize*0.9, cbHeigh];
            options.logScale=false; options.title=[self.txt.report([amevaVarTp.name,'Short']) ' (' amevaVarTp.units ')' ]; options.orientation = 'horizontal';
            self.drawColorBar(cbTpPos, cTp, [min(somTp), max(somTp)], options);
            
            % Hs cb
            cbHsPos = [0.02, cbYDown+0.08, bigHexSize*0.9, cbHeigh];
            options.logScale=false; options.title=[self.txt.report([amevaVarHs.name,'Short']) ' (' amevaVarHs.units ')' ]; options.orientation = 'horizontal';
            self.drawColorBar(cbHsPos, cHs, [min(somHs), max(somHs)], options);
            
            % Dir cb
            cbDirPos = [0.01+xP3, cbYDown, smallHexSize*0.9, cbHeigh];
            options.logScale=false; options.title=[self.txt.report([amevaVarDir.name,'Short']) ' (' amevaVarDir.units ')' ]; options.orientation = 'horizontal';
            self.drawColorBar(cbDirPos, cDir, [min(somDir), max(somDir)], options);
            
            % Prob cb
            cbProbPos = [0.03+xP3+smallHexSize, cbYDown, smallHexSize*0.9, cbHeigh];
            options.logScale=true; options.title=self.txt.report('tSomCBFreq'); options.orientation = 'horizontal';options.fitTicks=true;
            self.drawColorBar(cbProbPos, cProb, [minac, maxac], options);
            
            
        end
        
        function [param, percSectors] = addRmGevDir(self, amevaVar, amevaVarDir)
            param=[];
            y=amevaVar.getData;
            [~,sectorsIndex]=amevaVarDir.getDirSectors(16);
            percSectors=sum(sectorsIndex,1)*100/length(y); % Percentage related to each sector
            
            legendStr={'N','NNE','NE','ENE','E','ESE','SE','SSE','S','SSW','SW','WSW','W','WNW','NW','NNW'};
            colors=[1 0 0;0 0 0;0 0.5 0;0 0 1];
            
            for i=1:4
                sp = subplot(2, 2, i, 'Parent', self.mainPanel);                
                plots=[];
                plotsExist=false(4,1);
                for j=1:4
                    
                    index=(i-1)*4+j;
                    color=colors(j,:);
                    
                    ySector=y(sectorsIndex(:,index));
                    if  length(ySector)<30
                        param(index).mu=[];
                        param(index).psi=[];
                        param(index).xi=[];
                        
                    else
                        [p,x,xFit,yFit]=ameva.gevRM(ySector);
                        param(index).mu=p.mu;
                        param(index).psi=p.psi;
                        param(index).xi=p.xi;
                        
                        plots=cat(1,plots,plot(sp,xFit,yFit,'--','Color',color,'LineWidth',1));
                        
                        plotsExist(j)=true;
                        hold(sp,'on');
                        plot(sp,x,ySector,'.','Color',color);
                    end
                end
                hold(sp,'off');
                indexSubLegend=((i-1)*4+1):i*4;
                indexSubLegend=indexSubLegend(plotsExist);
                if ~isempty(plots)
                    legend(sp,plots,legendStr(indexSubLegend),'Location','NorthWest');
                end
                title(sp,sprintf(self.txt.report('tRmGev')));
                ylabel([self.txt.report([amevaVar.name,'Short']),' (',amevaVar.units,') ']);
                xlabel([self.txt.report('xRmGev'),', F(',self.txt.report([amevaVar.name,'Short']),')']);
                
                probFigure=[0.001 0.2 0.5 0.8 0.90 0.95 0.98 0.99 0.999];
                xFigure=-log(log(1./probFigure));
                xlim(sp,[min(xFigure) max(xFigure)]);
                
                set(sp,'XTick',xFigure,'XTickLabel',probFigure,'XGrid','on','YGrid','on');                  
                self.setFormat([sp,get(sp,'title'),get(sp,'xlabel'),get(sp,'ylabel'),sp]);
            end
        end
    end
    
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Information Items
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function writeText(self, relPosition, strText, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax = axes('Units','normalized', 'Position', relPosition, 'Parent', parent, 'visible', 'off');
            
            % Text plot
            text('string',strText,'FontSize',self.fontSizeEquation,'HorizontalAlignment','Center',...
                'VerticalAlignment','Middle','Parent',ax,'interpreter','latex', 'Position',[0.5, 0.5]);
        end   
        
        function writeString(self,relPosition, str, parent,varargin)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            ax = axes('Units','normalized', 'Position', relPosition, 'Parent', parent, 'visible', 'off');
            
            % Text plot
            t=text('string',str,'Parent',ax, 'Position',[0, 0]);
            self.setFormat(t);    
            for i=1:2:length(varargin)
                set(t,varargin(i),varargin(i+1));
            end
        end
          
        function addAnnotation(self, absPosition, strText)
            % Annotation plot            
            annotation('textbox', absPosition, 'String', strText ,'LineWidth',self.edgeWidth, 'FontSize',self.fontSizeMedium,'FontName',self.fontName);            
        end
        
        function addTable(self, relPosition, data, header, rowNames, parent,varargin)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            % Cell array
            cellData = num2cell(data);
            
            % Table border parameter
            adjustBorder = 0.01;
            
            % Header and column names
            if ~isempty(rowNames)
                cellData = [rowNames', cellData];
            end
            if ~isempty(header)
                cellData = [header; cellData];
            end
            
            % Table axes
            ax = axes('Units','normal', 'Position', relPosition, 'Parent', parent, 'Visible','off');
            
            % Table Build
            nRow = size(cellData,1)+1; nCol = size(cellData,2)+1;
            yRow = linspace(adjustBorder, 1-adjustBorder, nRow); cellH=yRow(2)-yRow(1);
            xCol = linspace(adjustBorder, 1-adjustBorder, nCol); cellW=xCol(2)-xCol(1);
            
            % Table Plot
            for i = 1:length(yRow)
                line('Xdata',[adjustBorder, 1-adjustBorder],'Ydata',[yRow(i), yRow(i)],'Parent',ax);
            end
            for i = 1:length(xCol)
                line('Xdata',[xCol(i), xCol(i)],'Ydata',[adjustBorder, 1-adjustBorder],'Parent',ax);
            end
            set(ax,'xlim',[0,1],'ylim',[0,1]);
            
            % Text (fill table)
            for yIndex = 1:size(cellData,1)
                for xIndex = 1:size(cellData,2)
                    fudData = flipud(cellData);
                    cellContent = fudData{yIndex,xIndex};
                    
                    if iscell(cellContent)
                        cellContent=cellContent{1};
                    end
                    if isnumeric(cellContent) 
                        if isnan(cellContent)
                            cellContent = '-';
                        else
                            cellContent = num2str(cellContent,'%.2f');
                        end
                    end
                    
                    t = text(xCol(xIndex)+cellW/2,yRow(yIndex)+cellH/2, cellContent,'HorizontalAlignment','center');
                    if (yIndex == size(cellData,1) && ~isempty(header)) || (xIndex == 1 && ~isempty(rowNames))
                        set(t,'FontWeight','bold');
                        if strcmp(cellContent(1),'\')                            
                            %set(t,'String',['$',cellContent,'$'],'Interpreter','latex');
                            %set(t,'Interpreter','latex');
                        end
                    end
                end
            end
            
            self.setFormat(findall(ax,'type','text'),varargin{:});
        end
        
        function putLogos(self)
            self.putImage(self.getElementPosition('logo'),'logo_report.png',self.fig,self.cfg('region'));
            self.putImage(self.getElementPosition('info_partners'),'partners_report.png',self.fig,self.cfg('region'));
        end
        
        function putImage(self,position,imageFile,parent,region)
            if ~exist('parent','var')
                p = self.createPanel(position, false);
            else
                p = self.createPanel(position, false,parent);
            end
            
            if ~exist('region','var')
                region='';
            end
            
            ax=axes('Parent',p,'YTick',[],'XTick',[],'position',[0 0 1 1]);
            imshow(fullfile('icon',region,imageFile),'Parent',ax);
            axis(ax,'image');
        end
        
    end
    
    methods (Static=true)
        function equation=latexEquation(eqId)
            equation='';
            switch eqId
                case 'gev'
                    equation='F(x;\mu,\psi,\xi)\;\;\;& = & \exp\; \left( -\left( 1+ \xi \left( \frac{x-\mu}{\psi} \; \right) \right )\;^{-1/ \xi}\; \right)';
                    %equation='F(x;\mu,\psi,\xi)\;& = & \exp\;(-(1+\xi(\frac{x-\mu}{\psi}\;))\;^{-1/\xi} \;)';
                case 'gevInv'
                    equation='x(F;\mu,\psi,\xi)=\mu-\frac{\psi}{\xi} \left( 1-(\ln (F))\;^{-\xi}\; \right)';
                case 'gevInvRt'
                    equation='x(T_r;\mu,\psi,\xi)=\mu-\frac{\psi}{\xi}\left( 1-\left(\frac{1}{T_r}\right )^{-\xi}\right) \,\,\,\, x=\{H_s,T_p\}';
                case 'gevInvRtSimple'
                    equation='x(T_r;\mu,\psi,\xi)=\mu-\frac{\psi}{\xi}\left( 1-\left(\frac{1}{T_r}\right )\;^{-\xi}\;\right)';
                case 'pareto'
                    equation='F(x;u,\lambda,\sigma,\xi)\;\;\;\;& = &\exp \;\left( \;-\lambda\left( 1+ \xi \left( \frac{x-u}{\sigma}\;\right) \right )\;^{-1/\xi}\;\; \right)';
                case 'paretoInvRt'
                    equation='x(T_r;u,\lambda,\sigma)=u-\frac{\sigma}{\xi}\left( 1-\left(\lambda T_r\right )^{\xi}\right) \,\,\,\, x=\{H_s,T_p\}';
                case 'bcPoly'
                    equation='BC(\hat{x};c_0,c_1,c_2,c_3)=\hat{x}\pm\left(c_0+c_1\hat{x}+c_2\hat{x}^2+c_3\hat{x}^3 \right) \,\,\,\, \hat{x}=\{\hat{H_s},\hat{T_p}\}';                    
                case 'Cerc'
                    equation='Q\;  = \frac{k\rho_w g^{1/2}}{16(\rho_s -\rho_w)(1-p)\sqrt{\gamma_b}\;\;\;\;\;\;\;\;}\;\;\;\;\;\;H_{b}^{5/2}\;\;\sin{\;\;2\theta_b}\;\;\,\,\, [m^3/s] \;\;\;\;\,\,\,\,\, CERC\,\,\, (1984)';
                case 'Kamphuis'
                    equation='Q\;  =\frac{k}{(\rho_s-\rho_w)(1-p)\;\;\;\;\;}\;\;\;\;(H_b)^a\; (T_p)^b\; (m_b)^c\; (D_{50})^d \; (\sin{\;\;2\theta_b\;})^e\;\,\, [m^3/s] \;\;\;\,\,\, Kamphuis \,\,\, (1991)';
                case 'Bayram' 
                    equation='Q\;  =\frac{k}{(\rho_s-\rho_w)(1-a)gw_s\;\;\;\;\;\;\;}\;\;\;\;F \cdot   \vec{V}\;\,\, [m^3/s] \;\;\;\;\,\,\,\,\, Bayram\,\,et\,\,al.,\,\,\, (2007)';
                case 'birkmeier'
                    equation='h_*=1.75H_{s12}-57.9\left ( \frac{H_{s12}^2}{gT_p^2} \right )';
                case 'hallermeier'
                    equation='h_*=2.28H_{s12}-68.5\left ( \frac{H_{s12}^2}{gT_p^2} \right )';
                
            end
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Tools
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function setFormat(self,controls,varargin)
            for i=1:length(controls)
                set(controls(i),'FontSize', self.fontSizeMedium, 'FontName', self.fontName);
                for j=1:2:length(varargin)
                    set(controls(i),varargin{j},varargin{j+1});
                end            
            end
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Private Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Access=private)
        function position = getElementPosition(self, element)
            switch element
                case 'back'
                    position=[0.001,(1-self.height)/2, self.width-0.002, self.height];
                case 'main'
                    position=[0,(1-self.height)/2,self.widthMain, self.height-self.heightTop];
                case 'title'
                    position =[0, (1+self.height)/2-self.heightTop, self.widthMain, self.heightTop];
                case 'logo'
                    position=[self.widthMain+self.widthSecond*0.2,(1+self.height)/2-self.heightTop, self.widthSecond*0.7, self.heightTop];
                case 'second'
                    position=[self.widthMain,(1+self.height)/2-self.heightTop-self.heightSecond, self.widthSecond, self.heightSecond];
                case 'map'
                    position=[self.widthMain,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap,self.widthSecond,self.heightMap];
                case 'info_coords'
                    position=[self.widthMain+self.widthInfoLabel,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-self.heightInfo ,self.widthSecond-self.widthInfoLabel,self.heightInfo];
                case 'info_period'
                    position=[self.widthMain+self.widthInfoLabel,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-2*self.heightInfo ,self.widthSecond-self.widthInfoLabel,self.heightInfo];
                case 'info_project'
                    position=[self.widthMain+self.widthInfoLabel,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-3*self.heightInfo ,self.widthSecond-self.widthInfoLabel,self.heightInfo];
                case 'info_alternativa'
                    position=[self.widthMain+self.widthInfoLabel,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-4*self.heightInfo ,self.widthSecond-self.widthInfoLabel,self.heightInfo];
                case 'info_coords_label'
                    position=[self.widthMain,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-self.heightInfo ,self.widthInfoLabel,self.heightInfo];
                case 'info_period_label'
                    position=[self.widthMain,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-2*self.heightInfo ,self.widthInfoLabel,self.heightInfo];
                case 'info_project_label'
                    position=[self.widthMain,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-3*self.heightInfo ,self.widthInfoLabel,self.heightInfo];
                case 'info_alternativa_label'
                    position=[self.widthMain,(1+self.height)/2-self.heightTop-self.heightSecond-self.heightMap-4*self.heightInfo ,self.widthInfoLabel,self.heightInfo];
                case 'info_partners'
                    position=[self.widthMain,(1-self.height)/2, self.widthSecond, self.heightLogo];
            end
        end
        
        function p = createPanel(self, position, drawBorder, parent)
            if ~exist('parent','var')
                parent = self.fig;
            end
            if drawBorder
                p = uipanel(parent,'Position',position,'BackGroundColor','w','ShadowColor','k','HighLightColor','k');
            else
                p = uipanel(parent,'Position',position,'BackGroundColor','w','BorderWidth',0);
            end
        end       
        
        function drawEdges(self)
            annotation('rectangle',self.getElementPosition('back'),'LineWidth',self.edgeWidth);
            annotation('rectangle',self.getElementPosition('info_partners'),'LineWidth',self.edgeWidth);
            annotation('rectangle',self.getElementPosition('map'),'LineWidth',self.edgeWidth);
            %annotation('rectangle',self.getElementPosition('map'),'LineWidth',self.edgeWidth);
        end        
        
        function drawColorBar(self, relPosition, color, clim, options, parent)
           % TODO: FUENTE DEL TITULO.            
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            if ~exist('options','var')
                options.title='';
                options.logScale=false;
                options.orientation='vertical';
            end
            if ~isfield(options,'title'),options.title='';end
            if ~isfield(options,'logScale'),options.logScale=false;end
            if ~isfield(options,'orientation'),options.orientation='vertical';end
            if ~isfield(options,'fitTicks'),options.fitTicks=false;end
            
            ax = axes('Parent', parent);
            
            colormap(ax,color);
            
            if strcmp(options.orientation,'horizontal')
                c=colorbar('peer',ax,'Location','North');
                dir='X';
            else
                c=colorbar('peer',ax);
                dir='Y';
            end
            
            title(c,options.title);
            caxis(ax,clim);
            
            %Ticks
            if options.fitTicks
                nTicks=length(get(c,[dir,'Tick']));
                set(c,[dir,'Tick'],linspace(clim(1),clim(2),nTicks+1));
            end
            if options.logScale
                tickLabel=num2str(exp(get(c,[dir,'Tick'])),'%.2f;');
                tickLabel=regexp(tickLabel(1:end-1),';','split');
                set(c,[dir,'TickLabel'], tickLabel);
            end
            
            set(c,'Position',relPosition);
            set(ax,'Visible','off');            
            self.setFormat([c,get(c,'title'),get(c,'XLabel'),get(c,'YLabel')]);
            util.cbfreeze(c);
           
        end
        
        function [ax, unit_coords] = drawSomPatch(self, m, n, relSize, color, edgeColor, titleStr, ax, relPosition, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            if isempty(ax)
                ax=axes('Parent',parent,'Position',relPosition);
            end
            
            msize=[m n];
            patchform=[0 0.6667;0.5 0.3333;0.5 -0.3333;0 -0.6667;-0.5 -0.3333;-0.5 0.3333];
            l=size(patchform,1);     % number of vertices
            munits=prod(msize);
            
            unit_coords(:,1)=reshape(repmat([1:msize(2)],msize(1),1),1,munits)';
            unit_coords(:,2)=repmat([1:msize(1)]',msize(2),1);
            d=rem(unit_coords(:,2),2) == 0;
            unit_coords(d,1)=unit_coords(d,1)+.5;
            
            color=reshape(color,[1 munits 3]);
            
            x=repmat(unit_coords(:,1)',l,1);
            y=repmat(unit_coords(:,2)',l,1);
            nx=repmat(patchform(:,1),1,munits);
            ny=repmat(patchform(:,2),1,munits);
            x=(x./relSize+nx).*relSize; y=(y./relSize+ny).*relSize;    
            
            xdim=msize(1);ydim=msize(2);
            set(ax,'Visible','off');
            set(ax,'XaxisLocation','Top');            % axis orientation
            set(ax,'xdir','normal');                  % = axis ij = matrix mode
            set(ax,'ydir','reverse');
            
            lelim=-.51; rilim=1.01; uplim=-.67; lolim=.67; % axis limits
            set(ax,'DataAspectRatio',[0.9015 1 1]);
            
            set(ax,'XLim',[1+lelim ydim+rilim],'YLim',[1+uplim xdim+lolim], ...
                'XLimMode','manual','YLimMode','manual'); % tighten the axis
            
            h_=patch(x,y,color,'Parent',ax);
            set(h_,'EdgeColor',edgeColor);
            
            % Title configuration
            if ~isempty(titleStr)
                t_ = title(ax,titleStr,'Visible','on', 'FontWeight','b');     
                set(t_,'units','normalized');
                oldPos = get(t_,'Position');
                newPos = [oldPos(1), 1.02 ,oldPos(3)];
                set(t_,'Position',newPos)
            end
             self.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);
            
        end
        
        function [ax, unit_coords] = drawSomVector(self, m, n, dir, ax, relPosition, parent)
            if ~exist('parent','var')
                parent = self.mainPanel;
            end
            if isempty(ax)
                ax=axes('Parent',parent,'Position',relPosition);
            end
            msize=[m n];
            munits=prod(msize);
            
            unit_coords(:,1)=reshape(repmat([1:msize(2)],msize(1),1),1,munits)';
            unit_coords(:,2)=repmat([1:msize(1)]',msize(2),1);
            d=rem(unit_coords(:,2),2) == 0;
            unit_coords(d,1)=unit_coords(d,1)+.5;
            
            M=0.45;
            Dir=3*pi/2-dir*pi/180;
            
            U=-M.*cos(Dir);
            V=M.*sin(Dir);
            
            util.udquiver(ax,unit_coords(:,1),unit_coords(:,2),U,V,0,'k');
        end
        
        function drawSomText(self, somData, ax, unit_coords, textColor)
            for j = 1 : length(somData)
                text(unit_coords(j,1),unit_coords(j,2), num2str(somData(j),'%.1f'),'Parent',ax,'Color',textColor,'HorizontalAlignment','center','FontName','Arial Narrow');
            end
        end
    end
end


classdef ReportCreator    
    
    properties
        project;
        alternativa;
        mopla;
        batData;
        batDataMeshesZoom=[];
        batDataRoiZoom=[];
        caseInfo;
        offshoreData;
        txt;
        levelCoords;       
        maxProfiles=20;
        maxPois=20;
    end
    
    methods
        function self=ReportCreator(project,moplaId,altName)
            
            self.txt.report = util.loadText('report');   
            %%% Cargo el idioma
            %self.txt=util.loadLanguage;
            %%%%%%%%%%%%%%%%%%%
            
            self.project=project;
            self.alternativa=smc.loadAlternativa(project.folder,altName);
            self.mopla=smc.loadMopla(fullfile(project.folder,altName),moplaId);
            
            
            self.batDataMeshesZoom=self.getMeshesZoom;
            self.batDataRoiZoom=self.getRoiZoom;
            
            x_=self.alternativa.xyz(:,1);
            y_=self.alternativa.xyz(:,2);            
            x=linspace(min(x_),max(x_),100);
            y=linspace(min(y_),max(y_),100);

            [X,Y]=meshgrid(x,y);            
            Z=self.alternativa.F(X,Y);

            self.batData.X=X;
            self.batData.Y=Y;
            self.batData.Z=Z;
            self.batData.costas=self.alternativa.costas;
            
            
            self.caseInfo.lat=self.mopla.lat;
            timeVec=datevec(self.mopla.time);
            self.caseInfo.lon=self.mopla.lon;
            self.caseInfo.depth=self.mopla.z;
            self.caseInfo.yearIni=timeVec(1,1);
            self.caseInfo.yearEnd=timeVec(end,1);
            %self.caseInfo.projectDes=self.project.description;
            self.caseInfo.projectDes=self.project.nombre;
            self.caseInfo.alternativeName=altName;
            
            db=util.loadDb();
            self.offshoreData=ameva.loadAmevaDataFromLatLon(db,self.mopla.lat,self.mopla.lon,[]);            
            %%%%Add Mean Sea Level
            eta=self.offshoreData('surge');
            tide=self.offshoreData('tide');
            level=ameva.AmevaVar('level','m',eta.getData+tide.getData,eta.getTime);
            self.offshoreData('level')=level;
            %%%%Get Coords
            self.levelCoords=containers.Map;  
            [gos.lat,gos.lon] = data.getGOSPoint(db,self.mopla.lat,self.mopla.lon);
            self.levelCoords('surge')=gos;
            
            [got.lat,got.lon] = data.getGOTPoint(db,self.mopla.lat,self.mopla.lon);
            self.levelCoords('tide')=got;
        end
        
        function zoneReport(self)
            r=report.Report(self.caseInfo,self.batData,self.txt.report('rtStudyZone'));
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,self.mopla.x,self.mopla.y,'.r','MarkerSize',12);            
            
            %%%% Put content in mainPanel
            ax=r.addMap([0.1 0.1 0.8 0.8],self.batData);
            cb=colorbar('peer',ax);            
            r.setFormat(cb);
            hold(ax,'on')
            plot(ax,self.project.nodosDow.x,self.project.nodosDow.y,'.k');            
            plot(ax,self.mopla.x,self.mopla.y,'or','LineWidth',2);  
            
            %%%% Print and Delete
            outFile=fullfile(self.outputFolder,'studyZone.png');
            r.printPng(outFile);
            delete(r);
        end
        
        function levelReport(self,levelVarId)
            strTitle=self.txt.report(['rtLevel_',levelVarId,'Report']);
            r=report.Report(self.caseInfo,self.batData,strTitle);
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,self.mopla.x,self.mopla.y,'.r','MarkerSize',12);            
            
            %%%% Put content in infoPanel
            if strcmp(levelVarId,'level')                
                  r.writeString([0.05 0.6 0.8 0.4],{sprintf(self.txt.report('lCoord'),self.txt.report('surge')),...
                      '',sprintf(['   - ',self.txt.report('lat'),': %.2f �'],self.levelCoords('surge').lat),...
                      '',sprintf(['   - ',self.txt.report('lon'),': %.2f �'],self.levelCoords('surge').lon),...
                      '','','',sprintf(self.txt.report('lCoord'),self.txt.report('tide')),...
                      '',sprintf(['   - ',self.txt.report('lat'),': %.2f �'],self.levelCoords('tide').lat),...
                      '',sprintf(['   - ',self.txt.report('lon'),': %.2f �'],self.levelCoords('tide').lon)
                      }, r.secondPanel);                    
            else
                r.writeString([0.05 0.7 0.8 0.2],{sprintf(self.txt.report('lCoord'),self.txt.report(levelVarId)),...
                      '',sprintf(['   - ',self.txt.report('lat'),': %.2f �'],self.levelCoords(levelVarId).lat),...
                      '',sprintf(['   - ',self.txt.report('lon'),': %.2f �'],self.levelCoords(levelVarId).lon)
                      }, r.secondPanel);                    
                
            end
            
            %%%% Put content in mainPanel
            var=self.offshoreData(levelVarId);
            
            % Graphs position and size
            x1 = 0.1;
            x2 = 0.58;
            y1 = 0.14;
            y2 = 0.61;
            graphSizeY = 0.34;
            graphSizeX = 0.36;
            
            r.addTimeSeries([x1, y2, graphSizeX, graphSizeY], var); % Series            
            r.addHist([x2, y2, graphSizeX, graphSizeY], var); % Histogram                        
            r.addRmGevScalar([x1, y1, graphSizeX, graphSizeY], var); % GEV            
            r.addGevChart([x2, y1, graphSizeX, graphSizeY],var); % Pot
   
            strEq=sprintf('$$%s%s%s$$','\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}',...
               report.Report.latexEquation('gev'),'\end{array}');            
            r.writeText([0.1 0 graphSizeX 0.1], strEq);
            strEq=sprintf('$$%s%s%s$$','\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}',...
               report.Report.latexEquation('gevInvRtSimple'),'\end{array}');            
            r.writeText([0.58 0 graphSizeX 0.1], strEq);            
          
            %%%% Print and Delete
            outFile=fullfile(self.outputFolder,sprintf('%sCharacterization.png',levelVarId));
            r.printPng(outFile);
            delete(r);
        end
        
        function waveReport(self,poiId)
            calmFilter=0.1; %Hs<0.1 m es considerada una calma
            if exist('poiId','var')                
                data=ameva.loadAmevaDataFromPoi(self.mopla,poiId,self.project,self.alternativa);
                amevaHs=data('hs');            
                amevaDir=data('waveDir');
                outFile=fullfile(self.outputFolder,sprintf('%sWaveCharacterization.png',poiId));
                strTitle=sprintf(self.txt.report('rtWaveCharacterization'),poiId);
                x=self.mopla.pois(poiId).x;
                y=self.mopla.pois(poiId).y;
                color='y';
                r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
                
            else
                amevaHs=self.offshoreData('hs');            
                amevaDir=self.offshoreData('waveDir');                
                outFile=fullfile(self.outputFolder,'dowWaveCharacterization.png');
                strTitle=sprintf(self.txt.report('rtWaveCharacterization'),' DOW');
                x=self.mopla.x;
                y=self.mopla.y;
                color='r';
                r=report.Report(self.caseInfo,self.batData,strTitle);
                
            end
            
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,x,y,['.',color],'MarkerSize',12);            
  
            %%%% Put content in infoPanel
            header={'Sector','%','Hs_{50%}','Hs_{75%}','Hs_{95%}','Hs_{99%}','Hs_{12}'};
            rowNames={'N','NNE','NE','ENE','E','ESE','SE','SSE','S','SSW','SW','WSW','W','WNW','NW','NNW',self.txt.report('lScalar'),self.txt.report('lCalm')};            
            
            data=zeros(17,6);
            y=amevaDir.getData;            
            [~,sectorsIndex]=amevaDir.getDirSectors(16);
            data(1:end-1,1)=sum(sectorsIndex,1)*100/length(y); % Percentage related to each sector
            data(end,1)=100;
            for i=1:17
                if i==17
                    subSet=amevaHs.getData;
                    calmPrc=sum(subSet<calmFilter)/length(subSet);
                else
                    subSet=amevaHs.getSubsetData(sectorsIndex(:,i));
                end
                data(i,2)=prctile(subSet,50);
                data(i,3)=prctile(subSet,75);
                data(i,4)=prctile(subSet,95);
                data(i,5)=prctile(subSet,99);
                data(i,6)=prctile(subSet,99.863);                
            end           
            
            data=cat(1,data,[calmPrc*100,NaN,NaN,NaN,NaN,NaN]);
            
            
            r.addTable([0.01,0.02,0.98,0.96], data, header, rowNames, r.secondPanel);
            
            %%%% Put content in mainPanel
            amevaTp=self.offshoreData('tp');
            % Graphs position and size
            x1 = 0.1;
            x2 = 0.58;
            y1 = 0.14;
            y2 = 0.58;
            graphSize = 0.36;            
            r.addTimeSeries([x1, 0.61, graphSize, 0.34], amevaHs); % Series
            r.addDirRose([0.52, 0.56, 0.38, 0.38], amevaHs, amevaDir);% Roses
            r.addDirRose([0.52, 0.07, 0.38, 0.38], amevaTp, amevaDir);            
            r.addRmGevScalar([x1, y1, graphSize, 0.34], amevaHs); % GEV
            % Text
            fDistGev2=['$$\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}F(x;\mu,\psi,\xi)\;& = & \exp\;(-(1+\xi(\frac{x-\mu}{\psi}\;))\;^{-1/\xi} \;) \\ ', ...
            ' x(F;\mu,\psi,\xi)\;& = & \mu - \frac{\psi}{\xi}(1 - (-ln(F))\;^{-\xi} ) \end{array} $$ '];
            r.writeText([x1 0 graphSize 0.1], fDistGev2);
            
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
            
        end
        
        function wave2DReport(self,poiId)
             if exist('poiId','var')                
                data=ameva.loadAmevaDataFromPoi(self.mopla,poiId,self.project,self.alternativa);
                amevaHs=data('hs');   
                amevaTp_=self.offshoreData('tp');
                tp=interp1(amevaTp_.getTime,amevaTp_.getData,amevaHs.getTime);
                amevaTp=ameva.AmevaVar('tp','s',tp,amevaHs.getTime);
                
                outFile=fullfile(self.outputFolder,sprintf('%sWaveCharacterization2D.png',poiId));
                strTitle=sprintf(self.txt.report('rtWaveCharacterization2D'),poiId);
                x=self.mopla.pois(poiId).x;
                y=self.mopla.pois(poiId).y;
                color='y';
                r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            else
                amevaHs=self.offshoreData('hs'); 
                amevaTp=self.offshoreData('tp');
                outFile=fullfile(self.outputFolder,'dowWaveCharacterization2D.png');
                strTitle=sprintf(self.txt.report('rtWaveCharacterization2D'),' DOW');
                x=self.mopla.x;
                y=self.mopla.y;
                color='r';            
                r=report.Report(self.caseInfo,self.batData,strTitle);
             end
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,x,y,['.',color],'MarkerSize',12);               
            
            %%%% Put content in mainPanel
            
            % Graphs position and size
            x1 = 0.1;
            x2 = 0.58;            
            y2 = 0.53;
            y3 = 0.75;
            graphSize = 0.4;
            smallGraphSize = 0.14;
            tableHeight = 0.2;
            tableWidth = 0.8;
            
            PDFaxes = r.addPDF2D([x1, y2, graphSize, graphSize], amevaHs, amevaTp);% PDF            
            r.addPDF2D_Percentile([x2, y3, graphSize, smallGraphSize], amevaHs, amevaTp, 50, PDFaxes, [0.8 0.8 0.8]);% Percentiles (Percentil 50 y Hs12)
            probHs12 = 100*(1-12/(365*24));
            tp12=r.addPDF2D_Percentile([x2, y2, graphSize, smallGraphSize], amevaHs, amevaTp, probHs12, PDFaxes, [1 0 0]);            
            r.addBin2DTable([x1, 0.05, tableWidth, tableHeight], amevaHs, amevaTp);% Bin Table
            
            r.writeString([0.62 0.94 graphSize 0.05], self.txt.report('tPDFPercGeneral'));
            
            %Percentiles Table
            r.writeString([0.12,0.42,0.2,0.05],self.txt.report('lPrcHs'));
            header={'Hs_{50%}(m)','Hs_{12}(m)'};
            hs50=prctile(amevaHs.getData,50);
            hs12=prctile(amevaHs.getData,99.863);
            data=[hs50,hs12];
            r.addTable([0.15,0.32,0.2,0.07],data,header,[]);
            
            %Closure depth
            if exist('poiId','var')  
                r.writeString([0.46,0.42,0.2,0.05],self.txt.report('lClosDepth'));
                g=9.81;
                hc_birk=1.75*hs12-57.9*(hs12/(g*tp12^2));
                hc_hall=2.28*hs12-68.5*(hs12/(g*tp12^2));
                
                strHclo={sprintf('Hs_{12} = %.2f m',hs12),'',sprintf('Tp(Tp|Hs_{12}) = %.2f s', tp12)};
                r.writeString([0.46 0.36 0.1 0.12],strHclo);
                
                strEq=sprintf('$$%s%s%s%.2f%s%s%s%s%.2f%s%s$$','\left\lbrace\begin{array}{l}',...
               report.Report.latexEquation('birkmeier'),' \approx ',hc_birk,'\;m\quad Birkmeier\,(1985) \\','\\',...
               report.Report.latexEquation('hallermeier'),' \approx ',hc_hall,'\;m\quad Hallermeier\,(1981)',...
               '\end{array}\right.');            
            r.writeText([0.62 0.3 0.25 0.12], strEq);
                
            end
            
            
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);

        end
        
        function waveRmReport(self,varId,poiId)
            if exist('poiId','var')                
                data=ameva.loadAmevaDataFromPoi(self.mopla,poiId,self.project,self.alternativa);
                amevaHs=data('hs');
                amevaDir=data(amevaHs.dir);
                if strcmp(varId,'tp')                
                    amevaTp=self.offshoreData('tp');
                    tp=interp1(amevaTp.getTime,amevaTp.getData,amevaHs.getTime);
                    amevaVar=ameva.AmevaVar('tp','s',tp,amevaHs.getTime);
                else
                    amevaVar=amevaHs;
                end
                
                outFile=fullfile(self.outputFolder,sprintf('%sRm_%s.png',poiId,varId));
                strTitle=sprintf(self.txt.report('rtRm'),self.txt.report(varId),self.txt.report([varId,'Short']),poiId); %% quetzal
                x=self.mopla.pois(poiId).x;
                y=self.mopla.pois(poiId).y;
                color='y';
                r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            else
                amevaVar=self.offshoreData(varId);
                amevaDir=self.offshoreData(amevaVar.dir);
                outFile=fullfile(self.outputFolder,sprintf('dowRm_%s.png',varId));
                strTitle=sprintf(self.txt.report('rtRm'),self.txt.report(varId),self.txt.report([varId,'Short']),'DOW'); %% quetzal
                x=self.mopla.x;
                y=self.mopla.y;
                color='r';
                r=report.Report(self.caseInfo,self.batData,strTitle);
            end
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,x,y,['.',color],'MarkerSize',12);          
            
            %%%% Put content in mainPanel
            [par,perSectors] = r.addRmGevDir(amevaVar,amevaDir);
            
            mu = nan(length(par),1);
            psi = nan(length(par),1);
            xi = nan(length(par),1);
            
            for i = 1: length(par)
                if ~isempty(par(i).mu)
                    mu(i)=par(i).mu;
                    psi(i)=par(i).psi;
                    xi(i)=par(i).xi;
                end
            end
            
            tableData=[mu, psi ,xi,perSectors'];
            header={'Sector','\mu','\psi','\xi','%'};
            rowNames={'N','NNE','NE','ENE','E','ESE','SE','SSE','S','SSW','SW','WSW','W','WNW','NW','NNW'};

            % Table
            r.addTable([0.05 0.2 0.9 0.75], tableData, header, rowNames, r.secondPanel);

            % Text
            r.writeString([0.05 0.15 1 0.05],self.txt.report('lGevDistribution'),r.secondPanel);%,'FontWeight','b')
             strEq=sprintf('$$%s%s%s$$','\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}',...
               report.Report.latexEquation('gev'),'\end{array}');                        
            r.writeText([0 0.02 1 0.1],strEq, r.secondPanel)            
            
            
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
            
            
        end
        
        function waveExtremeReport(self,poiId)
            if exist('poiId','var')                
                data=ameva.loadAmevaDataFromPoi(self.mopla,poiId,self.project,self.alternativa);
                amevaHs=data('hs');   
                amevaTp_=self.offshoreData('tp');
                tp=interp1(amevaTp_.getTime,amevaTp_.getData,amevaHs.getTime);
                amevaTp=ameva.AmevaVar('tp','s',tp,amevaHs.getTime);
                
                outFile=fullfile(self.outputFolder,sprintf('%sWaveExtreme.png',poiId));
                strTitle=sprintf(self.txt.report('rtWaveExtreme'),poiId);
                x=self.mopla.pois(poiId).x;
                y=self.mopla.pois(poiId).y;
                color='y';
                r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            else
                amevaHs=self.offshoreData('hs'); 
                amevaTp=self.offshoreData('tp');
                outFile=fullfile(self.outputFolder,'dowWaveExtreme.png');
                strTitle=sprintf(self.txt.report('rtWaveExtreme'),' DOW');
                x=self.mopla.x;
                y=self.mopla.y;
                color='r';            
                r=report.Report(self.caseInfo,self.batData,strTitle);
            end
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,x,y,['.',color],'MarkerSize',12);            
            
            %%%% Put content in infoPanel
            %r.writeString([0.05 0.9 0.9 0.05],self.txt.report('lPPDistribution'),r.secondPanel,'FontWeight','b');            
            r.writeString([0.05 0.7 1 0.05],self.txt.report('lGevDistribution'),r.secondPanel);%,'FontWeight','b')
            %r.writeString([0.05 0.3 0.9 0.05],self.txt.report('lPPDistribution'),r.secondPanel,'FontWeight','b')
            %strEq=sprintf('$$%s %s %s %s %s$$','\begin{array}{rcl}',report.Report.latexEquation('pareto'),'\\',report.Report.latexEquation('paretoInvRt'),'\end{array}');
            %r.writeText([0.05 0.75 0.6 0.1],strEq, r.secondPanel)
            strEq1=sprintf('$$%s %s %s$$','\begin{array}{rcl}',report.Report.latexEquation('gev'),'\end{array}');
            strEq2=sprintf('$$%s %s %s$$','\begin{array}{rcl}',report.Report.latexEquation('gevInvRt'),'\end{array}');
            r.writeText([0 0.45 1 0.15],{strEq1,'','',strEq2}, r.secondPanel)
            %strEq=sprintf('$$%s %s %s$$','\begin{array}{rcl}',report.Report.latexEquation('bcPoly'),'\end{array}');
            %r.writeText([0.05 0.2 0.75 0.1],strEq, r.secondPanel)
            
            %%%% Put content in mainPanel
            x1 = 0.1;
            x2 = 0.58;            
            y2 = 0.58;
            graphSize = 0.36;            
            
            %Pot Charts
            %[fitParHs,bcHs,thrHs]=r.addPotChart([x1, y2, graphSize, graphSize],amevaHs);
            %[fitParTp,bcTp,thrTp]=r.addPotChart([x2, y2, graphSize, graphSize],amevaTp);
            fitParHs=r.addGevChart([x1, y2, graphSize, graphSize],amevaHs);
            fitParTp=r.addGevChart([x2, y2, graphSize, graphSize],amevaTp);
            
            
            %Table
            %header={'Variable','u','\lambda','\sigma','\mu','\psi','\xi','c_0','c_1','c_2','c_3'};
            header={'Variable','\mu','\psi','\xi'};
            rowNames={'Hs','Tp'};
            data=zeros(2,3);
            data(1,:)=[fitParHs.mu,fitParHs.psi,fitParHs.xi];
            data(2,:)=[fitParTp.mu,fitParTp.psi,fitParTp.xi];
            r.addTable([0.35 0.4 0.3 0.08], data, header, rowNames);
            r.writeString([0.38 0.5 0.3 0.08],self.txt.report('ltitletablehstp')) % Quetzal %%%%%%%%%%%%%%%%%%%%%%%%%%
            %Max Annual Chart
            fitParMax=r.addAnnualMaxChart([x1,0.05,graphSize,0.3],amevaHs,amevaTp);
            %fit equation
            %strEq=sprintf('$$%s %s$$','\begin{array}{rcl}\mu(Tp|Hs)=aHs^b','\\ \sigma(Tp|Hs)=aHs^b \end{array}');
            %r.writeText([0.6 0.25 0.2 0.05],strEq);
            r.writeString([0.6 0.3 0.2 0.05],{'\mu_{MA}(Tp|Hs)=a(Hs)^b',...
                '','\sigma_{MA}(Tp|Hs)=a(Hs)^b'});
            %Table Fit
            header={'Tp','a','b'};
            rowNames={'\mu(Tp|Hs)','\sigma(Tp|Hs)'};
            data=[fitParMax.a,fitParMax.b; fitParMax.aSigma,fitParMax.bSigma];
            r.addTable([0.58 0.13 0.25 0.1], data, header, rowNames);
            
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
            
        end
        
        function waveSeasonRoseReport(self,poiId)
            if exist('poiId','var')                
                data=ameva.loadAmevaDataFromPoi(self.mopla,poiId,self.project,self.alternativa);
                amevaVar=data('hs');   
                amevaDir=data('waveDir');
                outFile=fullfile(self.outputFolder,sprintf('%sWaveSeasonRose.png',poiId));
                strTitle=sprintf(self.txt.report('rtWaveSeasonRose'),poiId);
                x=self.mopla.pois(poiId).x;
                y=self.mopla.pois(poiId).y;
                color='y';
                r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
                
            else
                amevaVar=self.offshoreData('hs');                 
                amevaDir=self.offshoreData('waveDir');
                outFile=fullfile(self.outputFolder,'dowWaveSeasonRose.png');
                strTitle=sprintf(self.txt.report('rtWaveSeasonRose'),' DOW');
                x=self.mopla.x;
                y=self.mopla.y;
                color='r';            
                r=report.Report(self.caseInfo,self.batData,strTitle);
                
            end
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,x,y,['.',color],'MarkerSize',12);
            
            %%%% Put content in infoPanel
            strText = {self.txt.report('lWinter'),self.txt.report('lSpring'),self.txt.report('lSummer'), self.txt.report('lAutumn')};
            r.addAnnotation([0.77, 0.69, 0.2, 0.08], strText);
            r.writeString([0.18 0.72 0.2 0.05],self.txt.report('lTitle'),r.secondPanel) %% Quetzal %%%%%%%%%%%%%%%%%%%%%%%
            
            %%%% Put content in mainPanel
            rosesSize = 0.38;
            r.addDirRoseSeas(amevaVar, amevaDir, rosesSize);
            
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);

        end
        
        function waveSomReport(self,poiId)
            if exist('poiId','var')                
                data=ameva.loadAmevaDataFromPoi(self.mopla,poiId,self.project,self.alternativa);
                amevaHs=data('hs');   
                amevaDir=data('waveDir');
                amevaTp_=self.offshoreData('tp');
                tp=interp1(amevaTp_.getTime,amevaTp_.getData,amevaHs.getTime);
                amevaTp=ameva.AmevaVar('tp','s',tp,amevaHs.getTime);
                
                outFile=fullfile(self.outputFolder,sprintf('%sWaveCharacterizationSOM.png',poiId));
                strTitle=sprintf(self.txt.report('rtWaveCharacterizationSOM'),poiId);
                x=self.mopla.pois(poiId).x;
                y=self.mopla.pois(poiId).y;
                color='y';
                r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            else
                amevaHs=self.offshoreData('hs'); 
                amevaDir=self.offshoreData('waveDir');
                amevaTp=self.offshoreData('tp');
                outFile=fullfile(self.outputFolder,'dowWaveCharacterizationSOM.png');
                strTitle=sprintf(self.txt.report('rtWaveCharacterizationSOM'),' DOW');
                x=self.mopla.x;
                y=self.mopla.y;
                color='r';            
                r=report.Report(self.caseInfo,self.batData,strTitle);
            end
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,x,y,['.',color],'MarkerSize',12);             
           
            
            %%%% Put content in mainPanel
            nSom = 15;
            r.addSOMChar(amevaHs, amevaTp, amevaDir, nSom);
            
            %%%% Put content in infoPanel
            strText = {self.txt.report('lWinter'),self.txt.report('lSpring'),self.txt.report('lSummer'), self.txt.report('lAutumn')}; 
            r.addAnnotation([0.77, 0.52, 0.2, 0.07], strText);
            r.writeString([0.18 0.24 0.2 0.05],self.txt.report('lTitle'),r.secondPanel) 
            % imagen
            %r.putImage([0.1 0.2 0.8 0.6],'hexagono_SOM.png',r.secondPanel);
            r.putImage([0.15 0.44 0.7 0.5],'hexagono_SOM.png',r.secondPanel);
            % caja de simbolos
            strText2 = {self.txt.report('hsdef'),self.txt.report('tpdef'),self.txt.report('wddef')};
            r.addAnnotation([0.77 0.62 0.2 0.05], strText2);
 
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
        end
        
        function classificationReport(self)
            amevaHs=ameva.AmevaVar('hs','m',self.mopla.hs,self.mopla.time);
            amevaDir=ameva.AmevaVar('waveDir','� N',self.mopla.dir,self.mopla.time);
            amevaTp=ameva.AmevaVar('tp','s',self.mopla.tp,self.mopla.time);
            prob=self.mopla.probabilidad;
            casosMD=self.mopla.casosMD(logical(self.mopla.propagate));
            
            outFile=fullfile(self.outputFolder,'waveClassification.png');
            r=report.Report(self.caseInfo,self.batData,self.txt.report('rtClassification'));
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            plot(r.mapAxes,self.mopla.x,self.mopla.y,'.r','MarkerSize',12);
            
            %%%% Put content in mainPanel
            r.addClasifficationChart([0.05 0.25 0.9 0.75],prob,amevaTp,amevaHs,amevaDir,casosMD);
            r.addClasifficationScatterChart([0.05 0.05 0.25 0.15],amevaHs,amevaTp,casosMD);
            r.addClasifficationScatterChart([0.375 0.05 0.25 0.15],amevaHs,amevaDir,casosMD);
            r.addClasifficationScatterChart([0.7 0.05 0.25 0.15],amevaTp,amevaDir,casosMD);
            
            %%%% Put content in infoPanel
            % caja de simbolos
            strText = {self.txt.report('hsdef'),self.txt.report('tpdef'),self.txt.report('wddef'),...
                self.txt.report('prbdef')};
            r.addAnnotation([0.77 0.55 0.2 0.07], strText);

            r.putImage([0.1 0.2 0.8 0.6],'hexagono_MaxDiss.png',r.secondPanel);
            
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
        end
        
        function meshesReport(self)  
            
            mallasTmp=self.mopla.mallas;
            mallasStr={};
            mallas=[];
            
            for i=1:length(mallasTmp)
                if self.mopla.propagate(i)
                    malla=self.alternativa.mallas(mallasTmp{i});
                    if ~ismember(malla.nombre,mallasStr)
                        mallas=cat(1,mallas,malla);        
                        mallasStr=cat(1,mallasStr,malla.nombre);        
                    end
                end
            end
            
            nFichas=ceil(length(mallas)/4);
            for i=1:nFichas
                indLow=(i-1)*4+1;
                indUp=min(i*4,length(mallas));                
                
                outFile=fullfile(self.outputFolder,sprintf('meshReport_%d.png',i));
                r=report.Report(self.caseInfo,self.batData,sprintf(self.txt.report('rtMeshes'),i,nFichas));
                util.freezeColors(r.mapAxes);
                 
                x=[0.1,0.58,0.1,0.58];
                y=[0.58,0.58,0.1,0.1];
                graphSize = 0.36;                
                index=1;         
                data={};
                for j=indLow:indUp                                       
                   ax=r.addMap([x(index) y(index) graphSize graphSize],self.batDataMeshesZoom);  
                   % set(ax,'YTick',[],'XTick',[]);
                    
                    hold(ax,'on');
                    malla=mallas(j);                                    
                    [polX,polY]=util.mallaVertex(malla);
                    fill(polX,polY,'k','EdgeColor','y','LineWidth',1.5,'FaceColor','none','Parent',ax);
                    if ~isempty(malla.parent)
                        mallaParent=self.alternativa.mallas(malla.parent);
                        [polX,polY]=util.mallaVertex(mallaParent);
                        fill(polX,polY,'k','EdgeColor','y','LineWidth',1.5,'FaceColor','none','Parent',ax);
                        strTitle=[mallaParent.nombre ,' + ', malla.nombre];
                        data{index,1}=strrep(mallaParent.nombre, '_', ' ');
                        data{index,2}=mallaParent.dx;
                        data{index,3}=mallaParent.angle;                        
                        data{index,4}=strrep(malla.nombre, '_', ' ');
                        data{index,5}=malla.dx;
                    else
                        data{index,1}=strrep(malla.nombre, '_', ' ');
                        data{index,2}=malla.dx;
                        data{index,3}=malla.angle;                                                
                        data{index,4}='-';                                                
                        data{index,5}='-';                                                
                        strTitle=malla.nombre;
                    end
                    strTitle=strrep(strTitle, '_', ' ');
                    strTitle=strrep(strTitle, '\', ' ');
                    title(ax,strTitle);
                    hold(ax,'off');
                    set(ax,'XTick',[],'YTick',[]);
                    r.setFormat(get(ax,'title'));
                    index=index+1;                    
                end
                
                
                header={sprintf('%s 1',self.txt.report('mesh')),'\delta x_1 (m)',['\theta_{',self.txt.report('mesh'),'}'],sprintf('%s 2',self.txt.report('mesh')),'\delta x_2 (m)'};
                r.addTable([-0.02 0.3 1 0.4], data, header, [], r.secondPanel,'FontSize',6);
                
                % aqui la leyenda! %%%%% Quetzal
                strText = {self.txt.report('deltaX'),self.txt.report('thetamalla')};
                r.addAnnotation([0.77 0.55 0.2 0.05], strText);
                
                
                %%%% Set the Mini-Map
                r.addZoomBox(self.batDataMeshesZoom,'r','LineStyle','--');
                hold(r.mapAxes,'on');
                plot(r.mapAxes,self.mopla.x,self.mopla.y,'.r','MarkerSize',12);
                
                %%%% Print and Delete            
                r.printPng(outFile);
                delete(r);                
            end            
        end
        
        function selectedCasesReport(self)
             
            nFichas=ceil(length(self.mopla.casosMD)/100);
            hs=self.mopla.hs(self.mopla.casosMD);
            tp=self.mopla.tp(self.mopla.casosMD);
            dir=self.mopla.dir(self.mopla.casosMD);
            
            %Calculo el intervalo Nikuradse e Iteraciones para Copla
            %Esto es común para todos las fichas
            if self.mopla.coplaTime>0
                tiempoTotal=self.mopla.coplaTime;
                mallasTmp=self.mopla.mallas;
                mallasStr={};                
                iteracciones=[];
                eddy=[];
                %nikuradse=[];
            
                for i=1:length(mallasTmp)
                    if self.mopla.propagate(i)
                        malla=self.alternativa.mallas(mallasTmp{i});
                        if ~ismember(malla.nombre,mallasStr)
                            h=malla.h;
                            h(h<0)=[];
                            h(isnan(h))=[];
                            intervalo=round(malla.dx/(sqrt(9.81*mean(mean(h)))+0.5));
                            iteracciones=cat(1,iteracciones,floor(tiempoTotal/intervalo));
                            eddy=cat(1,eddy,round(min(malla.dx/2,malla.dy/2)));
                            mallasStr=cat(1,mallasStr,malla.nombre);  
                            
                        end
                    end
                end
            end
          
            
            for i=1:nFichas
                indLow=(i-1)*100+1;
                indUp=min(i*100,length(self.mopla.casosMD));
                
                outFile=fullfile(self.outputFolder,sprintf('selectedCases_%d.png',i));
                r=report.Report(self.caseInfo,self.batData,sprintf(self.txt.report('rtSelectedCases'),i,nFichas));
                data1={};                
                data2={};                
                
                probIndex=1;
                for j=indLow:indUp
                    index=j-indLow+1;
                    malla=self.alternativa.mallas(self.mopla.mallas{j});
                    propagated='false';                    
                    if self.mopla.propagate(j)
                        propagated='true';
                        prob=self.mopla.probabilidad(probIndex)*100;                        
                        probIndex=probIndex+1;
                    else
                        prob='-';
                    end
                    dir_rel=270-dir(index)-malla.angle;
                    dir_rel=atan2(sind(dir_rel),cosd(dir_rel))*180/pi;                       
                     
                    
                    if ~isempty(malla.parent)
                        mallaParent=self.alternativa.mallas(malla.parent);
                        if index>50
                            data2=cat(1,data2,{num2str(j),hs(index),tp(index),dir(index),dir_rel,prob,propagated,strrep(mallaParent.nombre, '_', ' '),strrep(malla.nombre, '_', ' ');});
                        else
                            data1=cat(1,data1,{num2str(j),hs(index),tp(index),dir(index),dir_rel,prob,propagated,strrep(mallaParent.nombre, '_', ' '),strrep(malla.nombre, '_', ' ')});
                        end
                        
                    else
                        if index>50
                            data2=cat(1,data2,{num2str(j),hs(index),tp(index),dir(index),dir_rel,prob,propagated,strrep(malla.nombre, '_', ' '),'-'});
                        else
                            data1=cat(1,data1,{num2str(j),hs(index),tp(index),dir(index),dir_rel,prob,propagated,strrep(malla.nombre, '_', ' '),'-'});
                        end
                    end
                end
                
                for j=index+1:50
                    data1=cat(1,data1,{'-','-','-','-','-','-','-','-','-'});
                end
                if index>=50
                    for j=index+1:100
                        data2=cat(1,data2,{'-','-','-','-','-','-','-','-','-'});
                    end
                end
                
                header={self.txt.report('case'),'Hs','Tp','Dir','DirRel','Prob (%)',self.txt.report('lPropagated'),[self.txt.report('mesh'),' 1'],[self.txt.report('mesh'),' 2']};
                r.addTable([0.016 0.1 0.475 0.8], data1, header, [],r.mainPanel,'FontSize',5);
                if ~isempty(data2)
                    r.addTable([0.5 0.1 0.475 0.8], data2, header, [],r.mainPanel,'FontSize',5);
                end
                
                %%%% Set the info panel (Tide Plot)
                tide=self.offshoreData('tide');                
                
                tide_=tide.getData;
                tide_=tide_-min(tide_);  
                IncY=((max(tide_)-min(tide_))/50);
                V=floor(min(tide_)):IncY:max(tide_);
                [N,x]=hist(tide_,V+(IncY/2));
                %N=tide.getHistN();
                %x=tide.getHistX();                 
                %x=x-min(x);
                prob=cumsum(N);
                prob=prob/prob(end);
                ax=axes('Parent',r.secondPanel,'Position',[0.2 0.52 0.6 0.36]);
                plot(ax,x,prob);
                title(ax,sprintf(self.txt.report('tSeaLevelCdf')));
                xlabel(ax,[self.txt.report('tideShort'),' (m)']);
                ylabel(ax,'CFD');                
                hold(ax,'on');                                
                probLevel=interp1(x,prob,self.mopla.tide);
                mopTide=self.mopla.tide;
                probLevel(mopTide<min(x))=0;
                probLevel(mopTide>max(x))=1;                
                
                for j=1:length(self.mopla.tide)                    
                    plot(ax,[self.mopla.tide(j),self.mopla.tide(j)],[0 probLevel(j)],'r');
                    plot(ax,[0,self.mopla.tide(j)],[probLevel(j) probLevel(j)],'r');
                    plot(ax,self.mopla.tide(j),probLevel(j),'or','MarkerFaceColor','r');
                end
                hold(ax,'off');
                set(ax,'xlim',[0 max(x)]);
                
                %%%% Put data in info panel
                r.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel')]);         
                levelStr=sprintf('%.2f m',self.mopla.tide(1));
                for j=2:length(self.mopla.tide);
                    levelStr=cat(2,levelStr,sprintf(', %.2f m',self.mopla.tide(j)));
                end                                
                levelStr=[self.txt.report('lSeaLevels'),': ',levelStr];
                
                gammaStr=sprintf('%.2f',self.mopla.gamma(1,2));
                if size(self.mopla.gamma,1)>1
                    gammaStr=sprintf('%.2f - %2.f',min(self.mopla.gamma(:,2)),max(self.mopla.gamma(:,2)));
                end
                gammaStr=[self.txt.report('lFreqDisp'),': ',gammaStr];
                
                sigmaStr=sprintf('%.2f�',self.mopla.sigma(1,2)); %%%% Quetzal
                if size(self.mopla.sigma,1)>1
                    sigmaStr=sprintf('%.2f - %2.f�',min(self.mopla.sigma(:,2)),max(self.mopla.sigma(:,2))); %%%%% Quetzal
                end
                sigmaStr=[self.txt.report('lDirDisp'),': ',sigmaStr];
                
                r.writeString([0.05 0.34 0.9 0.2],{levelStr,sigmaStr,gammaStr},r.secondPanel);   
                
                if self.mopla.coplaTime>0
                    nikuStr=sprintf('  %s (K_{SWC}): 1 m',self.txt.report('lNikuradse'));
                    eddyStr=sprintf('  %s (\\xi): %2.f - %2.f m^2/s',self.txt.report('lEddy'),min(eddy),max(eddy));
                    iterStr=sprintf('  %s: %2.d - %2.d',self.txt.report('lIteration'),min(iteracciones),max(iteracciones));
                    r.writeString([0.05 0.15 0.9 0.2],{self.txt.report('lCurrents'),nikuStr,eddyStr,iterStr},r.secondPanel);   
                end
                
                
                
                
                %%%% Set the Mini-Map
                r.addZoomBox(self.batDataRoiZoom,'r');
                hold(r.mapAxes,'on');
                plot(r.mapAxes,self.mopla.x,self.mopla.y,'.r','MarkerSize',12); 
                
                %%%% Print and Delete            
                r.printPng(outFile);
                delete(r);            
                
            end
        end
        
        function poisReport(self)
            pois=self.getRebuildedPois();
            [~,~,z1]=util.deg2utm(self.mopla.lat,self.mopla.lon);
            
            
            %nFichas=ceil(length(pois)/25);            
            %for i=1:nFichas
            i=1;
            indLow=(i-1)*self.maxPois+1;
            indUp=min(i*self.maxPois,length(pois));      

            outFile=fullfile(self.outputFolder,sprintf('pointsOfInterest.png'));
            %r=report.Report(self.caseInfo,self.batDataRoiZoom,sprintf(self.txt.report('rtPois'),i,nFichas));                
            r=report.Report(self.caseInfo,self.batDataRoiZoom,sprintf(self.txt.report('rtPois')));                

            %%%% Put content in mainPanel
           

            data={};                  
            ax=r.addMap([0.1 0.1 0.8 0.8],self.batDataRoiZoom);   
             cb=colorbar('peer',ax);            
            r.setFormat(cb);

            hold(ax,'on')                
            for j=indLow:indUp
                poi=self.mopla.pois(pois{j});
                plot(ax,poi.x,poi.y,'or','MarkerFaceColor','r','MarkerSize',10);                      
                text(poi.x,poi.y,num2str(j),'Color','w','HorizontalAlignment','center','Parent',ax);                                                          
                [lat,lon]=util.utm2deg(poi.x,poi.y,z1);
                
                data=cat(1,data,{num2str(j),poi.nombre,lat,lon,poi.x,poi.y,poi.z});
            end

            hold(ax,'off')               
            r.setFormat(findall(ax,'type','text'),'FontWeight','bold');

            %%%% Put content in infoPanel                                
            header={self.txt.report('point'),'Id','Lat (�)','Lon (�)','x utm (m)','y utm (m)','z (m)'};%self.txt.report('depth')};
            
            for j=indUp-indLow+2:self.maxPois
                data=cat(1,data,{'-','-','-','-','-','-','-'});
            end
            r.addTable([-0.01 0.05 1 0.9], data, header, [],r.secondPanel,'FontSize',5.4);


            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');
            for j=1:length(pois)                    
                poi=self.mopla.pois(pois{j});
                plot(r.mapAxes,poi.x,poi.y,'.y','MarkerSize',12); 
            end                

            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
            %end
        end
        
        function femReport(self)            
            pois=self.getRebuildedPois();
            [~,~,z1]=util.deg2utm(self.mopla.lat,self.mopla.lon);                
            %nFichas=ceil(length(pois)/25);            
            %for i=1:nFichas
                i=1;
                indLow=(i-1)*self.maxPois+1;
                indUp=min(i*self.maxPois,length(pois));      
                
                outFile=fullfile(self.outputFolder,sprintf('energyFluxMap.png'));
                %r=report.Report(self.caseInfo,self.batDataRoiZoom,sprintf(self.txt.report('rtFem'),i,nFichas));                 
                r=report.Report(self.caseInfo,self.batDataRoiZoom,sprintf(self.txt.report('rtFem')));                 
               
                %%%% Put content in mainPanel         
                l=self.calcularFemEscala/2;
                data={};                  
                ax=r.addMap([0.1 0.1 0.8 0.8],self.batDataRoiZoom);   
                cb=colorbar('peer',ax);            
                r.setFormat(cb);
                
                
                hold(ax,'on')  
                
                for j=indLow:indUp
                    poi=self.mopla.pois(pois{j});
                    [~,efDir]=self.calculaFem(pois{j}); 
                    [x,y,u,v]=util.getVectorVertex(poi.x,poi.y,l,270-efDir,'center');
                    util.arrow3([x,y],[u+x,v+y],'r-1',0.5,1);
                    %quiver(ax,x,y,u,v,'r','LineWidth',1);
                    plot(ax,poi.x,poi.y,'or','MarkerFaceColor','r','MarkerSize',9);                      
                    text(poi.x,poi.y,num2str(j),'Color','w','HorizontalAlignment','center','Parent',ax);                                          
                    [lat,lon]=util.utm2deg(poi.x,poi.y,z1);                    
                    data=cat(1,data,{num2str(j),poi.nombre,lat,lon,num2str(poi.x,'%.0f'),num2str(poi.y,'%.0f'),poi.z,efDir});
                end
                
                hold(ax,'off')               
                r.setFormat(findall(ax,'type','text'),'FontWeight','b');
                
                %%%% Put content in infoPanel                                
                header={self.txt.report('point'),'Id','Lat (�)','Lon (�)','x utm (m)','y utm (m)','z (m)','\theta_{FME} (�)'};
                for j=indUp-indLow+2:self.maxPois
                    data=cat(1,data,{'-','-','-','-','-','-','-','-'});
                end
                r.addTable([-0.01 0.05 1 0.9], data, header, [],r.secondPanel,'FontSize',5);
                
                
                %%%% Set the Mini-Map
                hold(r.mapAxes,'on');
                for j=1:length(pois)                    
                    poi=self.mopla.pois(pois{j});
                    plot(r.mapAxes,poi.x,poi.y,'.y','MarkerSize',12); 
                end                
                
                %%%% Print and Delete            
                r.printPng(outFile);
                delete(r);
            %end
        end  
        
        function femPoiReport(self,poiId)    
            outFile=fullfile(self.outputFolder,sprintf('%sEnergyFlux.png',poiId));
            r=report.Report(self.caseInfo,self.batDataRoiZoom,sprintf(self.txt.report('rtFemPoi'),poiId));                 
            
            %%%% Put content in main panel
            [ef,efDir]=self.calculaFem(poiId);            
            timeVec=datevec(self.offshoreData('tide').getTime);
            yearIni=min(timeVec(:,1));
            yearEnd=max(timeVec(:,1));
            efYear=[];
            efDirYear=[];
            for i=yearIni:yearEnd
                [ef_,efDir_]=self.calculaFem(poiId,i);
                efYear=cat(1,efYear,ef_);
                efDirYear=cat(1,efDirYear,efDir_);
            end
            ax = axes('Parent',r.mainPanel,'Position', [0.1 0.58 0.8 0.36]);
            plot(ax,yearIni:yearEnd,efYear,'o','MarkerFaceColor','r','LineStyle','-','Color','k','MarkerEdgeColor','k','LineWidth',1.2);
            hold(ax,'on');
            plot(ax,[yearIni,yearEnd],[ef,ef],'LineWidth',1.5);
            set(ax,'xlim',[yearIni,yearEnd]);
            
            title(ax,self.txt.report('tFemMagnitude'));
            xlabel(ax,self.txt.report('xYear'));
            ylabel(ax,[self.txt.report('fem'),' (J m^{-1} s^{-1})']);
            hold(ax,'off');
            l=legend('F_{FME}',sprintf('F_{FME %d-%d}',timeVec(1,1),timeVec(end,1)));
            r.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
            
            ax = axes('Parent',r.mainPanel,'Position', [0.1 0.1 0.8 0.36]);
            plot(ax,yearIni:yearEnd,efDirYear,'o','MarkerFaceColor','r','LineStyle','-','Color','k','MarkerEdgeColor','k','LineWidth',1.2);
            hold(ax,'on');
            plot(ax,[yearIni,yearEnd],[efDir,efDir],'LineWidth',1.2);
            set(ax,'xlim',[yearIni,yearEnd]);
            title(ax,self.txt.report('tFemDir'));
            xlabel(ax,self.txt.report('xYear'));            
            ylabel(ax,[self.txt.report('femDir'),' (� N)']);
            hold(ax,'off');
            l=legend('\theta_{FME}',['\theta_{FME ',num2str(timeVec(1,1)),'-',num2str(timeVec(end,1)),'}']);
            
            r.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
            
            
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');             
            x=self.mopla.pois(poiId).x;
            y=self.mopla.pois(poiId).y;
            plot(r.mapAxes,x,y,'.y','MarkerSize',12);                 
                
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
        end
        
        function profilesReport(self)
            profiles=self.getRebuildedProfiles();
            %nFichas=ceil(length(profiles)/25);            
            %for i=1:nFichas
            i=1;
            indLow=(i-1)*self.maxProfiles+1;
            indUp=min(i*self.maxProfiles,length(profiles));      

            outFile=fullfile(self.outputFolder,sprintf('profiles.png'));
            r=report.Report(self.caseInfo,self.batDataRoiZoom,sprintf(self.txt.report('rtProfiles'))); 
            
            
                
                %%%% Put content in mainPanel                
                data={};                  
                ax=r.addMap([0.1 0.1 0.8 0.8],self.batDataRoiZoom);   
                cb=colorbar('peer',ax);            
                r.setFormat(cb);
                
                hold(ax,'on')                
                for j=indLow:indUp
                    profile=self.mopla.perfiles(profiles{j});
                    plot(ax,profile.posicion(:,1),profile.posicion(:,2),'y','LineWidth',1.5);                      
                    %textPosition=[profile.posicion(2,1),profile.posicion(2,2)];
                    [textPosition(1),textPosition(2)]=profile.getMiddlePoint;
                    text(textPosition(1),textPosition(2),num2str(j),'Color','k',...%[0.4,0.4,0.4],...
                        'HorizontalAlignment','center','BackgroundColor','y','Parent',ax);    
                    
                    angle=90-profile.getAngle;
                    if angle<0,angle=angle+360;end
                    %tanB=post.getProfileSlope(self.mopla,self.project,self.alternativa.nombre,profiles{j});
                    tanB=profile.slope;
                    data=cat(1,data,{num2str(j),profile.nombre,profile.posicion(1,1),profile.posicion(1,2),angle,profile.d50,tanB});
                end
                
                hold(ax,'off')               
                r.setFormat(findall(ax,'type','text'),'FontWeight','bold');
                
                %%%% Put content in infoPanel                                
                header={self.txt.report('profile'),'Id','x_0 (m)','y_0 (m)','\theta_{costa} (�)','D50 (mm)','tan\beta'};
                for j=indUp-indLow+2:self.maxProfiles
                    data=cat(1,data,{'-','-','-','-','-','-','-'});
                end
                r.addTable([0 0.05 0.97 0.9], data, header, [],r.secondPanel,'FontSize',5);
                
                
                %%%% Set the Mini-Map
                hold(r.mapAxes,'on');
                for j=1:length(profiles)                    
                    profile=self.mopla.perfiles(profiles{j});
                    plot(r.mapAxes,profile.posicion(:,1),profile.posicion(:,2),'y','LineWidth',1.25);
                end                
                
                %%%% Print and Delete            
                r.printPng(outFile);
                delete(r);
            %end
        end
        
        function transportReport(self,season)
             profiles=self.getRebuildedProfiles();
             %nFichas=ceil(length(profiles)/25);                  
             
             if ~exist('season','var'), season='';end
             
             qP=[];qM=[];
              for i=1:length(profiles)
                    [qPPerfil,qMPerfil]=self.calculaTransporte(profiles{i},season);
                    qP=cat(1,qP,qPPerfil);
                    qM=cat(1,qM,qMPerfil);
              end
              qScale=self.calculaQEscala(max([qP;abs(qM)]));
              
             %for i=1:nFichas
             i=1;
                 indLow=(i-1)*self.maxProfiles+1;
                 indUp=min(i*self.maxProfiles,length(profiles));                      
                 outFile=fullfile(self.outputFolder,sprintf('transportMap%s.png',season));
                 
                if isempty(season)
                    strSeason=self.txt.report('lAnnual');
                    axTitle=self.txt.report('tTransportAvg');                    
                else
                    strSeason=self.txt.report(['l',season]);
                    axTitle=self.txt.report('tTransportAvgSeason');
                end
                 q=sum(qP+qM);
                 date=datevec(self.mopla.time);
                 yearIni=date(1,1)+1; %Elimino el primer a�o
                 yearEnd=date(end,1);
                 axTitle=[axTitle,sprintf(' (Qm_{%.0f-%.0f}=%.0f m^3/%s)',yearIni,yearEnd,q,self.txt.report('year'))];
                 
                 r=report.Report(self.caseInfo,self.batDataRoiZoom,self.txt.report(['rtTransport_',season]));                 
                 %%%% Put content in mainPanel                
                 data={};                        
                 %ax=r.addMap([0.1 0.1 0.8 0.8],self.batDataRoiZoom);   
                 ax=r.addCoastMap([0.1 0.25 0.8 0.67],self.batDataRoiZoom);  
                 title(ax,axTitle);                
                 
                 hold(ax,'on')                
                 for j=indLow:indUp
                     profile=self.mopla.perfiles(profiles{j});
                     plot(ax,profile.posicion(:,1),profile.posicion(:,2),'k','LineWidth',1);                      
                     textPosition=[profile.posicion(1,1),profile.posicion(1,2)];
                     %[textPosition(1),textPosition(2)]=profile.getMiddlePoint;
                     text(textPosition(1),textPosition(2),num2str(j),'Color','w',...
                         'HorizontalAlignment','center','BackgroundColor','k','Parent',ax); 
                     
                     [x1,y1,x2,y2]=profile.getNormalVector(-qM(j)*qScale,0.25);
                     %[x,y,u,v]=util.getVectorVertex(poi.x,poi.y,l,270-efDir,'center');
                     if x1~=x2 || y1~=y2
                        util.arrow3([x1,y1],[x2,y2],'b-1',0.5,1);
                     end
                     %quiver(ax,x1,y1,x2-x1,y2-y1,'b','LineWidth',0.7);                     
                     [x1,y1,x2,y2]=profile.getNormalVector(-qP(j)*qScale,0.5);                     
                     %quiver(ax,x1,y1,x2-x1,y2-y1,'r','LineWidth',0.7);
                     if x1~=x2 || y1~=y2
                        util.arrow3([x1,y1],[x2,y2],'r-1',0.5,1);
                     end
                     [x1,y1,x2,y2]=profile.getNormalVector(-(qP(j)+qM(j))*qScale,1);
                     %quiver(ax,x1,y1,x2-x1,y2-y1,'k','LineWidth',0.7);
                     if x1~=x2 || y1~=y2
                        util.arrow3([x1,y1],[x2,y2],'k-1',0.5,1);
                     end                     
                     data=cat(1,data,{num2str(j),profile.nombre,qP(j)+qM(j),qP(j),qM(j)});
                 end
                 
                hold(ax,'off')               
                r.setFormat(findall(ax,'type','text'),'FontWeight','bold');                  
                r.setFormat(get(ax,'title'),'FontWeight','normal');
               r.writeString([0.1,0.17,0.3,0.05], self.txt.report('lTransportFormulation'));
               strEq=sprintf('$$%s%s$$','\renewcommand{\arraystretch}{2}',...               
               report.Report.latexEquation(self.mopla.transportModel));     
               %strEq=sprintf('$$%s$$',report.Report.latexEquation('cerc'));     
                r.writeText([0.1,0.09,0.3,0.05], strEq);
                %r.writeString([0.36,0.12,0.15,0.05], ' CERC (1984)');
                
                
                a1=annotation('textarrow', [.46 .4], [.195 .195], 'String' , ' Q:', 'Color', 'k');
                a2=annotation('textarrow', [.46 .4], [.152 .152], 'String' , ' Q^+:', 'Color', 'r');
                a3=annotation('textarrow', [.46 .4], [.11 .11], 'String' , ' Q^-:', 'Color', 'b');
                r.writeString([0.64,0.17,0.3,0.05], self.txt.report('lTransportNet'));
                r.writeString([0.64,0.12,0.3,0.05], self.txt.report('lTransportPositive'));
                r.writeString([0.64,0.07,0.3,0.05], self.txt.report('lTransportNegative'));
                r.setFormat([a1,a2,a3]);
                
                 %%%% Put content in infoPanel                                
                 header={self.txt.report('profile'),'Id','Q (m^3)','Q^+(m^3)','Q^-(m^3)'};
                 for j=indUp-indLow+2:self.maxProfiles
                     data=cat(1,data,{'-','-','-','-','-'});
                 end
                 %if j<25
                     data=cat(1,data,{self.txt.report('mean'),'-',mean(qP+qM),mean(qP),mean(qM)});
                 %end
                 r.addTable([0.05 0.05 0.9 0.9], data, header, [],r.secondPanel,'FontSize',5);
                 
                 %%%% Set the Mini-Map
                 hold(r.mapAxes,'on');
                 for j=1:length(profiles)                    
                     profile=self.mopla.perfiles(profiles{j});
                     plot(r.mapAxes,profile.posicion(:,1),profile.posicion(:,2),'y','LineWidth',1.25);
                 end               
                
                %%%% Print and Delete            
                r.printPng(outFile);
                delete(r);
        %end
        end
        
        function transportProfileReport(self,perfilId)
            outFile=fullfile(self.outputFolder,sprintf('%sTransport.png',perfilId));
            r=report.Report(self.caseInfo,self.batDataRoiZoom,sprintf(self.txt.report('rtTransportProfile'),perfilId));                 
            
            %%%% Put content in infoPanel
            r.writeString([0.2 0.85 0.6 0.05],self.txt.report('lTransportCriteria'),r.secondPanel);
            r.putImage([0.1 0.2 0.6 0.6],'transport_criteria.png',r.secondPanel);
            
            %%%% Put content in main panel
            profile=self.mopla.perfiles(perfilId);                                                    
            angle=90-profile.getAngle;
            if angle<0;angle=angle+360;end;
            if angle>360;angle=angle-360;end;
            
            tteFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[perfilId,'_tte.mat']);
            tte=load(tteFile,'-mat','hs_b','h_b','dir_b','l_b','uAvg');
             if isfield(tte,'uAvg')
                  uAvg=tte.uAvg;
                else
                  uAvg=[];
            end
            tte.q=post.longshoreTransport(tte.hs_b,tte.h_b,tte.dir_b,self.mopla.tp,tte.l_b,uAvg,...
                profile,self.mopla.transportModel,self.mopla.kModel);

            
             perfilAng=atan2(profile.y(end)-profile.y(1),profile.x(end)-profile.x(1))*180/pi;
             dir_x=tte.dir_b+perfilAng-180; %%Direcci�n repecto a X_UTM
            [ef_x,ef_y] = post.fem(tte.hs_b,self.mopla.tp,tte.h_b,dir_x);
            tte.ef_x=ef_x;
            tte.ef_y=ef_y;
            %tte=tte.perfilTTE;
            
            
            timeVec=datevec(self.mopla.time);
            yearIni=min(timeVec(:,1))+1; %Quito el primer a�o porque el transporte no es fiabhe
            yearEnd=max(timeVec(:,1));
            nYears=yearEnd-yearIni+1;
            
            qYear=[];
            efDirYear=[];
            q=sum(tte.q)*3600/nYears;
            efDir=270-atan2(sum(tte.ef_y),sum(tte.ef_x))*180/pi;
            if efDir<0,efDir=efDir+360;end
            if efDir>360,efDir=efDir-360;end
            for i=yearIni:yearEnd
                index=timeVec(:,1)==i;                
                qYear=cat(1,qYear,sum(tte.q(index))*3600);
                efDirYear=cat(1,efDirYear,270-atan2(sum(tte.ef_y(index)),sum(tte.ef_x(index)))*180/pi);
            end
            efDirYear(efDirYear<0)=efDirYear(efDirYear<0)+360;
            efDirYear(efDirYear>360)=efDirYear(efDirYear>360)-360;
            ax = axes('Parent',r.mainPanel,'Position', [0.1 0.58 0.8 0.36]);
            years=yearIni:yearEnd;
            qPlus=plot(ax,years,qYear,'o','MarkerFaceColor','r','LineStyle','-','Color','k','MarkerEdgeColor','k','LineWidth',1.2);
            hold(ax,'on');
            %index=qYear<0;
            %qMinus=plot(ax,years(index),qYear(index),'o','MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',1.2);
            qMean=plot(ax,[yearIni,yearEnd],[q,q],'k','LineWidth',1.2);
            set(ax,'xlim',[yearIni,yearEnd]);
            
            title(ax,self.txt.report('tTransport'));
            xlabel(ax,self.txt.report('xYear'));
            ylabel(ax,[self.txt.report('transport'),' (m^3)']);
            hold(ax,'off');
            %l=legend([qPlus,qMinus,qMean],'Q+','Q-',sprintf('Q_{%d-%d}',yearIni,yearEnd));
            l=legend([qPlus,qMean],'Q',sprintf('Q_{%d-%d}',yearIni,yearEnd));
            r.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
            
            ax = axes('Parent',r.mainPanel,'Position', [0.1 0.1 0.8 0.36]);
            fePos=plot(ax,years,efDirYear,'o','MarkerFaceColor','r','LineStyle','-','Color','k','MarkerEdgeColor','k','LineWidth',1.2);
            hold(ax,'on')
            diff=(angle-efDirYear);
            diff(diff<-180)=diff(diff<-180)+360;
            diff(diff>180)=diff(diff>180)-360;
            index=diff<0;
            feNeg=plot(ax,years(index),efDirYear(index),'o','MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',1.2);
            fem=plot(ax,[yearIni,yearEnd],[efDir,efDir],'k','LineWidth',1.2);
            thetaC=plot(ax,[yearIni,yearEnd],[angle,angle],'--','Color',[0.6,0.6,0.6],'LineWidth',1.2);
            set(ax,'xlim',[yearIni,yearEnd]);
            title(ax,self.txt.report('tFemDir'));
            xlabel(ax,self.txt.report('xYear'));             
            ylabel(ax,[self.txt.report('femDir'),' (�N)']);
            hold(ax,'off');
            
            if sum(index)>0
            l=legend([fePos,feNeg,fem,thetaC],'\theta_{FMEb} (\theta_{costa}-\theta_{FMEb}>0)',...
                '\theta_{FMEb} (\theta_{COSTA}-\theta_{FMEb}<0)',...
                sprintf('\\theta_{FMEb-%.0f-%.0f}',yearIni,yearEnd),'\theta_{costa}');            
            else
            l=legend([fePos,fem,thetaC],'\theta_{FMEb} (\theta_{costa}-\theta_{FMEb}>0)',...                
                sprintf('\\theta_{FMEb-%.0f-%.0f}',yearIni,yearEnd),'\theta_{costa}');            
            end
            r.setFormat([ax,get(ax,'title'),get(ax,'xlabel'),get(ax,'ylabel'),l]);
            
            
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');                         
              plot(r.mapAxes,profile.posicion(:,1),profile.posicion(:,2),'y','LineWidth',1.25);              
                
            %%%% Print and Delete            
            r.printPng(outFile);
            delete(r);
        end
        
        function levelProfileReport(self,perfilId,levelVarId)
            strTitle=sprintf(self.txt.report('rtLevelProfileReport'),self.txt.report(levelVarId),perfilId);
            r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            
            %%%% Put content in mainPanel            
            eta=self.offshoreData('surge').getData;
            tide=self.offshoreData('tide').getData;
            
            tide=interp1(self.offshoreData('tide').getTime,tide,self.mopla.time);            
            eta=interp1(self.offshoreData('surge').getTime,eta,self.mopla.time);  
       
            %slope=post.getProfileSlope(self.mopla,tide+eta,perfilId);
            profile=self.mopla.perfiles(perfilId);
            slope=profile.slope;
            
            tteFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[perfilId,'_tte.mat']);
            tte=load(tteFile,'-mat');
            %tte=tte.perfilTTE;
            reference=0;
            hs=tte.hs_b;
            tp=self.mopla.tp;

            if slope>=0.1
               runUp=1.98*0.47*sqrt(hs.*(1.56*tp.^2))*slope;
            else
               runUp=1.98*0.04*sqrt(hs.*(1.56*tp.^2));
            end

            ci=reference+tide+eta+runUp;   
            if strcmp(levelVarId,'ci')
            var=ameva.AmevaVar('ci','m',ci,self.mopla.time);
            else
            var=ameva.AmevaVar('runUp','m',runUp,self.mopla.time);
            end
            
            % Graphs position and size
            x1 = 0.1;
            x2 = 0.58;
            y1 = 0.14;
            y2 = 0.61;
            graphSizeY = 0.34;
            graphSizeX = 0.36;
            
            r.addTimeSeries([x1, y2, graphSizeX, graphSizeY], var); % Series            
            r.addHist([x2, y2, graphSizeX, graphSizeY], var); % Histogram                        
            r.addRmGevScalar([x1, y1, graphSizeX, graphSizeY], var); % GEV            
            r.addGevChart([x2, y1, graphSizeX, graphSizeY],var); % Gev Extremos
            
             strEq=sprintf('$$%s%s%s$$','\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}',...
               report.Report.latexEquation('gev'),'\end{array}');            
            r.writeText([0.1 0 graphSizeX 0.1], strEq);
            strEq=sprintf('$$%s%s%s$$','\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}',...
               report.Report.latexEquation('gevInvRtSimple'),'\end{array}');            
            r.writeText([0.58 0 graphSizeX 0.1], strEq);   
            
            %%%% Set the Mini-Map            
            hold(r.mapAxes,'on');
               plot(r.mapAxes,profile.posicion(:,1),profile.posicion(:,2),'y','LineWidth',1.25);   

            %%%% Print and Delete
            outFile=fullfile(self.outputFolder,sprintf('%s_%sCharacterization.png',perfilId,levelVarId));
            r.printPng(outFile);
            delete(r);
        end
            
        function climateChangeCIReport(self,perfilId,currentYear,nYears)
            
            strTitle=sprintf(self.txt.report('rtClimateChangeCiReport'),perfilId);
            r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            
            %%%% Put content in mainPanel            
            eta=self.offshoreData('surge').getData;
            tide=self.offshoreData('tide').getData;            
            tide=interp1(self.offshoreData('tide').getTime,tide,self.mopla.time);            
            eta=interp1(self.offshoreData('surge').getTime,eta,self.mopla.time);  
            
            %slope=post.getProfileSlope(self.mopla,tide+eta,perfilId);
            profile=self.mopla.perfiles(perfilId);
            slope=profile.slope;
            
            tteFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[perfilId,'_tte.mat']);
            tte=load(tteFile,'-mat');
            %tte=tte.perfilTTE;
            reference=0;
            hs=tte.hs_b;
            tp=self.mopla.tp;
            if slope>=0.1
               runUp=1.98*0.47*sqrt(hs.*(1.56*tp.^2))*slope;
            else
               runUp=1.98*0.04*sqrt(hs.*(1.56*tp.^2));
            end
            ci=reference+tide+eta+runUp;   
            var=ameva.AmevaVar('ci','m',ci,self.mopla.time);           
            
            timeOptions.scale='Year';
            timeOptions.type='P995';  %Percentill del 99.5%
            
            [param,~,isSignificative,significative]=r.addHeterocedasticTrendPlot([0.1, 0.65, 0.8, 0.3], var,timeOptions); % Trend
%             var.setData(datenum(2000,1,1),currentCi);
%             r.addTrendPdf([0.3, 0.05, 0.3, 0.3], param,nYears,var); % Pdf
            r.addTrendTestTable([0.5 0.48 0.45 0.08],[],isSignificative(1));  %Comprueba la media
            
%             
            muModel='\mu_{CI}=\mu_{CI}^0 +\delta\mu_{CI}t';
            if ~significative(2)
               muModel='\mu_{CI}=\mu_{CI}^0';                
            end            
            sigmaModel='\sigma_{CI}=\sigma_{CI}^0 +\delta\sigma_{CI}t';
            if ~significative(4)
               sigmaModel='\sigma_{CI}=\sigma_{CI}^0';                
            end     
             r.writeString([0.1 0.52 0.2 0.1],{self.txt.report('model'),'',muModel,...
                 '',sigmaModel},r.mainPanel);
             
             r.writeString([0.3 0.52 0.2 0.1],{self.txt.report('trend'),'',...
                 ['\delta\mu_{CI}=',num2str(param.mu1*1000,'%.3f'),' mm/',self.txt.report('year')],'',...
                 ['\delta\sigma_{CI}=',num2str(param.sigma1*1000,'%.3f'),' mm/',self.txt.report('year')]},r.mainPanel);
             
             
             %Trend Extremo
             [fitParameters,muIni,muFuture]=r.addExtremalTrend([0.1,0.1,0.35,0.3],var,param,isSignificative,currentYear,currentYear+nYears);
             
             %Tabla con los parametros del ajuste             
             data=[fitParameters.lambda,fitParameters.xi,fitParameters.u,fitParameters.sigma,fitParameters.psi,muIni];
             data=repmat(data,2,1);
             data(end,end)=muFuture;
             header={' ','\lambda','\xi','u','\sigma (m)','\psi (m)','\mu (m)'};
             rowNames={num2str(currentYear),num2str(currentYear+nYears)};             
             r.addTable([0.5 0.28 0.45 0.12], data, header, rowNames)
             
             %Ecuaciones
             strEq=sprintf('$$%s%s%s$$','\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}',...
               report.Report.latexEquation('gev'),'\end{array}');            
             %strEq=sprintf('$$%s$$',report.Report.latexEquation('gev'));
             r.writeText([0.5 0.16 0.45 0.08],strEq) 
             strEq=sprintf('$$%s%s%s$$','\renewcommand{\arraystretch}{1.8}\begin{array}{rcl}',...
               report.Report.latexEquation('pareto'),'\end{array}');            
             r.writeText([0.5 0.08 0.45 0.1],strEq) 

            %%%% Put content in infoPanel               
            r.writeString([0.1 0.2 0.5 0.1],{sprintf('%s %s=%d',self.txt.report('year')...
                ,self.txt.report('current'),currentYear),'',...
                sprintf('N=%d %s',nYears,...
                self.txt.report('years'))},...
            r.secondPanel,'FontSize',10);
            
            
            %%%% Set the Mini-Map            
            hold(r.mapAxes,'on');
            plot(r.mapAxes,profile.posicion(:,1),profile.posicion(:,2),'y','LineWidth',1.25);    

            %%%% Print and Delete
            outFile=fullfile(self.outputFolder,sprintf('%sClimateChangeCI.png',perfilId));
            r.printPng(outFile);
            delete(r);
        end
        
        function climateChangeBackward(self,poiId,hs12,d50,berma,yearIni,nYears)
            %d50 mm
            strTitle=sprintf(self.txt.report('rtClimateChangeBackward'),poiId);
            r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            
            %%%% Put content in mainPanel            
            eta=self.offshoreData('surge').getData;
            tide=self.offshoreData('tide').getData;            
            tide=interp1(self.offshoreData('tide').getTime,tide,self.mopla.time);            
            eta=interp1(self.offshoreData('surge').getTime,eta,self.mopla.time);  
            
            level=tide+eta;   
            var=ameva.AmevaVar('level','m',level,self.mopla.time);           
            
            timeOptions.scale='Year';
            timeOptions.type='Mean';
            
            %Grafica de tendencia
            [param,isNormal,isSignificative,significative]=r.addHeterocedasticTrendPlot([0.1, 0.65, 0.8, 0.3], var,timeOptions); % Trend
            %tabla de significancia
            r.addTrendTestTable([0.5 0.48 0.45 0.08],isNormal,isSignificative(1) | isSignificative(2));  %Comprueba la media
            %Resultado del modelo
            [muIni,sigmaIni]=ameva.calculeTrendValues(param,level,significative,yearIni);
            [muEnd,sigmaEnd]=ameva.calculeTrendValues(param,level,significative,yearIni+nYears);
            deltaMu=muEnd-muIni; % Mu, SLR
            %deltaSigma=sigmaEnd-sigmaIni; % Sigma, SLR
            deltaSigma=sqrt(sigmaEnd^2+sigmaIni^2); % Sigma, SLR
            [back,backPdf,muBack,sigmaBack] = post.bruun(hs12,deltaMu,deltaSigma,berma,d50/1000);
            
            muModel='\mu_{\eta}=\mu_{\eta}^0 +\delta\mu_{\eta}t';
            if ~significative(2)
               muModel='\mu_{\eta}=\mu_{\eta}^0';                
            end            
            sigmaModel='\sigma_{\eta}=\sigma_{\eta}^0 +\delta\sigma_{\eta}t';
            if ~significative(4)
               sigmaModel='\sigma_{\eta}=\sigma_{\eta}^0';                
            end     
             r.writeString([0.1 0.52 0.2 0.1],{self.txt.report('model'),'',muModel,...
                 '',sigmaModel},r.mainPanel);
             
             r.writeString([0.3 0.52 0.2 0.1],{self.txt.report('trend'),'',...
                 ['\delta\mu_{\eta}=',num2str(param.mu1*1000,'%.3f'),' mm/',self.txt.report('year')],'',...
                 ['\delta\sigma_{\eta}=',num2str(param.sigma1*1000,'%.3f'),' mm/',self.txt.report('year')]},r.mainPanel);
            
            r.writeString([0.5 0.435 0.5 0.05],sprintf(self.txt.report('tTrendPdf'),nYears),r.mainPanel,'FontWeight','b','HorizontalAlignment','center');
            r.addTrendPdf([0.1,0.08,0.32,0.28],deltaMu,deltaSigma,'\Delta\eta','m');
            r.addBackTrendPdf([0.5,0.08,0.32,0.28],back,backPdf,'bruun')
            l1={['\mu:' num2str(muBack,'% 10.3f') ' m']};
            l2={['\sigma:' num2str(sigmaBack,'% 10.3f') ' m']};
            ltot=[l1;l2];
            r.addAnnotation([0.85*0.75,0.31,0.08,0.04],ltot);
            
            %%%% Put content in infoPanel                           
            r.putImage([0.05 0.5 0.9 0.4],'beach_profile.png',r.secondPanel);
            r.writeString([0.1 0.25 0.5 0.4],{sprintf('%s %s=%d',self.txt.report('year')...
                ,self.txt.report('current'),yearIni),'',...
                sprintf('N=%d %s',nYears,self.txt.report('years')),'',...
                sprintf('D_{50}=%.2f mm',d50),'',...
                sprintf('B=%.2f m',berma)...                
                },r.secondPanel,'FontSize',10);
            
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');             
            x=self.mopla.pois(poiId).x;
            y=self.mopla.pois(poiId).y;
            plot(r.mapAxes,x,y,'.y','MarkerSize',12);                 
                
            %%%% Print and Delete   
            outFile=fullfile(self.outputFolder,sprintf('%sClimateChangeBackward.png',poiId));
            r.printPng(outFile);
            delete(r);
        end
        
        function climateChangeRotation(self,poiId,l,yearIni,nYears)
            %l metros
            strTitle=sprintf(self.txt.report('rtClimateChangeRotation'),poiId);
            r=report.Report(self.caseInfo,self.batDataRoiZoom,strTitle);
            
            %%%% Put content in mainPanel            
            [~,~,efX,efY]=self.calculaFem(poiId);            
            var=ameva.AmevaVar('femDir','�',complex(efX,efY),self.mopla.time);      
            
            timeOptions.scale='Year';
            timeOptions.type='Mean';
            
            %Grafica de tendencia
            [param,isNormal,isSignificative,significative]=r.addHeterocedasticTrendPlot([0.1, 0.65, 0.8, 0.3], var,timeOptions); % Trend
            %tabla de significancia
            r.addTrendTestTable([0.5 0.48 0.45 0.08],isNormal,isSignificative(1) | isSignificative(2));  %Comprueba la media
            
            %Resultado del modelo
            [muIni,sigmaIni]=ameva.calculeTrendValues(param,complex(efX,efY),significative,yearIni);
            [muEnd,sigmaEnd]=ameva.calculeTrendValues(param,complex(efX,efY),significative,yearIni+nYears);
            deltaMu=atan2(sind(muEnd-muIni),cosd(muEnd-muIni))*180/pi;
            %if deltaMu<0,deltaMu=deltaMu+360;end            
            %deltaMu=atan2(sind(muEnd)-sind(muIni),cosd(muEnd)-cosd(muIni))*180/pi;            
            %deltaSigma=atan2(sind(sigmaEnd)-sind(sigmaIni),cosd(sigmaEnd)-cosd(sigmaIni))*180/pi;    
            deltaSigma=sqrt(sigmaIni.^2+sigmaEnd.^2);
            
            [back,backPdf,muBack,sigmaBack] = post.beachRotation(abs(deltaMu),deltaSigma,l);            
            
            muModel='\mu_{\theta_{FME}}=\mu_{\theta_{FME}}^0 +\delta\mu_{\theta_{FME}}t';
            if ~significative(2)
               muModel='\mu_{\theta_{FME}}=\mu_{\theta_{FME}}^0';                
            end            
            sigmaModel='\sigma_{\theta_{FME}}=\sigma_{\theta_{FME}}^0 +\delta\sigma_{\theta_{FME}}t';
            if ~significative(4)
               sigmaModel='\sigma_{\theta_{FME}}=\sigma_{\theta_{FME}}^0';                
            end     
             r.writeString([0.1 0.52 0.2 0.1],{self.txt.report('model'),'',muModel,...
                 '',sigmaModel},r.mainPanel);
             
             r.writeString([0.3 0.52 0.2 0.1],{self.txt.report('trend'),'',...
                 ['\delta\mu_{\theta_{FME}}=',num2str(param.mu1,'%.3f'),' (�)/',self.txt.report('year')],'',...
                 ['\delta\sigma_{\theta_{FME}}=',num2str(param.sigma1,'%.3f'),' (�)/',self.txt.report('year')]},r.mainPanel);
             
            r.writeString([0.5 0.435 0.5 0.05],sprintf(self.txt.report('tTrendPdf'),nYears),r.mainPanel,'FontWeight','b','HorizontalAlignment','center');
            
            r.addTrendPdf([0.1,0.08,0.32,0.28],deltaMu,deltaSigma,'\Delta\beta','�');
            r.addBackTrendPdf([0.5,0.08,0.32,0.28],back,backPdf,'rot')
            
            if isempty(muBack) || isempty(sigmaBack)
                muStr='-';
                sigmaStr='-';
            else
                muStr=[num2str(muBack,'% 10.3f'),' m'];
                sigmaStr=[num2str(sigmaBack,'% 10.3f'),' m'];
            end
            l1={['\mu: ',muStr ]};
            l2={['\sigma: ',sigmaStr]};
            ltot=[l1;l2];
            r.addAnnotation([0.85*0.75,0.31,0.08,0.04],ltot);
            
            
            %%%% Put content in infoPanel                           
            r.putImage([0 0.5 1 0.4],'beach_planform.png',r.secondPanel);
            r.writeString([0.1 0.25 0.5 0.4],{sprintf('%s %s=%d',self.txt.report('year')...
                ,self.txt.report('current'),yearIni),'',...
                sprintf('N=%d %s',nYears,self.txt.report('years')),'',...                
                sprintf('L=%.2f m',l)...                
                },r.secondPanel,'FontSize',10);
            
            %%%% Set the Mini-Map
            hold(r.mapAxes,'on');             
            x=self.mopla.pois(poiId).x;
            y=self.mopla.pois(poiId).y;
            plot(r.mapAxes,x,y,'.y','MarkerSize',12);                 
                
            %%%% Print and Delete   
            outFile=fullfile(self.outputFolder,sprintf('%sClimateChangeRotation.png',poiId));
            r.printPng(outFile);
            delete(r);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Private methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods         
        function folder=outputFolder(self)
            folder=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,'reports');
            if ~exist(folder,'dir')
                mkdir(folder);
            end
        end                
        
        function pois=getRebuildedPois(self)
         pois={};
         poisKeys=keys(self.mopla.pois);
            for i=1:length(poisKeys)                       
               poiFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[poisKeys{i},'.poi']);
               if exist(poiFile,'file')
                   pois=cat(1,pois,poisKeys{i});
               end
            end
        end        
        
        function profiles=getRebuildedProfiles(self)
            profiles={};
            profilesKeys=keys(self.mopla.perfiles);
            for i=1:length(profilesKeys)
                profileFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[profilesKeys{i},'_tte.mat']);
                if exist(profileFile,'file')
                   profiles=cat(1,profiles,profilesKeys{i});
               end
            end
        end
        
        function batData=getMeshesZoom(self)
             mallas=self.mopla.mallas(logical(self.mopla.propagate));
             xIni=realmax;
             xEnd=realmin;
             yIni=realmax;
             yEnd=realmin;
             
             for i=1:length(mallas)
                 malla=self.alternativa.mallas(mallas{i});
                 x_=[0,malla.x,malla.x,0];
                 y_=[0,0,malla.y,malla.y];
                 x=x_*cosd(malla.angle)-y_*sind(malla.angle);
                 y=x_*sind(malla.angle)+y_*cosd(malla.angle);
                 x=x+malla.xIni;
                 y=y+malla.yIni;
                 
                 if min(x)<xIni,xIni=min(x);end
                 if min(y)<yIni,yIni=min(y);end
                 if max(y)>yEnd, yEnd=max(y);end
                 if max(x)>xEnd, xEnd=max(x);end                 
                 if ~isempty(malla.parent)                     
                     malla=self.alternativa.mallas(malla.parent);
                     x_=[0,malla.x,malla.x,0];
                     y_=[0,0,malla.y,malla.y];
                     x=x_*cosd(malla.angle)-y_*sind(malla.angle);
                     y=x_*sind(malla.angle)+y_*cosd(malla.angle);
                     x=x+malla.xIni;
                     y=y+malla.yIni;
                     if min(x)<xIni,xIni=min(x);end
                     if min(y)<yIni,yIni=min(y);end
                     if max(y)>yEnd, yEnd=max(y);end
                     if max(x)>xEnd, xEnd=max(x);end                 
                 end
             end
             
             dx=xEnd-xIni;
             dy=yEnd-yIni;
             
             xIni=xIni-dx/2;
             xEnd=xEnd+dx/2;
             yIni=yIni-dy/2;
             yEnd=yEnd+dy/2;
             
             if xIni<min(self.alternativa.xyz(:,1)), xIni=min(self.alternativa.xyz(:,1));end
             if yIni<min(self.alternativa.xyz(:,2)), yIni=min(self.alternativa.xyz(:,2));end
             if xEnd>max(self.alternativa.xyz(:,1)), xEnd=max(self.alternativa.xyz(:,1));end
             if yEnd>max(self.alternativa.xyz(:,2)), yEnd=max(self.alternativa.xyz(:,2));end
             
            x=linspace(xIni,xEnd,100);
            y=linspace(yIni,yEnd,100);             
            [X,Y]=meshgrid(x,y);            
            Z=self.alternativa.F(X,Y);
            batData.X=X; batData.Y=Y; batData.Z=Z;             
            batData.costas=util.costasInPoly(self.alternativa.costas,[xIni,xEnd,xEnd,xIni],[yIni,yEnd,yIni,yEnd]);
        end 
        
        function batData=getRoiZoom(self)
            %%%&Convex hull de las mallas
            mallasTmp=containers.Map;            
            mallas=[];
             for i=1:length(self.mopla.mallas)
                 if self.mopla.propagate(i)
                 if ~isKey(mallasTmp,self.mopla.mallas{i})
                     mallasTmp(self.mopla.mallas{i})='';
                     malla=self.alternativa.mallas(self.mopla.mallas{i});
                     mallas=cat(1,mallas,malla);
                 end
                 end
             end
            delete(mallasTmp);
            poly = post.getConvexMallas(mallas);            
            
             xIni=min(poly(:,1)); xEnd=max(poly(:,1));yIni=min(poly(:,2)); yEnd=max(poly(:,2));             
             
             dx=xEnd-xIni;
             dy=yEnd-yIni;
             
             xIni=xIni-dx/3;
             xEnd=xEnd+dx/3;
             yIni=yIni-dy/3;
             yEnd=yEnd+dy/3;
             
             if xIni<min(self.alternativa.xyz(:,1)), xIni=min(self.alternativa.xyz(:,1));end
             if yIni<min(self.alternativa.xyz(:,2)), yIni=min(self.alternativa.xyz(:,2));end
             if xEnd>max(self.alternativa.xyz(:,1)), xEnd=max(self.alternativa.xyz(:,1));end
             if yEnd>max(self.alternativa.xyz(:,2)), yEnd=max(self.alternativa.xyz(:,2));end
             
            x=linspace(xIni,xEnd,100);
            y=linspace(yIni,yEnd,100);             
            [X,Y]=meshgrid(x,y);            
            Z=self.alternativa.F(X,Y);
            batData.X=X; batData.Y=Y; batData.Z=Z;             
            batData.costas=util.costasInPoly(self.alternativa.costas,[xIni,xEnd,xEnd,xIni],[yIni,yEnd,yIni,yEnd]);
        end
        
        function [ef,efDir,efX,efY]=calculaFem(self,poiId,year)   
            poi=self.mopla.pois(poiId);
            poiFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[poiId,'.poi']);
            poiData=load(poiFile,'-mat');
            
            tp=self.mopla.tp;
            hs=poiData.outData.hs;
            dir=poiData.outData.dir;
            moplaTime=self.mopla.time;
            nHours=round((moplaTime(end)-moplaTime(1)+1)*24);
           
            if exist('year','var')
                timeVec=datevec(self.mopla.time);
                index=timeVec(:,1)==year;
                tp=tp(index);
                hs=hs(index);
                dir=dir(index);
                moplaTime=moplaTime(index);
                nHours=365*24;
            end
           
            
            nivelMarea=self.mopla.tide(1); %%Media marea
            if length(self.mopla.tide)==2 %%%Bajamar + Pleamar
                nivelMarea=(self.mopla.tide(2)+self.mopla.tide(1))/2;
            elseif length(self.mopla.tide)==3
                nivelMarea=self.mopla.tide(2);
            end
            time=self.offshoreData('tide').getTime;
            nivel=self.offshoreData('tide').getData;
            nivel=interp1(time,nivel,moplaTime);
            nivel=nivel+nivelMarea;
            
            g=9.81;
            rho=1025;
            h_nivel=poi.z+nivel;
            h_nivel(h_nivel<0)=0;
            [k,~]=util.dispersion_hunt(h_nivel,tp);
            w=2*pi./tp;
            %cg=sqrt(g* h_nivel);         %%%Celeridad de grupo en shallow water
            kh=k.*h_nivel;
            cg=g*(tanh(kh)+kh./(cosh(kh).^2))./(2*w);

            ef=rho*g*cg.*(hs).^2/8;
            efX=ef.*cosd(dir);
            efY=ef.*sind(dir);
            ef_x=sum(ef.*cosd(dir))/nHours;
            ef_y=sum(ef.*sind(dir))/nHours;
            ef=sqrt(ef_x^2+ef_y^2);
            efDir=atan2(ef_y,ef_x)*180/pi;
            efDir(efDir<0)=efDir(efDir<0)+360;        
        end
        
        function femLength=calcularFemEscala(self)
            femLength=1000; %Longitud de la flecha (m)            
            perfiles=keys(self.mopla.perfiles);          
            l=zeros(length(perfiles),1);
            for i=1:length(perfiles)   
               perfilId=perfiles{i};
               p=self.mopla.perfiles(perfilId);
               l(i)=p.getLength;                 
            end            
            l=max(l)/2;                  
            if l>0
                femLength=l;
            end
        end
        
        function qScale=calculaQEscala(self,maxQ)
            perfiles=keys(self.mopla.perfiles);          
            l=zeros(length(perfiles),1);
            for i=1:length(perfiles)   
               perfilId=perfiles{i};
               p=self.mopla.perfiles(perfilId);
               l(i)=p.getLength;                 
            end            
            l=max(l)/2;                             
            qScale=l/maxQ;
            
        end
        
        function [qP,qM]=calculaTransporte(self,perfilId,season)
			tteFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[perfilId,'_tte.mat']);
            tte=load(tteFile,'-mat','hs_b','h_b','dir_b','l_b','uAvg');
			profile=self.mopla.perfiles(perfilId);
             if isfield(tte,'uAvg')
                  uAvg=tte.uAvg;
                else
                  uAvg=[];
             end
            tte.q=post.longshoreTransport(tte.hs_b,tte.h_b,tte.dir_b,self.mopla.tp,tte.l_b,uAvg,...
                profile,self.mopla.transportModel,self.mopla.kModel);
            %tteFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',self.mopla.moplaId,[perfilId,'_tte.mat']);
            %tte=load(tteFile,'-mat');
            %tte=tte.perfilTTE;
            date=datevec(self.mopla.time);
            mIni=1;
            mEnd=12;
            
            %quito el primer a�o ya que en la serie temporal Enero del 48 no esta           
            index=date(:,1)==1948;
            date(index,:)=[];
            tte.q(index)=[];
            
            nYears=date(end,1)-date(1,1)+1;
            switch season
                case 'Winter'
                    mIni=12;mEnd=2;
                case 'Spring'
                    mIni=3;mEnd=5;
                case 'Summer'
                    mIni=6;mEnd=8;
                case 'Autumn'
                    mIni=9;mEnd=11;
            end
            
            
            if mIni<mEnd
                 index=date(:,2)>=mIni & date(:,2)<=mEnd;                    
            else                    
                 index= (date(:,2)>=mIni | date(:,2)<=mEnd);                                        
            end
            
            qSeason=tte.q(index);
            qP=sum(qSeason(qSeason>=0))*3600/nYears;
            qM=sum(qSeason(qSeason<0))*3600/nYears;
            
            
        end
       
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% End private methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end


function  reportForm(project,altName,moplaId)
    
           
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%  Defaul Text Box data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    textBoxData.tbYearActual='2008';
    textBoxData.tbNYears='100';
    textBoxData.tbD50='3';
    textBoxData.tbBerma='1';
    textBoxData.tbHs12='1';
    textBoxData.tbLength='100';
    %%%% End Defaul Text Box data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %txt.report=util.loadText('report');  
    txt=util.loadLanguage;
    
    rc=report.ReportCreator(project,moplaId,altName);
    scrPos=get(0,'ScreenSize');
    width=800;
    height=600;
    f = figure( 'MenuBar', 'none', 'Name',txt.report('rfTitle'),'Visible','off',...
        'NumberTitle', 'off', 'Toolbar', 'none','Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],...
        'tag',['smcWinReport_',altName,moplaId]);
    set(f, 'resize', 'off');
    pos=get(f,'Position');
    
    bOk=uicontrol(f,'Style','pushbutton','String', txt.report('rfOk'),'Position',[30 10 60 20],'Callback',@generate);    
    bCancel=uicontrol(f,'Style','pushbutton','String',txt.report('rfCancel'), 'Position',[120 10 60 20],'Callback',@cancel);  
    
    tabControl=uitoolbar(f);  %%Toolbar para el manejo de las pestañas
    tabs=[];
    tabPanels=[];
    xLeft=20;
    xLeftTab=35;
    xRight=400;
    xRightTab=415;
    yPos=500:-25:50;
    width=350;
    height=25;
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Informes generales y DOW
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tabs(1)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.report('rfGeneralReportsTitle'));
    util.setControlIcon(tabs(1),'dow.gif');
    util.toolBarSeparator(tabControl);
    
    tabPanels(1)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');    
    
    
    sections(1)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfStudyZone'),'Position',[xLeft yPos(1) width height],'tag','rc.zoneReport()');
    sections(2)=uicontrol(tabPanels(1),'Style','text','String',txt.report('rfWaveSection'),'Position',[xLeft yPos(3) width height]);    
    subSections(1)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfWaveCharacterization'),'Position',[xLeftTab yPos(4) width height],'tag','rc.waveReport()');
    subSections(2)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfWaveCharacterization2D'),'Position',[xLeftTab yPos(5) width height],'tag','rc.wave2DReport()');
    subSections(3)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfWaveSeasonRose'),'Position',[xLeftTab yPos(6) width height],'tag','rc.waveSeasonRoseReport()');
    subSections(4)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfRmHs'),'Position',[xLeftTab yPos(7) width height],'tag','rc.waveRmReport(''hs'')');
    subSections(5)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfRmTp'),'Position',[xLeftTab yPos(8) width height],'tag','rc.waveRmReport(''tp'')');
    subSections(6)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfWaveExtreme'),'Position',[xLeftTab yPos(9) width height],'tag','rc.waveExtremeReport()');
    subSections(7)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfWaveCharacterizationSOM'),'Position',[xLeftTab yPos(10) width height],'tag','rc.waveSomReport()');
    
    sections(3)=uicontrol(tabPanels(1),'Style','text','String',txt.report('rfLevelSection'),'Position',[xRight yPos(1) width height]);    
    subSections(8)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfLevelTide'),'Position',[xRightTab yPos(2) width height],'tag','rc.levelReport(''tide'');');
    subSections(9)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfLevelSurge'),'Position',[xRightTab yPos(3) width height],'tag','rc.levelReport(''surge'');');
    subSections(10)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfLevelLevel'),'Position',[xRightTab yPos(4) width height],'tag','rc.levelReport(''level'');');
    
    sections(4)=uicontrol(tabPanels(1),'Style','text','String',txt.report('rfClassifitionSection'),'Position',[xRight yPos(6) width height]);
    subSections(11)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfClassification'),'Position',[xRightTab yPos(7) width height],'tag','rc.classificationReport();');
    subSections(12)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfSelectedCases'),'Position',[xRightTab yPos(8) width height],'tag','rc.selectedCasesReport();');
    subSections(13)=uicontrol(tabPanels(1),'Style','checkbox','String',txt.report('rfMeshes'),'Position',[xRightTab yPos(9) width height],'tag','rc.meshesReport();');
    
    %%%%% End Informes generales y DOW
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Informes de pois
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pois=rc.getRebuildedPois;
    enabled='on';
    if isempty(pois)
        enabled='off';
    end
    tabs(2)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.report('rfGeneralReportsPoisTitle'));
    util.setControlIcon(tabs(2),'add_point.gif');
    util.toolBarSeparator(tabControl);   
    tabPanels(2)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');     
    
    sections(5)=uicontrol(tabPanels(2),'Style','text','String',txt.report('rfWaveSection'),'Position',[xLeft yPos(1) width height]);    
    subSections(14)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfWaveCharacterization'),'Position',[xLeftTab yPos(2) width height],'tag','rc.waveReport(''%poi'')');
    subSections(15)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfWaveCharacterization2D'),'Position',[xLeftTab yPos(3) width height],'tag','rc.wave2DReport(''%poi'')');
    subSections(16)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfWaveSeasonRose'),'Position',[xLeftTab yPos(4) width height],'tag','rc.waveSeasonRoseReport(''%poi'')');
    subSections(17)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfRmHs'),'Position',[xLeftTab yPos(5) width height],'tag','rc.waveRmReport(''hs'',''%poi'')');
    subSections(18)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfRmTp'),'Position',[xLeftTab yPos(6) width height],'tag','rc.waveRmReport(''tp'',''%poi'')');
    subSections(19)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfWaveExtreme'),'Position',[xLeftTab yPos(7) width height],'tag','rc.waveExtremeReport(''%poi'')');    
    
    sections(6)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfPoisSection'),'Position',[xRight yPos(1) width height],'tag','rc.poisReport;');
    sections(7)=uicontrol(tabPanels(2),'Style','text','String',txt.report('rfFemSection'),'Position',[xRight yPos(3) width height]);
    subSections(20)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'Enable',enabled,'String',txt.report('rfFem'),'Position',[xRightTab yPos(4) width height],'tag','rc.femReport;');
    subSections(21)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfFemPoi'),'Position',[xRightTab yPos(5) width height],'tag','rc.femPoiReport(''%poi'')');
    
    sections(8)=uicontrol(tabPanels(2),'Style','text','String',txt.report('rfClimateChangeSection'),'Position',[xRight yPos(7) width height]);
    subSections(22)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfClimateChangeBackward'),'Position',[xRightTab yPos(9) width height],'tag','rc.climateChangeBackward(''%poi'',-tbHs12-,-tbD50-,-tbBerma-,-tbYearActual-,-tbNYears-)');
    subSections(23)=uicontrol(tabPanels(2),'Style','checkbox','Enable',enabled,'String',txt.report('rfClimateChangeRotation'),'Position',[xRightTab yPos(11) width height],'tag','rc.climateChangeRotation(''%poi'',-tbLength-,-tbYearActual-,-tbNYears-)');
    
    uicontrol(tabPanels(2),'Style','text','String',txt.report('rfClimateChangeYearIni'),'Position',[xRightTab yPos(8)+5 70 15]);
    uicontrol(tabPanels(2),'Style','edit','Enable',enabled,'String',textBoxData.tbYearActual,'Tag','tbYearActual',...
        'Position',[xRightTab+75 yPos(8)+5 45 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,true,2008,3000});
    uicontrol(tabPanels(2),'Style','text','String',txt.report('rfClimateChangeYears'),'Position',[xRightTab+width/2 yPos(8)+5 45 15]);
    uicontrol(tabPanels(2),'Style','edit','Enable',enabled,'String',textBoxData.tbNYears,'Tag','tbNYears'...
        ,'Position',[xRightTab+width/2+50 yPos(8)+5 45 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,true,1,1000});
    
    uicontrol(tabPanels(2),'Style','text','String','B (m)','Position',[xRightTab yPos(10)+5 40 15]);
    uicontrol(tabPanels(2),'Style','edit','Enable',enabled,'String',textBoxData.tbBerma,'Tag','tbBerma',...
        'Position',[xRightTab+45 yPos(10)+5 40 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,0,20});
    uicontrol(tabPanels(2),'Style','text','String','d50 (mm)','Position',[xRightTab+width/3 yPos(10)+5 45 15]);
    
    uicontrol(tabPanels(2),'Style','edit','Enable',enabled,'String',textBoxData.tbD50,'Tag','tbD50'...
        ,'Position',[xRightTab+width/3+50 yPos(10)+5 30 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,0.1,10});
    uicontrol(tabPanels(2),'Style','text','String','Hs12 (m)','Position',[xRightTab+width*2/3 yPos(10)+5 45 15]);
    uicontrol(tabPanels(2),'Style','edit','Enable',enabled,'String',textBoxData.tbHs12,'Tag','tbHs12'...
        ,'Position',[xRightTab+width*2/3+50 yPos(10)+5 30 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,0.01,20});
    
    uicontrol(tabPanels(2),'Style','text','String','L (m)','Position',[xRightTab+width*2/3 yPos(11)+5 40 15]);
    uicontrol(tabPanels(2),'Style','edit','Enable',enabled,'String',textBoxData.tbLength,'Tag','tbLength'...
        ,'Position',[xRightTab+width*2/3+45 yPos(11)+5 45 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,false,1,5000});
    
    
    poiChecks=[];
    ax=addMap([xRight,25,350,200],rc.batDataRoiZoom,tabPanels(2));
    hold(ax,'on');
    %pois=keys(rc.mopla.pois);
    for i=1:length(pois)  
        poi=rc.mopla.pois(pois{i});
        plot(ax,poi.x,poi.y,'or','MarkerFaceColor','r','MarkerSize',11);                              
    end    
       
    
    sections(9)=uicontrol(tabPanels(2),'Style','text','String',txt.report('rfPois'),'Position',[xLeft yPos(12) width height]);
    
    for i=1:length(pois)
        poi=rc.mopla.pois(pois{i});
        plot(ax,poi.x,poi.y,'ok','MarkerFaceColor','k','MarkerSize',11);                              
        text(poi.x,poi.y,num2str(i),'Color','w','HorizontalAlignment','center','Parent',ax,'FontWeight','b');        
        if i>7
            poiChecks(i)=uicontrol(tabPanels(2),'Style','checkbox','String',poi.nombre,'Position',[xLeftTab + 120 yPos(12+i-7) 100 height]);
        elseif i>14
            poiChecks(i)=uicontrol(tabPanels(2),'Style','checkbox','String',poi.nombre,'Position',[xLeftTab + 240 yPos(12+i-14) 100 height]);
        else
            poiChecks(i)=uicontrol(tabPanels(2),'Style','checkbox','String',poi.nombre,'Position',[xLeftTab yPos(12+i) 100 height]);
        end
    end
    hold(ax,'off'); 
    %%%%% End Informes de Pois
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Informes de profiles
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    profiles=rc.getRebuildedProfiles;
    enabled='on';
    if isempty(profiles)
        enabled='off';
    end
    tabs(3)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.report('rfGeneralReportsProfileTitle'));
    util.setControlIcon(tabs(3),'open_polygon.gif');    
    tabPanels(3)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');
    
    sections(10)=uicontrol(tabPanels(3),'Style','text','String',txt.report('rfTransportSection'),'Position',[xLeft yPos(1) width height]);
    subSections(24)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfTransportAnnual'),'Position',[xLeftTab yPos(2) width height],'tag','rc.transportReport;');
    subSections(25)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfTransportSpring'),'Position',[xLeftTab yPos(3) width height],'tag','rc.transportReport(''Spring'');');
    subSections(26)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfTransportSummer'),'Position',[xLeftTab yPos(4) width height],'tag','rc.transportReport(''Summer'');');
    subSections(27)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfTransportAutumn'),'Position',[xLeftTab yPos(5) width height],'tag','rc.transportReport(''Autumn'');');
    subSections(28)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfTransportWinter'),'Position',[xLeftTab yPos(6) width height],'tag','rc.transportReport(''Winter'');');
    subSections(29)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfTransportProfile'),'Position',[xLeftTab yPos(7) width height],'tag','rc.transportProfileReport(''%profile'');');
    
    sections(11)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfProfiles'),'Position',[xRight yPos(1) width height],'tag','rc.profilesReport;');
   
    sections(12)=uicontrol(tabPanels(3),'Style','text','String',txt.report('rfProfileLevelSection'),'Position',[xRight yPos(3) width height]);
    subSections(30)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfProfileRunUp'),'Position',[xRightTab yPos(4) width height],'tag','rc.levelProfileReport(''%profile'',''runUp'');');
    subSections(31)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfProfileCI'),'Position',[xRightTab yPos(5) width height],'tag','rc.levelProfileReport(''%profile'',''ci'');');
    
    sections(13)=uicontrol(tabPanels(3),'Style','text','String',txt.report('rfClimateChangeSection'),'Position',[xRight yPos(7) width height]);
    subSections(32)=uicontrol(tabPanels(3),'Style','checkbox','Enable',enabled,'String',txt.report('rfClimateChangeCi'),'Position',[xRightTab yPos(9) width height],'tag','rc.climateChangeCIReport(''%profile'',-tbYearActual-,-tbNYears-);');
    uicontrol(tabPanels(3),'Style','text','String',txt.report('rfClimateChangeYearIni'),'Position',[xRightTab yPos(8)+5 70 15]);
    uicontrol(tabPanels(3),'Style','edit','Enable',enabled,'String',textBoxData.tbYearActual,'Tag','tbYearActual',...
        'Position',[xRightTab+75 yPos(8)+5 45 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,true,2008,3000});
    uicontrol(tabPanels(3),'Style','text','String',txt.report('rfClimateChangeYears'),'Position',[xRightTab+width/2 yPos(8)+5 45 15]);
    uicontrol(tabPanels(3),'Style','edit','Enable',enabled,'String',textBoxData.tbNYears,'Tag','tbNYears'...
        ,'Position',[xRightTab+width/2+50 yPos(8)+5 45 15],'BackgroundColor','w','Callback',...
        {@checkTextBox,true,1,1000});
    
    profileChecks=[];
    ax=addMap([xRight,25,350,200],rc.batDataRoiZoom,tabPanels(3));
    hold(ax,'on');
    %profiles=keys(rc.mopla.perfiles);
    for i=1:length(profiles)
        profile=rc.mopla.perfiles(profiles{i});
        plot(ax,profile.posicion(:,1),profile.posicion(:,2),'r');
    end  
    
    sections(14)=uicontrol(tabPanels(3),'Style','text','String',txt.report('rfProfiles'),'Position',[xLeft yPos(12) width height]);
    
    for i=1:length(profiles)
        profile=rc.mopla.perfiles(profiles{i});
        plot(ax,profile.posicion(:,1),profile.posicion(:,2),'k');
         textPosition=[profile.posicion(1,1),profile.posicion(1,2)];                    
         text(textPosition(1),textPosition(2),num2str(i),'Color','w','HorizontalAlignment','center','BackgroundColor','k','Parent',ax);         
        if i>7
            profileChecks(i)=uicontrol(tabPanels(3),'Style','checkbox','String',profile.nombre,'Position',[xLeftTab + 120 yPos(12+i-7) 100 height]);
        elseif i>14
            profileChecks(i)=uicontrol(tabPanels(3),'Style','checkbox','String',profile.nombre,'Position',[xLeftTab + 240 yPos(12+i-14) 100 height]);
        else
            profileChecks(i)=uicontrol(tabPanels(3),'Style','checkbox','String',profile.nombre,'Position',[xLeftTab yPos(12+i) 100 height]);
        end
    end
    hold(ax,'off');    
    %%%%% End Informes de Perfiles
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    setFormat(sections,'head');
    setFormat(subSections,'normal');
    setFormat(poiChecks,'normal');
    
    set(tabs(1),'State','on');    
    set(f,'Visible','on');    
    
   
    %uiwait(f);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Funciones
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function showTab(varargin)
       util.disableOtherButtons(tabControl,varargin{1});
       tab=(tabs==varargin{1});
       set(tabPanels(tab),'Visible','on');
    end

    function hideTab(varargin)
        tab=(tabs==varargin{1});
       set(tabPanels(tab),'Visible','off');
    end    

    function setFormat(controls,style)
        if strcmp(style,'head')
            for i=1:length(controls)
                set(controls(i),'FontName','Arial','FontSize',14,'FontWeight','bold','HorizontalAlignment', 'left');
            end
        else
            for i=1:length(controls)
                set(controls(i),'FontName','Arial','FontSize',10,'HorizontalAlignment', 'left');                
            end
        end
    end

    function generate(varargin)
        core.blockMainForm;
        checks=[sections,subSections];    
        poisChecked={};        
        for i=1:length(poiChecks)
            if get(poiChecks(i),'Value');
                poisChecked=cat(1,poisChecked,pois{i});
            end
        end
        profilesChecked={};
        for i=1:length(profileChecks)
            if get(profileChecks(i),'Value');
                profilesChecked=cat(1,profilesChecked,profiles{i});
            end
        end
        jobs={};
        for i=1:length(checks)
            tag=get(checks(i),'Tag');
            if ~isempty(tag)
                if get(checks(i),'Value');
                    [startIndex,endIndex]=regexp(tag,'-\w+-');
                    while ~isempty(startIndex)
                        control=tag(startIndex(1)+1:endIndex(1)-1);
                        value=num2str(textBoxData.(control));
                        tag=strrep(tag,tag(startIndex(1):endIndex(1)),value);
                        [startIndex,endIndex]=regexp(tag,'-\w+-');
                    end
                    
                    if strfind(tag,'%poi')
                        for j=1:length(poisChecked)
                            jobs=cat(1,jobs,strrep(tag,'%poi',poisChecked{j}));
                        end
                    elseif strfind(tag,'%profile')
                        for j=1:length(profilesChecked)
                            jobs=cat(1,jobs,strrep(tag,'%profile',profilesChecked{j}));
                        end
                    else
                        jobs=cat(1,jobs,tag);
                    end
                end
            end
        end
        set(bOk,'Enable','off');
        set(bCancel,'Enable','off');
        h=waitbar(0,sprintf('Reports [%.0f]',length(jobs)),'CloseRequestFcn',@cancelExec);
        error={};
        stop=false;
        for i=1:length(jobs)
            if stop,break;end
            try
                eval(jobs{i})
            catch ex
                error=cat(1,error,[jobs{i},' ',ex.message]);
            end
            %waitbar(i/length(jobs),sprintf('Report %.0f/%.0f',i,length(jobs)));
            waitbar(i/length(jobs));
        end
        %if ~exist('h','var')
        delete(h);
        %end
        if ~isempty(error)
            msgbox({txt.report('rfErrorGeneration'),error{:}},'Report Errors','error');
        end
        set(bOk,'Enable','on');
        set(bCancel,'Enable','on');
        core.blockMainForm;
        msgbox(fullfile(project.folder,altName,'Mopla',moplaId,'reports'));
        
        
        function cancelExec(varargin)
            stop=true;
            %delete(h);
        end
    end

    function cancel(varargin)
        clear rc;
        delete(f);
%         close(gcbf);
%         pause(0.3)
%         close(gcbf);
%         pause(0.3)
    end

    function ax=addCoastMap(position,batData,parent)
      
        ax=axes('Parent',parent,'units','pixel','Position',position);            
        hold(ax,'on');
        for i=1:length(batData.costas)
            plot(ax,batData.costas(i).x,batData.costas(i).y,'-','Color',[0.5 0.5 0.5]);
        end
        hold(ax,'off');
        xMin=min(min(batData.X));xMax=max(max(batData.X));
        yMin=min(min(batData.Y));yMax=max(max(batData.Y));
        axis(ax,'image');
        set(ax,'xlim',[xMin xMax],'ylim',[yMin,yMax]);  
        set(ax,'xtick',[]); set(ax,'ytick',[]);
    end

    function ax=addMap(position,batData,parent)
            
            
            ax=axes('Parent',parent,'units','pixel','Position',position);               
            
            levels=util.getDemLevels(batData.Z);
            contourf(ax,batData.X,batData.Y,-batData.Z,-levels,'LineStyle','none');
            util.demcmap(-batData.Z);
%            hold(ax,'on');
%             for i=1:length(batData.costas)
%                 plot(ax,batData.costas(i).x,batData.costas(i).y,'-k','LineWidth',1);
%             end
%            hold(ax,'off');
            xMin=min(min(batData.X));xMax=max(max(batData.X));
            yMin=min(min(batData.Y));yMax=max(max(batData.Y));
            axis(ax,'image');
            set(ax,'xlim',[xMin xMax],'ylim',[yMin,yMax]);  
            set(ax,'xlim',[xMin xMax],'ylim',[yMin,yMax]);  
            set(ax,'xtick',[]); set(ax,'ytick',[]);
            
                        
    end
    
    function checkTextBox(tbControl,obj2,isInteger,minValue,maxValue)
        strValue=get(tbControl,'String');
        textBox=get(tbControl,'Tag');
        
        value=str2double(strValue);
        
        if ~isnan(value) 
            if isInteger,value=round(value);end
            if value<minValue,value=minValue;end
            if value>maxValue,value=maxValue;end
            textBoxData.(textBox)=value;
        else
            value=textBoxData.(textBox);            
        end
        tbControls=findall(tabPanels,'Tag',textBox);
        for j=1:length(tbControls)
            set(tbControls(j),'String',value);
        end
        
    end
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% End Funciones
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end
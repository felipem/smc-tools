classdef AlternativaForm<handle
    
    properties
        toolbar;
        alternativa;
        project;        
        ax;
        %infoPanel;
        rootNode;
        panel;
        tree;
        %oldInfoWidth;
        toolsCount;
        mallasLayer;
        %perfilesLayer;
        costaLayer=[];        
        dowLayer=[];
        dowCacheLayer=[];
        dowSelectedLayers=[];
        cmDowSelected=[];
        cmMopla=[];        
        %cmPerfil=[];
        currentPoint=[];
         profileColor=[255,160,122]/255;
        txt;
        folders;
        db;
    end
    
    events
        ChangeMenu
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Constructor/Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function self=AlternativaForm(db,panel,project,toolbar,tree,rootNode)  
            try
            %%% Cargo el idioma
            self.txt=util.loadLanguage;
            %%%%%%%%%%%%%%%%%%%
            
            self.db=db;
            self.folders=util.getUserFolders;
            
            self.panel=panel;
            self.toolbar=toolbar;
            self.tree=tree;
            self.rootNode=rootNode;
            %self.infoPanel=infoPanel;
            self.project=project;
            
            alternativaName=get(rootNode,'Name');
            self.alternativa=smc.loadAlternativa(project.folder,alternativaName);
            if isempty(self.alternativa)
                watchoff;
                return;
            end
            
            %%%%%Init Axes
            self.ax=axes('Parent',self.panel,'Position',[0.05 0.05 0.9 0.9]);            
            x_=self.alternativa.xyz(:,1);
            y_=self.alternativa.xyz(:,2);
            %z=self.alternativa.xyz(:,3);
            x=linspace(min(x_),max(x_),100);
            y=linspace(min(y_),max(y_),100);
            [X,Y]=meshgrid(x,y);           
            Z=self.alternativa.F(X,Y);               
            
            
            %Z(Z>50)=50;
            levels=util.getDemLevels(Z);
            contourf(self.ax,X,Y,-Z,-levels,'LineStyle','none');colorbar                 
            util.demcmap(-Z)
            axis equal            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %%%%%Interpolo lo coordenada Z de los nodos DOW
            self.project.nodosDow.z=self.alternativa.F(self.project.nodosDow.x,self.project.nodosDow.y);
            
            
            self.initExplorer;           
            self.initLayers;
            self.initContextMenu;
             self.initToolBar;
            
           catch ex               
                delete(self);
                error(ex.message);
            end
            
        end
        
        function delete(self)
%             pos=get(self.panel,'Position');pos(3)=pos(3)-self.oldInfoWidth;
%             set(self.panel,'Position',pos);
%             set(self.infoPanel,'Visible','on');
            
            %Elimino los hijos por si hay algo
            util.cleanNode(self.rootNode,self.tree)
    
            
            jToolbar = get(get(self.toolbar,'JavaContainer'),'ComponentPeer');
            totalTools=jToolbar.getComponentCount();
            for i=totalTools-1:-1:totalTools-self.toolsCount;
                 jToolbar.remove(i);
            end
            
             f=findobj('-regexp','Tag','smcWinAlternativa*');
             for i=1:length(f)
                 close(f);
             end
            jToolbar(1).repaint;
            jToolbar(1).revalidate;
            
            util.resetPanel(self.panel);
            
           
%              for i=1:length(self.mallasLayer)
%                 delete(self.mallasLayer(i));
%              end
%              
%               for i=1:length(self.dowSelectedLayers)
%                 delete(self.dowSelectedLayers(i));
%               end             
% 
%              if ishandle(self.currentPoint),delete(self.currentPoint);end;
%             delete(self.mallasLayer);
%             delete(self.dowLayer);
%             delete(self.dowSelectedLayers);
%             delete(self.txt);
        end
        
        function initExplorer(self)
            util.cleanNode(self.rootNode,self.tree);
            mallasNode=uitreenode('v0','fld_mallas',self.txt.pre('exMeshes'),[],false);
            util.setNodeIcon(mallasNode,'folder_closed.png');
            self.rootNode.add(mallasNode);
            nodes=containers.Map;
            mallas=keys(self.alternativa.mallas);
            for i=1:length(mallas)
                malla=self.alternativa.mallas(mallas{i});
                mallaNode=uitreenode('v0',['chk_malla_',malla.nombre,'_0'],malla.nombre,[],false);
                util.setNodeIcon(mallaNode,'malla.gif');
                nodes(mallas{i})=mallaNode;
            end
            
            for i=1:length(mallas)
                malla=self.alternativa.mallas(mallas{i});
                if ~isempty(malla.parent)      
                    pNode=nodes(malla.parent);
                    pNode.add(nodes(mallas{i}));
                else
                    mallasNode.add(nodes(mallas{i}));
                end
            end            
            delete(nodes);
            
%             perfilesNode=uitreenode('v0','fld_perfiles','Perfiles',[],false);
%             util.setNodeIcon(perfilesNode,'folder_closed.png');
%             self.rootNode.add(perfilesNode);
%             self.refreshPerfilExplorer;
            
            costaNode=uitreenode('v0','chk_costa_1',self.txt.pre('exCoast'),[],true);
            util.setNodeIcon(costaNode,'open_polygon.gif');
            self.rootNode.add(costaNode);
            
            cacheNode=uitreenode('v0','chk_cache_0',self.txt.pre('exCache'),[],true);
            util.setNodeIcon(cacheNode,'cache.gif');
            self.rootNode.add(cacheNode);

            oleajeNode=uitreenode('v0','chk_dow_0',self.txt.pre('exWave'),[],false);
            util.setNodeIcon(oleajeNode,'dow.gif');
            self.rootNode.add(oleajeNode);
            
             dowSelNodes=keys(self.alternativa.dowSelectedNodes);
            for i=1:length(dowSelNodes)                                
                sel=dowSelNodes{i};                
                selNode=uitreenode('v0',['chk_dow_',sel,'_0'],...
                sel,[],true);
                util.setNodeIcon( selNode,'dow.gif');                                
                oleajeNode.add(selNode);
            end
            
            %%%%%%% Cargo los moplas
            moplaNode=uitreenode('v0','moplaSmc','Mopla',[],false);
            util.setNodeIcon(moplaNode,'smc.gif');
            self.rootNode.add(moplaNode);
            self.refreshMoplaExplorer();
            self.tree.reloadNode(self.rootNode);
            util.refreshTree(self.tree);
           
        end
        
        function initToolBar(self)
            self.toolsCount=1;
            ptSaveAlt=uipushtool(self.toolbar,'ClickedCallback',@self.saveAlternativa,'TooltipString',self.txt.pre('tbSave'));                 
            util.setControlIcon(ptSaveAlt,'file_save.gif');
            self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar);
                       
            self.toolsCount=self.toolsCount+core.navigateToolbar(self.toolbar,self.panel);
            self.toolsCount=self.toolsCount+core.measureToolbar(self.toolbar,gca,'utm');            
            self.toolsCount=self.toolsCount+1;util.toolBarSeparator(self.toolbar);
  
            ptSelectPoint=uipushtool(self.toolbar,'ClickedCallback',@self.selectPoint,'TooltipString',self.txt.pre('tbSelectPoint'));            
            util.setControlIcon( ptSelectPoint,'add_point.gif');    
            self.toolsCount=self.toolsCount+1;
            
            ptAddProfile=uipushtool(self.toolbar,'ClickedCallback',@self.createPerfil,'TooltipString',self.txt.pre('tbProfile'));            
            util.setControlIcon( ptAddProfile,'open_polygon.gif');    
            self.toolsCount=self.toolsCount+1;
        end
        
        function initLayers(self)
            hold(self.ax,'on');
            costas=self.alternativa.costas; 
            for i=1:length(costas)
                self.costaLayer(i)=plot(self.ax,costas(i).x,costas(i).y,'-k','LineWidth',2);
            end            
            self.mallasLayer=util.initMap(self.mallasLayer);
           
            mallas=keys(self.alternativa.mallas);
            for i=1:length(mallas)
                malla=self.alternativa.mallas(mallas{i});
                xIni=malla.xIni;yIni=malla.yIni;                
                x_=[0,0,malla.x,malla.x];y_=[0,malla.y,malla.y,0];                
                x=xIni+cosd(malla.angle).*x_-sind(malla.angle).*y_;
                y=yIni+sind(malla.angle).*x_+cosd(malla.angle).*y_;                
                polX=x;
                polY=y;
                self.mallasLayer(malla.nombre)=fill(polX,polY,'k','EdgeColor','k','FaceColor','none','LineWidth',2.5,'Visible','off','Parent',self.ax);                               
            end
            
            %%%%%%%%%%%%%%%%%%%Puntos en Cache
            [~,~,prjZone]=util.deg2utm(self.project.nodosDow.lat(1),self.project.nodosDow.lon(1));
            
            files=dir(fullfile(self.folders.cache,'*.dow')); 
              x=[];
              y=[];
              xyz=self.alternativa.xyz;
              minX=min(xyz(:,1));
              maxX=max(xyz(:,1));
              minY=min(xyz(:,2));
              maxY=max(xyz(:,2));
             for i=1:length(files)
                 nodo=files(i).name;
                 latLon=regexp(nodo(4:end-4),'_','split');
                 lon=str2double(latLon{2});
                 lat=str2double(latLon{1});                                      
                    [~,~,z1]=util.deg2utm(lat,lon);
                    if strcmp(z1,prjZone)                        
                        [x_,y_]=util.deg2utm(lat,lon);                
                        if x_>=minX && x_<=maxX && y_>=minY && y_<=maxY
                            x=cat(1,x,x_);
                            y=cat(1,y,y_);
                        end
                    end
             end
             
             self.dowCacheLayer=plot(self.ax,x,y,'ok','MarkerFaceColor','magenta','MarkerSize',10,'LineWidth',0.5,'Visible','off');
            
                
            self.dowLayer=plot(self.ax,self.project.nodosDow.x,self.project.nodosDow.y,'oy','MarkerFaceColor','y','MarkerSize',5,'Visible','off');
            
             dowSelNodes=keys(self.alternativa.dowSelectedNodes);
             
              self.dowSelectedLayers=util.initMap(self.dowSelectedLayers);
              
            for i=1:length(dowSelNodes)                                
                sel=dowSelNodes{i};
                dowDataNode=self.alternativa.dowSelectedNodes(sel);                                
                self.dowSelectedLayers(sel)=plot(self.ax,dowDataNode.x,dowDataNode.y,'or','MarkerFaceColor',[255;190;50]/255,'MarkerSize',10,'LineWidth',2,'Visible','off');            
            end
             hold(self.ax,'off');
        end 
        
        function initContextMenu(self)
            self.cmDowSelected.jmenu=javax.swing.JPopupMenu;
            
            self.cmDowSelected.ameva=javax.swing.JMenuItem('Ameva');                        
            set(self.cmDowSelected.ameva,'ActionPerformedCallback',@self.openAmeva);                    
            self.cmDowSelected.jmenu.add(self.cmDowSelected.ameva);
            
            self.cmDowSelected.mopla=javax.swing.JMenuItem(self.txt.pre('cmWaveCreate'));                        
            set(self.cmDowSelected.mopla,'ActionPerformedCallback',@self.createMopla);                    
            self.cmDowSelected.jmenu.add(self.cmDowSelected.mopla);
            
            self.cmDowSelected.delete=javax.swing.JMenuItem(self.txt.pre('cmWaveDelete'));               
            set(self.cmDowSelected.delete,'ActionPerformedCallback',@self.eliminarNodo);        
            self.cmDowSelected.jmenu.add(self.cmDowSelected.delete);
            

            %%%%%%%Mopla
            self.cmMopla.jmenu=javax.swing.JPopupMenu;
            
            self.cmMopla.poblarSMC=javax.swing.JMenuItem(self.txt.pre('cmMoplaCreate'));               
            set(self.cmMopla.poblarSMC,'ActionPerformedCallback',@self.poblarSMC);        
            self.cmMopla.jmenu.add(self.cmMopla.poblarSMC);
            
            
            self.cmMopla.limpiar=javax.swing.JMenuItem(self.txt.pre('cmMoplaClean'));               
            set(self.cmMopla.limpiar,'ActionPerformedCallback',@self.limpiarMopla);        
            self.cmMopla.jmenu.add(self.cmMopla.limpiar);
            
            self.cmMopla.jmenu.addSeparator
            self.cmMopla.moplaLauncher=javax.swing.JMenuItem(self.txt.pre('cmMoplaLauncher'));               
            set(self.cmMopla.moplaLauncher,'ActionPerformedCallback',@self.moplaLauncher);        
            self.cmMopla.jmenu.add(self.cmMopla.moplaLauncher);            
            self.cmMopla.jmenu.addSeparator
            
            self.cmMopla.copiar=javax.swing.JMenuItem(self.txt.pre('cmMoplaCopy'));               
            set(self.cmMopla.copiar,'ActionPerformedCallback',@self.copiarMopla);        
            self.cmMopla.jmenu.add(self.cmMopla.copiar);
            
            self.cmMopla.eliminar=javax.swing.JMenuItem(self.txt.pre('cmMoplaDelete'));               
            set(self.cmMopla.eliminar,'ActionPerformedCallback',@self.eliminarMopla);        
            self.cmMopla.jmenu.add(self.cmMopla.eliminar);            
            
            self.cmMopla.jmenu.addSeparator
            
            
            self.cmMopla.propiedades=javax.swing.JMenuItem(self.txt.pre('cmMoplaProperties'));               
            set(self.cmMopla.propiedades,'ActionPerformedCallback',@self.propiedadesMopla);        
            self.cmMopla.jmenu.add(self.cmMopla.propiedades);
            
        end
    end
    %%%%% Constructor/Destructor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Toolbar
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods   
       function selectPoint(self,varargin)  
            util.disableOtherButtons(self.toolbar,0)
             util.disableOtherButtons(self.toolbar,0)
            core.blockMainForm;
            try
            [x,y]=ginput(1);
            x_point=[];
            y_point=[];
            Z=0;       
            xData=self.project.nodosDow.x;yData=self.project.nodosDow.y;

             xMin=min(self.alternativa.xyz(:,1));xMax=max(self.alternativa.xyz(:,1));
             yMin=min(self.alternativa.xyz(:,2));yMax=max(self.alternativa.xyz(:,2));  
            if xMin<=x && xMax>=x && yMin<=y && yMax>=y
                [~,index]=min(sqrt((xData-x).^2+(yData-y).^2));
                x_point=xData(index);
                y_point=yData(index);                
                lat=self.project.nodosDow.lat(index);
                lon=self.project.nodosDow.lon(index);                
                Z=self.project.nodosDow.z(index);
            end
            if isempty(x_point),return;end

            if ishandle(self.currentPoint),delete(self.currentPoint);end;
            hold(self.ax,'on');
            point=plot(self.ax,x_point,y_point,'or','MarkerFaceColor','r','MarkerSize',12);
            hold(self.ax,'off');
            catch ex
                core.blockMainForm;
                msgbox(ex.message)
                return;
            end            
            core.blockMainForm;
            option=smc.selectPointForm(x_point,y_point,lat,lon,Z);
            
            switch option
                case 'select'
                   key=[num2str(x_point),'_',num2str(y_point)];                     
                   dataNode.x=x_point;
                   dataNode.y=y_point;
                   dataNode.lat=lat;
                   dataNode.lon=lon;       
                   dataNode.z=Z;
                   if ishandle(point),delete(point);end;
                   %%Añado a la capa                 
                  if ~isKey(self.alternativa.dowSelectedNodes,key)
                    self.alternativa.dowSelectedNodes(key)=dataNode;
                    hold(self.ax,'on');
                    self.dowSelectedLayers(key)=plot(self.ax,x_point,y_point,'or','LineWidth',2,'MarkerFaceColor',[255;190;50]/255,'MarkerSize',10);
                    hold(self.ax,'off');
                    %%Añado al explorador
                    parent=util.getNodeByKey(self.tree,'chk_dow_0');
                    selNode=uitreenode('v0',['chk_dow_',key,'_1'],...
                    key,[],true);
                    util.setNodeIcon(selNode,'dow.gif');                                
                    parent.add(selNode);
                    self.tree.reloadNode(parent);
                    %util.refreshTree(self.tree);
                 end
                case 'move'
                  if ishandle(self.currentPoint),delete(self.currentPoint);end;
                  self.currentPoint=point;
                  self.selectPoint;       
                case 'cancel'
                    if ishandle(self.currentPoint),delete(self.currentPoint);end;                     
                    if ishandle(point),delete(point);end;   
            end
       end

       function saveAlternativa(self,varargin)      
            util.disableOtherButtons(self.toolbar,0)
            smctFile=fullfile(self.project.folder,self.alternativa.nombre,'Alternativa.smct');
            altPrj.dowSelectedNodes=self.alternativa.dowSelectedNodes;          
            altPrj.costas=self.alternativa.costas;          
            altPrj.coastDate=self.alternativa.coastDate;          
            altPrj.xyz=self.alternativa.xyz;          
            altPrj.xyzDate=self.alternativa.xyzDate;          
            altPrj.F=self.alternativa.F;          
            save(smctFile,'altPrj','-mat');
       end

       function refreshPrj(self,varargin)
           self.refreshMoplaExplorer;
       end          

       function createPerfil(self,varargin)
            util.disableOtherButtons(self.toolbar,0)
            core.blockMainForm;
            try
                  
            line=imline(self.ax);
            if isempty(line)
                core.blockMainForm;
                return;
            end
            f=findobj('tag','smcWinAlternativaProfile');
            if ~isempty(f)
                close(f(1));
            end      
            position=line.getPosition;
            delete(line);
            xyz=self.alternativa.xyz;
            F=TriScatteredInterp(xyz(:,1),xyz(:,2),xyz(:,3));
            x_=linspace(position(1,1),position(2,1),50);
            y_=linspace(position(1,2),position(2,2),50);
            z_=F(x_,y_);  
            l=sqrt((x_-x_(1)).^2+(y_-y_(1)).^2);
            hold(self.ax,'on')
            line=plot(self.ax,position(:,1),position(:,2),'Color',self.profileColor,'LineWidth',2.5);
            hold(self.ax,'off')
            catch ex
                core.blockMainForm;
                msgbox(ex.message)
                return;
            end
            core.blockMainForm;
            smc.profileForm(l,-z_,line);            
       end
    end
    %%% End  Toolbar
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Layers
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods 
         function showHideMalla(self,mallaNombre)            
            visible=get(self.mallasLayer(mallaNombre),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            set(self.mallasLayer(mallaNombre),'Visible',visible);
        end
        
         function showHideDow(self)            
            visible=get(self.dowLayer,'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end
            set(self.dowLayer,'Visible',visible);
         end
        
         function showHideCache(self)            
            visible=get(self.dowCacheLayer,'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end
            set(self.dowCacheLayer,'Visible',visible);
         end
        
         function showHideCosta(self)
             if isempty(self.costaLayer),return;end
              visible=get(self.costaLayer(1),'Visible');
              if strcmp(visible,'on'),visible='off';
              else visible='on';end                
              for i=1:length(self.costaLayer)
                set(self.costaLayer,'Visible',visible);
              end
         end
         
         function showHideDowSelected(self,dowKey)            
            visible=get(self.dowSelectedLayers(dowKey),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            set(self.dowSelectedLayers(dowKey),'Visible',visible);
         end
         
         function showHidePerfil(self,perfil)
            visible=get(self.perfilesLayer(perfil),'Visible');
            if strcmp(visible,'on'),visible='off';
            else visible='on';end                
            set(self.perfilesLayer(perfil),'Visible',visible);
         end
     end       
    %%%   End Layers
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
         function mousePressedCallback(self,jtree,eventData)
             % Get the clicked node
             clickX = eventData.getX;
             clickY = eventData.getY;
             treePath = jtree.getPathForLocation(clickX, clickY);
              if ~isempty(treePath)
                 % check if the checkbox was clicked
                 node = treePath.getLastPathComponent;
                 if clickX <= (jtree.getPathBounds(treePath).x+16)         
                    [~,isCheckNode,~]=util.isCheckNode(node);                                   
                    if isCheckNode                        
                        [~,nodeId] = util.checkNode(self.tree,node);
                        if  ~isempty(strfind(nodeId,'malla_'))
                            malla=nodeId(7:end);
                            self.showHideMalla(malla)
                         elseif strcmp(nodeId,'dow')
                              self.showHideDow;
                         elseif strcmp(nodeId,'cache')
                              self.showHideCache;
                         elseif strcmp(nodeId,'costa')
                              self.showHideCosta;
                        elseif strfind(nodeId,'dow_')
                            dowKey=nodeId(5:end);
                            self.showHideDowSelected(dowKey);                                                                       
%                         elseif strfind(nodeId,'perfil_')
%                             perfil=nodeId(8:end);                            
%                             self.showHidePerfil(perfil);                        
                        end
                    end
                 end
                 %%%%Select Node Method
                 %self.nodeSelected(node);
                 jtree.setSelectionPath(treePath);
                  if eventData.isMetaDown
                      [isCheck,isCheckNode,nodeId]=util.isCheckNode(node);
                       if isCheckNode
                           if strfind(nodeId,'dow_')
                               self.cmDowSelected.jmenu.show(jtree, clickX, clickY);
                           elseif strfind(nodeId,'perfil_')
                               perfil=nodeId(8:end);
                               perfilPlot=self.perfilesLayer(perfil);
                               isEdit=strcmp(get(perfilPlot,'Tag'),'imline');
                               set(self.cmPerfil.editar,'Text','Editar');
                               if isEdit,set(self.cmPerfil.editar,'Text','Aplicar cambios');end
                               set(self.cmPerfil.editar,'Enabled','on');
                               if ~isCheck,set(self.cmPerfil.editar,'Enabled','off');end
                               self.cmPerfil.jmenu.show(jtree, clickX, clickY);
                           end
                       else
                           if strfind(nodeId,'moplaCase')
%                                status=smc.getMoplaStatusByNode(node);
%                                set(self.cmMopla.analisis,'Enabled','off');
%                                if status.run==status.all
%                                    set(self.cmMopla.analisis,'Enabled','on');
%                                end
                               self.cmMopla.jmenu.show(jtree, clickX, clickY);
                               %La opción de análisis solo se ve si está
                               %verde
                           end
                       end
                  end
                 %%%%ContextMenu
              end
         end
         
         function openAmeva(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);
            dowData=self.alternativa.dowSelectedNodes(nodeId(5:end));                 
            core.addAmevaNode(self.tree,dowData.lat,dowData.lon,'DOW'); 
            notify(self,'ChangeMenu');
         end
         
         function eliminarNodo(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);
            node.removeFromParent;
            parent=util.getNodeByKey(self.tree,'chk_dow_0');
            self.tree.reloadNode(parent);
            %util.refreshTree(self.tree);
            self.alternativa.dowSelectedNodes.remove(nodeId(5:end));
            delete(self.dowSelectedLayers(nodeId(5:end)));
            self.dowSelectedLayers.remove(nodeId(5:end));
            notify(self,'ChangeMenu');
         end
         
         function createMopla(self,varargin)
             mallas=keys(self.alternativa.mallas);
             mallasMopla={};
             for i=1:length(mallas)
                 if length(mallas{i})==10
                     mallasMopla=cat(1,mallasMopla,mallas{i});
                 end
             end
             
              if isempty(mallasMopla)
                 msgbox(self.txt.pre('iMoplaNameErrorLabel'),self.txt.pre('iMoplaNameErrorTitle'),'error');
                 return;
             end
        
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);	
            dowData=self.alternativa.dowSelectedNodes(nodeId(5:end));         
            
            [outData,~]=data.getDOWTemporalSerie(self.db,dowData.lat,dowData.lon,100);
            pause(0.5);
            sourceMopla.lat=dowData.lat;
            sourceMopla.lon=dowData.lon;
            sourceMopla.time=outData.time;
            sourceMopla.y=dowData.y;
            sourceMopla.x=dowData.x;
            
            sourceMopla.z=dowData.z;
            sourceMopla.hs=outData.hs;
            sourceMopla.tp=outData.tp;  %Cambiar!!!!!
            sourceMopla.dir=outData.dir;
            sourceMopla.w_spr=outData.w_spr;
            altDir=fullfile(self.project.folder,self.alternativa.nombre);
            core.blockMainForm;
            try
            hasChanges=smc.casosMoplaForm(self.db,altDir,self.alternativa,sourceMopla,mallasMopla,'new');                        
            catch ex
                hasChanges=false;
                msgbox(ex.message,'error','error');
            end  

            %smc.casosMoplaForm(self.alternativa,sourceMopla,mallas,'new');
            
            if hasChanges
                self.refreshMoplaExplorer
                self.saveAlternativa; %TODOOOOOOOOOOOOOOO
            end
             core.blockMainForm;
	        %smc.createMoplaCasos(nodeId--);
         end
    end 
    %%% End   Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Mopla
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function refreshMoplaExplorer(self)
            moplaNode=util.getNodeByKey(self.tree,'moplaSmc');
            util.cleanNode(moplaNode,self.tree);
            moplaFiles=dir(fullfile(self.project.folder,self.alternativa.nombre,'Mopla','*.smct'));
            for i=1:length(moplaFiles)
                moplaId=moplaFiles(i).name;
                moplaId=moplaId(1:end-5);
                status=smc.getMoplaStatus(fullfile(self.project.folder,self.alternativa.nombre,'Mopla'),moplaId,self.alternativa.mallas);                
                casoMoplaNode=uitreenode('v0',['moplaCase_',moplaId],[moplaId, '[',num2str(status.run),...
                   '/', num2str(status.all),']'],[],true);
                icon='ball_yellow.gif'; 
                if status.run>0, icon='ball_red.gif'; end
                if status.run>=status.all, icon='ball_green.gif'; end %%TODO:STATUS
                util.setNodeIcon(casoMoplaNode,icon);
                moplaNode.add(casoMoplaNode);
            end

            self.tree.reloadNode(moplaNode);
            util.refreshTree(self.tree);
        end
        
        function propiedadesMopla(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);	            
            mopla=smc.loadMopla(fullfile(self.project.folder,self.alternativa.nombre),nodeId(11:end));
            
             mallas=keys(self.alternativa.mallas);        
             mallasMopla={};
             for i=1:length(mallas)                
                 if length(mallas{i})==10
                     mallasMopla=cat(1,mallasMopla,mallas{i});
                 end
             end
            
             if isempty(mallasMopla)
                 msgbox(self.txt.pre('iMoplaNameErrorLabel'),self.txt.pre('iMoplaNameErrorTitle'),'error');
                 return;
             end
            
             status=smc.getMoplaStatusByNode(node);
             if status.run==0
                smc.casosMoplaForm(self.db,fullfile(self.project.folder,self.alternativa.nombre),self.alternativa,mopla,mallasMopla,'edit');                 
             else
               smc.casosMoplaForm(self.db,fullfile(self.project.folder,self.alternativa.nombre),self.alternativa,mopla,mallasMopla,'properties',status); 
             end
        end
        
        function copiarMopla(self,varargin)
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);	            
            mopla=smc.loadMopla(fullfile(self.project.folder,self.alternativa.nombre),nodeId(11:end));
            
             mallas=keys(self.alternativa.mallas);
             mallasMopla={};
             for i=1:length(mallas)                
                 if length(mallas{i})==10
                     mallasMopla=cat(1,mallasMopla,mallas{i});
                 end
             end
            
             if isempty(mallasMopla)
                 msgbox(self.txt.pre('iMoplaNameErrorLabel'),self.txt.pre('iMoplaNameErrorTitle'),'error');
                 return;
             end
                     
            smc.casosMoplaForm(self.db,fullfile(self.project.folder,self.alternativa.nombre),self.alternativa,mopla,mallasMopla,'copy');
            
            %self.refreshMoplaExplorer;
            core.refreshProjectExplorer(self.tree,self.project, self);
        end
        
        function eliminarMopla(self,varargin)            
            moplaId=self.limpiarMopla();
%             jtree = handle(self.tree.getTree,'CallbackProperties'); 
%             treePath=jtree.getSelectionPath();
%             node = treePath.getLastPathComponent;
%             [~,~,nodeId]=util.isCheckNode(node);	
            moplaFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',[moplaId,'.smct']);
            moplaDir=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',moplaId);
            
            delete(moplaFile);
            if exist(moplaDir,'dir')
                rmdir(moplaDir);
            end
            core.refreshProjectExplorer(self.tree,self.project, self);
            core.refreshAmevaExplorer(self.tree,self.project,self);
            
        end
        
        function moplaId=limpiarMopla(self,varargin)
             msgbox(self.txt.pre('iSMCCloseLabel'),...
                self.txt.pre('iSMCCloseTitle'), 'warning');
            uiwait
            watchon
            jtree = handle(self.tree.getTree,'CallbackProperties'); 
            treePath=jtree.getSelectionPath();
            node = treePath.getLastPathComponent;
            [~,~,nodeId]=util.isCheckNode(node);	
            poiDir=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',nodeId(11:end));            
            delete(fullfile(poiDir,'*.poi'));                        
            delete(fullfile(poiDir,'*.poi_'));                        
            
            moplaId=nodeId(11:end);
            mopla=smc.loadMopla(fullfile(self.project.folder,self.alternativa.nombre),nodeId(11:end));
            folder=fullfile(self.project.folder,self.alternativa.nombre,'Mopla','SP');
            calcFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla','ScriptCalculo.dat');
            delete(calcFile);
            fid=fopen(calcFile,'w');
            fclose(fid);            
            c=1;
            mallasStr=mopla.mallas(logical(mopla.propagate));
            for i=1:length(mopla.tide)        
                for j=1:length(mallasStr)                    
                    mallaId=mallasStr{j};
                    malla=self.alternativa.mallas(mallaId);
                    casoId=['000',num2str(c)];
                    casoId=casoId(end-3:end);                    
                    delete(fullfile(folder,[mallaId,mopla.moplaId,casoId,'*.*'])); 
                    mallaPadre=malla;
                    while  ~isempty(mallaPadre.parent)                        
                        mallaPadre=self.alternativa.mallas(mallaPadre.parent);                
                        delete(fullfile(folder,[mallaPadre.nombre,mopla.moplaId,casoId,'*.*'])); 
                    end
                    delete(fullfile(self.project.folder,self.alternativa.nombre,'Mopla',[mallaPadre.nombre,mopla.moplaId,casoId,'ENC.DAT']));
                    delete(fullfile(self.project.folder,self.alternativa.nombre,'Mopla',[mallaPadre.nombre,mopla.moplaId,casoId,'IN2.DAT']));
                    delete(fullfile(self.project.folder,self.alternativa.nombre,'Mopla',[mallaPadre.nombre,mopla.moplaId,casoId,'COP2.DAT']));
                    delete(fullfile(self.project.folder,self.alternativa.nombre,'Mopla',[malla.nombre,mopla.moplaId,casoId,'COP2.DAT']));
                    c=c+1;
                end
            end
            
            core.refreshProjectExplorer(self.tree,self.project, self);
            core.refreshAmevaExplorer(self.tree,self.project,self);
            watchoff
            
        end
        
        function poblarSMC(self,varargin)
            moplaId=self.limpiarMopla();
%             jtree = handle(self.tree.getTree,'CallbackProperties'); 
%             treePath=jtree.getSelectionPath();
%             node = treePath.getLastPathComponent;
%             [~,~,nodeId]=util.isCheckNode(node);	
%            moplaFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla',[moplaId,'.smct']);            
%            mopla=load(moplaFile,'-mat');
%            mopla=mopla.mopla;
            mopla=smc.loadMopla(fullfile(self.project.folder,self.alternativa.nombre),moplaId);
            smc.createMoplaCases(fullfile(self.project.folder,self.alternativa.nombre),mopla,self.alternativa.mallas);
            %self.refreshMoplaExplorer;
        end               
        
        function moplaLauncher(self,varargin)
           calcFile=fullfile(self.project.folder,self.alternativa.nombre,'Mopla','ScriptCalculo.dat');
           if exist(calcFile,'file')
            smc.moplaLauncher(fullfile(self.project.folder,self.alternativa.nombre,'Mopla','ScriptCalculo.dat')); 
           end
        end
    end
    
    %%% End   Mopla
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    
end


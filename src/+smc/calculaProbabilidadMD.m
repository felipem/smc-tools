function outMopla=calculaProbabilidadMD(mopla)

    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
casosMD=mopla.casosMD(logical(mopla.propagate));
h=waitbar(0,txt.pre('pdMDProbability'));
hs=mopla.hs(casosMD);
dir=mopla.dir(casosMD);
tp=mopla.tp(casosMD);
%probabilidad=zeros(length(mopla.tide),length(hs));
probabilidad=zeros(length(hs),1);

hs=hs/max(mopla.hs);
tp=tp/max(mopla.tp);

hs_ori=mopla.hs/max(mopla.hs);
tp_ori=mopla.tp/max(mopla.tp);

cosdir=cosd(dir);
sindir=cosd(dir);
cosdir_ori=cosd(mopla.dir);
sindir_ori=cosd(mopla.dir);
%db=load(fullfile('data','smcTools.db'),'-mat');
%db=db.db;
%[tideTime,tide]=data.getGOTTemporalSerie(db,mopla.lat,mopla.lon);
%waitbar(0.1);
%tide=interp1(tideTime,tide,mopla.time);
waitbar(0.2);



for i=1:length(mopla.hs)    
    distance=(hs-hs_ori(i)).^2+(tp-tp_ori(i)).^2+(cosdir-cosdir_ori(i)).^2+(sindir-sindir_ori(i)).^2;
    [~,idxCaso]=min(distance);
    %[~,idxMarea]=min(abs(tide(i)-mopla.tide));
    %probabilidad(idxMarea,idxCaso)=probabilidad(idxMarea,idxCaso)+1;    
    probabilidad(idxCaso)=probabilidad(idxCaso)+1;    
end
waitbar(1);
outMopla=mopla;
%outMopla.probabilidad=probabilidad./sum(sum(probabilidad));
outMopla.probabilidad=probabilidad/sum(probabilidad);
delete(h);

end


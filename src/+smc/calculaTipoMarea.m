function [c,tipo]=calculaTipoMarea(tide)
%%% tide = serie temporal de marea astronóminca
%%% c = media de la carrera de marea
%%% tipo = tipo de marea ('micro','meso','macro')

c=1;
tipo='micro';

end
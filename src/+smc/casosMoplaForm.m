function hasChanges=casosMoplaForm(db,altDir,alternativa,sourceMopla,mallas,mode,moplaStatus)
    
    hasChanges=false;
    format='%.2f';  %Formato de visualizacion de numeros;
    maxAngulo=60; %Angulo a cada lado de oluca
    
    hs=sourceMopla.hs;
    dir=sourceMopla.dir;
    tp=sourceMopla.tp;
    w_spr=sourceMopla.w_spr;
    %%% Cargo el idioma
    txt=util.loadLanguage;
    cfg=util.loadConfig;
    %%%%%%%%%%%%%%%%%%%
    
    mallas_dic=containers.Map;
    
    for i=1:length(mallas)
        malla=mallas{i};
        if ~isKey(mallas_dic,malla)
            %moplaDir=fullfile(altDir,'Mopla');
            %ref2file=fullfile(moplaDir,[malla,'REF2.DAT']);
            mallas_dic(malla)=alternativa.mallas(malla);
        end
    end    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Nuevo Mopla
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(mode,'new') 
        [moplaId,numCasos,dirMin,dirMax,dateIni,dateEnd]=smc.newMoplaDlg(altDir);
        if isempty(moplaId)
            hasChanges=false;
            return;
        end    
    
        sourceMopla.dirMin=dirMin;
        sourceMopla.dirMax=dirMax;  
        %%%Pongo el en el caso en que se buscque por direcciones menores que 0 sean <0 (para pasar por
        %%%el 0
        dir_tmp=dir;
%         if dirMin<0            
%             dir_tmp(dir_tmp>180)=dir_tmp(dir_tmp>180)-360;
%         end       
        if dirMin<=dirMax
            index_dir=dir_tmp>=dirMin & dir_tmp<=dirMax;
        else
            index_dir=dir_tmp>=dirMin | dir_tmp<=dirMax;
        end
        index=index_dir & sourceMopla.time>=dateIni & sourceMopla.time<=dateEnd;
        hs=hs(index);
        dir=dir(index);
        tp=tp(index);
        w_spr=w_spr(index);
        sourceMopla.time=sourceMopla.time(index);
        %sourceMopla.pois=containers.Map;        
        sourceMopla.moplaId=moplaId;
        sourceMopla.dirMin=dirMin;
        sourceMopla.dirMax=dirMax;
        sourceMopla.casosMD=ameva.maxDiss(hs,tp,dir,numCasos);
        sourceMopla.hsMin=min(hs);
        sourceMopla.tpMin=min(tp);
        sourceMopla.gamma=[0,3.3];
        sourceMopla.sigma=[0,20];
        sourceMopla.tide=zeros(3,1);
        sourceMopla.mallas={};
        sourceMopla.propagate=[];
        sourceMopla.date=clock;
        sourceMopla.hs=hs;
        sourceMopla.dir=dir;
        sourceMopla.tp=tp;
        sourceMopla.w_spr=w_spr;  
        sourceMopla.coplaTime=500; %COPLA Temporal
        dir_md_=dir(sourceMopla.casosMD); 
        for i=1:length(sourceMopla.casosMD)
            dir_rel=999;
            mallaIndex=1;
            for k=1:length(mallas)
                dir_rel_tmp=abs(calculaDirRel(dir_md_(i),mallas_dic(mallas{k})));
                if dir_rel_tmp<dir_rel
                    dir_rel=dir_rel_tmp;
                    mallaIndex=k;
                end
            end
            mallaHija=getMallaHija(mallas_dic(mallas{mallaIndex}));
            sourceMopla.mallas{i}=mallaHija.nombre;
            sourceMopla.propagate(i)=true;
        end
    end
    %%%%% End Nuevo Mopla
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Copiar Mopla
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(mode,'copy')
        moplaId=smc.newMoplaLiteDlg(altDir);
        if isempty(moplaId),return;end
        sourceMopla.moplaId=moplaId;
        sourceMopla.date=clock;
    end
    %%%%% End Copiar Mopla
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Editar
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(mode,'edit')
        moplaId=sourceMopla.moplaId;        
    end    
    %%%%% End Editar
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Modo - Propiedades
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    canEdit=true;
    moplaRun=0;
    casosAll=length(sourceMopla.casosMD)*length(sourceMopla.tide);
    if strcmp(mode,'properties')
        moplaId=sourceMopla.moplaId;        
        canEdit=false;
        moplaRun=moplaStatus.run;
        casosAll=moplaStatus.all;
    end    
    %%%%% End Modo - Propiedades
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Creaci�n de la ventana Principal
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    f = figure( 'MenuBar', 'none', 'Name', 'Mopla', 'NumberTitle', 'off', 'Toolbar', 'none','Position',[50 50 800 600],'CloseRequestFcn',@closeForm,'ResizeFcn',@resize,'Visible','off');
    pos=get(f,'Position');
    
    btOk=uicontrol(f,'Style','pushbutton','String', 'Ok','Position',[30 10 50 20],'Callback',@ok);
    if ~canEdit,set(btOk,'Enable','off'); end
    uicontrol(f,'Style','pushbutton','String','Cancel', 'Position',[120 10 50 20],'Callback',@cancel);  
    
    tabControl=uitoolbar(f);  %%Toolbar para el manejo de las pestañas
    tabs=[];
    tabPanels=[];
    %%%%% End Creaci�n de la ventana Principal
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Pestaña de información general
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    tabs(1)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.pre('wMoplaInfoTitle'));
    util.setControlIcon(tabs(1),'information.gif');
    util.toolBarSeparator(tabControl);
    
    tabPanels(1)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');
    
    pos=get(tabPanels(1),'position');
    infoPanel=uipanel('Parent',tabPanels(1),'units','pixel','position',[0 pos(4)-200,pos(3)/2,200]);
    logoPanel=uipanel('Parent',infoPanel,'units','pixel','position',[10,40,128,128]);
    axLogo=axes('Parent',logoPanel,'XTick',[],'YTick',[],'position',[0 0 1 1]);
    imshow(fullfile('icon','emc.gif'),'Parent',axLogo);
    
    
    uicontrol(infoPanel,'Style','text','String',['ID: ',moplaId],'FontSize',14,'FontWeight','bold','Position',[138 145 200 25]);
    uicontrol(infoPanel,'Style','text','String',['Dir <= ',num2str(sourceMopla.dirMax,format)],'FontSize',14,'FontWeight','bold','Position',[138 120 200 25]);
    uicontrol(infoPanel,'Style','text','String',['Dir >= ',num2str(sourceMopla.dirMin,format)],'FontSize',14,'FontWeight','bold','Position',[138 90 200 25]);
    uicontrol(infoPanel,'Style','text','String',[txt.pre('wMoplaInfoExec'),': ',num2str(moplaRun),'/',num2str(casosAll)],'FontSize',14,'FontWeight','bold','Position',[138 70 200 25]);
    uicontrol(infoPanel,'Style','text','String',[txt.pre('wMoplaInfoDate'),': ',datestr(sourceMopla.date)],'FontSize',14,'FontWeight','bold','Position',[138 45 200 25]);
    
    mapPanel=uipanel('Parent',tabPanels(1),'units','pixel','position',[0 0,pos(3)/2,pos(4)-200]);
    axMap=axes('Parent',mapPanel,'XTick',[],'YTick',[],'Box','on');
    xMin=min(alternativa.xyz(:,1));
    xMax=max(alternativa.xyz(:,1));
    yMin=min(alternativa.xyz(:,2));
    yMax=max(alternativa.xyz(:,2));
    [~,~,z1]=util.deg2utm(sourceMopla.lat,sourceMopla.lon);
    [I,x,y]=util.getGMImage(yMin,xMin,yMax,xMax,'hybrid',z1);
    
    if ~isempty(I) %%%Ha descargado la Imagen
        image(x,y,I,'Parent',axMap);
        set(axMap,'YDir','normal');
        axis(axMap,'image');
        hold(axMap,'on');
        plot(axMap,sourceMopla.x,sourceMopla.y,'or','MarkerSize',8,'MarkerFaceColor','r');
        hold(axMap,'off');
    end
    
    
    geoPanel=uipanel('Parent',tabPanels(1),'units','pixel','position',[pos(3)/2,85,pos(3)-pos(3)/2,pos(4)-85]);
    axGeo=axes('Parent',geoPanel,'XTick',[],'YTick',[],'Box','on');
    
    if isfield(db,'shp_region') %Casos puntales como el de españa
        shp=load(fullfile('data',cfg('region'),'shp','regionalMap.mat'));
        shp=shp.shp;
         hold(axGeo,'on');
        for i=1:length(shp.maps)
            fill(shp.maps(i).x,shp.maps(i).y, [0.6 0.6 0.6],'LineWidth',1.2,'Parent',axGeo);
        end                    
        %axis(axGeo, db.coord_region); 
       
        plot(axGeo,sourceMopla.lon,sourceMopla.lat,'or','MarkerSize',8,'MarkerFaceColor','r');
        hold(axGeo,'off');
    end
    
    coordPanel=uipanel('Parent',tabPanels(1),'units','pixel','position',[pos(3)/2 0 pos(3)-pos(3)/2 85]);
    txtLat=uicontrol(coordPanel,'Style','text','String',['Lat: ',num2str(sourceMopla.lat,format)],'FontSize',14,'FontWeight','bold','Position',[0 55 pos(3)-pos(3)/2 25]);
    txtLon=uicontrol(coordPanel,'Style','text','String',['Lon: ',num2str(sourceMopla.lon,format)],'FontSize',14,'FontWeight','bold','Position',[0 30 pos(3)-pos(3)/2 25]);
    txtZ=uicontrol(coordPanel,'Style','text','String',['Z: ',num2str(sourceMopla.z,format)],'FontSize',14,'FontWeight','bold','Position',[0 5 pos(3)-pos(3)/2 25]);

    
    %%%%% End Pesta�a de informaci�n general
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Pesta�a Maxdiss
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tabs(2)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.pre('wMoplaMDTitle'));
    util.setControlIcon(tabs(2),'maxdiss.gif');
    pause(0.1);
    util.toolBarSeparator(tabControl);
    tabPanels(2)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');
    
    mdBtPanel=uipanel(tabPanels(2),'units','pixel','position',[0 pos(4)-30 pos(3) 30]);
    uicontrol(mdBtPanel,'Style','text','String','hs>=','Position',[10 0 40 20]);
    tbHsMin=uicontrol(mdBtPanel,'Style','edit','String',num2str(sourceMopla.hsMin,format),'Position',[50 3 40 20]);
    uicontrol(mdBtPanel,'Style','text','String','Tp>=','Position',[95 0 40 20]);
    tbTpMin=uicontrol(mdBtPanel,'Style','edit','String',num2str(sourceMopla.tpMin,format),'Position',[135 3 40 20]);
    bRefresh=uicontrol(mdBtPanel,'Style','pushbutton','Position',[190 2 24 24],'Callback',@refreshGraph);
    util.setControlIcon(bRefresh,'refresh_graph.gif');
    
    cbMdGraph=uicontrol(mdBtPanel,'Style','popup','String','hs-tp|hs-dir|tp-dir','Position',[230 5 100 20],'Callback',@refreshGraph);
    
    mdAxPanel=uipanel(tabPanels(2),'units','pixel','position',[0 0 pos(3) pos(4)-30]);        
    mdAxes=axes('Parent',mdAxPanel);
    refreshGraph();
    %%%%% Pestaña Maxdiss
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Pesta�a Mallas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tabs(3)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.pre('wMoplaMeshTitle'));
    util.setControlIcon(tabs(3),'malla.gif');
    util.toolBarSeparator(tabControl);
    tabPanels(3)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');
    
    cnames={'malla','hs','tp','dir','dir_rel',txt.pre('wMoplaMeshProp')};
    cformat={mallas','char','char','char','char','logical'};
     
    hs_md_=hs(sourceMopla.casosMD);         
    tp_md_=tp(sourceMopla.casosMD);         
    dir_md_=dir(sourceMopla.casosMD); 
    hs_md_(hs_md_<sourceMopla.hsMin)=sourceMopla.hsMin;
    tp_md_(tp_md_<sourceMopla.tpMin)=sourceMopla.tpMin;
    
    tblData={};
    for i=1:length(sourceMopla.casosMD)
         
         dir_rel=calculaDirRel(dir_md_(i),mallas_dic(sourceMopla.mallas{i}));
         if (abs(dir_rel)>maxAngulo)
             dir_rel=colText(num2str(dir_rel,format),'red');
         else
             dir_rel=num2str(dir_rel,format);
         end
        tblData=cat(1,tblData,{sourceMopla.mallas{i},num2str(hs_md_(i),format),num2str(tp_md_(i),format),...
            num2str(dir_md_(i),format),dir_rel,logical(sourceMopla.propagate(i))});
    end
    %data{:,end}=logical(data{:,end});
    tblMallas=uitable(tabPanels(3),'units','normalized','Position',[0 0 1 1],'Data',tblData,...
        'ColumnName',cnames,'ColumnFormat',cformat,...
        'ColumnEditable',[true,false,false,false,false,true]);
    
    set(tblMallas,'CellEditCallback',@cellChange);
    %%%%% End Pesta�a Mallas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Pesta�a Spectrum
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tabs(4)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.pre('wMoplaSpcTitle'));
    util.toolBarSeparator(tabControl);
    util.setControlIcon(tabs(4),'cdf.gif');    
    tabPanels(4)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');
    spTidePanel=uipanel(tabPanels(4),'units','pixel','position',[0  30+(pos(4)-30)*2/3 pos(3)/2 (pos(4)-30)/3]);
    spTideGraphPanel=uipanel(tabPanels(4),'units','pixel','position',[pos(3)/2  30+(pos(4)-30)/2 pos(3)/2 (pos(4)-30)/2]);
    spGammaPanel=uipanel(tabPanels(4),'units','pixel','position',[0 30+(pos(4)-30)*1/3 pos(3)/2 (pos(4)-30)*1/3]);
    spSigmaPanel=uipanel(tabPanels(4),'units','pixel','position',[0 30 pos(3)/2 (pos(4)-30)*1/3]);
    spGammaGraphPanel=uipanel(tabPanels(4),'units','pixel','position',[pos(3)/2 30 pos(3)/2 (pos(4)-30)/2]);
    spCurrentsPanel=uipanel(tabPanels(4),'units','pixel','position',[0 0 pos(3)-100 30]);
    spCurrentsPanel2=uipanel(tabPanels(4),'units','pixel','position',[pos(3)*0.75 0 pos(3)*0.25 30]);
    
    %Pinto el regimen de marea
    [tideTime,tideData]=data.getGOTTemporalSerie(db,sourceMopla.lat,sourceMopla.lon);
    tideData=tideData-min(tideData);
    tideVar=ameva.AmevaVar('tide','m',tideData,tideTime);
    ameva.Graphs.distFunction(spTideGraphPanel,tideVar,txt);
    
    pos=get(spTidePanel,'position');
    
    uicontrol(spTidePanel,'Style','text','String',txt.pre('wMoplaLowTide'),'Position',[20 20 100 20]);
    uicontrol(spTidePanel,'Style','text','String',txt.pre('wMoplaMediumTide'),'Position',[20 60 100 20]);
    uicontrol(spTidePanel,'Style','text','String',txt.pre('wMoplaHighTide'),'Position',[20 100 100 20]);
    
    tbLow=uicontrol(spTidePanel,'Style','edit','Position',[140 20 50 20],'BackgroundColor',[1 1 1]);
    tbMedium=uicontrol(spTidePanel,'Style','edit','Position',[140 60 50 20],'BackgroundColor',[1 1 1]);
    tbHigh=uicontrol(spTidePanel,'Style','edit','Position',[140 100 50 20],'BackgroundColor',[1 1 1]);
    
    bLow=uicontrol(spTidePanel,'Style','togglebutton','Value',1,'Position',[210 20 24 24],'Callback',{@tideButtonChange,'low'});    
    bMedium=uicontrol(spTidePanel,'Style','togglebutton','Value',1,'Position',[210 60 24 24],'Callback',{@tideButtonChange,'medium'});    
    bHigh=uicontrol(spTidePanel,'Style','togglebutton','Value',1,'Position',[210 100 24 24],'Callback',{@tideButtonChange,'high'});
    
    util.setControlIcon(bLow,'ball_green.gif');    
    util.setControlIcon(bMedium,'ball_green.gif');    
    util.setControlIcon(bHigh,'ball_green.gif');
    
    if strcmp(mode,'new') 
        set(tbLow,'String',num2str('0'));
        set(tbMedium,'String',num2str(mean(tideData),format));
        set(tbHigh,'String',num2str(max(tideData),format));
    elseif length(sourceMopla.tide)==3
        set(tbLow,'String',num2str(sourceMopla.tide(1),format));
        set(tbMedium,'String',num2str(sourceMopla.tide(2),format));
        set(tbHigh,'String',num2str(sourceMopla.tide(3),format));
    elseif length(sourceMopla.tide)==2
        set(tbLow,'String',num2str(sourceMopla.tide(1),format));
        set(tbMedium,'String',num2str(mean(tideData),format),'BackgroundColor',[0.7 0.7 0.7],'Enable','off');
        set(tbHigh,'String',num2str(sourceMopla.tide(2),format));
        set(bMedium,'Value',0);
        util.setControlIcon(bMedium,'ball_red.gif');    
    elseif length(sourceMopla.tide)==1
        set(tbLow,'String',num2str('0'),'BackgroundColor',[0.7 0.7 0.7],'Enable','off');
        set(tbMedium,'String',num2str(sourceMopla.tide(1),format));
        set(tbHigh,'String',num2str(max(tideData),format),'BackgroundColor',[0.7 0.7 0.7],'Enable','off');
        set(bLow,'Value',0);
        set(bHigh,'Value',0);
        util.setControlIcon(bLow,'ball_red.gif');    
        util.setControlIcon(bHigh,'ball_red.gif');    
    end
    
    tblGamma=uitable('Parent',spGammaPanel,'Position',[20 20 200 120],...
         'Data',num2cell(sourceMopla.gamma),'ColumnName',{'tp>=','gamma'},...
         'ColumnEditable',[true,true]);
     uicontrol(spGammaPanel,'Style','pushbutton','String',txt.pre('wMoplaSpcAdd'),'Position',[250 50 50 20],'Callback',@addGamma);
     uicontrol(spGammaPanel,'Style','pushbutton','String',txt.pre('wMoplaSpcDelete'),'Position',[250 20 50 20],'Callback',@deleteGamma);
     
     tblSigma=uitable('Parent',spSigmaPanel,'Position',[20 20 200 120],...
         'Data',num2cell(sourceMopla.sigma),'ColumnName',{'tp>=','sigma'},...
         'ColumnEditable',[true,true]);
     uicontrol(spSigmaPanel,'Style','pushbutton','String',txt.pre('wMoplaSpcAdd'),'Position',[250 50 50 20],'Callback',@addSigma);
     uicontrol(spSigmaPanel,'Style','pushbutton','String',txt.pre('wMoplaSpcDelete'),'Position',[250 20 50 20],'Callback',@deleteSigma);
     
    chkCurrents=uicontrol(spCurrentsPanel,'Style','checkbox','Enable','on','Value',sourceMopla.coplaTime>=0,'String',txt.pre('wMoplaSpcCurrents'),'units','normalized','Position',[0 0 0.8 1]);                
    uicontrol(spCurrentsPanel2,'Style','text','String',txt.pre('wMoplaSpcCoplaTime'),'Position',[5 2 95 20]);                
    tbCoplaTime=uicontrol(spCurrentsPanel2,'Style','edit','String',sourceMopla.coplaTime,'Position',[100 5 40 20],'BackgroundColor','w');            
    
    %Pinto el Espectro Jonswap
    bJonswap=uicontrol(spGammaGraphPanel,'Style','pushbutton','Position',[110 5 24 24],'Callback',@pintaJonswap);    
    util.setControlIcon(bJonswap,'refresh_graph.gif');
     uicontrol(spGammaGraphPanel,'Style','text','Position',[5 0 50 20],'String','Gamma:');
    tbGamma=uicontrol(spGammaGraphPanel,'Style','edit','Position',[60 5 50 20],'String','3.300');
    
    spGammaAxes=axes('Parent',spGammaGraphPanel);
    pintaJonswap();
    %%%%% End Pestaña Spectrum
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Pestaña Spectrum Avanzado
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     tabs(5)=uitoggletool(tabControl,'OnCallback',@showTab,'OffCallback',@hideTab,'TooltipString',txt.pre('wMoplaSpcTitle'));    
%     util.setControlIcon(tabs(5),'gear.gif');    
%     tabPanels(5)=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40],'Visible','off');
    %%%%% End Pestaña Spectrum Avanzado
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    set(tabs(1),'State','on');
    set(f,'Visible','on');
    
    
    
    if canEdit
        %pause(0.5);
        %set(f,'WindowStyle','modal');
        uiwait(f);
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Funciones de resize
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function resize(varargin)        
        pos=get(f,'position');
        for w=1:length(tabPanels)
            set(tabPanels(w),'position',[0 40 pos(3) pos(4)-40]);
        end        
        if exist('infoPanel','var'),resizeInfoTab;end        
        if exist('mdBtPanel','var'),resizeMDTab;end
        if exist('spTidePanel','var'),resizeMallasTab;end
        if exist('tblMallas','var'),resizeSPTab;end
    end

    function resizeInfoTab()
        pos=get(tabPanels(1),'position');
        set(infoPanel,'position',[0 pos(4)-200,pos(3)/2,200]);
        set(mapPanel,'position',[0 0,pos(3)/2,pos(4)-200]);
        set(geoPanel,'position',[pos(3)/2 85 pos(3)-pos(3)/2 pos(4)-85]);
        set(coordPanel,'position',[pos(3)/2 0 pos(3)-pos(3)/2 85]);
        set(txtLat,'position',[0 55 pos(3)-pos(3)/2 25]);
        set(txtLon,'position',[0 30 pos(3)-pos(3)/2 25]);
        set(txtZ,'position',[0 5 pos(3)-pos(3)/2 25]);
    end

     function resizeMDTab()
        pos=get(tabPanels(1),'position');
        set(mdBtPanel,'position',[0 pos(4)-30 pos(3) 30]);
        set(mdAxPanel,'position',[0 0 pos(3) pos(4)-30]);
     end
 
    function resizeMallasTab()
        pos=get(tabPanels(1),'position');
        columnWidth={};%get(tblMallas,'ColumnWidth');
        for w=1:length(cnames)
            columnWidth{w}=(pos(3)-80)/length(cnames);
        end
        set(tblMallas,'ColumnWidth',columnWidth);
        
    end

    function resizeSPTab()
       pos=get(tabPanels(1),'position');
       set(spTidePanel,'position',[0 30+(pos(4)-30)*2/3 pos(3)/2 (pos(4)-30)/3]);
       set(spTideGraphPanel,'position',[pos(3)/2 30+(pos(4)-30)/2 pos(3)/2 (pos(4)-30)/2]);
       set(spGammaPanel,'position',[0 30+(pos(4)-30)*1/3 pos(3)/2 (pos(4)-30)*1/3]);
       set(spSigmaPanel,'position',[0 30 pos(3)/2 (pos(4)-30)*1/3]);
       set(spGammaGraphPanel,'position',[pos(3)/2 30 pos(3)/2 (pos(4)-30)/2]);
       set(spCurrentsPanel,'position',[0 0 pos(3)*0.75 30]);
       set(spCurrentsPanel2,'position',[pos(3)*0.75 0 pos(3)*0.25 30]);
       
        pos=get(spGammaPanel,'position');    
        %set(tblTide,'Position',[20 20 200 pos(4)-40]);         
        set(tblGamma,'Position',[20 20 200 pos(4)-40]);
        set(tblSigma,'Position',[20 20 200 pos(4)-40]);
 
    end
    %%%%% End Funciones de resize
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Funciones de control de pestañas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function showTab(varargin)
       util.disableOtherButtons(tabControl,varargin{1});
       tab=(tabs==varargin{1});
       set(tabPanels(tab),'Visible','on');
    end

    function hideTab(varargin)
        tab=(tabs==varargin{1});
       set(tabPanels(tab),'Visible','off');
    end
    %%%%% End Funciónes de concotrl de pestañas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% End Funciónes de mallas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    
     function dir_rel=calculaDirRel(dir,malla)        
        dir_rel=90-dir+180;
        dir_rel=atan2(sind(dir_rel),cosd(dir_rel))*180/pi;
        dir_rel=dir_rel-malla.angle;
        dir_rel=atan2(sind(dir_rel),cosd(dir_rel))*180/pi;   
     end
 
    function mallaHija=getMallaHija(malla)                 
         mallaHija=malla;
         
         for w=1:length(mallas)
             mallaTmp=mallas_dic(mallas{w});
             if strcmp(mallaTmp.parent,malla.nombre)
                 mallaHija=getMallaHija(mallaTmp);
                 break;
             end
         end
     end
    %%%%% End Funciónes de mallas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Marea
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function tideButtonChange(varargin) 
        
        if strcmp(varargin(end),'high') || strcmp(varargin(end),'low')            
            state=get(bHigh,'Value');
            if strcmp(varargin(end),'low')            
                state=get(bLow,'Value');
            end
            set(bLow,'Value',state);
            set(bHigh,'Value',state);            
            if state
                util.setControlIcon(bHigh,'ball_green.gif');           
                util.setControlIcon(bLow,'ball_green.gif');                         
                set(tbLow,'Enable','on','BackgroundColor',[1 1 1]);
                set(tbHigh,'Enable','on','BackgroundColor',[1 1 1]);             
            else
                util.setControlIcon(bHigh,'ball_red.gif');           
                util.setControlIcon(bLow,'ball_red.gif');           
                util.setControlIcon(bMedium,'ball_green.gif');           
                set(tbLow,'Enable','off','BackgroundColor',[0.7 0.7 0.7]);
                set(tbHigh,'Enable','off','BackgroundColor',[0.7 0.7 0.7]);
                set(tbMedium,'Enable','on','BackgroundColor',[1 1 1]);
                set(bMedium,'Value',~state);
            end
        else
            state=get(bMedium,'Value');           
            if ~state
                util.setControlIcon(bHigh,'ball_green.gif');           
                util.setControlIcon(bLow,'ball_green.gif');           
                util.setControlIcon(bMedium,'ball_red.gif');           
                set(tbLow,'Enable','on','BackgroundColor',[1 1 1]);
                set(tbHigh,'Enable','on','BackgroundColor',[1 1 1]);
                set(tbMedium,'Enable','off','BackgroundColor',[0.7 0.7 0.7]);
                set(bLow,'Value',~state);
                set(bHigh,'Value',~state);
            else             
                util.setControlIcon(bMedium,'ball_green.gif');                         
                set(tbMedium,'Enable','on','BackgroundColor',[1 1 1]);
            end
            
        end
        
    end
    
    %%%%% End Marea
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function outHtml = colText(inText, inColor)
        % return a HTML string with colored font
        outHtml = ['<html><font color="', ...
        inColor, ...
        '">', ...
        inText, ...
        '</font></html>'];
    end
    
    function cellChange(obj,eventData)
        if eventData.Indices(2)==1
          mallaStr=eventData.NewData;
         
          dir_rel=calculaDirRel(dir_md_(eventData.Indices(1)),mallas_dic(mallaStr));      
          if (abs(dir_rel)>maxAngulo)
              dir_rel=colText(num2str(dir_rel,format),'red');
          else
              dir_rel=num2str(dir_rel,format);
          end
         dat=get(tblMallas,'Data');
         dat{eventData.Indices(1),5}=dir_rel;
         set(tblMallas,'Data',dat);
        end
  
    end
    
    function closeForm(varargin)       
        close(gcbf);
        pause(0.3)
        close(gcbf);
        pause(0.3)
    end

    function ok(varargin)
        if ~canEdit
            delete(gcbf);
            return;
        end
        dat=get(tblMallas,'Data');
        for j=1:size(dat,1)
            sourceMopla.mallas{j}=dat{j,1};
            sourceMopla.propagate(j)=dat{j,6};
        end
        
        tides=[];
        if strcmp(get(tbLow,'Enable'),'on')
            tides=cat(1,tides,str2double(get(tbLow,'String')));
        end
        if strcmp(get(tbMedium,'Enable'),'on')
            tides=cat(1,tides,str2double(get(tbMedium,'String')));
        end
        if strcmp(get(tbHigh,'Enable'),'on')
            tides=cat(1,tides,str2double(get(tbHigh,'String')));
        end
        sourceMopla.tide=tides;
         
         gamma=cell2mat(get(tblGamma,'Data'));
         sourceMopla.gamma=gamma;
         
         sigma=cell2mat(get(tblSigma,'Data'));
         sourceMopla.sigma=sigma;
         
        %sourceMoplaOut=sourceMopla;
        if ~get(chkCurrents,'Value')
            sourceMopla.coplaTime=-1;
        else
            value=str2num(get(tbCoplaTime,'String'));
            if value>0
                sourceMopla.coplaTime=value;
            end
        end
        %sourceMopla.coplaTime=500; %COPLA Temporal
         hasChanges=true;
        smc.saveMopla(altDir,sourceMopla);        
        pause(0.3)
       
        close(gcbf);
        pause(0.3)
         
    end

    function cancel(varargin)
        close(gcbf);
    end

    function refreshGraph(varargin)
        type=get(cbMdGraph,'Value');
        cla(mdAxes);
        

        hs_md=hs(sourceMopla.casosMD);
        tp_md=tp(sourceMopla.casosMD);         
        dir_md=dir(sourceMopla.casosMD); 
        sourceMopla.hsMin=str2double(get(tbHsMin,'String'));
        sourceMopla.tpMin=str2double(get(tbTpMin,'String'));
        hs_md(hs_md<sourceMopla.hsMin)=sourceMopla.hsMin;
        tp_md(tp_md<sourceMopla.tpMin)=sourceMopla.tpMin;
     

        if type==1
            plot(mdAxes,tp,hs,'k.');
            xlabel(mdAxes,'tp (s)');
            ylabel(mdAxes,'hs (m)');
            hold(mdAxes,'on');           
            plot(mdAxes,tp_md,hs_md,'or','MarkerFaceColor','r','MarkerSize',4);           
            hold(mdAxes,'off');
        elseif type==2
            plot(mdAxes,dir,hs,'k.');
            xlabel(mdAxes,'dir (º)');
            ylabel(mdAxes,'hs (m)');
            hold(mdAxes,'on');          
            plot(mdAxes,dir_md,hs_md,'or','MarkerFaceColor','r','MarkerSize',4);             
            hold(mdAxes,'off');
        else
            plot(mdAxes,dir,tp,'k.');
            xlabel(mdAxes,'dir ()');
            ylabel(mdAxes,'tp (s)');
            hold(mdAxes,'on');          
            plot(mdAxes,dir_md,tp_md,'or','MarkerFaceColor','r','MarkerSize',4);                     
            hold(mdAxes,'off');
        end      
        
        %%%%%Refresco el grid
        if exist('tblMallas','var')
        dat=get(tblMallas,'Data');
        for w=1:length(dat)
            hs_tb=dat{w,2};
            tp_tb=dat{w,3};
            if hs_tb<sourceMopla.hsMin
                dat{w,2}=sourceMopla.hsMin;
            end
            
            if tp_tb<sourceMopla.tpMin
                dat{w,3}=sourceMopla.tpMin;
            end
        end
         set(tblMallas,'Data',dat);
       end
        
    end
   
    function addGamma(varargin)
        if ~canEdit,return;end
        gamma=cell2mat(get(tblGamma,'Data'));
        gamma=cat(1,gamma,[gamma(end,1)+1,3.3]);
        set(tblGamma,'Data',num2cell(gamma));
    end

    function deleteGamma(varargin)
        if ~canEdit,return;end
        gamma=cell2mat(get(tblGamma,'Data'));
        if size(gamma,1)>1
            gamma(end,:)=[];
            set(tblGamma,'Data',num2cell(gamma));
        end
    end

    function addSigma(varargin)
        if ~canEdit,return;end
        sigma=cell2mat(get(tblSigma,'Data'));
        sigma=cat(1,sigma,[sigma(end,1)+1,20]);
        set(tblSigma,'Data',num2cell(sigma));
    end

    function deleteSigma(varargin)
        if ~canEdit,return;end
        sigma=cell2mat(get(tblSigma,'Data'));
        if size(sigma,1)>1
            sigma(end,:)=[];
            set(tblSigma,'Data',num2cell(sigma));
        end
    end

    function pintaJonswap(varargin)
        hs_=mean(sourceMopla.hs);
        tp_=mean(sourceMopla.tp);
        gamma_=str2double(get(tbGamma,'String'));
        [freq,S]=util.jonswap(hs_,tp_,gamma_);        
        fill(freq,S,'r','parent',spGammaAxes);
        xlabel(spGammaAxes,'f (s-1)');
        ylabel(spGammaAxes,'S');
    end

end
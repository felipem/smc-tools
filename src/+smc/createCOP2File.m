function createCOP2File(folder,caso,malla,marea,tiempoTotal)       
    
	if ischar(tiempoTotal)
		tiempoTotal=str2num(tiempoTotal);
	end
    %%% Calculo la profundidad media eliminado los datos >0
    h=malla.h;
    h(h<0)=[];
    h(isnan(h))=[];
    %%% End profundidad media
    
    intervalo=round(malla.dx/(sqrt(9.81*mean(mean(h)))+0.5));
    eddy=round(min(malla.dx/2,malla.dy/2));

    file=fullfile(folder,[malla.nombre,caso,'COP2.DAT']);
    fid=fopen(file,'w');    
    fprintf(fid,['[GENERAL]','\n']);
    fprintf(fid,['Clave=',caso,'\n']);
    fprintf(fid,['Descripcion=','\n']);
    fprintf(fid,['Intervalo=',num2str(intervalo),'\n']);
    fprintf(fid,['EscriturasFichero=1','\n']);
    fprintf(fid,['Iteraciones=',num2str(floor(tiempoTotal/intervalo)),'\n']);
    fprintf(fid,['ComienzoEscritura=1','\n']);
    fprintf(fid,['FechaModificacion=', datestr(clock,'dd/mm/yyyy HH:MM:SS'),'\n']);
    fprintf(fid,['[PARAMETROS]','\n']);
    fprintf(fid,['Chezy=1','\n']);
    fprintf(fid,['Eddy=',num2str(eddy),'\n']);
    fprintf(fid,['Coriolis=0','\n']);
    fprintf(fid,['NumTerminosNoLineales=3','\n']);
    fprintf(fid,['NoLinealidad=1','\n']);
    fprintf(fid,['Inundacion=0','\n']);
    fprintf(fid,['FriccionContorno=0','\n']);
    fprintf(fid,['TiempoTotal=',num2str(tiempoTotal),'\n']);
    fprintf(fid,['Marea=',num2str(marea),'\n']);
    fclose(fid);
end

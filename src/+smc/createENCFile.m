function createENCFile(folder,caso,malla,mallas,fp)    

    mallaPadre=malla;
    hijos={};
    
    while  ~isempty(mallaPadre.parent)
        hijos=cat(1,hijos,mallaPadre.nombre);
        mallaPadre=mallas(mallaPadre.parent);                
    end
    
    file=fullfile(folder,[mallaPadre.nombre,caso,'ENC.DAT']);
    fid=fopen(file,'w');    
    subDivY=floor(fp*malla.dy);
    if subDivY<1, subDivY=1;end
    fprintf(fid,['[GENERAL]','\n']);
    fprintf(fid,['Clave=',caso,'\n']);
    fprintf(fid,['Desc=',caso,'\n']);
    fprintf(fid,['NumMallas=',num2str(length(hijos)+1),'\n']); %Cambiar con el padre
    fprintf(fid,['SubdivisionesYUltima=',num2str(subDivY),'\n']);
    fprintf(fid,['CasoMaestro=',mallaPadre.nombre,caso,'\n']);
    fprintf(fid,['ClaveCopla=',caso,'\n']);
    fprintf(fid,['Zoom=0','\n']);
    fprintf(fid,['ZoomMinX=1','\n']);
    fprintf(fid,['ZoomMinY=1','\n']);
    fprintf(fid,['ZoomMaxX=',num2str(mallaPadre.nx),'\n']);
    fprintf(fid,['ZoomMaxY=',num2str(mallaPadre.ny),'\n']);
    fprintf(fid,['FechaModificacion=', datestr(clock,'dd/mm/yyyy HH:MM:SS'),'\n']);
    fprintf(fid,['[EspectroPuntual]','\n']);
    fprintf(fid,['CalcularEn=00000000000','\n']);
    fprintf(fid,['Tipo=0','\n']);
    fprintf(fid,['[SuperficieLibre]','\n']);
    fprintf(fid,['CalcularEn=00000000000','\n']);
    if ~isempty(hijos)
        fprintf(fid,['[MALLAS]','\n']);
        for i=1:length(hijos)
            hijo=hijos{i};
            fprintf(fid,['Malla',num2str(i),'=',hijo,'\n']);
        end
    end
    fclose(fid);
end


function  createIN2File(folder,caso,malla,mallas,hs,waveDir,fp,h,gamma,w_spr,marea)

    
    waveDir=90-waveDir+180;
    waveDir=atan2(sind(waveDir),cosd(waveDir))*180/pi;
    waveDir=waveDir-malla.angle;
    waveDir=atan2(sind(waveDir),cosd(waveDir))*180/pi;
    
    mallaPadre=malla;
    hijos={};
    while  ~isempty(mallaPadre.parent)
        hijos=cat(1,hijos,mallaPadre.nombre);
        mallaPadre=mallas(mallaPadre.parent);                
    end
    
    file=fullfile(folder,[mallaPadre.nombre,caso,'IN2.DAT']);
    fid=fopen(file,'w');
    fprintf(fid,['[GENERAL]','\n']);
    fprintf(fid,['Desc=','\n']);
    fprintf(fid,['TipoCaso=Espectral','\n']);
    fprintf(fid,['FechaModificacion=', datestr(clock,'dd/mm/yyyy HH:MM:SS'),'\n']);
    fprintf(fid,['[PARAMETROS]','\n']);
    fprintf(fid,['Contornos=Abiertos','\n']);
    fprintf(fid,['Amortiguacion=Turbulenta ','\n']);      
    fprintf(fid,['Linealidad=Compuesto','\n']);
    fprintf(fid,['Periodo=',num2str(1/fp),'\n']);
    fprintf(fid,['Marea=',num2str(marea),'\n']);
    fprintf(fid,['Amplitud=',num2str(hs/2),'\n']);
    fprintf(fid,['Direccion=',num2str(waveDir),'\n']); %TODO
    fprintf(fid,['PendienteMaxFond=0.333333333333333','\n']);
    fprintf(fid,['TipoFiltro=Rangos','\n']);
    fprintf(fid,['[COMPONENTES_ESPECTRO]','\n']);
    fprintf(fid,['Propagar=0','\n']);
    fprintf(fid,['Frecuencia=1','\n']);
    fprintf(fid,['Direccion=1','\n']);
    fprintf(fid,['[ESPECTRO]','\n']);
    fprintf(fid,['TipoEspectro=TMA','\n']);
    fprintf(fid,['Unidades=MKS','\n']);
    fprintf(fid,['ModeloDisipacion=Battjes','\n']);  %Comprobar
    fprintf(fid,['DireccionMedia=',num2str(waveDir),'\n']);
    %%%%%%Debería hacerse con el w_spr, pero entre 5 y 40
    %%%%%%
    fprintf(fid,['DispersionEspectroDireccional=',num2str(w_spr),'\n']);
    fprintf(fid,['NumComponentesDireccionales=15','\n']);
    fprintf(fid,['[ESPECTRO_TMA]','\n']);
    fprintf(fid,['Profundidad=',num2str(h),'\n']);
    fprintf(fid,['FrecuenciaPico=',num2str(fp),'\n']);
    fprintf(fid,['FrecuenciaMaxima=0.4','\n']);
    fprintf(fid,['Hs=',num2str(hs),'\n']);
    fprintf(fid,['Gama=',num2str(gamma),'\n']);
    fprintf(fid,['NumComponentes=10','\n']);
    
    fclose(fid);

end


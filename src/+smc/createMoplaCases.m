function createMoplaCases(altDir,mopla,mallas)
    nTides=length(mopla.tide);
    casosMD=mopla.casosMD(logical(mopla.propagate));
    nCases=length(casosMD);
    tp_gamma=mopla.gamma(:,1);
    tp_sigma=mopla.sigma(:,1);
    
    if nTides*nCases>9999, return;end
    
    calcFile=fullfile(altDir,'Mopla','ScriptCalculo.dat');
    delete(calcFile);
    fid=fopen(calcFile,'w');
    c=1;
    hs_md=mopla.hs(casosMD);
    dir_md=mopla.dir(casosMD);
    tp_md=mopla.tp(casosMD);
    spr_md=mopla.w_spr(casosMD);    
    mallasStr=mopla.mallas(logical(mopla.propagate));
    for i=1:nTides
        tide=mopla.tide(i);
        for j=1:nCases
            hs=hs_md(j);
            tp=tp_md(j);
            if hs<mopla.hsMin, hs=mopla.hsMin;end;
            if tp<mopla.tpMin, tp=mopla.tpMin;end;            
            waveDir=dir_md(j);
            w_spr=spr_md(j);
            idx_gamma=sum(tp_gamma<=tp);
            gamma=mopla.gamma(idx_gamma,2);
            idx_sigma=sum(tp_sigma<=tp);
            sigma=mopla.sigma(idx_sigma,2);
            casoId=mopla.moplaId;
            malla=mallas(mallasStr{j});            
            casoNumber=['000',num2str(c)];
            casoNumber=casoNumber(end-3:end);
            caso=[casoId,casoNumber];
            if mopla.coplaTime>=0 %Si tiene que calcular corrientes!
                smc.createCOP2File(fullfile(altDir,'Mopla'),caso,malla,tide,mopla.coplaTime)
            end
            smc.createENCFile(fullfile(altDir,'Mopla'),caso,malla,mallas,1/tp);
            smc.createIN2File(fullfile(altDir,'Mopla'),caso,malla,mallas,hs,waveDir,1/tp,mopla.z,gamma,sigma,tide);
            fprintf(fid,['Calcular("',caso,'");\n']);
            c=c+1;
        end
    end
    fclose(fid);
end


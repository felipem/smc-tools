function [created,err] = createProyect(folder,name,bat,cartas,lineas,nodosDow,images)
%CREATEPROYECT Summary of this function goes here
%   Detailed explanation goes here
err='';
created=true;

try

    %%%%%%Creo el poyecto smc 3.0
    folder=fullfile(folder,name);
    if ~exist(folder,'dir'),mkdir(folder),end
    mkdir(fullfile(folder,'Batimetrias'));
    mkdir(fullfile(folder,'Costas'));
    mkdir(fullfile(folder,'Imagenes'));
    mkdir(fullfile(folder,'Poligonos'));
    mkdir(fullfile(folder,'Alternativa 1'));

    fid=fopen(fullfile(folder,'Proyecto.smc'),'w');
    fprintf(fid,'[GENERAL]\n');
    fprintf(fid,'VersionSMC=1.0\n');
    fprintf(fid,'VersionProyecto=1.0\n');
    fprintf(fid,'Descripcion=\n');
    fprintf(fid,['Nombre=',name,'\n']);
    fprintf(fid,'CoordenadasBatimetria=Batimetricas\n');
    fprintf(fid,'[PLANOS]\n');
    fprintf(fid,'Item1=Alternativa 1\n');
    fprintf(fid,'[POLIGONOS]\n');
    fprintf(fid,'Item1=batimetriaBase.pol\n');
    if ~isempty(lineas)
        fprintf(fid,'[COSTA]\n');
        itemId=1;

        %%%TODO Castas nauticas

        %%% Lineas de costa
        for i=1:length(lineas)        
            costa=lineas(i);
            fprintf(fid,['Item',num2str(itemId),'=costa',num2str(i),'.cos\n']);
            generateCosFile(costa,itemId);
            itemId=itemId+1;
        end
        %%%% Imagenes
        if exist('images','var')
        for i=1:length(images)        
            image=images(i);            
            fprintf(fid,['Item',num2str(itemId),'=image',num2str(itemId),'.cos:Image\n']);
            generateImageFile(image,itemId);
            itemId=itemId+1;
        end
        end
        

    end

    fclose(fid);
    
    generatePolFile();
    generatePlanoFile(fullfile(folder,'Alternativa 1'));
    generateSmctFile();
catch ex
    created=false;
    err=ex.message;    
end

    function generateCosFile(costa,itemId)
        cosfid=fopen(fullfile(folder,'Costas',['costa',num2str(itemId),'.cos']),'w');
        datfid=fopen(fullfile(folder,'Costas',['costa',num2str(itemId),'.dat']),'w');

        fprintf(cosfid,'[DATOS_COSTA]\n');
        fprintf(cosfid,['Nombre=' , 'costa' ,num2str( itemId),'\n']);
        fprintf(cosfid,'Descripcion=\n');
        fprintf(cosfid,['Fichero=' , 'costa' , num2str(itemId),'.dat\n']);
        fprintf(cosfid,'Color=000080FF\n');
        fprintf(cosfid,'AnchuraLinea=1\n');
        fprintf(cosfid,'PoligonoDeReferencia=batimetriaBase\n');
        fclose(cosfid);
        
        
        fprintf(datfid,[num2str(length(costa.x)),' 0\n']);        
        [x,y] =util.deg2utm(costa.y,costa.x);
        for j=1:length(x)
            fprintf(datfid,[' ', num2str(x(j)),'  ',num2str(y(j)),'\n']);
        end
        fclose(datfid);
        
        
    end

    function generateImageFile(image,itemId)
        I=flipdim(image.I,1);
        file=fullfile(folder,'Imagenes',['image',num2str(itemId),'.png']);
        imwrite(I,file);
        
        latIni=min(image.y);
        latEnd=max(image.y);
  
        lonIni=min(image.x);
        lonEnd=max(image.x);        
        [x,y] =util.deg2utm([latIni,latEnd],[lonIni,lonEnd]);
        
        
        cosfid=fopen(fullfile(folder,'Costas',['image',num2str(itemId),'.cos']),'w');
        fprintf(cosfid,'[DATOS_COSTA]\n');
        fprintf(cosfid,['Nombre=' , 'image' ,num2str( itemId),'\n']);
        fprintf(cosfid,'Descripcion=\n');
        fprintf(cosfid,['Imagen=' , 'image' , num2str(itemId),'.png\n']);        
        fprintf(cosfid,'PoligonoDeReferencia=\n');
        fprintf(cosfid,['ImagenOrigen_X=',num2str(x(1)),'\n']);
        fprintf(cosfid,['ImagenOrigen_Y=',num2str(y(1)),'\n']);
        fprintf(cosfid,['ImagenVectorX_X=',num2str(x(2)-x(1)),'\n']);
        fprintf(cosfid,'ImagenVectorY_X=0\n');
        fprintf(cosfid,'ImagenVectorX_Y=0\n');
        fprintf(cosfid,['ImagenVectorY_Y=',num2str(y(2)-y(1)),'\n']);
        fprintf(cosfid,'UsarCanalAlfa=1\n');
        fprintf(cosfid,'Transparencia=255\n');
        fprintf(cosfid,'MantenerRelacionDeAspecto=1\n');
        fprintf(cosfid,'ParalelaALaBatimetria=1\n');

        fclose(cosfid);
        
        
        
        
        
    end

    function generatePolFile()
         polfid =fopen(fullfile(folder,'Poligonos','batimetriaBase.pol'),'w');
         datfid =fopen(fullfile(folder,'Poligonos','batimetriaBase.dat'),'w');

            fprintf(polfid,'[DATOS_POLIGONO]\n');
            fprintf(polfid,'Nombre=batimetriaBase\n');
            fprintf(polfid,'Descripcion=\n');
            fprintf(polfid,'Fichero=batimetriaBase.dat\n');
            fprintf(polfid,'IncX=10\n');
            fprintf(polfid,'IncY=10\n');
            fprintf(polfid,'TipoBlanqueo=4\n');
            fprintf(polfid,'NivelConstante=-1\n');
            fprintf(polfid,'Inc=10\n');
            fprintf(polfid,'PuntosEnPolilinea=0\n');
            fprintf(polfid,'PuntosSoloEnVertices=0\n');
            fprintf(polfid,'Cerrado=1\n');
            fclose(polfid);
            
            [x,y] = util.deg2utm(bat(:,2),bat(:,1));
            
            fprintf(datfid,'4 1\n');
%             lonIni=min(bat(:,1));
%             latIni=min(bat(:,2));
%             lonEnd=max(bat(:,1));
%             latEnd=max(bat(:,2));
%             
%             [lonB,latB] =util.deg2utm([latIni,latEnd],[lonIni,lonEnd]);

%             fprintf(datfid,[num2str(lonB(1)), ' ',num2str(latB(1)),'\n']);
%             fprintf(datfid,[num2str(lonB(1)), ' ',num2str(latB(2)),'\n']);
%             fprintf(datfid,[num2str(lonB(2)),' ',num2str(latB(2)),'\n']);
%             fprintf(datfid,[num2str(lonB(2)),' ',num2str(latB(1)),'\n']);                       
             fprintf(datfid,[num2str(min(x)), ' ',num2str(min(y)),'\n']);
             fprintf(datfid,[num2str(min(x)), ' ',num2str(max(y)),'\n']);
             fprintf(datfid,[num2str(max(x)),' ',num2str(max(y)),'\n']);
             fprintf(datfid,[num2str(max(x)),' ',num2str(min(y)),'\n']);                       


            
            z=bat(:,3);
            
            for j=1:length(x)            
                fprintf(datfid,[' ',num2str(x(j)),'  ',num2str(y(j)),'  ',num2str(z(j)),'\n']);
            end
            
            fclose(datfid);   
            
    end

    function generatePlanoFile(alternativaFolder)
          lonIni=min(bat(:,1));
            latIni=min(bat(:,2));
            lonEnd=max(bat(:,1));
            latEnd=max(bat(:,2));
            
            [lonB,latB] =util.deg2utm([latIni,latEnd],[lonIni,lonEnd]);
            
             incrX = (lonB(2) - lonB(1)) / 100;
             incrY = (latB(2) - latB(1)) / 100;
             
             planofid=fopen(fullfile(alternativaFolder,'Plano.dat'),'w');             
             
            fprintf(planofid,'[GENERAL]\nTipoFichero=Plano1.0\nDescripcion=\nNombre=Alternativa 1\nNivelTierra=0\nIncrementoX=');
            fprintf(planofid,[num2str(incrX),'\n']);
            fprintf(planofid,['IncrementoY=',num2str(incrY),'\n']);
            fprintf(planofid,'Regenerado=0\nUnidades=metros\nAbreviaturaUnidades=m\nDecimalesUnidades=3\n');
            fprintf(planofid,['MaxX=', num2str(lonB(2)),'\n']);
            fprintf(planofid,['MaxY=' , num2str(latB(2)),'\n']);
            fprintf(planofid,['MinX=' , num2str(lonB(1)),'\n']);
            fprintf(planofid,['MinY=' , num2str(latB(1)),'\n']);

            fprintf(planofid,'[NivelesEdicion]\nEscalaLimitada=38\nNivel0=-100.000000,1,clBlack,0,1,$000489D0,$000489D0,0,1\nNivel1=-90.000000,1,clBlack,0,1,$001491D4,$001491D4,0,1\nNivel2=-80.000000,1,clBlack,0,1,$002599D8,$002599D8,0,1\nNivel3=-70.000000,1,clBlack,0,1,$0035A1DB,$0035A1DB,0,1\nNivel4=-60.000000,1,clBlack,0,1,$0046A9DF,$0046A9DF,0,1\nNivel5=-50.000000,1,clBlack,0,1,$0056B2E3,$0056B2E3,0,1\nNivel6=-40.000000,1,clBlack,0,1,$0067BAE7,$0067BAE7,0,1\nNivel7=-30.000000,1,clBlack,0,1,$0077C2EA,$0077C2EA,0,1\nNivel8=-20.000000,1,clBlack,0,1,$0088CAEE,$0088CAEE,0,1\nNivel9=-10.000000,1,clBlack,0,1,$0099D3F2,$0099D3F2,0,1\n');
            fprintf(planofid,'Nivel10=-9.000000,1,clBlack,0,1,$00A3D7F3,$00A3D7F3,0,1\nNivel11=-8.000000,1,clBlack,0,1,$00ADDBF5,$00ADDBF5,0,1\nNivel12=-7.000000,1,clBlack,0,1,$00B7E0F6,$00B7E0F6,0,1\nNivel13=-6.000000,1,clBlack,0,1,$00C1E4F7,$00C1E4F7,0,1\nNivel14=-5.000000,1,clBlack,0,1,$00CCE9F8,$00CCE9F8,0,1\nNivel15=-4.000000,1,clBlack,0,1,$00D6EDFA,$00D6EDFA,0,1\nNivel16=-3.000000,1,clBlack,0,1,$00E0F1FB,$00E0F1FB,0,1\nNivel17=-2.000000,1,clBlack,0,1,$00EAF6FC,$00EAF6FC,0,1\nNivel18=-1.000000,1,clBlack,0,1,$00F4FAFE,$00F4FAFE,0,1\nNivel19=0.000000,1,clBlack,0,1,clWhite,clWhite,0,1\nNivel20=10.000000,1,clBlack,0,1,$00F9F0F0,$00F9F0F0,0,1\n');
            fprintf(planofid,'Nivel21=20.000000,1,clBlack,0,1,$00F4E1E1,$00F4E1E1,0,1\nNivel22=30.000000,1,clBlack,0,1,$00EED2D2,$00EED2D2,0,1\nNivel23=40.000000,1,clBlack,0,1,$00E9C3C3,$00E9C3C3,0,1\nNivel24=50.000000,1,clBlack,0,1,$00E3B4B4,$00E3B4B4,0,1\nNivel25=60.000000,1,clBlack,0,1,$00DEA5A6,$00DEA5A6,0,1\nNivel26=70.000000,1,clBlack,0,1,$00D89697,$00D89697,0,1\nNivel27=80.000000,1,clBlack,0,1,$00D38788,$00D38788,0,1\nNivel28=90.000000,1,clBlack,0,1,$00CD7879,$00CD7879,0,1\nNivel29=100.000000,1,clBlack,0,1,$00C86A6A,$00C86A6A,0,1\nNivel30=200.000000,1,clBlack,0,1,$00BB6465,$00BB6465,0,1\nNivel31=300.000000,1,clBlack,0,1,$00AE5F60,$00AE5F60,0,1\n');
            fprintf(planofid,'Nivel32=400.000000,1,clBlack,0,1,$00A15A5B,$00A15A5B,0,1\nNivel33=500.000000,1,clBlack,0,1,$00945556,$00945556,0,1\nNivel34=600.000000,1,clBlack,0,1,$00885050,$00885050,0,1\nNivel35=700.000000,1,clBlack,0,1,$007B4B4B,$007B4B4B,0,1\nNivel36=800.000000,1,clBlack,0,1,$006E4646,$006E4646,0,1\nNivel37=900.000000,1,clBlack,0,1,$00614141,$00614141,0,1\n[NivelesRegenerado]\nEscalaLimitada=38\nNivel0=-100.000000,1,clBlack,0,1,$001F54F1,$001F54F1,0,1\nNivel1=-90.000000,1,clBlack,0,1,$002D5FEF,$002D5FEF,0,1\nNivel2=-80.000000,1,clBlack,0,1,$003C6AED,$003C6AED,0,1\nNivel3=-70.000000,1,clBlack,0,1,$004B76EB,$004B76EB,0,1\nNivel4=-60.000000,1,clBlack,0,1,$005A81E9,$005A81E9,0,1\n');
            fprintf(planofid,'Nivel5=-50.000000,1,clBlack,0,1,$00688DE7,$00688DE7,0,1\nNivel6=-40.000000,1,clBlack,0,1,$007798E5,$007798E5,0,1\nNivel7=-30.000000,1,clBlack,0,1,$0086A4E3,$0086A4E3,0,1\nNivel8=-20.000000,1,clBlack,0,1,$0095AFE1,$0095AFE1,0,1\nNivel9=-10.000000,1,clBlack,0,1,$00A4BBDF,$00A4BBDF,0,1\nNivel10=-9.000000,1,clBlack,0,1,$00ADC1E2,$00ADC1E2,0,1\nNivel11=-8.000000,1,clBlack,0,1,$00B6C8E5,$00B6C8E5,0,1\nNivel12=-7.000000,1,clBlack,0,1,$00BFCFE9,$00BFCFE9,0,1\nNivel13=-6.000000,1,clBlack,0,1,$00C8D6EC,$00C8D6EC,0,1\nNivel14=-5.000000,1,clBlack,0,1,$00D1DDEF,$00D1DDEF,0,1\nNivel15=-4.000000,1,clBlack,0,1,$00DAE3F2,$00DAE3F2,0,1\n');
            fprintf(planofid,'Nivel16=-3.000000,1,clBlack,0,1,$00E3EAF5,$00E3EAF5,0,1\nNivel17=-2.000000,1,clBlack,0,1,$00ECF1F9,$00ECF1F9,0,1\nNivel18=-1.000000,1,clBlack,0,1,$00F5F8FC,$00F5F8FC,0,1\nNivel19=0.000000,1,clBlack,0,1,clWhite,clWhite,0,1\nNivel20=10.000000,1,clBlack,0,1,$00FDECEE,$00FDECEE,0,1\nNivel21=20.000000,1,clBlack,0,1,$00FCDADD,$00FCDADD,0,1\nNivel22=30.000000,1,clBlack,0,1,$00FAC7CD,$00FAC7CD,0,1\nNivel23=40.000000,1,clBlack,0,1,$00F9B5BC,$00F9B5BC,0,1\nNivel24=50.000000,1,clBlack,0,1,$00F7A2AB,$00F7A2AB,0,1\nNivel25=60.000000,1,clBlack,0,1,$00F6909A,$00F6909A,0,1\nNivel26=70.000000,1,clBlack,0,1,$00F47D89,$00F47D89,0,1\n');
            fprintf(planofid,'Nivel27=80.000000,1,clBlack,0,1,$00F36B79,$00F36B79,0,1\nNivel28=90.000000,1,clBlack,0,1,$00F15868,$00F15868,0,1\nNivel29=100.000000,1,clBlack,0,1,$00F04657,$00F04657,0,1\nNivel30=200.000000,1,clBlack,0,1,$00DE4050,$00DE4050,0,1\nNivel31=300.000000,1,clBlack,0,1,$00CC3A49,$00CC3A49,0,1\nNivel32=400.000000,1,clBlack,0,1,$00BA3542,$00BA3542,0,1\nNivel33=500.000000,1,clBlack,0,1,$00A82F3B,$00A82F3B,0,1\nNivel34=600.000000,1,clBlack,0,1,$00972A33,$00972A33,0,1\nNivel35=700.000000,1,clBlack,0,1,$0085242C,$0085242C,0,1\nNivel36=800.000000,1,clBlack,0,1,$00731F25,$00731F25,0,1\nNivel37=900.000000,1,clBlack,0,1,$0061191E,$0061191E,0,1\n');
            fprintf(planofid,'[BLNK]\n');
            fprintf(planofid,'Item1=batimetriaBase\n');
            
            
            if ~isempty(lines) || ~isempty(cartas)             
                
                fprintf(planofid,'[COAS]\n');
                itemId = 1;                              

                %%%TODO Cartas
                
                
                %%% Lineas de Costa
                for j=1:length(lineas)                                                        
                    fprintf(planofid,['Item',num2str(itemId),'=costa',num2str(j),'\n']);
                    itemId=itemId+1;
                end
                
                if exist('images','var')
                for j=1:length(images)        
                    image=images(i);            
                    fprintf(planofid,['Item',num2str(itemId),'=image',num2str(itemId),'\n']);                    
                    itemId=itemId+1;
                end
                end
            end
            fclose(planofid);
           

    end

    function generateSmctFile()
        [xDow,yDow] =util.deg2utm(nodosDow.lat,nodosDow.lon);        
        nodosDow.x=xDow;
        nodosDow.y=yDow;
        smcToolsPrj.nodosDow=nodosDow;
        %smcToolsPrj.utmzone=z1;
        
        file=fullfile(folder,'Proyecto.smct');
        save(file,'smcToolsPrj');
        
    end

end


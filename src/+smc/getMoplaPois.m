function alternativas=getMoplaPois(project)
    folder=project.folder;
    alternativas=[];
    for i=1:length(project.alternativas)
        alt=project.alternativas{i};
        moplaFiles=dir(fullfile(folder,alt,'Mopla','*.smct')); 
        alternativa.nombre=alt;
        alternativa.moplas=[];        
        for j=1:length(moplaFiles)
            moplaId=moplaFiles(j).name;
            moplaId=moplaId(1:end-5);
            moplaDir=fullfile(folder,alt,'Mopla');
            altDir=fileparts(moplaDir);
            mopla=smc.loadMopla(altDir,moplaId,{'propagate','mallas','tide','pois','perfiles'});
            nTides=length(mopla.tide);
            nCases=sum(mopla.propagate);
            n=nTides*nCases;
            files=dir(fullfile(moplaDir,'SP',['*',moplaId,'*Height.GRD']));
            status.all=n;
            status.run=length(files);
            mop.nombre=moplaId;
            mop.pois=mopla.pois;
            if ~isfield(mopla,'perfiles')
                mop.perfiles=containers.Map;
            else
                mop.perfiles=mopla.perfiles;
            end
            if status.run>=status.all %TODO:STATUS
                if ~isempty(mopla.pois) || ~isempty(mop.perfiles)                  
                    mop.pois=mopla.pois;
                    alternativa.moplas=cat(1,alternativa.moplas,mop);
                end
                
            end
        end
        if ~isempty(alternativa.moplas)
            alternativas=cat(1,alternativas,alternativa);
        end
    end
end
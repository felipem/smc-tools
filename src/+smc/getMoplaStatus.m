function status=getMoplaStatus(moplaDir,moplaId,mallas)
%     mopla=load(fullfile(moplaDir,[moplaId,'.smct']),'-mat');
%     mopla=mopla.mopla;

    altDir=fileparts(moplaDir);
    mopla=smc.loadMopla(altDir,moplaId,{'propagate','mallas','tide'});
    nTides=length(mopla.tide);
    
    %%Tengo que comprobar si son padres    
    mallasDic=containers.Map;
    nCases=0;
    for i=1:length(mopla.propagate)
       if mopla.propagate(i) 
           if ~isKey(mallas,mopla.mallas{i})
               status=[];
               return;
           end
           malla=mallas(mopla.mallas{i});
           if ~isKey(mallasDic,malla.nombre)
                mallasDic(malla.nombre)='';
           end

           while ~isempty(malla.parent)
               if ~isKey(mallas,malla.parent)
                   status=[];
                   return;
               end
              malla=mallas(malla.parent);          
              if ~isKey(mallasDic,malla.nombre)
                mallasDic(malla.nombre)='';
              end
              nCases=nCases+1;
           end
           nCases=nCases+1;
       end
    end
    
    n=nTides*nCases;
    mallas=keys(mallasDic);
    nFiles=0;
    for i=1:length(mallas)
        nFiles=nFiles+length(dir(fullfile(moplaDir,'SP',[mallas{i},moplaId,'*Height.GRD'])));
    end
    status.all=n;
    status.run=nFiles;
end

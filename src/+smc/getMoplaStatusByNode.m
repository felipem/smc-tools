function status = getMoplaStatusByNode(node)
    nombre=get(node,'Name');
    initIndex=find(nombre=='[');
    endIndex=find(nombre==']');
    splitIndex=find(nombre=='/');
    status.run=str2num(nombre(initIndex+1:splitIndex-1));
    status.all=str2num(nombre(splitIndex+1:endIndex-1));
end


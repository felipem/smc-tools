function isUsed = isMoplaUsed(moplaId,altDir)
    files=dir(fullfile(altDir,'Mopla','*.smct'));    
    isUsed=false;
    
    for i=1:length(files)
        mopla=files(i).name;
        mopla=mopla(1:end-5);
        if strcmp(moplaId,mopla)
            isUsed=true;
            return;
        end
    end
end


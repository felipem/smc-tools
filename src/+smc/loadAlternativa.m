function alternativa=loadAlternativa(folder,name)
    
    alternativa=[];
    alternativa.nombre=name;
    altDir=fullfile(folder,name);
    moplaDir=fullfile(altDir,'Mopla');
    if ~exist(altDir,'dir') || ~exist(moplaDir,'dir')
        return;
    end
    
    
    %%%%Si existe cargo el el fichero smct
    smctFile=fullfile(altDir,'Alternativa.smct');
    alternativa.dowSelectedNodes=[];
    alternativa.dowSelectedNodes=util.initMap(alternativa.dowSelectedNodes);
    if exist(smctFile,'file')
        altPrj=load(smctFile,'-mat');
        altPrj=altPrj.altPrj;
        alternativa.dowSelectedNodes=altPrj.dowSelectedNodes;        
        if isfield(altPrj,'costas')
            alternativa.costas=altPrj.costas;
            alternativa.coastDate=altPrj.coastDate;
        end
        if isfield(altPrj,'xyz')
            alternativa.xyz=altPrj.xyz;
            alternativa.xyzDate=altPrj.xyzDate;
            alternativa.F=altPrj.F;
        end
    end
    
    %%%%Cargo la costa        
    costFile=fullfile(altDir,'Coasts.DXF');
    if ~isfield(alternativa,'costas')
        alternativa.costas=[];
        alternativa.coastDate=datenum(1900,1,1);
    end
    
    
    if exist(costFile,'file')
        coastInfo=dir(costFile);
        coastDate=datenum(coastInfo.date);
        if coastDate>alternativa.coastDate
            alternativa.costas=util.loadDXF(costFile);
            alternativa.coastDate=coastDate;
        end
        
    end
    
    
    %%%%XYZ
    if ~isfield(alternativa,'xyz')        
        alternativa.xyzDate=datenum(1900,1,1);
    end
    xyzFile=fullfile(moplaDir,[name,'.XYZ']);
    if ~exist(xyzFile,'file')
        alternativa=[];
        uiwait(msgbox('Alternativa no regenerada','Error','error'));
        return;
    end
    xyzInfo=dir(xyzFile);
        xyzDate=datenum(xyzInfo.date);
        if xyzDate>alternativa.xyzDate
            alternativa.xyz=load(xyzFile);
            alternativa.xyzDate=xyzDate;
            alternativa.F = TriScatteredInterp(alternativa.xyz(:,1),alternativa.xyz(:,2),alternativa.xyz(:,3));            
        end
    
    
    %%%%Cargo las mallas    
    
    mallasFiles=dir(fullfile(moplaDir,'*REF2.DAT'));
    alternativa.mallas=[];
    alternativa.mallas=util.initMap(alternativa.mallas);
    for i=1:length(mallasFiles)
        malla=smc.loadMalla(fullfile(moplaDir,mallasFiles(i).name));        
        if length(malla.nombre)==10 %Si no no vale para el smcTools
            xMalla=linspace(0,malla.x,malla.nx);
            yMalla=linspace(0,malla.y,malla.ny);
            [xMalla,yMalla]=meshgrid(xMalla,yMalla);
            xMallaRot=xMalla*cosd(malla.angle)-yMalla*sind(malla.angle);
            yMallaRot=yMalla*cosd(malla.angle)+xMalla*sind(malla.angle);
            xMalla=malla.xIni+xMallaRot;
            yMalla=malla.yIni+yMallaRot;
            %%%% Interpolo la batimetria en las mallas
            malla.h=alternativa.F(xMalla,yMalla);        
            alternativa.mallas(malla.nombre)=malla;        
        end
    end
     
    %%%%
    
    
    
end
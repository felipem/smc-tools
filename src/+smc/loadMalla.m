function malla=loadMalla(ref2file)
    
    fid=fopen(ref2file,'r');
    line=fgetl(fid);
    malla.nombre=line;
    fgetl(fid);
    
    line=fgetl(fid);
    xy=regexp(line,' ','split');
    malla.xIni=str2double(xy{2});
    malla.yIni=str2double(xy{3});
    
    line=fgetl(fid);    
    malla.angle=str2double(line);
    
    line=fgetl(fid);
    dimXY=regexp(line,' ','split');
    malla.x=str2double(dimXY{2});
    malla.y=str2double(dimXY{3});
    
    line=fgetl(fid);
    nXY=regexp(line,' ','split');
    malla.nx=str2double(nXY{1});
    malla.ny=str2double(nXY{2});
    
    line=fgetl(fid);
    dXY=regexp(line,' ','split');
    malla.dx=str2double(dXY{2});
    malla.dy=str2double(dXY{3});
    
    fgetl(fid);
    fgetl(fid);
    line=fgetl(fid);
    parent=regexp(line,' ','split');
    if length(parent)>2
        malla.parent=parent{3};
    else
        malla.parent='';
    end
    
    fclose(fid);
end
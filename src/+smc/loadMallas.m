function mallas = loadMallas(folder,name)

    altDir=fullfile(folder,name);
    moplaDir=fullfile(altDir,'Mopla');
    %%%%Cargo las mallas    
    mallasFiles=dir(fullfile(moplaDir,'*REF2.DAT'));
    mallas=[];
    mallas=util.initMap(mallas);
    for i=1:length(mallasFiles)
        malla=smc.loadMalla(fullfile(moplaDir,mallasFiles(i).name));        
        mallas(malla.nombre)=malla;        
    end
end


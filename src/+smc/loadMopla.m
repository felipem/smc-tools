function mopla=loadMopla(altDir,moplaId,fields)
    mopla=[];
    hasChanges=false;
    hasFields=exist('fields','var');
    moplaDir=fullfile(altDir,'Mopla');
    moplaFile=fullfile(moplaDir,[moplaId,'.smct']);
    if exist(moplaFile,'file')       
        if ~exist('fields','var')
            mopla=load(moplaFile,'-mat');      
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%Formato antiguo
            if isfield(mopla,'mopla')
                mopla=mopla.mopla;
                if ~isfield(mopla,'pois')
                    mopla.pois=[];
                    mopla.pois=util.initMap(mopla.pois);
                end        
                if ~strcmp(class(mopla.pois),'containers.Map')
                    mopla.pois=util.initMap(mopla.pois);
                end  
                
                if ~isfield(mopla,'coplaTime')
                    mopla.coplaTime=500; %%%COPLA Temporal
                end
                if ~isfield(mopla,'sigma')
                    mopla.sigma=[0,20]; %%%COPLA Temporal
                end
                if ~isfield(mopla,'tide')
                    mopla.tide=0;
                end
                hasChanges=true;
            end             
            %%%%%% End Formato antiguo
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        else
            mopla=load(moplaFile,'-mat',fields{:}); 
            %%%%%%Formato antiguo
            if isempty(fieldnames(mopla))
                mopla=smc.loadMopla(altDir,moplaId);
                hasChanges=true;  
                hasFields=false;
            end
        end
    end
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% A�ado los campos de postproceso
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if ~isfield(mopla,'pois')
        mopla.pois=[];
        mopla.pois=util.initMap(mopla.pois);
        hasChanges=true;
    end 

    if ~isfield(mopla,'perfiles')
        mopla.perfiles=[];
        mopla.perfiles=util.initMap(mopla.perfiles);
        hasChanges=true;
    elseif isempty(mopla.perfiles)
        mopla.perfiles=util.initMap(mopla.perfiles);
        hasChanges=true;
    else
        %TEMPORAL!! Pendiente por defecto
        perfiles=keys(mopla.perfiles);
        if ~isempty(perfiles)
            p1=mopla.perfiles(perfiles{1});
            if isempty(p1.slope)
                level=data.getLevelTemporalSerie(util.loadDb(),mopla.lat,mopla.lon,mopla.time);         
                for i=1:length(perfiles)
                    p=mopla.perfiles(perfiles{i});
                    if isempty(p.slope)
                        p.slope=post.getProfileSlope(mopla,level,perfiles{i});        
                        p.defaultSlope=p.slope;
                    end
                end
            end
        end        
    end
    
    if ~hasFields &&(~isfield(mopla,'transportModel') || ~isfield(mopla,'kModel') || ~isfield(mopla,'transportPeriod'))
        mopla.transportModel='Cerc';
        mopla.kModel='Valle';
        period.type='MediaAnualTotal';
        dates=datevec(mopla.time);
        i_=dates(:,1)>dates(1,1);
        dates=dates(i_,:);
        period.ini=dates(1,1);
        period.end=dates(end,1);  
        mopla.transportPeriod=period;
        hasChanges=true;
    end 
    
    
         
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Grabo por si hubo cambios
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if hasChanges && ~hasFields
        smc.saveMopla(altDir,mopla);
    end
    
        
    
end


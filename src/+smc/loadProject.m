function project = loadProject(folder)
    project=[];

    smctFile=fullfile(folder,'Proyecto.smct');
    smcFile=fullfile(folder,'Proyecto.smc');
    if ~exist(smcFile,'file')
        smcFile=fullfile(folder,'PROYECTO.SMC');
    end
    
    if ~exist(smctFile,'file')
        msgbox('Proyecto.smct not found','Error','error');
        uiwait
        pause(0.3);
        return;
    end

     smcToolsPrj=load(smctFile,'-mat');
     smcToolsPrj=smcToolsPrj.smcToolsPrj;
     project.nodosDow=smcToolsPrj.nodosDow;
     %project.utmzone=smcToolsPrj.utmzone;

    project.description=[];
    fid=fopen(smcFile);


    line=fgetl(fid);
    while line~=-1
        if ~isempty(strfind(line,'Nombre='))
            project.nombre=line(8:end);
        end
        
        if ~isempty(strfind(line,'Descripcion='))
            project.description=line(13:end);
        end

        if ~isempty(strfind(line,'[PLANOS]'))
            break;
        end

    line=fgetl(fid);
    end

    line=fgetl(fid);
    alternativas={};
    while line~=-1 
        alt=regexp(line,'=','split');
        alternativas=cat(1,alternativas,alt{2});
        line=fgetl(fid);
        if ~isempty(strfind(line,'['))
            break
        end
    end
    fclose(fid);
    project.alternativas=alternativas;
    project.folder=folder;

end


function moplaLauncher(scriptCalc)

    if ~exist('scriptCalc','var')
        scriptCalc='';
    end
    conv=fullfile('bin','MOPLA_Launcher.exe');
    command=['!mono ',conv,' "',scriptCalc,'"'];
    arch=computer('arch');
    if strcmp(arch(1:3),'win')
         command=['!',conv,' "',scriptCalc,'"'];
    end
    eval(command);
end


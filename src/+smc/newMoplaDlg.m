function [moplaId,numCasos,dirMin,dirMax,dateIni,dateEnd]=newMoplaDlg(altDir)
    %%% Cargo el idioma
     txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    moplaId=[];numCasos=[];dirMin=[];dirMax=[];dateIni=[];dateEnd=[];
      textBoxData.tbName='mopla';
      textBoxData.tbN=num2str(150,'%.0f');
      textBoxData.tbDirMin=num2str(0,'%.0f');
      textBoxData.tbDirMax=num2str(360,'%.0f');
      textBoxData.tbDateIni='1948-01-01';
      textBoxData.tbDateEnd='2009-01-01';
      
    scrPos=get(0,'ScreenSize');
    width=400;
    height=225;
    logoPos=[236,28,128,128];
    xLeft=10;    
    xLeftTab=130;
    yPos=150:-25:25;
      
      f = figure( 'MenuBar', 'none', 'Name', txt.pre('wNewMoplaTitle'), 'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@cancel,'WindowStyle','modal');
    set(f, 'resize', 'off');
    pos=get(f,'Position');
    uicontrol(f,'Style','pushbutton','String', txt.pre('wNewMoplaOk'),'Position',[30 10 60 20],'Callback',@ok);    
    uicontrol(f,'Style','pushbutton','String', txt.pre('wNewMoplaCancel'),'Position',[120 10 60 20],'Callback',@cancel);    
    
     pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    pLogo=uipanel(pMain,'units','pixel','position',logoPos);  
    ax=axes('Parent',pLogo,'YTick',[],'XTick',[],'position',[0,0,1,1]);
    imshow(fullfile('icon','emc.gif'),'Parent',ax);
    axis(ax,'image');
    
    uicontrol(pMain,'Style','text','String',txt.pre('wNewMoplaName'),'Position',[xLeft yPos(1) 90 15],'HorizontalAlignment','left');
    tbName=uicontrol(pMain,'Style','edit','String',textBoxData.tbName,'Tag','tbName',...
        'Position',[xLeftTab yPos(1) 70 15],'BackgroundColor','w','Callback',@checkName);
    lUsed=uicontrol(pMain,'Style','text','String','*','Position',[xLeftTab+80 yPos(1) 5 15],'Visible','off');
    
    uicontrol(pMain,'Style','text','String',txt.pre('wNewMoplaNumber'),'Position',[xLeft yPos(2) 100 15],'HorizontalAlignment','left');
    tbN=uicontrol(pMain,'Style','edit','String',textBoxData.tbN,'Tag','tbN',...
        'Position',[xLeftTab yPos(2) 40 15],'BackgroundColor','w','Callback',@checkNCasos);
    
    uicontrol(pMain,'Style','text','String',txt.pre('wNewMoplaDirMin'),'Position',[xLeft yPos(3) 115 15],'HorizontalAlignment','left');
    uicontrol(pMain,'Style','edit','String',textBoxData.tbDirMin,'Tag','tbDirMin',...
        'Position',[xLeftTab yPos(3) 40 15],'BackgroundColor','w','Callback',@checkDir);
    
    uicontrol(pMain,'Style','text','String',txt.pre('wNewMoplaDirMax'),'Position',[xLeft yPos(4) 115 15],'HorizontalAlignment','left');
    uicontrol(pMain,'Style','edit','String',textBoxData.tbDirMax,'Tag','tbDirMax',...
        'Position',[xLeftTab yPos(4) 40 15],'BackgroundColor','w','Callback',@checkDir);
    
    uicontrol(pMain,'Style','text','String',txt.pre('wNewMoplaDateIni'),'Position',[xLeft yPos(5) 100 15],'HorizontalAlignment','left');
    tbDateIni=uicontrol(pMain,'Style','edit','String',textBoxData.tbDateIni,'Tag','tbDateIni',...
        'Position',[xLeftTab yPos(5) 70 15],'BackgroundColor','w','Callback',@checkDate);
    
    uicontrol(pMain,'Style','text','String',txt.pre('wNewMoplaDateEnd'),'Position',[xLeft yPos(6) 90 15],'HorizontalAlignment','left');
    tbDateEnd=uicontrol(pMain,'Style','edit','String',textBoxData.tbDateEnd,'Tag','tbDateEnd',...
        'Position',[xLeftTab yPos(6) 70 15],'BackgroundColor','w','Callback',@checkDate);
    
    checkName();
    uiwait(f);
    
    function ok(varargin)
        if ~strcmp(get(lUsed,'Visible'),'on')
            moplaId=textBoxData.tbName;
            numCasos=round(str2double(textBoxData.tbN));
            dirMin=round(str2double(textBoxData.tbDirMin));
            dirMax=round(str2double(textBoxData.tbDirMax));
            dateIni=datenum(textBoxData.tbDateIni,'yyyy-mm-dd');
            dateEnd=datenum(textBoxData.tbDateEnd,'yyyy-mm-dd');
            delete(f);
            pause(0.2);
        end
    end

    function cancel(varargin) 
        moplaId=[];numCasos=[];dirMin=[];dirMax=[];dateIni=[];dateEnd=[];
        delete(f);
        pause(0.2);
    end

    function checkName(varargin)
        moplaId=get(tbName,'String');
        if smc.isMoplaUsed(moplaId,altDir) || length(moplaId)~=6
            set(lUsed,'Visible','on');
        else
            set(lUsed,'Visible','off');
           textBoxData.tbName=moplaId;
        end
    end

    function checkNCasos(varargin)
        n=round(str2double(get(tbN,'String')));        
        if isnan(n) || n<1 || n>300 
            
        else
            textBoxData.tbN=num2str(n,'%.0f');
        end
        set(tbN,'String',textBoxData.tbN);
    end
    
    function checkDir(varargin)
        tb=varargin{1};
        dir=round(str2double(get(tb,'String')));
        if isnan(dir) || dir<0 || dir>360 
            
        else
            textBoxData.(get(tb,'tag'))=num2str(dir,'%.0f');
        end
        set(tb,'String', textBoxData.(get(tb,'tag')));
    end

    function checkDate(varargin)
        dateIniStr=get(tbDateIni,'String');
        dateEndStr=get(tbDateEnd,'String');
        try
            dateIni=datenum(dateIniStr,'yyyy-mm-dd');
            dateEnd=datenum(dateEndStr,'yyyy-mm-dd');            
            if dateIni<dateEnd
                textBoxData.tbDateIni=dateIniStr;
                textBoxData.tbDateEnd=dateEndStr;
            else
                dateIniStr=textBoxData.tbDateIni;
                dateEndStr=textBoxData.tbDateEnd;
            end
        catch
            dateIniStr=textBoxData.tbDateIni;
            dateEndStr=textBoxData.tbDateEnd;
        end
        set(tbDateIni,'String',dateIniStr);
        set(tbDateEnd,'String',dateEndStr);
    end
%     prompt = {txt.pre('wNewMoplaName'),txt.pre('wNewMoplaNumber'),'dir>=','dir<=',txt.pre('wNewMoplaDateIni'),txt.pre('wNewMoplaDateEnd')};
%     dlg_title =txt.pre('wNewMoplaTitle');
%     num_lines = 1;
%     def = {'mopla','150','0','360','1948-01-01','2009-01-01'};
%     answer = inputdlg(prompt,dlg_title,num_lines,def);
%     pause(0.1);
%     if isempty(answer)
%         moplaId=[];numCasos=[];dirMin=[];dirMax=[];dateIni=[];dateEnd=[];
%         return;
%     end
%     moplaId=answer{1};
%     numCasos=str2double(answer{2});    
%     dirMin=str2double(answer{3});    
%     dirMax=str2double(answer{4});    
%     
%     try
%         dateIni=datenum(answer{5},'yyyy-mm-dd');
%         dateEnd=datenum(answer{6},'yyyy-mm-dd');
%     catch
%         dateIni=datenum(1900,1,1);
%         dateEnd=datenum(2100,1,1);
%     end
%     if smc.isMoplaUsed(moplaId,altDir) || numCasos<1 || numCasos>999 ||  dirMin>=dirMax || length(moplaId)~=6
%         [moplaId,numCasos,dirMin,dirMax]=smc.newMoplaDlg(altDir);
%         return;
%     end       
end
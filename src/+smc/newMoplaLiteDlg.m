function moplaId=newMoplaLiteDlg(altDir)
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    prompt = {txt.pre('wNewMoplaName')};
    dlg_title = txt.pre('wNewMoplaTitle');
    num_lines = 1;
    def = {'mopla'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    pause(0.1);
    if isempty(answer)
        moplaId=[];
        return;
    end
    moplaId=answer{1};
    
    if smc.isMoplaUsed(moplaId,altDir) || length(moplaId)~=6
        moplaId=smc.newMoplaDlg(altDir);
        return;
    end       
end
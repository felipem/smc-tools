function profileForm(l,h,line)
    
    %%% Cargo el idioma
    txt=util.loadLanguage;
    %%%%%%%%%%%%%%%%%%%
    
    scrPos=get(0,'ScreenSize');
    width=800;
    height=500;
    
     f = figure( 'MenuBar', 'none',...
         'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',@ok,'tag','smcWinAlternativaProfile');
    set(f, 'resize', 'off');
   
    pos=get(f,'Position');
    pMain=uipanel(f,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
    uicontrol(f,'Style','pushbutton','String', txt.post('wProfileNewAcept'),'Position',[30 10 60 20],'Callback',@ok);    
    
   
    
    
     polX=[0,l,l(end)];
    polY=[min(h),h,min(h)];
    polSeaX=[0,0,l(end),l(end)];
    polSeaY=[min(h),0,0,min(h)];
    
    axBeach=axes('Parent',pMain);
    sandyBrown=[244 164 96]/255;
    seaGreen=[32 178 170]/255;
    fill(polSeaX,polSeaY,seaGreen,'Parent',axBeach,'EdgeColor','none');
    hold(axBeach,'on');
    fill(polX,polY,sandyBrown,'Parent',axBeach,'EdgeColor','none');
    ylim=get(axBeach,'ylim');
    yLim=ylim(2);
    set(axBeach,'ylim',[min(h),yLim]);
    set(axBeach,'xlim',[0,l(end)]);
    hold(axBeach,'off');
    xlabel(axBeach, 'x (m)');
    ylabel(axBeach, 'h (m)');
    util.setgrid(axBeach,'xy','yx');
    
    
    function ok(varargin)
        if ishandle(line)
            delete(line)
        end
        delete(f);
    end
    
   
end
function saveMopla(altDir,mopla,fields)
    moplaDir=fullfile(altDir,'Mopla');
    if ~exist(moplaDir,'dir')
        mkdir(moplaDir);
    end    
    moplaFile=fullfile(moplaDir,[mopla.moplaId,'.smct']);    
    if ~exist('fields','var')
        fields=fieldnames(mopla);       
        save(moplaFile,'-struct','mopla',fields{:},'-mat');
    else
        save(moplaFile,'-struct','mopla',fields{:},'-mat','-append');
    end
end


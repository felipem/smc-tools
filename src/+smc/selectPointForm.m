function option = selectPointForm(x,y,lat,lon,z)

 option='cancel';   
 %%% Cargo el idioma
 txt=util.loadLanguage;
 %%%%%%%%%%%%%%%%%%%

  scrPos=get(0,'ScreenSize');
  width=400;
  height=225;
  logoPos=[236,28,128,128];
  %xLeft=10;    
  xLeftTab=20;
  yPos=130:-35:25;
 
h=dialog('Visible','off','Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]);
%hpos=get(h,'Position');hpos(3)=300;hpos(4)=220;
%set(h,'Position',hpos);
pos=get(h,'Position');

uicontrol(h,'Style','pushbutton','String',txt.pre('wPointSelect'),'units','pixel','Position',[30 10 60 20],'Callback',{@close,'select'});
uicontrol(h,'Style','pushbutton','String',txt.pre('wPointMove'),'units','pixel','Position',[120 10 60 20],'Callback',{@close,'move'});
uicontrol(h,'Style','pushbutton','String',txt.pre('wPointCancel'),'units','pixel','Position',[210 10 60 20],'Callback',{@close,'cancel'}); 

pMain=uipanel(h,'units','pixel','position',[0 40 pos(3) pos(4)-40]);  
pLogo=uipanel(pMain,'units','pixel','position',logoPos);  
ax=axes('Parent',pLogo,'YTick',[],'XTick',[],'position',[0,0,1,1]);
imshow(fullfile('icon','dow_large.png'),'Parent',ax);
axis(ax,'image');
    
uicontrol(pMain,'Style','text','String',sprintf('x: %.0f m [ %.3f �]',x,lon),'Position',[xLeftTab yPos(1) 160 15],...
    'HorizontalAlignment','left');            
uicontrol(pMain,'Style','text','String',sprintf('y: %.0f m [ %.3f �]',y,lat),'Position',[xLeftTab yPos(2) 160 15],...
    'HorizontalAlignment','left');          
uicontrol(pMain,'Style','text','String',sprintf('z: %.3f m',z),'Position',[xLeftTab yPos(3) 160 15],...
    'HorizontalAlignment','left');          

set(h,'Visible','on');
uiwait(h);

    function close(varargin)
        option=cell2mat(varargin(end));
        delete(h);
        pause(0.1);
    end
end


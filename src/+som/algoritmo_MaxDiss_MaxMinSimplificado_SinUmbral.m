function [subset, ncenters] = algoritmo_MaxDiss_MaxMinSimplificado_SinUmbral (semilla, num, train_n, escalar, direccional)  

%Inicializacion de los centroides
subset=[];
subset=[train_n(semilla,:)];

%Se elimina el centroide seleccionado de los datos de entrenamiento
elementos=1:length(train_n);
dum=find(elementos~=semilla);
train_n2=train_n(dum,:);

%Se repite el proceso hasta tener el numero de centroides deseado
ncenters=1;

qerr=0;
while ncenters<num 

    [m1,n1]=size(train_n2);
    m=ones(m1,1);
    [m2,n2]=size(subset);
    if m2==1 
        xx2=subset(m,:);
        dultima=som.distancia_normalizada(train_n2, xx2, escalar, direccional);
    else
        xx=subset(end,:);
        xx2=xx(m,:);
        danterior=som.distancia_normalizada(train_n2,xx2, escalar, direccional);
        [dultima,ii]=min([danterior dultima],[],2); 
    end
        
    [qerr,bmu]=max(dultima);
    
    if isnan(qerr)==0
        subset=[subset; train_n2(bmu,:)];
        elementos=1:length(train_n2);
        dum=find(elementos~=bmu);
        train_n2=train_n2(dum,:);
        dultima=dultima(dum);
    end
        
    [ncenters,dim]=size(subset);
    
    
%     if num>1000
%         auxi=num/100;
%         if rem(num,ncenters*auxi)==0,
%         disp(['MDA ' num2str(ncenters) ' / ' num2str(num)]);
%         end
%     else
%         disp(['MDA ' num2str(ncenters) ' / ' num2str(num)]);
%     end
    
end
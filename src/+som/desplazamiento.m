function hs4 = desplazamiento (t, cmaxx, fmaxx,hs)

hs=reshape(hs,t,t);

fcentro=ceil(t/2);
ccentro=fcentro;
%resto=rem(fcentro,2);
diferencia=fcentro-fmaxx;
resto2=rem(diferencia,2);

%if resto~=0
%    fcentro=fcentro-1;
%end
if resto2~=0
    fcentro=fcentro-1;
end

%hs2=hs;
hs2=zeros(t,t);
if cmaxx<ccentro
    hs2(:,ccentro:t) = hs(:,cmaxx:cmaxx+(t-ccentro));
    hs2(:,1:ccentro-cmaxx) = hs(:,cmaxx+(t-ccentro)+1:t);
    hs2(:,ccentro-cmaxx+1:ccentro-1) = hs(:,1:cmaxx-1);
else
    if cmaxx>ccentro
        hs2(:,ccentro:ccentro+(t-cmaxx))=hs(:,cmaxx:t);
        hs2(:,ccentro+(t-cmaxx)+1:t)=hs(:,1:cmaxx-ccentro);
        hs2(:,1:ccentro-1)=hs(:,cmaxx-ccentro+1:cmaxx-1);
    else
        %cmaxx==fcentro
        hs2=hs;
    end
end

hs3=zeros(t,t);
if fmaxx<fcentro
    hs3(fcentro:t,:) = hs2(fmaxx:fmaxx+(t-fcentro),:);
    hs3(1:fcentro-fmaxx,:) = hs2(fmaxx+(t-fcentro)+1:t,:);
    hs3(fcentro-fmaxx+1:fcentro-1,:) = hs2(1:fmaxx-1,:);
else
    if fmaxx>fcentro
        hs3(fcentro:fcentro+(t-fmaxx),:)=hs2(fmaxx:t,:);
        hs3(fcentro+(t-fmaxx)+1:t,:)=hs2(1:fmaxx-fcentro,:);
        hs3(1:fcentro-1,:)=hs2(fmaxx-fcentro+1:fmaxx-1,:);
    else
        %fmaxx==fcentro
        hs3=hs2;
    end
end

hs4=reshape(hs3,t*t,1);
%hs4=reshape(hs2,t*t,1);


    
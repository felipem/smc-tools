function [Co]=grafico_direcciones_oleaje_v5 (tt,dato1,dato2,dato3)
                               
t=tt.^0.5;

M=som.normaliza(dato1,[0.3,1]);
Dir=pi/2-dato3*pi/180;
d1=find(Dir(:,1)<-pi);
Dir(d1)=Dir(d1)+2*pi;
U=-M.*cos(Dir);
V=M.*sin(Dir);

%Escala de verdes (10 gamas)
cc=1-copper(11);
%Se eliminan el blanco 
c1=cc(2:11,:);  
c2(:,1)=c1(:,1);
c2(:,2)=c1(:,3);
c2(:,3)=c1(:,2);

dum=find(dato2~=0);
per=dato2(dum);
mmax=max(per);
mmin=min(per);

C1=round(som.normaliza2(dato2,mmax,mmin,[1 10]));
som.som_cplane('hexa',[t t],c2(C1,:));
hold on

cm2=flipud(hot(320));cm2=cm2(1:256,:);
CM2=round(som.normaliza(dato1,[1 256]));
som.som_cplane_2('hexa',[t t],cm2(CM2,:),0.5);

hold on 

Co = som.som_vis_coords('hexa',[t t]);

som.miquiver(Co(:,1),Co(:,2),U(:,1),V(:,1),1,[0 0 0]);




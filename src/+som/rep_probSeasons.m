function probMonth = rep_probSeasons(TimeVec, bmus, tamt, seasons)
probMonth = zeros(tamt,4);
for k = 1 : 4
    aux_= find(TimeVec(k,2)==seasons(k,1) | TimeVec(:,2)==seasons(k,2) | TimeVec(:,2)==seasons(k,3));
    bmus_= bmus(aux_);
    for j = 1 : tamt
        aux = find(bmus_==j);
        if ~isempty(aux)
            probMonth(j,k) = length(aux) / length(aux_)*100;
        end
    end
end
end
function [final2, bmus2, acierto_cien2] = rep_solveSOM3D(amevaVar1, amevaVar2, amevaVarDir, nSom)
%NData =10500;
NData=length(amevaVar1.getData());

% Data necessary for handling SOM toolbox functions.
escalar = [1 2];
direccional = 3;
dim = 3;

% Normalize data
data1 = amevaVar1.getData();
data1=data1(1:NData);


minData1 = min(data1); [maxData1, seed] = max(data1);

data2 = amevaVar2.getData();
data2=data2(1:NData);

minData2 = min(data2); maxData2 = max(data2);

dataDir = amevaVarDir.getData();
dataDir=dataDir(1:NData);

data1Norm = (data1 - minData1) ./ (maxData1 - minData1);
data2Norm = (data2 - minData2) ./ (maxData2 - minData2);
dataDirNorm = dataDir * pi / 180;

dataNorm = [data1Norm, data2Norm, dataDirNorm];




if NData > 10001
    % Data count > 10001. Apply MaxDiss
    [subset, ncenters] = som.algoritmo_MaxDiss_MaxMinSimplificado_SinUmbral(seed, 10000, dataNorm, escalar, direccional);
    sD = som.som_data_struct(subset);
else
    sD = som.som_data_struct(data);
end
lab{1} = [amevaVar1.name ' (' amevaVar1.units  ')'];
lab{2} = [amevaVar2.name ' (' amevaVar2.units  ')'];
lab{3} = [amevaVarDir.name ' (' amevaVarDir.units  ')'];
sD.comp_names=lab;

%Size
sizeSom = nSom^2;

%SOM Calculation
mask = ones(3,1);
sM = som.som_make_modificado_InitEOFs(sD,'msize',[nSom nSom],'algorithm','seq','init','lininit','shape','toroid','training','long','mask',mask,...
    'tipo','euclidiana2','escalares',escalar,'direccionales',direccional);

% bmus calculation for all data
bmus = zeros(NData,1);
m = ones(length(sM.codebook),1);
for i = 1 : NData
    retro = dataNorm(i,:);
    retro2 = retro(m,:);
    [qerr, bmu] = som.som_distancia_seq(sM.codebook, retro2, mask, 'euclidiana2', escalar, direccional);
    bmus(i) = bmu;
end

SM = sM.codebook;

% Denormalized results
maximos = [maxData1, maxData2];
minimos = [minData1, minData2];
final = zeros(sizeSom, dim);
for i = 1 : length(escalar)
    final(:, escalar(i)) = sM.codebook(:, escalar(i)) * (maximos(i)-minimos(i))+minimos(i);
end
for i=1:length(direccional)
    final(:, direccional(i))=sM.codebook(:, direccional(i))*180/pi;
end

% frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto = zeros(sizeSom,1);
for i = 1 : length(sM.codebook)
    acierto(i) = sum(bmus==i);
end
% frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto_cien = acierto / length(bmus) * 100;

% Desplamiento de la SOM para que la celda con la mayor altura de ola quede en el centro
xx = reshape(final(:,1), nSom, nSom);
temp = max(xx);
cmaxx = find(max(temp)==temp);    % columna con la maxima altura de ola
temp2 = xx(:,cmaxx);
fmaxx = find(max(temp2)==temp2);  % fila con la maxima altura de ola

final2(:,1) = som.desplazamiento (nSom, cmaxx, fmaxx, xx);
for i=2:dim
    final2(:,i) = som.desplazamiento (nSom, cmaxx, fmaxx, final(:,i));
end
acierto_cien2 = som.desplazamiento (nSom, cmaxx, fmaxx, acierto_cien);

for i=1:dim
    SM2(:,i) = som.desplazamiento (nSom, cmaxx, fmaxx, SM(:,i));
end

%calculo de los bmus de todos los datos
bmus2 = zeros(NData, 1);
m = ones(length(SM2),1);
for i = 1 : NData
    retro=dataNorm(i,:);
    retro2=retro(m,:);
    [qerr, bmu]=som.som_distancia_seq(SM2, retro2, mask, 'euclidiana2', escalar, direccional);
    bmus2(i) = bmu;
end
end
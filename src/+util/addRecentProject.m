function recentProjects = addRecentProject(recentProjects,project)
    if sum(strcmp(recentProjects,project.folder))
        return;
    end
    recentProjects=cat(1,project.folder,recentProjects);
    if length(recentProjects)>7
        recentProjects(end)=[];
    end
    
end


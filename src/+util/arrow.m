function a=arrow(x,y,u,v,ax,varargin)
    

     headLength=200*sqrt(u.^2 + v.^2);
         

    headWidth = 150 * sqrt(u.^2 + v.^2); % set the headWidth, function of length of arrow    
    a = annotation('arrow','headStyle','cback2','HeadLength',headLength,'HeadWidth',headWidth);
    set(a,'parent',ax);

    set(a,'position',[x y u v]);
    
     for i=1:2:length(varargin)
          set(a,varargin(i),varargin(i+1));
     end
      
     

end
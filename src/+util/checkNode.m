function [isCheck,nodeId] = checkNode(tree,node)
    nodeValue = node.getValue;
    nodeId=nodeValue;
    isCheck=0;
     if isempty(strfind(nodeValue,'chk'))
         return;
     end

     isCheck=logical(str2double(nodeValue(end)));
     isCheck=~isCheck;
     nodeId=nodeValue(5:end-2);     
     if isCheck
         load(fullfile('icon','checked.mat'));
         node.setIcon(im2java(checked.I,checked.map));             
     else
         load(fullfile('icon','unchecked.mat'));
         node.setIcon(im2java(unchecked.I,unchecked.map));             
     end         
     node.setValue([nodeValue(1:end-1),num2str(isCheck)]);
     jtree = handle(tree.getTree,'CallbackProperties'); 
     jtree.treeDidChange();
end


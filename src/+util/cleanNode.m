function cleanNode(node,mtree)
    if get(node,'ChildCount')>0
        nodes=[];
        childNode=get(node,'FirstChild');        
        while ~isempty(childNode)
            nodes=cat(1,nodes,childNode);
            childNode=get(childNode,'NextSibling');
        end
        for i=1:length(nodes)
            nodes(i).removeFromParent;
        end
    end
    mtree.reloadNode(node);

end


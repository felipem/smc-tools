function cleanSiblingNodes(node)     
        nodes=[];
        siblingNode=get(node,'NextSibling');        
        while ~isempty(siblingNode)
            nodes=cat(1,nodes,siblingNode);
            siblingNode=get(siblingNode,'NextSibling');
        end
        for i=1:length(nodes)
            nodes(i).removeFromParent;
        end
    
    %mtree.reloadNode(node);
end
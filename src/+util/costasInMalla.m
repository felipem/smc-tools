function costas = costasInMalla(costas,malla)

x_=[0,malla.x,malla.x,0];
y_=[0,0,malla.y,malla.y];


x=x_*cosd(malla.angle)-y_*sind(malla.angle);
y=x_*sind(malla.angle)+y_*cosd(malla.angle);
x=x+malla.xIni;
y=y+malla.yIni;

costas=util.costasInPoly(costas,x,y);

end


function costas = costasInPoly(costas,x,y)

for k=1:length(costas)
    IN = inpolygon(costas(k).x,costas(k).y,x,y);
    if isempty(IN)
        costas(k).x=[];
        costas(k).y=[];
%     else
%         costas(k).x=costas(k).x(IN);
%         costas(k).y=costas(k).y(IN);
    end
end

end
function createBatGrd(alternativa,mallaId,outFile)
    malla=alternativa.mallas(mallaId);
    
    x=linspace(0,malla.x,malla.nx);
    y=linspace(0,malla.y,malla.ny);    
    
    [xMalla,yMalla]=meshgrid(x,y);
    xMallaRot=xMalla*cosd(malla.angle)-yMalla*sind(malla.angle);
    yMallaRot=yMalla*cosd(malla.angle)+xMalla*sind(malla.angle);
    xMalla=malla.xIni+xMallaRot;
    yMalla=malla.yIni+yMallaRot;    
    z=alternativa.F(xMalla,yMalla);
    fid=fopen(outFile,'w');
    fprintf(fid,'DSAA\n');
    fprintf(fid,'          %d           %d\n',malla.ny,malla.nx);
    fprintf(fid,'%.16f %.16f\n',0,malla.y+malla.dy);
    fprintf(fid,'%.16f %.16f\n',0,malla.x+malla.dx);
    fprintf(fid,'%.16f %.16f\n',min(min(z)),max(max(z)));
    for i=1:malla.ny
        row=flipdim(z(:,i),1);        
        n=floor(numel(row)/16);
        for j=1:n
            for k=1:16                
                fprintf(fid,'%9.4f ',row((j-1)*16+k));
            end
            fprintf(fid,'\n');
        end
        for k=1:numel(row)-n*16
                fprintf(fid,'%9.4f ',row(n*16+k));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
end
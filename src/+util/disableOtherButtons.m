function disableOtherButtons(toolbar,button)
    tools=get(toolbar,'Children');
    for i=1:length(tools)
        if button~=tools(i)
            if strcmp(get(tools(i),'Type'),'uitoggletool')
                 set(tools(i),'State','off');
            end
        end
    end
end
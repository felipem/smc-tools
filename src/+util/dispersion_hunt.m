function [k,L]=dispersion_hunt(h,T)

% aceleraci�n de la gravedad
g=9.81;

% se obtiene el valor de sigma y x
sigma=2.*pi./T;
x=(sigma.*h)./sqrt(g.*h);

% se escriben los valores de d en formato vecorial
d=[0.6666666666 0.3555555555 0.1608465608 0.0632098765 0.0217540484 0.0065407983];

% se realiza un bucle de 6 pasos para realizar el sumatorio
A=0;
% A=0 debido a que en el primer paso del bucle, A se debe a sumar a si
% mismo y por lo tanto tiene que estar definido
for beta=1:6
    A=d(beta).*x.^(2.*beta)+A;
end

% Una vez obtenido el valor del sumatorio, se eval�a el valor de k y de L
k=sqrt(((x.^4)+(x.^2)./(1+A)).*(1./(h.^2)));
L=2.*pi./k;
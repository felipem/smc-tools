function levels = getDemLevels(Z)

minZ=min(min(Z));
maxZ=max(max(Z))-1;

levels=[];
level=maxZ;
while level>50
    levels=cat(1,levels,level);
    level=level/2;
end
levels=cat(1,levels,(50:-2:20)');
levels=cat(1,levels,(19:-1:5)');
levels=cat(1,levels,(4.5:-0.5:-5)');
level=-6;

while level>minZ
    levels=cat(1,levels,level);
    level=level*1.5;
end

if minZ<level
    levels=cat(1,levels,minZ);
end






end



function [I,x,y]=getGMImage(latIni,lonIni,latEnd,lonEnd,mapType,utmZone)


x_Ini=lonIni;
x_End=lonEnd;
y_Ini=latIni;
y_End=latEnd;

%Es UTMs
if ~isempty(utmZone)     
    [lat,lon] = util.utm2deg([lonIni,lonEnd],[latIni,latEnd],repmat(utmZone,2,1));
    latIni=lat(1);latEnd=lat(2);
    lonIni=lon(1);lonEnd=lon(2);
end

%mapType=satellite|raoadmap| hybrid| terrain

latC=(latEnd+latIni)/2;
lonC=(lonEnd+lonIni)/2;

[zoom,width,height]=getGMZoomLevel(latIni,lonIni,latEnd,lonEnd);

url=['http://maps.google.com/maps/api/staticmap?center=',...
    num2str(latC),',',num2str(lonC),'&zoom=',num2str(zoom),...
    '&size=',num2str(width),'x',num2str(height),'&maptype=',mapType,'&sensor=false'];

folders=util.getUserFolders();  %%Para tener permisos
tempFile=fullfile(folders.temp,'temp.png');
try
    urlwrite(url,tempFile);
catch
    I=[];
    x=[];
    y=[];
    return;
end

[I,map]=imread(tempFile);
delete(tempFile);

if ~isempty(map)
    I=ind2rgb(I,map);
end

I=flipdim(I,1);

y=linspace(y_Ini,y_End,height);
x=linspace(x_Ini,x_End,width);


     function [z,width,height]=getGMZoomLevel(latIni,lonIni,latEnd,lonEnd)
    
        zoom_levels=19;
        pixels=256;
        view_size=[640;640];
        pixel_origo=[];
        pixels_per_lon_degree=[];
        pixels_per_lon_radian=[];
        pixel_range=[];
        for z=1:zoom_levels
            origen=pixels/2;
            pixel_origo(z).x=origen;
            pixel_origo(z).y=origen;
            pixels_per_lon_degree(z)=pixels/360;
            pixels_per_lon_radian(z)=pixels / (2 * pi);
            pixel_range(z)=pixels;
            pixels=pixels*2;
        end

            for z=18:-1:0
                [xIni,yIni]=FromLatLngToPixel( latIni,lonIni, z);
                [xEnd,yEnd]=FromLatLngToPixel( latEnd,lonEnd, z);

                 if xIni > xEnd 
                    xIni =xIni- pixel_range(z+1);
                 end

                if abs(xEnd - xIni) <= view_size(1)  && abs(yEnd - yIni) <= view_size(2) 
                    break;
                end
            end

            width=abs(xEnd-xIni);
            height=abs(yEnd-yIni);

            function [x,y]=FromLatLngToPixel( lat,lng, zoom)    
                o = pixel_origo(zoom+1);
                x = round(o.x + lng * pixels_per_lon_degree(zoom+1));
                siny = sin(lat*pi/180);
                if siny<-0.9999, siny=-0.9999;end
                if siny>0.9999, siny=0.9999;end

                y = round(o.y + 0.5 * log((1 + siny) / (1 - siny)) * -pixels_per_lon_radian(zoom+1));

            end

end

end

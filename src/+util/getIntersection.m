function xi = getIntersection(x,y,y0)
    cross=(y(1:end-1)-y0).*(y(2:end)-y0);
    
    xi=[];
    %%%Los cruces son los cambios de signo
    index=find(cross<0);
    
    for i=1:length(index)
       x1=x(index(i));
       x2=x(index(i)+1);
       y1=y(index(i));
       y2=y(index(i)+1);
       xi(i)=(y0-y1)*(x2-x1)/(y2-y1)+x1;
    end
end


function node = getNodeByKey(tree,key)
    node=tree.Root;    
     if strfind(key,'chk_')
         key=key(1:end-1);
     end
     n=length(key);
    
    while ~strncmp(get(node,'Value'),key,n)        
        if isempty(node),return;end
        node=get(node,'NextNode');
    end
end


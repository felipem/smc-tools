function recentProjects = getRecentProjects()
    recentProjects={};
    folders=util.getUserFolders();
    file=fullfile(folders.temp,'projects.txt');
    if exist(file,'file')
        fid=fopen(file,'r');
         while 1
            tline = fgetl(fid);
            if ~ischar(tline), break, end
            recentProjects=cat(1,recentProjects,tline);
        end
        fclose(fid);
    end

end


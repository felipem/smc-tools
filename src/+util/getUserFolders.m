function folders=getUserFolders()
    if ispc
        main=fullfile(getenv('APPDATA'),'smcTools');
    else
        main=fullfile(getenv('HOME'),'smcTools');
    end
    folders.main=main;
    folders.cache=fullfile(main,'cache');
    folders.temp=fullfile(main,'temp');
    
end
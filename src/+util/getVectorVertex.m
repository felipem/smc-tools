function [x,y,u,v]=getVectorVertex(x0,y0,l,angle,refType)
    if ~exist('refType','var')
        refType='origin';
    end
    
    u=l.*cosd(angle);
    v=l.*sind(angle);
    switch refType
        case 'origin'            
            x=x0;
            y=y0;
        case 'center'
            x=x0-u/2;
            y=y0-v/2;
    end
end
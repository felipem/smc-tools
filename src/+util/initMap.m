function  map=initMap(map)
   map=containers.Map;
   if isvalid(map)
       if ~isempty(map)
          delete(map);                
       end
   end
   map=containers.Map;
end


function  initTree( mtree)
        mtree.NodeExpandedCallback=@expandTree;
        mtree.NodeCollapsedCallback=@collapseTree;    
        
    
    function nodes=expandTree(tree,value)
          nodes=[];
        node=get(value,'CurrentNode');
        nodeId=get(node,'Value');
        if strfind(nodeId,'fld')                        
            set(node,'Icon',im2java(imread(fullfile('icon','folder.png'))));
            tree.reloadNode(node);
        end
    end

    function nodes=collapseTree(tree,value)
        nodes=[];
        node=get(value,'CurrentNode');
        nodeId=get(node,'Value');
        if strfind(nodeId,'fld')                        
            set(node,'Icon',im2java(imread(fullfile('icon','folder_closed.png'))));
            tree.reloadNode(node);
        end
    end
   
end


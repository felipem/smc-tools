function zPerfil=interpPerfil(x,y,z,perfil,d)

        xIni=min(perfil.x);xEnd=max(perfil.x);yIni=min(perfil.y);yEnd=max(perfil.y);
        xIni=xIni-d;
        xEnd=xEnd+d;
        yIni=yIni-d;
        yEnd=yEnd+d;

        in=inpolygon(x,y,[xIni,xIni,xEnd,xEnd],[yIni,yEnd,yEnd,yIni]);
        x=x(in);
        y=y(in);
        z=z(in);

        F=TriScatteredInterp(x,y,z);
        zPerfil=F(perfil.x,perfil.y);
end
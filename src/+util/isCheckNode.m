function [isCheck,isCheckNode,nodeId]=isCheckNode(node)
    nodeValue = node.getValue;    
    isCheck=false;
    isCheckNode=true;
    nodeId=nodeValue;
     if isempty(strfind(nodeValue,'chk'))
         isCheckNode=false;
         return;
     else
         nodeId=nodeValue(5:end-2);  
     end
     isCheck=logical(str2double(nodeValue(end)));
     
end
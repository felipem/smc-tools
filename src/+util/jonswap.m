function [f,S]=jonswap(Hs,Tp,gama)
%
% Calcula el espectro Jonswap unidireccional
% segun la aproximacion de Goda (1985)
%

f=1/50:0.001:1/1; % f=(frecuencia m�nima - frecuencia m�xima)

%Frecuencia de pico
fp=1/Tp;

%Parametro gamma
%gama=3.3;

%Parametro alfa
alfa=0.0624/(0.230+0.0336*gama-0.185*(1.9*gama)^-1);
%Parametro sigma
ii=find(f<=fp);
is=find(f>fp);
siggma=ones(size(f));
siggma(ii)=0.07;
siggma(is)=0.09;
%for i=1:length(f)
%   if f<=fp
%      sigma(i)=0.07;
%   else
%      sigma(i)=0.09;
%   end
%end

bup=-(Tp*f-1).^2;
bdown=2*siggma.^2;
b1=bup./bdown;
b=exp(b1);

S=alfa*Hs^2*Tp^-4*f.^-5.*exp(-1.25*(Tp*f).^-4).*gama.^b;

%figure(1)
%plot(f,S)
%trapz(f,S)^0.5*4.004
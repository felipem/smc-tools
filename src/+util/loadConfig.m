function cfg=loadConfig
    file=fullfile('smcTools.cfg');
    fid=fopen(file,'r');
    txt=textscan(fid,'%s %s','Delimiter','=');
    fclose(fid);
    cfg=containers.Map(txt{1},txt{2});
end
function costas = loadDXF(file)

folders=util.getUserFolders();  %%Para tener permisos
tempFile=fullfile(folders.temp,'tempXY.txt');    

conv=fullfile('bin','dxf2xy.exe');
command=['!mono ',conv,' "',file,'" "',tempFile,'"'];
 arch=computer('arch');
 if strcmp(arch(1:3),'win')
     command=['!',conv,' "',file,'" "',tempFile,'"'];
 end
 

eval(command);

costas=[];
if ~exist(tempFile,'file')  
    return;
end
xy=load(tempFile);
delete(tempFile);


if ~isempty(xy)

    for i=xy(1,3):xy(end,3)
        index=xy(:,3)==i;
        x=xy(index,1);
        y=xy(index,2);
        costas(i).x=x;costas(i).y=y;
    end

end


function db = loadDb()
    cfg=util.loadConfig;
    db=load(fullfile('data',cfg('region'),'smcTools.db'),'-mat');
    db=db.db;
end


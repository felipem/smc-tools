function txt=loadLanguage()
    folders=util.getUserFolders;
    txt=load(fullfile(folders.temp,'txt.mat'));
    txt=txt.txt;
end
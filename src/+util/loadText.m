function txtDict = loadText(textType)

cfg=util.loadConfig;
file=fullfile('texts',cfg('language'),[textType,'.txt']);
fid=fopen(file,'r');
txt=textscan(fid,'%s %s','Delimiter','=');
fclose(fid);

txtDict=containers.Map(txt{1},txt{2});


end


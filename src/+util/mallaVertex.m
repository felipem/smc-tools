function [x,y]=mallaVertex(malla)
        xIni=malla.xIni;yIni=malla.yIni;                
        x_=[0,0,malla.x,malla.x];y_=[0,malla.y,malla.y,0];                
        x=xIni+cosd(malla.angle).*x_-sind(malla.angle).*y_;
        y=yIni+sind(malla.angle).*x_+cosd(malla.angle).*y_;                                               
end
function months = months()

 %%% Cargo el idioma
    txt=util.loadLanguage;
 %%%%%%%%%%%%%%%%%%%

months={txt.post('wMapPeriodJan'),txt.post('wMapPeriodFeb'),txt.post('wMapPeriodMar'),txt.post('wMapPeriodApr'),...
    txt.post('wMapPeriodMay'),txt.post('wMapPeriodJun'),txt.post('wMapPeriodJul'),txt.post('wMapPeriodAug'),...
    txt.post('wMapPeriodSep'),txt.post('wMapPeriodOct'),txt.post('wMapPeriodNov'),txt.post('wMapPeriodDec')};

end


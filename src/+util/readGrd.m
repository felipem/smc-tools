function [data,xMalla,yMalla] = readGrd(grdFile,malla)
   
    data=[];
    fid=fopen(grdFile,'r');
    fgetl(fid);
    xy=fscanf(fid,'%d',2);
    fgetl(fid);
    fgetl(fid);
    fgetl(fid);
    fgetl(fid);
    data=fscanf(fid,'%f',[xy(1),xy(2)]);
    fclose(fid);
    data=flipdim(data,1);   
    
    
     xMalla=linspace(0,malla.x,xy(2));
    yMalla=linspace(0,malla.y,xy(1));
    [xMalla,yMalla]=meshgrid(xMalla,yMalla);
    xMallaRot=xMalla*cosd(malla.angle)-yMalla*sind(malla.angle);
    yMallaRot=yMalla*cosd(malla.angle)+xMalla*sind(malla.angle);
    xMalla=malla.xIni+xMallaRot;clc
    yMalla=malla.yIni+yMallaRot;
    
    if isempty(data), error('empty data');end;

end


function data = readGrdPoint(grdFile,malla,x,y)
    data=[];
    x=x-malla.xIni;
    y=y-malla.yIni;
    x_rot=x*cosd(malla.angle)+y*sind(malla.angle);
    y_rot=y*cosd(malla.angle)-x*sind(malla.angle);
    n=round(x_rot/malla.dx)+1;
    m=round(y_rot/malla.dy)+1;
    pos=malla.ny*(n-1) + malla.ny-m+1;
    fid=fopen(grdFile,'r');
    fgetl(fid);
    fgetl(fid);
    fgetl(fid);
    fgetl(fid);
    fgetl(fid);
    fscanf(fid,'%f',pos-1);
    data=fscanf(fid,'%f',1);
    fclose(fid);
 
     if isempty(data), error('empty data');end;
end


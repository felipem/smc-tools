function [x,y,xMalla,yMalla,phase]=readOutFile(outFile,malla)
    
    
     %data=[];
     fid=fopen(outFile,'r');
     yxd=fscanf(fid,'%d',3);
     y=fscanf(fid,'%f',yxd(1));
     x=zeros(yxd(2),1);     
     psi=zeros(yxd(2),1); 
     z=zeros(length(x),length(y));
     a_real=zeros(length(x),length(y));
     a_imag=zeros(length(x),length(y));
     theta=zeros(length(x),length(y));
     
     for i=1:length(x)
         xpsi=fscanf(fid,'%f',2);
         x(i)=xpsi(1);
         psi(i)=xpsi(2);
         z(i,:)=fscanf(fid,'%f',length(y));
         %z(i,:)=fscanf(fid,'%f',length(y));
         %[a,b]=fscanf(fid,'%f',1);
         %fgetl(fid)
         c=fscanf(fid,' (%f,%f)',2*length(y));
         a_real(i,:)=c(1:2:end-1);
         a_imag(i,:)=c(2:2:end);
         if i>1
             theta(i,:)=fscanf(fid,'%f',length(y));
         end
                  
     end
     fclose(fid);
     
%      index=psi>2*pi;
%      psi(index)=rem(psi(index),2*pi);
%      psi(psi>pi)=psi(psi>pi)-2*pi;
     
     
%      [X,Y]=meshgrid(x,y);
     [PSI,~]=meshgrid(psi,y);
     %pcolor(X,Y,z');shading interp;
     
     
     
     a=complex(a_real,a_imag);
     
     %index=a_real==0 && a_imag==0;     
     phase=atan2(a_imag',a_real')+PSI+2*pi;
     index=phase>2*pi;
     phase(index)=rem(phase(index),2*pi);
     index=phase>pi;
     phase(index)=phase(index)-2*pi;
     phase=abs(phase)*180/pi;
     
     index=a_real'==0 & a_imag'==0;
     phase(index)=0;
     %phase=abs(imag(log(a')))*180/pi.*PSI;
     
     
%     xy=fscanf(fid,'%d',2);
%     fgetl(fid);
%     fgetl(fid);
%     fgetl(fid);
%     fgetl(fid);
%     data=fscanf(fid,'%f',[xy(1),xy(2)]);
%     
%     data=flipdim(data,1);       
    
    
    xMalla=linspace(0,malla.x,malla.nx);
    yMalla=linspace(0,malla.y,malla.ny);
    [xMalla,yMalla]=meshgrid(xMalla,yMalla);
    
    xMallaRot=xMalla*cosd(malla.angle)-yMalla*sind(malla.angle);
    yMallaRot=yMalla*cosd(malla.angle)+xMalla*sind(malla.angle);
    x=malla.xIni+xMallaRot;
    y=malla.yIni+yMallaRot;
    
    %phase=rand(malla.ny,malla.nx)*180;
    
    
    
end
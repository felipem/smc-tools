function [X,Y,qb]=readQb(malla,qbFile)
    xFile=regexprep(qbFile,'_qb','__x');
    x=load(xFile);

    fid=fopen(qbFile,'r');
    %%%Leo la primera fila        
    [data,yCount]=fscanf(fid,'%f');
    
    qb=zeros(length(x),yCount);
    qb(1,:)=data;

    for i=2:length(x)
        fscanf(fid,'%s',1); %Quito el #
        data=fscanf(fid,'%f',yCount);
        qb(i,:)=data;
    end
    fclose(fid);
    qb=qb';
    y=linspace(0,malla.y,yCount);
    [X_,Y_]=meshgrid(x,y);
    X=malla.xIni + X_*cosd(malla.angle)-Y_*sind(malla.angle);
    Y=malla.yIni + X_*sind(malla.angle)+Y_*cosd(malla.angle);
end
function saveRecentProjects(recentProjects)
    
    folders=util.getUserFolders();
    file=fullfile(folders.temp,'projects.txt');
    fid=fopen(file,'w');
    for i=1:length(recentProjects)
        str=recentProjects{i};
        str=strrep(str,'\','\\');
        fprintf(fid,[str,'\n']);
    end
    fclose(fid);

end


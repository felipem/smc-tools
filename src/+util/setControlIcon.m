function setControlIcon(uicontrol,iconFile)
    [cdata,map]=imread(fullfile('icon',iconFile));
    map(find(map(:,1)+map(:,2)+map(:,3)==3)) = NaN;
    set(uicontrol,'CData',ind2rgb(cdata,map));
end
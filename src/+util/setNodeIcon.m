function setNodeIcon(node,iconFile)
    %Para windows hay que ponerlo distinto
    %strfind(computer('arch'),'win')
    [isCheck,isCheckNode,nodeId]=util.isCheckNode(node);
    if isCheckNode
        iconFile=fullfile('file:',pwd,'icon',iconFile);
        name=get(node,'Name');
        name=['<html><img src="',iconFile,'"/>&nbsp;',name,'<html>'];
        set(node,'Name',name);
        if isCheck
            load(fullfile('icon','checked.mat'));
            node.setIcon(im2java(checked.I,checked.map));     
        else
            load(fullfile('icon','unchecked.mat'));
            node.setIcon(im2java(unchecked.I,unchecked.map));     
        end
    else
        [I,M]=imread(fullfile('icon',iconFile));
        if ~isempty(M)
            I=ind2rgb(I,M);
        end
        node.Icon=im2java(I);
    end
    
end
function setgrid(ax,varargin)
% setgrid('ax1','ax2',...,'axN')
% to draw gridlines on userdefined tickmarks
%
% axN:	define grid plane
% 'xy' | 1:	X-axis in XY-plane
% 'yx' | 2:	Y-axis in XY-plane
%

% created
% us 07nov00 / CSSM

if nargin < 1
     help setgrid;
     return;
end

% keep layout
     apar={'grid'};
     apar=[apar {'xlim' 'xscale'}];
     apar=[apar {'ylim' 'yscale'}];
     apar=[apar {'zlim' 'zscale'}];
     oax=get(ax,apar);
     agrd=get(ax,'grid');

% cycle through axis-defines
for i=1:length(varargin)
     agrid=varargin{i};
switch	agrid
case {'xy' 1}
     alim=get(ax,'ylim');
     atic=get(ax,'xtick');
     vtic=repmat(alim,length(atic),1);
     line([atic;atic],vtic',...
          'linestyle',agrd,...
          'color',[0 0 0]);
case {'yx' 2}
     alim=get(ax,'xlim');
     atic=get(ax,'ytick');
     vtic=repmat(alim,length(atic),1);
     line(vtic',[atic;atic],...
                'linestyle',agrd,...
                'color',[0 0 0]);
otherwise
     error('invalid axis spec <%s>',...
           char(agrid));
end
end
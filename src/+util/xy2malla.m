function [xMalla,yMalla] = xy2malla(x,y,malla)
    x0=x-malla.xIni;
    y0=y-malla.yIni;
    
    xMalla=x0*cosd(malla.angle)+y0*sind(malla.angle);
    yMalla=-x0*sind(malla.angle)+y0*cosd(malla.angle);
    
end


function smcTools(key)

    feature('DefaultCharacterSet', 'ISO8859-1');
    
    txt.main=util.loadText('main');    
    
    if ~exist('key','var')
        msgbox(txt.main('wSmcToolsLauncher'),'error','error');
        return;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%% Variables Globales
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    global currentForm;
    currentForm=[];            
    %%%%%%% End Variables Globales
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%% Crea las carpetas temporales
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
    folders=util.getUserFolders();
    if ~exist(folders.main,'dir')
        mkdir(folders.main);
        mkdir(folders.cache);
        mkdir(folders.temp);
    end
    %%%%%%% End crear las carpetas temporales
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%% Carga el diccionario con los idiomas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    txt.gis=util.loadText('gis');    
    txt.post=util.loadText('post');    
    txt.ameva=util.loadText('ameva');    
    txt.pre=util.loadText('pre');    
    txt.report=util.loadText('report');    
    tempFile=fullfile(folders.temp,'txt.mat');   
    save(tempFile,'txt');
    %%%%%%% Carga el diccionario con los idiomas
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    warning off
    
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%% Creo el fichero Key
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
   keyFiles=dir(fullfile(folders.main,'*.key'));
   for i=1:length(keyFiles)
       delete(fullfile(folders.main,keyFiles(i).name));
   end
   if exist('key','var')
    keyFile=fullfile(fullfile(folders.main,[key,'.key']));
    fid=fopen(keyFile,'w');
    fclose(fid);
   end
   %%%%%%% End Creo el fichero Key
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    scrPos=get(0,'ScreenSize');
    width=900;
    height=700;    
    
    
    f = figure('MenuBar', 'none', 'Name',txt.main('tMain'), 'NumberTitle', 'off', 'Toolbar', 'none',...
        'Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height],'CloseRequestFcn',...
    @closeForm,'Tag','smcToolsMainForm_1','Renderer','zbuffer');
    %set(f,'Renderer','openGL');
    %util.maxfig(f);
    
    
    tb=uitoolbar(f);
    db=util.loadDb();
    project=[];
    recentProjects=util.getRecentProjects();
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%  Paneles
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    panelM=uipanel(f,'Position',[0.25 0 0.75 1]);    
    %%%%  Paneles
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%  Explorer
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    root=uitreenode('v0','fld_root','SMC Tools',[],false); 
    projectNode=uitreenode('v0','fld_project',[txt.main('exProject'),' []'],[],false);
    amevaNode=uitreenode('v0','fld_ameva','Ameva',[],false);
    amevaDowNode=uitreenode('v0','fld_ameva_dow','DOW',[],false);
    amevaGosNode=uitreenode('v0','fld_ameva_gos','GOS',[],false);
    amevaGotNode=uitreenode('v0','fld_ameva_got','GOT',[],false);
    amevaNode.add(amevaDowNode);
    amevaNode.add(amevaGosNode);
    amevaNode.add(amevaGotNode);
    amevaProjectNode=uitreenode('v0','fld_ameva_project','Project []',[],false);
    amevaNode.add(amevaProjectNode);
    gisNode=uitreenode('v0','fld_gis',txt.main('exData'),[],false);
    util.setNodeIcon(root,'folder_closed.png'); 
    util.setNodeIcon(projectNode,'folder_closed.png'); 
    util.setNodeIcon(amevaNode,'folder_closed.png'); 
    util.setNodeIcon(amevaDowNode,'folder_closed.png'); 
    util.setNodeIcon(amevaGosNode,'folder_closed.png'); 
    util.setNodeIcon(amevaGotNode,'folder_closed.png'); 
    util.setNodeIcon(amevaProjectNode,'folder_closed.png'); 
    util.setNodeIcon(gisNode,'folder_closed.png'); 
    root.add(projectNode);
    root.add(amevaNode);
    root.add(gisNode);    
    pause(0.1);
    mtree = uitree('v0', 'Root', root);
    set(mtree,'Units','normalized')
    set(mtree,'Position',[0 0 0.25 1])
    util.initTree(mtree);   
    jtree = handle(mtree.getTree,'CallbackProperties');
    set(jtree, 'MousePressedCallback', @mousePressedCallback);    
    %%%%  Explorer
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %%%%  ToolBar
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     ptOpenPrj=uipushtool(tb,'ClickedCallback',@openPrj,'TooltipString',txt.main('tbOpenProject'));            
     util.setControlIcon(ptOpenPrj,'folder.gif');   
     
     ptRefreshPrj=uipushtool(tb,'ClickedCallback',@refreshPrj,'TooltipString',txt.main('tbRefreshProject'));                 
     util.setControlIcon(ptRefreshPrj,'refresh.gif');             
     set(ptRefreshPrj,'Enable','off')
     
     util.toolBarSeparator(tb);          
     %%%%End  ToolBar
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
   
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %%%%%%%%Context Menu
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %%%% Gis
     cmData.jmenu=javax.swing.JPopupMenu;
     cmData.open=javax.swing.JMenuItem(txt.main('cmGisOpen'));                        
     set(cmData.open,'ActionPerformedCallback',@openGis);        
     cmData.jmenu.add(cmData.open);
     
     %%%% Ameva dow
     cmAmeva.jmenu=javax.swing.JPopupMenu;
     cmAmeva.open=javax.swing.JMenuItem(txt.main('cmAmevaOpen'));                        
     set(cmAmeva.open,'ActionPerformedCallback',{@openAmeva,false});           
     cmAmeva.openNew=javax.swing.JMenuItem(txt.main('cmAmevaOpenNew'));                        
     set(cmAmeva.openNew,'ActionPerformedCallback',{@openAmevaNew,false});           
     cmAmeva.delete=javax.swing.JMenuItem(txt.main('cmAmevaDelete'));                        
     set(cmAmeva.delete,'ActionPerformedCallback',@deleteAmeva);  
     cmAmeva.jmenu.add(cmAmeva.open);     
     cmAmeva.jmenu.add(cmAmeva.openNew);     
     cmAmeva.jmenu.add(cmAmeva.delete);     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     
     %%%% Reconstruir y limpiar dow
     cmRebuildDow.jmenu=javax.swing.JPopupMenu;
     cmRebuildDow.rebuild=javax.swing.JMenuItem(txt.main('cmAmevaRebuild'));                        
     set(cmRebuildDow.rebuild,'ActionPerformedCallback',@rebuildDow);        
     cmRebuildDow.limpiar=javax.swing.JMenuItem(txt.main('cmAmevaClean'));                        
     set(cmRebuildDow.limpiar,'ActionPerformedCallback',@cleanDow);        
     cmRebuildDow.jmenu.add(cmRebuildDow.rebuild);     
     cmRebuildDow.jmenu.add(cmRebuildDow.limpiar);     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     
     %%%% Ameva  POI
     cmPoi.jmenu=javax.swing.JPopupMenu;
     cmPoi.open=javax.swing.JMenuItem(txt.main('cmAmevaOpen'));                        
     set( cmPoi.open,'ActionPerformedCallback',{@openAmevaPoi,false});        
     cmPoi.jmenu.add(cmPoi.open);     
     cmPoi.openNew=javax.swing.JMenuItem(txt.main('cmAmevaOpenNew'));                        
     set( cmPoi.openNew,'ActionPerformedCallback',{@openAmevaPoi,true});        
     cmPoi.jmenu.add(cmPoi.openNew);     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     %%%% Ameva  Perfil
     cmPerfil.jmenu=javax.swing.JPopupMenu;
     cmPerfil.open=javax.swing.JMenuItem(txt.main('cmAmevaOpen'));                        
     set( cmPerfil.open,'ActionPerformedCallback',{@openAmevaPerfil,false});        
     cmPerfil.jmenu.add(cmPerfil.open);     
     cmPerfil.openNew=javax.swing.JMenuItem(txt.main('cmAmevaOpenNew'));                        
     set( cmPerfil.openNew,'ActionPerformedCallback',{@openAmevaPerfil,true});        
     cmPerfil.jmenu.add(cmPerfil.openNew);     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     %%%% Ameva  Source
     cmAmevaSource.jmenu=javax.swing.JPopupMenu;
     cmAmevaSource.open=javax.swing.JMenuItem(txt.main('cmAmevaOpen'));                        
     set( cmAmevaSource.open,'ActionPerformedCallback',{@openAmevaSource,false});        
     cmAmevaSource.jmenu.add(cmAmevaSource.open);     
     cmAmevaSource.openNew=javax.swing.JMenuItem(txt.main('cmAmevaOpenNew'));                        
     set( cmAmevaSource.openNew,'ActionPerformedCallback',{@openAmevaSource,true});        
     cmAmevaSource.jmenu.add(cmAmevaSource.openNew);     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%varargin
     
     %%%% Reconstruir y limpiar POI
%      cmRebuildPoi.jmenu=javax.swing.JPopupMenu;
%      cmRebuildPoi.rebuild=javax.swing.JMenuItem(txt.main('cmAmevaRebuild'));                             
%      set(cmRebuildPoi.rebuild,'ActionPerformedCallback',@rebuildPoi);             
%      cmRebuildPoi.limpiar=javax.swing.JMenuItem(txt.main('cmAmevaClean'));                             
%      set(cmRebuildPoi.limpiar,'ActionPerformedCallback',@cleanPoi);             
%      cmRebuildPoi.jmenu.add(cmRebuildPoi.rebuild);
%      cmRebuildPoi.jmenu.add(cmRebuildPoi.limpiar);
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     
     %%%% Mopla post
     cmMopla.jmenu=javax.swing.JPopupMenu;
     cmMopla.open=javax.swing.JMenuItem(txt.main('cmPostOpen'));                        
     set(cmMopla.open,'ActionPerformedCallback',@openMopla);        
     cmMopla.jmenu.add(cmMopla.open);     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     
     %%%% Alternativa
     cmAlternativa.jmenu=javax.swing.JPopupMenu;
     cmAlternativa.open=javax.swing.JMenuItem(txt.main('cmPreOpen'));                        
     set(cmAlternativa.open,'ActionPerformedCallback',@openAlternativa);        
     cmAlternativa.jmenu.add(cmAlternativa.open);
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Main Menu
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    mArchivo=uimenu(f,'Label',txt.main('mFile'));
    uimenu(mArchivo,'Label',txt.main('mFileOpen'),'Callback',@openPrj);
    mRefreshPrj=uimenu(mArchivo,'Label',txt.main('mFileRefresh'),'Callback',@refreshPrj,'Enable','off');
    mRecentPrj=uimenu(mArchivo,'Label',txt.main('mFileRecent'));
    mRecentPrjs=[];
    refreshRecentPrjs();
    uimenu(mArchivo,'Label',txt.main('mFileOpenGis'),'Separator','on','Callback',@openGis);
    uimenu(mArchivo,'Label',txt.main('mFileQuit'),'Separator','on','Callback',@closeForm);
    
    mAmeva=uimenu(f,'Label',txt.main('mAmeva'));
    mAmevaDow=uimenu(mAmeva,'Label','Dow','Enable','off');
    mAmevaGos=uimenu(mAmeva,'Label','Gos','Enable','off');
    mAmevaGot=uimenu(mAmeva,'Label','Got','Enable','off');
    uimenu(mAmeva,'Label',txt.main('mAmevaClean'),'Separator','on','Callback',@cleanDow);
    
    mAmevaDows=[];    
    mAmevaGoss=[];    
    mAmevaGots=[];    
    %uimenu(mAmevaDow,'Label','');    
    
    mPreproceso=uimenu(f,'Label',txt.main('mPre'));
    mAlternativas=[];
    
    mPostproceso=uimenu(f,'Label',txt.main('mPost'));
    mAlternativasPost=[];
    mMoplas=[];
    
    mHerramientas=uimenu(f,'Label',txt.main('mTools'));
    %mGraficoFase=uimenu(mHerramientas,'Label',txt.main('mPhaseGraphTool'),'Callback',@graficoFase,'Enable','off');
    uimenu(mHerramientas,'Label','Mopla Launcher','Callback',@moplaLauncher);
    
    mAyuda=uimenu(f,'Label',txt.main('mHelp'));
    uimenu(mAyuda,'Label',txt.main('mHelpAbout'),'Callback',@core.acercaDeForm);    
    
    %%%Maximize figure
    pause(0.05);
    jFrame = get(handle(f),'JavaFrame');
    jFrame.setMaximized(true);
    %%%   Main Menu
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function enabled=isEnabled
        tag=get(f,'Tag');
        enabled=logical(str2double(tag(end)));
    end
    
   function mousePressedCallback(jtree,eventData)
       if ~isEnabled,return;end
             % Get the clicked node
             clickX = eventData.getX;
             clickY = eventData.getY;
             treePath = jtree.getPathForLocation(clickX, clickY);
              if ~isempty(treePath)
                 if eventData.isMetaDown
                    % check if the checkbox was clicked
                    node = treePath.getLastPathComponent;
                    if ~isempty(strfind(node.getValue,'ameva_root'));
                         cmAmeva.jmenu.show(jtree, clickX, clickY);
                    end
                    if ~isempty(strfind(node.getValue,'fld_gis'));
                         cmData.jmenu.show(jtree, clickX, clickY);
                    end
                    
                    if ~isempty(strfind(node.getValue,'edit_alt_'));
                         cmAlternativa.jmenu.show(jtree, clickX, clickY);
                    end
                    if ~isempty(strfind(node.getValue,'fld_ameva_dow'));
                         cmRebuildDow.jmenu.show(jtree, clickX, clickY);
                    end
                    
                    if ~isempty(strfind(node.getValue,'moplaView_'));
                        status=smc.getMoplaStatusByNode(node);
                        if status.run>=status.all  %TODO:STATUS
                          cmMopla.jmenu.show(jtree, clickX, clickY);
                        end
                    end                    
%                     if ~isempty(strfind(node.getValue,'moplaAmeva_'));
%                          cmRebuildPoi.jmenu.show(jtree, clickX, clickY);
%                     end
                    if ~isempty(strfind(node.getValue,'moplaSource_'));
                         cmAmevaSource.jmenu.show(jtree, clickX, clickY);
                    end
                    if ~isempty(strfind(node.getValue,'moplaPoi_'));
                         cmPoi.jmenu.show(jtree, clickX, clickY);
                    end
                    if ~isempty(strfind(node.getValue,'moplaPerfil_'));
                         cmPerfil.jmenu.show(jtree, clickX, clickY);
                    end
                 end
                 
              end
              
              if ~isempty(currentForm)
                  currentForm.mousePressedCallback(jtree,eventData);
              end
              
              
   end

    %%% End  Events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Tools
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    function graficoFase(varargin)
        if ~isempty(project)
            post.phaseGraphForm(project)
        end
    end

    function moplaLauncher(varargin)
        smc.moplaLauncher;
    end
    %%%   End Tools
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function openGis(varargin)
        core.blockMainForm;
        try 
        if ~isempty(currentForm)
            delete(currentForm);
            currentForm=[];
       end
         currentForm=gis.GisForm(db,panelM,tb,mtree,gisNode);
         addlistener(currentForm,'ChangeMenu',@refreshMenu);
        catch ex
            msgbox(ex.message,'error','error');
        end
         core.blockMainForm;
    end

    function openAmeva(varargin)       
       jtree = handle(mtree.getTree,'CallbackProperties'); 
       treePath=jtree.getSelectionPath();
       node = treePath.getLastPathComponent;
       nodeId=node.getValue();
       latLon=regexp(nodeId(15:end),'_','split');
       lat=str2double(latLon{1});
       lon=str2double(latLon{2});       
       %amevaData=ameva.loadAmevaData(fullfile('data','gow_1.mat'));
       
       type=nodeId(11:13);
       if strcmp(type,'dow')
        openAmevaDow([],[],lat,lon);
       elseif strcmp(type,'gos')
           openAmevaGos([],[],lat,lon);
       elseif strcmp(type,'got')
           openAmevaGot([],[],lat,lon);
       end
       
    end

    function openAmevaNew(varargin)       
        jtree = handle(mtree.getTree,'CallbackProperties'); 
       treePath=jtree.getSelectionPath();
       node = treePath.getLastPathComponent;
       nodeId=node.getValue();
       latLon=regexp(nodeId(15:end),'_','split');
       lat=str2double(latLon{1});
       lon=str2double(latLon{2});       
       %amevaData=ameva.loadAmevaData(fullfile('data','gow_1.mat'));
       
       type=nodeId(11:13);
       if strcmp(type,'dow')
        openAmevaDow([],[],lat,lon,true);
       elseif strcmp(type,'gos')
           openAmevaGos([],[],lat,lon,true);
       elseif strcmp(type,'got')
           openAmevaGot([],[],lat,lon,true);
       end
     end

    function openAlternativa(varargin)
       jtree = handle(mtree.getTree,'CallbackProperties'); 
       treePath=jtree.getSelectionPath();
       node = treePath.getLastPathComponent;
       
        core.blockMainForm;
       try 
       if ~isempty(currentForm)
            delete(currentForm);
            currentForm=[];
       end
       currentForm=smc.AlternativaForm(db,panelM,project,tb,mtree,node);
       catch ex
           msgbox(ex.message,'error','error');
       end
       core.blockMainForm;
       
       
%        param={db,panelM,project,tb,mtree,node};
%        formExpresion='smc.AlternativaForm(param{1},param{2},param{3},param{4},param{5},param{6});';
%        core.createForm(formExpresion,param);
       addlistener(currentForm,'ChangeMenu',@refreshMenu);
    end

    function deleteAmeva(varargin)
        jtree = handle(mtree.getTree,'CallbackProperties'); 
        treePath=jtree.getSelectionPath();
        node = treePath.getLastPathComponent;
        parent=node.getParent;
        node.removeFromParent();
        mtree.reloadNode(parent);   
        refreshMenu;
    end

    function closeForm(varargin)
        if ~isempty(currentForm)
        delete(currentForm);
        currentForm=[];
        end
        delete(f)
       clear classes;
       clear all;
    end

    function openPrj(varargin)
        util.disableOtherButtons(tb,ptOpenPrj)
        dir=uigetdir;
        if ~isempty(dir) && ischar(dir)
            
            openPrjFromFolder(dir);
            
        end
        
    end

    function openPrjFromFolder(varargin)
        folder=cell2mat(varargin(end));
        project=core.refreshProject(mtree,folder);
        if ~isempty(currentForm)
               delete(currentForm);
               currentForm=[];
        end
        if ~isempty(project)
            recentProjects=util.addRecentProject(recentProjects,project);
            refreshRecentPrjs();
            util.saveRecentProjects(recentProjects);
            set(ptRefreshPrj,'Enable','on');
            set(mRefreshPrj,'Enable','on');
            %set(mGraficoFase,'Enable','on');

        else
            index=strcmp(recentProjects,folder);
            recentProjects(index)=[];
            refreshRecentPrjs();
        end
        refreshMenu;
    end

    function refreshRecentPrjs()
        for i=1:length(mRecentPrjs)
            delete(mRecentPrjs(i));
        end
        mRecentPrjs=[];
        for i=1:length(recentProjects)
            mRecent=uimenu(mRecentPrj,'Label',recentProjects{i},'Callback',{@openPrjFromFolder,recentProjects{i}});
            mRecentPrjs=cat(1,mRecentPrjs,mRecent);
        end
        
    end

    function openAlternativaMenu(obj,obj2,altId)
       node=util.getNodeByKey(mtree,['edit_alt_',altId]);
        core.blockMainForm;
       try 
       if ~isempty(currentForm)
            delete(currentForm);
            currentForm=[];
       end
       currentForm=smc.AlternativaForm(db,panelM,project,tb,mtree,node);
       catch ex
           msgbox(ex.message,'error','error');
       end
       core.blockMainForm;
       addlistener(currentForm,'ChangeMenu',@refreshMenu);
        
%        core.blockMainForm;
%        param={db,panelM,project,tb,mtree,node};
%        formExpresion='smc.AlternativaForm(param{1},param{2},param{3},param{4},param{5},param{6});';
%        core.createForm(formExpresion,param);
    end

    function openMoplaMenu(obj,obj2,alt,moplaId,node)
       core.blockMainForm;
       try 
       if ~isempty(currentForm)
            delete(currentForm);
            currentForm=[];
       end
       currentForm=post.PostprocesoForm(panelM,project,moplaId,alt,tb,mtree,node);
       catch ex
           msgbox(ex.message,'error','error');
       end
       core.blockMainForm;
    end

    function refreshPrj(varargin)
         util.disableOtherButtons(tb,ptRefreshPrj)
        core.refreshProjectExplorer(mtree,project, currentForm);
        core.refreshAmevaExplorer(mtree,project, currentForm);  
        
        %%%Refrescar el menu 
        refreshMenu;
    end

    function rebuildDow(varargin)
        dowNode=util.getNodeByKey(mtree,'fld_ameva_dow');
        nchilds=get(dowNode,'ChildCount');
        for i=0:nchilds-1
            node=dowNode.getChildAt(i);
            nodeId=node.getValue();
            latLon=regexp(nodeId(15:end),'_','split');
            lat=str2double(latLon{1});
            lon=str2double(latLon{2});
            data.getDOWTemporalSerie(db,lat,lon);
        end
    end

    function cleanDow(varargin)
        files=dir(fullfile(folders.cache,'*.dow'));
        for i=1:length(files)
            file=fullfile(folders.cache,files(i).name);
            delete(file);
        end
    end

    function openMopla(varargin)
       core.blockMainForm;
       try          
           jtree = handle(mtree.getTree,'CallbackProperties'); 
           treePath=jtree.getSelectionPath();
           node = treePath.getLastPathComponent;
           nodeId=node.getValue();
           moplaAlt=regexp(nodeId(11:end),'_','split');
           alt=moplaAlt{1};
           moplaId=moplaAlt{2};
           if ~isempty(currentForm)
                delete(currentForm);
                currentForm=[];
           end      
           currentForm=post.PostprocesoForm(panelM,project,moplaId,alt,tb,mtree,node);
           addlistener(currentForm,'ChangeMenu',@refreshMenu);
       catch ex
           msgbox(ex.message,'error','error');
       end
       core.blockMainForm;       
    end

    function openAmevaSource(varargin)
        core.blockMainForm;
         try
         isNew=cell2mat(varargin(end));  
       
        jtree = handle(mtree.getTree,'CallbackProperties'); 
        treePath=jtree.getSelectionPath();
        sourceNode = treePath.getLastPathComponent;
        moplaNode=get(sourceNode,'Parent');
        nodeId=moplaNode.getValue();
        valor=nodeId(12:end);
        dataStr=regexp(valor,'_','split');
        altName=dataStr{1};
        moplaId=dataStr{2};
        mopla=smc.loadMopla(fullfile(project.folder,altName),moplaId);
        info.type='Mopla';
        info.nombre=[moplaId, ' [Mopla Source]]'];
        info.lat=mopla.y;
        info.lon=mopla.x;
        info.z=mopla.z;
        amevaData=ameva.loadAmevaDataFromMopla(mopla);
        pause(0.2);
        
        
        
        if isNew
            openAmevaNewWindow(sourceNode,info,amevaData)           
        else
        if ~isempty(currentForm)
            delete(currentForm);
            currentForm=[];
        end
            currentForm=ameva.AmevaForm(amevaData,panelM,tb,mtree,sourceNode,info);
        end
        
         catch ex
             msgbox(ex.message,'error','error');
         end
         core.blockMainForm;
        
    end

    function openAmevaPoi(varargin)
       core.blockMainForm;
       try
       isNew=cell2mat(varargin(end));  
        
       jtree = handle(mtree.getTree,'CallbackProperties'); 
       treePath=jtree.getSelectionPath();
       poiNode = treePath.getLastPathComponent;
       nodeId=poiNode.getValue();
       valor=nodeId(10:end);
       dataStr=regexp(valor,'_','split');
       altName=dataStr{1};
       moplaId=dataStr{2};
       poiId=dataStr{3};
       mopla=smc.loadMopla(fullfile(project.folder,altName),moplaId);
       alternativa=smc.loadAlternativa(project.folder,altName);
       amevaData = ameva.loadAmevaDataFromPoi(mopla,poiId,project,alternativa);   
       
       if isempty(amevaData),return;end       
       pause(0.2);    
       
       poi=mopla.pois(poiId);
       info.type='POI';
       info.nombre=poiId;
       info.lat=poi.y;
       info.lon=poi.x;
       info.z=poi.z;
       if isNew
           openAmevaNewWindow(poiNode,info,amevaData)           
       else
           if ~isempty(currentForm)
            delete(currentForm);
            currentForm=[];
           end
           currentForm=ameva.AmevaForm(amevaData,panelM,tb,mtree,poiNode,info);
       end
       
       catch ex
           msgbox(ex.message,'error','error');
       end
       core.blockMainForm;
       
 
%        answer=questdlg(txt.main('qCIQuestion'));
%        pause(0.2);
%        if strcmp(answer,'Yes')           
%                  
%            answer=inputdlg({txt.main('qCISlope'),txt.main('qCIReference')},txt.main('qCITitle'),1,{'0.1','0'});
%            pause(0.2);
%            if ~isempty(answer)
%            
%                 slope=str2double(answer{1});                 
%                 reference=str2double(answer{2}); 
%                 hs=amevaData('hs').getData;
%                 tp=mopla.tp;
% 
%                 if slope>=0.1
%                     runUp=1.98*0.47*sqrt(hs.*(1.56*tp.^2))*slope;
%                 else
%                     runUp=1.98*0.04*sqrt(hs.*(1.56*tp.^2));
%                 end
% 
%                 ci=reference+tide+eta+runUp;                
%                 ciVar=ameva.AmevaVar('ci','m',ci,amevaData('hs').getTime);amevaData('ci')=ciVar;            
%            end
%             
%         end
    end

    function openAmevaPerfil(varargin)
        core.blockMainForm;
       try
       isNew=cell2mat(varargin(end)); 
       
       jtree = handle(mtree.getTree,'CallbackProperties'); 
       treePath=jtree.getSelectionPath();
       perfilNode = treePath.getLastPathComponent;
       nodeId=perfilNode.getValue();
       valor=nodeId(13:end);
       dataStr=regexp(valor,'_','split');
       altName=dataStr{1};
       moplaId=dataStr{2};
       perfilId=dataStr{3};       
       mopla=smc.loadMopla(fullfile(project.folder,altName),moplaId,...
           {'tp','time','lat','lon','transportModel','kModel','perfiles'});
       moplaFolder=fullfile(project.folder,altName,'Mopla',moplaId);    
       amevaData = ameva.loadAmevaDataFromPerfil(moplaFolder,mopla,perfilId);        
       pause(0.2);
       info.type='POI';
       info.nombre=perfilId;
       info.lat=[];%poi.y;
       info.lon=[];%poi.x;
       info.z=[];%poi.z;
       
       if isNew
           openAmevaNewWindow(perfilNode,info,amevaData)           
       else
           if ~isempty(currentForm)
            delete(currentForm);
            currentForm=[];
           end
            currentForm=ameva.AmevaForm(amevaData,panelM,tb,mtree,perfilNode,info);
       end
       
       catch ex
           msgbox(ex.message,'error','error');
       end
       core.blockMainForm;
    end

    function refreshMenu(varargin)
        %%%DOW
        for i=1:length(mAmevaDows)
            delete(mAmevaDows(i));
        end        
        mAmevaDows=[];
        nchilds=get(amevaDowNode,'ChildCount');
        for i=0:nchilds-1
            node=amevaDowNode.getChildAt(i);
            nodeId=node.getValue();
            latLon=regexp(nodeId(15:end),'_','split');            
            lat=str2double(latLon{1});
            lon=str2double(latLon{2});
            menu=uimenu(mAmevaDow,'Label',nodeId(15:end),'Callback',{@openAmevaDow,lat,lon});
            mAmevaDows=cat(1,mAmevaDows,menu);
        end
        if nchilds>0
            set(mAmevaDow,'Enable','on');
        else
            set(mAmevaDow,'Enable','off');
        end
        
        %%%GOS
        for i=1:length(mAmevaGoss)
            delete(mAmevaGoss(i));
        end        
        mAmevaGoss=[];
        nchilds=get(amevaGosNode,'ChildCount');
        for i=0:nchilds-1
            node=amevaGosNode.getChildAt(i);
            nodeId=node.getValue();
            latLon=regexp(nodeId(15:end),'_','split');            
            lat=str2double(latLon{1});
            lon=str2double(latLon{2});
            menu=uimenu(mAmevaGos,'Label',nodeId(15:end),'Callback',{@openAmevaGos,lat,lon});
            mAmevaGoss=cat(1,mAmevaGoss,menu);
        end
        if nchilds>0
            set(mAmevaGos,'Enable','on');
        else
            set(mAmevaGos,'Enable','off');
        end
        
        %%%GOT
        for i=1:length(mAmevaGots)
            delete(mAmevaGots(i));
        end        
        mAmevaGots=[];
        nchilds=get(amevaGotNode,'ChildCount');
        for i=0:nchilds-1
            node=amevaGotNode.getChildAt(i);
            nodeId=node.getValue();
            latLon=regexp(nodeId(15:end),'_','split');            
            lat=str2double(latLon{1});
            lon=str2double(latLon{2});
            menu=uimenu(mAmevaGot,'Label',nodeId(15:end),'Callback',{@openAmevaGot,lat,lon});
            mAmevaGots=cat(1,mAmevaGots,menu);
        end
        if nchilds>0
            set(mAmevaGot,'Enable','on');
        else
            set(mAmevaGot,'Enable','off');
        end
        
        %%%%%% ALTERNATIVAS Y POST PROCESO
        
        
         %%% Recargo las alternativas
        for i=1:length(mMoplas)
            delete(mMoplas(i));
        end
        for i=1:length(mAlternativas)
            delete(mAlternativas(i));            
            delete(mAlternativasPost(i));            
        end        
        
      
        mAlternativas=[];
        mAlternativasPost=[];
        mMoplas=[];
        nchilds=get(projectNode,'ChildCount');
        for i=0:1:nchilds-1
            node=projectNode.getChildAt(i);            
            nodeId=node.getValue;
            menu=uimenu(mPreproceso,'Label',nodeId(9:end),'Callback',{@openAlternativaMenu,nodeId(9:end)});
            menuPost=uimenu(mPostproceso,'Label',nodeId(9:end));
            mAlternativas=cat(1, mAlternativas,menu);
            mAlternativasPost=cat(1, mAlternativasPost,menuPost);            
            alt=nodeId(9:end);
            moplaAltNode=util.getNodeByKey(mtree,['mopla_alt_',alt]);   
            nmoplas=get(moplaAltNode,'ChildCount');
            for j=0:nmoplas-1
                node=moplaAltNode.getChildAt(j); 
                status=smc.getMoplaStatusByNode(node);
                if status.run==status.all
                    nodeId=node.getValue;
                    moplaAlt=regexp(nodeId(11:end),'_','split');                
                    moplaId=moplaAlt{2};                
                    menu=uimenu(mAlternativasPost(i+1),'Label',moplaId,'Callback',{@openMoplaMenu,alt,moplaId,node});
                    mMoplas=cat(1,mMoplas,menu);
                end
            end
        end
        
    end

    function openAmevaDow(obj,obj2,lat,lon,new)
        core.blockMainForm;
        try
        if ~exist('new','var')
            new=false;
        end
        
        info.type='DOW';
        info.lat=lat;
        info.lon=lon;
        info.z=NaN;
       
        amevaData=ameva.loadAmevaDataFromLatLon(db,lat,lon,[]);
        node=util.getNodeByKey(mtree,['ameva_rootdow_',num2str(lat),'_',num2str(lon)]);
        
        if new
            openAmevaNewWindow(node,info,amevaData)
        else
            if ~isempty(currentForm)
                delete(currentForm);
                currentForm=[];
            end        
            currentForm=ameva.AmevaForm(amevaData,panelM,tb,mtree,node,info);
        end
         catch ex
            msgbox(ex.message,'error','error');
        end
        core.blockMainForm;
    end

    function openAmevaGos(obj,obj2,lat,lon,new)
        core.blockMainForm;
        try
        if ~exist('new','var')
            new=false;
        end
       
        info.type='GOS';
        info.lat=lat;
        info.lon=lon;
        info.z=NaN;
        
        [time,eta]=data.getGOSTemporalSerie(db,lat,lon);            
        amevaData=containers.Map;
        
        etaVar=ameva.AmevaVar('eta','m',eta,time);
        amevaData('eta')=etaVar;        
        node=util.getNodeByKey(mtree,['ameva_rootgos_',num2str(lat),'_',num2str(lon)]);
        
        if new
            openAmevaNewWindow(node,info,amevaData)
        else
            if ~isempty(currentForm)
                delete(currentForm);
                currentForm=[];
            end        
            currentForm=ameva.AmevaForm(amevaData,panelM,tb,mtree,node,info);
        end
        catch ex
            msgbox(ex.message,'error','error');
        end
        core.blockMainForm;
    end

    function openAmevaGot(obj,obj2,lat,lon,new)
         core.blockMainForm;
        try
        if ~exist('new','var')
            new=false;
        end
        
        info.type='GOT';
        info.lat=lat;
        info.lon=lon;
        info.z=NaN;
        
        [time,tide]=data.getGOTTemporalSerie(db,lat,lon);  
        amevaData=containers.Map;
        
        tideVar=ameva.AmevaVar('tide','m',tide,time);
        amevaData('tide')=tideVar;       
              
        node=util.getNodeByKey(mtree,['ameva_rootgot_',num2str(lat),'_',num2str(lon)]);
        
       if new
            openAmevaNewWindow(node,info,amevaData)
        else
            if ~isempty(currentForm)
                delete(currentForm);
                currentForm=[];
            end        
            currentForm=ameva.AmevaForm(amevaData,panelM,tb,mtree,node,info);
       end
         catch ex
            msgbox(ex.message,'error','error');
        end
        core.blockMainForm;
    end

    function openAmevaNewWindow(node,info,amevaData)
        scrPos=get(0,'ScreenSize');
        width=800;
        height=600;
        f_ = figure( 'MenuBar', 'none', 'Name', ['Ameva ',get(node,'Name')],...
             'NumberTitle', 'off', 'Toolbar', 'none','Position',[(scrPos(3)-width)/2,(scrPos(4)-height)/2,width,height]...
             ,'CloseRequestFcn',@closeForm);
         tb_=uitoolbar(f_);
         panelM_=uipanel(f_,'Position',[0.25 0 0.75 1]);
         root_=uitreenode('v0',get(node,'Value'),get(node,'Name'),[],false);    
         util.setNodeIcon(root_,'dow.gif');
         pause(0.1);
         mtree_ = uitree('v0', 'Root', root_);
         set(mtree_,'Units','normalized')
         set(mtree_,'Position',[0 0 0.25 1])
         util.initTree(mtree_); 
         try
             core.blockMainForm;
         form_=ameva.AmevaForm(amevaData,panelM_,tb_,mtree_,root_,info); 
         catch ex             
             msgbox(ex.message,'error','error');
             delete(f_);             
         end
             
         core.blockMainForm;
         
         function closeForm(varargin)
            delete(form_);
            delete(f_);
        end
    end
 
    %%%   Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    
    
    %%%%%%%%%%%%%%%%%%%
    %OLDDDDDDDDD
    
%     
%     function cleanPoi(varargin)
%        jtree = handle(mtree.getTree,'CallbackProperties'); 
%        treePath=jtree.getSelectionPath();
%        moplaNode = treePath.getLastPathComponent;
%        nodeId=moplaNode.getValue();
%        valor=nodeId(12:end);
%        dataStr=regexp(valor,'_','split');
%        altName=dataStr{1};
%        moplaId=dataStr{2};       
%        files=dir(fullfile(project.folder,altName,moplaId,'*.poi'));
%         for i=1:length(files)
%             file=fullfile(fullfile(project.folder,altName),files(i).name);
%             delete(file);
%         end
%     end
% 
%     function rebuildPoi(varargin)
%        jtree = handle(mtree.getTree,'CallbackProperties'); 
%        treePath=jtree.getSelectionPath();
%        moplaNode = treePath.getLastPathComponent;
%        nodeId=moplaNode.getValue();
%        valor=nodeId(12:end);
%        dataStr=regexp(valor,'_','split');
%        altName=dataStr{1};
%        moplaId=dataStr{2};
%        mopla=smc.loadMopla(fullfile(project.folder,altName),moplaId);
%        alternativa=smc.loadAlternativa(project.folder,altName);
%        
%         nchilds=get(moplaNode,'ChildCount');
%         for i=1:nchilds-2 %El primer nodo (0) es el mopla y el último es el transporte
%             node=moplaNode.getChildAt(i);
%             nodeId=node.getValue();
%             dataStr=regexp(nodeId(10:end),'_','split');
%             poi=dataStr{3};            
%             data.getPOITemporalSerie(mopla,poi,project,alternativa);
%         end
%        
%        
%     end
% 
%  
    
    

end